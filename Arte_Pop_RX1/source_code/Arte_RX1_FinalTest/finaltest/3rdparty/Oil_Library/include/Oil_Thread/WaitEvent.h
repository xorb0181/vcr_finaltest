// WaitEvent.h: interface for the CWaitEvent class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_WAITEVENT_H__424A4B36_83AD_4E52_94EA_ABD0CECCE613__INCLUDED_)
#define AFX_WAITEVENT_H__424A4B36_83AD_4E52_94EA_ABD0CECCE613__INCLUDED_

#include "../OilBase.h"

#ifdef _LINUX
	#include <pthread.h>
#else
	#if _MSC_VER > 1000
	#pragma once
	#endif // _MSC_VER > 1000
#endif

class OILLIB_API CWaitEvent
{
public:
	CWaitEvent();
	virtual ~CWaitEvent();
	unsigned int WaitEvent(unsigned int dwMilliseconds = 0xFFFFFFFF);
	void SetEvent();

protected:
#ifdef _LINUX
	pthread_mutex_t		m_mutex;
	pthread_cond_t		m_cond;
	int					m_flag;
#else
	HANDLE m_hEndEvent;
	bool   m_bEndEvent;
#endif
};

#endif // !defined(AFX_WAITEVENT_H__424A4B36_83AD_4E52_94EA_ABD0CECCE613__INCLUDED_)

