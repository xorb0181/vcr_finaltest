// SingleSocket.cpp: implementation of the CSingleSocket class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "SingleSock.h"
#include "TCPPacket.h"
#include "stdio.h"
#include "assert.h"

#ifdef _LINUX
	#include <stdarg.h>
	#include <stdio.h>
#else
// 	#ifdef _DEBUG
// 	#undef THIS_FILE
// 	static char THIS_FILE[]=__FILE__;
// 	#define new DEBUG_NEW
// 	#endif
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CSingleSock::CSingleSock(int type, const char *strName) : 
m_type(type),
m_csock(strName),
m_ssock(strName)
{
	m_cbEvent    = NULL;
	m_ssock.SetCallBackOnAccept (this, CSingleSock::_cbAccept);

	m_csock.SetCallBackOnReceive(this, CSingleSock::_cbRecv  );
	m_csock.SetCallBackOnClose  (this, CSingleSock::_cbClose );
}

CSingleSock::~CSingleSock()
{
	Close();
}

void CSingleSock::Close()
{
	m_ssock.Close();
	m_csock.Close();
}

void CSingleSock::CloseClient()
{
	m_csock.Close();
}

bool CSingleSock::Listen(int portNum)
{
	assert(m_type == 0);
	m_csock.Create();
	return m_ssock.Create(portNum);
}

bool CSingleSock::Connect(const char *addr, int port)
{
	assert(m_type == 1);
	m_csock.Close();
	m_csock.Create();
	return m_csock.Connect(addr, port) ? true : false;
}

void CSingleSock::_cbAccept(void *pVoid, int nErrorCode) { ( (CSingleSock*) pVoid)->_OnAccept(nErrorCode); }
void CSingleSock::_OnAccept(             int nErrorCode)
{
	m_csock.Close();
	m_csock.StartRecv(m_ssock.AcceptSocket());

	if (m_cbEvent) m_cbEvent(m_pvEvent, eAccept, nErrorCode, 0);
}

void CSingleSock::_cbRecv (void *pVoid, const char *pBuf, int size) { ( (CSingleSock*) pVoid)->_OnRecv(pBuf, size); }
void CSingleSock::_OnRecv (             const char *pBuf, int size)
{
	if (m_cbEvent) m_cbEvent(m_pvEvent, eRecv  , (int)pBuf, size);
}

void CSingleSock::_cbClose(void *pVoid, int nErrorCode) { ( (CSingleSock*) pVoid)->_OnClose(nErrorCode); }
void CSingleSock::_OnClose(             int nErrorCode)
{
	if (m_cbEvent) m_cbEvent(m_pvEvent, eClose , 0, 0);
}

void CSingleSock::SetCallBackOnEvent  (void *pVoid, void(*cbEvent)(void *, int, int, int))
{
	m_cbEvent = cbEvent;
	m_pvEvent = pVoid;
}

int CSingleSock::SendString(const char *pBuf)
{
	if (!m_csock.IsConnected()) return -1;

//	return m_sendPacket.SendString(m_csock.GetSocketHandle(), pBuf);
	return m_csock.SendString(pBuf);
}

int CSingleSock::SendFormatString(const char* format, ...)
{
	va_list		args;
	char		temp[3000];
	
	va_start(args, format);
	vsprintf(temp, format, args);
	
	return m_csock.SendString(temp);
}

int CSingleSock::SendBinary(const char *pBuf, int size)
{
	if (!m_csock.IsConnected()) return - 1;
//	return m_sendPacket.SendBinary(m_csock.GetSocketHandle(), pBuf, size);
	return m_csock.SendBinary(pBuf, size);
}

int CSingleSock::SendRawData(const char *pBuf, int size)
{
	if (!m_csock.IsConnected()) return - 1;
	return TCPPacket::SockSendData(m_csock.GetSocketHandle(), pBuf, size);
}

void FragSend(char *pBuf, const int size, CSingleSock &sock)
{
	const int packetSize = 1400;
	int count = size / packetSize;
	int rest  = size%packetSize;

	if (size%packetSize)
		count++;

//	memmove(pData, pBuf, size);
	int index = 1;
	int remain = size;

	for (int i = 0 ; i < count ; i++)
	{
		pBuf[0] = count;
		pBuf[1] = index;

		int sendSize;
		
		if (remain/packetSize)
			sendSize = packetSize;
		else
			sendSize = remain%packetSize;

		sock.SendBinary(pBuf, sendSize + 2);
//		printf("ImgSend [%d/%d] [%d/%d] %d\n", i+1, count, remain, size, sendSize);

		Sleep(10);
		remain -= sendSize;

		if (remain)
			pBuf += sendSize;

		index++;
	}
}

bool CSingleSock::Create()
{
	return m_csock.Create();
}

int  CSingleSock::GetHeaderSize()
{
	return sizeof(TCPPacket::header_tag);
}

void CSingleSock::MakeHeader(char *pBuf, int bufSize)
{
	TCPPacket::header_tag *header = (TCPPacket::header_tag *)pBuf;
	header->iden       = TCPPacket::MAGIC_NUM ;
	header->msgLength  = bufSize;
	header->msgType    = TCPPacket::MSG_BINARY;
	header->encodeType = TCPPacket::ENCODE_NORMAL;
	header->timeStamp  = GetTickCount();
}
