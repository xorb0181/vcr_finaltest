// CClientSock.h: interface for the CClientSock class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_SOCKETTHREAD_H__FCBC66B7_3920_4428_86BF_BE63C1E6D6FC__INCLUDED_)
#define AFX_SOCKETTHREAD_H__FCBC66B7_3920_4428_86BF_BE63C1E6D6FC__INCLUDED_

#include "../OilBase.h"

#include "../Oil_Thread/SingleThread.h"
#include "../Oil_Thread/Sync.h"
#include "TCPPacket.h"

#ifdef _LINUX

#else
	#if _MSC_VER > 1000
	#pragma once
	#endif // _MSC_VER > 1000
#endif

class OILLIB_API CClientSock  
{
public:
	CClientSock(const char *strName = NULL);
	virtual ~CClientSock();

public:
	bool Create();
	bool Connect(const char *addr, int port);
	void Close();

	bool IsConnected();

	void SetCallBackOnReceive(void *pVoid, void(*cbOnRecv )(void *, const char *, int ));
	void SetCallBackOnClose  (void *pVoid, void(*cbOnClose)(void *, int               ));

	int SendBinary(const char *pBuf, int size);
	int SendString(const char *pBuf);
	void SetRecvTimeOut(int time_out);
	void SetName(const char *strName);

	bool StartRecv(SOCKET hSock);

	SOCKET GetSocketHandle() { return m_hSocket; }

protected:
	void _OnClose();
	void _CloseSocket();
	
protected:
	SOCKET    m_hSocket;
	TCPPacket m_pktRecv;
	TCPPacket m_pktSend;

protected:
	CSingleThread m_Thread;
	CSync         m_csRecv;

	int       m_nRecvTimeOut;
	char     *m_szSocketName;
	bool      m_bInit;

protected:
	       bool OnRecvLoop();
	static bool cbRecvLoop(void *pVoid);
	
	void (* m_cbOnRecv)(void *, const char *pBuf, int size);
	void  * m_pvOnRecv;

	void (* m_cbOnClose)(void *, int nErrorCode);
	void  * m_pvOnClose;
};

#endif // !defined(AFX_SOCKETTHREAD_H__FCBC66B7_3920_4428_86BF_BE63C1E6D6FC__INCLUDED_)
