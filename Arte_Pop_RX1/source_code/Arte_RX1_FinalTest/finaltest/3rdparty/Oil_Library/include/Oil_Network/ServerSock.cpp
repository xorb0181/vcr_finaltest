// NetworkModule.cpp: implementation of the CServerSock class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "ServerSock.h"
#include "dksocket.h"
#include "assert.h"

// #ifdef _DEBUG
// #undef THIS_FILE
// static char THIS_FILE[]=__FILE__;
// #define new DEBUG_NEW
// #endif

#ifdef _LINUX
	#define _msg printf
#else
	#ifndef _WIN32_WCE
		#define _msg
	#else
		#define _msg printf
	#endif
#endif // _LINUX

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CServerSock::CServerSock(const char *strName)
{
	char strBuf[256];
	m_nPort = 10000;
	m_sockServer = 0;
	m_cbOnAccept = NULL;

	if (strName)
	{
		assert(strlen(strName) < 240);
		sprintf(strBuf, "%s:CServerSock", strName);
		m_Thread.SetName(strBuf);
	}
	else
		m_Thread.SetName("CServerSock");
}

CServerSock::~CServerSock()
{
	Close();
}

bool CServerSock::Create(int nPort)
{
	Close();
	m_nPort = nPort;
	m_sockServer = sock_create_server(m_nPort);

	if (m_sockServer == -1)
	{
		_msg("[CServerSock  ] [%d] Socket Creation Fail\n", nPort);
		return false;
	}
	else
	{
		_msg("[CServerSock  ] [%d] Socket Creation Success\n", nPort);
	}

	return m_Thread.BeginThread(CServerSock::_cbNetworkLoop, this, 0);
}

static void SendMsg(TCPPacket &packet, SOCKET &socket, const char *Func, const char *SubCmd, bool Ret, const char *Msg="", const char*ETC="")
{
	packet.SendFormatString(socket, "Function=%s; SubCmd=%s; Ret=%s; Msg=%s; %s",
		Func, SubCmd, Ret ? "TRUE":"FALSE", Msg, ETC);
}

void CServerSock::Close()
{
	if (m_sockServer) { sock_close(m_sockServer); m_sockServer = 0; }
	m_Thread.StopThread();
}

void CServerSock::SetCallBackOnAccept(void *pVoid, void (*cbOnAccept)(void *pVoid, int nErrorCode) )
{
	m_cbOnAccept = cbOnAccept;
	m_pvOnAccept = pVoid;
}

SOCKET CServerSock::AcceptSocket()
{
	m_bAccepted = true;
	return m_sockClient;
}

bool CServerSock::_cbNetworkLoop(void *pParam) { return ((CServerSock *) pParam)->_OnNetworkLoop(); }
bool CServerSock::_OnNetworkLoop()
{
	if (!m_sockServer)
	{
		_msg("[CServerSock  ] [%d] Socket handle is NULL. thread will stop\n", m_nPort);
		return false;
	}

	TCPPacket packet;
//	_msg("[CServerSock  ] [%d] Wait Socket\n", m_nPort);
	int timeOut = 1000;
	m_sockClient = sock_accept(m_sockServer, timeOut);

	switch (m_sockClient)
	{
	case -1:
		{
			_msg("[CServerSock  ] [%d] Sever Socket Closed\n", m_nPort);
			return false;
		}
		break;
	case -2:
		{
//			_msg("[CServerSock  ] [%d] Accept Time Out\n", m_nPort);
		}
		break;
	default:
		{
			m_bAccepted = false;
			_msg("[CServerSock  ] [%d] Client Socket Accepted[%d]\n", m_nPort, m_sockClient);

			assert(m_cbOnAccept);
			m_cbOnAccept(m_pvOnAccept, 1);
			if (!m_bAccepted)
			{
				_msg("[CServerSock  ] [%d] Failed to Accept Socket\n", m_nPort);
				sock_close(m_sockClient);
			}
		}
	}

	return true;
}

