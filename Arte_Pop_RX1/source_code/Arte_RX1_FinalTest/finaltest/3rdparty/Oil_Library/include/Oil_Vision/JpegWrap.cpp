#include "stdafx.h"
#include <stdlib.h>
#include <stdio.h>

#include "JpegWrap.h"
#include "../Oil_Thread/Sync.h"

#include "IJGWrap.h"

/*
#ifdef _DEBUG
	#pragma comment(lib, "JpegLibD")
#else
	#pragma comment(lib, "JpegLib" )
#endif
*/

/*
#ifndef _WIN32_WCE
	#ifdef _DEBUG
		#pragma comment(lib, "JpegLibD.lib")
	#else
		#pragma comment(lib, "JpegLib.lib")
	#endif
#else
	#ifdef _DEBUG
		#pragma comment(lib, "JpegLibCED.lib")
	#else
		#pragma comment(lib, "JpegLibCE.lib")
	#endif
#endif
*/

CJpegWrap::CJpegWrap()
{
	m_pEncBuf = malloc(sizeof(jpeg_compress_struct  ) + sizeof(jpeg_error_mgr));
	m_pDecBuf = malloc(sizeof(jpeg_decompress_struct) + sizeof(jpeg_error_mgr));
	((jpeg_compress_struct  *)m_pEncBuf)->err = jpeg_std_error( (jpeg_error_mgr*) ( (char*)m_pEncBuf + sizeof(jpeg_compress_struct  )) );
	((jpeg_decompress_struct*)m_pDecBuf)->err = jpeg_std_error( (jpeg_error_mgr*) ( (char*)m_pDecBuf + sizeof(jpeg_decompress_struct)) );

	jpeg_create_compress  ((jpeg_compress_struct  *)m_pEncBuf);
	jpeg_create_decompress((jpeg_decompress_struct*)m_pDecBuf);
}

CJpegWrap::~CJpegWrap()
{
	jpeg_destroy_compress  ((jpeg_compress_struct  *)m_pEncBuf);
	jpeg_destroy_decompress((jpeg_decompress_struct*)m_pDecBuf);

	free(m_pEncBuf);
	free(m_pDecBuf);
}


void CJpegWrap::JpegMemToImageHeader (BYTE * jpegBuf, UINT bufsize, int &width, int &height, int &channel)
{
	CIJGWrap::JpegMemToImageHeader((jpeg_decompress_struct*)m_pDecBuf, jpegBuf, bufsize, width, height, channel);
}

bool CJpegWrap::JpegMemToImageBody   (BYTE *dst, int step)
{
	return CIJGWrap::JpegMemToImageBody((jpeg_decompress_struct*)m_pDecBuf, dst, step);
}

void CJpegWrap::JpegFileToImageHeader(FILE * infile, int &width, int &height, int &channel)
{
	CIJGWrap::JpegFileToImageHeader((jpeg_decompress_struct*)m_pDecBuf, infile, width, height, channel);
}

bool CJpegWrap::JpegFileToImageBody  (FILE * infile, BYTE *dst, int step)
{
	return CIJGWrap::JpegFileToImageBody((jpeg_decompress_struct*)m_pDecBuf, infile, dst, step);
}

bool CJpegWrap::ImageToJpegMem (const BYTE *src, int step, int width, int height, int channel, BYTE *pBuffer, int &nBufSize, int quality)
{
	return CIJGWrap::ImageToJpegMem((jpeg_compress_struct*)m_pEncBuf, pBuffer, nBufSize, src, step, width, height, channel, quality);
}




/*
CJpegTrans::CJpegTrans()
{
	m_pImage   = NULL;
	m_nBufSize = 0;
	m_pBuf     = NULL;
	m_bLastState = false;
}

CJpegTrans::CJpegTrans(const ByteImage &image)
{
	m_pImage   = (ByteImage*)(&image);
	m_nBufSize = 0;
	m_pBuf     = NULL;
	m_bLastState = false;
}

CJpegTrans::CJpegTrans(ByteImage &image)
{
	m_pImage   = &image;
	m_nBufSize = 0;
	m_pBuf     = NULL;
	m_bLastState = false;
}

CJpegTrans::~CJpegTrans()
{
	if (m_pBuf) delete [] m_pBuf;
}

void CJpegTrans::Allocate(int nSize, int offset)
{
	if (nSize > m_nBufSize)
	{
//		if (m_nBufSize)	printf("[CJpegTrans] Buffer size is changed %d -> %d offset:%d\n", m_nBufSize, nSize, offset);
		if (m_pBuf) delete [] m_pBuf;
		m_nBufSize = nSize + offset;
		m_pBuf = new char[m_nBufSize];
	}
}

CJpegTrans & CJpegTrans::operator () (const ByteImage &image)
{
	m_pImage = (ByteImage*)&image;
	return *this;
}

CJpegTrans & CJpegTrans::operator () (ByteImage &image)
{
	m_pImage = &image;
	return *this;
}

std::ostream & operator << (std::ostream &ios, CJpegTrans &src)
{
	src.Allocate(src.m_pImage->GetXSize()*src.m_pImage->GetYSize()*src.m_pImage->GetCSize()/2, 0);
	if (src.m_jpeg.ImageToJpegMem(*src.m_pImage, (BYTE*)src.m_pBuf, src.m_nBufSize))
	{
		ios.write((char*)&src.m_nBufSize, sizeof(src.m_nBufSize));
		ios.write(src.m_pBuf, src.m_nBufSize);
		src.m_bLastState = true;
	}
	else
	{
		int size = -1;
		ios.write((char*)&size, sizeof(size));
		src.m_bLastState = false;
	}

	return ios;
}

std::istream & operator >> (std::istream &ios, CJpegTrans &src)
{
	int size;
	ios.read((char*)&size, sizeof(size));
	if (size != -1)
	{
		src.Allocate(size, size/4);
		ios.read(src.m_pBuf, size);
		src.m_jpeg.JpegMemToImage((BYTE*)src.m_pBuf, size, *src.m_pImage);
		src.m_bLastState = true;
	}
	else
		src.m_bLastState = false;
	return ios;
}
*/

/*
bool jpwImageToJpegMem(const ByteImage &input, BYTE *pBuffer, int &nBufSize, int quality)
{
#ifdef _USE_IJL_FOR_JPEG_WRAP_
	return CIJLWrap::ImageToJpegMem(pBuffer, nBufSize, input, quality);
#else
	return CIJGWrap::ImageToJpegMem(pBuffer, nBufSize, input, quality);
#endif
}
*/