#if !defined(AFX_CJpegWrap_H__4DAEA3D1_4DDD_49A7_B3DE_11F58CE23636__INCLUDED_)
#define AFX_CJpegWrap_H__4DAEA3D1_4DDD_49A7_B3DE_11F58CE23636__INCLUDED_

#include "../OilBase.h"

// #include "vipl.h"

class OILLIB_API CJpegWrap  
{
public:
	CJpegWrap();
	virtual ~CJpegWrap();

public:
	void JpegMemToImageHeader (BYTE * jpegBuf, UINT bufsize, int &width, int &height, int &channel);
	bool JpegMemToImageBody   (BYTE *dst, int step);
	void JpegFileToImageHeader(FILE * infile, int &width, int &height, int &channel);
	bool JpegFileToImageBody  (FILE * infile, BYTE *dst, int step);

	bool ImageToJpegMem (const BYTE *src, int step, int width, int height, int channel, BYTE *pBuffer, int &nBufSize, int quality = 75);

protected:
	void *m_pEncBuf;
	void *m_pDecBuf;
};

/*
bool CJpegWrap::JpegMemToImage(BYTE *pBuffer, int nBufSize, ByteImage &output)
{
	int width, height, channel;
	JpegMemToImageHeader(pBuffer, nBufSize, width, height, channel);
	
	output.Allocate(width, height, channel);
	
	return JpegMemToImageBody(output.GetBuffer(), output.GetWidthStep());
}

ByteImage CJpegWrap::JpegMemToImage(BYTE *pBuffer, int nBufSize)
{
	ByteImage output; JpegMemToImage(pBuffer, nBufSize, output);
	return output;
}

bool CJpegWrap::JpegFileToImage(const char * filename, ByteImage &output)
{
	FILE * infile=fopen(filename, "r");
	if ( !infile) return false;
	
	int width, height, channel;
	JpegFileToImageHeader(infile, width, height, channel);
	output.Allocate(width, height, channel);
	
	bool ret = JpegFileToImageBody(infile, output.GetBuffer(), output.GetWidthStep());
	fclose(infile);
	return ret;
}

ByteImage CJpegWrap::JpegFileToImage(const char * filename)
{
	ByteImage output;
	JpegFileToImage(filename, output);
	return output;
}

bool CJpegWrap::ImageToJpegFile(const ByteImage &input, const char *  filename, int quality)
{
	return CIJGWrap::ImageToJpegFile(
		(jpeg_compress_struct*)m_pEncBuf, filename, 
		input.GetBufferConst(), input.GetWidthStep(), input.GetXSize(), input.GetYSize(), 
		input.GetCSize() == 3 ? true : false, quality);
}

bool CJpegWrap::ImageToJpegMem (const ByteImage &input, BYTE *pBuffer, int &nBufSize, int quality)
{
	return CIJGWrap::ImageToJpegMem((jpeg_compress_struct*)m_pEncBuf, pBuffer, nBufSize, 
		input.GetBufferConst(), input.GetWidthStep(), input.GetXSize(), input.GetYSize(), 
		input.GetCSize() == 3 ? true : false, quality);
}
*/

// class OILLIB_API CJpegTrans
// {
// public:
// 	CJpegTrans();
// 	CJpegTrans(ByteImage &image);
// 	CJpegTrans(const ByteImage &image);
// 	virtual ~CJpegTrans();
// 
// 	void Allocate(int nSize, int offset);
// 
// 	CJpegTrans & operator () (ByteImage &image);
// 	CJpegTrans & operator () (const ByteImage &image);
// 
// public:
// 	CJpegWrap m_jpeg;
// 	ByteImage *m_pImage;
// 	char *m_pBuf;
// 	int   m_nBufSize;
// 	bool  m_bLastState;
// };
// 
// std::ostream & operator << (std::ostream &ios, CJpegTrans &src);
// std::istream & operator >> (std::istream &ios, CJpegTrans &src);

/*
ByteImage CreateImageFromJPEG(const char * filename);
ByteImage CreateImageFromJPEG(BYTE *pBuffer, int nBufSize);

bool WriteImageToJPEGFile(const ByteImage &input, const char *filename, int quality = 75);
*/
// bool jpwImageToJpegMem(const ByteImage &input, BYTE *pBuffer, int &nBufSize, int quality = 75);

#endif // !defined(AFX_CJpegWrap_H__4DAEA3D1_4DDD_49A7_B3DE_11F58CE23636__INCLUDED_)
