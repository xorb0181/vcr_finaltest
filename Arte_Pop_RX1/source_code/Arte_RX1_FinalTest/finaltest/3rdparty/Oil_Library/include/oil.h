 // VipEmbTools.h: interface for the CVipEmbTools class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(_oil_main_def_)
#define _oil_main_def_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef _WIN32_WCE

	#ifdef _DEBUG
		#pragma comment(lib, "OilWin32DLLD")
	#else
		#pragma comment(lib, "OilWin32DLL")
	#endif

#else
	#ifdef _viplib_emb_
		#ifdef _DEBUG
			#pragma comment(lib, "OilArmV4D.lib")
//			#pragma comment(lib, "JpegLibCED.lib")
		#else
			#pragma comment(lib, "OilArmV4.lib")
//			#pragma comment(lib, "JpegLibCE.lib")
		#endif
	#else
		#ifdef _DEBUG
			#pragma comment(lib, "OilWinCEDLLD.lib")
		#else
			#pragma comment(lib, "OilWinCEDLL.lib")
		#endif
	#endif
#endif

#endif // !defined(_oil_main_def_)
