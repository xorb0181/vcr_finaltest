 // VipEmbTools.h: interface for the CVipEmbTools class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(_DEF_OIL_BASE_)
#define _DEF_OIL_BASE_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


#ifdef _LINUX
	#define OILLIB_API 
#else
	#ifdef _viplib_emb_
		#define OILLIB_API 
	#else
		#ifdef OILLIB_EXPORTS
			#define OILLIB_API __declspec(dllexport)
		#else
			#define OILLIB_API __declspec(dllimport)
		#endif
	#endif
#endif

#endif // !defined(_DEF_OIL_BASE_)
