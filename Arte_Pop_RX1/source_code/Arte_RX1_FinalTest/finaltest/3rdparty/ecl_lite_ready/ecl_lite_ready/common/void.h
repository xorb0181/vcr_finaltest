/**
 * @file include/utilities/void.hpp
 * 
 * @brief A void object.
 * 
 * A void (empty) object that is occasionally useful.
 * 
 * @author Daniel J. Stonier
 * @date August 2007
 * 
 **/

/*****************************************************************************
** Defines
*****************************************************************************/

#ifndef YCL_CORE_VOID_HPP_
#define YCL_CORE_VOID_HPP_

/*****************************************************************************
** Includes
*****************************************************************************/

/*****************************************************************************
** Namespaces
*****************************************************************************/

namespace efl {


#define NULL 0

/*****************************************************************************
** Typedefs
*****************************************************************************/
/**
 * @brief Convenient type definition for empty functions.
 **/
typedef void (*VoidFunction)();

/*****************************************************************************
** Classes
*****************************************************************************/
/**
 * @brief A void (empty) object.
 * 
 * A void (empty) object that is occasionally useful for template instantiations.
 * When you want to instantiate a template class without a type, just set 
 * its default type to this void object and create a specialisation of that
 * template class.
 **/
class Void
{
    public:
        /*********************
        ** C&D
        **********************/
        Void() {};
        ~Void() {};

        template <typename OutputStream>
        friend OutputStream& operator << (OutputStream& ostream, const Void v);
};

/**
 * @brief Output stream operator for Void objects.
 * 
 * This function not serve any purpose except to allow the stream to move 
 * on to the next output object.
 **/
template <typename OutputStream>
OutputStream& operator << (OutputStream& ostream, const Void v) { return ostream; }

}; // Namespace efl



#endif /*YCL_CORE_VOID_HPP_*/
