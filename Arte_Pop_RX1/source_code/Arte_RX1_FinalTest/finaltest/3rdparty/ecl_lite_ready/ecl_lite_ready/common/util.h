#ifndef UTIL_HPP_
#define UTIL_HPP_

#include "container.h"

template<typename T>
class delayFunction
{
public:
  delayFunction() {}
  void operator() ( T cnt )
  {
    while( (--cnt) > 0 );
  }
};


template<int Period>
class periodicAction
{
public:
  periodicAction() 
  : counter(0) 
  {}

  periodicAction( int initialCounterValue ) 
  : counter(initialCounterValue) 
  {}
  
  bool doI()
  {
    if( ++counter >= Period )   
    {
      counter = 0;
      return true;
    }
    else       
    {
      return false;
    }
  }

  int getCounter()
  {
    return counter;
  }

protected:
  int counter;
};


template<typename F, F fValue, int DT=0 >
class waitForFlag
{
public:
  waitForFlag( F * guestObject )
  : guest(guestObject)
  {}

  bool operator() (void) 
  {
    if( DT == 0 )
    {
      return (*guest == fValue) ? true : false;
    }
    else if( DT == -1 )
    {
      delayFunction<int> delay;
      while( *guest != fValue ) delay(10);

      return true;
    }
    else
    {
      delayFunction<int> delay;      
      while( *guest != fValue ) delay( DT );
      return (*guest == fValue) ? true : false;
    }
  }
private:
  F * guest;
};               

template<typename T>
class repeatNTimes
{
public:
  repeatNTimes( const int desiredN, const T desiredValue )
  : N(desiredN), repeat(0), desired_value(desiredValue)
  {}
  
  bool operator() ( const T currentValue )
  {
    if( currentValue == desired_value ) repeat ++;
    else repeat = 0;
    
    return (repeat>=N)? true:false;    
  }

  int N;    
  int repeat;
  T desired_value;  
};

/*
template<typename T, int numberOfSample=16>
class MovingAvg
{
public:
  // orderOfSmoothingSample: MovingAvg make 2^orderOfSmoothingSample sample bin
  MovingAvg()
  :
  bin(),
  first_data(true),
  sum(0),
  average(0)
  {}

  int operator() (T datum)
  {
    if( first_data )
    {
      first_data=false;
      sum = datum*numberOfSample;
      average = datum;
      for( int i=0; i<(numberOfSample); i++ )
        bin.push_back( datum );
      return average;
    }
    
    int front = bin.pop_front();
    sum -= front;
    sum += datum;
    bin.push_back( datum );        
    average = (sum / numberOfSample);

    return average;
  }

  int operator() ()
  {
    return average;
  }
         
protected:
  PushAndPop<T,numberOfSample> bin;
  bool first_data;
  int sum;
  int average;
};
 */



template<typename T>
class IncreaseAndBound
{
public:
  IncreaseAndBound() {}
  IncreaseAndBound( const T & LowLimit, const T & UpperLimit) : LB(LowLimit), UB(UpperLimit) {}

  void operator() ( T & v, const T & accValue, const T & LowLimit, const T & UpperLimit)
  {
    // increase first with accValue
    v += accValue;
    
    // then saturate target value
    if( v > UpperLimit ) v = UpperLimit;
    if( v < LowLimit ) v = LowLimit;
  }

  void operator() ( T & v, const T & accValue )
  {
    // increase first with accValue
    v += accValue;

    // then saturate target value
    if( v > UB ) v = UB;
    if( v < LB ) v = LB;
  }

  int LB;
  int UB;
};


template<typename T, T LB, T UB>
class Bound
{
public:
  Bound(){}
  T operator() ( T V )
  {
    if( V < LB ) return LB;
    else if( V > UB ) return UB;
    else return V;
  }  
};


class absValue
{
public:
  template<typename T>
  T operator() ( const T & v )
  {
    return v > 0 ? v : -v;
  }
};


template<typename T>
class getMin
{
public:
  getMin(){}  
  T operator()( const T & v0, const T & v1 )
  {
    return v0 < v1 ? v0 : v1;
  }
};


class getMax
{
public:
  getMax(){}
  template<typename T>
  const T operator()( const T & v0, const T & v1 )
  {
    return v0 > v1 ? v0 : v1;
  }
};



#endif

