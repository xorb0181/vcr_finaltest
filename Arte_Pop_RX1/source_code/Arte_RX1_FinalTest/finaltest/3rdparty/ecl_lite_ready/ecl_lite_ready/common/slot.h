/**
 * @file include/signals/slot.hpp
 *
 * @brief Slots interface & implementation.
 *
 * Signals and slots provide a means for communication of events between
 * classes and methods in your application. This file includes the definition
 * details for the slots. Further details can be found in the @ref sigslotsDocumentation
 * "SigSlots" documentation.
 *
 * @author Daniel J. Stonier
 * @date July 2007
 **/
/*****************************************************************************
** Ifdefs
*****************************************************************************/

#ifndef SLOT_HPP_
#define SLOT_HPP_

/*****************************************************************************
** Includes
*****************************************************************************/

#include "void.h"
#include "function_handler.h"

/*****************************************************************************
** Namespaces
*****************************************************************************/

namespace efl {

/*****************************************************************************
** Using
*****************************************************************************/

/*****************************************************************************
 * Enums
 ****************************************************************************/
/**
 * @brief Indicate the priority of this slot.
 *
 * Indicates the priority of the slot. Mostly these are set by default to
 * prioritise threaded vs non-threaded slots, however,
 * if you wish, the slot template parameter for its priority can
 * be over-ridden.
 **/
enum SlotPriority {
    Low,
    High
};

/*****************************************************************************
** Classes
*****************************************************************************/

/*****************************************************************************
** Forward Declarations
*****************************************************************************/

template <typename T> class Signal;

/*****************************************************************************
** Slot
*****************************************************************************/
/**
 * @brief These provide a functor-style implementation for callbacks.
 *
 * Anywhere that a callback is required can be implemented with a slot. These
 * can be placed anywhere in your code and are initialised with either a
 * static (global) function, or a member function. Once initialised, they can
 * be hooked up to a signal (see Signal).
 *
 * Further details can be found in the \ref sigslotsDocumentation "SigSlots" documentation.
 *
 * @sa test_sigslots.cpp
 **/
template <typename T = Void, enum SlotPriority Priority = Low> 
class Slot
{
    /*********************
    ** Friends
    **********************/
    friend class Signal<T>;

    public:
        /*********************
        ** C&D's
        **********************/
        Slot() : functionHandler(0) {};
        template <typename F> Slot(F function);
        template <typename O, typename F> Slot(O *object, F function);
        virtual ~Slot();


        /*********************
         * Slot Methods
         *********************/
        template <typename F> void load(F function );
        template <typename O, typename F> void load(O *object, F function )
    {
        if ( functionHandler != NULL ) { delete functionHandler; }
        functionHandler = new MemberFunctionHandler<T,O>(object,function);
    }

        void unload();
        virtual void processSignal(T data);

     protected:
         /*********************
         ** Variables
         **********************/
         FunctionHandler<T> *functionHandler;/**< Function and execution details for this slot. **/
};

/*****************************************************************************
** Specialisations for <Void,Priority>
*****************************************************************************/

/*****************************************************************************
** Slot<Void,Priority>
*****************************************************************************/
/**
 * @brief Specialised slot to be used with a signal that doesn't pass
 * a message.
 *
 * Specialised slot that can be loaded with a function that receives no
 * arguments. This is the default state for a slot, so you need only
 * define the signal as shown in this code snippet.
 *
 * @code
 * void cb_Empty() { cout << "Empty Slot"; }
 *
 * int main() {
 * Slot<> slot;
 * Signal<> signal;
 *
 * slot.load(&cb_Empty);
 * signal.connect(slot);
 *
 * signal.emit();
 * @endcode
 **/
template <enum SlotPriority Priority> 
class Slot<Void,Priority>
{
    /*********************
    ** Friends
    **********************/
    friend class Signal<Void>;

    public:
        /*********************
        ** C&D's
        **********************/
        Slot() : functionHandler(NULL) {}; /**< Default constructor. **/
        template <typename F> Slot(F function);
        template <typename O, typename F> Slot(O *obj, F function);
        virtual ~Slot();

        /*********************
        ** Methods
        **********************/
        template <typename F> void load(F function );
        template <typename O, typename F> void load(O *obj, F function )
    {
       if ( functionHandler != NULL ) { delete functionHandler; }
        functionHandler = new MemberFunctionHandler<Void,O>(obj,function);
     }
        void unload();
        virtual void processSignal();

    protected:
        FunctionHandler<Void> *functionHandler; /**< Function and execution details for this slot. **/
};

/*****************************************************************************
** Template Code
*****************************************************************************/
/**
 * Default constructor.
 **/
//template <typename T, enum SlotPriority Priority>
//Slot<T,Priority>::Slot(): functionHandler(NULL) {}
/**
 * Constructor that accepts a global function of the form:
 *     typedef void (*F)(T data_);
 **/
template <typename T, enum SlotPriority Priority>
template<typename F>
Slot<T,Priority>::Slot(F memberFunction_)  : functionHandler(NULL)
{
    functionHandler = new GlobalFunctionHandler<T>(memberFunction_);
};
/**
 * Constructor that accepts a member function of the form:
 *     typedef void (O::*F)(T data_);
 * for some class O.
 **/
template <typename T, enum SlotPriority Priority>
template< typename O, typename F>
Slot<T,Priority>::Slot(O *object, F function) : functionHandler(NULL)
{
    functionHandler = new MemberFunctionHandler<T,O>(object,function);
};
/** Default destructor. **/
template <typename T, enum SlotPriority Priority>
Slot<T,Priority>::~Slot()
{
    if ( functionHandler != NULL ) { delete functionHandler; }
};

/**
 * Loads the slot with a global function of the form:
 *     F = void (*)(T data_);
 **/
template <typename T, enum SlotPriority Priority> 
template <typename F> 
void Slot<T,Priority>::load(F function )
{
    if ( functionHandler != NULL ) { delete functionHandler; }
    functionHandler = new GlobalFunctionHandler<T>(function);
}

/**
 * Loads the slot with a member function of the form:
 *     F = void (O::*)(T data_);
 * for some class O.
 **/
//template <typename T, enum SlotPriority Priority> 
//template <typename O, typename F> 
//void Slot<T,Priority>::load(O *object, F function )
//{
//    if ( functionHandler != NULL ) { delete functionHandler; }
//    functionHandler = new MemberFunctionHandler<T,O>(object,function);
//}

/**
 * Unloads the slot.
 **/
template <typename T, enum SlotPriority Priority>
void Slot<T,Priority>::unload()
{
    if ( functionHandler != NULL ) {
        delete functionHandler;
        functionHandler = NULL;
    }
}
/**
 * The executor of the slot's function code. Could also be set up
 * by overriding the operator() class - doing so is more usual, but
 * I prefer explicitly setting it out like this.
 **/
template <typename T, enum SlotPriority Priority> void Slot<T,Priority>::processSignal(T data)
{
    if( functionHandler ) { functionHandler->run(data); }
}

/*****************************************************************************
** Slot<Void,Priority> Implementation
*****************************************************************************/

/**
 * Constructor that accepts a global function of the form:
 *     typedef void (*F)(T data_);
 **/
template <enum SlotPriority Priority>
template <typename F>
Slot<Void,Priority>::Slot(F function)  : functionHandler(NULL)
{
    functionHandler = new GlobalFunctionHandler<Void>(function);
};

/**
 * Default destructor.
 **/
template <enum SlotPriority Priority>
Slot<Void,Priority>::~Slot() {
    if ( functionHandler != NULL ) {
        delete functionHandler;
    } else {
    }
};

/**
 * Constructor that accepts a member function of the form:
 *     typedef void (O::*F)(T data_);
 * for some class O.
 **/

template <enum SlotPriority Priority>
template <typename O, typename F>
Slot<Void,Priority>::Slot(O *obj, F function) : functionHandler(NULL)
{
    functionHandler = new MemberFunctionHandler<Void,O>(obj,function);
};

 /**
 * Loads the slot with a global function of the form:
 *     typedef void (*F)(T data_);
 **/
template <enum SlotPriority Priority>
template <typename F>
void Slot<Void,Priority>::load(F function )
{
 if ( functionHandler != NULL ) { delete functionHandler; }
    functionHandler = new GlobalFunctionHandler<Void>(function);
}
 /**
 * Loads the slot with a member function of the form:
 *     typedef void (O::*F)(T data_);
 * for some class O.
 **/
//template <enum SlotPriority Priority>
//template <typename O, typename F>
//void Slot<Void,Priority>::load(O *obj, F function ) {
// if ( functionHandler != NULL ) { delete functionHandler; }
//    functionHandler = new MemberFunctionHandler<Void,O>(obj,function);
// }
/**
 * Unloads the slot.
 **/
template <enum SlotPriority Priority>
void Slot<Void,Priority>::unload()
{
    if ( functionHandler != NULL ) {
        delete functionHandler;
        functionHandler = NULL;
    }
}
/**
 * The executor of the slot's function code. Could also be set up
 * by overriding the operator() class - doing so is more usual, but
 * I prefer explicitly setting it out like this.
 **/
template <enum SlotPriority Priority>
void Slot<Void,Priority>::processSignal() {
 if( functionHandler ) { functionHandler->run(); }
}

}; // Namespace efl



#endif /*SLOT_HPP_*/
