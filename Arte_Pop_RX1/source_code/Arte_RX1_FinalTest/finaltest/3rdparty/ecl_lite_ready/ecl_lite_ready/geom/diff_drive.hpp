#ifndef DIFF_DRIVE_HPP_
#define DIFF_DRIVE_HPP_


#include "geom.hpp"

class DifferentialDriveKinematics
{
public:
  DifferentialDriveKinematics( const float wheelToWheel, const float encoderToDisplacement )
    :   
  encoder_to_displacement( encoderToDisplacement ),
    wheel_to_wheel( wheelToWheel ), 
    travel_distance( 0 ),
    travel_heading(0),
    x(0),y(0),t(0),
    reset_encoder( true )
  {}
  
  void forward( const unsigned short int encoderLeft, const unsigned short int encoderRight )
  {
    if( reset_encoder )
    {
      encoder_left   = encoderLeft;
      encoder_right  = encoderRight;   
      reset_encoder   = false;
    }
    float dleft   = encoder_to_displacement* static_cast<float>( static_cast<signed short int>(encoderLeft - encoder_left) );
    float dright   = encoder_to_displacement* static_cast<float>( static_cast<signed short int>(encoderRight - encoder_right) );
    encoder_left   = encoderLeft;
    encoder_right   = encoderRight;
    
    float dv = (dleft + dright)*0.5f;
    float dw = (dright - dleft ) / wheel_to_wheel;
    
    forward(dv,dw);
  }
  
  //@param dv; mm
  //@param dw: radian
  void forward( const float dv, const float dw )
  {
    x += dv*cosf( dw + t );
    y += dv*sinf( dw + t );
    t += dw;
    
    t = dWrapAngle<float>()( t, pi, two_pi );
    
    travel_distance += dv;
    travel_heading  += dw;
  }
  
  template<typename T>
    void getPose2D( pose2D<T> & pose )
  {
    pose.p.x = static_cast<T>( x );
    pose.p.y = static_cast<T>( y );
    pose.t = Radian2degree<float,T>()( t, static_cast<T>(180) );      
  }
  
  template<typename T>
    void getPose2D10TimesDegree( pose2D<T> & pose )
  {
    pose.p.x = static_cast<T>( x );
    pose.p.y = static_cast<T>( y );
    pose.t = Radian2degree<T,float>()( t, static_cast<T>(1800) );
  }
  
  void getTravelDistance( int & travelDistance )
  {
    travelDistance = static_cast<int>(travel_distance);
  }
  
  void getTravelHeadingIn10TiemsDegree( int & travelHeading )
  {
    travelHeading = static_cast<int>(Radian2degree<int,float>()(travel_heading,1800.0f));
  }
  
private:
  float encoder_to_displacement;  // in [mm]                        
  float wheel_to_wheel;
  float travel_distance;
  float travel_heading;
  float x;  // robot coordinate along to x-axis in [mm]
  float y;     // robot coordinate along to y-axis in [mm]
  float t;  // robot heading in radian  
  unsigned short int encoder_left;
  unsigned short int encoder_right;
  bool reset_encoder;  
  };

#endif//#define IMATH_HPP_
