#ifndef DEVICE_TIME_HPP_
#define DEVICE_TIME_HPP_

//#include "../common/container.h"
//#include "../common/util.h"
#include "device_driver.h"

//#define USE_HIGH_RESOLUTION_TIME

typedef struct _time_t
{
  _time_t() : s(0), ns(0) {}
  unsigned long s;
  unsigned long ns;
}time_t;
                       
class DevTime : public DeviceDriver
{
public:  
  DevTime()
  : 
  DeviceDriver(0,-1)
  {
  }

#ifdef USE_HIGH_RESOLUTION_TIME
  static void setNanoSecondPerTick( const unsigned long & nanoSeconPerTick )
  {
    ns_tick = nanoSeconPerTick; 
  }
#else
  static void setMicroSecondPerTick( const unsigned long & microSeconPerTick )
  {
    us_tick = microSeconPerTick;
  }
#endif

  static void tick()
  {
    system_tick ++;
#ifdef USE_HIGH_RESOLUTION_TIME
    system_time.ns += ns_tick;
    if( system_time.ns > 1000000000 )
    {
      system_time.s += 1;
      system_time.ns -= 1000000000;
    }
#else
    us_time += us_tick;
#endif
  }

  static unsigned long getTickCount()
  {
    return system_tick;
  }
#ifdef USE_HIGH_RESOLUTION_TIME
  static unsigned long getNanoSecondPerTick()
  {
    return ns_tick;
  }
#else  
  static unsigned long getMicroSecondPerTick()
  {
    return us_tick;
  }
#endif

#ifdef USE_HIGH_RESOLUTION_TIME
  static unsigned long getMilliSecond()
  {  
    return (system_tick * ns_tick)/1000000;
  }

  static unsigned long getMicroSecond()
  {  
    return (system_tick * ns_tick)/1000;
  }
#else
  static unsigned long getMilliSecond()
  {  
    return (system_tick * us_tick)/1000;
  }

  static unsigned long getMicroSecond()
  {  
    return us_time;
  }
#endif

  static void sleepInMicroSecond( unsigned long sleepTime )
  {
    unsigned long end_time( getMicroSecond() + sleepTime );
    while( end_time > getMicroSecond() );
  }

protected:
  static time_t system_time;
  static unsigned long system_tick;
  static unsigned long ns_tick;

  // above three variable will be used for high resolution time management
  // below are used for low resolution time management
  static unsigned long us_time;
  static unsigned long us_tick;
};



#endif

