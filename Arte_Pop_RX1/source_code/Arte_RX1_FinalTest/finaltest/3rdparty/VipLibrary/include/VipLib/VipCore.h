////////////////////////////////////////////////////////////////////////////////////////
//
// Note : Functions presented in 'Vipcore.h' are not fixed yet.
//

#if !defined(_VIP_CORE_FUNC_)
#define _VIP_CORE_FUNC_

#include "ViplBase.h"
#include "math.h"

#include "VipDebug.h"

template <class T> class Vip2DPtr
{
public:
	T   *data;
	int  width_step;
	inline T & operator () (int x, int y)
	{
		return *((T*)((char*&)data + y*width_step) + x);
	}

	inline T & Read(int x, int y) const
	{
		return *((T*)((char*&)data + y*width_step) + x);
	}
};

template <class T> class Vip2DInfo
{
public:
	Vip2DPtr <T> buf;
	int width;
	int height;

	inline T & operator () (int x, int y)
	{
		VIP_ASSERT((unsigned)(x) < (unsigned)(width ) && (unsigned)(y) < (unsigned)(height), "", "");
		return *((T*)((char*&)buf.data + y*buf.width_step) + x);
	}

	inline T & Read (int x, int y) const
	{
		VIP_ASSERT((unsigned)(x) < (unsigned)(width ) && (unsigned)(y) < (unsigned)(height), "", "");
		return *((T*)((char*&)buf.data + y*buf.width_step) + x);
	}
};

template <class T>       Vip2DPtr <T> vip2DPtr(      T *data, int width_step)
{
	Vip2DPtr <T> ret; ret.data = data; ret.width_step = width_step;
	return ret;
}

template <class T>       Vip2DPtr <T> vip2DPtr(      Vip2DPtr <T> src, int x, int y)
{
	return vip2DPtr((T*) &(src(x, y)), src.width_step);
}

template <class T>       Vip2DInfo <T> vip2DInfo(      Vip2DPtr <T> src, int width, int height)
{
	Vip2DInfo <T> ret;
	ret.buf    = src;
	ret.width  = width;
	ret.height = height;
	return ret;
}

#define VIP_PTR(type, src, wStep, x, y) ((type *)((char*)(src) + (y) * (wStep)) + (x))

inline int vipAlign( int size, int align ) { return (size + align - 1) & -align; }

#define VIP_2D_FUNC_LOOP1_IMG1(img1, step1, height, operation)                                  \
for (; height-- ; (char*&)img1 += step1)                                                        \
{ operation }                                                                                   \

#define VIP_2D_FUNC_LOOP1_IMG2(img1, step1, img2, step2, height, operation)                     \
for (; height-- ; (char*&)img1 += step1, (char*&)img2  += step2)                                \
{ operation }                                                                                   \

#define VIP_2D_FUNC_LOOP1_IMG3(img1, step1, img2, step2, img3, step3, height, operation)        \
for (; height-- ; (char*&)img1 += step1, (char*&)img2  += step2, (char*&)img3  += step3)        \
{ operation }                                                                                   \

#define VIP_2D_FUNC_LOOP2_IMG1(img1, step1, width, height, operation)                           \
for (; height-- ; (char*&)img1 += step1)                                                        \
{                                                                                               \
	for (int x = 0 ; x < width ; x++)                                                           \
	{ operation }                                                                               \
}

#define VIP_2D_FUNC_LOOP2_IMG2(img1, step1, img2, step2, width, height, operation)              \
for (; height-- ; (char*&)img1 += step1, (char*&)img2  += step2)                                \
{                                                                                               \
	for (int x = 0 ; x < width ; x++)                                                           \
	{ operation }                                                                               \
}

#define VIP_2D_FUNC_LOOP2_IMG3(img1, step1, img2, step2, img3, step3, width, height, operation) \
for (; height-- ; (char*&)img1 += step1, (char*&)img2  += step2, (char*&)img3  += step3)        \
{                                                                                               \
	for (int x = 0 ; x < width ; x++)                                                           \
	{ operation }                                                                               \
}



#define VIP_2D_FUNC(func_name, operation)                                              \
template <class T1, class T2, class T3>                                                \
void func_name(                                                                        \
	const T1 *src1, int src1Step, const T2 *src2, int src2Step, T3 *dst , int dstStep, \
	int width, int height)                                                             \
{                                                                                      \
	VIP_2D_FUNC_LOOP2_IMG3(src1, src2Step, src2, src2Step, dst, dstStep, width, height,\
		dst[x] = (T3)(src1[x] operation src2[x]); );                                   \
}

#define VIP_2D_FUNC_CONST(func_name, operation)                                        \
template <class T1, class T2, class T3>                                                \
void func_name(                                                                        \
	const T1 *src1, int src1Step, const T2 src2, T3 *dst , int dstStep,                \
	int width, int height)                                                             \
{                                                                                      \
	VIP_2D_FUNC_LOOP2_IMG2(src1, src1Step, dst, dstStep, width, height,                \
		dst[x] = (T3)(src1[x] operation src2); );                                      \
}

#define VIP_2D_FUNC_CONST_FLIP(func_name, operation)                                    \
template <class T1, class T2, class T3>                                                 \
void func_name(                                                                         \
	const T1 *src1, int src1Step, const T2 src2, T3 *dst , int dstStep,                 \
	int width, int height, bool flip)                                                   \
{                                                                                       \
	if (!flip)                                                                          \
	{                                                                                   \
		VIP_2D_FUNC_LOOP2_IMG2(src1, src1Step, dst, dstStep, width, height,             \
				dst[x] = (T3)(src1[x] operation src2); );                               \
	}                                                                                   \
	else                                                                                \
	{                                                                                   \
		VIP_2D_FUNC_LOOP2_IMG2(src1, src1Step, dst, dstStep, width, height,             \
				dst[x] = (T3)(src2 operation src1[x]); );                               \
	}                                                                                   \
}

#define VIP_2D_FUNC_INPLACE(func_name, operation)                                       \
template <class T1, class T2>                                                           \
void func_name(const T1 *src, int srcStep, T2 *dst, int dstStep, int width, int height) \
{                                                                                       \
	VIP_2D_FUNC_LOOP2_IMG2(src, srcStep, dst, dstStep, width, height,                   \
		dst[x] operation (T2)src[x]; );                                                 \
}
	
#define VIP_2D_FUNC_CONST_INPLACE(func_name, operation)                                 \
template <class T1, class T2>                                                           \
void func_name(const T1 src, T2 *dst, int dstStep, int width, int height)              \
{                                                                                       \
	T2 value = (T2)src;                                                                 \
	VIP_2D_FUNC_LOOP2_IMG1(dst, dstStep, width, height,                                 \
		dst[x] operation value; );                                                      \
}

VIP_2D_FUNC              (vipAdd   , + )
VIP_2D_FUNC              (vipSub   , - )
VIP_2D_FUNC              (vipMul   , * )
VIP_2D_FUNC              (vipDiv   , / )

VIP_2D_FUNC_CONST        (vipAddC  , + )
VIP_2D_FUNC_CONST_FLIP   (vipSubC  , - )
VIP_2D_FUNC_CONST        (vipMulC  , * )
VIP_2D_FUNC_CONST_FLIP   (vipDivC  , / )

VIP_2D_FUNC_INPLACE      (vipAdd_I , +=)
VIP_2D_FUNC_INPLACE      (vipSub_I , -=)
VIP_2D_FUNC_INPLACE      (vipMul_I , *=)
VIP_2D_FUNC_INPLACE      (vipDiv_I , /=)

VIP_2D_FUNC_CONST_INPLACE(vipAddC_I, +=);
VIP_2D_FUNC_CONST_INPLACE(vipSubC_I, -=);
VIP_2D_FUNC_CONST_INPLACE(vipMulC_I, *=);
VIP_2D_FUNC_CONST_INPLACE(vipDivC_I, /=);

template <class T>
void vipTranspose(const T *src, int srcStep, int srcWidth, int srcHeight, T *dst, int dstStep, int elCount = 1)
{
	int sStep    = srcStep / sizeof(T);
	int dStep    = dstStep / sizeof(T);

	int Decrem   = (srcWidth * dStep - elCount);

	if (elCount == 1)
	{
		for(int j = 0; j < srcHeight; j++ )
		{
			for(int i = 0; i < srcWidth; i++)
			{
				*dst = src[i];
				dst += dStep;
			}
			dst -= Decrem;
			src += sStep;
		}
	}
	else
	{
		int addWidth   = elCount * srcWidth ;
		int ScalarSize = elCount * sizeof(T);
		for(int j = 0; j < srcHeight; j++ )
		{
			for(int i = 0 ; i < addWidth ; i += elCount )
			{
				memcpy(dst, src + i, ScalarSize);
				dst += dStep;
			}
			dst -= Decrem;
			src += sStep;
		}
	}
}

template <class T>
void vipTranspose(const Vip2DPtr <T> src, int srcWidth, int srcHeight, Vip2DPtr <T> dst, int elCount = 1)
{
	vipTranspose(src.data, src.width_step, srcWidth, srcHeight, dst.data, dst.width_step, elCount);
}

template <class T>
void vipMatMul(
	const T *src1, int srcStep1, int srcWidth1, int srcHeight1,
	const T *src2, int srcStep2, int srcWidth2, int srcHeight2,
	      T * dst, int dstStep  )
{
	VIP_ASSERT(srcWidth1 == srcHeight2, "", "");

	for (int j = 0 ; j < srcHeight1 ; j++, (char*&)src1 += srcStep1, (char*&)dst += dstStep)
	{
		for (int i = 0 ; i < srcWidth2 ; i++)
		{
			double sum = 0;
			const T * tmpSrc2 = src2 + i;
			for (int k = 0 ; k < srcWidth1 ; k++, (char*&)tmpSrc2 += srcStep2)
				sum += src1[k] * (*tmpSrc2);
			dst[i] = (T)sum;
		}
	}
}

template <class T>
void vipMatMul(
	const Vip2DPtr <T> src1, int srcWidth1, int srcHeight1,
	const Vip2DPtr <T> src2, int srcWidth2, int srcHeight2,
	      Vip2DPtr <T> dst)
{
	vipMatMul(src1.data, src1.width_step, srcWidth1, srcHeight1, src2.data, src2.width_step, srcWidth2, srcHeight2, dst.data, dst.width_step);
}

// A^T*A
template <class T>
void vipMulTransposedR(
	const T *src, int srcstep,
	      T *dst, int dststep, 
	int width, int height,
	T*col_buf, int buf_size)
{
	int i, j, k;
	T* tdst = dst;
	
	VIP_ASSERT(buf_size >= height*sizeof(T), "", "");
	
	for ( i = 0; i < width; i++, (char*&)tdst += dststep )
	{
		// copy i-th column
		for( j = 0; j < height; j++ )
			col_buf[j] = ((T*)((char*)src + j*srcstep))[i];
		
		for( j = i; j < width; j++ )
		{
			double s = 0;
			const T *tsrc = src + j;
			
			for( k = 0; k < height; k++, (char*&)tsrc += srcstep )
			{
				s += col_buf[k] * tsrc[0];
			}
			tdst[j] = (T)s;
		}
	}
	
	// fill the lower part of the destination matrix
	for( i = 1; i < width; i++ )
	{
		for( j = 0; j < i; j++ )
			((T*)((char*)dst + dststep*i))[j] =
			((T*)((char*)dst + dststep*j))[i];
	}
}

template <class T>
void vipMulTransposedR(
	const Vip2DPtr <T> src, Vip2DPtr <T> dst,
	int width, int height,
	T*col_buf, int buf_size)
{
	vipMulTransposedR(src.data, src.width_step, dst.data, dst.width_step,
		width, height, col_buf, buf_size);
}

// DST = A*A^T
template <class T>
void vipMulTransposedL(
	const T* src, int srcstep,
	      T* dst, int dststep,
	int width, int height)
{
	int i, j, k;
	T* tdst = dst;
	
	for( i = 0; i < height; i++, (char*&)tdst += dststep )
	{
		for( j = i; j < height; j++ )
		{
			double s = 0;
			const T *tsrc1 = (const T*)((char*)src + i*srcstep);
			const T *tsrc2 = (const T*)((char*)src + j*srcstep);
			
			for( k = 0; k < width; k++ )
				s += tsrc1[k] * tsrc2[k];
			tdst[j] = (T)s;
		}
	}
	
	// fill the lower part of the destination matrix
	for( j = 0; j < height - 1; j++ )
	{
		for( i = j; i < height; i++ )
			((T*)((char*)dst + dststep*i))[j] =
			((T*)((char*)dst + dststep*j))[i];
	}
}

template <class T>
void vipMulTransposedL(
	const Vip2DPtr <T> src, Vip2DPtr <T> dst,
	int width, int height)
{
	vipMulTransposedL(src.data, src.width_step, dst.data, dst.width_step, width ,height);
}

template <class T>
void vipCopy(
	const T *src, int srcStep,
	      T *dst, int dstStep,
	int width, int height)
{
	if (width == 1)
	{
		VIP_2D_FUNC_LOOP1_IMG2(src, srcStep, dst, dstStep, height,
			*dst = *src; );
	}
	else
	{
		int wSize = width * sizeof(T);
		VIP_2D_FUNC_LOOP1_IMG2(src, srcStep, dst, dstStep, height,
			memcpy(dst, src, wSize); );
	}
}

template <class T>
void vipCopy(
	const Vip2DPtr <T> src, Vip2DPtr <T> dst,
	int width, int height)
{
	vipCopy(src.data, src.width_step, dst.data, dst.width_step, width, height);
}

template <class T>
void vipMemSet(T *src, int srcStep, int width, int height, int value)
{
	int wSize = width * sizeof(T);
	VIP_2D_FUNC_LOOP1_IMG1(src, srcStep, height,
        memset(src, value, wSize); );
}

template <class T>
void vipMemSet(Vip2DPtr <T> src, int width, int height, int value)
{
	vipMemSet(src.data, src.width_step, width, height, value);
}

template <class T>
void vipMinMax(const T *src, int srcStep, int width, int height, T &minV, T &maxV, T minI = (T)  3.402823466e+38F, T maxI = (T) -3.402823466e+38F)
{
	T minVV = minI;
	T maxVV = maxI;

	VIP_2D_FUNC_LOOP2_IMG1(src, srcStep, width, height,
		if (src[x] < minVV) minVV = src[x];
		if (src[x] > maxVV) maxVV = src[x]; );
	minV = minVV;
	maxV = maxVV;
}

template <class T>
void vipMinMax(const Vip2DPtr <T> src, int width, int height, T &minV, T &maxV, T minI = (T)  3.402823466e+38F, T maxI = (T) -3.402823466e+38F)
{
	vipMinMax(src.data, src.width_step, width, height, minV, maxV, minI);
}

template <class T>
void vipMin(const T *src, int srcStep, int width, int height, T &minV, T minI = (T)  3.402823466e+38F)
{
	T minVV = minI;

	VIP_2D_FUNC_LOOP2_IMG1(src, srcStep, width, height,
		if (src[x] < minVV) minVV = src[x]; );
	minV = minVV;
}

template <class T>
void vipMax(const T *src, int srcStep, int width, int height, T &maxV, T maxI = (T) -3.402823466e+38F)
{
	T maxVV = maxI;
	VIP_2D_FUNC_LOOP2_IMG1(src, srcStep, width, height,
		if (src[x] > maxVV) maxVV = src[x]; );
	maxV = maxVV;
}

template <class T>
double vipNorm_L2(const T *src, int srcStep, int width, int height)
{
	double ret = 0;
	VIP_2D_FUNC_LOOP2_IMG1(src, srcStep, width, height,
		ret += src[x]*src[x]; );
	return ret;
}

template <class T>
double vipNorm_L2(const T *src1, int src1Step, const T *src2, int src2Step, int width, int height)
{
	double ret = 0;
	VIP_2D_FUNC_LOOP2_IMG2(src1, src1Step, src2, src2Step, width, height,
		T v = src1[x] - src2[x];
		ret += v*v; );
	return ret;
}

template <class T1, class T2>
void vipConvert(
	const T1 *src, int srcStep,
	      T2 *dst, int dstStep,
	int width, int height)
{
	VIP_2D_FUNC_LOOP2_IMG2(src, srcStep, dst, dstStep, width, height,
		dst[x] = (T2)src[x]; );
}

template <class T>
void vipAssign(T *dst, int dstStep, int width, int height, const T Value)
{
	if (Value == 0)
	{
		VIP_2D_FUNC_LOOP2_IMG1(dst, dstStep, width, height,
			memset(dst, 0, dstStep); );
	}
	else if (sizeof(T) == 1)
	{
		VIP_2D_FUNC_LOOP2_IMG1(dst, dstStep, width, height,
			memset(dst, Value, dstStep); );
	}
	else
	{
		int len = width - 4;
		for( ; height--; (char*&)dst += dstStep )
		{
			int x;
			for (x = 0 ; x < len   ; x+=4)
				dst[x] = dst[x+1] = dst[x+2] = dst[x+3] = Value;
			for (      ; x < width ; x++ )
				dst[x] = Value;
		}
	}
}

template <class T>
void vipCross(const T *src1, const T*src2, T *dst)
{
	dst[0] =                    - src1[2] * src2[1] + src1[1] * src2[2];
	dst[1] =  src1[2] * src2[0]                     - src1[0] * src2[2];
	dst[2] = -src1[1] * src2[0] + src1[0] * src2[1]                    ;
}

template <class T>
double vipSum_1d(const T *src, int count)
{
	double sum = 0;
	int len = count - 4;
	int x;
	for (x = 0 ; x < len   ; x+=4)
		sum += src[x] + src[x+1] + src[x+2] + src[x+3];
	for (      ; x < count ; x++ )
		sum += src[x];
	return sum;
}

template <class T>
double vipSum(const T *src, int srcStep, int width, int height)
{
	double sum = 0;
	int len = width - 4;
	for( ; height--; (char*&)src += srcStep )
	{
		int x;
		for (x = 0 ; x < len ; x+=4)
			sum += src[x] + src[x+1] + src[x+2] + src[x+3];
		for (      ; x < width ; x++)
			sum += src[x];
	}
	return sum;
}

template <class T>
double vipDot_1d(const T *src1, const T *src2, int count)
{
	double sum = 0;
	int len = count - 4;
	int x;
	for (x = 0 ; x < len   ; x+=4)
		sum += src1[x]*src2[x] + src1[x+1]*src2[x+1] + src1[x+2]*src2[x+2] + src1[x+3]*src2[x+3];
	for (      ; x < count ; x++ )
		sum += src1[x]*src2[x];
	return sum;
}

template <class T>
double vipDot(const T *src1, int src1Step, const T *src2, int src2Step, int width, int height)
{
	double sum = 0;
	int len = width - 4;
	for( ; height--; (char*&)src1 += src1Step, (char*&)src2 += src2Step )
	{
		int x;
		for (x = 0 ; x < len   ; x+=4)
			sum += src1[x]*src2[x] + src1[x+1]*src2[x+1] + src1[x+2]*src2[x+2] + src1[x+3]*src2[x+3];
		for (      ; x < width ; x++ )
			sum += src1[x]*src2[x];
	}
	return sum;
}

template <class T1, class T2>
void vipThres_1d(const T1 *src, T2 *dst, int count, T1 thres, T2 Big = 255, T2 Small = 0)
{
	while(count--)
		dst[count] = (src[count] > thres) ? Big : Small;;
}

template <class T1, class T2>
void vipThres(const T1 *src, int srcStep, T2 *dst, int dstStep, int width, int height, T1 thres, T2 Big = 255, T2 Small = 0)
{
	VIP_2D_FUNC_LOOP2_IMG2(src, srcStep, dst, dstStep, width, height,
		dst[x] = (src[x] > thres) ? Big : Small; );
}

template <class T>
void vipShiftL(const T *src, int srcStep, T *dst, int dstStep, int width, int height, int shift)
{
	VIP_2D_FUNC_LOOP2_IMG2(src, srcStep, dst, dstStep, width, height, 
		dst[x] = src[x]<<shift; );
}

template <class T>
void vipShiftR(const T *src, int srcStep, T *dst, int dstStep, int width, int height, int shift)
{
	VIP_2D_FUNC_LOOP2_IMG2(src, srcStep, dst, dstStep, width, height, 
		dst[x] = src[x]>>shift; );
}

template <class T>
void vipShiftL_I(T *src_dst, int widthStep, int width, int height, int shift)
{
	VIP_2D_FUNC_LOOP2_IMG1(src_dst, widthStep, width, height,
		src_dst[x] <<= shift; );
}

template <class T>
void vipShiftR_I(T *src_dst, int widthStep, int width, int height, int shift)
{
	VIP_2D_FUNC_LOOP2_IMG1(src_dst, widthStep, width, height,
		src_dst[x] >>= shift; );
}

template <class T1, class T2, class T3>
void vipTruncateMinMax(const T1 *src, int srcStep, T2 *dst, int dstStep, int width, int height, T3 minV, T3 maxV)
{
	VIP_2D_FUNC_LOOP2_IMG2(src, srcStep, dst, dstStep, width, height,
		T1 tmp = __max(src[x], minV);
		dst[x] = (T2) ( __min(tmp, maxV) );  );
}

template <class T1, class T2, class T3>
void vipTruncateMin(const T1 *src, int srcStep, T2 *dst, int dstStep, int width, int height, T3 minV)
{
	VIP_2D_FUNC_LOOP2_IMG2(src, srcStep, dst, dstStep, width, height,
		dst[x] = (T2) ( __max(src[x], minV) );  );
}

template <class T1, class T2, class T3>
void vipTruncateMax(const T1 *src, int srcStep, T2 *dst, int dstStep, int width, int height, T3 maxV)
{
	VIP_2D_FUNC_LOOP2_IMG2(src, srcStep, dst, dstStep, width, height,
		dst[x] = (T2) ( __min(src[x], maxV) );  );
}

template <class T1, class T2>
void vipSqrt(const T1 *src, int srcStep, T2 *dst, int dstStep, int width, int height)
{
	VIP_2D_FUNC_LOOP2_IMG2(src, srcStep, dst, dstStep, width, height,
		dst[x] = (T2) sqrt(src[x]);  );
}

template <class T1, class T2>
void vipAbs_i(const T1 *src, int srcStep, T2 *dst, int dstStep, int width, int height)
{
	VIP_2D_FUNC_LOOP2_IMG2(src, srcStep, dst, dstStep, width, height,
		dst[x] = (T2) abs(src[x]);  );
}

template <class T1, class T2>
void vipAbs_f(const T1 *src, int srcStep, T2 *dst, int dstStep, int width, int height)
{
	VIP_2D_FUNC_LOOP2_IMG2(src, srcStep, dst, dstStep, width, height,
		dst[x] = (T2) fabs((double) src[x]);  );
}

template <class T1, class T2>
void vipPow(const T1 *src, int srcStep, T2 *dst, int dstStep, int width, int height, T1 value)
{
	VIP_2D_FUNC_LOOP2_IMG2(src, srcStep, dst, dstStep, width, height,
		dst[x] = (T2) pow(src[x], value);  );
}

template <class T1, class T2>
void vipSquare(const T1 *src, int srcStep, T2 *dst, int dstStep, int width, int height)
{
	VIP_2D_FUNC_LOOP2_IMG2(src, srcStep, dst, dstStep, width, height,
		dst[x] = src[x]*src[x];  );
}

template <class T>
void vipSqrt_I(T *src, int srcStep, int width, int height)
{
	VIP_2D_FUNC_LOOP2_IMG1(src, srcStep, width, height,
		src[x] = (T) sqrt(src[x]);  );
}

template <class T>
double vipMean(const T *src, int srcStep, int width, int height)
{
	return vipSum(src, srcStep, width, height) / double(width*height);
};

template <class T1, class T2>
double vipVariance(const T1 *src, int srcStep, int width, int height, T2 mean)
{
	double sum = 0;

	int len = width - 4;
	double dim = width*height;
	for( ; height--; (char*&)src += srcStep )
	{
		int x;
		for (x = 0 ; x < len ; x+=4)
		{
			T2 buf0 = (T2)(src[x  ] - mean);
			T2 buf1 = (T2)(src[x+1] - mean);
			T2 buf2 = (T2)(src[x+2] - mean);
			T2 buf3 = (T2)(src[x+3] - mean); 
			sum += buf0*buf0 + buf1*buf1 + buf2*buf2 + buf3*buf3;
		}
		for (      ; x < width ; x++)
		{
			T2 buf0 = (T2)(src[x  ] - mean);
			sum += buf0*buf0;
		}
	}
	return sum / dim;
}

template <class T1, class T2>
void vipMeanZeroUnitVar(const T1 *src, int srcStep, const T2 *dst, int dstStep, int width, int height)
{
	T2 mean = (T2)vipMean(src, srcStep, width, height);
	T2 var  = (T2)vipVariance(src, srcStep, width, height, mean);

	T2 sqrt_var = (T2)sqrt(var);

	if (var)
	{
		VIP_2D_FUNC_LOOP2_IMG2(src, srcStep, dst, dstStep, width, height,
			dst[x] = (src[x]-mean)         ; );
	}
	else
	{
		VIP_2D_FUNC_LOOP2_IMG2(src, srcStep, dst, dstStep, width, height,
			dst[x] = (src[x]-mean)/sqrt_var; );
	}
}

template <class T>
bool vipIsNaN(const T *src, int srcStep, int width, int height)
{
	VIP_2D_FUNC_LOOP2_IMG1(src, srcStep, width, height,
		if (_isnan(src[x])) return true; );
	return false;
}

template <class T>
bool vipIsNaN_1d(const T *src, int count)
{
	while(count--)
		if (_isnan(src[count])) return true;
	return false;
}

#define VIP_1D_FUNC(func_name, operation)                                               \
template <class T1, class T2, class T3>                                                 \
void func_name(const T1 *src1, const T2 *src2, T3 *dst, int count)                      \
{                                                                                       \
	for (int i = 0 ; i < count ; i++)                                                   \
		dst[i] = (T3)(src1[i] operation src2[i]);                                       \
}

#define VIP_1D_FUNC_CONST(func_name, operation)                                         \
template <class T1, class T2, class T3>                                                 \
void func_name(const T1 *src1, const T2 src2, T3 *dst, int count)                       \
{                                                                                       \
	for (int i = 0 ; i < count ; i++)                                                   \
		dst[i] = (T3)(src1[i] operation src2);                                          \
}

#define VIP_1D_FUNC_CONST_FLIP(func_name, operation)                                    \
template <class T1, class T2, class T3>                                                 \
void func_name(const T1 *src1, const T2 src2, T3 *dst, int count, bool flip)            \
{                                                                                       \
	if (!flip)                                                                          \
		for (int i = 0 ; i < count ; i++)                                               \
			dst[i] = (T3)(src1[i] operation src2);                                      \
	else                                                                                \
		for (int i = 0 ; i < count ; i++)                                               \
			dst[i] = (T3)(src2 operation src1[i]);                                      \
}

#define VIP_1D_FUNC_INPLACE(func_name, operation)                                       \
template <class T1, class T2>                                                           \
void func_name(const T1 *src, T2 *dst, int count)                                       \
{                                                                                       \
	for (int i = 0 ; i < count ; i++)                                                   \
		dst[i] operation (T2)src[i];                                                    \
}
	
#define VIP_1D_FUNC_CONST_INPLACE(func_name, operation)                                 \
template <class T1, class T2>                                                           \
void func_name(const T1 *src, T2 *dst, int count)                                       \
{                                                                                       \
	T2 value = (T2)src;                                                                 \
	for (int i = 0 ; i < count ; i++)                                                   \
		dst[i] operation value;                                                         \
}

VIP_1D_FUNC              (vipAdd_1d   , + )
VIP_1D_FUNC              (vipSub_1d   , - )
VIP_1D_FUNC              (vipMul_1d   , * )
VIP_1D_FUNC              (vipDiv_1d   , / )

VIP_1D_FUNC_CONST        (vipAddC_1d  , + )
VIP_1D_FUNC_CONST_FLIP   (vipSubC_1d  , - )
VIP_1D_FUNC_CONST        (vipMulC_1d  , * )
VIP_1D_FUNC_CONST_FLIP   (vipDivC_1d  , / )

VIP_1D_FUNC_INPLACE      (vipAdd_I_1d , +=)
VIP_1D_FUNC_INPLACE      (vipSub_I_1d , -=)
VIP_1D_FUNC_INPLACE      (vipMul_I_1d , *=)
VIP_1D_FUNC_INPLACE      (vipDiv_I_1d , /=)

VIP_1D_FUNC_CONST_INPLACE(vipAddC_I_1d, +=)
VIP_1D_FUNC_CONST_INPLACE(vipSubC_I_1d, -=)
VIP_1D_FUNC_CONST_INPLACE(vipMulC_I_1d, *=)
VIP_1D_FUNC_CONST_INPLACE(vipDivC_I_1d, /=)


template <class T>
bool vipInverse2x2(const T *src, T *dst)
{
	T det = src[0]*src[3] - src[1]*src[2];
	if (det == 0) return false;
	dst[0] =  src[3] / det; dst[1] = -src[1] / det;
	dst[2] = -src[2] / det; dst[3] =  src[0] / det;
	return true;
}

template <class T>
bool vipInverse3x3(const T *src, T *dst)
{
	T det     = (src[0] * (src[4] * src[8] - src[5] * src[7]) +
				 src[1] * (src[5] * src[6] - src[3] * src[8]) +
				 src[2] * (src[3] * src[7] - src[4] * src[6]));
	if (det == 0) return false;
	T Mult     = T(1) / det;
	dst[0] = (src[4] * src[8] - src[5] * src[7]) * Mult;
	dst[1] = (src[2] * src[7] - src[1] * src[8]) * Mult;
	dst[2] = (src[1] * src[5] - src[2] * src[4]) * Mult;
	dst[3] = (src[5] * src[6] - src[3] * src[8]) * Mult;
	dst[4] = (src[0] * src[8] - src[2] * src[6]) * Mult;
	dst[5] = (src[2] * src[3] - src[0] * src[5]) * Mult;
	dst[6] = (src[3] * src[7] - src[4] * src[6]) * Mult;
	dst[7] = (src[1] * src[6] - src[0] * src[7]) * Mult;
	dst[8] = (src[0] * src[4] - src[1] * src[3]) * Mult;
	return true;
}

template <class T>
bool vipInverse4x4(const T *src, T *dst)
{
	T di = src[0];
	T d  = T(1.0) / di;

	dst[ 0] = d;
	dst[ 4] = src[ 4] * -d;
	dst[ 8] = src[ 8] * -d;
	dst[12] = src[12] * -d;
	dst[ 1] = src[ 1] *  d;
	dst[ 2] = src[ 2] *  d;
	dst[ 3] = src[ 3] *  d;
	dst[ 5] = src[ 5] + dst[ 4] * dst[1] * di;
	dst[ 6] = src[ 6] + dst[ 4] * dst[2] * di;
	dst[ 7] = src[ 7] + dst[ 4] * dst[3] * di;
	dst[ 9] = src[ 9] + dst[ 8] * dst[1] * di;
	dst[10] = src[10] + dst[ 8] * dst[2] * di;
	dst[11] = src[11] + dst[ 8] * dst[3] * di;
	dst[13] = src[13] + dst[12] * dst[1] * di;
	dst[14] = src[14] + dst[12] * dst[2] * di;
	dst[15] = src[15] + dst[12] * dst[3] * di;
	di = dst[5];
	if (di == 0) return false;
	dst[ 5] = d = 1.0f / di;
	dst[ 1] *= -d;
	dst[ 9] *= -d;
	dst[13] *= -d;
	dst[ 4] *=  d;
	dst[ 6] *=  d;
	dst[ 7] *=  d;
	dst[ 0] +=  dst[ 1] * dst[4] * di;
	dst[ 2] +=  dst[ 1] * dst[6] * di;
	dst[ 3] +=  dst[ 1] * dst[7] * di;
	dst[ 8] +=  dst[ 9] * dst[4] * di;
	dst[10] +=  dst[ 9] * dst[6] * di;
	dst[11] +=  dst[ 9] * dst[7] * di;
	dst[12] +=  dst[13] * dst[4] * di;
	dst[14] +=  dst[13] * dst[6] * di;
	dst[15] +=  dst[13] * dst[7] * di;
	di = dst[10];
	if (di == 0) return false;
	dst[10] = d = 1.0f / di;
	dst[ 2] *= -d;
	dst[ 6] *= -d;
	dst[14] *= -d;
	dst[ 8] *=  d;
	dst[ 9] *=  d;
	dst[11] *=  d;
	dst[ 0] +=  dst[ 2] * dst[ 8] * di;
	dst[ 1] +=  dst[ 2] * dst[ 9] * di;
	dst[ 3] +=  dst[ 2] * dst[11] * di;
	dst[ 4] +=  dst[ 6] * dst[ 8] * di;
	dst[ 5] +=  dst[ 6] * dst[ 9] * di;
	dst[ 7] +=  dst[ 6] * dst[11] * di;
	dst[12] +=  dst[14] * dst[ 8] * di;
	dst[13] +=  dst[14] * dst[ 9] * di;
	dst[15] +=  dst[14] * dst[11] * di;
	di = dst[15];
	if (di == 0) return false;
	dst[15] = d = 1.0f / di;
	dst[ 3] *= -d;
	dst[ 7] *= -d;
	dst[11] *= -d;
	dst[12] *=  d;
	dst[13] *=  d;
	dst[14] *=  d;
	dst[ 0] +=  dst[ 3] * dst[12] * di;
	dst[ 1] +=  dst[ 3] * dst[13] * di;
	dst[ 2] +=  dst[ 3] * dst[14] * di;
	dst[ 4] +=  dst[ 7] * dst[12] * di;
	dst[ 5] +=  dst[ 7] * dst[13] * di;
	dst[ 6] +=  dst[ 7] * dst[14] * di;
	dst[ 8] +=  dst[11] * dst[12] * di;
	dst[ 9] +=  dst[11] * dst[13] * di;
	dst[10] +=  dst[11] * dst[14] * di;
	return true;
}

template <class T>
bool vipLUDecomp(
	T* A, int stepA, int widthA, int heightA,
	T* B, int stepB, int widthB, int heightB,
	double* _det )
{
	int n = widthA;
	int m = 0, i;
	double det = 1;
	
	VIP_ASSERT( widthA == heightA, "vipLUDecomp", "Wrong Matrix Dimension");
	
	if( B )
	{
		VIP_ASSERT( heightA == heightB, "vipLUDecomp", "Wrong Matrix Dimension");
		m = widthB;
	}
	
	for( i = 0;i < n;i++, (char*&)A += stepA, (char*&)B += stepB )
	{
		int j, k = i;
		T *tA = A, *tB = 0;
		T kval = (T)fabs(A[i]);
		double inv_val;
		
		// find the pivot element
		for( j = i + 1;j < n;j++ )
		{ 
			T tval;
			(char*&)tA += stepA;
			
			tval = (T)fabs(tA[i]);
			if( tval > kval )
			{ 
				kval = tval;
				k = j;
			} 
		} 
		
		if( kval == 0 )
		{ 
			det = 0;
			break;
		} 
		
		// swap rows
		if( k != i )
		{ 
			tA = (T*)((char*)A + stepA*(k - i));
			det = -det;
			
			for( j = i;j < n;j++ )
			{ 
				T t = A[j]; 
				A[j] = tA[j];
				tA[j] = t;
			} 
			
			if( m > 0 )
			{ 
				tB = (T*)((char*)B + stepB*(k - i));
				
				for( j = 0;j < m;j++ )
				{ 
					T t = B[j];
					B[j] = tB[j];
					tB[j] = t;
				} 
			} 
		} 
		
		inv_val = 1./A[i];
		det *= A[i];
		tA = A;
		tB = B;
		A[i] = (T)inv_val;
		
		// update matrix and the right side of the system
		for( j = i + 1;j < n;j++ )
		{ 
			double alpha;
			
			(char*&)tA += stepA;
			(char*&)tB += stepB;
			
			alpha = -tA[i]*inv_val;
			
			for( k = i + 1;k < n;k++ )
				tA[k] = (T)(tA[k] + alpha*A[k]);
			
			if( m > 0 )
				for( k = 0;k < m;k++ )
					tB[k] = (T)(tB[k] + alpha*B[k]);
		} 
	} 
	
	if( _det )
		*_det = det;
	
	return true;
}

template <class T>
bool vipLUBack(
	T* A, int stepA, int widthA, int heightA,
	T* B, int stepB, int widthB, int heightB)
{
	int n = widthA;
	int m = widthB, i;
	
	VIP_ASSERT( m > 0 && widthA == heightA && 
		heightA == heightB, "vipLUBack", "Wrong Matrix Dimemsion");
	
	(char*&)A += stepA*(n - 1);
	(char*&)B += stepB*(n - 1);
	
	for( i = n - 1;i >= 0;i--, (char*&)A -= stepA )
	{ 
		int j, k;
		
		for( j = 0;j < m;j++ )
		{ 
			T* tB = B + j;
			double x = 0;
			
			for( k = n - 1;k > i;k--, (char*&)tB -= stepB )
				x += A[k]*tB[0];
			
			tB[0] = (T)((tB[0] - x)*A[i]);
		} 
	} 
	
	return true;
}

template <class T>
bool vipInverseNxN(const T *src, int srcStep, T *dst, int dstStep, int row_colSize, T *pBuf = NULL)
{
	T * buf;
	if (pBuf)
		buf = pBuf;
	else
		buf = new T[row_colSize * row_colSize];
	
	int bufStep = sizeof(T)*row_colSize;
	vipCopy(src, srcStep, buf, bufStep, row_colSize, row_colSize);

	int height = row_colSize;
	T *dstBuf = dst;
	int offset = 0;
	VIP_2D_FUNC_LOOP1_IMG1(dstBuf, dstStep, height, memset(dstBuf, 0, dstStep); dstBuf[offset++] = 1; );

	double det = 0;
    vipLUDecomp(
		buf, bufStep, row_colSize, row_colSize,
		dst, dstStep, row_colSize, row_colSize, &det);

    if( det != 0 )
    {
        vipLUBack(
			buf, bufStep, row_colSize, row_colSize,
			dst, dstStep, row_colSize, row_colSize);
    }

	if (!pBuf)
		delete [] buf;

	if (det) 
		return true;
	else
		return false;
}

template <class T>
bool vipInverse(const T *src, int srcStep, T *dst, int dstStep, int row_colSize)
{
	VIP_ASSERT(row_colSize > 0, "", "");
	if (srcStep == row_colSize*(int)sizeof(T) && dstStep == row_colSize*(int)sizeof(T))
	{
		switch(row_colSize)
		{
		case 1: dst[0] = T(1) / src[0];	break;
		case 2: return vipInverse2x2(src, dst);
		case 3: return vipInverse3x3(src, dst);
		case 4: return vipInverse4x4(src, dst);
		default:
				return vipInverseNxN(src, srcStep, dst, dstStep, row_colSize);
		}
	}
	else
	{
		return vipInverseNxN(src, srcStep, dst, dstStep, row_colSize);
	}
	return true;
}

#define VIP_TEMP_SWAP(a,b) {temp=(a);(a)=(b);(b)=temp;}

template <class T>
void vipGaussJ(T **src, int n, T **buf, int m)
{
	int i,icol,irow,j,k,l,ll;
	T big,dum,pivinv,temp;

	int *indxc = new int[n];
	int *indxr = new int[n];
	int *ipiv  = new int[n];

	for (j=0;j<n;j++) ipiv[j]=0;
	for (i=0;i<n;i++) 
	{
		big=0.0;
		for (j=0;j<n;j++)
		{
			if (ipiv[j] != 1)
				for (k=0;k<n;k++) 
				{
					if (ipiv[k] == 0) 
					{
						if (fabs(src[j][k]) >= big) 
						{
							big=(T)fabs(src[j][k]);
							irow=j;
							icol=k;
						}
					} 
					else if (ipiv[k] > 1)
						VIP_ASSERT(0, "vipGaussJ", "Singular Matrix-1");
				}
		}

		++(ipiv[icol]);
		if (irow != icol) 
		{
			for (l=0;l<n;l++) VIP_TEMP_SWAP(src[irow][l],src[icol][l])
			for (l=0;l<m;l++) VIP_TEMP_SWAP(buf[irow][l],buf[icol][l])
		}

		indxr[i]=irow;
		indxc[i]=icol;

		if (src[icol][icol] == 0.0)
			VIP_ASSERT(0, "vipGaussJ", "Singular Matrix-2");

		pivinv=T(1.0)/src[icol][icol];
		src[icol][icol]=1.0;

		for (l=0;l<n;l++) src[icol][l] *= pivinv;
		for (l=0;l<m;l++) buf[icol][l] *= pivinv;

		for (ll=0;ll<n;ll++)
		{
			if (ll != icol) 
			{
				dum=src[ll][icol];
				src[ll][icol]=0.0;
				for (l=0;l<n;l++) src[ll][l] -= src[icol][l]*dum;
				for (l=0;l<m;l++) buf[ll][l] -= buf[icol][l]*dum;
			}
		}
	}

	for (l=n-1;l>=0;l--) 
	{
		if (indxr[l] != indxc[l])
		{
			for (k=0;k<n;k++)
				VIP_TEMP_SWAP(src[k][indxr[l]],src[k][indxc[l]]);
		}
	}

	delete [] ipiv ;
	delete [] indxr;
	delete [] indxc;
}

template <class T>
void vipGaussJ(
	T *src, int srcStep, int n, 
	T *buf, int bufStep, int m)
{
	int i,icol,irow,j,k,l,ll;
	T big,dum,pivinv,temp;

	int *indxc = new int[n];
	int *indxr = new int[n];
	int *ipiv  = new int[n];

	T *srcT;
	T *bufT;

	for (j=0;j<n;j++) ipiv[j]=0;
	for (i=0;i<n;i++) 
	{
		big=0.0;
		srcT = src;
		for (j=0;j<n;j++, (char*&)srcT  += srcStep)
		{
			if (ipiv[j] != 1)
				for (k=0;k<n;k++) 
				{
					if (ipiv[k] == 0) 
					{
						if (fabs(srcT[k]) >= big) 
						{
							big=(T)fabs(srcT[k]);
							irow=j;
							icol=k;
						}
					} 
					else if (ipiv[k] > 1)
						VIP_ASSERT(0, "gaussj", "gaussj: Singular Matrix-1");
				}
		}

		++(ipiv[icol]);
		
		T *srcR = (T*)((char*)src + srcStep*irow);
		T *bufR = (T*)((char*)buf + bufStep*irow);
		T *srcC = (T*)((char*)src + srcStep*icol);
		T *bufC = (T*)((char*)buf + bufStep*icol);

		if (irow != icol) 
		{
			for (l=0;l<n;l++) VIP_TEMP_SWAP(srcR[l], srcC[l])
			for (l=0;l<m;l++) VIP_TEMP_SWAP(bufR[l], bufC[l])
		}

		indxr[i]=irow;
		indxc[i]=icol;

		if (srcC[icol] == 0.0)
			VIP_ASSERT(0, "vipGaussJ", "Singular Matrix-2");

		pivinv=T(1.0)/srcC[icol];
		srcC[icol]=1.0;

		for (l=0;l<n;l++) srcC[l] *= pivinv;
		for (l=0;l<m;l++) bufC[l] *= pivinv;

		srcT = src;
		bufT = buf;
		for (ll=0 ; ll<n ;ll++, (char*&)srcT  += srcStep, (char*&)bufT  += bufStep)
		{
			if (ll != icol) 
			{
				dum=srcT[icol];
				srcT[icol]=0.0;
				for (l=0;l<n;l++) srcT[l] -= srcC[l]*dum;
				for (l=0;l<m;l++) bufT[l] -= bufC[l]*dum;
			}
		}
	}

	for (l=n-1;l>=0;l--) 
	{
		if (indxr[l] != indxc[l])
		{
			srcT = src;
			for (k=0;k<n;k++, (char*&)srcT  += srcStep)
				VIP_TEMP_SWAP(srcT[indxr[l]],srcT[indxc[l]]);
		}
	}

	delete [] ipiv ;
	delete [] indxr;
	delete [] indxc;
}
#undef VIP_TEMP_SWAP

#define _VIP_INTERPOLATE_1(srcType, dstType, src, dst, srcStep, srcXPos, srcYPos, dstPos, a, b, shift) \
{                                                                                                     \
	const srcType *ptr = VIP_PTR(T1, src, srcStep, srcXPos, srcYPos);                                 \
	int p0 = (ptr[      0]<<shift) + a * (ptr[1          ] - ptr[0      ]);                           \
	int p1 = (ptr[srcStep]<<shift) + a * (ptr[srcStep + 1] - ptr[srcStep]);                           \
	dst[dstPos] = (dstType) ( (p0 +   ((b * (p1 - p0)) >> shift)   ) >> shift);                       \
}

#define _VIP_INTERPOLATE_N(srcType, dstType, src, dst, srcStep, srcXPos, srcYPos, dstPos, a, b, shift, elCount) \
{                                                                                                     \
	const srcType *ptr = VIP_PTR(T1, src, srcStep, srcXPos, srcYPos);                                 \
	int p0 = (ptr[      0]<<shift) + a * (ptr[elCount          ] - ptr[0      ]);                     \
	int p1 = (ptr[srcStep]<<shift) + a * (ptr[srcStep + elCount] - ptr[srcStep]);                     \
	dst[dstPos] = (dstType) ( (p0 +   ((b * (p1 - p0)) >> shift)   ) >> shift);                       \
}

template <class T1, class T2>
void vipTransformAffine(
	const T1 *src, int srcStep, int srcWidth, int srcHeight,
	      T2 *dst, int dstStep, int dstWidth, int dstHeight,
	const float *mat2x3, int elSize = 1, T2 fillval = (T2)0)
{             
	const int   shift = 10;
	const int   mask  = 0xffffffff>>(32-shift);
	const float s     = (float)(1<<shift);

	int A11 = int(mat2x3[0] * s), A12 = int(mat2x3[1] * s), A13 = int(mat2x3[2] * s);
	int A21 = int(mat2x3[3] * s), A22 = int(mat2x3[4] * s), A23 = int(mat2x3[5] * s);

	int left = -(dstWidth  >> 1), right  = dstWidth  + left - 1;
	int top  = -(dstHeight >> 1), bottom = dstHeight + top  - 1;
	
	int t_xs = left *A11 + top   *A12 + A13;
	int t_ys = left *A21 + top   *A22 + A23;
	int t_xe = right*A11 + top   *A12 + A13;
	int t_ye = right*A21 + top   *A22 + A23;
	int b_xs = left *A11 + bottom*A12 + A13;
	int b_ys = left *A21 + bottom*A22 + A23;
	int b_xe = right*A11 + bottom*A12 + A13;
	int b_ye = right*A21 + bottom*A22 + A23;

	if (
		(unsigned)(t_xs>>shift) < (unsigned)(srcWidth  - 1) &&
		(unsigned)(t_ys>>shift) < (unsigned)(srcHeight - 1) &&
		(unsigned)(t_xe>>shift) < (unsigned)(srcWidth  - 1) &&
		(unsigned)(t_ye>>shift) < (unsigned)(srcHeight - 1) &&
		(unsigned)(b_xs>>shift) < (unsigned)(srcWidth  - 1) &&
		(unsigned)(b_ys>>shift) < (unsigned)(srcHeight - 1) &&
		(unsigned)(b_xe>>shift) < (unsigned)(srcWidth  - 1) &&
		(unsigned)(b_ye>>shift) < (unsigned)(srcHeight - 1)
		)
	{
		if (elSize == 1)
		{
			dst -= left;
			for ( int y = top; y <= bottom; y++, (char*&)dst  += dstStep) 
			{         
				int xs = left *A11 + y*A12 + A13;
				int ys = left *A21 + y*A22 + A23;

				for (int x = left; x <= right; x++, xs += A11, ys += A21)
				{ 
					int ixs = xs>>shift;
					int iys = ys>>shift;

					int a = xs & mask;
					int b = ys & mask;
					
					_VIP_INTERPOLATE_1(T1, T2, src, dst, srcStep, ixs, iys, x, a, b, shift);
				}
			}
		}
		else
		{
			dst -= left*elSize;
			for ( int y = top; y <= bottom; y++, (char*&)dst  += dstStep) 
			{         
				int xs = left *A11 + y*A12 + A13;
				int ys = left *A21 + y*A22 + A23;

				for (int x = left; x <= right; x++, xs += A11, ys += A21)
				{ 
					int ixs = xs>>shift;
					int iys = ys>>shift;

					int a = xs & mask;
					int b = ys & mask;

					int srcX = ixs*elSize;
					int dstX = x  *elSize;
					for (int i = 0; i < elSize ; i++)
					{
						_VIP_INTERPOLATE_N(T1, T2, src, dst, srcStep, srcX + i, iys, dstX + i, a, b, shift, elSize);
					}
				}
			}
		}
	}
	else
	{
		if (elSize == 1)
		{
			dst -= left;
			for ( int y = top; y <= bottom; y++, (char*&)dst  += dstStep) 
			{         
				int xs = left *A11 + y*A12 + A13;
				int ys = left *A21 + y*A22 + A23;
				int xe = right*A11 + y*A12 + A13;
				int ye = right*A21 + y*A22 + A23;

				if (
					(unsigned)(xs>>shift) < (unsigned)(srcWidth  - 1) &&
					(unsigned)(ys>>shift) < (unsigned)(srcHeight - 1) &&
					(unsigned)(xe>>shift) < (unsigned)(srcWidth  - 1) &&
					(unsigned)(ye>>shift) < (unsigned)(srcHeight - 1)    )
				{
					for (int x = left; x <= right; x++, xs += A11, ys += A21)
					{ 
						int ixs = xs>>shift;
						int iys = ys>>shift;

						int a = xs & 0x000003ff;
						int b = ys & 0x000003ff;
						
						_VIP_INTERPOLATE_1(T1, T2, src, dst, srcStep, ixs, iys, x, a, b, shift);
					} 
				}
				else
				{                                                         
					for (int x = left; x <= right; x++, xs += A11, ys += A21)
					{                                                     
						int ixs = xs>>shift;
						int iys = ys>>shift;
						
						int a = xs & 0x000003ff;
						int b = ys & 0x000003ff;
						
						if( (unsigned)ixs < (unsigned)(srcWidth - 1) && (unsigned)iys < (unsigned)(srcHeight - 1) )
						{
							_VIP_INTERPOLATE_1(T1, T2, src, dst, srcStep, ixs, iys, x, a, b, shift);
						}                                                 
						else
							dst[x] = fillval;
					}                                                     
				} 
			}
		}
		else
		{
			dst -= left*elSize;
			for ( int y = top; y <= bottom; y++, (char*&)dst  += dstStep) 
			{         
				int xs = left *A11 + y*A12 + A13;
				int ys = left *A21 + y*A22 + A23;
				int xe = right*A11 + y*A12 + A13;
				int ye = right*A21 + y*A22 + A23;

				if (
					(unsigned)(xs>>shift) < (unsigned)(srcWidth  - 1) &&
					(unsigned)(ys>>shift) < (unsigned)(srcHeight - 1) &&
					(unsigned)(xe>>shift) < (unsigned)(srcWidth  - 1) &&
					(unsigned)(ye>>shift) < (unsigned)(srcHeight - 1)    )
				{
					for (int x = left; x <= right; x++, xs += A11, ys += A21)
					{ 
						int ixs = xs>>shift;
						int iys = ys>>shift;

						int a = xs & 0x000003ff;
						int b = ys & 0x000003ff;
						
						int srcX = ixs*elSize;
						int dstX = x  *elSize;
						for (int i = 0; i < elSize ; i++)
						{
							_VIP_INTERPOLATE_N(T1, T2, src, dst, srcStep, srcX + i, iys, dstX + i, a, b, shift, elSize);
						}
					} 
				}
				else
				{                                                         
					for (int x = left; x <= right; x++, xs += A11, ys += A21)
					{                                                     
						int ixs = xs>>shift;
						int iys = ys>>shift;
						
						int a = xs & 0x000003ff;
						int b = ys & 0x000003ff;
						
						if( (unsigned)ixs < (unsigned)(srcWidth - 1) && (unsigned)iys < (unsigned)(srcHeight - 1) )
						{
							int srcX = ixs*elSize;
							int dstX = x  *elSize;
							for (int i = 0; i < elSize ; i++)
							{
								_VIP_INTERPOLATE_N(T1, T2, src, dst, srcStep, srcX + i, iys, dstX + i, a, b, shift, elSize);
							}
						}
						/*
						else
						{
							int dstX = x  *elSize;
							for (int i = 0; i < elSize ; i++)
								dst[dstX + i] = fillval;
						}
						*/
					}                                                     
				} 
			}
		}
	}
}

template <class T1, class T2>
void vipTransformAffine(
	const Vip2DPtr <T1> &src, int srcWidth, int srcHeight,
	      Vip2DPtr <T2> &dst, int dstWidth, int dstHeight,
	const float *mat2x3, int elSize = 1, T2 fillval = (T2)0)
{
	vipTransformAffine(
		src.data, src.width_step, srcWidth, srcHeight,
		dst.data, dst.width_step, dstWidth, dstHeight, mat2x3, elSize, fillval);
}

template <class T1, class T2>
void vipTransformAffine(
	const Vip2DInfo <T1> &src, Vip2DInfo <T2> &dst,
	const float *mat2x3, int elSize = 1, T2 fillval = (T2)0)
{
	vipTransformAffine(
		src, src.width, src.height,
		dst, dst.width, dst.height, mat2x3, elSize, fillval);
}

template <class T1, class T2>
void vipRotate(
	const T1 *src, int srcStep, int srcWidth, int srcHeight, 
	      T2 *dst, int dstStep, int dstWidth, int dstHeight, 
	float degAngle, float cx, float cy, int elSize = 1)
{
	float radAngle = float(-degAngle/180.0 * (3.14159265358979324));
	float cc = (float)cos(radAngle);
	float ss = (float)sin(radAngle);

	float A2x3[2*3];
	A2x3[0] = cc; A2x3[1] = -ss; A2x3[2] = cx;
	A2x3[3] = ss; A2x3[4] =  cc; A2x3[5] = cy;

	vipTransformAffine(
		src, srcStep, srcWidth, srcHeight,
		dst, dstStep, dstWidth, dstHeight, A2x3, elSize);
}

template <class T1, class T2>
void vipRotate(
	const Vip2DPtr <T1> &src, int srcWidth, int srcHeight, 
	      Vip2DPtr <T2> &dst, int dstWidth, int dstHeight, 
	float degAngle, float cx, float cy, int elSize = 1)
{
	vipRotate(
		src.data, src.width_step, srcWidth, srcHeight,
		dst.data, dst.width_step, dstWidth, dstHeight, degAngle, cx, cy, elSize);
}

template <class T1, class T2>
void vipRotate(
	const Vip2DInfo <T1> &src, Vip2DInfo <T2> &dst,
	float degAngle, float cx, float cy, int elSize = 1)
{
	vipRotate(
		src, src.width, src.height,
		dst, dst.width, dst.height, degAngle, cx, cy, elSize);
}

template <class T1, class T2>
void vipResize(
	const T1 *src, int srcStep, int srcWidth, int srcHeight, 
	      T2 *dst, int dstStep, int dstWidth, int dstHeight, 
	int elSize = 1)
{
	float xRatio = float(srcWidth )/float(dstWidth );
	float yRatio = float(srcHeight)/float(dstHeight);
	float A2x3[2*3];
	A2x3[0] = xRatio; A2x3[1] =      0; A2x3[2] = float(srcWidth )/2.0f;
	A2x3[3] =      0; A2x3[4] = yRatio; A2x3[5] = float(srcHeight)/2.0f;

	vipTransformAffine(
		src, srcStep, srcWidth, srcHeight,
		dst, dstStep, dstWidth, dstHeight, A2x3, 3);
}

VIPLIB_API void vipGrayToRGB(const unsigned char *src, int srcStep, unsigned char *dst, int dstStep, int width, int height);
VIPLIB_API void vipGrayToRGB(const Vip2DPtr <unsigned char> src, Vip2DPtr <unsigned char> &dst, int width, int height);
VIPLIB_API void vipRGBToGray(const unsigned char *src, int srcStep, unsigned char *dst, int dstStep, int width, int height);


#endif // end _VIP_CORE_FUNC_
