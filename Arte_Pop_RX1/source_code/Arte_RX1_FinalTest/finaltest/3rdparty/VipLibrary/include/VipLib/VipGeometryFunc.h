// VipGeometryTools.h: interface for the CVipGeometryTools class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_VIPGeometryTools_H__2697E3DE_72D1_4338_805C_FE48B1188BC0__INCLUDED_)
#define AFX_VIPGeometryTools_H__2697E3DE_72D1_4338_805C_FE48B1188BC0__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "ViplBase.h"

#include "TemplPoint.h"
#include "PointFunction.h"

#if !defined(_WIN32_WCE) && !defined(_LINUX)

#include "VipImgTempl.h"
#include "VipImgFunc.h"
#include "Matrix.h"
#include "MatrixFunction.h"

VIPLIB_API f2DPoint    fMat2x1To2DPoint  (const FloatMatrix &Mat);
VIPLIB_API f2DPoint    fMat3x1To2DPoint  (const FloatMatrix &Mat);
VIPLIB_API f3DPoint    fMat3x1To3DPoint  (const FloatMatrix &Mat);
VIPLIB_API f3DPoint    fMat4x1To3DPoint  (const FloatMatrix &Mat);

VIPLIB_API FloatMatrix f2DPointToMat2x1  (const f2DPoint    &pt );
VIPLIB_API FloatMatrix f2DPointToMat3x1  (const f2DPoint    &pt );
VIPLIB_API FloatMatrix f3DPointToMat3x1  (const f3DPoint    &pt );
VIPLIB_API FloatMatrix f3DPointToMat4x1  (const f3DPoint    &pt );

VIPLIB_API void      MakeTransferTable(const FloatMatrix &HI, const f2DPoint srcCenter, const f2DPoint dstCenter, const iPoint dstSize, CRemapLUT &table);
VIPLIB_API CRemapLUT MakeTransferTable(const FloatMatrix &HI, const f2DPoint srcCenter, const f2DPoint dstCenter, const iPoint dstSize);

VIPLIB_API ByteImage TransferImage   (const FloatMatrix &H, const ByteImage &image, const f2DPoint srcCenter, const f2DPoint dstCenter, const iPoint dstSize, int interplolate = IPL_INTER_LINEAR);
VIPLIB_API ByteImage TransferImageInv(const FloatMatrix &H, const ByteImage &image, const f2DPoint srcCenter, const f2DPoint dstCenter, const iPoint dstSize, int interplolate = IPL_INTER_LINEAR);

VIPLIB_API ByteImage TransferImage   (const FloatMatrix &H, const ByteImage &image);
VIPLIB_API ByteImage TransferImageInv(const FloatMatrix &H, const ByteImage &image);
#endif

VIPLIB_API f2DPoint      vipTransferPoint2x2(const float *H, const f2DPoint      &pt);
VIPLIB_API f2DPoint      vipTransferPoint2x3(const float *H, const f2DPoint      &pt);
VIPLIB_API f2DPoint      vipTransferPoint3x3(const float *H, const f2DPoint      &pt);
VIPLIB_API f2DPointArray vipTransferPoint3x3(const float *H, const f2DPointArray &pt);
VIPLIB_API f3DPoint      vipTransferPoint3x3(const float *H, const f3DPoint      &pt);
VIPLIB_API f3DPoint      vipTransferPoint4x4(const float *H, const f3DPoint      &pt);
VIPLIB_API f2DPoint      vipTransferPoint3x4(const float *P, const f3DPoint      &pt);

#if !defined(_WIN32_WCE) && !defined(_LINUX)
VIPLIB_API f2DPoint      vipTransferPoint2x2(const FloatMatrix &H, const f2DPoint      &pt);
VIPLIB_API f2DPoint      vipTransferPoint2x3(const FloatMatrix &H, const f2DPoint      &pt);
VIPLIB_API f2DPoint      vipTransferPoint3x3(const FloatMatrix &H, const f2DPoint      &pt);
VIPLIB_API f2DPointArray vipTransferPoint3x3(const FloatMatrix &H, const f2DPointArray &pt);
VIPLIB_API f3DPoint      vipTransferPoint3x3(const FloatMatrix &H, const f3DPoint      &pt);
VIPLIB_API f3DPoint      vipTransferPoint4x4(const FloatMatrix &H, const f3DPoint      &pt);
VIPLIB_API f2DPoint      vipTransferPoint3x4(const FloatMatrix &P, const f3DPoint      &pt);
#endif

#endif // !defined(AFX_VIPGeometryTools_H__2697E3DE_72D1_4338_805C_FE48B1188BC0__INCLUDED_)
