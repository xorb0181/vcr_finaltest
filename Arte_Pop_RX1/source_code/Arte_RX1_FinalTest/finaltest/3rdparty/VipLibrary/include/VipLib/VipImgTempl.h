#if !defined(IPL_TEMPLATE_H)
#define IPL_TEMPLATE_H

/*------------------------------------------------------------------*/
// VipImgTempl.h: interface for the CVipImgTempl class.
/*------------------------------------------------------------------*/

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "VipImgBase.h"
#include "VipCore.h"
#include "VipArray.h"

/// #define DefImgTempl		template <class T, eIplBase_Type TM_TYPE>
#define DefImgTempl	template <class T, eIplBase_Type TM_TYPE>

/// #define TemplImage		CVipImgTempl <T, TM_TYPE>
#define TemplImage		CVipImgTempl   <T, TM_TYPE>
#define TemplImageArray	CVipArray < CVipImgTempl < T, TM_TYPE > >

DefImgTempl class CVipImgTempl;

/*------------------------------------------------------------------*/
///@name Type define
/*------------------------------------------------------------------*/
//@{

	/// Byte  Type Image
	typedef CVipImgTempl <BYTE  , ipl_BYTE   > ByteImage  ;
	/// Char Type Image
	typedef CVipImgTempl <char  , ipl_CHAR   > CharImage  ;

	/// Unsigned Short Type Image
	typedef CVipImgTempl <USHORT, ipl_USHORT > UShortImage;
	/// Short Type Image
	typedef CVipImgTempl <short , ipl_SHORT  > ShortImage ;

	/// Int   Type Image
	typedef CVipImgTempl <int   , ipl_INT    > IntImage   ;
	/// Float Type Image
	typedef CVipImgTempl <float , ipl_FLOAT  > FloatImage ;

	/// Float Type Convolution Kernel
	typedef CVipImgTempl <float , ipl_k_FLOAT> FloatConvKernel;

	/// Int   Type Convolution Kernel
	typedef CVipImgTempl <int   , ipl_k_INT  >   IntConvKernel;

	///@memo Classes of Image Array
	//@{
		/// ByteImage Array
		typedef CVipArray   <ByteImage > ByteImageArray ;
		/// IntImage Array
		typedef CVipArray   <IntImage  > IntImageArray  ;
		/// FloatImage Array
		typedef CVipArray   <FloatImage> FloatImageArray;
	//@}
//@}

/*------------------------------------------------------------------*/
// Macro Function
/*------------------------------------------------------------------*/
#define CVRT_IMG_P(pointer, x, y, c, cSize) pointer[y][x*cSize + c]

#define DEFINE_IMG1_IMG2(OPERATOR, OPERATION) 							\
DefImgTempl																\
TemplImage & OPERATOR (TemplImage &Op1, const TemplImage &Op2)			\
{ 																		\
	CHECK_DIM_IMG(Op1, Op2, #OPERATOR);									\
	IPL_LOOP(true, 														\
		Op1.GetIplImage(),												\
		Op2.GetIplImageConst(),											\
		0  , T, OPERATION);												\
	return Op1;															\
};

#define DEFINE_IMG1_SCALAR(OPERATOR, OPERATION, SCALAR_TYPE) 			\
DefImgTempl																\
TemplImage & OPERATOR (TemplImage &Op1, const SCALAR_TYPE Scalar) 		\
{ 																		\
	CHECK_NULL(Op1, #OPERATOR);											\
	IPL_LOOP(true, 														\
		Op1.GetIplImage(),												\
		0,																\
		0  , T, OPERATION);												\
	return  Op1;	 													\
};

// inline condition for speed up
#define CHECK_CLONE {if (m_pData->nRef > 1) _CopyBeforeWrite();}

/*------------------------------------------------------------------*/
/** Template Class of CVipImgBase
	This Class Inherite The CVipImgBase for Template Usage				*/
/*------------------------------------------------------------------*/

DefImgTempl 
class CVipImgTempl : public CVipImgBase  
{
public:
	/*--------------------------------------------------------------*/
	///@name	Construction/Destruction
	/*--------------------------------------------------------------*/
	//@{
	///
	CVipImgTempl()                              : CVipImgBase(TM_TYPE) {};
//	CVipImgTempl(const TemplImage      &source) : CVipImgBase(TM_TYPE) { _Copy(source); };
	CVipImgTempl(const   ByteImage     &source) : CVipImgBase(TM_TYPE) { _Copy(source); };
	CVipImgTempl(const    IntImage     &source) : CVipImgBase(TM_TYPE) { _Copy(source); };
	CVipImgTempl(const  FloatImage     &source) : CVipImgBase(TM_TYPE) { _Copy(source); };
	CVipImgTempl(const   CharImage     &source) : CVipImgBase(TM_TYPE) { _Copy(source); };
	CVipImgTempl(const  ShortImage     &source) : CVipImgBase(TM_TYPE) { _Copy(source); };
	CVipImgTempl(const UShortImage     &source) : CVipImgBase(TM_TYPE) { _Copy(source); };

	CVipImgTempl(const FloatConvKernel &source) : CVipImgBase(TM_TYPE) { _Copy(source); };
	CVipImgTempl(const   IntConvKernel &source) : CVipImgBase(TM_TYPE) { _Copy(source); };

	///
	CVipImgTempl(int XSize, int YSize, int channel = 1) : CVipImgBase(XSize, YSize, channel, TM_TYPE) {};
	///
	CVipImgTempl(const T *src, int step, int XSize, int YSize, int channel = 1) : CVipImgBase(XSize, YSize, channel, TM_TYPE)
	{
		vipCopy(src, step, GetBuffer(), GetWidthStep(), XSize*channel, YSize);
	};
	///
	explicit CVipImgTempl(const char *string) : CVipImgBase(TM_TYPE) { CVipImgBase::Allocate(string);	};
	virtual ~CVipImgTempl() {};
	//@}

public:
	/*--------------------------------------------------------------*/
	///@name	Image Data Acess
	/*--------------------------------------------------------------*/
	//@{
	///
	      T ** Get2DBuffer     ()       { CHECK_CLONE; return (      T**)m_ppVoid; }
	///
	const T ** Get2DBufferConst() const {              return (const T**)m_ppVoid; }
	/// Get 1D Buffer
	      T  * GetBuffer       ()       { return (      T*) CVipImgBase::GetBuffer     (); };
	/// Get 1D Buffer Const
	const T  * GetBufferConst  () const { return (const T*) CVipImgBase::GetBufferConst(); };
	
	      T  * GetBuffer       (int x, int y)       { return (      T*) CVipImgBase::GetBuffer     (x, y); };
	/// Get 1D Buffer Const
	const T  * GetBufferConst  (int x, int y) const { return (const T*) CVipImgBase::GetBufferConst(x, y); };

	      T  * roiBuffer      ()       { return (      T*) CVipImgBase::roiBuffer     (); };
	const T  * roiBufferConst () const { return (const T*) CVipImgBase::roiBufferConst(); };


	///
	TemplImage GetChannel(int channel) const
	{
		VIP_ASSERT(channel < GetCSize(), "GetChannel", "Wrong Channel Number");
		TemplImage Output(GetXSize(), GetYSize());

		const T ** ppSrc =        Get2DBufferConst();
		      T ** ppDst = Output.Get2DBuffer();
		int cSize = GetCSize();

		for (int y = 0 ; y < GetYSize() ; y++)
		{
			const T * pSrc = ppSrc[y];
			      T * pDst = ppDst[y];
			for (int x = 0 ; x < GetXSize() ; x++)
				pDst[x] = pSrc[x * cSize + channel];
		}
		return Output;
	}

	///
	void PutChannel(const TemplImage &src, int channel)
	{
		VIP_ASSERT(channel < GetCSize(), "GetChannel", "Wrong Channel Number");
		VIP_ASSERT(src.GetCSize() == 1, "GetChannel", "Channel Size of Input Image has to be 1");

		const T ** ppSrc = src.Get2DBufferConst();
		      T ** ppDst =     Get2DBuffer();
		int cSize = GetCSize();

		int srcXSize = src.GetXSize();
		int srcYSize = src.GetYSize();

		for (int y = 0 ; y < srcYSize ; y++)
		{
			const T * pSrc = ppSrc[y];
			      T * pDst = ppDst[y] + channel;
			for (int x = 0, dx = 0 ; x < srcXSize ; x++, dx+=cSize)
				pDst[dx] = pSrc[x];
		}
	};

	///
	operator       T   **() { CHECK_CLONE; return ((T**)m_ppVoid); };

	operator       void *() { CHECK_CLONE; return m_pData->pIplImage; };
	operator const void *() const {        return m_pData->pIplImage; };

	///
	inline const T & Read(int x, int y, int channel) const
	{
		CHECK_RANGE_IMG(x, y, channel);
		return ((T**)m_ppVoid) [ y ][ x * m_nCSize + channel ]; 
	}

	///
	inline const T & Read(int x, int y) const
	{
		CHECK_RANGE_IMG(x, y, 0);
		return ((T**)m_ppVoid) [ y ][ x ]; 
	}
	///
	inline T & operator () (int x, int y, int channel)
	{
		CHECK_RANGE_IMG(x, y, channel);
		CHECK_CLONE;
		return ((T**)m_ppVoid) [ y ][ x * m_nCSize + channel ]; 
	}

	///
	inline T & operator () (int x, int y)
	{
		CHECK_RANGE_IMG(x, y, 0);
		CHECK_CLONE;
		return ((T**)m_ppVoid) [ y ][ x ];
	}

	///
	inline T * operator [] (int y)
	{
		CHECK_CLONE;
		return ((T**)m_ppVoid)[y];
	}

	///
	TemplImage Crop(int xPos, int yPos, int width, int height) const
	{
		CHECK_NULL((*this), "Crop");
		TemplImage dst(width, height, GetCSize());

		int srcWStep   =     GetWidthStep();
		int dstWStep   = dst.GetWidthStep();
		int cSize      = GetCSize();

		int srcXLimit  = __min(xPos + width , GetXSize());
		int srcYLimit  = __min(yPos + height, GetYSize());

		int srcXPos    = (xPos > 0) ? xPos : 0    ;
		int srcYPos    = (yPos > 0) ? yPos : 0    ;
		int dstXPos    = (xPos > 0) ? 0    : -xPos;
		int dstYPos    = (yPos > 0) ? 0    : -yPos;

		int validWidth = (srcXLimit - srcXPos) * GetSizeofPixel() * cSize;
		if(validWidth > 0)
		{
			const char * pSrc = (const char*)    GetBufferConst() + srcYPos * srcWStep + srcXPos * cSize * GetSizeofPixel();
				  char * pDst = (      char*)dst.GetBuffer     () + dstYPos * dstWStep + dstXPos * cSize * GetSizeofPixel();

			for (int y = srcYPos ; y < srcYLimit ; y++)
			{
				memcpy(pDst, pSrc, validWidth);
				pSrc += srcWStep;
				pDst += dstWStep;
			}
		}
		
		return dst;
	}

	///
	void Paste(const TemplImage &src, int xPos, int yPos)
	{
		VIP_ASSERT(GetCSize() == src.GetCSize(), "Paste", "Channel Size have to be same");
		VIP_ASSERT(xPos < GetXSize() && yPos < GetYSize(), "Paste", "xPos & yPos are out out range");
		CHECK_CLONE;

		IplROI srcROI = src.GetROI();

		int dstXLimit     = __min(xPos + srcROI.width , GetXSize());
		int dstYLimit     = __min(yPos + srcROI.height, GetYSize());

		int dstXPos       = (xPos > 0) ? xPos           : 0    ;
		int dstYPos       = (yPos > 0) ? yPos           : 0    ;
		int srcXPos       = (xPos > 0) ? srcROI.xOffset : -xPos;
		int srcYPos       = (yPos > 0) ? srcROI.yOffset : -yPos;

		int srcWStep      = src.GetWidthStep();
		int dstWStep      = GetWidthStep();
		int cSize         = GetCSize();

		int validWidth    = (dstXLimit - dstXPos) * GetSizeofPixel() * cSize;

		const char * pSrc = (char*)&src.Read(srcXPos, srcYPos, 0);
		      char * pDst = (char*)&(*this) (dstXPos, dstYPos, 0);

		for (int y = dstYPos ; y < dstYLimit ; y++)
		{
			memcpy(pDst, pSrc, validWidth);
			pSrc += srcWStep;
			pDst += dstWStep;
		}
	}
	//@}
	
//	int XPos   () const { return roiXOffset(); }
//	int YPos   () const { return roiYOffset(); }
//	int	Width  () const { return roiWidth  (); }
//	int	Height () const { return roiHeight (); }

	/*--------------------------------------------------------------*/
	///@name	Copy Operator
	/*--------------------------------------------------------------*/
	//@{
	///
//	TemplImage & operator = (const TemplImage &source) { _Copy(source); return *this; }
	TemplImage & operator = (const   ByteImage     &source) { _Copy(source); return *this; }
	TemplImage & operator = (const  FloatImage     &source) { _Copy(source); return *this; }
	TemplImage & operator = (const    IntImage     &source) { _Copy(source); return *this; }
	TemplImage & operator = (const   CharImage     &source) { _Copy(source); return *this; }
	TemplImage & operator = (const UShortImage     &source) { _Copy(source); return *this; }
	TemplImage & operator = (const  ShortImage     &source) { _Copy(source); return *this; }


	TemplImage & operator = (const FloatConvKernel &source) { _Copy(source); return *this; }
	TemplImage & operator = (const   IntConvKernel &source) { _Copy(source); return *this; }

	//@}

	/*--------------------------------------------------------------*/
	///@name Geometry Functions
	/*--------------------------------------------------------------*/
	//@{

	///
    TemplImage Transpose() const
	{
		CHECK_NULL( (*this), "Transpose");
		TemplImage out(GetYSize(), GetXSize(), GetCSize());

		vipTranspose(
			(const T *)    GetBufferConst(),     GetWidthStep(), GetXSize(), GetYSize(),
			(      T *)out.GetBuffer     (), out.GetWidthStep(), GetCSize());

		return out;
	};

	/** Mirrors an image about a horizontal or vertical axis.

		@param		src			The source image
		@param		flipAxis	Specifies the axis to mirror the image.
								-  0 for the horizontal axis,
								-  1 for a vertical axis
								- -1 for both horizontal and vertical axis.
		@return		The resultant image
	*/
	TemplImage Mirror(int flipAxis) const
	{
		TemplImage Output(GetXSize(), GetYSize(), GetCSize());
		iplMirror(m_pData->pIplImage, Output, flipAxis);
		return Output;
	}

	//@}

	friend class CVipImgTempl <BYTE , ipl_BYTE  >;
	friend class CVipImgTempl <int  , ipl_INT   >;
	friend class CVipImgTempl <float, ipl_FLOAT >;
};
#undef CHECK_CLONE

/*------------------------------------------------------------------*/
// Global Operator
/*------------------------------------------------------------------*/

#define DEFINE_DYADIC(OPERATOR, FUNCTION)                               \
DefImgTempl                                                             \
TemplImage OPERATOR (const TemplImage &Op1, const TemplImage &Op2)      \
{                                                                       \
	CHECK_DIM_IMG(Op1, Op2, #OPERATOR);                                 \
	TemplImage Output(Op1.roiWidth(), Op1.roiHeight(), Op1.GetCSize()); \
	FUNCTION( (IplImage *)Op1   .GetIplImageConst(),                    \
			  (IplImage *)Op2   .GetIplImageConst(),                    \
			  (IplImage *)Output.GetIplImage     () );                  \
	return Output;                                                      \
}

#define DEFINE_MONADIC1(OPERATOR, FUNCTION, SCALAR_TYPE, FLAG)          \
DefImgTempl                                                             \
TemplImage OPERATOR (const TemplImage &Op1, const SCALAR_TYPE Scalar)   \
{                                                                       \
	CHECK_NULL(Op1, #OPERATOR);                                         \
	TemplImage Output(Op1.roiWidth(), Op1.roiHeight(), Op1.GetCSize()); \
	if(TM_TYPE == IPL_DEPTH_32F)                                        \
		FUNCTION##FP ((IplImage *)Op1.GetIplImageConst(),               \
						          Output, (float)Scalar FLAG );         \
	else                                                                \
		FUNCTION     ((IplImage *)Op1.GetIplImageConst(),               \
						          Output,   (int)Scalar FLAG );         \
	return Output;                                                      \
}

#define DEFINE_MONADIC2(OPERATOR, FUNCTION, SCALAR_TYPE, FLAG)			\
DefImgTempl																\
TemplImage OPERATOR (const SCALAR_TYPE Scalar, const TemplImage &Op1) 	\
{ 																		\
	CHECK_NULL(Op1, #OPERATOR);											\
	TemplImage Output(Op1.roiWidth(), Op1.roiHeight(), Op1.GetCSize()); \
	if(TM_TYPE == IPL_DEPTH_32F)										\
		FUNCTION##FP ((IplImage *)Op1.GetIplImageConst(),				\
						          Output, (float)Scalar FLAG );			\
	else																\
		FUNCTION     ((IplImage *)Op1.GetIplImageConst(),				\
						          Output,   (int)Scalar FLAG );			\
	return Output;														\
}

DefImgTempl
inline TemplImage operator - (TemplImage & src)
{
	TemplImage out = CloneBuffer(src);
	IPL_LOOP(true, src.GetIplImage(), 0, out.GetIplImage(), T, _IPL_SRC3_ = -_IPL_SRC1_ )
	return out;
};

#define MONADIC_NULL
#define MONADIC_FALSE , FALSE
#define MONADIC_TRUE  , TRUE

// Output = Image1 Operator Image2
DEFINE_DYADIC      (operator +, iplAdd     );
DEFINE_DYADIC      (operator -, iplSubtract);
DEFINE_DYADIC      (operator *, iplMultiply);
DEFINE_DYADIC      (operator /, vipDivide  );

// Output = Image1 Operator Scalar
DEFINE_MONADIC1    (operator +, iplAddS		, double, MONADIC_NULL );
DEFINE_MONADIC1    (operator -, iplSubtractS, double, MONADIC_FALSE);
DEFINE_MONADIC1    (operator *, iplMultiplyS, double, MONADIC_NULL );
DEFINE_MONADIC1    (operator /, vipDivideS  , double, MONADIC_FALSE);

DEFINE_MONADIC1    (operator +, iplAddS		, float , MONADIC_NULL );
DEFINE_MONADIC1    (operator -, iplSubtractS, float , MONADIC_FALSE);
DEFINE_MONADIC1    (operator *, iplMultiplyS, float , MONADIC_NULL );
DEFINE_MONADIC1    (operator /, vipDivideS  , float , MONADIC_FALSE);

DEFINE_MONADIC1    (operator +, iplAddS		, int   , MONADIC_NULL );
DEFINE_MONADIC1    (operator -, iplSubtractS, int   , MONADIC_FALSE);
DEFINE_MONADIC1    (operator *, iplMultiplyS, int   , MONADIC_NULL );
DEFINE_MONADIC1    (operator /, vipDivideS  , int   , MONADIC_FALSE);

DEFINE_MONADIC1    (operator +, iplAddS		, BYTE  , MONADIC_NULL );
DEFINE_MONADIC1    (operator -, iplSubtractS, BYTE  , MONADIC_FALSE);
DEFINE_MONADIC1    (operator *, iplMultiplyS, BYTE  , MONADIC_NULL );
DEFINE_MONADIC1    (operator /, vipDivideS  , BYTE  , MONADIC_FALSE);

// Output = Scalar Operator Image1
DEFINE_MONADIC2    (operator +, iplAddS		, double, MONADIC_NULL );
DEFINE_MONADIC2    (operator -, iplSubtractS, double, MONADIC_TRUE );
DEFINE_MONADIC2    (operator *, iplMultiplyS, double, MONADIC_NULL );
DEFINE_MONADIC2    (operator /, vipDivideS  , double, MONADIC_TRUE );

DEFINE_MONADIC2    (operator +, iplAddS		, float , MONADIC_NULL );
DEFINE_MONADIC2    (operator -, iplSubtractS, float , MONADIC_TRUE );
DEFINE_MONADIC2    (operator *, iplMultiplyS, float , MONADIC_NULL );
DEFINE_MONADIC2    (operator /, vipDivideS  , float , MONADIC_TRUE );

DEFINE_MONADIC2    (operator +, iplAddS		, int   , MONADIC_NULL );
DEFINE_MONADIC2    (operator -, iplSubtractS, int   , MONADIC_TRUE );
DEFINE_MONADIC2    (operator *, iplMultiplyS, int   , MONADIC_NULL );
DEFINE_MONADIC2    (operator /, vipDivideS  , int   , MONADIC_TRUE );

DEFINE_MONADIC2    (operator +, iplAddS		, BYTE  , MONADIC_NULL );
DEFINE_MONADIC2    (operator -, iplSubtractS, BYTE  , MONADIC_TRUE );
DEFINE_MONADIC2    (operator *, iplMultiplyS, BYTE  , MONADIC_NULL );
DEFINE_MONADIC2    (operator /, vipDivideS  , BYTE  , MONADIC_TRUE );

#undef MONADIC_NULL
#undef MONADIC_FALSE
#undef MONADIC_TRUE

// Image1 Operator Image2
DEFINE_IMG1_IMG2   (operator += , _IPL_SRC1_ += _IPL_SRC2_					);
DEFINE_IMG1_IMG2   (operator -= , _IPL_SRC1_ -= _IPL_SRC2_					);
DEFINE_IMG1_IMG2   (operator *= , _IPL_SRC1_ *= _IPL_SRC2_					);
DEFINE_IMG1_IMG2   (operator /= , VIP_ASSERT(_IPL_SRC2_ , "operator /=" , "Divide by Zero"); \
								  _IPL_SRC1_ /= _IPL_SRC2_					);

// Image1 Operator Scalar
DEFINE_IMG1_SCALAR (operator += , _IPL_SRC1_ += (T)_IPL_SCALAR_				, double);
DEFINE_IMG1_SCALAR (operator -= , _IPL_SRC1_ -= (T)_IPL_SCALAR_				, double);
DEFINE_IMG1_SCALAR (operator *= , _IPL_SRC1_ *= (T)_IPL_SCALAR_				, double);
DEFINE_IMG1_SCALAR (operator /= , VIP_ASSERT(_IPL_SCALAR_ , "operator /=" , "Divide by Zero"); \
								  _IPL_SRC1_ /= (T)_IPL_SCALAR_				, double);

DEFINE_IMG1_SCALAR (operator += , _IPL_SRC1_ += (T)_IPL_SCALAR_				, float );
DEFINE_IMG1_SCALAR (operator -= , _IPL_SRC1_ -= (T)_IPL_SCALAR_				, float );
DEFINE_IMG1_SCALAR (operator *= , _IPL_SRC1_ *= (T)_IPL_SCALAR_				, float );
DEFINE_IMG1_SCALAR (operator /= , VIP_ASSERT(_IPL_SCALAR_ , "operator /=" , "Divide by Zero"); \
								  _IPL_SRC1_ /= (T)_IPL_SCALAR_				, float );

DEFINE_IMG1_SCALAR (operator += , _IPL_SRC1_ += (T)_IPL_SCALAR_				, int   );
DEFINE_IMG1_SCALAR (operator -= , _IPL_SRC1_ -= (T)_IPL_SCALAR_				, int   );
DEFINE_IMG1_SCALAR (operator *= , _IPL_SRC1_ *= (T)_IPL_SCALAR_				, int   );
DEFINE_IMG1_SCALAR (operator /= , VIP_ASSERT(_IPL_SCALAR_ , "operator /=" , "Divide by Zero"); \
								  _IPL_SRC1_ /= (T)_IPL_SCALAR_				, int   );

DEFINE_IMG1_SCALAR (operator += , _IPL_SRC1_ += (T)_IPL_SCALAR_				, BYTE  );
DEFINE_IMG1_SCALAR (operator -= , _IPL_SRC1_ -= (T)_IPL_SCALAR_				, BYTE  );
DEFINE_IMG1_SCALAR (operator *= , _IPL_SRC1_ *= (T)_IPL_SCALAR_				, BYTE  );
DEFINE_IMG1_SCALAR (operator /= , VIP_ASSERT(_IPL_SCALAR_ , "operator /=" , "Divide by Zero"); \
								  _IPL_SRC1_ /= (T)_IPL_SCALAR_				, BYTE  );

class CRemapLUT
{
public:
	CRemapLUT() {}
	CRemapLUT(int XSize, int YSize) { Allocate(XSize, YSize); }

	int GetXSize() const { return m_tblX.GetXSize(); }
	int GetYSize() const { return m_tblX.GetYSize(); }

	int roiWidth () const { return m_tblX.roiWidth (); }
	int roiHeight() const { return m_tblX.roiHeight(); }
	
	bool IsEmpty() const { return m_tblX.IsEmpty();}

	void SetROI (int xPos, int yPos, int width, int height, int channel = 0)
	{
		m_tblX.SetROI(xPos, yPos, width, height, channel = 0);
		m_tblY.SetROI(xPos, yPos, width, height, channel = 0);
	}

	void SetROI (const IplROI &roi)
	{
		m_tblX.SetROI(roi);
		m_tblY.SetROI(roi);
	}

	void RemoveROI()
	{
		m_tblX.RemoveROI();
		m_tblY.RemoveROI();
	}

	void Allocate(int XSize, int YSize)
	{
		m_tblX.Allocate(XSize, YSize);
		m_tblY.Allocate(XSize, YSize);
	}

public:
	FloatImage m_tblX;
	FloatImage m_tblY;
};

std::ostream & operator << (std::ostream &ios, const CRemapLUT   &src);
std::istream & operator >> (std::istream &ios,       CRemapLUT   &src);

#endif // !defined(IPL_TEMPLATE_H)
