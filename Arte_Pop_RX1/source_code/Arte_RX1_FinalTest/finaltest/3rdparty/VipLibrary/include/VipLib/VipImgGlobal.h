#if !defined(_IMAGE_GLOBAL)
#define _IMAGE_GLOBAL

#include "VipCore.h"
#include "VipImgIplWrap.h"
/*------------------------------------------------------------------*/
/**					Base Data Type of CVipImgBase						*/
/*------------------------------------------------------------------*/

#define VIP_KERNEL_SIGN 0x40000000
#define VIP_KERNEL_MASK 0xBFFFFFFF

enum eIplBase_Type
{
	/// Byte  Type Image
	ipl_BYTE	= IPL_DEPTH_8U ,
	/// Char  Type Image
	ipl_CHAR    = IPL_DEPTH_8S ,
	/// Unsigned Short Type Image
	ipl_USHORT  = IPL_DEPTH_16U,
	/// Short Type Image
	ipl_SHORT   = IPL_DEPTH_16S,
	/// Int   Type Image
	ipl_INT		= IPL_DEPTH_32S,
	/// Float Type Image
	ipl_FLOAT	= IPL_DEPTH_32F,

	/// Int   Type Image
	ipl_k_INT	= VIP_KERNEL_SIGN | IPL_DEPTH_32S,
	/// Float Type Image
	ipl_k_FLOAT	= VIP_KERNEL_SIGN | IPL_DEPTH_32F
};

#define _IPL_SRC1_	A.buf.data[x]
#define _IPL_SRC2_	B.buf.data[x]
#define _IPL_SRC3_	C.buf.data[x]
#define _IPL_SCALAR_	Scalar

#define IPL_LOOP_BASE(bUseROI, src1, src2, src3, TA, TB, TC, OPERATION)						\
{																					\
	Vip2DInfo <TA> A = ExtractVip2DInfo <TA> ((IplImage*)src1, bUseROI);      \
	Vip2DInfo <TB> B = ExtractVip2DInfo <TB> ((IplImage*)src2, bUseROI);      \
	Vip2DInfo <TC> C = ExtractVip2DInfo <TC> ((IplImage*)src3, bUseROI);      \
                                                                                    \
	int width  = A.width ;                                                          \
	int height = A.height;                                                          \
                                                                                    \
	VIP_2D_FUNC_LOOP2_IMG3(                                                         \
		A.buf.data, A.buf.width_step,                                               \
		B.buf.data, B.buf.width_step,                                               \
		C.buf.data, C.buf.width_step, width, height, OPERATION; );                  \
}

#define IPL_LOOP(bUseROI, srcA, srcB, dstC, T, OPERATION)									\
		IPL_LOOP_BASE(bUseROI, srcA, srcB, dstC, T, T, T, OPERATION)

//--------------------------------------------------------------------
//					Global Ipl Function
//--------------------------------------------------------------------

IplImage *	AllocateIplImage(eIplBase_Type type, int XSize, int YSize, int channel, int bFillZero = 1, const char *colorModel = NULL, const char *channelSeq = NULL);
IplImage *	LoadIplImage(const char *filename, int iscolor = -1);
void		IplMemCopy(IplImage *dst, const IplImage *src);
bool		IplCompareSize(const IplImage *dst, const IplImage *src, bool bUseROI);

IplImage *	AllocateIplImageHeader(eIplBase_Type type, int XSize, int YSize, int channel, const char *colorModel = NULL, const char *channelSeq = NULL);
IplImage        MakeIplImageHeader(eIplBase_Type type, int XSize, int YSize, int channel, const char *colorModel = NULL, const char *channelSeq = NULL);

bool		WriteIplImage(IplImage *image, const char *filename);

template <class T>
Vip2DInfo <T> ExtractVip2DInfo(IplImage *src, bool bUseROI = true)
{
	Vip2DInfo <T> ret;
	if(src)
	{
//		ret.buf    = *((Vip2DPtr<T> *)src->imageData);
		ret.buf.data       = (T*)src->imageData;
		ret.buf.width_step =     src->widthStep;
		if(src->roi && bUseROI)
		{
			ret.buf    = vip2DPtr(ret.buf, src->roi->xOffset, src->roi->yOffset);
			ret.width  = src->roi->width * src->nChannels;
			ret.height = src->roi->height;
		}
		else
		{
			ret.width  = src->width * src->nChannels;
			ret.height = src->height;
		}
	}
	else
		memset(&ret, 0, sizeof(ret));
	return ret;
}

#endif // !defined(_IMAGE_GLOBAL)
