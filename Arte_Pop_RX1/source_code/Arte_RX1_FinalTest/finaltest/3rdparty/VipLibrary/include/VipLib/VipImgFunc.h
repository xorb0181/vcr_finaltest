#if !defined(_IMAGE_FUNCTIONS_H_)
#define _IMAGE_FUNCTIONS_H_

/** @file VipImgFunc.h */

#include "ViplBase.h"

#include "VipImgTempl.h"
#include "math.h"
#include "NumericalRecipes.h"
#include "Matrix.h"
#include "VipCore.h"

#include "VipGlobalFunc.h"

#include <float.h>
#include "limits.h"

/**	Labelling
    @param		src			The source image
    @param		dstBuf		Temp Buf for Recursive Label Tracking
	@param		bUseCross	Use Cross Mask
    @return		Label Count
    @author		WooYeon Jeong
    @version	0.1
*/

VIPLIB_API int Labelling(const ByteImage &src, IntImage &dstBuf, bool bUseCross = true);

VIPLIB_API int LabellingRect (const ByteImage &srcBuf, IntImage &dstBuf, ByteImage &stkBuf);
VIPLIB_API int LabellingCross(const ByteImage &srcBuf, IntImage &dstBuf, ByteImage &stkBuf);

/**	Create LogPolar Lookup Table
	@param		centerX       X Center of Polar
	@param		centerY       Y Center of Polar
	@param		srcRStart     Start of Radius
	@param		srcRSize      End   of Radius
	@param		dstRSize      Resolution of Destination Radius
	@param		dstThetaSize  Resolution of Destination Theta 
	@param		XMap          Result X Lookup Table
	@param		YMap          Result Y Lookup Table
*/
VIPLIB_API void MakeLogPolarTable(double centerX, double centerY, double srcRStart, double srcRSize, int dstRSize, int dstThetaSize, FloatImage &XMap, FloatImage &YMap);

///
VIPLIB_API CRemapLUT MakeLogPolarTable(double centerX, double centerY, double srcRStart, double srcRSize, int dstRSize, int dstThetaSize);


/**	Create Polar Lookup Table
	@param		centerX       X Center of Polar
	@param		centerY       Y Center of Polar
	@param		srcRStart     Start of Radius
	@param		srcRSize      End   of Radius
	@param		dstRSize      Resolution of Destination Radius
	@param		dstThetaSize  Resolution of Destination Theta 
	@param		XMap          Result X Lookup Table
	@param		YMap          Result Y Lookup Table
*/
VIPLIB_API void MakePolarTable   (double centerX, double centerY, double srcRStart, double srcRSize, int dstRSize, int dstThetaSize, FloatImage &XMap, FloatImage &YMap);

///
VIPLIB_API CRemapLUT MakePolarTable(double centerX, double centerY, double srcRStart, double srcRSize, int dstRSize, int dstThetaSize);

/// Generate Gabor Kernel Image
/// k   = pow(2.0, -(k + 2.0) / 2.0) * 2 * PI
/// phi = phi * (PI / 8)
VIPLIB_API FloatImage MakeGaborKernel(
					int XSize,
					int YSize,
					double k,
					double phi,
					int phase,
					double sigma = 2 * 3.141592654
					);

VIPLIB_API IplConvKernel   * AllocateConvKernel  (const char *szInput);
VIPLIB_API IplConvKernelFP * AllocateConvKernelFP(const char *szInput);
VIPLIB_API IplConvKernelFP * ImageToConvKernel(const FloatImage &image);
VIPLIB_API IplConvKernel   * ImageToConvKernel(const IntImage   &image);
VIPLIB_API void DeleteConvKernel(IplConvKernel   *pKernel);
VIPLIB_API void DeleteConvKernel(IplConvKernelFP *pKernel);

/**	Create Clone Buffer
    @param		input		The source image
    @return		Zero Padded Image with same size of input
*/
DefImgTempl
TemplImage CloneBuffer(const TemplImage &input)
{
	CHECK_NULL(input, "CloneBuffer");
	return TemplImage(input.GetXSize(), input.GetYSize(), input.GetCSize());
}

/**	Create Clone Buffer using ROI Region

    @param		input		The source image
    @return		Zero Padded Image with same ROI of input
*/
DefImgTempl
TemplImage CloneROIBuffer(const TemplImage &input)
{
	CHECK_NULL(input, "CloneROIBuffer");
	return TemplImage(input.roiWidth(), input.roiHeight(), input.GetCSize());
}

/**	Convert Image to Double Column Vector

    @param		input		The source image
    @return		Double Column Vector (Row Size : XSize*YSize, Col Size : 1)
*/
DefImgTempl
DoubleMatrix ImageToDoubleVector(const TemplImage &input)
{
	VIP_ASSERT(input.GetCSize() == 1, "ImageToDoubleVector", "Input Image Channel has to be 1");
	int XSize = input.GetXSize();
	int YSize = input.GetYSize();
	DoubleMatrix output(XSize * YSize, 1);

	double   *pDst =(double *)output.GetBuffer();
	const T **ppSrc = input.Get2DBufferConst();
	for (int y = 0; y < YSize; y++)
	{
		const T *pSrc = ppSrc[y];
		for (int x = 0; x < XSize; x++)
			pDst[x] =(double)pSrc[x];
		pDst += XSize;
	}
	return output;
}

/**	Convert Image to Float Column Vector

    @param		input		The source image
    @return		Float Column Vector (Row Size : XSize*YSize, Col Size : 1)
*/
DefImgTempl
FloatMatrix ImageToFloatVector(const TemplImage &input)
{
	VIP_ASSERT(input.GetCSize() == 1, "ImageToDoubleVector", "Input Image Channel has to be 1");
	int XSize = input.GetXSize();
	int YSize = input.GetYSize();
	FloatMatrix output(XSize * YSize, 1);

	float    *pDst  =(float *)output.GetBuffer();
	const T **ppSrc = input.Get2DBufferConst();
	for (int y = 0; y < YSize; y++)
	{
		const T *pSrc = ppSrc[y];
		for (int x = 0; x < XSize; x++)
			pDst[x] =(float)pSrc[x];
		pDst += XSize;
	}
	return output;
}

/**	Convert Matrix to Image
    @param		input		Column Vector (Matrix)
    @param		XSize       Image XSize
    @param		YSize       Image YSize
    @return		Image (XSize, YSize)
*/
template <class T>
FloatImage MatrixToImage(const CMatrix <T> &input, int XSize, int YSize)
{
	VIP_ASSERT(input.GetColSize() == 1, "MatrixToImage", "Input Matrix have to be Column Vector");
	VIP_ASSERT(input.GetRowSize() == XSize * YSize, "MatrixToImage", "Row Size does not match with XSize & YSize");

	FloatImage output(XSize, YSize);

	const T *pSrc  = (const T *)input .GetBufferConst();
	float  **ppDst = output.Get2DBuffer();
	for (int y = 0; y < YSize; y++)
	{
		float *pDst = ppDst[y];
		for (int x = 0; x < XSize; x++)
			pDst[x] =(float) pSrc[x];
		pSrc += XSize;
	}

	return output;
}


/*------------------------------------------------------------------*/
///@name	Conversion & Data Exchange
/*------------------------------------------------------------------*/
//@{

// DefImgTempl
// TemplImage ColorToGray(const TemplImage & input)
// {
// 	CHECK_NULL(input, "ColorToGray");
// 	VIP_ASSERT(input.GetCSize() == 3, "ColorToGray", "Source image must be color image");
// 
// 	TemplImage output(input.GetXSize(), input.GetYSize());
// 	iplColorToGray((IplImage *)input.GetIplImageConst(), output);
// 	return output;
// };

/**	Convert Color To Gray

    @param		input		The source image(Gray)
    @return		Result Image(Color)
*/
VIPLIB_API ByteImage ColorToGray(const ByteImage & input);

/**	Convert Gray To Color
    @param		input		The source image(Color)
	@param		FractR		The red   intensity (Default is 1)
	@param		FractG		The green intensity (Default is 1)
	@param		FractB		The blue  intensity (Default is 1)
    @return		Result Image(Gray)
*/
VIPLIB_API ByteImage GrayToColor(const ByteImage & input);

// DefImgTempl
// TemplImage GrayToColor(const TemplImage & input, float FractR = 1, float FractG = 1, float FractB = 1)
// {
// 	CHECK_NULL(input, "GrayToColor");
// 	VIP_ASSERT(input.GetCSize() == 1, "GrayToColor", "Source image must be gray image");
// 
// 	TemplImage output(input.GetXSize(), input.GetYSize(), 3);
// 	iplGrayToColor((IplImage *)input.GetIplImageConst(), output, FractR, FractG, FractB);
// 	return output;
// };


//@}

#ifndef _VIP_IPL_WRAP_
/*------------------------------------------------------------------*/
///@name	Filtering Functions
/*------------------------------------------------------------------*/
//@{

/**	Convolution

    @param Operand1   Input Image
    @param Operand2   Template Image : It have to be IntImage
    @param nShift     Shift of Result

    @return CVipImgTempl
*/

VIPLIB_API FloatImage Conv(const FloatImage & src, const IplConvKernelFP * pKernel, int combineMethod = IPL_SUM);
VIPLIB_API IntImage   Conv(const IntImage   & src, const IplConvKernel   * pKernel, int combineMethod = IPL_SUM);
VIPLIB_API ByteImage  Conv(const ByteImage  & src, const IplConvKernel   * pKernel, int combineMethod = IPL_SUM);

/** Convolves an image with a predefined kernel.

    @param		src		The source image
	@param		filter	One of the predfined kernels
				- IPL_PREWITT_3x3_V = 0
				- IPL_PREWITT_3x3_H
				- IPL_SOBEL_3x3_V
				- IPL_SOBEL_3x3_H
				- IPL_LAPLACIAN_3x3
				- IPL_LAPLACIAN_5x5
				- IPL_GAUSSIAN_3x3
				- IPL_GAUSSIAN_5x5
				- IPL_HIPASS_3x3
				- IPL_HIPASS_5x5
				- IPL_SHARPEN_3x3
    @return		The resultant image
*/
DefImgTempl TemplImage FixedFilter(const TemplImage & src, int filter)
{
	TemplImage Output = CloneROIBuffer(src);
	iplFixedFilter((IplImage *)src.GetIplImageConst(), Output, (IplFilter)filter);
	return Output;
}

/** Median Filter
    @param		src			The source image
	@param		nCols		Number of columns in the neighbourhood to use.
	@param		nRows		Number of rows in the neighbourhood to use.
	@param		anchorX		
	@param		anchorY		The[x, y] coordinates of the anchor cell in the neighbourhood.
    @return		The resultant image
*/
DefImgTempl
TemplImage MedianFilter(const TemplImage & src, int nCols = 3, int nRows = 3, int anchorX = -1, int anchorY = -1)
{
	if (anchorX == -1) anchorX = nCols / 2;
	if (anchorY == -1) anchorY = nRows / 2;
	TemplImage Output = CloneROIBuffer(src);
	iplMedianFilter((IplImage *)src.GetIplImageConst(), Output, nCols, nRows, anchorX, anchorY);
	return Output;
}
//@}

/*------------------------------------------------------------------*/
///@name	Morphological Operations
/*------------------------------------------------------------------*/
//@{

/** Erode
    @param		input	The source image
    @param		iter	Iteration
    @return		The resultant image
*/
DefImgTempl
TemplImage Erode(const TemplImage & input, int iter = 1)
{
	TemplImage Output = CloneROIBuffer(input);
	iplErode((IplImage *)input.GetIplImageConst(), Output, iter);
	return Output;
};

/** Dilate
    @param input
    @param iter
    @return CVipImgTempl
*/
DefImgTempl
TemplImage Dilate(const TemplImage & input, int iter = 1)
{
	TemplImage Output = CloneROIBuffer(input);
	iplDilate((IplImage *)input.GetIplImageConst(), Output, iter);
	return Output;
};

/**	Open
    @param input
    @param iter
    @return CVipImgTempl
*/
DefImgTempl
TemplImage Open(const TemplImage & input, int iter = 1)
{
	TemplImage Output = CloneROIBuffer(input);
	iplOpen((IplImage *)input.GetIplImageConst(), Output, iter);
	return Output;
};

/**	Close
    @param input
    @param iter
    @return CVipImgTempl
*/
DefImgTempl
TemplImage Close(const TemplImage & input, int iter = 1)
{
	TemplImage Output = CloneROIBuffer(input);
	iplClose((IplImage *)input.GetIplImageConst(), Output, iter);
	return Output;
};
//@}

#endif

/*------------------------------------------------------------------*/
///@name	Histogram and Thresholding Functions
/*------------------------------------------------------------------*/
//@{

/**	Thres
	Binarize Image by Threshold

	@param		src			The source image.
	@param		Threshold	Array of X Position
	@param		Big			Set Pixel to this value if Pixel is bigger than Threshold : Default 255
	@param		Small		Set Pixel to this value if Pixel is smaller than Threshold : Deault 0

	@return		CIplIempl
*/

DefImgTempl
TemplImage Thres(const TemplImage & src, float Threshold, float Big = 255, float Small = 0)
{
	TemplImage dst = CloneROIBuffer(src);

	T tmpT = (T)Threshold;
	T tmpB = (T)Big;
	T tmpS = (T)Small;

	vipThres(
		src.roiBufferConst(), src.GetWidthStep(),
		dst.GetBuffer     (), dst.GetWidthStep(), dst.GetXSize(), dst.GetYSize(),
		tmpT, tmpB, tmpS);

	return dst;
}

#ifndef _VIP_IPL_WRAP_

extern int histKey[257];
extern int histValue[256];

/**	Histogram Equalize
	@param		src			The source image.
	@param		plut		Histogram Lookup Table
	@return		CIplIempl
*/
DefImgTempl
TemplImage HistoEqualize(const TemplImage & src, IplLUT *plut = NULL)
{
	TemplImage output = CloneROIBuffer(src);
	const int range = 256;
	IplLUT lut = {range + 1, NULL, NULL, NULL, IPL_LUT_LOOKUP };
	if (!plut)
	{
		lut.key   = histKey;
		lut.value = histValue;
		plut = &lut;
	}

	iplComputeHisto((IplImage *)src.GetIplImageConst(), &plut);
	iplHistoEqualize((IplImage *)src.GetIplImageConst(), output, &plut);

	return output;
}

/**	Contrast Stretch
	@param		src			The source image.
	@param		plut		Histogram Lookup Table
	@return		CIplIempl
*/
DefImgTempl
TemplImage ContrastStretch(const TemplImage & src, IplLUT *plut = NULL)
{
	TemplImage output = CloneROIBuffer(src);
	const int range = 256;
	IplLUT lut = {range + 1, NULL, NULL, NULL, IPL_LUT_LOOKUP };
	if (!plut)
	{
		lut.key   = histKey;
		lut.value = histValue;
		plut = &lut;
	}

	iplComputeHisto((IplImage *)src.GetIplImageConst(), &plut);
	iplContrastStretch((IplImage *)src.GetIplImageConst(), output, &plut);

	return output;
}

/**	Min Max Truncate
	@param		src			The source image.
	@param		min			Min Value
	@param		max			Max Value
	@return		CIplIempl
*/
template <class T, eIplBase_Type TM_TYPE, class T2>
TemplImage Truncate(const TemplImage & src, T2 minV, T2 maxV)
{
	TemplImage dst;
	vipTruncateMinMax(
		src.roiBufferConst(), src.GetWidthStep(),
		dst.GetBuffer     (), dst.GetWidthStep(), dst.GetXSize(), dst.GetYSize(), minV, maxV);
	return dst;
}


//@}


/*------------------------------------------------------------------*/
///@name	Fast Fourier and Discrete Cosine Transforms
/*------------------------------------------------------------------*/
//@{

/**	Fourier Transform Using RC Pack
    @param		input source image
    @return		FloatImage
*/
DefImgTempl
FloatImage FFTRCPack(const TemplImage & input)
{
	FloatImage output(input.GetXSize(), input.GetYSize(), input.GetCSize());
	iplRealFft2D((IplImage *)input.GetIplImageConst(), output, IPL_FFT_Forw | IPL_FFT_UseFloat);
	return output;
}

/**	Inverse Fourier Transform Using RC Pack
    @param		input source image
    @return		FloatImage
*/
DefImgTempl
FloatImage IFFTRCPack(const TemplImage & input)
{
	FloatImage output(input.GetXSize(), input.GetYSize(), input.GetCSize());
	iplCcsFft2D((IplImage *)input.GetIplImageConst(), output, IPL_FFT_Inv | IPL_FFT_UseFloat);
	return output;
}

/**	Multiply RCPack Image
    @param		op1		Operand1
    @param		op2		Operand2
    @return		FloatImage
*/
DefImgTempl
FloatImage MultiplyRCPack(const TemplImage & op1, const TemplImage & op2)
{
	FloatImage output(op1.GetXSize(), op2.GetYSize(), op1.GetCSize());
	iplMpyRCPack2D(
		(IplImage *)op1.GetIplImageConst(),
		(IplImage *)op2.GetIplImageConst(),
		output);

	return output;
}

/**	Calculate Minimum FFT Size
    @param		in	image size
    @return		FFT Size
*/
VIPLIB_API int GetFFTSize(int in);

/**	Fourier Transform
    @param		input		Input Image
    @param		isign       Direction
    @return		FFT Result (2 Channel)  */
VIPLIB_API FloatImage  FFT(const FloatImage & input);

/**	Fourier Transform with zero padding
    @param		input		Input Image
	@param		XSize		X Size
	@param		YSize		Y Size
    @param		isign       Direction
    @return		FFT Result (2 Channel)  */
VIPLIB_API FloatImage  FFT(const FloatImage & input, int XSize, int YSize);

/**	Inverse Transform
    @param		input		Input Image
    @return		FFT Result (2 Channel)  */
VIPLIB_API FloatImage IFFT(const FloatImage & input);

/**	Inverse Fourier Transform with zero padding
    @param		input		Input Image
	@param		XSize		X Size
	@param		YSize		Y Size
    @param		isign       Direction
    @return		FFT Result (2 Channel)  */
VIPLIB_API FloatImage IFFT(const FloatImage & input, int XSize, int YSize);

//@}

#endif

/*------------------------------------------------------------------*/
///@name	Image Features functions
/*------------------------------------------------------------------*/
//@{

/**	Find Maxinum Value
    @param		src		Input Image
    @return		Maxinum Value
*/
DefImgTempl
T GetMax(const TemplImage & src)
{
	int width  = src.roiWidth ();
	int height = src.roiHeight();
	T maxV;
	switch(TM_TYPE)
	{
	case ipl_BYTE   : vipMax(src.roiBufferConst(), src.GetWidthStep(), width, height, maxV, (T)        0); break;
	case ipl_k_INT  :
	case ipl_INT    : vipMax(src.roiBufferConst(), src.GetWidthStep(), width, height, maxV, (T)  INT_MIN); break;
	case ipl_SHORT  : vipMax(src.roiBufferConst(), src.GetWidthStep(), width, height, maxV, (T) SHRT_MIN); break;
	case ipl_CHAR   : vipMax(src.roiBufferConst(), src.GetWidthStep(), width, height, maxV, (T)SCHAR_MIN); break;
	case ipl_USHORT : vipMax(src.roiBufferConst(), src.GetWidthStep(), width, height, maxV, (T)        0); break;
	case ipl_k_FLOAT:
	case ipl_FLOAT  :
		{
			float Min, Max;
			iplMinMaxFP((IplImage *)src.GetIplImageConst(), &Min, &Max);
			maxV = (T)Max;
		}
		break;
	default: VIP_ASSERT(0, "GetMax", "Wrong Image Data Type");
	}

	return maxV;
}

/**	Find Minimum Value
    @param		src		Input Image
    @return		Minimum Value
*/
DefImgTempl
T GetMin(const TemplImage & src)
{
	int width  = src.roiWidth ();
	int height = src.roiHeight();
	T minV;
	switch(TM_TYPE)
	{
	case ipl_BYTE   : vipMin(src.roiBufferConst(), src.GetWidthStep(), width, height, minV,         255); break;
	case ipl_k_INT  :
	case ipl_INT    : vipMin(src.roiBufferConst(), src.GetWidthStep(), width, height, minV, (T) INT_MAX); break;
	case ipl_SHORT  : vipMin(src.roiBufferConst(), src.GetWidthStep(), width, height, minV, (T)SHRT_MAX); break;
	case ipl_CHAR   :
	case ipl_USHORT : VIP_ASSERT(0, "GetMax", "Does not support this function"); break;
	case ipl_k_FLOAT:
	case ipl_FLOAT  :
		{
			float Min, Max;
			iplMinMaxFP((IplImage *)src.GetIplImageConst(), &Min, &Max);
			minV = (T)Min;
		}
		break;
	default: VIP_ASSERT(0, "", "");
	}
	return minV;
}

/** Find Maximum Value and Position
    @param		src		The source image.
    @param		pX		pointer of XPosition Array
	@param		pY		pointer of Y Position Array
	@param		pV		pointer of Return Value Array
	@param		Count	Size of array (rank)
*/
DefImgTempl
void GetMax(const TemplImage & src, int *pX, int *pY, T *pV, int Count = 1)
{
	CHECK_NULL(src, "GetMax");

	char *pShiftBuf = new char[sizeof(float) * Count]; // 만약 double image 가 지원 되면 buffer size 변경.

	switch(VIP_KERNEL_MASK&src.GetDepth())
	{
	case ipl_BYTE  : { memset(pV, 0, Count * sizeof(T));                              } break;
	case ipl_INT   : { for (int k = 0 ; k < Count ; k++) ((int *)  pV)[k] =  INT_MIN; } break; // memset으로 대치 고려.
	case ipl_FLOAT : { for (int k = 0 ; k < Count ; k++) ((float *)pV)[k] = -FLT_MAX; } break;
	case ipl_CHAR  : VIP_ASSERT(0, "GetMax", "Does not support this function"); break;
	case ipl_SHORT : VIP_ASSERT(0, "GetMax", "Does not support this function"); break;
	case ipl_USHORT: VIP_ASSERT(0, "GetMax", "Does not support this function"); break;
	default        : VIP_ASSERT(0, "Type Error", "GetMax");
	}

	const T *pSrc = (const T *)src.GetBufferConst();
	int Size  = src.GetBufferSize() / sizeof(T);
	int WStep = src.GetWidthStep()  / sizeof(T);

	for (int i = 0 ; i < Size ; i++)
	{
		for (int j = 0 ; j < Count ; j++)
		{
			if(pSrc[i] > pV[j])
			{
				int LeftSize = Count-j-1;
				if(LeftSize)
				{
					memcpy(pShiftBuf , pV + j   , LeftSize * sizeof(T)  );
					memcpy(pV + j + 1, pShiftBuf, LeftSize * sizeof(T)  );
					memcpy(pShiftBuf , pX + j   , LeftSize * sizeof(int));
					memcpy(pX + j + 1, pShiftBuf, LeftSize * sizeof(int));
				}

				pV[j] = pSrc[i];
				pX[j] = i;
				break;
			}
		}
	}

	for (int j = 0 ; j < Count ; j++)
	{
		pY[j] = pX[j] / WStep;
		pX[j] %= WStep;
	}

	delete [] pShiftBuf;
}

/**	Find Minimum Value and Position

    @param		src		The source image.
    @param		pX		pointer of XPosition Array
	@param		pY		pointer of Y Position Array
	@param		pV		pointer of Return Value Array
	@param		Count	Size of array (rank)

    @return		void
*/
DefImgTempl
void GetMin(const TemplImage & src, int *pX, int *pY, T *pV, int Count = 1)
{
	CHECK_NULL(src, "GetMax");

	char *pShiftBuf = new char[sizeof(float) * Count]; // 만약 double image 가 지원 되면 buffer size 변경.

	switch(VIP_KERNEL_MASK&src.GetDepth())
	{
	case ipl_BYTE  : { memset(pV, 0xff, Count * sizeof(T));                          } break;
	case ipl_INT   : { for (int k = 0 ; k < Count ; k++) ((int *)  pV)[k] = INT_MAX; } break;
	case ipl_FLOAT : { for (int k = 0 ; k < Count ; k++) ((float *)pV)[k] = FLT_MAX; } break;
	case ipl_CHAR  : VIP_ASSERT(0, "GetMax", "Does not support this function"); break;
	case ipl_SHORT : VIP_ASSERT(0, "GetMax", "Does not support this function"); break;
	case ipl_USHORT: VIP_ASSERT(0, "GetMax", "Does not support this function"); break;
	default        :	VIP_ASSERT(0, "Type Error", "GetMax");
	}

	const T *pSrc = (const T *)src.GetBufferConst();
	int Size  = src.GetBufferSize() / sizeof(T);
	int WStep = src.GetWidthStep()  / sizeof(T);

	for (int i = 0 ; i < Size ; i++)
	{
		for (int j = 0 ; j < Count ; j++)
		{
			if(pSrc[i] < pV[j])
			{
				int LeftSize = Count-j-1;
				if(LeftSize)
				{
					memcpy(pShiftBuf , pV + j   , LeftSize * sizeof(T)  );
					memcpy(pV + j + 1, pShiftBuf, LeftSize * sizeof(T)  );
					memcpy(pShiftBuf , pX + j   , LeftSize * sizeof(int));
					memcpy(pX + j + 1, pShiftBuf, LeftSize * sizeof(int));
				}

				pV[j] = pSrc[i];
				pX[j] = i;
				break;
			}
		}
	}

	for (int j = 0 ; j < Count ; j++)
	{
		pY[j] = pX[j] / WStep;
		pX[j] %= WStep;
	}

	delete [] pShiftBuf;
}


//@}

/*------------------------------------------------------------------*/
///@name	Arithmetic Functions
/*------------------------------------------------------------------*/
//@{


/**	Extract Phase Image
    @param input			The source image.
    @return CIplIempl
*/
VIPLIB_API FloatImage Phase    (const FloatImage &input);

/**	Extract Magnitude Image
    @param input			The source image.
    @return CIplIempl
*/
VIPLIB_API FloatImage Magnitude(const FloatImage &input);

/**	Square Root
    @param src			The source image.
    @return CIplIempl
*/
DefImgTempl
TemplImage Sqrt(const TemplImage & src)
{
	CHECK_NULL(src, "Sqrt");
	TemplImage dst = CloneROIBuffer(src);
	vipSqrt(
		src.roiBufferConst(), src.GetWidthStep(),
		dst.GetBuffer     (), dst.GetWidthStep(), dst.GetXSize(), dst.GetYSize() );
	return dst;
}

/** Absolute Value
    @param src			The source image.
    @return CIplIempl
*/
DefImgTempl
TemplImage Abs(const TemplImage & src)
{
	TemplImage dst = CloneROIBuffer(src);

	if (src.GetDepth() == ipl_FLOAT)
	{
		vipAbs_f(
			src.roiBufferConst(), src.GetWidthStep(),
			dst.GetBuffer     (), dst.GetWidthStep(), dst.GetXSize(), dst.GetYSize() );
	}
	else
	{
		vipAbs_i(
			src.roiBufferConst(), src.GetWidthStep(),
			dst.GetBuffer     (), dst.GetWidthStep(), dst.GetXSize(), dst.GetYSize() );
	}
	return dst;
}

/** Pow
    @param src			The source image.
    @return CIplIempl
*/
DefImgTempl
TemplImage Pow(const TemplImage & src, T value)
{
	TemplImage Output = CloneROIBuffer(src);
	switch(TM_TYPE)
	{
	case ipl_k_FLOAT:
	case ipl_FLOAT  : vipPowSFP((IplImage *)src.GetIplImageConst(), Output, value, 0);
	default         : vipPowS  ((IplImage *)src.GetIplImageConst(), Output, value, 0);
	}
	return Output;
}

#ifndef _VIP_IPL_WRAP_
/** Norm
    @param  input1		The source image 1.
    @param  input2		The source image 2.
    @param  normType	Norm Type
    @return CIplIempl
*/
DefImgTempl
double Norm(const TemplImage & input1, const TemplImage & input2, int normType = IPL_L2)
{
	return iplNorm((IplImage *)input1.GetIplImageConst(), (IplImage *)input2.GetIplImageConst(), normType);
};

/** Norm
    @param  input1		The source image.
    @param  normType	Norm Type
    @return CIplIempl
*/
DefImgTempl
double Norm(const TemplImage & input1, int normType = IPL_L2)
{
	return iplNorm((IplImage *)input1.GetIplImageConst(), NULL, normType);
};
#endif //_WIN32_CE

/** Sum

    @param src			The source image.
    @return CIplIempl
*/
DefImgTempl
double Sum(const TemplImage & src)
{
	CHECK_NULL(src, "Sum");
	return vipSum(src.roiBufferConst(), src.GetWidthStep(), src.roiWidth(), src.roiHeight() );
};

/** Mean

    @param src			The source image.
    @return CIplIempl
*/
DefImgTempl
double Mean(const TemplImage & src)
{
	CHECK_NULL(src, "Mean");
	return vipMean(src.roiBufferConst(), src.GetWidthStep(), src.roiWidth(), src.roiHeight() );
};

/** Variance

    @param src			The source image.
	@param mean			mean
    @return CIplIempl
*/

template <class T, eIplBase_Type TM_TYPE, class T2>
double Variance(const TemplImage & src, const T2 mean)
{
	CHECK_NULL(src, "Variance");
	return vipVariance(src.roiBufferConst(), src.GetWidthStep(), src.roiWidth(), src.roiHeight(), mean);
}

/** Normalize
    @param src			The source image.
    @return CIplIempl
*/

DefImgTempl
TemplImage MeanZeroUnitVariance(const TemplImage & src)
{
	CHECK_NULL(src, "Normalize");
	TemplImage dst = CloneROIBuffer(src);
	vipMeanZeroUnitVar(
		src.roiBufferConst(), src.GetWidthStep(),
		dst.GetBuffer     (), dst.GetWidthStep(), dst.GetXSize(), dst.GetYSize() );
}

/** Multiply Two Complex type Image
    @param  op1		Operand1
    @param  op2		Operand2
    @return CIplIempl
*/
DefImgTempl
TemplImage MultiplyCmplx(const TemplImage & op1, const TemplImage &op2)
{
	VIP_ASSERT(op1.GetCSize() == 2 && op2.GetCSize() == 2, "MultiplyCmplx", "Two Image have to be 2 channel Image");
	VIP_ASSERT(op1.GetXSize() == op2.GetXSize() && op1.GetYSize() == op2.GetYSize(), "MultiplyCmplx", "Image Dimensions are not same");

	TemplImage out = CloneBuffer(op1);

	const T ** ppOp1 = op1.Get2DBufferConst();
	const T ** ppOp2 = op2.Get2DBufferConst();
	      T ** ppOut = out.Get2DBuffer();

	int XSize = op1.GetXSize();
	int YSize = op2.GetYSize();

	int xCount = XSize * 2;

	for (int y = 0; y < op1.GetYSize(); y++)
	{
		const T * pOp1 = ppOp1[y];
		const T * pOp2 = ppOp2[y];
		      T * pOut = ppOut[y];

		for (int x = 0; x < xCount; x+=2)
		{
			T a = pOp1[x  ];
			T b = pOp1[x+1];
			T c = pOp2[x  ];
			T d = pOp2[x+1];
			pOut[x    ] = a*c - b*d;
			pOut[x + 1] = b*c + a*d;
		}
	}

	return out;
}

/** Divide Two Complex type Image
    @param	op1		Operand1
    @param	op2		Operand2
    @return CIplIempl
*/
DefImgTempl
TemplImage DivideCmplx(const TemplImage & op1, const TemplImage &op2)
{
	VIP_ASSERT(op1.GetCSize() == 2 && op2.GetCSize() == 2, "MultiplyCmplx", "Two Image have to be 2 channel Image");
	VIP_ASSERT(op1.GetXSize() == op2.GetXSize() && op1.GetYSize() == op2.GetYSize(), "MultiplyCmplx", "Image Dimensions are not same");

	TemplImage out = CloneBuffer(op1);

	const T ** ppOp1 = op1.Get2DBufferConst();
	const T ** ppOp2 = op2.Get2DBufferConst();
	      T ** ppOut = out.Get2DBuffer();

	int XSize = op1.GetXSize();
	int YSize = op2.GetYSize();

	int xCount = XSize * 2;

	for (int y = 0; y < op1.GetYSize(); y++)
	{
		const T * pOp1 = ppOp1[y];
		const T * pOp2 = ppOp2[y];
		      T * pOut = ppOut[y];

		for (int x = 0; x < xCount; x+=2)
		{
			T a = pOp1[x    ];
			T b = pOp1[x + 1];
			T c = pOp2[x    ];
			T d = pOp2[x + 1];
			T temp = d*d + c*c;
			if(temp)
			{
				pOut[x    ] = 1./temp * (a*c + b*d);
				pOut[x + 1] = 1./temp * (b*c - a*d);
			}
		}
	}

	return out;
}

/** Complex Conjugate
    @param	input	The source image.
    @return CIplIempl
*/
DefImgTempl
TemplImage ComplexConjugate(const TemplImage &input)
{
	VIP_ASSERT(input.GetCSize() == 2, "ComplexConjugate", "input channel size must be 2");

	int XSize = input.GetXSize();
	int YSize = input.GetYSize();

	TemplImage output = input;
	const T **ppSrc = input .Get2DBufferConst();
	      T **ppDst = output.Get2DBuffer();

	for (int y = 0; y < YSize; y++)
	{
		const T *pSrc = ppSrc[y];
		      T *pDst = ppDst[y];
		for (int x = 1; x < XSize*2; x += 2)
			pDst[x] = -pSrc[x];
	}

	return output;
}

/** Check Image is NAN(Not a Number)
    @param	input	The source image.
    @return true or false
*/
DefImgTempl
bool IsNaN(const TemplImage & input)
{
	CHECK_NULL(input, "IsNaN");
	const T  *pBuf =(const T *)input.GetBufferConst();
	int nBufSize =      input.GetBufferSize() / sizeof(T);

	for (int i = 0; i < nBufSize; i++)
	{
		if (_isnan(pBuf[i]))
			return true;
	}

	return false;
};
//@}

/*------------------------------------------------------------------*/
///@name	Geometric Transformation Functions
/*------------------------------------------------------------------*/
//@{

/** Vertical concatenation
    @param	count	Number of Image for concatenation
    @param	first   Image1, Image2, Image3, ...
    @return Concatenated Image
*/
DefImgTempl
TemplImage VertCat(const int count, const TemplImage first, ...)
{
	va_list marker;	

	// Get output Size
	int outputXSize = first.GetXSize();
	int outputYSize = first.GetYSize();
	int outputCSize = first.GetCSize();

	va_start( marker, first ); // Initialize variable arguments	
	{
		for (int i = 1 ; i < count ; i++)
		{
			TemplImage & CurImage = va_arg(marker, TemplImage);
			outputXSize = __max(outputXSize, CurImage.GetXSize());
			outputCSize = __max(outputCSize, CurImage.GetCSize());
			outputYSize += CurImage.GetYSize();
		}
	}
	va_end( marker );  // Reset variable arguments
	
	// Allocate output
	TemplImage output(outputXSize, outputYSize, outputCSize);

	// Set elements of output
	int nStartRowIndex = 0;
	output.Paste(first, 0, nStartRowIndex);
	nStartRowIndex += first.GetYSize();

	va_start(marker, first); // Initialize variable arguments
	{
		for (int i = 1 ; i < count ; i++)
		{
			TemplImage & CurImage = va_arg(marker, TemplImage);
			output.Paste(CurImage, 0, nStartRowIndex);
			nStartRowIndex += CurImage.GetYSize();
		}
	}
	va_end( marker ); // Reset variable arguments

	return output;
}

/** Horizontal concatenation
    @param	count	Number of Image for concatenation
    @param	first   Image1, Image2, Image3, ...
    @return Concatenated Image
*/
DefImgTempl
TemplImage HorzCat(const int count, const TemplImage first, ...)
{
	va_list marker;	

	// Get output Size
	int outputXSize = first.GetXSize();
	int outputYSize = first.GetYSize();
	int outputCSize = first.GetCSize();

	va_start( marker, first ); // Initialize variable arguments	
	{
		for (int i = 1 ; i < count ; i++)
		{
			TemplImage & CurImage = va_arg(marker, TemplImage);
			outputYSize = __max(outputYSize, CurImage.GetYSize());
			outputCSize = __max(outputCSize, CurImage.GetCSize());
			outputXSize += CurImage.GetXSize();
		}
	}
	va_end( marker );  // Reset variable arguments

	// Allocate output
	TemplImage output(outputXSize, outputYSize, outputCSize);

	// Set elements of output
	int nStartColIndex = 0;
	output.Paste(first, nStartColIndex, 0);
	nStartColIndex += first.GetXSize();

	va_start(marker, first); // Initialize variable arguments
	{
		for (int i = 1 ; i < count ; i++)
		{
			TemplImage & CurImage = va_arg(marker, TemplImage);
			output.Paste(CurImage, nStartColIndex, 0);
			nStartColIndex += CurImage.GetXSize();
		}
	}
	va_end( marker ); // Reset variable arguments

	return output;
}

/** Remapping Image
	@param		input		The source image.
	@param		XMap		X Lookup Table
	@param		YMap		Y Lookup Table
	@param		output		The resultant image
	@param		interpolate	The type of interpolation to perform for resampling	the input image. The following are currently supported.
				- IPL_INTER_NN       Nearest neighbour interpolation.
				- IPL_INTER_LINEAR   Linear interpolation.
				- IPL_INTER_CUBIC    Cubic convolution interpolation.
				- IPL_INTER_SUPER    Super sampling interpolation.
    @return		void
*/
DefImgTempl
void Remap(const TemplImage &input, const FloatImage &XMap, const FloatImage &YMap, TemplImage &output, int interpolate = IPL_INTER_LINEAR)
{
	iplRemap(
		(IplImage*)input.GetIplImageConst(), 
		(IplImage*)XMap .GetIplImageConst(), 
		(IplImage*)YMap .GetIplImageConst(), 
		output, 
		interpolate);
}

/** Remapping Image
	@param		input		The source image.
	@param		XMap		X Lookup Table
	@param		YMap		Y Lookup Table
	@param		interpolate	The type of interpolation to perform for resampling	the input image. The following are currently supported.
				- IPL_INTER_NN       Nearest neighbour interpolation.
				- IPL_INTER_LINEAR   Linear interpolation.
				- IPL_INTER_CUBIC    Cubic convolution interpolation.
				- IPL_INTER_SUPER    Super sampling interpolation.
    @return		The resultant image
*/
DefImgTempl
TemplImage Remap(const TemplImage &input, const FloatImage &XMap, const FloatImage &YMap, int interpolate = IPL_INTER_LINEAR)
{
	TemplImage output(XMap.roiWidth(), YMap.roiHeight());
	Remap(input, XMap, YMap, output, interpolate);
	return output;
}

/** Remapping Image
	@param		input		The source image.
	@param		Table		Lookup Table
	@param		output		The resultant image
	@param		interpolate	The type of interpolation to perform for resampling	the input image. The following are currently supported.
				- IPL_INTER_NN       Nearest neighbour interpolation.
				- IPL_INTER_LINEAR   Linear interpolation.
				- IPL_INTER_CUBIC    Cubic convolution interpolation.
				- IPL_INTER_SUPER    Super sampling interpolation.
    @return		void
*/
DefImgTempl
void Remap(const TemplImage &input, const CRemapLUT &Table, TemplImage &output, int interpolate = IPL_INTER_LINEAR)
{
	Remap(input, Table.m_tblX, Table.m_tblY, output, interpolate);
}

/** Remapping Image
	@param		input		The source image.
	@param		Table		Lookup Table
	@param		interpolate	The type of interpolation to perform for resampling	the input image. The following are currently supported.
				- IPL_INTER_NN       Nearest neighbour interpolation.
				- IPL_INTER_LINEAR   Linear interpolation.
				- IPL_INTER_CUBIC    Cubic convolution interpolation.
				- IPL_INTER_SUPER    Super sampling interpolation.
    @return		The resultant image
*/
DefImgTempl
TemplImage Remap(const TemplImage &input, const CRemapLUT &Table, int interpolate = IPL_INTER_LINEAR)
{
	return Remap(input, Table.m_tblX, Table.m_tblY, interpolate);
}

/** Remapping Image
	@param		input		The source image.
	@param		Table		Lookup Table
	@param		roi			Region of Interest
	@param		output		The resultant image
	@param		interpolate	The type of interpolation to perform for resampling	the input image. The following are currently supported.
				- IPL_INTER_NN       Nearest neighbour interpolation.
				- IPL_INTER_LINEAR   Linear interpolation.
				- IPL_INTER_CUBIC    Cubic convolution interpolation.
				- IPL_INTER_SUPER    Super sampling interpolation.
    @return		void
*/

DefImgTempl
void Remap(const TemplImage &input, const CRemapLUT &Table, const IplROI &roi, TemplImage &output, int interpolate = IPL_INTER_LINEAR)
{
	IplImage *pX = (IplImage *)Table.m_tblX.GetIplImageConst();
	IplImage *pY = (IplImage *)Table.m_tblY.GetIplImageConst();

	IplROI *pROIX = pX->roi;
	IplROI *pROIY = pY->roi;

	pX->roi = pY->roi = (IplROI *)&roi;

	Remap(input, Table, output, interpolate);

	pX->roi = pROIX;
	pY->roi = pROIY;
}

/** Remapping Image
	@param		input		The source image.
	@param		Table		Lookup Table
	@param		roi			Region of Interest
	@param		interpolate	The type of interpolation to perform for resampling	the input image. The following are currently supported.
				- IPL_INTER_NN       Nearest neighbour interpolation.
				- IPL_INTER_LINEAR   Linear interpolation.
				- IPL_INTER_CUBIC    Cubic convolution interpolation.
				- IPL_INTER_SUPER    Super sampling interpolation.
    @return		The resultant image
*/

DefImgTempl
TemplImage Remap(const TemplImage &input, const CRemapLUT &Table, const IplROI &roi, int interpolate = IPL_INTER_LINEAR)
{
	TemplImage output(roi.width, roi.height);
	Remap(input, Table, roi, output, interpolate);
	return output;
}

/** Image Resize
	@param		src			The source image.
	@param		width		Width of Target Image
	@param		height		Height of Traget Image
	@param		interpolate	The type of interpolation to perform for resampling	the input image. The following are currently supported.
				- IPL_INTER_NN       Nearest neighbour interpolation.
				- IPL_INTER_LINEAR   Linear interpolation.
				- IPL_INTER_CUBIC    Cubic convolution interpolation.
				- IPL_INTER_SUPER    Super sampling interpolation.
    @return		The resultant image
*/
DefImgTempl
TemplImage Resize(const TemplImage &src, int width, int height, int interpolate = IPL_INTER_LINEAR)
{
	TemplImage dst(width, height, src.GetCSize());// = CloneROIBuffer(src);
	vipResize(
		src.GetBufferConst(), src.GetWidthStep(), src.GetXSize(), src.GetYSize(),
		dst.GetBuffer     (), dst.GetWidthStep(), dst.GetXSize(), dst.GetYSize(), src.GetCSize());
	return dst;
}

/** Image Rotate 
	@param		src			The source image.
	@param		angle		Degree angle to rotate.
	@param		cx			Rotation Center X
	@param		cy			Rotation Center Y
	@param		width		Width  of Target Image
	@param		height		Height of Target Image
    @return		The resultant image
*/
DefImgTempl
void Rotate(const TemplImage &src, TemplImage &dst, float degAngle, float cx, float cy)
{
	VIP_ASSERT(src.GetDepth() != ipl_FLOAT, "Rotate", "Wrong src data type");
	VIP_ASSERT(src.GetCSize() == dst.GetCSize(), "Rotate", "Wrong dimension");

	vipRotate(
		src.GetBufferConst(), src.GetWidthStep(), src.GetXSize(), src.GetYSize(),
		dst.GetBuffer     (), dst.GetWidthStep(), dst.GetXSize(), dst.GetYSize(),
		degAngle, cx, cy, src.GetCSize());
}

/** Image Rotate 
	@param		src			The source image.
	@param		angle		Degree angle to rotate.
	@param		cx			Rotation Center X
	@param		cy			Rotation Center Y
	@param		width		Width  of Target Image
	@param		height		Height of Target Image
    @return		The resultant image
*/
DefImgTempl
TemplImage Rotate(const TemplImage &src, float degAngle, float cx = -10000, float cy = -10000, int width = 0, int height = 0)
{
	if(!width || !height)
	{
		width  = src.GetXSize();
		height = src.GetYSize();
	}
	if (cx == -10000 || cy == -10000)
	{
		cx = float(width  / 2);
		cy = float(height / 2);
	}

	TemplImage dst(width, height, src.GetCSize());

	Rotate(src, dst, degAngle, cx, cy);
	return dst;
}

/**	FFTShift
    @param src			The source image.
*/
DefImgTempl
TemplImage FFTShift(const TemplImage &src)
{
	TemplImage output = CloneBuffer(src);

	int XSize = src.GetXSize();
	int YSize = src.GetYSize();

	output.Paste(src.Crop(0      , 0      , XSize/2, YSize/2), XSize/2, YSize/2);
	output.Paste(src.Crop(XSize/2, YSize/2, XSize/2, YSize/2), 0      , 0      );
	output.Paste(src.Crop(XSize/2, 0      , XSize/2, YSize/2), 0      , YSize/2);
	output.Paste(src.Crop(0      , YSize/2, XSize/2, YSize/2), XSize/2, 0      );

	return output;
}

//@}

/*------------------------------------------------------------------*/
///@name	Image Array Handle Function
/*------------------------------------------------------------------*/
//@{

/** Generate Mean Image of ImageArray
    @param array	ImageArray
    @return CVipImgTempl
*/

DefImgTempl
TemplImage GetAverage(const TemplImageArray &array)
{
	VIP_ASSERT(array.GetSize(), "GetAverage", "Array Size is Zero");

	int DataCount = array[0].GetBufferSize() / array[0].GetSizeofPixel();
	int ArraySize = array.GetSize();

	double *pBuf = new double[DataCount];
	memset(pBuf, 0, sizeof(double) * DataCount);

	for (int i = 0; i < ArraySize; i++)
	{
		const TemplImage & test  = array[i];
		const T *         pImage =(const T *)array[i].GetBufferConst();

		VIP_ASSERT( DataCount == array[i].GetBufferSize() / array[i].GetSizeofPixel(), "", "");

		int     count = DataCount;
		double  *pDst = pBuf;
		const T *pSrc = pImage;

		while(count--)
			*(pDst++) += *(pSrc++);
	}

	for (int j = 0; j < DataCount; j++)
		pBuf[j] /= ArraySize;

	TemplImage output = CloneBuffer(array[0]);

	T *pImage =(T *)output.GetBuffer();
	for (int k = 0; k < DataCount; k++)
		pImage[k] =(T) pBuf[k];

	delete[] pBuf;

	return output;
}

DefImgTempl
DoubleMatrix ImageArrayToDoubleMatrix(const TemplImageArray &array)
{
	VIP_ASSERT(array.GetSize(), "ImageArrayToDoubleMatrix", "Array Size is Zero");

	int Row_ImageSize  = array[0].GetXSize() * array[0].GetYSize();
	int Col_ImageCount = array.GetSize();
	DoubleMatrix output(Row_ImageSize, Col_ImageCount);

	for (int c = 0; c < Col_ImageCount; c++)
	{
		DoubleMatrix buffer = ImageToDoubleVector(array[c]);
		output.SetColVector(buffer, c);
	}

	return output;
}

DefImgTempl
FloatMatrix ImageArrayToFloatMatrix(const TemplImageArray &array)
{
	VIP_ASSERT(array.GetSize(), "ImageArrayToFloatMatrix", "Array Size is Zero");

	int Row_ImageSize  = array[0].GetXSize() * array[0].GetYSize();
	int Col_ImageCount = array.GetSize();
	FloatMatrix output(Row_ImageSize, Col_ImageCount);

	for (int c = 0; c < Col_ImageCount; c++)
	{
		FloatMatrix buffer = ImageToFloatVector(array[c]);
		output.SetColVector(buffer, c);
	}

	return output;
}
//@}

/*------------------------------------------------------------------*/
///@name	Not Verified Functions
/*------------------------------------------------------------------*/
//@{

/** Project Image to X Axis
    @param src
    @return CVipImgTempl
*/
DefImgTempl
IntImage ProjectionXAxis(const TemplImage & src)
{
	int width  = src.roiWidth();
	int height = src.roiHeight();
	int xPos   = src.roiXOffset();
	int yPos   = src.roiYOffset();

	IntImage dst(width, 1);

	const T   **ppSrc = src.Get2DBufferConst();
	      int **ppDst = dst.Get2DBuffer();
		  int *pDst  = ppDst[0];

	int XSize = xPos + width;
	int YSize = yPos + height;

	for (int y = yPos; y < YSize; y++)
	{
		const T *pSrc = ppSrc[y];
		for (int x = xPos; x < XSize; x++)
		{
			pDst[x - xPos] += pSrc[x];
		}
	}
	return dst;
}

/** Project Image to Y Axis
    @param src
    @return CVipImgTempl
*/
DefImgTempl
IntImage ProjectionYAxis(const TemplImage & src)
{
	int width  = src.roiWidth();
	int height = src.roiHeight();
	int xPos   = src.roiXOffset();
	int yPos   = src.roiYOffset();

	IntImage dst(1, height);

	const T   **ppSrc = src.Get2DBufferConst();
	      int **ppDst = dst.Get2DBuffer();

	int XSize = xPos + width;
	int YSize = yPos + height;

	for (int y = yPos; y < YSize; y++)
	{
		const T   *pSrc  = ppSrc[y];
		      int *pDst  = ppDst[y - yPos];
		for (int x = xPos; x < XSize; x++)
		{
			*pDst += pSrc[x];
		}
	}
	return dst;
}

/*
///
DefImgTempl
FloatImage Hilbert(TemplImage & input, double theta = PI)
{
	// "row" & "col" must be an integer power of 2
	int fftXSize =(int) pow(2.0, (double)(_logb((double)(input.GetXSize() - 1)) + 1));
	int fftYSize =(int) pow(2.0, (double)(_logb((double)(input.GetYSize() - 1)) + 1));

	FloatImage fftbuffer = FFT(input, fftXSize, fftYSize);

	int centerX = fftXSize / 2;
	int centerY = fftYSize / 2;

	float cc =(float) cos(theta);
	float dd =(float) sin(theta);

	for (int x = 0; x < fftXSize; x++)
	{
		for (int y = 0; y < fftYSize; y++)
		{
			if ((x > centerX && y > centerY) ||(x < centerX && y < centerY))
			{
				// ac - bd + (cb + ad) i;
				float aa = fftbuffer(x, y, 0);
				float bb = fftbuffer(x, y, 1);

				fftbuffer(x, y, 0) = aa * cc - bb * dd;
				fftbuffer(x, y, 1) = cc * bb + aa * dd;
			}
			else if (x == centerX || y == centerY)
			{
				fftbuffer(x, y, 0) = 0;
				fftbuffer(x, y, 1) = 0;
			}
			else
			{
				float aa = fftbuffer(x, y, 0);
				float bb = fftbuffer(x, y, 1);

				fftbuffer(x, y, 0) = aa * cc + bb * dd;
				fftbuffer(x, y, 1) = cc * bb - aa * dd;
			}
		}
	}

	fftbuffer = IFFT(fftbuffer);

	FloatImage output(input.GetXSize(), input.GetYSize(), 1);

	for (x = 0; x < input.GetXSize(); x++)
	{
		for (int y = 0; y < input.GetYSize(); y++)
			output(x, y, 0) = fftbuffer(x, y, 0);
	}

	return Output;
};

*/
//@}


//_CONSOLE,_MBCS,
// WIN32,NDEBUG,_MBCS,_LIB

#endif // _IMAGE_FUNCTIONS_H_
