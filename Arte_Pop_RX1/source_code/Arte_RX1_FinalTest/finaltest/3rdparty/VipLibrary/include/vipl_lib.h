//////////////////////////////////////////////////////////////////////
//
// vipl.h: Include All Header & Library of Vip Library
//
//////////////////////////////////////////////////////////////////////

/** @file vipl.h */

#if !defined(_VIPL_Include)
#define _VIPL_Include


#include "./VipLib/TemplPoint.h"
#include "./VipLib/PointFunction.h"
#include "./VipLib/VipImgTempl.h"
#include "./VipLib/VipImgFunc.h"
#include "./VipLib/Matrix.h"
#include "./VipLib/MatrixFunction.h"

#ifdef _LINUX

#else

#ifndef _WIN32_WCE
	#ifdef _DEBUG
		#pragma comment(lib, "VipLibDLLD.lib")
	#else
		#pragma comment(lib, "ViplibDLL.lib")
	#endif
#else
	#ifdef _DEBUG
		#pragma comment(lib, "VipLibCED.lib")
	#else
		#pragma comment(lib, "VipLibCE.lib")
	#endif
#endif

#endif // end of _LINUX

#endif
