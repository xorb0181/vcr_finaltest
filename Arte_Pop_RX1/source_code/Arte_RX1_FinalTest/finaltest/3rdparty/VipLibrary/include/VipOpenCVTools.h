// VipOpenCVTools.h: interface for the CVipOpenCVTools class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_VipOpenCVFunc_H__2697E3DE_72D1_4338_805C_FE48B1188BC0__INCLUDED_)
#define AFX_VipOpenCVFunc_H__2697E3DE_72D1_4338_805C_FE48B1188BC0__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "vipl.h"
#include "./VipOpenCVTools/VipOpenCVFunc.h"

#ifdef _LINUX

#else
	#ifdef _DEBUG
		#pragma comment(lib, "VipOpenCVToolsDLLD.lib")
	#else
		#pragma comment(lib, "VipOpenCVToolsDLL.lib")
	#endif
#endif

#endif // !defined(AFX_VipOpenCVFunc_H__2697E3DE_72D1_4338_805C_FE48B1188BC0__INCLUDED_)


