#ifndef IMAGE_UTIL_HPP_
#define IMAGE_UTIL_HPP_

#include <cv.h>
#include <cxcore.h>
#include <cvaux.h>
#include <highgui.h>
#include "jpeg_decoder.h"

template<class T> 
class Image
{
  private:
	  IplImage* imgp;
  public:
	  Image(IplImage* img=0) {imgp=img;}
	  ~Image(){imgp=0;}
	  void operator=(IplImage* img) {imgp=img;}
	  inline T* operator[](const int rowIndx) {
		  return ((T *)(imgp->imageData + rowIndx*imgp->widthStep));}
};

typedef struct{
	unsigned char b,g,r;
} RgbPixel;

typedef struct{
	float b,g,r;
} RgbPixelFloat;

typedef Image<RgbPixel>       RgbImage;
typedef Image<RgbPixelFloat>  RgbImageFloat;
typedef Image<unsigned char>  BwImage;
typedef Image<float>          BwImageFloat;

//void rotate180( IplImage * dstImg )
//{
//	IplImage * srcImg = cvCloneImage( dstImg );
//	RgbImage rgb_src( srcImg );
//	RgbImage rgb_dst( dstImg );
//	
//	for( int u(0); u<320; u++ )
//		for( int v(0); v<240; v++ )
//		{
//			rgb_dst[240-1-v][320-1-u] = rgb_src[v][u];
//		}
//				
//		cvReleaseImage( &srcImg );
//}


#endif // IMAGE_UTIL_HPP_