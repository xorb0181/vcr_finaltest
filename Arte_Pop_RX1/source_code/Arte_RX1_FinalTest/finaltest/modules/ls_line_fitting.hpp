#ifndef LEAST_SQUARE_LINE_FITTING_HPP_
#define LEAST_SQUARE_LINE_FITTING_HPP_


// y = ax + c
// ax +by + b = 0;
class LSLineFitting
{
public:
	typedef struct pointSample_t
	{
		double x;
		double y;
	}pointSample;

	LSLineFitting() {}
	~LSLineFitting()
	{
	}

	double fit( const pointSample * sampleIncoming, const int numberOfData )
	{
		double n = static_cast<double>(numberOfData);
	
		double x2(0), x(0), y(0), xy(0);
		double tx, ty;
		for( int i(0); i<numberOfData; i++ )
		{
			tx = sampleIncoming[i].x;
			ty = sampleIncoming[i].y;
			x2 += (tx*tx);
			x += (tx);
			y += (ty);
			xy += (tx*ty);
		}
		
		a =(n*xy-x*y)/(n*x2-x*x);
		//c =(-x*xy+x2*y)/(n*x2-x*x);
		c = (y - a * x) / (n);
		b = -1.0;
		A = -b/a;
		C = -c/a;
		B = -1;

		return evaluate( sampleIncoming, numberOfData );
	}	

	double evaluate(  const pointSample * sampleIncoming, const int numberOfData )
	{
		double sum_of_distance(0);
		if( fabs(c) < fabs(C) )
		{
			double a2b2( a*a + b*b );
			for( int i(0); i<numberOfData; i++ )
			{
				sum_of_distance += ( fabs(a*sampleIncoming[i].x+b*sampleIncoming[i].y+c) / (a2b2) );
			}
		}
		else
		{
			double a2b2( A*A + B*B );
			for( int i(0); i<numberOfData; i++ )
			{
				sum_of_distance += ( fabs(A*sampleIncoming[i].y+B*sampleIncoming[i].x+C) / (a2b2) );
			}

		}

		return sum_of_distance;
	}

	double getAngle()
	{
		return atan2( 1.0, -A/B );
	}

public:
	int num_of_samples;
	double a;
	double b;
	double c;
	double A;
	double C;
	double B; 
};



#endif