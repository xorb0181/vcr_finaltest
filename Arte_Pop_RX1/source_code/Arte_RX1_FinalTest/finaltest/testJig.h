// testJig.h : main header file for the TESTJIG application
//

#if !defined(AFX_TESTJIG_H__76F54164_C94D_4103_ABE0_6EB17AAF1202__INCLUDED_)
#define AFX_TESTJIG_H__76F54164_C94D_4103_ABE0_6EB17AAF1202__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols

/////////////////////////////////////////////////////////////////////////////
// CTestJigApp:
// See testJig.cpp for the implementation of this class
//

class CTestJigApp : public CWinApp
{
public:
	CTestJigApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CTestJigApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(CTestJigApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TESTJIG_H__76F54164_C94D_4103_ABE0_6EB17AAF1202__INCLUDED_)
