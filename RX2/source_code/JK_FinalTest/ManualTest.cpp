#include "stdafx.h"
#include "ManualTest.h"


CManualTest::CManualTest()
	: b_dust_sensor(0)
	, b_dust_bin(0)
	, b_top_door(0)
	, b_dc_jack(0)
	, m_test_state(0)
{
}


CManualTest::~CManualTest()
{
}

void CManualTest::initialize()
{
	m_test_state = 0;
	b_dust_sensor = 0;
	b_dust_bin = 0;
	b_top_door = 0;
	b_dc_jack = 0;
	for (int i = 0; i < 5; i++)
	{
		b_top_bumper[i] = 0;
	}
}
bool CManualTest::Process(msg4queue msg)
{
	if (msg.body[0] > 1 && msg.body[0] < 6)
	{
		if (m_test_state == 0)
		{
			DustSensor_Test(msg);
		}
		else if (m_test_state == 1)
		{
			DustBin_Test(msg);
		}
		else if (m_test_state == 2)
		{
			TopDoor_Test(msg);
		}
		else if (m_test_state == 3)
		{
			TopBumper_Test(msg);
		}

		if (m_test_state == 4)
		{
			return true;
		}
	}
	else
	{
		printf("NO!!!!!!! final test confuse test mode!!!!!!!!!\n");
		return true;
	}
	return false;
}


int CManualTest::GetDustSensorResult()
{
	return b_dust_sensor;
}
int CManualTest::GetDustBinResult()
{
	return b_dust_bin;
}
int CManualTest::GetTopdoorResult()
{
	return b_top_door;
}
int CManualTest::GetDcjackResult()
{
	return b_dc_jack;
}
int* CManualTest::GetTopbumperResult()
{
	return b_top_bumper;
}


void CManualTest::DustSensor_Test(msg4queue& msg)
{
	return;
}
void CManualTest::DustBin_Test(msg4queue& msg)
{
	return;
}
void CManualTest::TopDoor_Test(msg4queue& msg)
{
	return;
}
void CManualTest::DcJack_Test()
{
	return;
}
void CManualTest::TopBumper_Test(msg4queue& msg)
{
	return;
}