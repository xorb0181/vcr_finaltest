
// JK_FinalTestDlg.cpp : 구현 파일
//

#include "stdafx.h"
#include "JK_FinalTest.h"
#include "JK_FinalTestDlg.h"
#include "afxdialogex.h"
#include "SerialComm.h"
#include "GyroTest.h"
#include "jpgd.h"
#include <fstream>
#include "RawSocket.h"
#ifdef _DEBUG
#define new DEBUG_NEW
#endif

CCommThread *comth = NULL;
CJK_FinalTestDlg *jk_test = NULL;
int portNum = 1;
bool bOpenPort = false;
int test_state(0);
void NotifyResult(int control_num, int status_flag);
bool GetCheckBoxValue(int num);
int SendCommand(const char* command, int length);
void SetFocusOnFab();
bool wait_for_ssid = false;

bool check_ssid = false;
std::string input_password;
bool locker_fab = false;


class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

	// 대화 상자 데이터입니다.
	enum { IDD = IDD_ABOUTBOX };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);

	// 구현입니다.
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


CJK_FinalTestDlg::CJK_FinalTestDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CJK_FinalTestDlg::IDD, pParent)
	, b_test_start(false)
	, m_check_cam_cal(TRUE)
	, m_check_version(TRUE)
	, m_check_dust_sensor(TRUE)
	, m_check_dust_bin(TRUE)
	, m_check_top_door(TRUE)
	, m_check_dc_jack(TRUE)
	, m_check_top_bumper(TRUE)
	, m_check_psd_cal(TRUE)
	, m_check_motor(TRUE)
	, m_check_ir_psd(TRUE)
	, m_check_bumper(TRUE)
	, m_check_magnet(TRUE)
	, m_check_base(TRUE)
	, m_check_battery(TRUE)
	, m_check_rtc(TRUE)
	, m_check_gyro(TRUE)
	, main_model(0)
	, sub_model(0)
	, cb_ping_counter(0)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	jk_test = this;
	total_test.SetResultCallback(NotifyResult);
	total_test.SetCheckBoxCallback(GetCheckBoxValue);
	total_test.SetSendCallback(SendCommand);
	total_test.SetFocusCallback(SetFocusOnFab);
}

void CJK_FinalTestDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EDIT_VERSION, m_edit_version);
	DDX_Control(pDX, IDC_EDIT_FW, m_edit_fw);
	DDX_Control(pDX, IDC_EDIT_UI, m_edit_ui);
	DDX_Control(pDX, IDC_EDIT_SENSOR, m_edit_sensor);
	DDX_Control(pDX, IDC_EDIT_HW, m_edit_hw);
	DDX_Control(pDX, IDC_EDIT_PCB, m_edit_pcb);
	DDX_Control(pDX, IDC_EDIT_MODEL_MAIN, m_edit_model_main);
	DDX_Control(pDX, IDC_EDIT_MODEL_SUB, m_edit_model_sub);
	DDX_Control(pDX, IDC_EDIT_FAB_NUM, m_edit_fab_num);
	DDX_Control(pDX, IDC_EDIT_PORT, m_edit_port);
	DDX_Check(pDX, IDC_CHECK_CAM_CAL, m_check_cam_cal);
	DDX_Check(pDX, IDC_CHECK_VERSION, m_check_version);
	DDX_Check(pDX, IDC_CHECK_DUST_SENSOR, m_check_dust_sensor);
	DDX_Check(pDX, IDC_CHECK_DUSTBIN, m_check_dust_bin);
	DDX_Check(pDX, IDC_CHECK_TOP_DOOR, m_check_top_door);
	DDX_Check(pDX, IDC_CHECK_DC_JACK, m_check_dc_jack);
	DDX_Check(pDX, IDC_CHECK_TOP_BUMPER, m_check_top_bumper);
	DDX_Check(pDX, IDC_CHECK_PSD_CAL, m_check_psd_cal);
	DDX_Check(pDX, IDC_CHECK_MOTOR, m_check_motor);
	DDX_Check(pDX, IDC_CHECK_IR_PSD, m_check_ir_psd);
	DDX_Check(pDX, IDC_CHECK_BUMPER, m_check_bumper);
	DDX_Check(pDX, IDC_CHECK_MAGNET, m_check_magnet);
	DDX_Check(pDX, IDC_CHECK_BASE, m_check_base);
	DDX_Check(pDX, IDC_CHECK_BATTERY, m_check_battery);
	DDX_Check(pDX, IDC_CHECK_RTC, m_check_rtc);
	DDX_Check(pDX, IDC_CHECK_GYRO, m_check_gyro);
	DDX_Control(pDX, IDC_PIC_VERSION, m_pic_version);
	DDX_Control(pDX, IDC_PIC_FW, m_pic_fw);
	DDX_Control(pDX, IDC_PIC_UI, m_pic_ui);
	DDX_Control(pDX, IDC_PIC_SENSOR, m_pic_sensor);
	DDX_Control(pDX, IDC_PIC_HW, m_pic_hw);
	//  DDX_Control(pDX, IDC_PIC_PCB, m_pic_pcb);
	DDX_Control(pDX, IDC_PIC_RTC, m_pic_rtc);
	DDX_Control(pDX, IDC_PIC_CHARGER, m_pic_charger);
	//  DDX_Control(pDX, IDC_PIC_DUSTBIN, m_pic_dustbin);
	DDX_Control(pDX, IDC_PIC_DUST_BIN, m_pic_dust_bin);
	//  DDX_Control(pDX, IDC_PIC_DC_JACK, m_pic_dc_jack);
	DDX_Control(pDX, IDC_PIC_TOP_BUMPER_R, m_pic_top_bumper_r);
	DDX_Control(pDX, IDC_PIC_TOP_BUMPER_C, m_pic_top_bumper_c);
	DDX_Control(pDX, IDC_PIC_TOP_BUMPER_L, m_pic_top_bumper_l);
	DDX_Control(pDX, IDC_PIC_PSD_CAL, m_pic_psd_cal);
	DDX_Control(pDX, IDC_PIC_BOTTOM_PSD_R, m_pic_bottom_psd_r);
	DDX_Control(pDX, IDC_PIC_BOTTOM_PSD_C, m_pic_bottom_psd_c);
	DDX_Control(pDX, IDC_PIC_BOTTOM_PSD_L, m_pic_bottom_psd_l);
	DDX_Control(pDX, IDC_PIC_MAIN_BRUSH, m_pic_main_brush);
	DDX_Control(pDX, IDC_PIC_SIDE_BRUSH_R, m_pic_side_brush_r);
	DDX_Control(pDX, IDC_PIC_SIDE_BRUSH_L, m_pic_side_brush_l);
	DDX_Control(pDX, IDC_PIC_VACUUM, m_pic_vacuum);
	DDX_Control(pDX, IDC_PIC_FRONT_IR, m_pic_front_ir);
	DDX_Control(pDX, IDC_PIC_SIDE_IR_R1, m_pic_side_ir_r1);
	DDX_Control(pDX, IDC_PIC_SIDE_IR_R2, m_pic_side_ir_r2);
	DDX_Control(pDX, IDC_PIC_SIDE_IR_L1, m_pic_side_ir_l1);
	DDX_Control(pDX, IDC_PIC_SIDE_IR_L2, m_pic_side_ir_l2);
	DDX_Control(pDX, IDC_PIC_SIDE_PSD_R, m_pic_side_psd_r);
	DDX_Control(pDX, IDC_PIC_SIDE_PSD_L, m_pic_side_psd_l);
	DDX_Control(pDX, IDC_PIC_DOCKING_IR, m_pic_docking_ir);
	DDX_Control(pDX, IDC_PIC_WHEEL_L, m_pic_wheel_l);
	DDX_Control(pDX, IDC_PIC_WHEEL_R, m_pic_wheel_r);
	DDX_Control(pDX, IDC_PIC_ERR_F, m_pic_err_f);
	DDX_Control(pDX, IDC_PIC_ERR_B, m_pic_err_b);
	DDX_Control(pDX, IDC_PIC_HALL, m_pic_hall);
	DDX_Control(pDX, IDC_PIC_DOCKING, m_pic_docking);
	DDX_Control(pDX, IDC_PIC_CHARGING, m_pic_charging);
	DDX_Control(pDX, IDC_PIC_GYRO, m_pic_gyro);
	DDX_Control(pDX, IDC_PIC_RESULT, m_pic_result);
	DDX_Control(pDX, IDC_PIC_TOP_DOOR, m_pic_top_door);
	DDX_Control(pDX, IDC_PIC_DUST_SENSOR, m_pic_dust_sensor);
	DDX_Control(pDX, IDC_PIC_SIDE_ARM_R, m_pic_side_arm_r);
	DDX_Control(pDX, IDC_PIC_SIDE_ARM_L, m_pic_side_arm_l);
	DDX_Control(pDX, IDC_PIC_BUMPER_C, m_pic_bumper_c);
	DDX_Control(pDX, IDC_PIC_BUMPER_L, m_pic_bumper_l);
	DDX_Control(pDX, IDC_PIC_BUMPER_R, m_pic_bumper_r);
	DDX_Control(pDX, IDC_PIC_GYRO_AVAILABLE, m_pic_gyro_available);
	DDX_Control(pDX, IDC_PIC_WIFI, m_pic_wifi);
	DDX_Control(pDX, IDC_PIC_CAM_PARAM, m_pic_cam_param);
	DDX_Control(pDX, IDC_EDIT_U0, m_edit_u0);
	DDX_Control(pDX, IDC_EDIT_V0, m_edit_v0);
	DDX_Control(pDX, IDC_EDIT_SSID, m_edit_ssid);
	DDX_Control(pDX, IDC_EDIT_PW, m_edit_pw);
	DDX_Control(pDX, IDC_PIC_FABRICATION, m_pic_fabrication);
}

BEGIN_MESSAGE_MAP(CJK_FinalTestDlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_BUTTON_CONNECT, &CJK_FinalTestDlg::OnBnClickedButtonConnect)
	ON_BN_CLICKED(IDC_BUTTON_START, &CJK_FinalTestDlg::OnBnClickedButtonStart)
	ON_BN_CLICKED(IDC_CHECK_CAM_CAL, &CJK_FinalTestDlg::OnCheckCamCal)
	ON_BN_CLICKED(IDC_CHECK_VERSION, &CJK_FinalTestDlg::OnCheckVersion)
	ON_BN_CLICKED(IDC_CHECK_DUST_SENSOR, &CJK_FinalTestDlg::OnCheckDustSensor)
	ON_BN_CLICKED(IDC_CHECK_DUSTBIN, &CJK_FinalTestDlg::OnCheckDustbin)
	ON_BN_CLICKED(IDC_CHECK_TOP_DOOR, &CJK_FinalTestDlg::OnCheckTopDoor)
	ON_BN_CLICKED(IDC_CHECK_DC_JACK, &CJK_FinalTestDlg::OnCheckDcJack)
	ON_BN_CLICKED(IDC_CHECK_TOP_BUMPER, &CJK_FinalTestDlg::OnCheckTopBumper)
	ON_BN_CLICKED(IDC_CHECK_PSD_CAL, &CJK_FinalTestDlg::OnCheckPsdCal)
	ON_BN_CLICKED(IDC_CHECK_MOTOR, &CJK_FinalTestDlg::OnCheckMotor)
	ON_BN_CLICKED(IDC_CHECK_IR_PSD, &CJK_FinalTestDlg::OnCheckIrPsd)
	ON_BN_CLICKED(IDC_CHECK_BUMPER, &CJK_FinalTestDlg::OnCheckBumper)
	ON_BN_CLICKED(IDC_CHECK_MAGNET, &CJK_FinalTestDlg::OnCheckMagnet)
	ON_BN_CLICKED(IDC_CHECK_BASE, &CJK_FinalTestDlg::OnCheckBase)
	ON_BN_CLICKED(IDC_CHECK_BATTERY, &CJK_FinalTestDlg::OnCheckBattery)
	ON_BN_CLICKED(IDC_CHECK_RTC, &CJK_FinalTestDlg::OnCheckRtc)
	ON_BN_CLICKED(IDC_CHECK_GYRO, &CJK_FinalTestDlg::OnCheckGyro)
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_BUTTON_CHECK_ALL, &CJK_FinalTestDlg::OnButtonCheckAll)
	ON_BN_CLICKED(IDC_BUTTON_INIT_CHECKBOX, &CJK_FinalTestDlg::OnButtonInitCheckbox)
//	ON_BN_CLICKED(IDC_BUTTON_RESULT, &CJK_FinalTestDlg::OnClickedButtonResult)
	ON_BN_CLICKED(IDC_BUTTON_WIFI_OK, &CJK_FinalTestDlg::OnBnClickedButtonWifiOk)
	ON_BN_CLICKED(IDC_BUTTON_WIFI_FAIL, &CJK_FinalTestDlg::OnBnClickedButtonWifiFail)
	ON_BN_CLICKED(IDC_BUTTON_CHECK_SSID, &CJK_FinalTestDlg::OnBnClickedButtonCheckSsid)
END_MESSAGE_MAP()


// CJK_FinalTestDlg 메시지 처리기

BOOL CJK_FinalTestDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);
	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}
	SetIcon(m_hIcon, TRUE);
	SetIcon(m_hIcon, FALSE);
#if HOMOGRAPHY == 1
	std::string homography_version = "WEBCAM CALIBRATION PROGRAM";
	SetWindowText(homography_version.c_str());
#endif // end homography
#if FABRICATION == 1
	int port_num = GetPrivateProfileInt(_T("system"), "port", 0, config_location);
	SetDlgItemInt(IDC_EDIT_PORT, port_num);
	std::string fabrication_version = "FABRICATION REGISTRATION PROGRAM";
	SetWindowText(fabrication_version.c_str());

#endif // end fabrication
#if FINAL_TEST == 1
	std::string program_version = "JK_TEST VER 1.0.5";
	SetWindowText(program_version.c_str());
	/**************개발자 정의 초기화 루틴****************/
	ini_location = ".\\diag_standard_rx2.ini";
	Init_CheckBox();
	int port_num = GetPrivateProfileInt(_T("system"), "port", 0, config_location);
	SetDlgItemInt(IDC_EDIT_PORT, port_num);
	int sw_version = GetPrivateProfileInt("VERSION_", "default", 0, ini_location.c_str());
	int fw_version = GetPrivateProfileInt("VER___FW", "default", 0, ini_location.c_str());
	int ui_version = GetPrivateProfileInt("VER___UI", "default", 0, ini_location.c_str());
	int ss_version = GetPrivateProfileInt("VER___SS", "default", 0, ini_location.c_str());
	int hw_version = GetPrivateProfileInt("VER___HW", "default", 0, ini_location.c_str());
	SetDlgItemInt(IDC_EDIT_VERSION, sw_version);
	SetDlgItemInt(IDC_EDIT_FW, fw_version);
	SetDlgItemInt(IDC_EDIT_UI, ui_version);
	SetDlgItemInt(IDC_EDIT_SENSOR, ss_version);
	SetDlgItemInt(IDC_EDIT_HW, hw_version);
	SetDlgItemInt(IDC_EDIT_PCB, 0);

	GetDlgItem(IDC_EDIT_SSID)->EnableWindow(false);
	GetDlgItem(IDC_EDIT_SSID)->ShowWindow(false);
	GetDlgItem(IDC_EDIT_PW)->EnableWindow(false);
	GetDlgItem(IDC_EDIT_PW)->ShowWindow(false);
	GetDlgItem(IDC_BUTTON_CHECK_SSID)->EnableWindow(false);
	GetDlgItem(IDC_BUTTON_CHECK_SSID)->ShowWindow(false);
	GetDlgItem(IDC_STATIC_SSID)->ShowWindow(false);
	GetDlgItem(IDC_STATIC_PW)->ShowWindow(false);
	GetDlgItem(IDC_STATIC_FABRICATION)->ShowWindow(false);
	GetDlgItem(IDC_PIC_FABRICATION)->EnableWindow(false);
	GetDlgItem(IDC_PIC_FABRICATION)->ShowWindow(false);

	main_model = GetPrivateProfileInt("main_model", "default", 0, ini_location.c_str());
	sub_model = GetPrivateProfileInt("sub_model", "default", 0, ini_location.c_str());
	SetDlgItemInt(IDC_EDIT_MODEL_MAIN, main_model);
	SetDlgItemInt(IDC_EDIT_MODEL_SUB, sub_model);
	total_test.SetModel(main_model, sub_model);
	Initialize();
	if (main_model == 3)
	{
		if (total_test.gyro_test.homography_exist == 1)
		{
			int camera_param = GetPrivateProfileInt("system", "cam_num", 0, config_location);
			total_test.gyro_test.vc.open(camera_param);
			if (total_test.gyro_test.vc.isOpened())
			{
				NotifyResult(5,4);
				total_test.gyro_test.vc.release();
			}
			else
			{
				NotifyResult(5,2);
			}
		}
		else
		{
			NotifyResult(5,2);
		}
		if (sub_model == 0)
		{
			GetDlgItem(IDC_STATIC_DUST_SENSOR)->EnableWindow(true);
			GetDlgItem(IDC_STATIC_TOP_DOOR)->EnableWindow(false);
			GetDlgItem(IDC_STATIC_DUST_SENSOR)->ShowWindow(true);
			// we do not use top door sensor anymore
			GetDlgItem(IDC_STATIC_TOP_DOOR)->ShowWindow(false);
			m_pic_dust_sensor.ShowWindow(true);
			m_pic_top_door.ShowWindow(false);
			m_pic_dust_sensor.EnableWindow(true);
			m_pic_top_door.EnableWindow(false);
		}
		else if (sub_model == 1)
		{
			GetDlgItem(IDC_STATIC_DUST_SENSOR)->EnableWindow(false);
			GetDlgItem(IDC_STATIC_TOP_DOOR)->EnableWindow(false);
			GetDlgItem(IDC_STATIC_DUST_SENSOR)->ShowWindow(false);
			GetDlgItem(IDC_STATIC_TOP_DOOR)->ShowWindow(false);
			m_pic_dust_sensor.ShowWindow(false);
			m_pic_top_door.ShowWindow(false);
			m_pic_dust_sensor.EnableWindow(false);
			m_pic_top_door.EnableWindow(false);
		}
	}
	else
	{
		AfxMessageBox("[LoadINI]1 main model load error");
		exit(1);
	}
	/*****************************************************/
#endif // end final_test
	return TRUE;
}

void CJK_FinalTestDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

void CJK_FinalTestDlg::Initialize()
{
	printf("[Dlg] Initialize dialog...!\n");
	//msg 초기화.
	for (int i = 0; i < cmd_msg.size(); i++)
	{
		cmd_msg.pop();
	}
	total_test.SetModel(main_model, sub_model);
	if (b_test_start)
	{
		SetDlgItemTextA(IDC_BUTTON_START, "START");
		b_test_start = false;
		KillTimer(1);
	}
	enable_counter = false;
	time_counter = 0;
	GetDlgItem(IDC_EDIT_FAB_NUM)->SetFocus();

	if (main_model == 3)
	{
		if (sub_model == 0)
		{
			GetDlgItem(IDC_STATIC_DUST_SENSOR)->EnableWindow(true);
			GetDlgItem(IDC_STATIC_TOP_DOOR)->EnableWindow(true);
			GetDlgItem(IDC_STATIC_DUST_SENSOR)->ShowWindow(true);
			GetDlgItem(IDC_STATIC_TOP_DOOR)->ShowWindow(true);
			m_pic_dust_sensor.ShowWindow(true);
			m_pic_top_door.ShowWindow(true);
			m_pic_dust_sensor.EnableWindow(true);
			m_pic_top_door.EnableWindow(true);
		}
		else if (sub_model == 1)
		{
			GetDlgItem(IDC_STATIC_DUST_SENSOR)->EnableWindow(false);
			GetDlgItem(IDC_STATIC_TOP_DOOR)->EnableWindow(false);
			GetDlgItem(IDC_STATIC_DUST_SENSOR)->ShowWindow(false);
			GetDlgItem(IDC_STATIC_TOP_DOOR)->ShowWindow(false);
			m_pic_dust_sensor.ShowWindow(false);
			m_pic_top_door.ShowWindow(false);
			m_pic_dust_sensor.EnableWindow(false);
			m_pic_top_door.EnableWindow(false);
		}
	}
	else
	{
		AfxMessageBox("[LoadINI]2 main model load error ");
		exit(1);
	}
}
void CJK_FinalTestDlg::Init_CheckBox()
{
	printf("[Dlg] Init CheckBox\n");
	CheckDlgButton(IDC_CHECK_BASE, FALSE);
	CheckDlgButton(IDC_CHECK_BATTERY, FALSE);
	CheckDlgButton(IDC_CHECK_BUMPER, FALSE);
	CheckDlgButton(IDC_CHECK_CAM_CAL, FALSE);
	CheckDlgButton(IDC_CHECK_DC_JACK, FALSE);
	CheckDlgButton(IDC_CHECK_DUSTBIN, FALSE);
	CheckDlgButton(IDC_CHECK_DUST_SENSOR, FALSE);
	CheckDlgButton(IDC_CHECK_GYRO, FALSE);
	CheckDlgButton(IDC_CHECK_IR_PSD, FALSE);
	CheckDlgButton(IDC_CHECK_MAGNET, FALSE);
	CheckDlgButton(IDC_CHECK_MOTOR, FALSE);
	CheckDlgButton(IDC_CHECK_PSD_CAL, FALSE);
	CheckDlgButton(IDC_CHECK_RTC, FALSE);
	CheckDlgButton(IDC_CHECK_TOP_BUMPER, FALSE);
	CheckDlgButton(IDC_CHECK_TOP_DOOR, FALSE);
	CheckDlgButton(IDC_CHECK_VERSION, FALSE);
	UpdateData(TRUE);
}

void CJK_FinalTestDlg::OnButtonCheckAll()
{
	printf("[Dlg] Select all checkbox\n");
	CheckDlgButton(IDC_CHECK_BASE, TRUE);
	CheckDlgButton(IDC_CHECK_BATTERY, TRUE);
	CheckDlgButton(IDC_CHECK_BUMPER, TRUE);
	CheckDlgButton(IDC_CHECK_CAM_CAL, TRUE);
	CheckDlgButton(IDC_CHECK_DC_JACK, TRUE);
	CheckDlgButton(IDC_CHECK_DUSTBIN, TRUE);
	CheckDlgButton(IDC_CHECK_DUST_SENSOR, TRUE);
	CheckDlgButton(IDC_CHECK_GYRO, TRUE);
	CheckDlgButton(IDC_CHECK_IR_PSD, TRUE);
	CheckDlgButton(IDC_CHECK_MAGNET, TRUE);
	CheckDlgButton(IDC_CHECK_MOTOR, TRUE);
	CheckDlgButton(IDC_CHECK_PSD_CAL, TRUE);
	CheckDlgButton(IDC_CHECK_RTC, TRUE);
	CheckDlgButton(IDC_CHECK_TOP_BUMPER, TRUE);
	CheckDlgButton(IDC_CHECK_TOP_DOOR, TRUE);
	CheckDlgButton(IDC_CHECK_VERSION, TRUE);
	UpdateData(TRUE);
}

void CJK_FinalTestDlg::OnButtonInitCheckbox()
{
	Init_CheckBox();
}

void CJK_FinalTestDlg::OnBnClickedButtonConnect()
{
	printf("try connect..");
	if (bOpenPort){
		printf("disconnecting....");
		comth->ClosePort();
		delete comth;
		bOpenPort = false;
		GetDlgItem(IDC_BUTTON_CONNECT)->SetWindowTextA("CONNECT");
		Initialize();
		return;
	}
	CString strPort;
	portNum = GetDlgItemInt(IDC_EDIT_PORT);
	comth = new CCommThread();
	bOpenPort = comth->OpenPort(portNum);
	WritePrivateProfileString(_T("system"), _T("port"), std::to_string(portNum).c_str(), config_location);
	if (!bOpenPort){
		AfxMessageBox("연결 실패! 포트번호와 연결을 다시한번 확인해주세요");
		printf("연결 실패! 포트번호와 연결을 다시한번 확인해주세요");
	}
	else{
		printf("success!");
		GetDlgItem(IDC_BUTTON_CONNECT)->SetWindowTextA("DISCONNECT");
	}
	/*
	printf("[Dlg] Try connect..\n");
	if (comth == NULL)
	{
	CString strPort;
	int portNum = GetDlgItemInt(IDC_EDIT_PORT);
	comth = new CCommThread();
	WritePrivateProfileString(_T("system"), _T("port"), std::to_string(portNum).c_str(), config_location);
	comth->OpenPort(portNum);
	if (!comth->m_bConnected){
	AfxMessageBox("연결 실패! 포트번호와 연결을 다시한번 확인해주세요");
	printf("[Dlg] 연결 실패! 포트번호와 연결을 다시한번 확인해주세요");
	comth->ClosePort();
	delete comth;
	}
	else{
	printf("[Dlg] Success!");
	GetDlgItem(IDC_BUTTON_CONNECT)->SetWindowTextA("DISCONNECT");
	}
	}
	else
	{
	printf("[Dlg] Disconnecting....");
	comth->ClosePort();
	delete comth;
	GetDlgItem(IDC_BUTTON_CONNECT)->SetWindowTextA("CONNECT");
	Initialize();
	}
	*/
}

void CJK_FinalTestDlg::OnBnClickedButtonStart()
{
#if HOMOGRAPHY == 1
	CGyroTest for_homography;
	if (for_homography.Pre_Process(true))
	{
		AfxMessageBox("Finish Web Cam Calibration !");
	}

#endif // define HOMOGRAPHY
#if FABRICATION == 1
	if (!b_test_start)
	{
		SetDlgItemTextA(IDC_BUTTON_START, "STOP");
		b_test_start = true;
		//msg 초기화.
		for (int i = 0; i < cmd_msg.size(); i++)
		{
			cmd_msg.pop();
		}
		//test start
		SetTimer(1, 50, NULL);
		GetDlgItem(IDC_EDIT_FAB_NUM)->SetFocus();
	}
	else
	{
		SetDlgItemTextA(IDC_BUTTON_START, "START");
		b_test_start = false;
		KillTimer(1);
	}
#endif
#if FINAL_TEST == 1
	static BOOL bContinue = TRUE;
	if (!b_test_start)
	{
		if (!(m_check_cam_cal || m_check_version || m_check_dust_sensor || m_check_dust_bin || m_check_top_door || m_check_dc_jack || m_check_top_bumper || m_check_psd_cal || m_check_motor || m_check_ir_psd || m_check_bumper || m_check_magnet || m_check_base || m_check_battery || m_check_rtc || m_check_gyro))
		{
			AfxMessageBox("하나 이상의 테스트를 선택해야 합니다.");
			return;
		}
		SetDlgItemTextA(IDC_BUTTON_START, "STOP");
		b_test_start = true;
		//msg 초기화.
		for (int i = 0; i < cmd_msg.size(); i++)
		{
			cmd_msg.pop();
		}
		//test start
		SetTimer(1, 50, NULL);
		GetDlgItem(IDC_EDIT_FAB_NUM)->SetFocus();

		//ping thread start
		bContinue = TRUE;
		CWinThread *pThread = ::AfxBeginThread(ping_thread, &bContinue);
	}
	else
	{
		SetDlgItemTextA(IDC_BUTTON_START, "START");
		b_test_start = false;
		KillTimer(1);
		bContinue = FALSE;
	}
#endif // define FINAL_TEST
	
}


void CJK_FinalTestDlg::OnCheckCamCal()
{
	m_check_cam_cal = !m_check_cam_cal;
}


void CJK_FinalTestDlg::OnCheckVersion()
{
	m_check_version = !m_check_version;
}


void CJK_FinalTestDlg::OnCheckDustSensor()
{
	m_check_dust_sensor = !m_check_dust_sensor;
}


void CJK_FinalTestDlg::OnCheckDustbin()
{
	m_check_dust_bin = !m_check_dust_bin;
}


void CJK_FinalTestDlg::OnCheckTopDoor()
{
	m_check_top_door = !m_check_top_door;
}


void CJK_FinalTestDlg::OnCheckDcJack()
{
	m_check_dc_jack = !m_check_dc_jack;
}


void CJK_FinalTestDlg::OnCheckTopBumper()
{
	m_check_top_bumper = !m_check_top_bumper;
}


void CJK_FinalTestDlg::OnCheckPsdCal()
{
	m_check_psd_cal = !m_check_psd_cal;
}


void CJK_FinalTestDlg::OnCheckMotor()
{
	m_check_motor = !m_check_motor;
}


void CJK_FinalTestDlg::OnCheckIrPsd()
{
	m_check_ir_psd = !m_check_ir_psd;
}


void CJK_FinalTestDlg::OnCheckBumper()
{
	m_check_bumper = !m_check_bumper;
}


void CJK_FinalTestDlg::OnCheckMagnet()
{
	m_check_magnet = !m_check_magnet;
}


void CJK_FinalTestDlg::OnCheckBase()
{
	m_check_base = !m_check_base;
}


void CJK_FinalTestDlg::OnCheckBattery()
{
	m_check_battery = !m_check_battery;
}


void CJK_FinalTestDlg::OnCheckRtc()
{
	m_check_rtc = !m_check_rtc;
}


void CJK_FinalTestDlg::OnCheckGyro()
{
	m_check_gyro = !m_check_gyro;
}

void CJK_FinalTestDlg::OnTimer(UINT_PTR nIDEvent)
{
#if FABRICATION == 1
	if (!locker_fab)
	{
		locker_fab = true;
		if (cmd_msg.size() != 0)
		{
			msg4queue packet = cmd_msg.pop();
			// ssid
			if (packet.msg_type == 1)
			{
				std::string ssid = "Miele Scout RX2";
				//ssid = Miele Scout RX2
				if (check_ssid)
				{
					if (packet.body[0] == 1)
					{
						SetDlgItemText(IDC_EDIT_SSID, ssid.c_str());
					}
					else
					{
						ssid = "fail";
						SetDlgItemText(IDC_EDIT_SSID, ssid.c_str());
					}
				}
			}
			//password
			else if (packet.msg_type == 2)
			{

				// show pass or fail
				if (check_ssid)
				{
					std::string password = to_string(packet.body[0]);
					SetDlgItemText(IDC_EDIT_PW, password.c_str());
					//pass
					if (password == input_password)
					{
						NotifyResult(48, 4);
					}
					//fail
					else
					{
						NotifyResult(48, 2);
					}
				}
				//check_ssid = false;
			}
			//booting
			else if (packet.msg_type == 3)
			{
				SetDlgItemText(IDC_EDIT_SSID, " ");
				SetDlgItemText(IDC_EDIT_PW, " ");

				NotifyResult(48, 3);
				Sleep(1000);
				std::string root = "root\n";
				SendCommand(root.c_str(), root.size());
				Sleep(1000);
				std::string get_hostapd = "cat /etc/hostapd.conf\n";
				SendCommand(get_hostapd.c_str(), get_hostapd.size());
			}
		}
		locker_fab = false;
	}
#endif
#if FINAL_TEST == 1
	static bool timer_locker = false;
	static bool ping_locker = false;
	if (nIDEvent == 1)
	{
		if (wait_for_ssid)
		{
			static int cnt_ssid = 0;
			std::cout << "!!!!!!!!!! >>" << cnt_ssid << std::endl;
			if (++cnt_ssid > 150)
			{
				//change ssid to Final_test8
				wait_for_ssid = false;
				cnt_ssid = 0;
				std::string ssid_8 = "send [JK_TEST]0 2 100 8 \n";
				SendCommand(ssid_8.c_str(), ssid_8.size());
			}
		}
		if (!timer_locker)
		{
			timer_locker = true;
			test_state = total_test.getTestState();
			cv::Mat left;
			cv::Mat right;
			if (!left_image.empty())
			{
				printf("left....\n");
				decodedJpegImage((char *)left_image.data(), left);
				cv::imshow("camera_left", left);
				cv::waitKey(5);
				total_test.image_left_flag = true;
				left_image.clear();
			}
			if (!right_image.empty())
			{
				printf("right....\n");
				decodedJpegImage((char *)right_image.data(), right);
				cv::imshow("camera_right", right);
				cv::waitKey(5);
				total_test.image_right_flag = true;
				right_image.clear();
			}
			if (total_test.image_left_flag && total_test.image_right_flag)
			{
				printf("both of right and left!!!\n");
				std::string kill_image_server = "killall image_server\n";
				SendCommand(kill_image_server.c_str(), kill_image_server.size());
				Sleep(10);
				std::string on_rx2 = "cd /home/yroot/;./run_rx2 &\n";
				SendCommand(on_rx2.c_str(), on_rx2.size());
				Sleep(10);
				std::string on_robot_node = "cd /home/connections;./robot_node &\n";
				SendCommand(on_robot_node.c_str(), on_robot_node.size());
				left_image.clear();
				right_image.clear();
				wait_for_ssid = true;
				image_read_flag_left = false;
				image_read_flag_right = false;
				total_test.image_left_flag = false;
				total_test.image_right_flag = false;
				timer_locker = false;
				return;
			}

			if (cmd_msg.size() != 0)
			{
				msg4queue packet = cmd_msg.pop();

				//gyro angle info
				if (packet.msg_type == 4)
				{
					total_test.gyro_test.SetPose(packet.msg_number, packet.body[0] / 100000.0);
					timer_locker = false;
					return;
				}
				else if (packet.msg_type == 99)
				{
					float u0 = packet.body[1];
					float v0 = packet.body[0];
					
					u0 = u0 / 1000;
					v0 = v0 / 1000;

					u0 -= 64;
					v0 = 240 - (v0 - (336 - 240) / 2) - 1;

					float ru0 = 160.0f;
					float rv0 = 120.0f;
					 
					float eu = ru0 - u0;
					float ev = rv0 - v0;
					float error_norm = std::sqrt(eu*eu + ev*ev);
					if (error_norm < 25)
					{
						std::cout << std::endl << std::endl << "u0 : " << u0 << " v0 : " << v0 << "error norm : " << error_norm << std::endl << std::endl;
						NotifyResult(47, 4);
						string u0_string = to_string(u0);
						string v0_string = to_string(v0);
						SetDlgItemText(IDC_EDIT_U0, u0_string.c_str());
						SetDlgItemText(IDC_EDIT_V0, v0_string.c_str());
					}
					else
					{
						NotifyResult(47, 2);
					}
				}
				total_test.Process(packet, true);
			}
			else
			{
				msg4queue packet;
				packet.msg_type = 10;
				total_test.Process(packet, false);
				timer_locker = false;
				return;
			}
			timer_locker = false;
		}
	}
#endif
}

void NotifyResult(int control_num, int status_flag)
{
	HBITMAP hbit;
	switch (status_flag)
	{
		//3i_off
	case 0:
		hbit = ::LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_BITMAP_3I_OFF));
		break;
		//3i_on
	case 1:
		hbit = ::LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_BITMAP_3I_ON));
		break;
		//fail
	case 2:
		hbit = ::LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_BITMAP_FAIL));
		break;
		//pending
	case 3:
		hbit = ::LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_BITMAP_PENDING));
		break;
		//success
	case 4:
		hbit = ::LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_BITMAP_SUCCESS));
		break;
		//res_pending
	case 5:
		hbit = ::LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_BITMAP_RES_PENDING));
		break;
		//res_success
	case 6:
		hbit = ::LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_BITMAP_RES_SUCCESS));
		break;
		//res_fail
	case 7:
		hbit = ::LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_BITMAP_RES_FAIL));
		break;
	default:
		hbit = ::LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_BITMAP_PENDING));
		break;
	}
	switch (control_num)
	{
	case 0:
		jk_test->m_pic_version.SetBitmap(hbit);
		break;
	case 1:
		jk_test->m_pic_fw.SetBitmap(hbit);
		break;
	case 2:
		jk_test->m_pic_ui.SetBitmap(hbit);
		break;
	case 3:
		jk_test->m_pic_sensor.SetBitmap(hbit);
		break;
	case 4:
		jk_test->m_pic_hw.SetBitmap(hbit);
		break;
	case 5:
		jk_test->m_pic_gyro_available.SetBitmap(hbit);
		break;
	case 6:
		jk_test->m_pic_rtc.SetBitmap(hbit);
		break;
	case 7:
		jk_test->m_pic_charger.SetBitmap(hbit);
		break;
	case 8:
		jk_test->m_pic_dust_bin.SetBitmap(hbit);
		break;
	case 9:
		jk_test->m_pic_top_door.SetBitmap(hbit);
		break;
	case 10:
		jk_test->m_pic_bumper_l.SetBitmap(hbit);
		break;
	case 11:
		jk_test->m_pic_top_bumper_r.SetBitmap(hbit);
		break;
	case 12:
		jk_test->m_pic_top_bumper_c.SetBitmap(hbit);
		break;
	case 13:
		jk_test->m_pic_top_bumper_l.SetBitmap(hbit);
		break;
	case 14:
		jk_test->m_pic_psd_cal.SetBitmap(hbit);
		break;
	case 15:
		jk_test->m_pic_bottom_psd_r.SetBitmap(hbit);
		break;
	case 16:
		jk_test->m_pic_bottom_psd_c.SetBitmap(hbit);
		break;
	case 17:
		jk_test->m_pic_bottom_psd_l.SetBitmap(hbit);
		break;
	case 18:
		jk_test->m_pic_main_brush.SetBitmap(hbit);
		break;
	case 19:
		jk_test->m_pic_side_brush_r.SetBitmap(hbit);
		break;
	case 20:
		jk_test->m_pic_side_brush_l.SetBitmap(hbit);
		break;
	case 21:
		jk_test->m_pic_vacuum.SetBitmap(hbit);
		break;
	case 22:
		jk_test->m_pic_front_ir.SetBitmap(hbit);
		break;
	case 23:
		jk_test->m_pic_side_ir_r1.SetBitmap(hbit);
		break;
	case 24:
		jk_test->m_pic_side_ir_r2.SetBitmap(hbit);
		break;
	case 25:
		jk_test->m_pic_side_ir_l1.SetBitmap(hbit);
		break;
	case 26:
		jk_test->m_pic_side_ir_l2.SetBitmap(hbit);
		break;
	case 27:
		jk_test->m_pic_side_psd_r.SetBitmap(hbit);
		break;
	case 28:
		jk_test->m_pic_side_psd_l.SetBitmap(hbit);
		break;
	case 29:
		jk_test->m_pic_docking_ir.SetBitmap(hbit);
		break;
	case 30:
		jk_test->m_pic_wheel_l.SetBitmap(hbit);
		break;
	case 31:
		jk_test->m_pic_wheel_r.SetBitmap(hbit);
		break;
	case 32:
		jk_test->m_pic_err_b.SetBitmap(hbit);
		break;
	case 33:
		jk_test->m_pic_err_f.SetBitmap(hbit);
		break;
	case 34:
		jk_test->m_pic_hall.SetBitmap(hbit);
		break;
	case 35:
		jk_test->m_pic_docking.SetBitmap(hbit);
		break;
	case 36:
		jk_test->m_pic_charging.SetBitmap(hbit);
		break;
	case 37:
		jk_test->m_pic_gyro.SetBitmap(hbit);
		break;
	case 38:
		jk_test->m_pic_dust_sensor.SetBitmap(hbit);
		break;
	case 39:
		jk_test->m_pic_side_arm_r.SetBitmap(hbit);
		break;
	case 40:
		jk_test->GetDlgItem(IDC_EDIT_FAB_NUM)->SetWindowTextA("");
		break;
	case 41:
		jk_test->m_pic_side_arm_l.SetBitmap(hbit);
		break;
	case 42:
		jk_test->m_pic_bumper_r.SetBitmap(hbit);
		break;
	case 43:
		jk_test->m_pic_bumper_c.SetBitmap(hbit);
		break;
	case 44:
		jk_test->SetDlgItemTextA(IDC_EDIT_RESULT,"");
		break;
	case 45:
		jk_test->m_pic_result.SetBitmap(hbit);
		break;
	case 46:
		jk_test->m_pic_wifi.SetBitmap(hbit);
		break;
	case 47:
		jk_test->m_pic_cam_param.SetBitmap(hbit);
		break;
	case 48:
		jk_test->m_pic_fabrication.SetBitmap(hbit);
	default:
		break;
	}
}

bool GetCheckBoxValue(int num)
{
	bool return_val = false;
	switch (num)
	{
	case 0:
		return_val = jk_test->m_check_cam_cal;
		break;
	case 1:
		return_val = jk_test->m_check_version;
		break;
	case 2:
		return_val = jk_test->m_check_dust_sensor;
		break;
	case 3:
		return_val = jk_test->m_check_dust_bin;
		break;
	case 4:
		return_val = jk_test->m_check_top_door;
		break;
	case 5:
		return_val = jk_test->m_check_dc_jack;
		break;
	case 6:
		return_val = jk_test->m_check_top_bumper;
		break;
	case 7:
		return_val = jk_test->m_check_psd_cal;
		break;
	case 8:
		return_val = jk_test->m_check_motor;
		break;
	case 9:
		return_val = jk_test->m_check_ir_psd;
		break;
	case 10:
		return_val = jk_test->m_check_bumper;
		break;
	case 11:
		return_val = jk_test->m_check_magnet;
		break;
	case 12:
		return_val = jk_test->m_check_base;
		break;
	case 13:
		return_val = jk_test->m_check_battery;
		break;
	case 14:
		return_val = jk_test->m_check_rtc;
		break;
	case 15:
		return_val = jk_test->m_check_gyro;
		break;
	}
	return return_val;
}

int SendCommand(const char* command, int length)
{
	BYTE *buff;
	int return_val = 0;
	buff = new BYTE[length];
	for (int i = 0; i < length; i++){
		buff[i] = command[i];
	}
	return_val = comth->WriteComm(buff, length);
	// causion ! memory leakage... please delete buff at this line...
	return return_val;
}

void SetFocusOnFab()
{
	jk_test->GetDlgItem(IDC_EDIT_FAB_NUM)->SetFocus();
}

BOOL CJK_FinalTestDlg::PreTranslateMessage(MSG* pMsg)
{
	// 키가 눌리면
	if (pMsg->message == WM_KEYDOWN)
	{
		//게다가 그 키가 Enter라면
		if (pMsg->wParam == VK_RETURN)
		{
			//게다가 그 키가 IDC_EDIT_FAB_NUM가 포커스를 가질 때 눌렸다면
			if (pMsg->hwnd == GetDlgItem(IDC_EDIT_FAB_NUM)->GetSafeHwnd())
			{
				//GetDlgItem(IDC_EDIT_FAB_NUM)->SetFocus();
				CString fab_num;
				GetDlgItemText(IDC_EDIT_FAB_NUM,fab_num);
				int length = fab_num.GetLength();
				string real_fab_num;
				string login_string = "root\n";
				GetDlgItem(IDC_EDIT_FAB_NUM)->SetWindowTextA("");
				SendCommand(login_string.c_str(), login_string.size());
				Sleep(100);
				input_password.clear();
				for (int i = 0; i < length; i++)
				{
					real_fab_num += fab_num[i];
					if (i != length - 1)
					{
						input_password += fab_num[i];
					}
					
				}
				real_fab_num = "send [FAB]" + real_fab_num + " \n";
				SendCommand(real_fab_num.c_str() , real_fab_num.size());
				check_ssid = true;
			}
			
			//그 키가 IDC_EDIT1가 포커스를 가질 때 눌렸다면
			else
			{
				OnOK();
			}
			
			return FALSE; // FALSE를 리턴하면 Enter를 무시하게 된다.
		}
	}
	return CDialogEx::PreTranslateMessage(pMsg);
}


void CJK_FinalTestDlg::OnBnClickedButtonWifiOk()
{
	msg4queue packet;
	packet.msg_type = 5;  // event
	packet.body[0] = 1; // wifi check
	packet.body[1] = 1; // ok
	packet.body_size = 2;

	cmd_msg.push(packet);
	return;
}


void CJK_FinalTestDlg::OnBnClickedButtonWifiFail()
{
	msg4queue packet;
	packet.msg_type = 5;  // event
	packet.body[0] = 1; // wifi check
	packet.body[1] = 0; // fail
	packet.body_size = 2;

	cmd_msg.push(packet);
	return;
}

int CJK_FinalTestDlg::decodedJpegImage(char * compressedImagePtr, cv::Mat & gimg)
{
	//   std::cout << "compressedImagePtr = " << std::endl;
	//   for( int i(0); i<10; i++ )
	//   {
	//     printf("%02x", static_cast<unsigned char>(compressedImagePtr[i]) );
	//   }
	//   std::cout << std::endl;

	int uncomp_width = 0, uncomp_height = 0, uncomp_actual_comps = 0, uncomp_req_comps = 1;
	int comp_size = 320 * 240;
	unsigned char * uncom_img = jpgd::decompress_jpeg_image_from_memory((unsigned char*)compressedImagePtr,
		comp_size,
		&uncomp_width,
		&uncomp_height,
		&uncomp_actual_comps,
		uncomp_req_comps);

	if (uncomp_height != 240 || uncomp_width != 320)
	{
		std::cout << "void decodedJpegImage( char * compressedImagePtr, cv::Mat & gimg ); failed by wrong image size" << std::endl;
		std::cout << uncomp_width << " x " << uncomp_height << std::endl;
		return -1;
	}

	cv::Mat tmp_gimg(uncomp_height, uncomp_width, CV_8U, uncom_img);
	tmp_gimg.copyTo(gimg);
	free(uncom_img);

	return 0;
}

UINT CJK_FinalTestDlg::ping_thread(LPVOID pParam)
{
	BOOL *pbContinue = (BOOL *)pParam;
	while (pbContinue)
	{
		Sleep(1000);
		if (test_state >= 11 && test_state < 18 && ping_result < 10)
		{
			//ping test
			CString sDestAddress;
			WSADATA wsaData;

			int nRet = WSAStartup(MAKEWORD(2, 2), &wsaData);	// 귀찮아서 MAKEWORD(1,1)을 쓰지 않고 바로 0x0101 로 WORD 형 자료를 완성.
			if (nRet)
			{
				TRACE(_T("WSAStartup() 오류 발생했습니다. 코드 번호 : %d\n"));
				WSACleanup();
				continue;

			}

			HINSTANCE hIcmp = LoadLibrary(_T("ICMP.DLL"));

			if (hIcmp == 0)
			{
				TRACE(_T("error\n"));
				continue;
			}

			sDestAddress = "10.0.0.1";

			// Look up an IP address for the given host name
			hostent* phe;
			phe = gethostbyname(_T("10.0.0.1"));
			//phe = gethostbyname(sDestAddress);
			if (phe == 0)
			{
				TRACE(_T("Could not find IP address for\n"));
				continue;
			}

			// Get handles to the functions inside ICMP.DLL that we'll need
			typedef HANDLE(WINAPI* pfnHV)(VOID);
			typedef BOOL(WINAPI* pfnBH)(HANDLE);
			typedef DWORD(WINAPI* pfnDHDPWPipPDD)(HANDLE, DWORD, LPVOID, WORD, PIP_OPTION_INFORMATION, LPVOID, DWORD, DWORD); // evil, no?
			pfnHV pIcmpCreateFile;
			pfnBH pIcmpCloseHandle;
			pfnDHDPWPipPDD pIcmpSendEcho;

			pIcmpCreateFile = (pfnHV)GetProcAddress(hIcmp, "IcmpCreateFile");
			pIcmpCloseHandle = (pfnBH)GetProcAddress(hIcmp, "IcmpCloseHandle");
			pIcmpSendEcho = (pfnDHDPWPipPDD)GetProcAddress(hIcmp, "IcmpSendEcho");

			if ((pIcmpCreateFile == 0) || (pIcmpCloseHandle == 0) || (pIcmpSendEcho == 0))
			{
				TRACE(_T("Failed to get proc addr for function.\n"));
				FreeLibrary((HINSTANCE)hIcmp);
				continue;
			}

			// Open the ping service
			HANDLE hIP = pIcmpCreateFile();
			if (hIP == INVALID_HANDLE_VALUE) {
				TRACE(_T("Unable to open ping service.\n"));
				continue;
			}

			// Build ping packet
			char acPingBuffer[64];
			memset(acPingBuffer, '\xAA', sizeof(acPingBuffer));
			PIP_ECHO_REPLY pIpe = (PIP_ECHO_REPLY)GlobalAlloc(GMEM_FIXED | GMEM_ZEROINIT, sizeof(IP_ECHO_REPLY) + sizeof(acPingBuffer));

			if (pIpe == 0)
			{
				TRACE(_T("Failed to allocate global ping packet buffer.\n"));
				continue;
			}
			pIpe->Data = acPingBuffer;
			pIpe->DataSize = sizeof(acPingBuffer);

			// Send the ping packet
			DWORD dwStatus = pIcmpSendEcho(hIP, *((DWORD*)phe->h_addr_list[0]),
				acPingBuffer, sizeof(acPingBuffer), NULL, pIpe,
				sizeof(IP_ECHO_REPLY) + sizeof(acPingBuffer), 50);

			CString sLog;
			if (dwStatus != 0)
			{
				sLog.Format(_T("[Wifi Test]Addr:%d.%d.%d.%d BYTE:%d RTT:%dms TTL:%d"),
					int(LOBYTE(LOWORD(pIpe->Address))),
					int(HIBYTE(LOWORD(pIpe->Address))),
					int(LOBYTE(HIWORD(pIpe->Address))),
					int(HIBYTE(HIWORD(pIpe->Address))),
					pIpe->DataSize,
					int(pIpe->RoundTripTime),
					int(pIpe->Options.Ttl));
				ping_result++;
			}
			else
			{
				sLog.Format(_T("Error obtaining info from ping packet."));
			}
			//TRACE(sLog);

			//result
			std::cout << sLog << std::endl;
			//m_ListLog.AddString(sLog);
			//m_ListLog.SetCurSel(m_ListLog.GetCount() - 1);

			// Shut down...
			GlobalFree(pIpe);
			FreeLibrary(hIcmp);

			WSACleanup();
		}
		else
		{
			Sleep(1000);
		}
	}
	return 0;
}



void CJK_FinalTestDlg::OnBnClickedButtonCheckSsid()
{
	GetDlgItem(IDC_BUTTON_CHECK_SSID)->EnableWindow(false);
	std::string root = "root\n";
	SendCommand(root.c_str(), root.size());
	Sleep(100);
	std::string get_hostapd = "cat /etc/hostapd.conf\n";
	SendCommand(get_hostapd.c_str(), get_hostapd.size());
}
