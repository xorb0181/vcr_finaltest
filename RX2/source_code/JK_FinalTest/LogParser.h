#pragma once
#include <string>
#include <iostream>
class CLogParser
{
public:
	CLogParser();
	~CLogParser();
	void Initialize();
	void MakeALine(char character);
	bool parser();
	int CharToInt(const char* input_line, int& start_pos);
	double CharToDouble(const char* input_line, int& start_pos);
	int GetMessageNumber();
	void SetMessageNumber(int message_number);
private:
	std::vector <unsigned char> is_image;
	std::string line;
	msg4queue msg;
	int msg_num;
	bool verbose;
public:
	void clear_msg();
};

