#pragma once

#include <queue>
#include <mutex>




class message
{
	std::string m_message;
	int    m_number;

public:
	message(const char* _message, int number)
		: m_message(_message), m_number(number) 
	{
	}
	~message() 
	{
	}
	const char* getMessage()
	{
		return m_message.c_str();
	}
	int getNumber() 
	{
		return m_number; 
	}
};




template <typename T> class concurrent_queue
{
public:
	concurrent_queue()
	{
	}
	void push(T data)
	{
		locker.lock();
		_queue_.push(data);
		locker.unlock();
	}

	T pop()
	{
		locker.lock();
		T popped_data;
		popped_data = _queue_.front();
		_queue_.pop();
		locker.unlock();
		return popped_data;
	}
	int size()
	{
		int queue_size = _queue_.size();
		return queue_size;
	}
private:
	std::queue<T> _queue_;
	std::mutex locker;
};