#include "stdafx.h"
#include "LogParser.h"


CLogParser::CLogParser()
	:verbose(true)
{
}


CLogParser::~CLogParser()
{
}


void CLogParser::MakeALine(char character)
{
	if (character != '\n')
	{
		line += character;
		is_image.push_back(character);
		return;
	}
	else
	{
		//end of line
		int res = parser();
		if (verbose)
		{
			std::cout << line << std::endl;
		}
		else
		{
			verbose = true;
		}
		if (res)
		{
			cmd_msg.push(msg);
			line.clear();
			clear_msg();
		}
		else
		{
			line.clear();
			clear_msg();
		}
	}
}
// if line is valid packet, return true and make message.
// Msg type
// 1 : data packet   2 : booting
// 3 : acknowledge   4 : gyro data   5 : event
bool CLogParser::parser()
{
	int data_start = 0;
	int no_data = is_image.size();
	if (line.size() > 0)
	{
		const char* temp_line = line.c_str();
#if FABRICATION == 1

		if (line.find("[BOOT_FINISH]") != std::string::npos)
		{
			int body_size = 1;
			int cmd_num = 0;

			int header_size = 4;

			msg.msg_number = msg_number;
			msg.msg_type = 3;
			msg.body_size = 1;
			msg.body[0] = 0;

			return true;
		}

		int start_ssid = line.find("ssid=");
		int start_wpa = line.find("wpa_passphrase=");
		if (start_ssid != std::string::npos)
		{
			msg.body_size = 1;
			msg.msg_type = 1;
			if (line.find("Miele Scout RX2") != std::string::npos)
			{
				msg.body[0] = 1; // pass
			}
			else
			{
				msg.body[0] = 0; // fail
			}
			return true;
		}
		else if (start_wpa != std::string::npos)
		{
			msg.body_size = 1;
			msg.msg_type = 2;
			std::string divided_three;
			divided_three.assign(line, start_wpa + 15, 9);
			msg.body[0] = std::stoi(divided_three);
			printf("%d >>> %d\n", 0, msg.body[0]);
			return true;
		}
#endif
#if FINAL_TEST == 1
		if (line.find("[BOOT_FINISH]") != std::string::npos)
		{
				int body_size = 1;
				int cmd_num = 0;

				int header_size = 4;

				msg.msg_number = msg_number;
				msg.msg_type = 2;
				msg.body_size = 1;
				msg.body[0] = 0;

				return true;
		}
		//we can know that [JK_TEST] is data packet
		else if ((data_start =line.find("[JK_TEST]")) != std::string::npos)
		{
			if (line.find("send") == std::string::npos)
			{
				int iter = 9 + data_start;
				int msg_num = CharToInt(temp_line, iter);
				int body_size = CharToInt(temp_line, iter);
				int msg_type = 1;

				msg.msg_number = msg_num;
				msg.msg_type = 1;
				msg.body_size = body_size;
				for (int i = 0; i < body_size; i++)
				{
					msg.body[i] = CharToInt(temp_line, iter);
				}
				return true;
			}
		}
		//we can know that [ACK is acknowledge packet
		else if ((data_start = line.find("[ACK]")) != std::string::npos)
		{
			int iter = 5+data_start;
			int msg_num = CharToInt(temp_line, iter);
			
			msg.msg_type = 3;
			msg.msg_number = msg_num;
			msg.body_size = CharToInt(temp_line, iter);
			for (int i = 0; i < msg.body_size; i++)
			{
				msg.body[i] = CharToInt(temp_line, iter);
			}
			return true;
		}
		else if ((data_start = line.find("[GYRO_POSE]")) != std::string::npos)
		{
			int iter = data_start + 11;
			msg.msg_type = 4;
			msg.msg_number = CharToInt(temp_line, iter);
			msg.body_size = 1;
			msg.body[0] = CharToDouble(temp_line, iter) * 100000;
			return true;
		}
		else if ((data_start = line.find("u0, v0: [")) != std::string::npos)
		{
			int iter = data_start + 9;
			msg.msg_type = 99;
			msg.body_size = 2;
			msg.body[0] = CharToDouble(temp_line, iter) * 1000;
			std::cout << "!!!!!!!!!!!!!!!!!!!!!!!!!   >> " << msg.body[0] << std::endl;
			msg.body[1] = CharToDouble(temp_line, iter) * 1000;
			std::cout << "!!!!!!!!!!!!!!!!!!!!!!!!!   >> " << msg.body[1] << std::endl;
			return true;
		}
		else if (is_image[0] == '[' && is_image[1] == 'e' && is_image[2] =='x')
		{
			verbose = false;
			if (!left_image.empty() && !right_image.empty())
			{
				return false;
			}
			int image_id(0);
			int no_bytes(0);
			sscanf((char *)is_image.data(), "[exportJpeg][%d][%d]", &image_id, &no_bytes);
			std::cout << "iamge_id ::" << (int)image_id << " no_bytes ::" << (int)no_bytes << std::endl;
			// find data section
			int idx_data_start(0);
			int no_bracket(0);
			for (int i(0); i < 100; i++)
			{
				printf("%c", is_image[i]);
				if (is_image[i] == ']')
				{
					if (++no_bracket == 3)
					{
						idx_data_start = i + 2;
						printf("first char = %c \n", is_image[idx_data_start]);
						break;
					}
				}
			}
			    
			std::vector<unsigned char> data;
			no_data -= idx_data_start;
			no_data -= 2;
			for (int i(0); i < no_data; i += 2)
			{
				char hex[3] = { 0, 0, 0 };
				hex[0] = is_image[i + idx_data_start + 0];
				hex[1] = is_image[i + idx_data_start + 1];
				unsigned int value;
				sscanf(hex, "%02x", &value);
				data.push_back(static_cast<unsigned char>(value));
			}

			if (no_bytes == data.size() )
			{
				//left image
				if (image_id == 0 && !image_read_flag_left)
				{
					left_image = data;
					image_read_flag_left = true;
				}
				//right image
				if (image_id == 1 && !image_read_flag_right)
				{
					right_image = data;
					image_read_flag_right = true;
				}
			}
			else
			{
				printf("wrong image size: %lu %d \n", data.size(), no_bytes);
				return false;
			}
		}
#endif
	}
	return false;
}


int CLogParser::CharToInt(const char* input_line, int& start_pos)
{
	int iter = start_pos;
	std::string temp;
	int return_val = 0;
	//for ascii
	while ((input_line[iter] != 32 && input_line[iter] != '\n') && ((input_line[iter] < 58 && input_line[iter] > 47) || input_line[iter] == 45))
	{
		temp += input_line[iter];
		iter++;
	}
	start_pos = iter;
	start_pos++;
	if (temp.size() != 0){
		return_val = std::stoi(temp);
	}
	return return_val;
}

double CLogParser::CharToDouble(const char* input_line, int& start_pos)
{
	int iter = start_pos;
	std::string temp;
	double return_val = 0;
	//for ascii
	while ((input_line[iter] != 32 && input_line[iter] != '\n') && ((input_line[iter] < 58 && input_line[iter] > 47) || input_line[iter] == 45 || input_line[iter] == 46))
	{
		temp += input_line[iter];
		iter++;
	}
	start_pos = iter;
	start_pos++;
	if (temp.size() != 0){
		return_val = std::stod(temp);
	}
	return return_val;
}
int CLogParser::GetMessageNumber()
{
	return msg_num;
}
void CLogParser::SetMessageNumber(int message_number)
{
	msg_num = message_number;
}

void CLogParser::Initialize()
{
	line.clear();
	msg_num = 0;
}


void CLogParser::clear_msg()
{
	for (int i = 0; i < 16; i++)
	{
		msg.body[i] = 0;
	}
	msg.body_size = 0;
	msg.msg_number = 0;
	msg.msg_type = 0;
	is_image.clear();
}
