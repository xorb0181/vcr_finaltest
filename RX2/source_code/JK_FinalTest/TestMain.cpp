#include "stdafx.h"
#include "TestMain.h"
#include "resource.h"
#include <fstream>
enum  enum_self_diagnosis_list
{
	SELF_DIAGNOSIS_STATE_TEST,              //0
	SELF_DIAGNOSIS_STATE_LED,               //1
	SELF_DIAGNOSIS_STATE_DUST_SENSOR,       //2
	SELF_DIAGNOSIS_STATE_DUST_BIN,          //3
	SELF_DIAGNOSIS_STATE_TOP_DOOR,          //4
	SELF_DIAGNOSIS_STATE_SIDE_ARM,          //5
	SELF_DIAGNOSIS_STATE_BOTTOM_PSD,        //6
	SELF_DIAGNOSIS_STATE_MOTOR,             //7
	SELF_DIAGNOSIS_STATE_FRIR_PSD_DOCKING,  //8
	SELF_DIAGNOSIS_STATE_BUMPER,            //9
	SELF_DIAGNOSIS_STATE_BACKWARD,          //10
	SELF_DIAGNOSIS_STATE_FORWARD,           //11
	SELF_DIAGNOSIS_STATE_HALL,              //12
	SELF_DIAGNOSIS_STATE_TOP_BUMPER,        //13
	SELF_DIAGNOSIS_STATE_GOTO_BASE,         //14
	SELF_DIAGNOSIS_STATE_CHARGER,           //15
	SELF_DIAGNOSIS_STATE_BACKWARD_OUT,      //16
	SELF_DIAGNOSIS_STATE_ANGLE,             //17
	SELF_DIAGNOSIS_STATE_RTC,               //18
	SELF_DIAGNOSIS_STATE_SAVE_RESULT,       //19
	SELF_DIAGNOSIS_STATE_GOTO_TESTPOINT,    //20
	SELF_DIAGNOSIS_STATE_END,               //21
};

// we can add state when we need more state
enum enum_internal_state
{
	SELF_DIAGNOSIS_START,
	SELF_DIAGNOSIS_PREPARE,
	SELF_DIAGNOSIS_RUN,
	SELF_DIAGNOSIS_END,
};
CTestMain::CTestMain()
	: m_test_state(0), m_model_main(0), m_model_sub(0), counter(0), m_test_wifi(false),
	result_version(0), result_firmware(0), result_ui(0), result_sensor(0),result_hardware(0),result_dust_sensor(0),result_dust_bin(0),result_top_bumper_r(0),result_top_bumper_c(0),result_top_bumper_l(0),
    result_side_arm_r(0),result_side_arm_l(0),result_bottom_psd_r(0),result_bottom_psd_c(0),result_bottom_psd_l(0),result_psd_cal(0),result_main_brush(0),result_side_brush_r(0),result_side_brush_l(0),
    result_vacuum(0),result_front_ir(0), result_side_ir_r1(0), result_side_ir_r2(0),result_side_ir_l1(0),result_side_ir_l2(0),result_side_psd_r(0),result_side_psd_l(0),result_docking_ir(0),result_bumper_r(0),
	result_bumper_c(0),result_bumper_l(0),result_wheel_r_b(0),result_wheel_l_b(0),result_wheel_l_f(0),result_wheel_r_f(0),result_error_b(0),result_error_f(0),result_hall(0),result_docking(0),result_charging(0),result_rtc(0),result_wifi(0),
	result_gyro(0), result_charger(0), result_total(0)
{
	CTime time = CTime::GetCurrentTime();
	ini_location = ".\\diag_standard_rx2.ini";
	result_location = ".\\result\\log_"+ to_string(time.GetYear()) + to_string(time.GetMonth()) + to_string(time.GetDay()) + ".txt";
	log_location = ".\\debug_log\\log_" + to_string(time.GetYear()) + to_string(time.GetMonth()) + to_string(time.GetDay()) + ".txt";


	//ini에서 읽어온 기준값
	st_dust_bin = GetPrivateProfileInt("dUSt____", "default", 0, ini_location.c_str());
	st_bottom_psd = GetPrivateProfileInt("Bottom_PSD", "default", 0, ini_location.c_str());

	st_dust_sensor.min = GetPrivateProfileInt("dUSt_Sen", "min", 0, ini_location.c_str());
	st_motor_main_brush.min = GetPrivateProfileInt("Brush___", "min", 0, ini_location.c_str());
	st_motor_side_brush.min = GetPrivateProfileInt("SBrush__", "min", 0, ini_location.c_str());
	st_wheel_error_backward = GetPrivateProfileInt("Back_Err", "default", 0, ini_location.c_str());
	st_wheel_error_forward = GetPrivateProfileInt("For__Err", "default", 0, ini_location.c_str());
	st_wheel_current.min = GetPrivateProfileInt("Wheel__C", "min", 0, ini_location.c_str());

	st_dust_sensor.max = GetPrivateProfileInt("dUSt_Sen", "max", 0, ini_location.c_str());
	st_motor_main_brush.max = GetPrivateProfileInt("Brush___", "max", 0, ini_location.c_str());
	st_motor_side_brush.max = GetPrivateProfileInt("SBrush__", "max", 0, ini_location.c_str());
	st_wheel_current.max = GetPrivateProfileInt("Wheel__C", "max", 0, ini_location.c_str());

	st_sw_ver = GetPrivateProfileInt("VERSION_", "default", 0, ini_location.c_str());
	st_fw_ver = GetPrivateProfileInt("VER___FW", "default", 0, ini_location.c_str());
	st_ui_ver = GetPrivateProfileInt("VER___UI", "default", 0, ini_location.c_str());
	st_sensor_ver = GetPrivateProfileInt("VER___SS", "default", 0, ini_location.c_str());
	st_hw_ver = GetPrivateProfileInt("VER___HW", "default", 0, ini_location.c_str());
	st_charging = GetPrivateProfileInt("Charging", "default", 0, ini_location.c_str());
	main_model = GetPrivateProfileInt("main_model", "default", 0, ini_location.c_str());
	sub_model = GetPrivateProfileInt("sub_model", "default", 0, ini_location.c_str());
	wlan = to_string(GetPrivateProfileInt("Wlan", "default", 0, ini_location.c_str()))+" \n";

	if (main_model == 3)
	{
		//prestage
		if (sub_model == 0)
		{
			st_motor_vacuum.min = GetPrivateProfileInt("Vacuum_Prestage", "min", 0, ini_location.c_str());
			st_motor_vacuum.max = GetPrivateProfileInt("Vacuum_Prestage", "max", 0, ini_location.c_str());
		}
		//premium
		else if (sub_model == 1)
		{
			st_motor_vacuum.min = GetPrivateProfileInt("Vacuum_Premium", "min", 0, ini_location.c_str());
			st_motor_vacuum.max = GetPrivateProfileInt("Vacuum_Premium", "max", 0, ini_location.c_str());
		}
	}

	//100 = 50sec
//	timer_dust_sensor = 40;
//	timer_dust_bin = 30;
//	timer_top_door = 30;
//	timer_top_bumper = 30;
	timer_dust_sensor = 50;
	timer_dust_bin = 50;
	timer_top_door = 50;
	timer_top_bumper = 50;
}


CTestMain::~CTestMain()
{
}
void CTestMain::SetResultCallback(callback_1 func)
{
	SetBitmap = func;
}
void CTestMain::SetCheckBoxCallback(callback_2 func)
{
	GetCheck = func;
}
void CTestMain::SetSendCallback(callback_3 func)
{
	SendCmd = func;
}
void CTestMain::SetFocusCallback(callback_4 func)
{
	FocusFab = func;
}
void CTestMain::SetModel(int model_main, int model_sub)
{
	m_model_main = model_main;
	m_model_sub = model_sub;
	return;
}
void CTestMain::Process(msg4queue msg, bool exist_msg)
{
	if (msg.msg_type == 4)
	{
		return;
	}
	if (msg.msg_type == 2 && msg.body_size != 0)
	{
		if (msg.body[0] == 0){
			ResetTest();
		}
	}
	
	//event
	if (msg.msg_type == 5)
	{
		// wifi check
		if (msg.body[0] == 1)
		{
			//fail
			if (msg.body[1] == 0)
			{
				SetBitmap(46, 2);
				m_test_wifi = false;
				result_wifi = 0;
				////InsertResultString(manual_result, 30, 0);
			}
			//ok
			else if (msg.body[0] == 1)
			{
				SetBitmap(46, 4);
				m_test_wifi = true;
				result_wifi = 1;
				//InsertResultString(manual_result, 30, 1);
			}
		}
		return;
	}

	switch (m_test_state)
	{
		//printf("this state is Num :: %d\n", m_test_state);
		//waiting boot log
	case SELF_DIAGNOSIS_STATE_TEST:
		if (msg.msg_type == 2)
		{
			if (init_lock)
			{
				break;
			}
			init_lock = true;
			PreRun(msg.body[0], 0);
			init_lock = false;
		}
		else if (msg.msg_type == 1)
		{
			if (msg.body_size == 3)
			{
				switch (msg.body[1])
				{
				case 0:
					if (msg.body[2] == st_sw_ver)
					{
						SetBitmap(0, 4);
						m_test_version[0] = true;
					}
					else
					{
						SetBitmap(0, 2);
					}
					result_version = msg.body[2];
					//InsertResultString(manual_result, 0, msg.body[2]);
					break;
				case 1:
					if (msg.body[2] == st_fw_ver)
					{
						SetBitmap(1, 4);
						m_test_version[1] = true;
					}
					else
					{
						SetBitmap(1, 2);
					}
					result_firmware = msg.body[2];
					//InsertResultString(manual_result, 3, msg.body[2]);
					break;
				case 2:
					if (msg.body[2] == st_ui_ver)
					{
						SetBitmap(2, 4);
						m_test_version[2] = true;
					}
					else
					{
						SetBitmap(2, 2);
					}
					result_ui = msg.body[2];
					//InsertResultString(manual_result, 6, msg.body[2]);
					break;
				case 3:
					if (msg.body[2] == st_sensor_ver)
					{
						SetBitmap(3, 4);
						m_test_version[3] = true;
					}
					else
					{
						SetBitmap(3, 2);
					}
					result_sensor = msg.body[2];
					//InsertResultString(manual_result, 9, msg.body[2]);
					break;
				case 4:
					if (msg.body[2] == st_hw_ver)
					{
						SetBitmap(4, 4);
						m_test_version[4] = true;
					}
					else
					{
						SetBitmap(4, 2);
					}
					result_hardware = msg.body[2];
					//InsertResultString(manual_result, 12, msg.body[2]);


					for (int i = 0; i < 5; i++)
					{
						if (m_test_version[i] == false)
						{
							SetBitmap(i, 2);
						}
						if (m_model_sub == 0)
						{
							m_test_state = 2;
						}
						else if (m_model_sub == 1)
						{
							m_test_state = 3;
						}
					}
					string cmd = "send [JK_TEST]0 2 100 " + wlan + " \n";
					SendCmd(cmd.c_str(), cmd.size());
					break;
				}
			}
		}
		break;
	case SELF_DIAGNOSIS_STATE_DUST_SENSOR:
		timer_counter++;
		if (timer_counter > timer_dust_sensor * 10)
		{
			printf("[DUST SENSOR] TIME OUT \n");
			SetBitmap(38, 2);
			m_test_dust_sensor = 0;
			m_test_state = SELF_DIAGNOSIS_STATE_DUST_BIN;
			timer_counter = 0;
			result_dust_sensor = 0;
			//InsertResultString(manual_result, 14, 0);
			string cmd = "send [JK_TEST]0 2 2 444 \n";
			SendCmd(cmd.c_str(), cmd.size());
		}
		b_check_dust_sensor = GetCheck(2);
		if (b_check_dust_sensor)
		{
			if (msg.msg_type == 1)
			{
				if (msg.body_size < 2)
				{
					//printf("case %d :: body size error!!!!\n", m_test_state);
					break;
				}
				if (msg.body[0] == 2 && msg.body[1] == st_dust_sensor.min)
				{
					printf("[DUST SENSOR] CALIBRATING \n");
				}
				else if (msg.body[0] == 2 && msg.body[1] == 1)
				{
					//dust sensor 
					printf("[DUST SENSOR] CALIBRATION COMPLETE \n");
					SetBitmap(38, 1);
				}
				else if (msg.body[0] == 2 && msg.body[1] == -1)
				{
					printf("[DUST SENSOR] CALIBRATION FAIL \n");
					SetBitmap(38, 2);
					m_test_dust_sensor = 0;
					m_test_state = SELF_DIAGNOSIS_STATE_DUST_BIN;
					result_dust_sensor = 0;
					//InsertResultString(manual_result, 14, 0);
					timer_counter = 0;
				}
				else if (msg.body[0] == 2 && msg.body[1] > st_dust_sensor.max)
				{
					printf("[DUST SENSOR] SUCCESS \n");
					SetBitmap(38, 4);
					m_test_dust_sensor = 2;
					m_test_state = SELF_DIAGNOSIS_STATE_DUST_BIN;
					result_dust_sensor = 1;
					//InsertResultString(manual_result, 14, 1);
					timer_counter = 0;
				}
			}
			break;
		}
		else
		{
			//printf("this state isn't checked Num :: %d\n", m_test_state);
			m_test_state = SELF_DIAGNOSIS_STATE_DUST_BIN;
		}
		break;
	case SELF_DIAGNOSIS_STATE_DUST_BIN:
		timer_counter++;
		if (timer_counter > timer_dust_bin * 10)
		{
			printf("[DUST BIN] TIME OUT \n");
			SetBitmap(8, 2);
			m_test_dust_bin = false;
			m_test_state = SELF_DIAGNOSIS_STATE_SIDE_ARM;
			timer_counter = 0;
			result_dust_bin = 0;
			//InsertResultString(manual_result, 16, 0);
			string cmd = "send [JK_TEST]0 2 3 444 \n";
			SendCmd(cmd.c_str(), cmd.size());
		}
		b_check_dust_bin = GetCheck(3);
		if (b_check_dust_bin)
		{
			if (msg.msg_type == 1 && msg.body_size != 0)
			{
				if (msg.body_size < 2)
				{
					//printf("case %d :: body size error!!!!\n", m_test_state);
					break;
				}
				if (msg.body[0] == 3 && msg.body[1] == 0)
				{
					printf("[DUST BIN] RELEASE \n");
					SetBitmap(8, 0);
				}
				else if (msg.body[0] == 3 && msg.body[1] == 1)
				{
					printf("[DUST BIN] PUSH \n");
					SetBitmap(8, 1);
				}
				else if (msg.body[0] == 3 && msg.body[1] == -1)
				{
					printf("[DUST BIN] FAIL \n");
					SetBitmap(8, 2);
					m_test_dust_bin = false;
					result_dust_bin = 0;
					//InsertResultString(manual_result, 16, 0);
					timer_counter = 0;
					m_test_state = SELF_DIAGNOSIS_STATE_SIDE_ARM;
				}
				else if (msg.body[0] == 3 && msg.body[1] == 3)
				{
					printf("[DUST BIN] SUCCESS \n");
					SetBitmap(8, 4);
					m_test_dust_bin = true;
					result_dust_bin = 1;
					//InsertResultString(manual_result, 16, 1);
					timer_counter = 0;
					m_test_state = SELF_DIAGNOSIS_STATE_SIDE_ARM;
				}
			}
			break;
		}
		else
		{
			printf("this state isn't checked Num :: %d\n", m_test_state);
			m_test_state = SELF_DIAGNOSIS_STATE_SIDE_ARM;
		}
		break;

	case SELF_DIAGNOSIS_STATE_SIDE_ARM:
		//top bumper & side arm
		timer_counter++;
		if (timer_counter > timer_top_bumper * 10)
		{
			printf("[TOP BUMPER & SIDE ARM] TIME OUT \n");
			// top bumper r
			if (!m_test_top_bumper[2])
			{
				SetBitmap(11, 2);
				result_top_bumper_r = 0;
				//InsertResultString(manual_result, 20, 0);
			}
			// top bumper c
			if (!m_test_top_bumper[1])
			{
				SetBitmap(12, 2);
				result_top_bumper_c = 0;
				//InsertResultString(manual_result, 22, 0);
			}
			//top bumper l
			if (!m_test_top_bumper[0])
			{
				SetBitmap(13, 2);
				result_top_bumper_l = 0;
				//InsertResultString(manual_result, 24, 0);
			}
			if (!m_test_side_arm[0])
			{
				SetBitmap(39, 2);
				result_side_arm_r = 0;
				//InsertResultString(manual_result, 26, 0);
			}
			if (!m_test_side_arm[1])
			{
				SetBitmap(41, 2);
				result_side_arm_l = 0;
				//InsertResultString(manual_result, 28, 0);
			}
			m_test_state = SELF_DIAGNOSIS_STATE_BOTTOM_PSD;
			timer_counter = 0;
			string cmd = "send [JK_TEST]0 2 13 444 \n";
			SendCmd(cmd.c_str(), cmd.size());
			break;
		}
		///////// exception handling
		if (msg.msg_type != 1)
		{
			return;
		}
		if (msg.body[0] != 5)
		{
			printf("[ADMIN] something wrong... command number is not 5 \n");
			return;
		}
		///////// test
		b_check_top_bumper = GetCheck(6);
		if (b_check_top_bumper)
		{
			if (msg.body_size == 3)
			{
				// error handling - already pushed bumper
				if (msg.body[2] == -2)
				{
					switch (msg.body[1])
					{
					case 0:
						printf("[TOP BUMPER R] Fail, -2 \n");
						m_test_top_bumper[0] = false;
						SetBitmap(13, 2);
						result_top_bumper_r = 0;
						//InsertResultString(manual_result, 20, 0);
						break;
					case 1:
						printf("[TOP BUMPER C] Fail, -2 \n");
						m_test_top_bumper[1] = false;
						SetBitmap(12, 2);
						result_top_bumper_c = 0;
						//InsertResultString(manual_result, 22, 0);
						break;
					case 2:
						printf("[TOP BUMPER L] Fail, -2 \n");
						m_test_top_bumper[2] = false;
						SetBitmap(11, 2);
						result_top_bumper_l = 0;
						//InsertResultString(manual_result, 24, 0);
						break;
					case 3:
						printf("[SIDE ARM R] Fail, -2 \n");
						m_test_side_arm[0] = false;
						SetBitmap(39, 2);
						result_side_arm_r = 0;
						//InsertResultString(manual_result, 26, 0);
						break;
					case 4:
						printf("[SIDE ARM L] Fail, -2 \n");
						m_test_side_arm[1] = false;
						SetBitmap(41, 2);
						result_side_arm_l = 0;
						//InsertResultString(manual_result, 28, 0);
						break;
					default:
						printf("[ADMIN] Case 5 :: Error ! Wrong packet from robot \n");
						break;
					}
				}
				// error handling - not release after pushed
				else if (msg.body[2] == -1)
				{
					switch (msg.body[1])
					{
					case 0:
						printf("[TOP BUMPER R] Fail, -1 \n");
						m_test_top_bumper[0] = false;
						SetBitmap(11, 2);
						result_top_bumper_r = 0;
						//InsertResultString(manual_result, 20, 0);
						break;
					case 1:
						printf("[TOP BUMPER C] Fail, -1 \n");
						m_test_top_bumper[1] = false;
						SetBitmap(12, 2);
						result_top_bumper_c = 0;
						//InsertResultString(manual_result, 22, 0);
						break;
					case 2:
						printf("[TOP BUMPER L] Fail, -1 \n");
						m_test_top_bumper[2] = false;
						SetBitmap(13, 2);
						result_top_bumper_l = 0;
						//InsertResultString(manual_result, 24, 0);
						break;
					case 3:
						printf("[SIDE ARM R] Fail, -1 \n");
						m_test_side_arm[0] = false;
						SetBitmap(39, 2);
						result_side_arm_r = 0;
						//InsertResultString(manual_result, 26, 0);
						break;
					case 4:
						printf("[SIDE ARM L] Fail, -1 \n");
						m_test_side_arm[1] = false;
						SetBitmap(41, 2);
						result_side_arm_l = 0;
						//InsertResultString(manual_result, 28, 0);
						break;
					default:
						printf("[ADMIN] Case 5 :: Error ! Wrong packet from robot \n");
						break;
					}
				}
				// process - bumper pushed
				else if (msg.body[2] == 0)
				{
					switch (msg.body[1])
					{
					case 0:
						printf("[TOP BUMPER R] PUSHED, 0 \n");
						SetBitmap(11, 1);
						break;
					case 1:
						printf("[TOP BUMPER C] PUSHED, 0 \n");
						SetBitmap(12, 1);
						break;
					case 2:
						printf("[TOP BUMPER L] PUSHED, 0 \n");
						SetBitmap(13, 1);
						break;
					case 3:
						printf("[SIDE ARM R] PUSHED, 0 \n");
						SetBitmap(39, 1);
						break;
					case 4:
						printf("[SIDE ARM L] PUSHED, 0 \n");
						SetBitmap(41, 1);
						break;
					default:
						printf("[ADMIN] Case 5 :: Error ! Wrong packet from robot \n");
						break;
					}
				}
				// process - bumper released
				else if (msg.body[2] == 1)
				{
					switch (msg.body[1])
					{
					case 0:
						printf("[TOP BUMPER R] SUCCESS, 1 \n");
						m_test_top_bumper[0] = true;
						SetBitmap(11, 4);
						result_top_bumper_r = 1;
						//InsertResultString(manual_result, 20, 1);
						break;
					case 1:
						printf("[TOP BUMPER C] SUCCESS, 1 \n");
						m_test_top_bumper[1] = true;
						SetBitmap(12, 4);
						result_top_bumper_c = 1;
						//InsertResultString(manual_result, 22, 1);
						break;
					case 2:
						printf("[TOP BUMPER L] SUCCESS, 1 \n");
						m_test_top_bumper[2] = true;
						SetBitmap(13, 4);
						result_top_bumper_l = 0;
						//InsertResultString(manual_result, 24, 1);
						break;
					case 3:
						printf("[SIDE ARM R] SUCCESS, 1 \n");
						m_test_side_arm[0] = true;
						SetBitmap(39, 4);
						result_side_arm_r = 1;
						//InsertResultString(manual_result, 26, 1);
						break;
					case 4:
						printf("[SIDE ARM L] SUCCESS, 1 \n");
						m_test_side_arm[1] = true;
						SetBitmap(41, 4);
						result_side_arm_l = 0;
						//InsertResultString(manual_result, 28, 1);
						break;
					default:
						printf("[ADMIN] Case 5 :: Error ! Wrong packet from robot \n");
						break;
					}
				}
				if (m_test_top_bumper[0] && m_test_top_bumper[1] && m_test_top_bumper[2] && m_test_side_arm[0] && m_test_side_arm[1])
				{
					m_test_state = SELF_DIAGNOSIS_STATE_BOTTOM_PSD;
					timer_counter = 0;
					break;
				}
			}
			else
			{
				std::cout << "wrong msg body size!" << std::endl;
			}
		}
		else
		{
			printf("this state isn't checked Num :: %d\n", m_test_state);
			m_test_state = SELF_DIAGNOSIS_STATE_BOTTOM_PSD;
		}
		break;
	case SELF_DIAGNOSIS_STATE_BOTTOM_PSD:
		//bottom psd
		if (exist_msg == false)
		{
			return;
		}
		if (msg.body_size != 0 && m_test_state != msg.body[0])
		{
			printf("[FINAL_TEST] test state do not equal robot state\n");
			printf("[FINAL_TEST] adjust test state.....goto %d\n", msg.body[0]);
			m_test_state = msg.body[0];
			Process(msg, true);
			return;
		}
		b_check_psd_cal = GetCheck(7);
		if (b_check_psd_cal)
		{
			static bool first_run = true;
			if (msg.msg_type == 1 && msg.body_size > 1)
			{
				if (msg.body[0] == 6)
				{
					if (first_run)
					{
						printf("[BOTTOM PSD] FIRST \n");
						st_bottom_psd_l.min = msg.body[1];
						st_bottom_psd_l.max = msg.body[1];

						st_bottom_psd_c.min = msg.body[2];
						st_bottom_psd_c.max = msg.body[2];

						st_bottom_psd_r.min = msg.body[3];
						st_bottom_psd_r.max = msg.body[3];
						first_run = false;
						break;
					}
					//a robot is on the floor
					else if (msg.body[1] == 999)
					{
						if (st_bottom_psd_l.max - st_bottom_psd_l.min > st_bottom_psd)
						{
							printf("[BOTTOM PSD] PSD_L SUCCESS \n");
							SetBitmap(17, 4);
							m_test_bottom_psd++;
							//InsertResultString(auto_result, 4, 1);
						}
						else
						{
							printf("[BOTTOM PSD] PSD_L FAIL \n");
							SetBitmap(17, 2);
							//InsertResultString(auto_result, 4, 0);
						}

						if (st_bottom_psd_c.max - st_bottom_psd_c.min > st_bottom_psd)
						{
							printf("[BOTTOM PSD] PSD_C SUCCESS \n");
							SetBitmap(16, 4);
							m_test_bottom_psd++;
							//InsertResultString(auto_result, 2, 1);
						}
						else
						{
							printf("[BOTTOM PSD] PSD_C FAIL \n");
							SetBitmap(16, 2);
							//InsertResultString(auto_result, 2, 0);
						}

						if (st_bottom_psd_r.max - st_bottom_psd_r.min > st_bottom_psd)
						{
							printf("[BOTTOM PSD] PSD_R SUCCESS \n");
							SetBitmap(15, 4);
							m_test_bottom_psd++;
							//InsertResultString(auto_result, 0, 1);
						}
						else
						{
							printf("[BOTTOM PSD] PSD_R FAIL \n");
							SetBitmap(15, 2);
							//InsertResultString(auto_result, 0, 0);
						}
					}
					//calibration result
					else if (msg.body[1] == 888)
					{
						//cout << "888 들어옴!!!!" << endl;
						if (msg.body[2] == 0)
						{
							if (msg.body[3] >= -21 && msg.body[3] < -15)
							{
								m_test_psd_cal++;
							}
							else
							{
								result_psd_cal = 0;
								SetBitmap(14, 2);
								//InsertResultString(auto_result, 6, 0);
							}
							result_bottom_psd_r = msg.body[3];
							//InsertResultString(psd_cal_result, 0, msg.body[3]);
						}
						else if (msg.body[2] == 1)
						{
							if (msg.body[3] >= -21 && msg.body[3] < -15)
							{
								m_test_psd_cal++;
							}
							else
							{
								SetBitmap(14, 2);
								//InsertResultString(auto_result, 6, 0);
							}
							result_bottom_psd_c = msg.body[3];
							//InsertResultString(psd_cal_result, 4, msg.body[3]);
						}
						else if (msg.body[2] == 2)
						{
							if (msg.body[3] >= -21 && msg.body[3] < -15)
							{
								m_test_psd_cal++;
							}
							else
							{
								SetBitmap(14, 2);
								//InsertResultString(auto_result, 6, 0);
							}
							result_bottom_psd_l = msg.body[3];
							//InsertResultString(psd_cal_result, 8, msg.body[3]);
						}
					}
					else if (msg.body[1] == 444)
					{
				
						if (m_test_psd_cal == 3)
						{
							printf("[BOTTOM PSD] PSD CALIBRATION SUCCESS \n");
							SetBitmap(14, 4);
							//InsertResultString(auto_result, 6, 1);
							first_run = true;
						}
						else
						{
							printf("[BOTTOM PSD] PSD CALIBRATION FAIL \n");
							SetBitmap(14, 2);
							//InsertResultString(auto_result, 6, 0);
							first_run = true;
						}
						m_test_state = SELF_DIAGNOSIS_STATE_MOTOR;
						break;
					}
					// min max
					else
					{
						if (msg.body[1] < st_bottom_psd_l.min)
						{
							st_bottom_psd_l.min = msg.body[1];
						}
						if (msg.body[1] > st_bottom_psd_l.max)
						{
							st_bottom_psd_l.max = msg.body[1];
						}
						if (msg.body[2] < st_bottom_psd_c.min)
						{
							st_bottom_psd_c.min = msg.body[2];
						}
						if (msg.body[2] > st_bottom_psd_c.max)
						{
							st_bottom_psd_c.max = msg.body[2];
						}
						if (msg.body[3] < st_bottom_psd_r.min)
						{
							st_bottom_psd_r.min = msg.body[3];
						}
						if (msg.body[3] > st_bottom_psd_r.max)
						{
							st_bottom_psd_r.max = msg.body[3];
						}
					}
				}
				//printf(" psd_l max :: %d psd_l min :: %d psd_c max :: %d psd_c min :: %d psd_r max :: %d psd_r min :: %d", st_bottom_psd_l.max, st_bottom_psd_l.min, st_bottom_psd_c.max, st_bottom_psd_c.min, st_bottom_psd_r.max, st_bottom_psd_r.min);
			}
			break;
		}
		else
		{
			printf("this state isn't checked Num :: %d\n", m_test_state);
			m_test_state = SELF_DIAGNOSIS_STATE_MOTOR;
		}
		break;
	case SELF_DIAGNOSIS_STATE_MOTOR:
		if (exist_msg == false)
		{
			return;
		}
		if (msg.body_size != 0 && m_test_state != msg.body[0])
		{
			printf("[FINAL_TEST] test state do not equal robot state\n");
			printf("[FINAL_TEST] adjust test state.....goto %d\n", msg.body[0]);
			m_test_state = msg.body[0];
			Process(msg, true);
			return;
		}
		//motor
		b_check_motor = GetCheck(8);
		if (b_check_motor)
		{
			if (msg.msg_type == 1 && msg.body_size == 3)
			{
				if (msg.body[0] == 7)
				{
					switch (msg.body[1])
					{
					case 0:
						if (msg.body[2] > st_motor_main_brush.min && msg.body[2] < st_motor_main_brush.max)
						{
							printf("[MOTOR] MAIN BRUSH SUCCESS %d \n",msg.body[2]);
							m_test_motor[0] = true;
							SetBitmap(18, 4);
							//InsertResultString(auto_result, 8, msg.body[2]);
						}
						else
						{
							printf("[MOTOR] MAIN BRUSH FAIL %d \n", msg.body[2]);
							SetBitmap(18, 2);
							//InsertResultString(auto_result, 8, msg.body[2]);
						}
						result_main_brush = msg.body[2];
						break;
					case 1:
						if (msg.body[2] > st_motor_side_brush.min && msg.body[2] < st_motor_side_brush.max)
						{
							printf("[MOTOR] SIDE BRUSH L SUCCESS %d \n", msg.body[2]);
							m_test_motor[2] = true;
							SetBitmap(20, 4);
							//InsertResultString(auto_result, 15, msg.body[2]);
						}
						else
						{
							printf("[MOTOR] SIDE BRUSH L FAIL %d \n", msg.body[2]);
							SetBitmap(20, 2);
							//InsertResultString(auto_result, 15, msg.body[2]);
						}
						result_side_brush_l = msg.body[2];
						break;
					case 2:
						if (msg.body[2] > st_motor_side_brush.min && msg.body[2] < st_motor_side_brush.max)
						{
							printf("[MOTOR] SIDE BRUSH R SUCCESS %d \n", msg.body[2]);
							m_test_motor[1] = true;
							SetBitmap(19, 4);
							//InsertResultString(auto_result, 12, msg.body[2]);
						}
						else
						{
							printf("[MOTOR] SIDE BRUSH R FAIL %d \n", msg.body[2]);
							SetBitmap(19, 2);
							//InsertResultString(auto_result, 12, msg.body[2]);
						}
						result_side_brush_r = msg.body[2];
						break;
					case 3:
						if (msg.body[2] > st_motor_vacuum.min && msg.body[2] < st_motor_vacuum.max)
						{
							printf("[MOTOR] VACUUM SUCCESS %d \n", msg.body[2]);
							m_test_motor[3] = true;
							SetBitmap(21, 4);
							//InsertResultString(auto_result, 18, msg.body[2]);
						}
						else
						{
							printf("[MOTOR] VACUUM FAIL %d \n", msg.body[2]);
							SetBitmap(21, 2);
							//InsertResultString(auto_result, 18, msg.body[2]);
						}
						result_vacuum = msg.body[2];
						break;
					case 4:
						printf("[MOTOR] FINISH \n");
						if (!m_test_motor[0])
						{
							SetBitmap(18, 2);
							//InsertResultString(auto_result, 8, msg.body[2]);
						}
						if (!m_test_motor[1])
						{
							SetBitmap(19, 2);
							//InsertResultString(auto_result, 12, msg.body[2]);
						}
						if (!m_test_motor[2])
						{
							SetBitmap(20, 2);
							//InsertResultString(auto_result, 15, msg.body[2]);
						}
						if (!m_test_motor[3])
						{
							SetBitmap(21, 2);
							//InsertResultString(auto_result, 18, msg.body[2]);
						}
						m_test_state = SELF_DIAGNOSIS_STATE_FRIR_PSD_DOCKING;
						break;
					}
				}
			}
			break;
		}
		else
		{
			printf("this state isn't checked Num :: %d\n", m_test_state);
			m_test_state = 8;
		}
		break;
	case SELF_DIAGNOSIS_STATE_FRIR_PSD_DOCKING:
		if (exist_msg == false)
		{
			return;
		}
		if (msg.body_size != 0 && m_test_state != msg.body[0])
		{
			printf("[FINAL_TEST] test state do not equal robot state\n");
			printf("[FINAL_TEST] adjust test state.....goto %d\n", msg.body[0]);
			m_test_state = msg.body[0];
			Process(msg, true);
			return;
		}
		//frir psd docking
		b_check_ir_psd = GetCheck(9);
		if (b_check_ir_psd)
		{
			if (msg.body[0] == 8 && msg.msg_type == 1)
			{
				if (msg.body_size < 2)
				{
					printf("case %d :: body size error!!!!\n", m_test_state);
					break;
				}
				switch (msg.body[1])
				{
				case 0:
					if (msg.body[2] == 1)
					{
						printf("[IR & PSD] PSD R SUCCESS \n");
						SetBitmap(27, 4);
						m_test_ir_psd[0] = true;
						result_side_psd_r = 1;
						//InsertResultString(auto_result, 33, 1);
					}
					else
					{
						printf("[IR & PSD] PSD R FAIL \n");
						SetBitmap(27, 2);
						result_side_psd_r = 0;
						//InsertResultString(auto_result, 33, 0);
					}
					break;
				case 1:
					if (msg.body[2] == 1)
					{
						printf("[IR & PSD] IR R2 SUCCESS \n");
						SetBitmap(24, 4);
						m_test_ir_psd[1] = true;
						result_side_ir_r2 = 1;
						//InsertResultString(auto_result, 27, 1);
					}
					else
					{
						printf("[IR & PSD] IR R2 FAIL \n");
						SetBitmap(24, 2);
						result_side_ir_r2 = 0;
						//InsertResultString(auto_result, 27, 0);
					}
					break;
				case 2:
					if (msg.body[2] == 1)
					{
						printf("[IR & PSD] IR R1 SUCCESS \n");
						SetBitmap(23, 4);
						m_test_ir_psd[2] = true;
						result_side_ir_r1 = 1;
						//InsertResultString(auto_result, 25, 1);
					}
					else
					{
						printf("[IR & PSD] IR R1 FAIL \n");
						SetBitmap(23, 2);
						result_side_ir_r1 = 0;
						//InsertResultString(auto_result, 25, 0);
					}
					break;
				case 3:
					if (msg.body[2] == 1)
					{
						printf("[IR & PSD] IR CENTER SUCCESS \n");
						SetBitmap(22, 4);
						m_test_ir_psd[3] = true;
						result_front_ir = 1;
						//InsertResultString(auto_result, 23, 1);
					}
					else
					{
						printf("[IR & PSD] IR CENTER FAIL \n");
						SetBitmap(22, 2);
						result_front_ir = 0;
						//InsertResultString(auto_result, 23, 0);
					}
					break;
				case 4:
					if (msg.body[2] == 1)
					{
						printf("[IR & PSD] IR CENTER SUCCESS \n");
						SetBitmap(22, 4);
						m_test_ir_psd[4] = true;
						result_front_ir = 1;
						//InsertResultString(auto_result, 23, 1);
					}
					else
					{
						printf("[IR & PSD] IR CENTER FAIL \n");
						SetBitmap(22, 2);
						result_front_ir = 0;
						//InsertResultString(auto_result, 23, 0);
					}
					break;
				case 5:
					if (msg.body[2] == 1)
					{
						printf("[IR & PSD] IR L1 SUCCESS \n");
						SetBitmap(25, 4);
						m_test_ir_psd[5] = true;
						result_side_ir_l1 = 1;
						//InsertResultString(auto_result, 29, 1);
					}
					else
					{
						printf("[IR & PSD] IR L1 FAIL \n");
						SetBitmap(25, 2);
						result_side_ir_l1 = 0;
						//InsertResultString(auto_result, 29, 0);
					}
					break;
				case 6:
					if (msg.body[2] == 1)
					{
						printf("[IR & PSD] IR L2 SUCCESS \n");
						SetBitmap(26, 4);
						m_test_ir_psd[6] = true;
						result_side_ir_l2 = 1;
						//InsertResultString(auto_result, 31, 1);
					}
					else
					{
						printf("[IR & PSD] IR L2 FAIL \n");
						SetBitmap(26, 2);
						result_side_ir_l2 = 0;
						//InsertResultString(auto_result, 31, 0);
					}
					break;
				case 7:
					if (msg.body[2] == 1)
					{
						printf("[IR & PSD] PSD L SUCCESS \n");
						SetBitmap(28, 4);
						m_test_ir_psd[7] = true;
						result_side_psd_l = 1;
						//InsertResultString(auto_result, 35, 1);
					}
					else
					{
						printf("[IR & PSD] PSD L FAIL \n");
						SetBitmap(28, 2);
						result_side_psd_l = 0;
						//InsertResultString(auto_result, 35, 0);
					}
					break;
				case 8:
					if (msg.body[2] == 1)
					{
						printf("[IR & PSD] DOCKING IR R \n");
						SetBitmap(29, 1);
						m_test_docking_ir++;
					}
					break;
				case 9:
					if (msg.body[2] == 1)
					{
						printf("[IR & PSD] DOCKING IR C \n");
						SetBitmap(29, 1);
						m_test_docking_ir++;
					}
					break;
				case 10:
					if (msg.body[2] == 1)
					{
						printf("[IR & PSD] DOCKING IR L \n");
						SetBitmap(29, 1);
						m_test_docking_ir++;
					}
					break;
				case 11:
					if (m_test_docking_ir == 3)
					{
						printf("[IR & PSD] DOCKING IR SUCCESS \n");
						SetBitmap(29, 4);
						result_docking_ir = 1;
						//InsertResultString(auto_result, 37, 1);
					}
					else
					{
						printf("[IR & PSD] DOCKING IR FAIL \n");
						SetBitmap(29, 2);
						result_docking_ir = 0;
						//InsertResultString(auto_result, 37, 0);
					}
					break;
				case 12:
					m_test_state = SELF_DIAGNOSIS_STATE_BUMPER;
					break;
				}
			}
			break;
		}
		else
		{
			printf("this state isn't checked Num :: %d\n", m_test_state);
			m_test_state = SELF_DIAGNOSIS_STATE_BUMPER;
		}
		break;
		case SELF_DIAGNOSIS_STATE_BUMPER:
		if (exist_msg == false)
		{
			return;
		}
		if (msg.body_size != 0 && m_test_state != msg.body[0])
		{
			printf("[FINAL_TEST] test state do not equal robot state\n");
			printf("[FINAL_TEST] adjust test state.....goto %d\n", msg.body[0]);
			m_test_state = msg.body[0];
			Process(msg, true);
			return;
		}
		//bumper
		b_check_bumper = GetCheck(10);
		if (b_check_bumper)
		{
			if (msg.body[0] == 9 && msg.msg_type == 1)
			{
				if (msg.body_size < 2)
				{
					printf("case %d :: body size error!!!!\n", m_test_state);
					break;
				}
				switch (msg.body[1])
				{
				case 0:
					if (msg.body[2] == 1)
					{
						printf("[BUMPER] BUMPER R SUCCESS \n");
						SetBitmap(42, 4);
						m_test_bumper[0] = true;
						result_bumper_r = 1;
						//InsertResultString(auto_result, 39, 1);
					}
					else
					{
						SetBitmap(42, 2);
						result_top_bumper_r = 0;
						//InsertResultString(auto_result, 39, 0);
					}
					break;
				case 1:
					if (msg.body[2] == 1)
					{
						printf("[BUMPER] BUMPER C SUCCESS \n");
						SetBitmap(43, 4);
						m_test_bumper[1] = true;
						result_top_bumper_c = 1;
						//InsertResultString(auto_result, 41, 1);
					}
					else
					{
						SetBitmap(43, 2);
						result_top_bumper_c = 0;
						//InsertResultString(auto_result, 41, 0);
					}
					break;
				case 2:
					if (msg.body[2] == 1)
					{
						printf("[BUMPER] BUMPER L SUCCESS \n");
						SetBitmap(10, 4);
						m_test_bumper[2] = true;
						result_bumper_l = 1;
						//InsertResultString(auto_result, 43, 1);
					}
					else
					{
						SetBitmap(10, 2);
						result_bumper_l = 0;
						//InsertResultString(auto_result, 43, 0);
					}
					break;
				case 3:
					cout << "[final_test] bumper test finish" << endl;
					if (!m_test_bumper[0])
					{
						printf("[BUMPER] BUMPER R FAIL \n");
						SetBitmap(42, 2);
						//InsertResultString(auto_result, 39, 0);
					}
					if (!m_test_bumper[1])
					{
						printf("[BUMPER] BUMPER C FAIL \n");
						SetBitmap(43, 2);
						//InsertResultString(auto_result, 41, 0);
					}
					if (!m_test_bumper[2])
					{
						printf("[BUMPER] BUMPER L FAIL \n");
						SetBitmap(10, 2);
						//InsertResultString(auto_result, 43, 0);
					}
					m_test_state = SELF_DIAGNOSIS_STATE_BACKWARD;
					break;
				}
			}
			else
			{
				printf("this state isn't checked Num :: %d\n", m_test_state);
				m_test_state = 10;
			}
		}
		break;
		case SELF_DIAGNOSIS_STATE_BACKWARD:
		if (exist_msg == false)
		{
			return;
		}
		if (msg.body_size != 0 && m_test_state != msg.body[0])
		{
			printf("[FINAL_TEST] test state do not equal robot state\n");
			printf("[FINAL_TEST] adjust test state.....goto %d\n", msg.body[0]);
			m_test_state = msg.body[0];
			Process(msg, true);
			return;
		}
		//run backward forward
		b_check_motor = GetCheck(8);
		if (b_check_motor)
		{
			if (msg.msg_type == 1)
			{
				if (msg.body_size < 3)
				{
					printf("case %d :: body size error!!!!\n", m_test_state);
					break;
				}
				if (msg.body[0] == 10)
				{
					//cout << "wheel!!!!! ::" << msg.body[2] << "  " << st_wheel_error_backward << "  " << st_wheel_error_forward << "   " << st_wheel_current.max << "   " << st_wheel_current.min << endl;
					switch (msg.body[1])
					{
					case 0:
						if (abs(100 - msg.body[2]) < st_wheel_error_backward)
						{
							printf("[WHEEL] PE BACKWARD SUCCESS \n");
							SetBitmap(32, 4);
							m_test_error_b = 1;
						}
						else
						{
							printf("[WHEEL] PE BACKWARD FAIL \n");
							SetBitmap(32, 2);
						}
						result_error_b = msg.body[2];
						//InsertResultString(auto_result, 65, msg.body[2]);
						break;
					case 1:
						if (msg.body[2] < st_wheel_current.max && msg.body[2] > st_wheel_current.min)
						{
							SetBitmap(31, 1);
							m_test_wheel_current_r = 1;
						}
						else
						{
							SetBitmap(31, 2);
						}
						result_wheel_r_b = msg.body[2];
						//InsertResultString(auto_result, 49, msg.body[2]);
						break;
					case 2:
						if (msg.body[2] < st_wheel_current.max && msg.body[2] > st_wheel_current.min)
						{
							SetBitmap(30, 1);
							m_test_wheel_current_l = 1;
						}
						else
						{
							SetBitmap(30, 2);
						}
						result_wheel_l_b = msg.body[2];
						//InsertResultString(auto_result, 57, msg.body[2]);
						break;
					case 3:
						if ( abs(100 - msg.body[2]) < st_wheel_error_forward)
						{
							printf("[WHEEL] PE FORWARD SUCCESS \n");
							SetBitmap(33, 4);
							m_test_error_b = 1;
						}
						else
						{
							printf("[WHEEL] PE FORWARD FAIL \n");
							SetBitmap(33, 2);
						}
						result_error_f = msg.body[2];
						//InsertResultString(auto_result, 61, msg.body[2]);
						break;
					case 4:
						if (msg.body[2] < st_wheel_current.max && msg.body[2] > st_wheel_current.min && m_test_wheel_current_r == 1)
						{
							printf("[WHEEL] WHEEL MOTOR R SUCCESS \n");
							SetBitmap(31, 4);
							m_test_wheel_current_r++;
						}
						else
						{
							printf("[WHEEL] WHEEL MOTOR R FAIL \n");
							SetBitmap(31, 2);
						}
						result_wheel_r_f = msg.body[2];
						//InsertResultString(auto_result, 45, msg.body[2]);
						break;
					case 5:
						if (msg.body[2] < st_wheel_current.max && msg.body[2] > st_wheel_current.min && m_test_wheel_current_l == 1)
						{
							printf("[WHEEL] WHEEL MOTOR L SUCCESS \n");
							SetBitmap(30, 4);
							m_test_wheel_current_l++;
						}
						else
						{
							printf("[WHEEL] WHEEL MOTOR L FAIL \n");
							SetBitmap(30, 2);
						}
						result_wheel_l_f = msg.body[2];
						//InsertResultString(auto_result, 53, msg.body[2]);
						break;
					case 6:
						if ( m_test_error_b != 1)
						{
							SetBitmap(32, 2);
							SetBitmap(33, 2);
						}
						else
						{
							SetBitmap(32, 4);
							SetBitmap(33, 4);
						}
						if (m_test_wheel_current_r != 2)
						{
							SetBitmap(31, 2);
						}
						if (m_test_wheel_current_l != 2)
						{
							SetBitmap(30, 2);
						}
						m_test_state = SELF_DIAGNOSIS_STATE_HALL;
						break;
					}
				}
			}
		}
		else
		{
			printf("this state isn't checked Num :: %d\n", m_test_state);
			m_test_state = SELF_DIAGNOSIS_STATE_HALL;
		}
		break;
	case SELF_DIAGNOSIS_STATE_HALL:
		if (exist_msg == false)
		{
			return;
		}
		if (msg.body_size != 0 && m_test_state != msg.body[0])
		{
			printf("[FINAL_TEST] test state do not equal robot state\n");
			printf("[FINAL_TEST] adjust test state.....goto %d\n", msg.body[0]);
			m_test_state = msg.body[0];
			Process(msg, true);
			return;
		}
		//hall
		b_check_magnet = GetCheck(10);
		if (b_check_magnet)
		{
			if (msg.msg_type == 1)
			{
				if (msg.body_size < 2)
				{
					printf("case %d :: body size error!!!!\n", m_test_state);
					break;
				}
				if (msg.body[0] == 12)
				{
					if (msg.body[1] == 1)
					{
						printf("[HALL] HALL SUCCESS \n");
						m_test_hall++;
						SetBitmap(34, 4);
						result_hall = 1;
						//InsertResultString(auto_result, 69, 1);
					}
					else if (msg.body[1] == 0)
					{
						printf("[HALL] HALL FAIL \n");
						SetBitmap(34, 2);
						result_hall = 0;
						//InsertResultString(auto_result, 69, 0);
					}
					else if (msg.body[1] == 2)
					{
						printf("[HALL] FINISH, GO TO BASE \n");
						m_test_state = SELF_DIAGNOSIS_STATE_GOTO_BASE;
						break;
					}
				}
			}
		}
		else
		{
			printf("this state isn't checked Num :: %d\n", m_test_state);
			m_test_state = SELF_DIAGNOSIS_STATE_GOTO_BASE;
		}
		break;

	case SELF_DIAGNOSIS_STATE_GOTO_BASE:
		if (exist_msg == false)
		{
			return;
		}
		if (msg.body_size != 0 && m_test_state != msg.body[0])
		{
			printf("[FINAL_TEST] test state do not equal robot state\n");
			printf("I'm in %d\n", m_test_state);
			printf("[FINAL_TEST] adjust test state.....goto %d\n", msg.body[0]);
			m_test_state = msg.body[0];
			Process(msg, true);
			return;
		}
		//goto base
		b_check_base = GetCheck(12);
		if (b_check_base)
		{
			if (msg.msg_type == 1)
			{
				if (msg.body_size < 2)
				{
					printf("case %d :: body size error!!!!\n", m_test_state);
					break;
				}
				if (msg.body[0] == SELF_DIAGNOSIS_STATE_GOTO_BASE)
				{
					if (msg.body[1] == 1)
					{
						printf("[DOCKING] SUCCESS \n");
						SetBitmap(35, 4);
						m_test_base = true;
						result_docking = 1;
						//InsertResultString(auto_result, 71, 1);
						string start_diag = "send [JK_TEST]0 1 0 \n";
						SendCmd(start_diag.c_str(), start_diag.size());
						Sleep(20);
						string charger = "send [JK_TEST]0 1 15 \n";
						SendCmd(charger.c_str(), charger.size());
					}
					else if (msg.body[1] == -1)
					{
						printf("[DOCKING] FAIL \n");
						SetBitmap(35, 2);
						result_docking = 0;
						//InsertResultString(auto_result, 71, 0);
					}
					m_test_state = SELF_DIAGNOSIS_STATE_CHARGER;
				}
			}
			break;
		}
		else
		{
			printf("this state isn't checked Num :: %d\n", m_test_state);
			m_test_state = SELF_DIAGNOSIS_STATE_CHARGER;
		}
		break;
	case SELF_DIAGNOSIS_STATE_CHARGER:
		if (exist_msg == false)
		{
			return;
		}
		if (msg.body[0] == 0)
		{
			return;
		}
		if (msg.body_size != 0 && m_test_state != msg.body[0])
		{
			printf("[FINAL_TEST] test state do not equal robot state\n");
			printf("[FINAL_TEST] adjust test state.....goto %d\n", msg.body[0]);
			printf("I'm in %d\n", m_test_state);
			m_test_state = msg.body[0];
			Process(msg, true);
			return;
		}
		//charger
		b_check_base = GetCheck(12);
		if (b_check_base)
		{
			if (msg.msg_type == 1)
			{
				if (msg.body_size < 2)
				{
					printf("case %d :: body size error!!!!\n", m_test_state);
					break;
				}
				if (msg.body[0] == SELF_DIAGNOSIS_STATE_CHARGER)
				{
					if (msg.body[1] == 0)
					{
						pre_voltage = msg.body[2];
					}
					else if (msg.body[1] == 1)
					{
						if ((msg.body[2] - pre_voltage) > st_charging)
						{
							printf("[CHARGING] SUCCESS %d \n", (msg.body[2] - pre_voltage) > st_charging);
							SetBitmap(36, 4);
							result_charging = 1;
							//InsertResultString(auto_result, 73, 1);
							m_test_charging = true;
						}
						else
						{
							printf("[CHARGING] FAIL %d \n", (msg.body[2] - pre_voltage) > st_charging);
							SetBitmap(36, 2);
							result_charging = 0;
							//InsertResultString(auto_result, 73, 0);
						}
						if (msg.body[3] == 6)
						{
							printf("[CHARGER] SUCCESS \n");
							m_test_charger = true;
							SetBitmap(7, 4);
							//InsertResultString(auto_result, 77, 1);
						}
						else if (msg.body[3] == 0)
						{
							printf("[CHARGER] FAIL \n");
							SetBitmap(7, 2);
							//InsertResultString(auto_result, 77, 0);
						}
						result_charger = msg.body[3];
						m_test_state = SELF_DIAGNOSIS_STATE_ANGLE;
					}
				}
			}
			break;
		}
		else
		{
			printf("this state isn't checked Num :: %d\n", m_test_state);
			m_test_state = SELF_DIAGNOSIS_STATE_ANGLE;
		}
		break;
	case SELF_DIAGNOSIS_STATE_BACKWARD_OUT:
		if (exist_msg == false)
		{
			return;
		}
		if (msg.body_size != 0 && m_test_state != msg.body[0])
		{
			printf("[FINAL_TEST] test state do not equal robot state\n");
			printf("[FINAL_TEST] adjust test state.....goto %d\n", msg.body[0]);
			printf("I'm in %d\n", m_test_state);
			m_test_state = msg.body[0];
			Process(msg, true);
			return;
		}
		//backward out
		b_check_gyro = GetCheck(15);
		if (b_check_gyro)
		{
			if (msg.body[1] == 1)
			{
				m_test_state = SELF_DIAGNOSIS_STATE_ANGLE;
			}
			break;
		}
		else
		{
			printf("this state isn't checked Num :: %d\n", m_test_state);
			m_test_state = SELF_DIAGNOSIS_STATE_ANGLE;
			break;
		}	
	case SELF_DIAGNOSIS_STATE_ANGLE:
		if (msg.body_size != 0 && m_test_state != msg.body[0] && exist_msg)
		{
			printf("[FINAL_TEST] test state do not equal robot state\n");
			printf("[FINAL_TEST] adjust test state.....goto %d\n", msg.body[0]);
			printf("I'm in %d\n", m_test_state);
			m_test_state = msg.body[0];
			Process(msg, true);
			break;
		}

		//angle
		b_check_gyro = GetCheck(15);
		if (b_check_gyro)
		{
			bool result = false;
			static int ack = 0;
			int rotate_cmd = 0;
			double scale_factor = 0.0;
			bool recursive_thing = false;
			string result_string;
			//ack
			if (msg.msg_type == 3)
			{
				ack = msg.body[1];
			}
			bool is_it_end = gyro_test.Process(result, ack, rotate_cmd, scale_factor, result_string, recursive_thing);
			if (is_it_end)
			{
				//pass
				if (result)
				{
					string cmd = "send [JK_TEST]0 2 18 0 \n";
					SendCmd(cmd.c_str(), cmd.size());
					Sleep(30);
					m_test_gyro = true;
					SetBitmap(37, 4);
					result_gyro = 1;
					m_test_state = SELF_DIAGNOSIS_STATE_RTC;
					SendCmd(result_string.c_str(), result_string.size());
				}
				//fail
				else
				{
					string cmd = "send [JK_TEST]0 2 18 0 \n";
					SendCmd(cmd.c_str(), cmd.size());
					Sleep(30);
					m_test_gyro = false;
					SetBitmap(37, 2);
					result_gyro = 0;
					m_test_state = SELF_DIAGNOSIS_STATE_RTC;
					SendCmd(result_string.c_str(), result_string.size());
				}
			}
			if (rotate_cmd != 0)
			{
				string cmd = "send [JK_TEST]0 2 17 " + to_string(rotate_cmd) + " \n";
				SendCmd(cmd.c_str(), cmd.size());
			}
			if (scale_factor != 0)
			{
				string cmd = "send [JK_TEST]0 2 333 " + to_string((int)(scale_factor * 1000000)) + " \n"; 
				SendCmd(cmd.c_str(), cmd.size());
			}
			if (recursive_thing)
			{
				cmd_msg.push(msg);
			}
		}
		break;
	case SELF_DIAGNOSIS_STATE_RTC:
		if (exist_msg == false)
		{
			return;
		}
		if (msg.body_size != 0 && m_test_state != msg.body[0])
		{
			printf("[FINAL_TEST] test state do not equal robot state\n");
			printf("[FINAL_TEST] adjust test state.....goto %d\n", msg.body[0]);
			m_test_state = msg.body[0];
			Process(msg, true);
			return;
		}
		b_check_rtc = GetCheck(15);
		if (b_check_rtc && msg.body_size == 2)
		{
			if (msg.msg_type == 1 && msg.body[0] == SELF_DIAGNOSIS_STATE_RTC)
			{
				// wifi check... i am tired to add new state....T.T
				//pass
				if (ping_result > 9)
				{
					SetBitmap(46, 4);
					m_test_wifi = true;
					result_wifi = 1;
				}
				//fail
				else
				{
					SetBitmap(46, 2);
					m_test_wifi = false;
					result_wifi = 0;
				}
				if (msg.body[1] == 1)
				{
					printf("[RTC] SUCCESS \n");
					SetBitmap(6, 4);
					result_rtc = 1;
					//InsertResultString(auto_result, 79, 1);
					m_test_rtc = 1;
				}
				else
				{
					printf("[RTC] FAIL \n");
					SetBitmap(6, 2);
					result_rtc = 0;
					//InsertResultString(auto_result, 79, 0);
				}
			}
		}

		break;

	case SELF_DIAGNOSIS_STATE_SAVE_RESULT:
	{
		string result_for_sending = "send [JK_TEST]0 45 50 ";
		string result_for_saving = to_string(result_version) + " " + to_string(result_firmware) + " " + to_string(result_ui) + " " + to_string(result_sensor) + " " + to_string(result_hardware)
			+ " " + to_string(result_dust_sensor) + " " + to_string(result_dust_bin) + " " + to_string(result_top_bumper_r) + " " + to_string(result_top_bumper_c) + " " + to_string(result_top_bumper_l) + " " + to_string(result_side_arm_r) + " " + to_string(result_side_arm_l)
			+ " " + to_string(result_bottom_psd_r) + " " + to_string(result_bottom_psd_c) + " " + to_string(result_bottom_psd_l) + " " + to_string(result_psd_cal) + " " + to_string(result_main_brush)
			+ " " + to_string(result_side_brush_r) + " " + to_string(result_side_brush_l) + " " + to_string(result_vacuum) + " " + to_string(result_front_ir) + " " + to_string(result_side_ir_r1) + " " + to_string(result_side_ir_r2)
			+ " " + to_string(result_side_ir_l1) + " " + to_string(result_side_ir_l2) + " " + to_string(result_side_psd_r) + " " + to_string(result_side_psd_l) + " " + to_string(result_docking_ir)
			+ " " + to_string(result_bumper_r) + " " + to_string(result_bumper_c) + " " + to_string(result_bumper_l) + " " + to_string(result_wheel_l_b) + " " + to_string(result_wheel_r_b) + " " + to_string(result_wheel_l_f) + " " + to_string(result_wheel_r_f)
			+ " " + to_string(result_error_b) + " " + to_string(result_error_f) + " " + to_string(result_hall) + " " + to_string(result_docking) + " " + to_string(result_charging) + " " + to_string(result_gyro) + " " + to_string(result_charger) + " " + to_string(result_rtc) + " " + to_string(result_wifi);
		result_for_sending = result_for_sending + result_for_saving + "\n";
		SendCmd(result_for_sending.c_str(), result_for_sending.size());
		std::ofstream out_file(result_location, ios::app);
		cout << "test result ::::" << result_for_saving << endl;
		out_file << result_for_saving << endl;
		m_test_state = SELF_DIAGNOSIS_STATE_GOTO_TESTPOINT;

	}
	break;

	case SELF_DIAGNOSIS_STATE_GOTO_TESTPOINT:
	{
		static bool cmd_locker(false);
		Sleep(1000);
		if (!cmd_locker)
		{
			cmd_locker = true;
			string kill_rx2 = "killall run_rx2\n";
			SendCmd(kill_rx2.c_str(), kill_rx2.size());
			Sleep(100);
			kill_rx2 = "killall robot_node\n";
			SendCmd(kill_rx2.c_str(), kill_rx2.size());
			Sleep(100);
			kill_rx2 = "killall ccr\n";
			SendCmd(kill_rx2.c_str(), kill_rx2.size());
			Sleep(100);
			kill_rx2 = "killall avahi_publish\n";
			SendCmd(kill_rx2.c_str(), kill_rx2.size());
			Sleep(100);
			string on_image_server = "cd /home/yroot/;export LD_LIBRARY_PATH=/home/ylibs/;./image_server 5 2 &\n";
			SendCmd(on_image_server.c_str(), on_image_server.size());

			int full_count = 0;
			full_count = m_test_wifi + m_test_base + m_test_bottom_psd + m_test_charger + m_test_charging + m_test_docking_ir + m_test_dust_bin + m_test_dust_sensor + m_test_error_b + m_test_gyro + m_test_hall + m_test_psd_cal + m_test_rtc + m_test_wheel_current_l + m_test_wheel_current_r;
			for (int i = 0; i < 8; i++)
			{
				full_count += m_test_ir_psd[i];
			}
			for (int i = 0; i < 5; i++)
			{
				full_count += m_test_version[i];
			}
			for (int i = 0; i < 4; i++)
			{
				full_count += m_test_motor[i];
			}
			for (int i = 0; i < 3; i++)
			{
				full_count += m_test_top_bumper[i];
				full_count += m_test_bumper[i];
			}
			full_count += m_test_side_arm[0];
			full_count += m_test_side_arm[1];

			if (m_model_sub == 0)
			{
				if (full_count == 49)
				{
					SetBitmap(45, 6);
				}
				else
				{
					SetBitmap(45, 7);
				}
			}
			else if (m_model_sub == 1)
			{
				if (full_count == 47)
				{
					SetBitmap(45, 6);
				}
				else
				{
					SetBitmap(45, 7);
				}
			}
			m_test_state++;
			cmd_locker = false;
		}
	}
	break;
	}
	return;
}



void CTestMain::ResetTest()
{
	Initialize();
	m_test_state = 0;
	FocusFab();
}


void CTestMain::PreRun(int cmd_num, int value)
{
	std::string cmd;
	switch (cmd_num)
	{
	//login & test start
	case 0:
		Sleep(3000);
		cmd = "root\n";
		SendCmd(cmd.c_str(), cmd.size());
		cmd.clear();
		Sleep(30);
		cmd = "send [JK_TEST]" + std::to_string(msg_number) + " 3 0 "+to_string(m_model_main)+" "+to_string(m_model_sub) + " \n";
		SendCmd(cmd.c_str(),cmd.size());
		break;
	case 1:
		break;
	case 2:
		break;
	case 3:
		break;
	case 4:
		break;
	case 5:
		break;
	default:
		printf("booting log\n");
		break;
	}
}


void CTestMain::Initialize()
{
	for (int i = 0; i < 40; i++)
	{
		if (i == 5)
		{
			continue;
		}
		SetBitmap(i, 3);
	}
	for (int i = 41; i < 44; i++)
	{
		SetBitmap(i, 3);
	}
	SetBitmap(45, 5);
	SetBitmap(46, 3);
	//SetBitmap(47, 3);
	counter = 0;
	inner_case = 0;
	b_check_camera = false;
	b_check_version = false;
	b_check_dust_sensor = false;
	b_check_dust_bin = false;
	b_check_top_door = false;
	b_check_dc_jack = false;
	b_check_top_bumper = false;
	b_check_motor = false;
	b_check_wheel = false;
	b_check_ir_psd = false;
	b_check_bumper = false;
	b_check_magnet = false;
	b_check_base = false;
	b_check_battery = false;
	b_check_gyro = false;
	b_check_psd_cal = false;
	b_check_rtc = false;
	//bottom psd min/max 저장용
	st_bottom_psd_l.min = 0;
	st_bottom_psd_c.min = 0;
	st_bottom_psd_r.min = 0;
	st_bottom_psd_l.max = 0;
	st_bottom_psd_c.max = 0;
	st_bottom_psd_r.max = 0;

	//양불 판정 결과 저장용 변수
	m_test_wifi = false;
	m_test_dust_sensor = 0;
	m_test_gyro = false;
	for (int i = 0; i < 8; i++)
	{
		m_test_ir_psd[i] = false;
	}
	for (int i = 0; i < 5; i++)
	{
		m_test_version[i] = false;
	}
	for (int i = 0; i < 4; i++)
	{
		m_test_motor[i] = false;
	}
	for (int i = 0; i < 3; i++)
	{
		m_test_top_bumper[i] = false;
		m_test_bumper[i] = false;
	}
	m_test_side_arm[0] = false;
	m_test_side_arm[1] = false;
	m_test_bottom_psd = 0;
	m_test_psd_cal = 0;
	m_test_docking_ir = 0;
	m_test_error_b = 0;
	m_test_wheel_current_r = 0;
	m_test_wheel_current_l = 0;
	m_test_hall = 0;
	m_test_base = false;
	m_test_charger = false;
	m_test_top_door = false;
	m_test_rtc = false;

	/*manual_result.clear();
	manual_result.resize(32);
	psd_cal_result.clear();
	psd_cal_result.resize(11);
	auto_result.clear();
	auto_result.resize(80);*/
	pre_voltage = 0;
	timer_counter = 0;
	init_lock = false;
	gyro_lock = false;
	//InsertResultString(manual_result, 999, 0);
	//InsertResultString(auto_result, 999, 1);
	//InsertResultString(psd_cal_result, 999, 2);
	gyro_test.initialize();
	right_image.clear();
	left_image.clear();
	image_left_flag = false;
	image_right_flag = false;
	result_version=0; result_firmware=0; result_ui=0; result_sensor=0; result_hardware=0; result_dust_sensor=0; result_dust_bin=0; result_top_bumper_r=0; result_top_bumper_c=0; result_top_bumper_l=0;
	result_side_arm_r=0; result_side_arm_l=0; result_bottom_psd_r=0; result_bottom_psd_c=0; result_bottom_psd_l=0; result_psd_cal=0; result_main_brush=0; result_side_brush_r=0; result_side_brush_l=0;
	result_vacuum=0; result_front_ir=0; result_side_ir_r1=0; result_side_ir_r2=0; result_side_ir_l1=0; result_side_ir_l2=0; result_side_psd_r=0; result_side_psd_l=0; result_docking_ir=0; result_bumper_r=0;
	result_bumper_c=0; result_bumper_l=0; result_wheel_r_b=0; result_wheel_l_b=0; result_wheel_l_f=0; result_wheel_r_f=0; result_error_b=0; result_error_f=0; result_hall=0; result_docking=0; result_charging=0; result_rtc=0; result_wifi=0;
	result_gyro=0; result_charger=0; result_total=0;

	ping_result = 0;
}


int CTestMain::getTestState()
{
	return m_test_state;
}