#pragma once
#include "ManualTest.h"
#include "AutomaticTest.h"
#include "SystemCheck.h"
#include "GyroTest.h"
typedef std::function<void(int, int)> callback_1;
typedef std::function<bool(int)> callback_2;
typedef std::function<int(const char*, int)> callback_3;
typedef std::function<void()> callback_4;
//min max or clear block
typedef struct{
	int min;
	int max;
}standard_value;
class CTestMain
{
public:
	CTestMain();
	~CTestMain();
	void Process(msg4queue msg, bool exist_msg);
	void ResetTest();
	void PreRun(int cmd_num, int value);
	void SetModel(int model_main, int model_sub);
	callback_1 SetBitmap;
	callback_2 GetCheck;
	callback_3 SendCmd;
	callback_4 FocusFab;
	void SetResultCallback(callback_1 func);
	void SetCheckBoxCallback(callback_2 func);
	void SetSendCallback(callback_3 func);
	void SetFocusCallback(callback_4 func);
	//테스트
	CManualTest manual_test;
	CGyroTest gyro_test;
	//최종 결과
	string ini_location;
	string result_location;
	string log_location;
	bool init_lock;
	bool gyro_lock;
	bool image_left_flag;
	bool image_right_flag;
	//void InsertResultString(string &result, int locate, int input_data)
	//{
	//	if (locate == 999)
	//	{
	//		//manual result
	//		if (input_data == 0)
	//		{
	//			result.replace(2, 1, " ");
	//			result.replace(5, 1, " ");
	//			result.replace(8, 1, " ");
	//			result.replace(11, 1, " ");
	//			result.replace(13, 1, " ");
	//			result.replace(15, 1, " ");
	//			result.replace(17, 1, " ");
	//			result.replace(19, 1, " ");
	//			result.replace(21, 1, " ");
	//			result.replace(23, 1, " ");
	//			result.replace(25, 1, " ");
	//			result.replace(27, 1, " ");
	//			result.replace(29, 1, " ");
	//		}
	//		//auto result
	//		else if (input_data == 1)
	//		{
	//			result.replace(1, 1, " ");
	//			result.replace(3, 1, " ");
	//			result.replace(5, 1, " ");
	//			result.replace(7, 1, " ");
	//			result.replace(11, 1, " ");
	//			result.replace(14, 1, " ");
	//			result.replace(17, 1, " ");
	//			result.replace(22, 1, " ");
	//			result.replace(24, 1, " ");
	//			result.replace(26, 1, " ");
	//			result.replace(28, 1, " ");
	//			result.replace(30, 1, " ");
	//			result.replace(32, 1, " ");
	//			result.replace(34, 1, " ");
	//			result.replace(36, 1, " ");
	//			result.replace(38, 1, " ");
	//			result.replace(40, 1, " ");
	//			result.replace(42, 1, " ");
	//			result.replace(44, 1, " ");
	//			result.replace(48, 1, " ");
	//			result.replace(52, 1, " ");
	//			result.replace(56, 1, " ");
	//			result.replace(60, 1, " ");
	//			result.replace(64, 1, " ");
	//			result.replace(68, 1, " ");
	//			result.replace(70, 1, " ");
	//			result.replace(72, 1, " ");
	//			result.replace(74, 1, " ");
	//			result.replace(76, 1, " ");
	//			result.replace(78, 1, " ");
	//		}
	//		//psd cal data
	//		else if (input_data == 2)
	//		{
	//			result.replace(3, 1, " ");
	//			result.replace(7, 1, " ");
	//		}
	//	}
	//	else if (result.size() > locate){
	//		string temp = to_string(input_data);
	//		result.replace(locate, temp.size(), temp);
	//	}
	//	else
	//	{
	//		AfxMessageBox("[SystemError] Result String Overflow!");
	//	}
	//	return;
	//}
private:
	int m_test_state;
	int m_model_main;
	int m_model_sub;
public:
	void Initialize();
	//std::string total_result;
	//std::string manual_result;
	//std::string auto_result;
	//std::string psd_cal_result;
	int result_wifi;
	int getTestState();
private:
	bool b_check_camera;
	bool b_check_version;
	bool b_check_dust_sensor;
	bool b_check_dust_bin;
	bool b_check_top_door;
	bool b_check_dc_jack;
	bool b_check_top_bumper;
	bool b_check_motor;
	bool b_check_wheel;
	bool b_check_ir_psd;
	bool b_check_bumper;
	bool b_check_magnet;
	bool b_check_base;
	bool b_check_battery;
	bool b_check_gyro;
	bool b_check_psd_cal;
	bool b_check_rtc;

	int pre_voltage;
	//bottom psd min/max 저장용
	standard_value st_bottom_psd_l;
	standard_value st_bottom_psd_c;
	standard_value st_bottom_psd_r;

	//양불 판정 결과 저장용 변수
	bool m_test_wifi;
	int m_test_dust_sensor;
	bool m_test_gyro;
	bool m_test_top_bumper[3];
	bool m_test_side_arm[2];
	int m_test_bottom_psd;
	int m_test_psd_cal;
	bool m_test_motor[4];
	bool m_test_ir_psd[8];
	int m_test_docking_ir;
	bool m_test_bumper[3];
	int m_test_error_b;
	int m_test_error_f;
	int m_test_wheel_current_r;
	int m_test_wheel_current_l;
	int m_test_hall;
	bool m_test_base;
	bool m_test_charger;
	bool m_test_version[5];
	bool m_test_top_door;
	bool m_test_dust_bin;
	bool m_test_charging;
	bool m_test_rtc;

	// for reporting test result
	int result_version;
	int result_firmware;
	int result_ui;
	int result_sensor;
	int result_hardware;
	int result_dust_sensor;
	int result_dust_bin;
	int result_top_bumper_r;
	int result_top_bumper_c;
	int result_top_bumper_l;
	int result_side_arm_r;
	int result_side_arm_l;
	int result_bottom_psd_r;
	int result_bottom_psd_c;
	int result_bottom_psd_l;
	int result_psd_cal;
	int result_main_brush;
	int result_side_brush_r;
	int result_side_brush_l;
	int result_vacuum;
	int result_front_ir;
	int result_side_ir_r1;
	int result_side_ir_r2;
	int result_side_ir_l1;
	int result_side_ir_l2;
	int result_side_psd_r;
	int result_side_psd_l;
	int result_docking_ir;
	int result_bumper_r;
	int result_bumper_c;
	int result_bumper_l;
	int result_wheel_r_b;
	int result_wheel_l_b;
	int result_wheel_r_f;
	int result_wheel_l_f;
	int result_error_b;
	int result_error_f;
	int result_hall;
	int result_docking;
	int result_charging;
	int result_gyro;
	int result_charger;
	int result_rtc;
	int result_total;

	//ini에서 읽어온 기준값
	standard_value st_dust_sensor;
	int st_dust_bin;
	int st_bottom_psd;
	standard_value st_motor_main_brush;
	standard_value st_motor_side_brush;
	standard_value st_motor_vacuum;
	int st_wheel_error_backward;
	int st_wheel_error_forward;
	standard_value st_wheel_current;
	int st_charging;
	int st_sw_ver;
	int st_fw_ver;
	int st_ui_ver;
	int st_sensor_ver;
	int st_hw_ver;
	string wlan;

	int counter;
	int inner_case;
	int main_model;
	int sub_model;

	int timer_counter;

	int timer_dust_sensor;
	int timer_dust_bin;
	int timer_top_door;
	int timer_top_bumper;
};