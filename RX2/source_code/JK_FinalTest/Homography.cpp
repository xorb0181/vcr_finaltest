/*
 * Homography.cpp
 *
 *  Created on: 2015. 7. 6.
 *      Author: jmk
 */
#include "stdafx.h"
#include "Homography.h"

Homography::Homography()
{
    // TODO Auto-generated constructor stub

}

Homography::~Homography() {
    // TODO Auto-generated destructor stub
}

Mat Homography::homography(Mat input,vector<Point2f> P1){
    //point selection until 4 points setting
    //prepare to get homography matrix
    vector< Point2f> P2(4);

    //user setting position �ٲ����
    P2[0].x = 0; P2[0].y = 0;
    P2[1].x = 800; P2[1].y = 0;
    P2[2].x = 800; P2[2].y = 800;
    P2[3].x = 0; P2[3].y = 800;
    RoI_X_Start = P2[0].x;
    RoI_X_End = P2[2].x;
    RoI_Y_Start = P2[0].y;
    RoI_Y_End = P2[2].y;

    //get homography
    Mat H = cv::findHomography(P1,P2,RANSAC);
    return H;
}

void Homography::calConfirm(Mat H, vector<Point2f> P1, vector<Point2f> P2){
	        ///////////////////////////
	        //calculation confirm

	        cout << "h" << endl << H << endl;
	        cout << "size rows and cols " << H.rows << " " << H.cols << endl;

	        Mat A(3,4,CV_64F); //3xN, P1
	        Mat B(3,4,CV_64F); //3xN, P2
	        //B = H*A  (P2 = h(P1))


	        for(int i=0; i< 4; ++i)
	        {
	            A.at< double>(0,i) = P1[i].x;
	            A.at< double>(1,i) = P1[i].y;
	            A.at< double>(2,i) = 1;


	            B.at< double>(0,i) = P2[i].x;
	            B.at< double>(1,i) = P2[i].y;
	            B.at< double>(2,i) = 1;
	        }

	        cout << "a" << endl << A << endl;
	        cout << "b" << endl << B << endl;
	        Mat HA = H*A;

	        for(int i=0; i< 4; ++i)
	        {
	            HA.at< double>(0,i) /= HA.at< double>(2,i);
	            HA.at< double>(1,i) /= HA.at< double>(2,i);
	            HA.at< double>(2,i) /= HA.at< double>(2,i);
	        }

	        cout << "HA" << endl << HA << endl;
}
Mat Homography::warpImage(Mat input,Mat H,Size2i inputSize){
	Mat output;
	warpPerspective(input, output, H,inputSize);
	rectangle(input, Point(0,0), Point(800,800), CV_RGB(255,0,0) );
	Rect RoI_Cutter(RoI_X_Start,RoI_Y_Start,RoI_X_End,RoI_Y_End);
	output = output(RoI_Cutter);
	//imshow("warped_image", output);
	return output;
}
