#include "stdafx.h"
#include "SystemCheck.h"


CSystemCheck::CSystemCheck()
	: b_is_cam_cal(false)
	, b_sw_result(false)
	, b_fw_result(false)
	, b_ui_result(false)
	, b_sensor_result(false)
	, b_pcb_result(false)
	, b_hw_result(false)
{
}

CSystemCheck::~CSystemCheck()
{
}

void CSystemCheck::Initializer()
{
	b_is_cam_cal = false;
	b_sw_result = false;
	b_fw_result = false;
	b_ui_result = false;
	b_sensor_result = false;
	b_pcb_result = false;
	b_hw_result = false;
}

bool CSystemCheck::GetCamResult()
{
	return b_is_cam_cal;
}
bool CSystemCheck::GetSwResult()
{
	return b_sw_result;
}
bool CSystemCheck::GetFwResult()
{
	return b_fw_result;
}
bool CSystemCheck::GetUiResult()
{
	return b_ui_result;
}
bool CSystemCheck::GetSensorResult()
{
	return b_sensor_result;
}
bool CSystemCheck::GetPcbResult()
{
	return b_pcb_result;
}
bool CSystemCheck::GetHwResult()
{
	return b_hw_result;
}
void CSystemCheck::SetVerStandard(std::string sw, std::string fw, std::string ui, std::string sensor, std::string pcb, std::string hw)
{
	sw_Version = sw;
	fw_Version = fw;
	ui_Version = ui;
	sensor_Version = sensor;
	pcb_Version = pcb;
	hw_Version = hw;
	return;
}

void CSystemCheck::CamCal_Checker()
{
	return;
}
void CSystemCheck::Version_Checker()
{
	return;
}
