/*
 * CameraCalib.h
 *
 *  Created on: 2015. 7. 22.
 *      Author: jmk
 */

#ifndef CAMERACALIB_H_
#define CAMERACALIB_H_

#include "TypeMat.h"
using namespace cv;

class CameraCalib
{
	double cx,cy,fx,fy,k1,k2,k3,p1,p2,skew_c;
public:
	CameraCalib(void);
	~CameraCalib(void);

	void UndistortImage(const Mat& frame, Mat& frame_undistort);

	void SetParams(double cx, double cy, double fx, double fy, double k1, double k2, double k3, double p1, double p2, double skew_c);
	void Normalize(double &x, double& y);
	void Denormalize(double &x, double& y);
	void Distort(double& x, double& y);
	void DistortPixel(int& x, int& y);
	void UndistortPixel(int& x, int& y);
};
#endif /* CAMERACALIB_H_ */


