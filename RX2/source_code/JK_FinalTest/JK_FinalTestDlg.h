
// JK_FinalTestDlg.h : 헤더 파일
//

#pragma once
#include "afxwin.h"
#include "TestMain.h"
// CJK_FinalTestDlg 대화 상자
class CJK_FinalTestDlg : public CDialogEx
{
// 생성입니다.
public:
	CJK_FinalTestDlg(CWnd* pParent = NULL);	// 표준 생성자입니다.
	const char* config_location = ".\\diag_config.ini";
// 대화 상자 데이터입니다.
	enum { IDD = IDD_JK_FINALTEST_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 지원입니다.


// 구현입니다.
protected:
	HICON m_hIcon;

	// 생성된 메시지 맵 함수
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	DECLARE_MESSAGE_MAP()
public:
	CTestMain total_test;
	CEdit m_edit_version;
	CEdit m_edit_fw;
	CEdit m_edit_ui;
	CEdit m_edit_sensor;
	CEdit m_edit_hw;
	CEdit m_edit_pcb;
	CEdit m_edit_model_main;
	CEdit m_edit_model_sub;
	CEdit m_edit_fab_num;
	CEdit m_edit_port;
	afx_msg void OnBnClickedButtonConnect();
	void Initialize();
	afx_msg void OnBnClickedButtonStart();
	bool b_test_start;
	BOOL m_check_cam_cal;
	BOOL m_check_version;
	BOOL m_check_dust_sensor;
	BOOL m_check_dust_bin;
	BOOL m_check_top_door;
	BOOL m_check_dc_jack;
	BOOL m_check_top_bumper;
	BOOL m_check_psd_cal;
	BOOL m_check_motor;
	BOOL m_check_ir_psd;
	BOOL m_check_bumper;
	BOOL m_check_magnet;
	BOOL m_check_base;
	BOOL m_check_battery;
	BOOL m_check_rtc;
	BOOL m_check_gyro;
	afx_msg void OnCheckCamCal();
	afx_msg void OnCheckVersion();
	afx_msg void OnCheckDustSensor();
	afx_msg void OnCheckDustbin();
	afx_msg void OnCheckTopDoor();
	afx_msg void OnCheckDcJack();
	afx_msg void OnCheckTopBumper();
	afx_msg void OnCheckPsdCal();
	afx_msg void OnCheckMotor();
	afx_msg void OnCheckIrPsd();
	afx_msg void OnCheckBumper();
	afx_msg void OnCheckMagnet();
	afx_msg void OnCheckBase();
	afx_msg void OnCheckBattery();
	afx_msg void OnCheckRtc();
	afx_msg void OnCheckGyro();
	void Init_CheckBox();
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnButtonCheckAll();
	afx_msg void OnButtonInitCheckbox();
	CStatic m_pic_version;
	CStatic m_pic_fw;
	CStatic m_pic_ui;
	CStatic m_pic_sensor;
	CStatic m_pic_hw;
	CStatic m_pic_rtc;
	CStatic m_pic_charger;
	CStatic m_pic_dust_bin;
	CStatic m_pic_top_bumper_r;
	CStatic m_pic_top_bumper_c;
	CStatic m_pic_top_bumper_l;
	CStatic m_pic_psd_cal;
	CStatic m_pic_bottom_psd_r;
	CStatic m_pic_bottom_psd_c;
	CStatic m_pic_bottom_psd_l;
	CStatic m_pic_main_brush;
	CStatic m_pic_side_brush_r;
	CStatic m_pic_side_brush_l;
	CStatic m_pic_vacuum;
	CStatic m_pic_front_ir;
	CStatic m_pic_side_ir_r1;
	CStatic m_pic_side_ir_r2;
	CStatic m_pic_side_ir_l1;
	CStatic m_pic_side_ir_l2;
	CStatic m_pic_side_psd_r;
	CStatic m_pic_side_psd_l;
	CStatic m_pic_docking_ir;
	CStatic m_pic_wheel_l;
	CStatic m_pic_wheel_r;
	CStatic m_pic_err_b;
	CStatic m_pic_err_f;
	CStatic m_pic_hall;
	CStatic m_pic_docking;
	CStatic m_pic_charging;
	CStatic m_pic_gyro;
	CStatic m_pic_result;
	CStatic m_pic_top_door;
	std::string ini_location;
	int main_model;
	int sub_model;
	CStatic m_pic_dust_sensor;
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	CStatic m_pic_side_arm_r;
	CStatic m_pic_side_arm_l;
	CStatic m_pic_bumper_c;
	CStatic m_pic_bumper_l;
	CStatic m_pic_bumper_r;
	CStatic m_pic_gyro_available;

	bool enable_counter;
	int time_counter;
//	afx_msg void OnClickedButtonResult();
	CStatic m_pic_wifi;
	afx_msg void OnBnClickedButtonWifiOk();
	afx_msg void OnBnClickedButtonWifiFail();

	int decodedJpegImage(char * compressedImagePtr, cv::Mat & gimg);
	int cb_ping_counter;
	static UINT ping_thread(LPVOID pParam);
	CStatic m_pic_cam_param;
	CEdit m_edit_u0;
	CEdit m_edit_v0;
	CEdit m_edit_ssid;
	CEdit m_edit_pw;
	afx_msg void OnBnClickedButtonCheckSsid();
	CStatic m_pic_fabrication;
};
