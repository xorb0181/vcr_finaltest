#pragma once

#include <string>
class CSystemCheck
{
public:
	CSystemCheck();
	~CSystemCheck();
	void Initializer();
	void CamCal_Checker();
	void Version_Checker();

	bool GetCamResult();
	bool GetSwResult();
	bool GetFwResult();
	bool GetUiResult();
	bool GetSensorResult();
	bool GetPcbResult();
	bool GetHwResult();

	void SetVerStandard(std::string sw, std::string fw, std::string ui, std::string sensor, std::string pcb, std::string hw);

private:
	std::string sw_Version;
	std::string fw_Version;
	std::string ui_Version;
	std::string sensor_Version;
	std::string pcb_Version;
	std::string hw_Version;

	bool b_is_cam_cal;
	bool b_sw_result;
	bool b_fw_result;
	bool b_ui_result;
	bool b_sensor_result;
	bool b_pcb_result;
	bool b_hw_result;
};

