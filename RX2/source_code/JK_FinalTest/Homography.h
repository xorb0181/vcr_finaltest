/*
 * Homography.h
 *
 *  Created on: 2015. 7. 6.
 *      Author: jmk
 */

#ifndef HOMOGRAPHY_H_
#define HOMOGRAPHY_H_
#include "opencv2/opencv.hpp"
#include <iostream>
#include <stdio.h>
using namespace cv;
using namespace std;
class Homography {
public:
    Homography();
    virtual ~Homography();
    Mat homography(Mat input,vector<Point2f>P1);
    void calConfirm(Mat H, vector<Point2f> P1, vector<Point2f> P2);
    Mat warpImage(Mat input,Mat H,Size2i inputSize);
private:
    int RoI_X_Start;
    int RoI_X_End;
    int RoI_Y_Start;
    int RoI_Y_End;
};

#endif /* HOMOGRAPHY_H_ */
