#include "stdafx.h"
#include "GyroTest.h"
#include <algorithm>
#include <fstream>
#define PI 3.141592
const char* config_location = ".\\diag_config.ini";
const char* standard_location = ".\\diag_standard_rx2.ini";
Homography homography;

enum enum_gyro_test
{
	SELF_DIAGNOSIS_GYRO_PREPARE,
	SELF_DIAGNOSIS_GYRO_WAIT_ACK,
	SELF_DIAGNOSIS_GYRO_CAL_RUN1,
	SELF_DIAGNOSIS_GYRO_CAL_RUN2,
	SELF_DIAGNOSIS_GYRO_CAL_RUN3,
	SELF_DIAGNOSIS_GYRO_CAL_SCALE_FACTOR,
	SELF_DIAGNOSIS_GYRO_TEST_RUN1,
	SELF_DIAGNOSIS_GYRO_TEST_RUN2,
	SELF_DIAGNOSIS_GYRO_TEST_RUN3,
	SELF_DIAGNOSIS_GYRO_TEST_PRE_RESULT,
	SELF_DIAGNOSIS_GYRO_TEST_RESULT,
};

bool sort_Func(const pair<float, float>&A, const pair<float, float>&B)
{
	return A.second < B.second;
}
CGyroTest::CGyroTest()
	:tagDetector(NULL)
	, first_angle(0)
	, second_angle(0)
	, third_angle(0)
	, order_count(0)
	, camera_param(0)
	, test_state(0)
	, result_counter(0)
{
	//default :: 36h11
	tagDetector = new AprilTags::TagDetector(AprilTags::tagCodes36h11);
	homography_exist = GetPrivateProfileInt(_T("system"), "exist_homography", 0, config_location);
	if (homography_exist == 1)
	{
		camera_param = GetPrivateProfileInt("system", "cam_num", 0, config_location);

		homography_point[0].x = GetPrivateProfileInt("system", "x0", 0, config_location);
		homography_point[0].y = GetPrivateProfileInt("system", "y0", 0, config_location);

		homography_point[1].x = GetPrivateProfileInt("system", "x1", 0, config_location);
		homography_point[1].y = GetPrivateProfileInt("system", "y1", 0, config_location);

		homography_point[2].x = GetPrivateProfileInt("system", "x2", 0, config_location);
		homography_point[2].y = GetPrivateProfileInt("system", "y2", 0, config_location);

		homography_point[3].x = GetPrivateProfileInt("system", "x3", 0, config_location);
		homography_point[3].y = GetPrivateProfileInt("system", "y3", 0, config_location);
		gyro_l_standard = GetPrivateProfileInt("Gyro___L", "standard", 0, standard_location);
		gyro_r_standard = GetPrivateProfileInt("Gyro___R", "standard", 0, standard_location);
		gyro_o_standard = GetPrivateProfileInt("Gyro___O", "standard", 0, standard_location);
		gyro_l_standard /= 10.0;
		gyro_r_standard /= 10.0;
		gyro_o_standard /= 10.0;
		//printf("l : %f, r : %f, o : %f \n",-gyro_l_standard,-gyro_r_standard,-gyro_o_standard);
		CTime time = CTime::GetCurrentTime();
		gyro_result_location = ".\\result\\gyrodata_" + to_string(time.GetYear()) + to_string(time.GetMonth()) + to_string(time.GetDay()) + ".txt";
	}
	else
	{
		AfxMessageBox("homography를 계산해주세요. homography가 존재하지 않습니다.");
	}
}


CGyroTest::~CGyroTest()
{
}


bool CGyroTest::Process(bool& result, int& ack, int& rotate_cmd, double& scale_factor, string& result_data, bool& recursive)
{
	bool return_val = false;
	if (homography_exist == 0)
	{
		AfxMessageBox("homography를 계산해주세요. homography가 존재하지 않습니다.");
		result = false;
		return true;
	}
	//exist homography
	else
	{
		Mat warped_image;
		Mat get_image;
		if (vc.isOpened())
		{
			vc >> get_image;
			if (get_image.empty())
			{
				printf("[WebCam] Cannot get the Image!! maybe camera need to fix!! [1] \n");
				vc.release();
				bool recursive_val = false;
				if (ack != 0)
				{
					recursive = true;
				}
				return recursive_val;
			}
			vector<Point2f> P1;
			for (int i = 0; i < 4; ++i)
			{
				P1.push_back(homography_point[i]);
			}
			//get homography
			H = homography.homography(get_image, P1);
			warped_image = homography.warpImage(get_image, H, cv::Size(get_image.cols, get_image.rows));
			imshow("gyro_test", warped_image);
			waitKey(3);
		}
		else
		{
			printf("[WebCam] Cannot get the Image!! maybe camera need to fix!! [2] \n");
			vc.open(camera_param);
			if (vc.isOpened())
			{
				vc.set(CV_CAP_PROP_FRAME_WIDTH, 1920);
				vc.set(CV_CAP_PROP_FRAME_HEIGHT, 1080);

				vc >> get_image;
				//prepare to get homography matrix
				vector<Point2f> P1;
				for (int i = 0; i < 4; ++i)
				{
					P1.push_back(homography_point[i]);
				}
				//get homography
				H = homography.homography(get_image, P1);
			}
			bool recursive_val = false;
			if (ack != 0)
			{
				recursive = true;
			}
			return recursive_val;
		}

		switch (test_state)
		{
		case SELF_DIAGNOSIS_GYRO_PREPARE:
		{
			//if we wanna prepare for gyro test, we can prepare this case.
			test_state = SELF_DIAGNOSIS_GYRO_WAIT_ACK;
		}
		break;
		case SELF_DIAGNOSIS_GYRO_WAIT_ACK:
		{
			switch (ack)
			{
			case 1:
				ack = 0;
				if (!first_try)
				{
					test_state = SELF_DIAGNOSIS_GYRO_CAL_RUN1;
				}
				else
				{
					test_state = SELF_DIAGNOSIS_GYRO_TEST_RUN1;
				}
				break;
			case 2:
				ack = 0;
				if (!first_try)
				{
					test_state = SELF_DIAGNOSIS_GYRO_CAL_RUN2;
				}
				else
				{
					test_state = SELF_DIAGNOSIS_GYRO_TEST_RUN2;
				}
				break;
			case 3:
				ack = 0;
				if (!first_try)
				{
					test_state = SELF_DIAGNOSIS_GYRO_CAL_RUN3;
				}
				else
				{
					test_state = SELF_DIAGNOSIS_GYRO_TEST_RUN3;
				}
				break;
			}
		}
		break;
		case SELF_DIAGNOSIS_GYRO_CAL_RUN1:
		{
			Point2i points[4];
			Point2i center;
			if (FindAprilTags(warped_image, points, center))
			{
				order_count = 0;
				CalculateAngle(warped_image, points, center,1);
				rotate_cmd = 1;
				test_state = SELF_DIAGNOSIS_GYRO_WAIT_ACK;
			}
		}
		break;
		case SELF_DIAGNOSIS_GYRO_CAL_RUN2:
		{
			Point2i points[4];
			Point2i center;
			if (FindAprilTags(warped_image, points, center))
			{
				order_count = 1;
				CalculateAngle(warped_image, points, center,2);
				rotate_cmd = 2;
				test_state = SELF_DIAGNOSIS_GYRO_WAIT_ACK;
			}
		}
		break;
		case SELF_DIAGNOSIS_GYRO_CAL_RUN3:
		{
			Point2i points[4];
			Point2i center;
			if (FindAprilTags(warped_image, points, center))
			{
				order_count = 2;
				CalculateAngle(warped_image, points, center,3);
				rotate_cmd = 3;
				test_state = SELF_DIAGNOSIS_GYRO_CAL_SCALE_FACTOR;
			}
		}
		break;
		case SELF_DIAGNOSIS_GYRO_CAL_SCALE_FACTOR:
		{
			first_try = true;
			static int sf_counter = 0;
			if (sf_counter == 0)
			{
				double first_scale_factor = ((second_robot_angle - first_robot_angle) + 720.0) / ((second_angle - first_angle) + 720.0);
				double second_scale_factor = (720.0 - (third_robot_angle - second_robot_angle)) / (720.0 - (third_angle - second_angle));
				first_scale_factor = 1 / first_scale_factor;
				second_scale_factor = 1 / second_scale_factor;
				cout << "first scale factor >>" << first_scale_factor << endl;
				cout << "second scale factor >>" << second_scale_factor << endl;
				double scalefactor = (first_scale_factor + second_scale_factor) / 2.0;
				scale_factor = scalefactor;
			}
			printf("sf_counter >> %d\n", sf_counter);
			if (++sf_counter > 30)
			{
				test_state = SELF_DIAGNOSIS_GYRO_TEST_RUN1;
				sf_counter = 0;
				SetPose(0, 0);
			}
		}
		break;
		case SELF_DIAGNOSIS_GYRO_TEST_RUN1:
		{
			Point2i points[4];
			Point2i center;
			if (FindAprilTags(warped_image, points, center))
			{
				order_count = 0;
				CalculateAngle(warped_image, points, center,1);
				rotate_cmd = 1;
				test_state = SELF_DIAGNOSIS_GYRO_WAIT_ACK;
			}
		}
		break;
		case SELF_DIAGNOSIS_GYRO_TEST_RUN2:
		{
			Point2i points[4];
			Point2i center;
			if (FindAprilTags(warped_image, points, center))
			{
				order_count = 1;
				CalculateAngle(warped_image, points, center,2);
				rotate_cmd = 2;
				test_state = SELF_DIAGNOSIS_GYRO_WAIT_ACK;
			}
		}
		break;
		case SELF_DIAGNOSIS_GYRO_TEST_RUN3:
		{
			Point2i points[4];
			Point2i center;
			if (FindAprilTags(warped_image, points, center))
			{
				order_count = 2;
				CalculateAngle(warped_image, points, center,3);
				rotate_cmd = 3;
				test_state = SELF_DIAGNOSIS_GYRO_TEST_PRE_RESULT;
			}
		}
		break;
		case SELF_DIAGNOSIS_GYRO_TEST_PRE_RESULT:
		{
			result = GyroResult();
			if (result)
			{
				return_val = true;
			}
			else
			{
				result_counter++;
				printf("result counter >>> %d \n", result_counter);
				if (result_counter == 3)
				{
					result = false;
					return_val = true;
				}
				else
				{
					test_state = SELF_DIAGNOSIS_GYRO_CAL_SCALE_FACTOR;
				}
			}
		}
		break;

		}

	}
	return return_val;
}


bool CGyroTest::Pre_Process(bool calculate)
{
	VideoCapture cap(camera_param);
	cap.set(CV_CAP_PROP_FRAME_WIDTH, 1920);
	cap.set(CV_CAP_PROP_FRAME_HEIGHT, 1080);
	cap.set(CV_CAP_PROP_SETTINGS, 1);
	if (!cap.isOpened()) 
	{
		cerr << "ERROR: Can't find video device \n";
		AfxMessageBox("카메라가 연결되어 있는지 확인하세요.");
		return false;
	}
	//for homography calculation
	if (calculate)
	{
		bool loop_breaker = true;
		while (loop_breaker)
		{
			cap >> origin_img;
			if (FindAprilTags(origin_img, homography_point, true))
			{
				PointOrderbyConner(homography_point);
				//prepare to get homography matrix
				vector<Point2f> P1;
				for (int i = 0; i < 4; ++i)
				{
					P1.push_back(homography_point[i]);
					//circle(origin_img, homography_point[i], 2, Scalar(255, 100, 100));
				}
				/* for debug
				imshow("image", origin_img);
				waitKey(10);
				continue;
				*/
				//get homography
				H = homography.homography(origin_img, P1);
				//warping
				Mat warped_image = homography.warpImage(origin_img, H, cv::Size(origin_img.cols, origin_img.rows));
				
				//verify homography
				Point2i verify_homo[4];
				if (FindAprilTags(warped_image, verify_homo, cxy_point))
				{
					//printf("검증까진 들어옴\n");
					PointOrderbyConner(verify_homo);
					bool result = VerifyHomography(verify_homo);
					if (result)
					{
						WritePrivateProfileString(_T("system"), _T("x0"), to_string(homography_point[0].x).c_str(), config_location);
						WritePrivateProfileString(_T("system"), _T("y0"), to_string(homography_point[0].y).c_str(), config_location);
						WritePrivateProfileString(_T("system"), _T("x1"), to_string(homography_point[1].x).c_str(), config_location);
						WritePrivateProfileString(_T("system"), _T("y1"), to_string(homography_point[1].y).c_str(), config_location);
						WritePrivateProfileString(_T("system"), _T("x2"), to_string(homography_point[2].x).c_str(), config_location);
						WritePrivateProfileString(_T("system"), _T("y2"), to_string(homography_point[2].y).c_str(), config_location);
						WritePrivateProfileString(_T("system"), _T("x3"), to_string(homography_point[3].x).c_str(), config_location);
						WritePrivateProfileString(_T("system"), _T("y3"), to_string(homography_point[3].y).c_str(), config_location);
						WritePrivateProfileString(_T("system"), _T("exist_homography"), "1", config_location);
						homography_exist = 1;
						loop_breaker = false;
						return true;
					}
				}
			}
			imshow("image", origin_img);
			int key;
			key = waitKey(15);
		}
	}
	else
	{
		for (int i = 0; i < 4; i++)
		{
			cout << i << homography_point[i] << endl;
		}

		vector<Point2f> P1;
		for (int i = 0; i < 4; ++i) {
			P1.push_back(homography_point[i]);
		}
		//get homography
		H = homography.homography(origin_img, P1);
		bool loop_breaker = true;
		while (loop_breaker)
		{
			cap >> origin_img;
			Mat warped_image = homography.warpImage(origin_img, H, cv::Size(origin_img.cols, origin_img.rows));
			imshow("image", warped_image);
			int key;
			key = waitKey(15);

			//verify homography
			Point2i verify_homo[4];
			if (FindAprilTags(warped_image, verify_homo, cxy_point))
			{
				PointOrderbyConner(verify_homo);
				bool result = VerifyHomography(verify_homo);
				if (result)
				{
					return true;
				}
			}
		}
	}
	return false;
}
bool CGyroTest::FindAprilTags(Mat& image, Point2i *points, bool draw)
{
	bool return_val = false;
	//we need gray image to detect tags
	Mat image_gray;
	cv::cvtColor(image, image_gray, CV_BGR2GRAY);
	int count = 0;
	vector<AprilTags::TagDetection> detections = tagDetector->extractTags(image_gray);

	//we can do many things by using detected tag
	for (int i = 0; i < detections.size(); i++)
	{
		//for getting homography
		if (detections[i].id == 10)
		{
			points[0].x = detections[i].cxy.first;
			points[0].y = detections[i].cxy.second;
			count++;
			if (draw)
			{
				detections[i].draw(image);
			}
		}
		else if (detections[i].id == 11)
		{
			points[1].x = detections[i].cxy.first;
			points[1].y = detections[i].cxy.second;
			count++;
			if (draw)
			{
				detections[i].draw(image);
			}
		}
		else if (detections[i].id == 12)
		{
			points[2].x = detections[i].cxy.first;
			points[2].y = detections[i].cxy.second;
			count++;
			if (draw)
			{
				detections[i].draw(image);
			}
		}
		else if (detections[i].id == 13)
		{
			points[3].x = detections[i].cxy.first;
			points[3].y = detections[i].cxy.second;
			count++;
			if (draw)
			{
				detections[i].draw(image);
			}
		}
	}
	if (count == 4)
	{
		return_val = true;
	}
	return return_val;
}
bool CGyroTest::FindAprilTags(Mat& image, Point2i *points, Point2i& center)
{
	bool return_val = false;
	//we need gray image to detect tags
	Mat image_gray;
	cv::cvtColor(image, image_gray, CV_BGR2GRAY);

	vector<AprilTags::TagDetection> detections = tagDetector->extractTags(image_gray);

	//we can do many things by using detected tag
	for (int i = 0; i < detections.size(); i++)
	{
		//for angle or tracking
		if (detections[i].id == 4)
		{
			for (int j = 0; j < 4; j++)
			{
				points[j].x = detections[i].p[j].first;
				points[j].y = detections[i].p[j].second;
				center.x = detections[i].cxy.first;
				center.y = detections[i].cxy.second;
			}
			return_val = true;
		}
	}
	return return_val;
}
// 순서에 따라 정렬. y값으로 우선 정렬하고 x값으로 정렬
void CGyroTest::PointOrderbyConner(Point2i* inPoints) 
{
	vector<pair<int, int> > s_point;
	for (int i = 0; i < 4; ++i)
	{
		s_point.push_back(make_pair(inPoints[i].x, inPoints[i].y));
	}
	//y 값 오름차순 정렬
	std::sort(s_point.begin(), s_point.end(), sort_Func);

	//x 값 오름차순 정렬
	if (s_point[0].first < s_point[1].first) 
	{
		inPoints[0].x = s_point[0].first;
		inPoints[0].y = s_point[0].second;

		inPoints[1].x = s_point[1].first;
		inPoints[1].y = s_point[1].second;
	}
	else 
	{
		inPoints[0].x = s_point[1].first;
		inPoints[0].y = s_point[1].second;

		inPoints[1].x = s_point[0].first;
		inPoints[1].y = s_point[0].second;
	}

	if (s_point[2].first > s_point[3].first) 
	{
		inPoints[2].x = s_point[2].first;
		inPoints[2].y = s_point[2].second;

		inPoints[3].x = s_point[3].first;
		inPoints[3].y = s_point[3].second;

	}
	else 
	{
		inPoints[2].x = s_point[3].first;
		inPoints[2].y = s_point[3].second;

		inPoints[3].x = s_point[2].first;
		inPoints[3].y = s_point[2].second;
	}
}

bool CGyroTest::VerifyHomography(Point2i* points)
{
	double p0_dist[4];
	int count = 0;
	p0_dist[0] = sqrt(pow(points[1].x - points[0].x, 2) + pow(points[1].y - points[0].y, 2));
	p0_dist[1] = sqrt(pow(points[3].x - points[0].x, 2) + pow(points[3].y - points[0].y, 2));
	p0_dist[2] = sqrt(pow(points[2].x - points[1].x, 2) + pow(points[2].y - points[1].y, 2));
	p0_dist[3] = sqrt(pow(points[3].x - points[2].x, 2) + pow(points[3].y - points[2].y, 2));
	double d;
	d = abs(p0_dist[0] - p0_dist[1]);
	if (d < 5.0)
	{
		cout <<"1 :"<< d << endl;
		count++;
	}
	d = abs(p0_dist[0] - p0_dist[2]);
	if (d < 5.0)
	{
		cout << "2 :" << d << endl;
		count++;
	}
	d = abs(p0_dist[0] - p0_dist[3]);
	if (d < 5.0)
	{
		cout << "3 :" << d << endl;
		count++;
	}
	d = abs(p0_dist[1] - p0_dist[2]);
	if (d < 5.0)
	{
		cout << "4 :" << d << endl;
		count++;
	}
	d = abs(p0_dist[1] - p0_dist[3]);
	if (d < 5.0)
	{
		cout << "5 :" << d << endl;
		count++;
	}
	d = abs(p0_dist[2] - p0_dist[3]);
	if (d < 5.0)
	{
		cout << "6 :" << d << endl;
		count++;
	}
	
	if (count > 3)
	{
//		printf("검증 성공!");
		return true;
	}
//	printf("검증 실패");
	return false;
}


bool CGyroTest::CalculateAngle(Mat& image, Point2i* point, Point2i cxy, int order)
{
	//convert Image coordinate to XY coordinate
	Image2XyC(image, point[0]);
	Image2XyC(image, point[1]);
	Image2XyC(image, point[2]);
	Image2XyC(image, point[3]);
	Image2XyC(image, cxy);
	Point2i center;
	center.x = (point[0].x + point[1].x) / 2;
	center.y = (point[0].y + point[1].y) / 2;
	
	if (order == 1){
		first_angle = atan2(center.y - cxy.y, center.x - cxy.x);
		first_angle = first_angle*180.0 / PI;
		cout << "first angle :::  " << first_angle << endl;
		order_count++;
		return true;
	}
	else if (order == 2)
	{
		second_angle = atan2(center.y - cxy.y, center.x - cxy.x);
		second_angle = second_angle*180.0 / PI;
		cout << "second angle :::  " << second_angle << endl;
		order_count++;
		return true;
	}
	else if (order == 3)
	{
		third_angle = atan2(center.y - cxy.y, center.x - cxy.x);
		third_angle = third_angle*180.0 / PI;
		cout << "third angle :::  " << third_angle << endl;
		return true;
	}
	return false;
}


void CGyroTest::Image2XyC(Mat& image,Point2i& input)
{
	input.x = input.x - image.cols/2;
	input.y = image.rows/2 - input.y;
	return;
}

double CGyroTest::CalculateScaleFactor()
{
	double original_angle_cam = 0.0;
	double orignal_angle_robot = first_robot_angle;
	double first_angle_cam = second_angle - first_angle;
	double first_angle_robot = second_robot_angle;
	double second_angle_cam = third_angle - first_angle;
	double second_angle_robot = third_robot_angle;
	double result_1 = (first_angle_robot - orignal_angle_robot) - (first_angle_cam - original_angle_cam);
	double result_2 = (second_angle_robot - first_angle_robot) - (second_angle_cam - first_angle_cam);
	double result_3 = (second_angle_robot - orignal_angle_robot) - (second_angle_cam - original_angle_cam);
}

bool CGyroTest::GyroResult()
{
	/*
	double original_angle_cam = 0.0;
	double orignal_angle_robot = first_robot_angle;
	double first_angle_cam = second_angle - first_angle;
	double first_angle_robot = second_robot_angle;
	double second_angle_cam = third_angle - first_angle;
	double second_angle_robot = third_robot_angle;
	double result_1 = (first_angle_robot - orignal_angle_robot) - (first_angle_cam - original_angle_cam);
	double result_2 = (second_angle_robot - first_angle_robot) - (second_angle_cam - first_angle_cam);
	double result_3 = (second_angle_robot - orignal_angle_robot) - (second_angle_cam - original_angle_cam);
	*/

	double result_1 = (second_robot_angle - first_robot_angle) - (second_angle - first_angle);
	double result_2 = (third_robot_angle - second_robot_angle) - (third_angle - second_angle);
	double result_3 = (third_robot_angle - first_robot_angle) - (third_angle - first_angle);
	bool gyro_result = true;
	if (result_3 > gyro_o_standard || result_3 < -gyro_o_standard)
	{
		gyro_result = false;
	}
	if (result_2 > gyro_r_standard || result_2 < -gyro_r_standard)
	{
		gyro_result = false;
	}
	if (result_1 > gyro_l_standard || result_1 < -gyro_l_standard)
	{
		gyro_result = false;
	}
	cout << ">>> Angle information" << endl;
	cout << ">>> Gyro output : " << first_robot_angle << ", " << second_robot_angle << ", " << third_robot_angle << endl;
	cout << ">>> Estimated : " << first_angle << ", " << second_angle << ", " << third_angle << endl;
	cout << ">>> Angle Difference" << endl;
	cout << ">>> TurningLeft " << "         : " << result_1 << " = " << (second_robot_angle - first_robot_angle) << " - " << (second_angle - first_angle) << endl;
	cout << ">>> TurningRight " << "        : " << result_2 << " = " << (third_robot_angle - second_robot_angle) << " - " << (third_angle - second_angle) << endl;
	cout << ">>> TurningOrigin " << "       : " << result_3 << " = " << (third_robot_angle - first_robot_angle) << " - " << (third_angle - first_angle) << endl;
	cout << ">>> Error ratio" << endl;
	cout << ">>> rotation angles: " << to_string(720.0 + second_robot_angle - first_robot_angle) << ", " << to_string(-720.0 + third_robot_angle - second_robot_angle) << endl;
	cout << ">>> rotation anglesE: " << to_string(720.0 + second_angle - first_angle) << ", " << to_string(-720.0 + third_angle - second_angle) << endl;
	cout << ">>> Left : " << to_string((720.0 + second_angle - first_angle) / (720.0 + second_robot_angle - first_robot_angle)) << endl;
	cout << ">>> Right : " << to_string((-720.0 + third_angle - second_angle) / (-720.0 + third_robot_angle - second_robot_angle)) << endl;
	cout << "++GTEST_ORIGIN: " << result_3 << "  ++GTEST_LEFT: " << result_1 << "  ++GTEST_RIGHT: " << result_2 << " = " << gyro_result << endl;

	angle_data = to_string(first_robot_angle) + " " + to_string(second_robot_angle) + " " + to_string(third_robot_angle) + " "
		+ to_string(first_angle) + " " + to_string(second_angle) + " " + to_string(third_angle) + " "
		+ to_string(result_1) + " " + to_string(result_2) + " " + to_string(result_3);

	std::ofstream gyro_out(gyro_result_location, ios::app);
	if (gyro_result)
	{
		gyro_out  << angle_data << endl;
		cout << "GYRO TEST SUCCESS!!!" << endl;
		angle_data = "send [JK_TEST]0 10 49 " + to_string((int)(first_angle * 100000)) + " " + to_string((int)(second_angle * 100000)) + " " + to_string((int)(third_angle * 100000)) + " " + to_string((int)(first_robot_angle * 100000)) + " " + to_string((int)(second_robot_angle * 100000)) + " " + to_string((int)(third_robot_angle * 100000)) + " " + to_string((int)(result_1 * 100000)) + " " + to_string((int)(result_2 * 100000)) + " " + to_string((int)(result_3 * 100000)) + " \n";
	}
	else
	{
		gyro_out << angle_data << endl;
		cout << "GYRO TEST FAIL..." << endl;
		angle_data = "send [JK_TEST]0 10 49 " + to_string((int)(first_angle * 100000)) + " " + to_string((int)(second_angle * 100000)) + " " + to_string((int)(third_angle * 100000)) + " " + to_string((int)(first_robot_angle * 100000)) + " " + to_string((int)(second_robot_angle * 100000)) + " " + to_string((int)(third_robot_angle * 100000)) + " " + to_string((int)(result_1 * 100000)) + " " + to_string((int)(result_2 * 100000)) + " " + to_string((int)(result_3 * 100000)) + " \n";
	}
	return gyro_result;
}


void CGyroTest::SetPose(int order, double angle)
{
	switch (order)
	{
	case 0:
		first_robot_angle = angle;
		break;
	case 1:
		second_robot_angle = angle;
		break;
	case 2:
		third_robot_angle = angle;
		break;
	}
	return;
}


void CGyroTest::initialize()
{
	first_angle=0;
	second_angle=0;
	third_angle=0;
	first_robot_angle=0;
	second_robot_angle=0;
	third_robot_angle=0;
	order_count=0;
	test_state = 0;
	angle_data.clear();
	first_try = false;
	result_counter = 0;
	destroyAllWindows();
	if (vc.isOpened())
	{
		vc.release();
	}
}

