#pragma once
#include "TagDetector.h"
#include "Tag36h11.h"
#include "Homography.h"
#include <iostream>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <cstdio>
#include <Eigen>
using namespace cv;

class CGyroTest
{
public:
	CGyroTest();
	~CGyroTest();

	//gyro test
	bool Process(bool& result, int& ack, int& rotate_cmd, double& scale_factor, string& result_data, bool& recursive);
	int test_state;
	/*전처리 단계
	카메라 캘리브레이션과 perspective transform을 수행
	*/
	bool Pre_Process(bool calculate);


	//it can find april tags and process
	//it decide tag's usage by id
	//id 4 : tracking or calculating its heading angle
	//id 10~13 : getting 4 points and get homography
	//-----------------------------------------------
	//return value
	//true : find tag
	//false : cannot find tag
	//-----------------------------------------------
	//flag : 1 = find main tag (id = 4) 2 = find homography tag (id = 10 ~ 13)
	bool FindAprilTags(Mat& image, Point2i *points, Point2i& center);
	bool FindAprilTags(Mat& image, Point2i *points, bool draw);
	AprilTags::TagDetector* tagDetector;
	void PointOrderbyConner(Point2i* inPoints);
	int homography_exist;
	VideoCapture vc;
private:
	bool first_try;
	Mat origin_img;
	Mat H;
	Point2i point4angle[4];
	Point2i cxy_point;
	int camera_param;
	double gyro_r_standard;
	double gyro_l_standard;
	double gyro_o_standard;
public:
	Point2i homography_point[4];
	bool VerifyHomography(Point2i* points);
	double first_angle;
	double second_angle;
	double third_angle;
	double first_robot_angle;
	double second_robot_angle;
	double third_robot_angle;

	int order_count;
	bool CalculateAngle(Mat& image, Point2i* point, Point2i cxy,int order);
	double CalculateScaleFactor();
	void Image2XyC(Mat& image, Point2i& input);
	bool GyroResult();
	void SetPose(int order, double angle);
	void initialize();

	string gyro_result_location;
	string angle_data;

	double origin_ref;
	double left_ref;
	double right_ref;

	int result_counter;
};

