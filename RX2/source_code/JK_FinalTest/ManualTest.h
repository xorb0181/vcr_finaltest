#pragma once
class CManualTest
{
public:
	CManualTest();
	~CManualTest();

	void initialize();

	bool Process(msg4queue msg);
	int m_test_state;
	int GetDustSensorResult();
	int GetDustBinResult();
	int GetTopdoorResult();
	int GetDcjackResult();
	int* GetTopbumperResult();

	void DustSensor_Test(msg4queue& msg);
	void DustBin_Test(msg4queue& msg);
	void TopDoor_Test(msg4queue& msg);
	void DcJack_Test();
	void TopBumper_Test(msg4queue& msg);

private:
	int b_dust_sensor;
	int b_dust_bin;
	int b_top_door;
	int b_dc_jack;
	int b_top_bumper[5];
};

