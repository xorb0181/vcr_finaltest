#ifndef CUSTOM_CALIBRATION_HPP_
#define CUSTOM_CALIBRATION_HPP_

#include <cv.h>
#include <iostream>
#include <string>
#include <sstream>
#include <vector>

class char2string
{
public:
	char2string() {}
	void operator() ( const char * src, std::string & dst )
	{
		std::stringstream ss;
		ss << src;
		ss >> dst;
	}
};

class chessboardDetector
{
public:
	chessboardDetector( const double & Ox=160, const double & Oy=120, const double & focalLength = 234 ) : board_w(8), board_h(6), board_total(board_w*board_h),
		n_boards(1), cell_size(0.0227), fx(focalLength), fy(focalLength), cx(Ox), cy(Oy), verbose(true)
	{}

	void setOthers( int boardW, int boardH, const double & cellSize, bool verboseOption )
	{
		board_w = boardW;
		board_h = boardH;
		board_total = board_h * board_w;
		cell_size = cellSize;
		verbose = verboseOption;
	}

  void enableLogging(bool enable)
  {
    verbose = enable;
  }

	//bool run( std::string & fileName, double & estimatedHeadingAngle );
	bool run( IplImage * srcImage, double & estimatedHeadingAngle );

//protected:
	int board_w;
	int board_h;	
	int board_total;
	int n_boards;
	double cell_size;
	double fx, fy, cx,cy;
	bool verbose;

public:
	double	average_reprojection_error;
	int		no_found_corners;
	double estiamted_heading_angle;
	double fit_error;
};


#endif //CUSTOM_CALIBRATION_HPP_