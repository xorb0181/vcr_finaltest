#ifndef SELF_DIAG_HPP_
#define SELF_DIAG_HPP_

#include <SSTREAM>

#define CONSOLE_DATA_SIZE       1024*22
#define BUFFER_SIZE					    1024*64
#define RESULT_VALUE_LENGTH			20
#define DIAG_PRINT_TAG_LENGTH		11 + 1
#define DIAG_PRINT_SUB_LENGTH		11 + 1
#define DIAG_INI_LENGTH				  8 + 1
#define DIAG_GTEST					    51
#define DIAG_RESULT					    99

enum eModelVersion
{
	model_arte,
	model_miele,
	model_pop,
	model_pop_lowcost,
	model_arte25,
	model_arte30, 
	model_pluto
};

enum eNotifyMessage
{
	ACTION_CAMERA = 9001,
	ACTION_DIAG,
	ACTION_IMAGE,
	ACTION_GTEST,
	ACTION_GET_BATTERY,
	ACTION_GOTO_TESTPOINT,
	ACTION_QUIT_DIAG,
	ACTION_INIT_PIC,
	ACTION_BACKWARD_OUT,
	ACTION_SET_MODEL_MAIN,
	ACTION_SET_MODEL_SUB,
	ACTION_SET_RESERV_DISABLE,
	ACTION_GET_BATTERY_AGAIN,
	ACTION_GET_ANGLE_X,
	ACTION_GET_ANGLE_Y,
	ACTION_GET_CHARGER_STATE,
	ACTION_CLEAR_RTC,
	ACTION_WAIT_FOR_FINISH,
	ACTION_SET_TIME,
	ACTION_GET_TIME,
	ACTION_INIT_PROCESS,
	ACTION_ENTER_KEY,
	ACTION_DISABLE_RETRY,
	ACTION_TEST_DIAG,
	ACTION_GET_PSD,
	ACTION_SET_SN,
	ACTION_GET_SN,
	ACTION_SET_DIAGTIME,
	ACTION_VERSION_TEST,
	
	INFO_ANGLE = 10001,
	INFO_MOTION,
	INFO_GOT_CPUID,
	INFO_GOT_BATTERY,
	INFO_DIAG_START,
	INFO_DIAG_END,
	INFO_MODEL_SUB,
	INFO_VERSION,
	INFO_PSD_CHECK_NEXT,
	INFO_PSD_CHECK_END,
	INFO_GOT_IMAGE,
	INFO_SERIAL_NUMBER,
	
	ERR_ANGLE = 11001,
	ERR_TIME,
	ERR_BUMPER,
	ERR_HALL,
	ERR_PSD,
	ERR_ANGLE_X,
	ERR_ANGLE_Y,
	ERR_CAMERA_CALIBRATION,
	ERR_CAMERA_INFORMATION,
};

#define INI_LOCATION_RESULT			".\\diag_result.ini"

#define JPEG_SIZE					    "[jpeg size]"
#define JPEG_DATA					    "[jpeg data]"
#define DIAG_TAG					    "[Self_Diag]"

#define HEADER_MAIN_ANGLE			"[GyroAngle]"
#define HEADER_MAIN_MOTION		"[Motion]"
#define HEADER_GET_ID				  "[get_serial]"
#define HEADER_GET_BATTERY		"[get_battery]"
#define HEADER_GET_ANGX				"[AngX]"
#define HEADER_GET_ANGY				"[AngY]"
#define HEADER_GET_DCJACK		"[dcJackCharging]"

#define DIAG_PRINT_VERSION              "VERSION_"
#define DIAG_PRINT_VERSION_FIRMWARE		"VER___FW"
#define DIAG_PRINT_VERSION_UI		    "VER___UI"
#define DIAG_PRINT_VERSION_SENSOR		"VER___SS"
#define DIAG_PRINT_VERSION_HARDWARE		"VER___HW"
#define DIAG_PRINT_VERSION_PCB    		"VER__PCB"
#define DIAG_PRINT_CHARGER_ON		    "Char__On"
#define DIAG_PRINT_CHARGER_OFF		    "Char_Off"
#define DIAG_PRINT_BRUSH				      "Brush___"
#define DIAG_PRINT_SIDEBRUSH_LEFT		  "SBrush_L"
#define DIAG_PRINT_SIDEBRUSH_RIGHT		"SBrush_R"
#define DIAG_PRINT_VACUUM				      "Vacuum__"
#define DIAG_PRINT_CPSD_RIGHT			    "CPSD___0"
#define DIAG_PRINT_CPSD_CENTER			  "CPSD___1"
#define DIAG_PRINT_CPSD_LEFT			    "CPSD___2"
#define DIAG_PRINT_PSD_RIGHT			    "PSD____0"
#define DIAG_PRINT_PSD_CENTER			    "PSD____1"
#define DIAG_PRINT_PSD_LEFT				    "PSD____2"
#define DIAG_PRINT_PSD_RIGHT_TOP		    "PSD____3"
#define DIAG_PRINT_PSD_CENTER_TOP		    "PSD____4"
#define DIAG_PRINT_PSD_LEFT_TOP			    "PSD____5"
#define DIAG_PRINT_PE_FORWARD			    "For__Err"
#define DIAG_PRINT_FORWARD_LEFT			  "For____L"
#define DIAG_PRINT_FORWARD_RIGHT		  "For____R"
#define DIAG_PRINT_PE_BACKWARD			  "Back_Err"
#define DIAG_PRINT_BACKWARD_LEFT		  "Back___L"
#define DIAG_PRINT_BACKWARD_RIGHT		  "Back___R"
#define DIAG_PRINT_IR_FRONT				    "IR_Front"
#define DIAG_PRINT_IR_DOCKING			    "IR__Dock"
#define DIAG_PRINT_IR_FRONT_RIGHT		"FrIr___R"
#define DIAG_PRINT_IR_FRONT_CENTER		"FrIr___C"
#define DIAG_PRINT_IR_FRONT_LEFT		"FrIr___L"
#define DIAG_PRINT_IR_FRONT_EX0		    "IrEx___0"
#define DIAG_PRINT_IR_FRONT_EX1		    "IrEx___1"
#define DIAG_PRINT_IR_FRONT_EX2		    "IrEx___2"
#define DIAG_PRINT_IR_FRONT_EX3		    "IrEx___3"
#define DIAG_PRINT_IR_DOCKING_RIGHT		"doIr___R"
#define DIAG_PRINT_IR_DOCKING_CENTER	"doIr___C"
#define DIAG_PRINT_IR_DOCKING_LEFT		"doIr___L"
#define DIAG_PRINT_DISTANCE_PE			  "Dist__PE"
#define DIAG_PRINT_DISTANCE_WHEEL		  "Dist__Wh"
#define DIAG_PRINT_SERIAL_NUMBER		  "SerialNo"
#define DIAG_PRINT_CHARGING				    "Charging"
#define DIAG_PRINT_HALL					      "Hall____"
#define DIAG_PRINT_DUST_SENSOR			      "dUSt_Sen"
#define DIAG_PRINT_DUST					      "dUSt____"
#define DIAG_PRINT_NOZZLE				      "nozzle__"
#define DIAG_PRINT_MOP					      "mOP_____"
#define DIAG_PRINT_DCJACK				      "DCJack__"
#define DIAG_PRINT_ANGLE_XY				    "ANGLE_XY"
#define DIAG_PRINT_GYRO_LEFT			    "Gyro___L"
#define DIAG_PRINT_GYRO_RIGHT			    "Gyro___R"
#define DIAG_PRINT_GYRO_ORIGIN			  "Gyro___O"

#define GTEST_LEFT						        "TurningLeft :"
#define GTEST_RIGHT						        "TurningRight:"
#define GTEST_ORIGIN					        "TurningOrigin"

#define BOOT_LOG_INIREADER				    "[INI Reader   ]"
#define BOOT_LOG_CSV113GRABBER			  "[CSV113Grabber]"
#define BOOT_LOG_INTEGRATOR				    "[Integrator   ]"
#define BOOT_LOG_MPTOOL_APP				    "[MPTool:App   ]"
#define BOOT_LOG_MPTOOL_APPLIB			  "[MPTool:AppLib]"
#define BOOT_LOG_MPTOOL_APPINI			  "[MPTool:AppIni]"
#define BOOT_LOG_CSERIALLINUX			    "[CSerialLinux ]"
#define BOOT_LOG_CAMERA					      "./camera.ini:"
#define BOOT_LOG_CAMERA_MTD2			    "/dev/mtd2:"
#define BOOT_LOG_CAMERA_OPTX			    "optX"
#define BOOT_LOG_CAMERA_OPTY			    "optY"
#define BOOT_LOG_CAMERA_F				      "f"
#define BOOT_LOG_CAMERA_CAMX			    "camX"
#define BOOT_LOG_CAMERA_CAMY			    "camY"
#define BOOT_LOG_CAMERA_ANG				    "ang"
#define BOOT_LOG_CAMERA_R				      "R"
#define BOOT_LOG_CAMERA_SCALE			    "scale"
#define BOOT_LOG_MPTOOL_VER				    "Ver  :"
#define BOOT_LOG_MPTOOL_MODEL			    "Model:"
#define BOOT_LOG_MPTOOL_RES				    "Res:"

#define CHECK_MAIN_MODEL        "[INI Reader   ] /dev/mtd2: main_model = "
#define CHECK_SUB_MODEL         "[INI Reader   ] /dev/mtd2: sub_model = "
//#define CHECK_SUB_MODEL         "[INI Reader   ] /dev/mtd2: model_sub = "
//#define CHECK_RIGHT_PSD         "[INI Reader   ] /root/mtd2.ini: [psd] right_adjust = "
//#define CHECK_CENTER_PSD        "[INI Reader   ] /root/mtd2.ini: [psd] center_adjust = "
//#define CHECK_LEFT_PSD          "[INI Reader   ] /root/mtd2.ini: [psd] left_adjust = "
#define CHECK_RIGHT_PSD         "[psd] right_adjust = "
#define CHECK_CENTER_PSD        "[psd] center_adjust = "
#define CHECK_LEFT_PSD          "[psd] left_adjust = "
//#define CHECK_RIGHT_TOP_PSD     "[psd] right_top_adjust = "
//#define CHECK_CENTER_TOP_PSD    "[psd] center_top_adjust = "
//#define CHECK_LEFT_TOP_PSD      "[psd] left_top_adjust = "

#define INI_KEY_DEFAULT   "default"
#define INI_KEY_MIN       "min"
#define INI_KEY_MAX       "max"

enum eGetBatteryLevelState
{
	eNone,
		eGetFirstBatteryLevel,
		eGetSecondBatteryLevel,
		eGetSecondBatteryLevelEx,
		eGetDifferenceBatteryLevel,
		eGetBatteryLevelTest,
		eGetChargerState,
		eFinished,
};

enum enum_main_tag
{
	ENUM_MAIN_DIAG,
		ENUM_MAIN_INIREADER,
		ENUM_MAIN_CAMERA,
		ENUM_MAIN_CSV113GRABBER,
		ENUM_MAIN_INTEGRATOR,
		ENUM_MAIN_MPTOOL_APP,
		ENUM_MAIN_MPTOOL_APPLIB,
		ENUM_MAIN_MPTOOL_APPINI,
		ENUM_MAIN_CSERIALLINUX,
		ENUM_MAIN_TAG_LENGTH
};

enum enum_sub_tag_camera
{
	ENUM_SUB_CAMERA,
		ENUM_SUB_CAMERA_OPTX,
		ENUM_SUB_CAMERA_OPTY,
		ENUM_SUB_CAMERA_F,
		ENUM_SUB_CAMERA_CAMX,
		ENUM_SUB_CAMERA_CAMY,
		ENUM_SUB_CAMERA_ANG,
		ENUM_SUB_CAMERA_R,
		ENUM_SUB_CAMERA_SCALE,
		ENUM_SUB_CAMERA_LENGTH
};

enum enum_sub_tag_mptool
{
	ENUM_SUB_MPTOOL_APP_VER,
		ENUM_SUB_MPTOOL_APP_MODEL,
		ENUM_SUB_MPTOOL_APPLIB_VER,
		//	ENUM_SUB_MPTOOL_APPINI_RES,
		ENUM_SUB_MPTOOL_LENGTH
};

enum diagnosis_list
{
	ENUM_DIAG_SERIAL_NUMBER, 
	ENUM_DIAG_RTC,
	ENUM_DIAG_DCJACK,
	ENUM_DIAG_MOP,
	ENUM_DIAG_DUST_SENSOR,
	ENUM_DIAG_DUST, //5
	ENUM_DIAG_NOZZLE,
	ENUM_DIAG_VERSION,
	ENUM_DIAG_VERSION_FIRMWARE,
	ENUM_DIAG_VERSION_UI,
	ENUM_DIAG_VERSION_SENSOR, //10
	ENUM_DIAG_VERSION_HARDWARE,
	ENUM_DIAG_VERSION_PCB,
	ENUM_DIAG_CHARGER_OFF,
	ENUM_DIAG_BRUSH,
	ENUM_DIAG_SIDEBRUSH_LEFT, //15
	ENUM_DIAG_SIDEBRUSH_RIGHT,
	ENUM_DIAG_VACUUM,
	ENUM_DIAG_PSD_RIGHT,
	ENUM_DIAG_PSD_CENTER,
	ENUM_DIAG_PSD_LEFT, //20
	ENUM_DIAG_PSD_LEFT_TOP,
	ENUM_DIAG_PSD_CENTER_TOP,
	ENUM_DIAG_PSD_RIGHT_TOP,
	ENUM_DIAG_IR_FRONT_RIGHT,
	ENUM_DIAG_IR_FRONT_CENTER, //25
	ENUM_DIAG_IR_FRONT_LEFT,
	ENUM_DIAG_IR_FRONT_EX0,
	ENUM_DIAG_IR_FRONT_EX1, //28
	ENUM_DIAG_IR_FRONT_EX2, //29
	ENUM_DIAG_IR_FRONT_EX3, //30
	ENUM_DIAG_IR_DOCKING_RIGHT,
	ENUM_DIAG_IR_DOCKING_CENTER,
	ENUM_DIAG_IR_DOCKING_LEFT,
	ENUM_DIAG_FORWARD_LEFT,
	ENUM_DIAG_FORWARD_RIGHT,
	ENUM_DIAG_BACKWARD_LEFT,
	ENUM_DIAG_BACKWARD_RIGHT,
	ENUM_DIAG_PE_FORWARD,
	ENUM_DIAG_PE_BACKWARD,
	ENUM_DIAG_HALL,
	ENUM_DIAG_CHARGER_ON,
	ENUM_DIAG_CHARGING,
	ENUM_DIAG_GTEST_LEFT,
	ENUM_DIAG_GTEST_RIGHT,
	ENUM_DIAG_GTEST_ORIGIN,
	ENUM_DIAG_DCJACK_SIGNAL,
	ENUM_DIAG_RESULT,
	ENUM_DIAG_LENGTH
};

enum stateOfCheckingSevenIR
{
	IR7_standby,
		IR7_left,
		IR7_left_1st,
		IR7_left_2nd,
		IR7_center,
		IR7_right_2nd,
		IR7_right_1st,
		IR7_right,
		IR7_finish,
};

typedef void(*callback_notice)(int diag_list, bool result);
typedef void(*callback_triple)(int diag_list, int result);
typedef void(*callback_boot)(int main, int sub, std::string str_res, bool b_res);

//class diagnosis_common
//{
//public:
//	diagnosis_common()	{}
//
//	int charp_size(char* _p)
//	{
//		int _n = 0;
//		while(_p[_n++] != NULL)	{}
//		return _n - 1;
//	}
//};
//}; 

class CCompareString
{
public:
	CCompareString() {}
	~CCompareString() {}
	
private:
	std::map<const char*, unsigned int> mList;
};

class diagnosis_element
{
protected:
	int		_value, _min, _max, _default;
	
#ifdef FAB_NUM_READER
	std::string		_value_str;
#endif	

	//	char	_tag_sub[DIAG_PRINT_SUB_LENGTH];
	//	char	_tag_ini[DIAG_INI_LENGTH];
	std::string	_sub;
	std::string _ini;
	
	int	_id;
	
	bool is_set_;
	bool result_;
	
public:
	diagnosis_element()					
	{	initialize();				}
	
	void setResult(bool result) { result_ = result; }
	bool getResult() { return result_; }
	
	virtual void setValue(int val)
	{
		_value = val;
		is_set_ = true;
	}

#ifdef FAB_NUM_READER
	virtual void setValue(std::string val)
	{
		_value_str = val;
		is_set_ = true;
	}
#endif

	virtual void setDefault(int val)		{	_default	= val;	}
	virtual void setId(int id)				{	_id			= id;	}
	virtual void setMin(int val)			{	_min		= val;	}
	virtual void setMax(int val)			{	_max		= val;	}
	
	//	virtual void set_sub(char* val)			{	sprintf(_tag_sub, "%s", val);	}
	//	virtual void set_ini(char* val)			{	sprintf(_tag_ini, "%s", val);	}
	virtual void setTag(std::string& val)
	{
		_sub = addBracket(val);
		_ini = val;
	}
	
	virtual int getValue()					{	return _value;		}
	virtual int getDefault()				{	return _default;	}
	virtual int getMin()					{	return _min;		}
	virtual int getMax()					{	return _max;		}
	
	virtual std::string& getSub()			{	return _sub;		}
	virtual std::string& getIni()			{	return _ini;		}
	
	virtual int getId()					{	return _id;			}
	
	bool isSetValue() { return is_set_; }
	
	std::string& addBracket(std::string& val)
	{
		val.insert(0, "[");
		val.append("]");
		return val;
	}
	
	virtual void initialize()
	{
		setValue(0);
		setDefault(0);
		setMin(0);
		setMax(0);
		setId(0);
		setResult(false);
		
		is_set_ = false;
	}
	
	void initializeResult()
	{
		setValue(0);
		setResult(false);
		
		is_set_ = false;
	}
};

class BootData
{
public:
	
	BootData() : value_(0.0f), set_(false)  {}
	~BootData(){}
	
	void setValue(float value)
	{
		if (set_)
		{
			printf(">>> this element was set before.\n");
		}
		value_ = value;
		set_ = true;
	}
	
	float getValue() { return value_; }
	
	bool isSet() { return set_; }
	
	void initialize()
	{
		value_ = 0;
		set_ = false;
	}
	
protected:
private:
	
	float value_;
	bool set_;
};

class diagnosis_report
{
	//	================================================================================================================
	//	MEMBER VARIABLE
	//	================================================================================================================
	
public:
	
	diagnosis_element	_elem[ENUM_DIAG_LENGTH];
	diagnosis_element	_elemGyro[3];
	diagnosis_element	_elemAngle;
	
	std::string			boot_mptool[ENUM_SUB_MPTOOL_LENGTH];
	//	float				    boot_camera[ENUM_SUB_CAMERA_LENGTH];
	bool				    boot_csv113;
	int					    boot_gyro_type;
	
	BootData      boot_camera_[ENUM_SUB_CAMERA_LENGTH];
	
	int   cpu_id;
	int		battery[3];
	int		_timeout;
	int   psd_chk_[2];
	int   psd_chk_woc_[2];
	int   psd_top_chk_[2];
	int   psd_center_top_chk_[2];
	int   ir_chk_[2];
	
	double  gtest_left,	gtest_right, gtest_origin;
	//bool		_result[ENUM_DIAG_LENGTH];
	
	//	@sj - 2012. 6. 8. Fri.
	//	model number sub == 1
	
	int   modelNumberSub;
	int   modelNumberMain;
	
	char pcb_version_[5];
	
	std::string   got_pcb_version;
	std::string   ini_location;
	
	float   gyro_info[3];
	float   gyro_detail[18][3];
	
	//	================================================================================================================
	//	MEMBER FUNCTION
	//	================================================================================================================
	
public:
	
	diagnosis_report() : gtest_left(0), gtest_right(0), gtest_origin(0), cpu_id(0), _timeout(0)
	{
		int n;
		
		for(n=0; n < ENUM_SUB_CAMERA_LENGTH; n++)	{ boot_camera_[n].initialize();  }
		for(n=0; n < ENUM_SUB_MPTOOL_LENGTH; n++)	{ boot_mptool[n] = "";    }
		
		boot_csv113 = false;
		boot_gyro_type = 0;
		
		ini_location = ".\\diag_standard_arte.ini";
	}
	
	void setIniLocation(std::string location_value)
	{
		ini_location = location_value;
		initializeOnce();
		initializeElements();
	}
	
	void initializeOnce()
	{
		//printf(">>> initializeOnce() \n");
		for(int n = 0; n < ENUM_DIAG_LENGTH; n++)
		{
			//_result[n] = false;
			
			switch(n)
			{
			case ENUM_DIAG_VERSION_PCB:
				break;
				
			default:
				{
					std::string _tag = getTag( n );
					
					_elem[n].setId( n );
					_elem[n].setValue( 0 );
					_elem[n].setMin( GetPrivateProfileInt( _tag.c_str(), "min", 0, ini_location.c_str()) );
					_elem[n].setMax( GetPrivateProfileInt( _tag.c_str(), "max", 0, ini_location.c_str()) );
					_elem[n].setDefault( GetPrivateProfileInt( _tag.c_str(), "default", 0, ini_location.c_str()) );
					_elem[n].setTag(_tag);
				}
				break;
			}
			
			//diagnosis_list id = i2list(n);
			
			std::string _tag = getTag(n);
			//			WritePrivateProfileString( _tag.c_str(), "res", "0", INI_LOCATION_RESULT);
			//printf(">>> [%2d] %3d, %3d, %3d, %s\n", n, _elem[n].get_min(), _elem[n].get_max(), _elem[n].get_default(), _tag.c_str());
		}
		
		_elemGyro[0].setMin( GetPrivateProfileInt( DIAG_PRINT_GYRO_LEFT, INI_KEY_MIN, 0, ini_location.c_str()) );
		_elemGyro[1].setMin( GetPrivateProfileInt( DIAG_PRINT_GYRO_RIGHT, INI_KEY_MIN, 0, ini_location.c_str()) );
		_elemGyro[2].setMin( GetPrivateProfileInt( DIAG_PRINT_GYRO_ORIGIN, INI_KEY_MIN, 0, ini_location.c_str()) );
		
		_elemGyro[0].setMax( GetPrivateProfileInt( DIAG_PRINT_GYRO_LEFT, INI_KEY_MAX, 0, ini_location.c_str()) );
		_elemGyro[1].setMax( GetPrivateProfileInt( DIAG_PRINT_GYRO_RIGHT, INI_KEY_MAX, 0, ini_location.c_str()) );
		_elemGyro[2].setMax( GetPrivateProfileInt( DIAG_PRINT_GYRO_ORIGIN, INI_KEY_MAX, 0, ini_location.c_str()) );
		
		_elemAngle.setMin( GetPrivateProfileInt( DIAG_PRINT_ANGLE_XY, INI_KEY_MIN, 0, ini_location.c_str()) );
		_elemAngle.setMax( GetPrivateProfileInt( DIAG_PRINT_ANGLE_XY, INI_KEY_MAX, 0, ini_location.c_str()) );
		
		modelNumberSub = GetPrivateProfileInt( "sub_model", INI_KEY_DEFAULT, 0, ini_location.c_str());
		modelNumberMain = GetPrivateProfileInt( "main_model", INI_KEY_DEFAULT, 0, ini_location.c_str());
		
		psd_chk_[0] = GetPrivateProfileInt( "psd__chk_wc", "clear", 0, ini_location.c_str());
		psd_chk_[1] = GetPrivateProfileInt( "psd__chk_wc", "block", 0, ini_location.c_str());

		psd_chk_woc_[0] = GetPrivateProfileInt( "psd__chk_woc", "clear", 0, ini_location.c_str());
		psd_chk_woc_[1] = GetPrivateProfileInt( "psd__chk_woc", "block", 0, ini_location.c_str());

		psd_top_chk_[0] = GetPrivateProfileInt( "psdT_chk", "clear", 0, ini_location.c_str());
		psd_top_chk_[1] = GetPrivateProfileInt( "psdT_chk", "block", 0, ini_location.c_str());

		psd_center_top_chk_[0] = GetPrivateProfileInt( "psdCT_chk", "clear", 0, ini_location.c_str());
		psd_center_top_chk_[1] = GetPrivateProfileInt( "psdCT_chk", "block", 0, ini_location.c_str());
		
		ir_chk_[0] = GetPrivateProfileInt( "ir___chk", "clear", 0, ini_location.c_str());
		ir_chk_[1] = GetPrivateProfileInt( "ir___chk", "block", 0, ini_location.c_str());
		
#if ARTE_VERSION > 123
		int rtc_default = GetPrivateProfileInt( "RTC_____", INI_KEY_DEFAULT, 0, ini_location.c_str());
		_elem[ENUM_DIAG_RTC].setDefault(rtc_default);
#endif
		
		GetPrivateProfileString( DIAG_PRINT_VERSION_PCB, INI_KEY_DEFAULT, " ", pcb_version_, 5, ini_location.c_str() );
		//printf(">>> pcb_version_ = %s \n", pcb_version_);
		
		for(n=0; n<3; n++)	{	battery[n] = 0;	}
	}
	
	void initializeElements()
	{
		int n = 0;
		
		for(n=0; n < ENUM_DIAG_LENGTH; n++)
		{
			_elem[n].initializeResult();
			//_result[n] = false;
		}
		
		for(n=0; n<3; n++)
		{
			_elemGyro[n].initializeResult();
			battery[n] = 0;
			gyro_info[n] = 0.0f;
		}
		
		for(n=0; n < ENUM_SUB_CAMERA_LENGTH; n++)
		{
			boot_camera_[n].initialize();
		}
		
		for(n=0; n < ENUM_SUB_MPTOOL_LENGTH; n++)
		{
			boot_mptool[n] = "";
		}
		
		for(n=0; n<18; n++)
		{
			for (int nn=0; nn<3; nn++)
			{
				gyro_detail[n][nn] = 0;
			}
		}
		
		//_elemAngle.setValue(0);
		_elemAngle.initializeResult();
		got_pcb_version = "";
		
		gtest_left = gtest_right = gtest_origin = 0;
		cpu_id = _timeout = 0;
		boot_gyro_type = 0;
		
		boot_csv113 = false;
	}
	
	void setElemResult(int diag_id, bool result) { _elem[diag_id].setResult(result); }
	bool getElemResult(int diag_id) { return _elem[diag_id].getResult(); }
	
	int getElemValue(int diag_id)			{	return	_elem[diag_id].getValue();		}
	int getElemDefault(int diag_id)		{	return	_elem[diag_id].getDefault();	}
	int getElemMin(int diag_id)			{	return	_elem[diag_id].getMin();		}
	int getElemMax(int diag_id)			{	return	_elem[diag_id].getMax();		}
	
	bool isSetElemValue(int diag_id)  { return _elem[diag_id].isSetValue(); }
	
	void getPsdCheckThreshold(int& clear, int& block)
	{
		clear = psd_chk_[0];
		block = psd_chk_[1];
	}

	void getPsdCheckThreshold(int& clear, int& block, int& clear_woc, int& block_woc)
	{
		clear = psd_chk_[0];
		block = psd_chk_[1];
		clear_woc = psd_chk_woc_[0];
		block_woc = psd_chk_woc_[1];
	}

	void getPsdTopCheckThreshold(int& clear, int& block)
	{
		clear = psd_top_chk_[0];
		block = psd_top_chk_[1];
	}

	void getPsdCenterTopCheckThreshold(int& clear, int& block)
	{
		clear = psd_center_top_chk_[0];
		block = psd_center_top_chk_[1];
	}
	
	void getIRCheckThreshold(int& clear, int& block)
	{
		clear = ir_chk_[0];
		block = ir_chk_[1];
	}
	
	std::string& getElemSub(int diag_id)	{	return	_elem[diag_id].getSub();		}
	std::string& getElemIni(int diag_id)	{	return	_elem[diag_id].getIni();		}
	
	void setElemValue(int diag_id, int val)
	{
		_elem[diag_id].setValue(val);
		
		std::ostringstream strm;
		strm << val;
		WritePrivateProfileString( getTag(diag_id), "res", strm.str().c_str(), INI_LOCATION_RESULT);	
	}

#ifdef FAB_NUM_READER
	void setElemValue(int diag_id, std::string& val)
	{
		_elem[diag_id].setValue(val);
		
		std::ostringstream strm;
		strm << val;
		WritePrivateProfileString( getTag(diag_id), "res", strm.str().c_str(), INI_LOCATION_RESULT);	
	}
#endif
	
	//	================================================================================================================
	//	MEMBER FUNCTION FOR DEFINE
	//	================================================================================================================
	
	char* getTag(int val)
	{
		switch(val)
		{
		case ENUM_DIAG_SERIAL_NUMBER:        return DIAG_PRINT_SERIAL_NUMBER;
		case ENUM_DIAG_RTC:                  return "RTC_____";
		case ENUM_DIAG_DUST_SENSOR:			 return DIAG_PRINT_DUST_SENSOR;
		case ENUM_DIAG_DUST:				 return DIAG_PRINT_DUST;
		case ENUM_DIAG_NOZZLE:				 return DIAG_PRINT_NOZZLE;
		case ENUM_DIAG_DCJACK:				 return DIAG_PRINT_DCJACK;
		case ENUM_DIAG_MOP:					 return DIAG_PRINT_MOP;
		case ENUM_DIAG_VERSION:				 return DIAG_PRINT_VERSION;
		case ENUM_DIAG_VERSION_FIRMWARE: 	 return DIAG_PRINT_VERSION_FIRMWARE;
		case ENUM_DIAG_VERSION_UI: 	         return DIAG_PRINT_VERSION_UI;
		case ENUM_DIAG_VERSION_SENSOR: 	     return DIAG_PRINT_VERSION_SENSOR;
		case ENUM_DIAG_VERSION_HARDWARE: 	 return DIAG_PRINT_VERSION_HARDWARE;
			//    case ENUM_DIAG_VERSION_PCB:       return DIAG_PRINT_VERSION_PCB;
		case ENUM_DIAG_CHARGER_ON:			  return DIAG_PRINT_CHARGER_ON;
		case ENUM_DIAG_CHARGER_OFF:			  return DIAG_PRINT_CHARGER_OFF;
		case ENUM_DIAG_BRUSH:				  return DIAG_PRINT_BRUSH;
		case ENUM_DIAG_SIDEBRUSH_LEFT:		  return DIAG_PRINT_SIDEBRUSH_LEFT;
		case ENUM_DIAG_SIDEBRUSH_RIGHT:		  return DIAG_PRINT_SIDEBRUSH_RIGHT;
		case ENUM_DIAG_VACUUM:				  return DIAG_PRINT_VACUUM;
		case ENUM_DIAG_PSD_RIGHT:			  return DIAG_PRINT_PSD_RIGHT;
		case ENUM_DIAG_PSD_CENTER:			  return DIAG_PRINT_PSD_CENTER;
		case ENUM_DIAG_PSD_LEFT:			  return DIAG_PRINT_PSD_LEFT;
		case ENUM_DIAG_PSD_RIGHT_TOP:		  return DIAG_PRINT_PSD_RIGHT_TOP;
		case ENUM_DIAG_PSD_CENTER_TOP:		  return DIAG_PRINT_PSD_CENTER_TOP;
		case ENUM_DIAG_PSD_LEFT_TOP:		  return DIAG_PRINT_PSD_LEFT_TOP;
		case ENUM_DIAG_FORWARD_LEFT:		  return DIAG_PRINT_FORWARD_LEFT;
		case ENUM_DIAG_FORWARD_RIGHT:		  return DIAG_PRINT_FORWARD_RIGHT;
		case ENUM_DIAG_BACKWARD_LEFT:		  return DIAG_PRINT_BACKWARD_LEFT;
		case ENUM_DIAG_BACKWARD_RIGHT:	      return DIAG_PRINT_BACKWARD_RIGHT;
		case ENUM_DIAG_PE_FORWARD:			  return DIAG_PRINT_PE_FORWARD;
		case ENUM_DIAG_PE_BACKWARD:			  return DIAG_PRINT_PE_BACKWARD;
		case ENUM_DIAG_IR_FRONT_RIGHT:		  return DIAG_PRINT_IR_FRONT_RIGHT;
		case ENUM_DIAG_IR_FRONT_CENTER:		  return DIAG_PRINT_IR_FRONT_CENTER;
		case ENUM_DIAG_IR_FRONT_LEFT:		  return DIAG_PRINT_IR_FRONT_LEFT;
		case ENUM_DIAG_IR_FRONT_EX0:          return DIAG_PRINT_IR_FRONT_EX0;
		case ENUM_DIAG_IR_FRONT_EX1:          return DIAG_PRINT_IR_FRONT_EX1;
		case ENUM_DIAG_IR_FRONT_EX2:          return DIAG_PRINT_IR_FRONT_EX2;
		case ENUM_DIAG_IR_FRONT_EX3:          return DIAG_PRINT_IR_FRONT_EX3;
		case ENUM_DIAG_IR_DOCKING_RIGHT:	  return DIAG_PRINT_IR_DOCKING_RIGHT;
		case ENUM_DIAG_IR_DOCKING_CENTER:	  return DIAG_PRINT_IR_DOCKING_CENTER;
		case ENUM_DIAG_IR_DOCKING_LEFT:		  return DIAG_PRINT_IR_DOCKING_LEFT;
		case ENUM_DIAG_CHARGING:			  return DIAG_PRINT_CHARGING;
		case ENUM_DIAG_HALL:				  return DIAG_PRINT_HALL;
			
		default:							  return "UNDEFINED";
		}
	}
	
	char* getMainTag(int val)
	{
		switch(val)
		{
		case ENUM_MAIN_DIAG:			    return DIAG_TAG;				break;
			//		case ENUM_MAIN_CAMERA:			return BOOT_LOG_CAMERA;			break;
		case ENUM_MAIN_CSV113GRABBER:	return BOOT_LOG_CSV113GRABBER;	break;
		case ENUM_MAIN_INIREADER:		  return BOOT_LOG_INIREADER;		break;
		case ENUM_MAIN_INTEGRATOR:		return BOOT_LOG_INTEGRATOR;		break;
		case ENUM_MAIN_MPTOOL_APP:		return BOOT_LOG_MPTOOL_APP;		break;
		case ENUM_MAIN_MPTOOL_APPLIB:	return BOOT_LOG_MPTOOL_APPLIB;	break;
		case ENUM_MAIN_MPTOOL_APPINI:	return BOOT_LOG_MPTOOL_APPINI;	break;
		case ENUM_MAIN_CSERIALLINUX:	return BOOT_LOG_CSERIALLINUX;	break;
			
		default:					return	"";						break;
		}
	}
	
	char* getSubTagAboutCamera(int val)
	{
		switch(val)
		{
		case ENUM_SUB_CAMERA:		    return BOOT_LOG_CAMERA;
		case ENUM_SUB_CAMERA_OPTX:	return BOOT_LOG_CAMERA_OPTX;	
		case ENUM_SUB_CAMERA_OPTY:	return BOOT_LOG_CAMERA_OPTY;	
		case ENUM_SUB_CAMERA_F:		  return BOOT_LOG_CAMERA_F;
		case ENUM_SUB_CAMERA_CAMX:	return BOOT_LOG_CAMERA_CAMX;
		case ENUM_SUB_CAMERA_CAMY:	return BOOT_LOG_CAMERA_CAMY;
		case ENUM_SUB_CAMERA_ANG:	  return BOOT_LOG_CAMERA_ANG;
		case ENUM_SUB_CAMERA_R:		  return BOOT_LOG_CAMERA_R;
		case ENUM_SUB_CAMERA_SCALE:	return BOOT_LOG_CAMERA_SCALE;
			
		default:					return "";
		}
	}
	
	char* getSubTagAboutMptool(int val)
	{
		switch(val)
		{
		case ENUM_SUB_MPTOOL_APP_VER:		  return BOOT_LOG_MPTOOL_VER;
		case ENUM_SUB_MPTOOL_APP_MODEL:		return BOOT_LOG_MPTOOL_MODEL;
		case ENUM_SUB_MPTOOL_APPLIB_VER:	return BOOT_LOG_MPTOOL_VER;
			//		case ENUM_SUB_MPTOOL_APPINI_RES:	return BOOT_LOG_MPTOOL_RES;
			
		default:							return "";
		}
	}
};

#endif