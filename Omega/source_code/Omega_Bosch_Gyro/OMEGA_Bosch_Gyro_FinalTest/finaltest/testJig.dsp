# Microsoft Developer Studio Project File - Name="testJig" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Application" 0x0101

CFG=testJig - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "testJig.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "testJig.mak" CFG="testJig - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "testJig - Win32 Release" (based on "Win32 (x86) Application")
!MESSAGE "testJig - Win32 Debug" (based on "Win32 (x86) Application")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "testJig - Win32 Release"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 5
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_AFXDLL" /Yu"stdafx.h" /FD /c
# ADD CPP /nologo /MT /W3 /GX /O2 /I "../3rdparty/ecl_lite_ready" /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_MBCS" /D "_SELF_DIAGNOSIS_" /D "OMEGA" /D "USE_BOSCH" /D "FAB_NUM_READER" /FR /FD /c
# SUBTRACT CPP /X /u /YX /Yc /Yu
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0xc09 /d "NDEBUG" /d "_AFXDLL"
# ADD RSC /l 0xc09 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /machine:I386
# ADD LINK32 cv.lib cxcore.lib highgui.lib /nologo /subsystem:windows /machine:I386 /out:"../bin/FinalTestR.exe"

!ELSEIF  "$(CFG)" == "testJig - Win32 Debug"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 6
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MDd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_AFXDLL" /Yu"stdafx.h" /FD /GZ /c
# ADD CPP /nologo /MDd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /D "_SELF_DIAGNOSIS_" /FR /YX /FD /GZ /c
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0xc09 /d "_DEBUG" /d "_AFXDLL"
# ADD RSC /l 0xc09 /d "_DEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /debug /machine:I386 /pdbtype:sept
# ADD LINK32 cv.lib cxcore.lib highgui.lib /nologo /subsystem:windows /debug /machine:I386 /out:"../bin/FinalTestD.exe" /pdbtype:sept

!ENDIF 

# Begin Target

# Name "testJig - Win32 Release"
# Name "testJig - Win32 Debug"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=.\PopUp.cpp
# End Source File
# Begin Source File

SOURCE=.\StdAfx.cpp
# ADD CPP /Yc"stdafx.h"
# End Source File
# Begin Source File

SOURCE=.\testJig.cpp
# End Source File
# Begin Source File

SOURCE=.\testJig.rc
# End Source File
# Begin Source File

SOURCE=.\testJigDlg.cpp
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl"
# Begin Source File

SOURCE=.\PopUp.h
# End Source File
# Begin Source File

SOURCE=.\Resource.h
# End Source File
# Begin Source File

SOURCE=.\StdAfx.h
# End Source File
# Begin Source File

SOURCE=.\testJig.h
# End Source File
# Begin Source File

SOURCE=.\testJigDlg.h
# End Source File
# End Group
# Begin Group "Resource Files"

# PROP Default_Filter "ico;cur;bmp;dlg;rc2;rct;bin;rgs;gif;jpg;jpeg;jpe"
# Begin Source File

SOURCE=.\res\bitmap1.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bitmap2.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bitmap3.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bitmap_3i_off.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bitmap_3i_on.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bitmap_f.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bitmap_res_fail.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bitmap_res_success.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bmp00001.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bmp00002.bmp
# End Source File
# Begin Source File

SOURCE=.\res\testJig.ico
# End Source File
# Begin Source File

SOURCE=.\res\testJig.rc2
# End Source File
# End Group
# Begin Group "module"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\modules\chessboard_detector.cpp
# End Source File
# Begin Source File

SOURCE=.\modules\chessboard_detector.hpp
# End Source File
# Begin Source File

SOURCE=.\modules\common_util.hpp
# End Source File
# Begin Source File

SOURCE=.\modules\dd_console.hpp
# End Source File
# Begin Source File

SOURCE=.\modules\gyro_tester.hpp
# End Source File
# Begin Source File

SOURCE=.\modules\image_util.hpp
# End Source File
# Begin Source File

SOURCE=.\modules\jpeg_decoder.h
# End Source File
# Begin Source File

SOURCE=.\modules\JUART2.H
# End Source File
# Begin Source File

SOURCE=.\modules\ls_line_fitting.hpp
# End Source File
# Begin Source File

SOURCE=.\modules\self_diag.hpp
# End Source File
# Begin Source File

SOURCE=.\modules\sv113_rectify.hpp
# End Source File
# Begin Source File

SOURCE=.\modules\txt2jpg.hpp
# End Source File
# End Group
# Begin Source File

SOURCE=.\ReadMe.txt
# End Source File
# End Target
# End Project
