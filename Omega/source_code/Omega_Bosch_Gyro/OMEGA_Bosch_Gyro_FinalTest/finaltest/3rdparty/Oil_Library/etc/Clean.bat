del ..\Bin\*.ilk

del ..\*.a /s
del ..\*.o /s
del ..\*.bsc /s
del ..\*.exp /s
del ..\*.pdb /s
del ..\*.tbl /s
del ..\*.ncb /s
del ..\*.aps /s
del ..\*.pch /s

rmdir ..\Temp /s /q
