// NetworkModule.h: interface for the CServerSock class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_NETWORKMODULE_H__DB291ACD_5014_4E85_BC9C_B8C93D342976__INCLUDED_)
#define AFX_NETWORKMODULE_H__DB291ACD_5014_4E85_BC9C_B8C93D342976__INCLUDED_

#include "../OilBase.h"

#include "../Oil_Thread/SingleThread.h"

#include "TCPPacket.h"
#include "ClientSock.h"

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class OILLIB_API CServerSock  
{
public:
	CServerSock(const char *strName = NULL);
	virtual ~CServerSock();

public:
	bool Create(int nPort);
	void Close();
	void SetCallBackOnAccept (void *pVoid, void (*cbOnAccept)(void *pVoid, int nErrorCode) );
	SOCKET AcceptSocket();

protected:
	static bool _cbNetworkLoop(void* pParam);
	       bool _OnNetworkLoop();

protected:
	SOCKET m_sockServer;
	SOCKET m_sockClient;
	CSingleThread m_Thread;
	void (*m_cbOnAccept)(void *pVoid, int nErrorCode);
	void  *m_pvOnAccept;
	int    m_nPort;
	bool   m_bAccepted;
};

#endif // !defined(AFX_NETWORKMODULE_H__DB291ACD_5014_4E85_BC9C_B8C93D342976__INCLUDED_)

