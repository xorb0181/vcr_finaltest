// SplitRecv.h: interface for the CSplitRecv class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_SPLITRECV_H__2781CAEB_BED4_4376_8678_36FB22368609__INCLUDED_)
#define AFX_SPLITRECV_H__2781CAEB_BED4_4376_8678_36FB22368609__INCLUDED_

#include "../OilBase.h"

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class OILLIB_API CSplitRecv  
{
public:
	CSplitRecv(int bufSize);
	virtual ~CSplitRecv();

public:
	int    recvTotalCnt;
	int    recvCurrent ;
	int    recvSize    ;
	char  *recvBuf;
	int    recvBufSize;

	bool PushData(const char *pBuf, int size);
	bool IsFull() { return (recvCurrent == recvTotalCnt) ? true : false; }
};

#endif // !defined(AFX_SPLITRECV_H__2781CAEB_BED4_4376_8678_36FB22368609__INCLUDED_)
