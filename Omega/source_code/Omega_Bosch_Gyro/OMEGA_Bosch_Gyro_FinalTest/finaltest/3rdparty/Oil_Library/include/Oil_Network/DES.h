#if !defined(AFX_DES_H__A863405E_C0F7_4AE8_83C9_EEAA4DD7F2CD__INCLUDED_)
#define AFX_DES_H__A863405E_C0F7_4AE8_83C9_EEAA4DD7F2CD__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

// DES.h : header file
//

int GetEncodeSize(int size);

void des_encode_alloc(const char *DesKey, const char *src, const int srcSize, char *&dst,       int &dstSize);
void des_decode_alloc(const char *DesKey,       char *src, const int srcSize, char *&dst, const int  dstSize);

void des_encode_replace(const char *DesKey, char *src_dst, const int size);
void des_decode_replace(const char *DesKey, char *src_des, const int size);

#ifndef _LINUX
CString EncodeDesHexaString(const char * DesKey, const CString &input);
CString DecodeDesHexaString(const char * DesKey, const CString &input);
#endif

int GetEncodedLength(int messageLen);

/////////////////////////////////////////////////////////////////////////////
#endif // !defined(AFX_DES_H__A863405E_C0F7_4AE8_83C9_EEAA4DD7F2CD__INCLUDED_)
