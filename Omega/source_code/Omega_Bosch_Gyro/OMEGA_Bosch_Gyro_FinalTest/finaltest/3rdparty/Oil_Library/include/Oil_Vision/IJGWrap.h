// IJGWrap.h: interface for the IJGWrap class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_IJGWRAP_H__43F29212_D6F3_4D6B_899A_5EBF3B3BA92F__INCLUDED_)
#define AFX_IJGWRAP_H__43F29212_D6F3_4D6B_899A_5EBF3B3BA92F__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "jpeglib.h"
#include "jinclude.h"
#include "jerror.h"

#include "assert.h"

#define INPUT_BUF_SIZE  4096

struct my_dst_mgr
{
	struct jpeg_destination_mgr pub; // public fields
	
	JOCTET * jpeg_buffer;
	int	     jpeg_buf_size;
	int    * jpeg_size;
};

struct my_src_mgr
{
	struct jpeg_source_mgr pub;	// public fields
	unsigned char * jpeg_buffer;
	int             jpeg_buf_size;
	int	          offset;

	JOCTET * buffer;		 // start of buffer
	boolean start_of_file; // have we gotten any data yet?
};

typedef my_src_mgr * my_src_ptr;
typedef my_dst_mgr * my_dst_ptr;

METHODDEF(void)
cb_init_destination (j_compress_ptr cinfo)
{
	my_dst_ptr dest = (my_dst_ptr) cinfo->dest;
	dest->pub.next_output_byte = dest->jpeg_buffer;
	dest->pub.free_in_buffer   = dest->jpeg_buf_size;
}

METHODDEF(boolean)
cb_empty_output_buffer (j_compress_ptr cinfo)
{
	my_dst_ptr dest = (my_dst_ptr) cinfo->dest;
	dest->pub.next_output_byte;
	dest->pub.free_in_buffer  ;
	return TRUE;
}

METHODDEF(void)
cb_term_destination (j_compress_ptr cinfo)
{
	my_dst_ptr dest = (my_dst_ptr) cinfo->dest;
	*(dest->jpeg_size) = dest->jpeg_buf_size - dest->pub.free_in_buffer;
}

METHODDEF(void)
cb_init_source (j_decompress_ptr cinfo)
{
  my_src_ptr src = (my_src_ptr) cinfo->src;
  src->start_of_file = TRUE;
}

METHODDEF(boolean)
cb_fill_input_buffer (j_decompress_ptr cinfo)
{
	my_src_ptr src	= (my_src_ptr) cinfo->src;
	size_t	 nbytes = 0;
	
	//  nbytes = JFREAD(src->infile, src->buffer, INPUT_BUF_SIZE);
	nbytes = ((src->offset + INPUT_BUF_SIZE) < src->jpeg_buf_size) ? INPUT_BUF_SIZE : src->jpeg_buf_size - src->offset;
	memcpy(src->buffer, src->jpeg_buffer + src->offset, nbytes);
	src->offset += nbytes;
	
	if (nbytes <= 0) {
		if (src->start_of_file)	/* Treat empty input file as fatal error */
			ERREXIT(cinfo, JERR_INPUT_EMPTY);
		WARNMS(cinfo, JWRN_JPEG_EOF);
		/* Insert a fake EOI marker */
		src->buffer[0] = (JOCTET) 0xFF;
		src->buffer[1] = (JOCTET) JPEG_EOI;
		nbytes = 2;
	}
	
	src->pub.next_input_byte = src->buffer;
	src->pub.bytes_in_buffer = nbytes;
	src->start_of_file = FALSE;
	
	return TRUE;
}

METHODDEF(void)
cb_skip_input_data (j_decompress_ptr cinfo, long num_bytes)
{
	my_src_ptr src = (my_src_ptr) cinfo->src;
	
	if (num_bytes > 0) 
	{
		while (num_bytes > (long) src->pub.bytes_in_buffer) 
		{
			num_bytes -= (long) src->pub.bytes_in_buffer;
			cb_fill_input_buffer(cinfo);
		}
		src->pub.next_input_byte += (size_t) num_bytes;
		src->pub.bytes_in_buffer -= (size_t) num_bytes;
	}
}

METHODDEF(void)
cb_term_source (j_decompress_ptr cinfo) {}

static void jpeg_stdio_dest(j_compress_ptr cinfo, unsigned char * jpegbuf, int &bufsize)
{
	my_dst_ptr dest;
	
	if (cinfo->dest == NULL)
	{
		cinfo->dest = (struct jpeg_destination_mgr *)
			(*cinfo->mem->alloc_small) 
			((j_common_ptr) cinfo, JPOOL_PERMANENT, SIZEOF(my_dst_mgr));
	}
	
	dest = (my_dst_ptr) cinfo->dest;
	dest->pub.init_destination    = cb_init_destination;
	dest->pub.empty_output_buffer = cb_empty_output_buffer;
	dest->pub.term_destination    = cb_term_destination;

	dest->jpeg_buffer   = jpegbuf;
	dest->jpeg_buf_size = bufsize;
	dest->jpeg_size     = &bufsize;
}

static void jpeg_stdio_src (j_decompress_ptr cinfo, unsigned char * jpegbuf, int  bufsize)
{
	my_src_ptr src;
	
	if (cinfo->src == NULL)
	{
		cinfo->src = (struct jpeg_source_mgr *) (*cinfo->mem->alloc_small) 
			((j_common_ptr) cinfo, JPOOL_PERMANENT, SIZEOF(my_src_mgr));
		src = (my_src_ptr) cinfo->src;
		src->buffer = (JOCTET *) (*cinfo->mem->alloc_small) 
			((j_common_ptr) cinfo, JPOOL_PERMANENT, INPUT_BUF_SIZE * SIZEOF(JOCTET));
	}
	
	src = (my_src_ptr) cinfo->src;
	src->pub.init_source       = cb_init_source;
	src->pub.fill_input_buffer = cb_fill_input_buffer;
	src->pub.skip_input_data   = cb_skip_input_data;
	src->pub.resync_to_restart = jpeg_resync_to_restart; /* use default method */
	src->pub.term_source       = cb_term_source;

	src->jpeg_buffer         = jpegbuf;
	src->jpeg_buf_size       = bufsize;
	src->offset              = 0;
	src->pub.bytes_in_buffer = 0;    /* forces fill_input_buffer on first read */
	src->pub.next_input_byte = NULL; /* until buffer loaded */
}


class CIJGWrap
{
public:
	static bool ImageToJpegMem (BYTE *dst, int &dstSize, const BYTE *src, int step, int width, int height, bool isColor, int quality)
	{
		assert(dst && dstSize && step && width && height);

		struct jpeg_compress_struct cinfo;
		struct jpeg_error_mgr jerr;
		cinfo.err = jpeg_std_error(&jerr);
		jpeg_create_compress(&cinfo);

		bool ret = ImageToJpegMem(&cinfo, dst, dstSize, src, step, width, height, isColor, quality);
		
		jpeg_destroy_compress(&cinfo);
		
		return ret;
	}

	static bool ImageToJpegMem (
		jpeg_compress_struct *cinfo, 
		BYTE *dst, int &dstSize, 
		const BYTE *src, int step, int width, int height, int channel, int quality)
	{
		assert(dst && dstSize && step && width && height);

		jpeg_stdio_dest(cinfo, dst, dstSize);
		
		cinfo->image_width      = width ;
		cinfo->image_height     = height;
		cinfo->input_components = (channel == 1) ? 1 : 3;
		cinfo->in_color_space   = (channel == 1) ? JCS_GRAYSCALE : JCS_RGB;

		jpeg_set_defaults(cinfo);
		jpeg_set_quality (cinfo, quality, TRUE /* limit to baseline-Jpeg values */);
		
		if (channel == 2)
			cinfo->jpeg_color_space = JCS_YCbCr16;

		cinfo->dct_method = JDCT_IFAST;
		jpeg_start_compress(cinfo, TRUE);
		while (cinfo->next_scanline < cinfo->image_height) 
		{
			BYTE * outRow = (BYTE *)(src + (cinfo->next_scanline * step));
			jpeg_write_scanlines(cinfo, &outRow, 1);
		}
		jpeg_finish_compress(cinfo);
		
		return true;
	}

	static bool ImageToJpegFile(const char * filename, const BYTE *src, int step, int width, int height, bool isColor, int quality)
	{
		struct jpeg_compress_struct cinfo;
		struct jpeg_error_mgr jerr;
		
		cinfo.err = jpeg_std_error(&jerr);
		jpeg_create_compress(&cinfo);
		bool ret = ImageToJpegFile(&cinfo, filename, src, step, width, height, isColor, quality);
		jpeg_destroy_compress(&cinfo);
		return true;
	}

	static bool ImageToJpegFile(jpeg_compress_struct *cinfo, const char * filename, const BYTE *src, int step, int width, int height, bool isColor, int quality)
	{
		assert(filename && step && width && height);

		FILE * outfile=fopen(filename, "wb");
		if ( !outfile) return false;

		jpeg_stdio_dest(cinfo, outfile);
		
		cinfo->image_width      = width ;
		cinfo->image_height     = height;
		cinfo->input_components = isColor ? 3 : 1;
		cinfo->in_color_space   = isColor ? JCS_RGB : JCS_GRAYSCALE;
		
		jpeg_set_defaults(cinfo);
		jpeg_set_quality (cinfo, quality, TRUE /* limit to baseline-Jpeg values */);
		
		jpeg_start_compress(cinfo, TRUE);
		while (cinfo->next_scanline < cinfo->image_height) 
		{
			BYTE * outRow = (BYTE *)(src + (cinfo->next_scanline * step));
			jpeg_write_scanlines(cinfo, &outRow, 1);
		}
		jpeg_finish_compress(cinfo);
		
		fclose(outfile);
		
		return true;
	}

	static void JpegMemToImageHeader(jpeg_decompress_struct * cinfo, BYTE * jpegBuf, UINT bufsize, int &width, int &height, int &channel)
	{
		jpeg_stdio_src  (cinfo, (BYTE *)jpegBuf, bufsize);
		jpeg_read_header(cinfo, TRUE);
		width   = cinfo->image_width ;
		height  = cinfo->image_height;
		channel = cinfo->num_components;
	}

	static bool JpegMemToImageBody  (jpeg_decompress_struct * cinfo, BYTE *dst, int step)
	{
		jpeg_start_decompress(cinfo);
		while (cinfo->output_scanline < cinfo->output_height) 
		{
			BYTE *outRow = dst + (cinfo->output_scanline * step);
			jpeg_read_scanlines(cinfo, &outRow, 1);
		}
		jpeg_finish_decompress(cinfo);
		return true;
	}

	static void JpegFileToImageHeader(jpeg_decompress_struct * cinfo, FILE * infile, int &width, int &height, int &channel)
	{
		jpeg_stdio_src  (cinfo, infile);
		jpeg_read_header(cinfo, TRUE);
		width   = cinfo->image_width ;
		height  = cinfo->image_height;
		channel = cinfo->num_components;
	}

	static bool JpegFileToImageBody(jpeg_decompress_struct * cinfo, FILE * infile, BYTE *dst, int step)
	{
		jpeg_start_decompress(cinfo);
		while (cinfo->output_scanline < cinfo->output_height) 
		{
			BYTE *outRow = dst + (cinfo->output_scanline * step);
			jpeg_read_scanlines(cinfo, &outRow, 1);
		}
		jpeg_finish_decompress(cinfo);
		return true;
	}

	/*
	static bool JpegMemToImage(BYTE * jpegBuf, UINT bufsize, ByteImage &image)
	{
		struct jpeg_decompress_struct cinfo;
		struct jpeg_error_mgr jerr;
		cinfo.err = jpeg_std_error(&jerr);
		jpeg_create_decompress(&cinfo);
		bool ret = JpegMemToImage(&cinfo, jpegBuf, bufsize, image);
		jpeg_destroy_decompress(&cinfo);
		return ret;
	}

	static bool JpegMemToImage(jpeg_decompress_struct * cinfo, BYTE * jpegBuf, UINT bufsize, ByteImage &image)
	{
		int width, height, channel;
		JpegMemToImageHeader(cinfo, jpegBuf, bufsize, width, height, channel);
		
		image.Allocate(width, height, channel);
		
		return JpegMemToImageBody(cinfo, image.GetBuffer(), image.GetWidthStep());
	}

	static bool JpegFileToImage(jpeg_decompress_struct * cinfo, const char * filename, ByteImage &image)
	{
		FILE * infile=fopen(filename, "r");
		if ( !infile) return false;

		int width, height, channel;
		JpegFileToImageHeader(cinfo, infile, width, height, channel);
		image.Allocate(width, height, channel);

		bool ret = JpegFileToImageBody(cinfo, infile, image.GetBuffer(), image.GetWidthStep());
		fclose(infile);

		return ret;
	}
	//*/
};


#endif // !defined(AFX_IJGWRAP_H__43F29212_D6F3_4D6B_899A_5EBF3B3BA92F__INCLUDED_)
