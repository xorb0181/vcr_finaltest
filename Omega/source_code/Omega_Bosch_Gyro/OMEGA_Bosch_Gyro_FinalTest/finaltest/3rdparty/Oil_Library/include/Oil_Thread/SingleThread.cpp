// SingleThread.cpp: implementation of the CSingleThread class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "SingleThread.h"
#include <stdio.h>

#ifdef _LINUX
	#include <pthread.h>
	#include <unistd.h>
	#define _msg printf
#else
//	#include <process.h>

// 	#ifdef _DEBUG
// 	#undef THIS_FILE
// 	static char THIS_FILE[]=__FILE__;
// 	#define new DEBUG_NEW
// 	#endif
#ifndef _WIN32_WCE
	#define _msg
#else
	#define _msg printf
#endif

#endif // _LINUX

static char * strDefault = "default";
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CSingleThread::CSingleThread(const char *strName) :
m_strThreadName(strDefault)
{
	m_cbLoop   = NULL;
	m_pVoid    = NULL;

	m_bThread  = false;
	m_bPause   = false;
	
#ifdef _LINUX
	m_threadid = 0;
#else
	m_hHandle  = 0;
#endif
	SetName(strName);
}

void CSingleThread::SetName(const char *strName)
{
	if (strName)
	{
		int size = strlen(strName);
		m_strThreadName = new char[size+1];
		strcpy(m_strThreadName, strName);
	}
}

CSingleThread::~CSingleThread()
{
	StopThread();

	if (m_strThreadName != strDefault)
		delete [] m_strThreadName;
}

/*
void CSingleThread::StopThread()
{
	try
	{
		if (m_bThread)
		{
			m_bThread = false;

#ifdef _LINUX
			_msg("[CSingleThread] [%30s] [ID:%8d] Thread Stop Wait\n", m_strThreadName, m_threadid);
			int status = pthread_join(m_threadid, NULL);
			m_threadid = 0;
#else
			_msg("[CSingleThread] [%30s] [0x%04x] Thread Stop Wait\n", m_strThreadName, m_hHandle);
			if (WaitForSingleObject(m_hHandle, 5000) != WAIT_OBJECT_0)
				_msg("[CSingleThread] [%30s]Error In StopThread()\n", m_strThreadName);
			m_hHandle = 0;
#endif
		}
	}
	catch( ... )
	{
		_msg("[CSingleThread] [%30s] Something wrong\n", m_strThreadName);
		getchar();
	}
}
/*/
void CSingleThread::StopThread()
{
	if (m_bThread)
	{
		m_bThread = false;

#ifdef _LINUX
		_msg("[CSingleThread] [%30s] [ID:%8d] Thread Stop Wait\n", m_strThreadName, m_threadid);
		int status = pthread_join(m_threadid, NULL);
		m_threadid = 0;
#else
		_msg("[CSingleThread] [%30s] [0x%04x] Thread Stop Wait\n", m_strThreadName, m_hHandle);
		if (WaitForSingleObject(m_hHandle, 5000) != WAIT_OBJECT_0)
			_msg("[CSingleThread] [%30s]Error In StopThread()\n", m_strThreadName);
		m_hHandle = 0;
#endif
	}
}
//*/

bool CSingleThread::BeginThread(
	bool( __cdecl *cbLoop)( void * ), 
	void *arglist,
	unsigned stack_size,
	int priority)
{
	m_cbLoop = cbLoop;
	m_pVoid  = arglist;
	m_bThread = true;

#ifdef _LINUX
	int status = pthread_create(	&m_threadid, 
									NULL, 
									CSingleThread::_cbThreadLoop, 
									this );
	if( status != 0 )
	{
		_msg("[CSingleThread] [%30s] Thread Create error\n", m_strThreadName);
		return false;
	}
#else
	m_hHandle = CreateThread(NULL, stack_size, CSingleThread::_cbThreadLoop, this, 0, NULL);
	if (!m_hHandle)
	{
		_msg("[CSingleThread] [%30s] Thread Create error\n", m_strThreadName);
		return false;
	}
#endif
	return true;
}

#ifdef _LINUX
	#define _TH_RETURN_TYPE_ void*
#else
	#define _TH_RETURN_TYPE_ unsigned long 
#endif

_TH_RETURN_TYPE_ CSingleThread::_cbThreadLoop(void *pParam) { return ((CSingleThread *)pParam)->_OnThreadLoop();}
_TH_RETURN_TYPE_ CSingleThread::_OnThreadLoop()
{

#ifdef _LINUX
//	_msg("[CSingleThread] [%30s] [ID:%8d] Thread Started\n", m_strThreadName, m_threadid);
	_msg("[CSingleThread] [%30s] [ID:%8d] Thread Started\n", m_strThreadName, getpid());
#else
	_msg("[CSingleThread] [%30s] [0x%04x] Thread Started\n", m_strThreadName, m_hHandle);
#endif

	while(m_bThread)
	{
		bool ret = true;
		if (!m_bPause)
		{
			if (m_cbLoop)
				ret = m_cbLoop(m_pVoid);
			else
				ret = vtlRun();
		}
		else
		{
			Sleep(1);
			m_wePause.SetEvent();
		}
		if (!ret) 
			m_bThread = false;
	}

#ifdef _LINUX
//	_msg("[CSingleThread] [%30s] [ID:%8d] Thread Stoped\n", m_strThreadName, m_threadid);
	_msg("[CSingleThread] [%30s] [ID:%8d] Thread Stoped\n", m_strThreadName, getpid());
	pthread_exit(NULL);
#else
	_msg("[CSingleThread] [%30s] [0x%04x] Thread Stoped\n", m_strThreadName, m_hHandle);
#endif

	return 0;
}
#undef _TH_RETURN_TYPE_

void CSingleThread::PauseThread (unsigned int dwTimeOut)
{
	if (m_bPause)
		return;
	m_bPause = true;
	if (m_bThread)
	{
//		_msg("[CSingleThread] [%30s] [0x%04x] Pause Wait\n", m_strThreadName, m_hHandle);
		m_wePause.WaitEvent(dwTimeOut);
//		_msg("[CSingleThread] [%30s] [0x%04x] Pause End\n" , m_strThreadName, m_hHandle);
	}
}

void CSingleThread::ResumeThread()
{
	m_bPause = false;
}

bool CSingleThread::IsThreadOn() { return m_bThread; }

