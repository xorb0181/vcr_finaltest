// memstream.h: interface for the memstream class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MEMSTREAM_H__D3F32CF9_A4FA_49FE_A5DF_527121B7EE57__INCLUDED_)
#define AFX_MEMSTREAM_H__D3F32CF9_A4FA_49FE_A5DF_527121B7EE57__INCLUDED_

#include "../OilBase.h"

#ifndef _LINUX
	#if _MSC_VER > 1000
	#pragma once
	#endif // _MSC_VER > 1000
#endif

#ifndef _WIN32_WCE

class OILLIB_API membuf : public std::streambuf
{
public:
	membuf();
	virtual ~membuf();
	
public:
	void DeleteBuffer();
	void Reset();
	
	char *GetBuffer();
	std::streamsize GetSize() const;
	
protected:
	virtual int overflow(int ch);
	
private:
	char *buf;
	int   bufSize;
};


class OILLIB_API memstream : public std::ostream 
{
public:
	memstream();
	int GetSize() const;
	const char *GetBuffer();
	void Reset();
	void DeleteBuffer();
	
protected:
	membuf _Sb;
};

#else

class OILLIB_API memstream : public std::ostream 
{
public:
	memstream()
	{
		m_pBuf = NULL;
		m_curSize = 0;
		m_maxSize = 0;
	}
	
	int GetSize() const
	{
		printf("getsize %d\n", m_curSize);
		return m_curSize;
	}
	const char *GetBuffer()
	{
		return (const char*)m_pBuf;
	}
	
	void Reset()
	{
		m_curSize = 0;
	}
	
	void DeleteBuffer()
	{
		if (m_pBuf)
			delete [] m_pBuf;
		m_pBuf = NULL;
		m_curSize = 0;
		m_maxSize = 0;
	}
	
public:
	virtual void write(const char *buf, int size)
	{
		printf("write %d\n", size);
		if (m_curSize+size < m_maxSize)
		{
		}
		else
		{
			m_maxSize += __max(size, 512);

			printf("resize %d -> %d\n", m_curSize, m_maxSize);

			BYTE *p = new BYTE[m_maxSize];
			memcpy(p, m_pBuf, m_curSize);
			delete [] m_pBuf;
			m_pBuf = p;
		}
		memcpy(m_pBuf + m_curSize, buf, size);
		m_curSize += size;
	}
	
protected:
	BYTE *m_pBuf;
	int   m_curSize;
	int   m_maxSize;
};
#endif


#endif // !defined(AFX_MEMSTREAM_H__D3F32CF9_A4FA_49FE_A5DF_527121B7EE57__INCLUDED_)

