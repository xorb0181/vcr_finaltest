// SplitRecv.cpp: implementation of the CSplitRecv class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "SplitRecv.h"

// #ifdef _DEBUG
// #undef THIS_FILE
// static char THIS_FILE[]=__FILE__;
// #define new DEBUG_NEW
// #endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CSplitRecv::CSplitRecv(int bufSize)
{
	recvTotalCnt = 0;
	recvCurrent  = 0;
	recvSize     = 0;;
	recvBufSize = bufSize;
	recvBuf = new char[recvBufSize];
}

CSplitRecv::~CSplitRecv()
{
	delete [] recvBuf;
}

bool CSplitRecv::PushData(const char *pBuf, int size)
{
	recvTotalCnt = pBuf[0];
	recvCurrent  = pBuf[1];
//	TRACE("%d/%d\n", recvCurrent, recvTotalCnt);

	if (recvCurrent == 1)
		recvSize   = 0;
	int dataSize = size-2;

	if (recvBufSize < recvSize + size)
	{
		char *pTemp = recvBuf;
		recvBufSize = (recvSize+size)*2;
		recvBuf = new char[recvBufSize];
		delete [] pTemp;
	}

	memcpy(recvBuf + recvSize, pBuf+2, dataSize);
	recvSize   += dataSize;

	return IsFull();
}
