// memstream.cpp: implementation of the memstream class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include <iostream>
#include "memstream.h"


// #ifdef _DEBUG
// #undef THIS_FILE
// static char THIS_FILE[]=__FILE__;
// #define new DEBUG_NEW
// #endif

using namespace std;

#ifndef _WIN32_WCE

membuf::membuf()
{
#ifndef _LINUX
	streambuf::_Init();
#endif
	bufSize = 0;
	buf = NULL;
}

membuf::~membuf()
{
	if (buf)
		delete [] buf;
}

void membuf::DeleteBuffer()
{
	if (buf)
		delete [] buf;
	buf = NULL;
}

char *membuf::GetBuffer()
{
	return (gptr()); 
}

streamsize membuf::GetSize() const
{
	return (pptr() == 0 ? 0 : pptr() - pbase()); 
} 

void membuf::Reset()
{
	if (bufSize)
	{
		setp(buf, buf + bufSize);
		setg(buf, buf, buf);
	}
}

int membuf::overflow(int ch)
{
	// try to extend write area
	int osize = gptr() == 0 ? 0 : epptr() - eback();
	bufSize = osize + 512;
	buf = new char[bufSize];
	
	if (osize == 0)
	{
		// setup new buffer
		setp(buf, buf + bufSize);
		setg(buf, buf, buf);
	}
	else
	{
		memcpy(buf, eback(), osize);
		delete[] eback();
		// revise old pointers
#ifndef _LINUX
		setp(pbase() - eback() + buf, pptr() - eback() + buf, buf + bufSize);
#endif
		setg(buf, gptr() - eback() + buf, pptr() + 1);
	}
#ifndef _LINUX
	return ((unsigned char)(*_Pninc() = (char)ch));
#else
	return traits_type::eof();
#endif
}

memstream::memstream() : ostream(&_Sb)
{
}

int memstream::GetSize() const
{
	return (_Sb.GetSize());
}

const char *memstream::GetBuffer() { return _Sb.GetBuffer(); }

void memstream::Reset() 
{
	_Sb.Reset();
}

void memstream::DeleteBuffer()
{
	_Sb.DeleteBuffer();
}

#endif