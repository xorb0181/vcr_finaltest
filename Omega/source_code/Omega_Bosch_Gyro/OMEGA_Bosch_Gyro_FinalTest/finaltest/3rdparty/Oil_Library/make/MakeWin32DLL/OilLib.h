
// The following ifdef block is the standard way of creating macros which make exporting 
// from a DLL simpler. All files within this DLL are compiled with the OILLIB_EXPORTS
// symbol defined on the command line. this symbol should not be defined on any project
// that uses this DLL. This way any other project whose source files include this file see 
// OILLIB_API functions as being imported from a DLL, wheras this DLL sees symbols
// defined with this macro as being exported.

// #ifdef OILLIB_EXPORTS
// #define OILLIB_API __declspec(dllexport)
// #else
// #define OILLIB_API __declspec(dllimport)
// #endif
// 
// // This class is exported from the OilLib.dll
// class OILLIB_API COilLib {
// public:
// 	COilLib(void);
// 	// TODO: add your methods here.
// };
// 
// extern OILLIB_API int nOilLib;
// 
// OILLIB_API int fnOilLib(void);
// 
