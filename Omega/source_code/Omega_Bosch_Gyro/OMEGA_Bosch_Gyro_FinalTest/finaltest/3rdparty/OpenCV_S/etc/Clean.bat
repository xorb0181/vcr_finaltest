del ..\*.exe /s

del ..\*.exp /s
del ..\*.ilk /s
del ..\*.bsc /s
del ..\*.pdb /s
del ..\*.idb /s

del ..\*.obj /s
del ..\*.pch /s
del ..\*.res /s
del ..\*.sbr /s
del ..\*.vcb /s

del ..\*.ncb /s
del ..\*.aps /s
del ..\*.o /s
del ..\*.a /s
del ..\*.tbl /s

rmdir ..\Temp /s /q
