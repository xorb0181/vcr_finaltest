// VipOpenCVTools.h: interface for the CVipOpenCVTools class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_VIPOPENCVTOOLS_H__2697E3DE_72D1_4338_805C_FE48B1188BC0__INCLUDED_)
#define AFX_VIPOPENCVTOOLS_H__2697E3DE_72D1_4338_805C_FE48B1188BC0__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifdef VIPOPENCVTOOLS_EXPORTS
#define VIPOPENCVTOOLS_API __declspec(dllexport)
#else
#define VIPOPENCVTOOLS_API __declspec(dllimport)
#endif

#include "vipl.h"

#include "opencv.h"

VIPOPENCVTOOLS_API void OptFlowPyrLK(
	const ByteImage &img1, const ByteImage &img2,                     // Input
	fPointArray &arr1, fPointArray &arr2, CVipArrBase <char> &status, // Output
	ByteImage *pByteBuf = NULL, int maskSize = 10, bool bUsePyrmid = false);              // Parameters

VIPOPENCVTOOLS_API fPointArray FindCorner(
	const ByteImage &image, 
	double quality = 0.01, double min_distance = 10, 
	bool bUseSubPixFit = false,
	int MaxCount = 1000,
	FloatImage *BufArray = NULL );

VIPOPENCVTOOLS_API void CornerSubPix(const ByteImage &img, fPoint      &pos);
VIPOPENCVTOOLS_API void CornerSubPix(const ByteImage &img, fPointArray &pos);

VIPOPENCVTOOLS_API void MatchTemplate(const FloatImage &src, const FloatImage &templ, FloatImage &dst, CvTemplMatchMethod method);
VIPOPENCVTOOLS_API FloatImage MatchTemplate(const FloatImage &src, const FloatImage &templ, CvTemplMatchMethod method);
VIPOPENCVTOOLS_API CvFont InitFont(double hscale = 0.3, double vscale = 0.3, double italic_scale = 0, int thickness = 1);

VIPOPENCVTOOLS_API void DrawCircle(ByteImage &canvas, const fPointArray &arr      , int r, CvScalar color, int thickness = 1);
VIPOPENCVTOOLS_API void DrawCross (ByteImage &canvas, const fPointArray &arrCenter, int r, CvScalar color, int thickness = 1);
VIPOPENCVTOOLS_API void DrawCross (ByteImage &canvas, const fPoint      &center   , int r, CvScalar color, int thickness = 1);

VIPOPENCVTOOLS_API CvMat vipCvMat(BYTE *src, int step, int width, int height, int type);

VIPOPENCVTOOLS_API CvPoint convert(const fPoint &src);

#endif // !defined(AFX_VIPOPENCVTOOLS_H__2697E3DE_72D1_4338_805C_FE48B1188BC0__INCLUDED_)
