#if !defined(_MATRIX_FUNCTIONS)
#define _MATRIX_FUNCTIONS

/** @file MatrixFunction.h */

#include "Matrix.h"
#include "NumericalRecipes.h"
#include <math.h>

/**	Create Clone Buffer
    @param		input		The source Matrix
    @return		Zero Padded Matrix with same size of input
*/
template <class T>
CMatrix <T> CloneBuffer(const CMatrix <T> &input)
{
	CHECK_NULL(input, "CloneBuffer");
	return CMatrix <T>(input.GetRowSize(), input.GetColSize());
}

/*------------------------------------------------------------------*/
/**@name		Global Function of Template Matrix Class			*/
/*------------------------------------------------------------------*/
//@{

/**	Create Matrix filled with one
    @param		input		The source Matrix
    @return		Result Matrix
*/
template <class T>
inline CMatrix <T> Ones(int nRowSize, int nColSize = 1)
{
	CMatrix <T> output(nRowSize, nColSize);
	output.Assign( (T) 1.0);
	return output;
}

/**	Create Matrix filled with zero
    @param		input		The source Matrix
    @return		Result Matrix
*/
template <class T>
inline CMatrix <T> Zeros (int nRowSize, int nColSize = 1)
{
	CMatrix <T> output(nRowSize, nColSize);
	output.Assign( (T) 0.0);
	return output;
}

/** Horizontal concatenation
    @param	count	Number of Matrix for concatenation
    @param	first   Matrix1, Matrix2, Matrix3, ...
    @return Concatenated Matrix
*/
template <class T>
CMatrix <T> HorzCat(const int count, const CMatrix <T> first, ...)
{
	va_list marker;	

	// Get output Size
	int outputColSize = first.GetColSize();
	int outputRowSize = first.GetRowSize();

	va_start( marker, first ); // Initialize variable arguments	
	int i;
	for (i = 1 ; i < count ; i++)
	{
		CMatrix <T> & CurMatrix = va_arg(marker, CMatrix<T>);
		VIP_ASSERT(outputRowSize == CurMatrix.GetRowSize(), "HorzCat", "not same row size");
		outputColSize += CurMatrix.GetColSize();
	}
	va_end( marker );  // Reset variable arguments

	// Allocate output
	CMatrix <T> output(outputRowSize, outputColSize);

	// Set elements of output
	int nStartColIndex = 0;
	output.SetColMatrix(first, nStartColIndex);
	nStartColIndex += first.GetColSize();

	va_start(marker, first); // Initialize variable arguments
	for (    i = 1 ; i < count ; i++)
	{
		CMatrix <T> & CurMatrix = va_arg(marker, CMatrix<T>);
		output.SetColMatrix(CurMatrix, nStartColIndex);
		nStartColIndex += CurMatrix.GetColSize();
	}
	va_end( marker ); // Reset variable arguments

	return output;
}

/** Vertical concatenation
    @param	count	Number of Matrix for concatenation
    @param	first   Matrix1, Matrix2, Matrix3, ...
    @return Concatenated Matrix
*/
template <class T>
CMatrix <T> VertCat(int count, const CMatrix <T> first, ...)
{
	va_list marker;	

	// Get output Size
	int outputColSize = first.GetColSize();
	int outputRowSize = first.GetRowSize();
	int i;

	va_start( marker, first ); // Initialize variable arguments	
	for (i = 1 ; i < count ; i++)
	{
		CMatrix <T> & CurMatrix = va_arg(marker, CMatrix<T>);
		VIP_ASSERT(outputColSize == CurMatrix.GetColSize(), "VertCat", "not same col size");
		outputRowSize += CurMatrix.GetRowSize();
	}
	va_end( marker );  // Reset variable arguments
	
	// Allocate output
	CMatrix <T> output(outputRowSize, outputColSize);

	// Set elements of output
	int nStartRowIndex = 0;
	output.SetRowMatrix(first, nStartRowIndex);
	nStartRowIndex += first.GetRowSize();

	va_start(marker, first); // Initialize variable arguments
	for (i = 1 ; i < count ; i++)
	{
		CMatrix <T> & CurMatrix = va_arg(marker, CMatrix<T>);
		output.SetRowMatrix(CurMatrix, nStartRowIndex);
		nStartRowIndex += CurMatrix.GetRowSize();
	}
	va_end( marker ); // Reset variable arguments

	return output;
}

#define eqRQ(a, b)  ( (a) / sqrt( (a)*(a) + (b)*(b) ))

/** The RQ decomposition of a 3x3 matrix A
	Carry out the RQ decomposition of a 3x3 matrix A using Givens rotations

	<reference>
	[1] Hartley and Zisserman, Multi View Geomerty, Cambridge Unv. , 2000, Appendix A3.1.1 RQ decomposition of a 3x3 matrix (pp.552)

    @param	src		Input Matrix
    @param	R		Result R
    @param	Q		Result Q
*/
template <class T> 
void RQDecomp3x3(const CMatrix <T> &src, CMatrix <T> &R, CMatrix <T> &Q)
{
	CMatrix <T> A = src;

	CMatrix <T> Qx(3, 3), Qy(3, 3), Qz(3, 3);

	// 1. Multiply by Qx so as to set A32 to zero
	T sinX, cosX;	
	cosX = -eqRQ(A.Read(2, 2), A.Read(2, 1));
	sinX =  eqRQ(A.Read(2, 1), A.Read(2, 2));
	
	Qx(0, 0) = 1;
	Qx(1, 1) =  cosX; Qx(1, 2) = -sinX;
	Qx(2, 1) =  sinX; Qx(2, 2) =  cosX;
	
	A = A * Qx;

	// 2. Multiply by Qy so as to set A31 to zero
	T sinY, cosY;
	sinY =  eqRQ(A.Read(2, 0), A.Read(2, 2));
	cosY =  eqRQ(A.Read(2, 2), A.Read(2, 0));

	Qy(0, 0) =  cosY; Qy(0, 2) = sinY;
	Qy(1, 1) =     1;	
	Qy(2, 0) = -sinY; Qy(2, 2) = cosY;
	
	A = A * Qy;

	// 3. Multiply by Qz so as to set A32 remains zero
	T sinZ, cosZ;
	sinZ =  eqRQ(A.Read(1, 0), A.Read(1, 1));
	cosZ = -eqRQ(A.Read(1, 1), A.Read(1, 0));	
	
	Qz(0, 0) = cosZ; Qz(0, 1) = -sinZ;
	Qz(1, 0) = sinZ; Qz(1, 1) =  cosZ; 	
	Qz(2, 2) =    1;

	A = A * Qz;

	// Consequently src = R Qz^T Qy^T Qx^T , 

	R = A;
	Q = Qz.Transpose() * Qy.Transpose() * Qx.Transpose();
}

#undef eqRQ

/** Cholesky decomposition
    @param	src		Input Matrix
    @return Result Matrix
*/
template <class T>
CMatrix <T> CholeskyDC(const CMatrix <T> &src)
{
	VIP_ASSERT(src.GetRowSize() == src.GetColSize(), "CholeskyDC", "Matrix is not Square");

	CMatrix <T> ret = src;
	int row = ret.GetRowSize();
	T *p = new T[row + 1];
	NumericalRecipes::choldc(ret.GetNRCMatrix(), row, p);

	for (int r = 0 ; r < row ; r++)
	{
		ret(r, r) = p[r + 1];
		for (int c = r + 1 ; c < row ; c++)
			ret(r, c) = 0;
	}
	delete [] p;

	return ret;
}

/** Check is NAN(Not A Number)
    @param	src		Input Matrix
    @return Result Matrix
*/
template <class T>
bool IsNaN(const CMatrix <T> & input)
{
	CHECK_NULL(input, "IsNaN");
	const T  *pBuf     = (T *)input.GetBufferConst();
	int      nBufSize  =      input.GetBufferSize() / sizeof(T);

	for (int i = 0 ; i < nBufSize ; i++)
		if(_isnan(pBuf[i])) return true;

	return false;
};

/// Dot Product of Matrix or Vector
template <class T>
T Dot(const CMatrix <T> &matA, const CMatrix <T> &matB)
{
	T output = 0;
	MATRIX_LOOP(
		&matA, &matB, 0, 
		output += _MAT1_ * _MAT2_;
		);
	return output;
};

/// Cross Product of Vector
template <class T>
CMatrix <T> Cross(const CMatrix <T> &op1, const CMatrix <T> &op2)
{
	CHECK_DIM_MATRIX(op1, op2);
	VIP_ASSERT(op1.GetColSize() == 1 && op1.GetRowSize() == 3, "Cross", "Wrong Input Dimension");
	VIP_ASSERT(op2.GetColSize() == 1 && op2.GetRowSize() == 3, "Cross", "Wrong Input Dimension");

	CMatrix <T> output(3);

	output(0) =                            - op1.Read(2) * op2.Read(1) + op1.Read(1) * op2.Read(2);
	output(1) =  op1.Read(2) * op2.Read(0)                             - op1.Read(0) * op2.Read(2);
	output(2) = -op1.Read(1) * op2.Read(0) + op1.Read(0) * op2.Read(1)                  ;

	return output;
};

/// Norm Product of Vector
template <class T>
double Norm(const CMatrix <T> &src)
{
	double sum = 0;
	MATRIX_LOOP(&src, 0, 0, sum += _MAT1_ * _MAT1_       );
	return sqrt( sum );
}

template <class T>
CMatrix <T> Pow(const CMatrix <T> &src, T value)
{
	CMatrix <T> output(src.GetRowSize(), src.GetColSize());

	if (value == 1)
	{
		return src;
	}
	else if (value == 2)
	{
		MATRIX_LOOP(&src,    0, &output, _OUTMAT_ = _MAT1_ * _MAT1_       );
	}
	else
	{
		MATRIX_LOOP(&src,    0, &output, _OUTMAT_ = pow(_MAT1_, value) );
	}
	return output;
}

/// Make Cross Product Matrix
template <class T>
CMatrix <T> MakeCrossMatrix(const CMatrix <T> &src)
{
	VIP_ASSERT(src.GetColSize() == 1 && src.GetRowSize() == 3, "Cross", "Wrong Input Dimension");

	T a = src.Read(0);
	T b = src.Read(1);
	T c = src.Read(2);

	CMatrix <T> output(3, 3);
	output(0, 0) =  0; output(0, 1) = -c; output(0, 2) =  b;
	output(1, 0) =  c; output(1, 1) =  0; output(1, 2) = -a;
	output(2, 0) = -b; output(2, 1) =  a; output(2, 2) =  0;

	return output;
};

/// Calculate Rank of Matrix
template <class T>
int Rank(const CMatrix <T> &src)
{
	CMatrix <T> U, W, V;
	SVD(src, U, W, V);
	int rank = 0;
	for (int i = 0 ; i < W.GetRowSize() ; i++)
	{
		if(W.Read(i, i))
			rank++;
	}
	return rank;
}


/*------------------------------------------------------------------*/
// Conversion Code of Template Matrix Class
/*------------------------------------------------------------------*/

/// Convert Matrix Type
template <class TSrc, class TDst>
void Convert(CMatrix <TDst> &dst, const CMatrix <TSrc> &src)
{
	dst.Allocate(src.GetRowSize(), src.GetColSize());
	const TSrc *pSrc = src.GetBufferConst();
	      TDst *pDst = dst.GetBuffer     ();

	int size = src.GetSize();

	for (int i = 0 ; i < size ; i++)
		pDst[i] = (TDst) pSrc[i];
};

/// Trace of Matrix
template <class T>
T Trace(CMatrix <T> &src)
{
	VIP_ASSERT(src.GetRowSize() == src.GetColSize(), "Trace", "Matrix is not Square");
	T temp = 0;
	for (int r = 0 ; r < src.GetRowSize() ; r++)
		temp += src.Read(r, r);
	return temp;
}

/// Exponential of Matrix
template <class T>
CMatrix <T> Exp(CMatrix <T> &src)
{
	CMatrix <T> output(src.GetRowSize(), src.GetColSize());
	MATRIX_LOOP(
		&src, 0, &output,
		_OUTMAT_ = exp(_MAT1_);
		);
	return output;
}

/// Square Root
template <class T>
CMatrix <T> Sqrt(CMatrix <T> &src)
{
	CMatrix <T> output(src.GetRowSize(), src.GetColSize());
	MATRIX_LOOP(
		&src, 0, &output,
		_OUTMAT_ = (T)sqrt(_MAT1_);
		);
	return output;
}

/// Absolute Value
template <class T>
CMatrix <T> Abs(const CMatrix <T> &src)
{
	CMatrix <T> output(src.GetRowSize(), src.GetColSize());
	MATRIX_LOOP(
		&src, 0, &output,
		_OUTMAT_ = (_MAT1_ > 0) ? _MAT1_ : _MAT1_ * (-1);
		);
	return output;
}

/// Sumation
template <class T>
double Sum(const CMatrix <T> &src)
{
	double temp = 0;
	MATRIX_LOOP(
		&src, 0, 0, 
		temp += _MAT1_;
		);
	return temp;
};

/// Mean
template <class T>
double Mean(const CMatrix <T> &src)
{
	return Sum(src) / (double)src.GetSize();
};

/// Variance
template <class T>
double Variance(const CMatrix <T> &src, T mean)
{
	double temp = 0;
	MATRIX_LOOP(
		&src, 0, 0,
		T buf = (T)(_MAT1_ - mean);
		temp += buf*buf;
		);

	return temp /(double)src.GetSize();
}

/// Make mean zero and unit variance
template <class T>
CMatrix <T> MeanZeroUnitVariance(const CMatrix <T> &src)
{
	T mean = (T)Mean(src);
	T var  = (T)Variance(src, mean);
	VIP_ASSERT(var != 0, "MeanZeroUnitVariance", "Variance is Zero");
	return (src - mean) / (T)sqrt(var);
}


///
template <class T>
T GetMax(const CMatrix <T> &src)
{
	T Max = FLT_MIN;
	MATRIX_LOOP(
		&src, 0, 0, 
		if(_MAT1_ > Max)
			Max = _MAT1_;
		);
	return Max;
};

///
template <class T>
T GetMin(const CMatrix <T> &src)
{
	T Min = (T)FLT_MAX;
	MATRIX_LOOP(
		&src, 0, 0, 
		if(_MAT1_ < Min)
			Min = _MAT1_;
		);
	return Min;
};

///
template <class T>
void GetMin(const CMatrix <T> & src, int *pY, int *pX, T *pV, int Count = 1)
{
	CHECK_NULL(src, "GetMax");

	char *pShiftBuf = new char[sizeof(float) * Count]; // 만약 double image 가 지원 되면 buffer size 변경.

	for (int k = 0 ; k < Count ; k++) ((T *)pV)[k] = FLT_MAX;

	const T *pSrc = (const T *)src.GetBufferConst();
	int Size  = src.GetSize();
	int WStep = src.GetColSize();

	for (int i = 0 ; i < Size ; i++)
	{
		for (int j = 0 ; j < Count ; j++)
		{
			if(pSrc[i] < pV[j])
			{
				int LeftSize = Count-j-1;
				if(LeftSize)
				{
					memcpy(pShiftBuf , pV + j   , LeftSize * sizeof(T)  );
					memcpy(pV + j + 1, pShiftBuf, LeftSize * sizeof(T)  );
					memcpy(pShiftBuf , pX + j   , LeftSize * sizeof(int));
					memcpy(pX + j + 1, pShiftBuf, LeftSize * sizeof(int));
				}

				pV[j] = pSrc[i];
				pX[j] = i;
				break;
			}
		}
	}

	for (int j = 0 ; j < Count ; j++)
	{
		pY[j] = pX[j] / WStep;
		pX[j] %= WStep;
	}

	delete [] pShiftBuf;
}

///
template <class T>
CMatrix <T> MeanCol(const CMatrix <T> &src)
{
	int Row = src.GetRowSize();
	int Col = src.GetColSize();

	CMatrix <T> output(Row);

	for (int r = 0 ; r < Row ; r++)
	{
		T buffer = 0;
		for (int c = 0 ; c < Col ; c++)
			buffer += src.Read(r, c);
		output(r) = buffer / (T)Col;
	}
	return output;
}

///
template <class T>
CMatrix <T> MeanRow(CMatrix <T> &src)
{
	int Row = src.GetRowSize();
	int Col = src.GetColSize();

	CMatrix <T> output(1, Col);

	for (int c = 0 ; c < Col ; c++)
	{
		T buffer = 0;
		for (int r = 0 ; r < Row ; r++)
			buffer += src(r, c);
		output(0, c) = buffer / Row;
	}
	return output;
}

///
template <class T>
void SortEigVector(CMatrix <T> &EigVector, CMatrix <T> &EigValue)
{
	int FaceCount = EigVector.GetColSize();

	T *pVal = EigValue .GetBuffer();

	for ( ;; )
	{
		int flag = 0;
		for (int i = 0 ; i < FaceCount -1 ; i++)
		{
			T &v0 = pVal[i    ];
			T &v1 = pVal[i + 1];
			if (v0 < v1)
			{
				T buffer = v0;
				v0 = v1;
				v1 = buffer;

				EigVector.ExchangeCol(i, i+1);
				flag = 1;
			}
		}
		if (flag == 0)
			break;
	}
}

///
template <class T>
bool Eig(const CMatrix <T> &input, CMatrix <T> &EigVector, CMatrix <T> &EigValue, int eps = 0)
{
	int row = input.GetRowSize();
	int col = input.GetColSize();

	VIP_ASSERT(row == col, "Eig", "Input Matrix has to be Square");

	EigVector = input;
	EigValue.Allocate(row, 1);

	T *off = new T[row + 1];
	NumericalRecipes::tred2(EigVector.GetNRCMatrix(), row, EigValue.GetNRCVector(), off);
	NumericalRecipes::tqli (EigValue .GetNRCVector(), off, row, EigVector.GetNRCMatrix());
	delete [] off;

	SortEigVector(EigVector, EigValue);

	return true;
};

/// A*EigValue = EigVector*B*Eigvalue
template <class T>
bool GeneralizedEig(CMatrix <T> &inputA, CMatrix <T> &inputB, CMatrix <T> &EigVector, CMatrix <T> &EigValue, int eps = 0)
{
	int row = inputA.GetRowSize();
	int col = inputA.GetColSize();
	
	VIP_ASSERT(row == col, "GeneralizedEig", "Input Matrix has to be Square");
	VIP_ASSERT(inputB.GetRowSize() == row && inputB.GetColSize() == col,
						   "GeneralizedEig", "Input Matrix has to be Square");

	CMatrix <T> LInverse = inputB;
	T *p = new T[row + 1];
	NumericalRecipes::choldc(LInverse.GetNRCMatrix(), row, p);

	T ** a = LInverse.GetNRCMatrix();
	for(int i=1 ; i<=row ; i++)
	{
		a[i][i] = float(1.0) / p[i];
		for(int j=i+1 ; j<=row ; j++)
		{
			T sum = 0;
			for(int k=i ; k<j ; k++) sum -= a[j][k] * a[k][i];
			a[j][i] = sum / p[j];
		}
	}

	delete [] p;

	LInverse = LInverse.Transpose();

	for(int r=0 ; r<row ; r++)
		for(int c=0 ; c<col ; c++)
			if(r > c)	LInverse(r, c) = 0;
	
	CMatrix <T> LInverse_Con;	
	LInverse_Con = LInverse * inputA * LInverse.Transpose();
	Eig(LInverse_Con , EigVector , EigValue);

	return true;
};

/// A*X = B Finding X;
template <class T>
void GSVD(const CMatrix <T> &A, CMatrix <T> &B, CMatrix <T> &X)
{
	int row = A.GetRowSize();
	int col = A.GetColSize();

	// allocate

	CMatrix <T> u = A;			// u[m * n]
	CMatrix <T> b = B;
	X.Allocate(col);

	CMatrix <T> w(col);		// w[n]
	CMatrix <T> v(col, col);	// v[n * n];

	/////////////// Main ///////////////////////////
	NumericalRecipes::svdcmp(u.GetNRCMatrix(), row, col, w.GetNRCVector(), v.GetNRCMatrix());

	double wmax = 0.0; int j;
	for (j = 0 ; j < col ; j++)
	{
		if(w(j) > wmax)
			wmax = w(j);
	}

	double wmin = wmax * 1.0e-6;
	for (j = 0 ; j < col ; j++)
	{
		if(w(j) < wmin)
			w(j) = 0.0;
	}

	NumericalRecipes::svbksb(u.GetNRCMatrix(), w.GetNRCVector(), v.GetNRCMatrix(), row, col, b.GetNRCVector(), X.GetNRCVector());
}

/// A*X = B Finding X;
template <class T>
void SVD(const CMatrix <T> &src, CMatrix <T> &U, CMatrix <T> &W, CMatrix <T> &V)
{
	int row = src.GetRowSize();
	int col = src.GetColSize();

	// allocate

	U = src;
	W.Allocate(col, col);
	V.Allocate(col, col);

	CMatrix <T> tempW(col);

	/////////////// Main ///////////////////////////
	NumericalRecipes::svdcmp(U.GetNRCMatrix(), row, col, tempW.GetNRCVector(), V.GetNRCMatrix());

	int j;
	double wmax = 0.0;
	for (j = 0 ; j < col ; j++) if(tempW(j) > wmax) wmax = tempW(j);
	double wmin = wmax * 1.0e-6;
	for (j = 0 ; j < col ; j++) if(tempW(j) < wmin) tempW(j) = 0.0;

	for (int c = 0 ; c < col ; c++)
		W(c, c) = tempW(c);
}

///
template <class T>
void SortColVector(CMatrix <T> &input)
{
	int RowSize = input.GetRowSize();
	input._CopyBeforeWrite();

	double *Array = input.GetBuffer();

	for ( ;; )
	{
		int flag = 1;
		for (int i = 0 ; i < RowSize -1 ; i++)
		{
			if (Array[i] < Array[i+1])
			{
				T buffer	= Array[i];
				Array[i]	= Array[i+1];
				Array[i+1]	= buffer;
				flag = 0;
			}
		}
		if (flag)
			break;
	}
}
//@}

#define det2(m)   (m[0][0]*m[1][1] - m[0][1]*m[1][0])
#define det3(m)   (m[0][0]*(m[1][1]*m[2][2] - m[1][2]*m[2][1]) -  \
                   m[0][1]*(m[1][0]*m[2][2] - m[1][2]*m[2][0]) +  \
                   m[0][2]*(m[1][0]*m[2][1] - m[1][1]*m[2][0]) )

template <class T>
double Det(const CMatrix <T> &mat)
{
    double result = 0;

	const T **ppBuffer = mat.Get2DBufferConst();

	int row = mat.GetRowSize();
	int col = mat.GetColSize();

	switch(row)
	{
	case 0: VIP_ASSERT(0, "", ""); break;
	case 1: result = ppBuffer[0][0]; break;
	case 2: result = det2(ppBuffer); break;
	case 3: result = det3(ppBuffer); break;
	default:
		{
			CMatrix <T> buf = mat;
			vipLUDecomp(buf.GetBuffer(), buf.GetWidthStep(), col, row, (T*)0, 0, col, row, &result);
		}
	}

    return result;
}
#undef det2
#undef det3




#endif // _MATRIX_FUNCTIONS
