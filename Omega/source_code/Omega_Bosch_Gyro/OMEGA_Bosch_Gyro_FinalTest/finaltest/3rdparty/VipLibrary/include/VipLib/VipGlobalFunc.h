#if !defined(_VIP_GLOBAL_FUNC_)
#define _VIP_GLOBAL_FUNC_

#include "ViplBase.h"

VIPLIB_API void DeleteStringMatrix(char ***strMatrix, int RowSize, int ColSize);
VIPLIB_API char *** StringToMatrix(char *string, int &RowSize, int &ColSize, const char* delRow, const char *delCol);
VIPLIB_API int  DivideExt(char *input, char* &file, char* &ext);

#endif // end _VIP_GLOBAL_FUNC_
