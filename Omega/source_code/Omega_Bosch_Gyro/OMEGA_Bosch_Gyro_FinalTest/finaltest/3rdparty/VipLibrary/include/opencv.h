//////////////////////////////////////////////////////////////////////
//
// VipLib.h: Include All Header & Library of Vip Library
//
//////////////////////////////////////////////////////////////////////

/** @file VipLib.h */

#if !defined(_opencv_Include)
#define _opencv_Include

#include "cv.h"

#ifndef _WIN32_WCE
	#include "cvaux.h"
	#include "highgui.h"

	#pragma comment(lib, "cv")
	#pragma comment(lib, "cvaux")
	#pragma comment(lib, "cxcore")
	#pragma comment(lib, "highgui")
#else
	#ifdef _DEBUG
		#pragma comment(lib, "CV_ARMV4D.lib")
	#else
		#pragma comment(lib, "CV_ARMV4.lib")
	#endif
#endif

#endif
