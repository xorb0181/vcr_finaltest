// IplBase.h: interface for the CVipArrBase class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(_VIP_IMAGE_BASE_)
#define _VIP_IMAGE_BASE_

/*------------------------------------------------------------------*/
/*			IplBase.h: interface for the CVipImgBase class.			*/
/*------------------------------------------------------------------*/

#include "VipGlobal.h"
#include "VipDebug.h"

#include "VipImgIplWrap.h"
#include "VipImgGlobal.h"

#include <iostream>

#define MAX_IPLIMAGE_MESSAGE 1000

#ifdef LoadImage
#undef LoadImage
#endif

#include "ViplBase.h"

/*------------------------------------------------------------------*/
/**                 Base Class of Image Data Type                   */
/*------------------------------------------------------------------*/
class VIPLIB_API CVipImgBase  
{
public:
	/// Storage
	struct tIplBaseData
	{
		/// Pointer of IPL Image
		IplImage  * pIplImage;
		/// Void Pointer for 2D Array
		void     ** ppVoid;
		/// Reference Count
		long	    nRef;
	};

protected:
	/// Storage Pointer
	tIplBaseData		* m_pData;   // pointer to ref counted string data
	/// 2D Data Pointer
	void     ** m_ppVoid;
	int         m_nCSize;
	/// Bit Per Pixel
	const eIplBase_Type	  m_nDepth;

	/// For Conversion to IplConvKernel
	union Convkernel
	{
		IplConvKernel     IntKernel;
		IplConvKernelFP   FPKernel;
	} m_ConvKernel;

public:
	/*--------------------------------------------------------------*/
	/**@name			Construction & Destruction					*/
	/*--------------------------------------------------------------*/
	//@{
	///
	explicit CVipImgBase(const eIplBase_Type depth);
	///
	CVipImgBase(int XSize, int YSize, int channel , eIplBase_Type type);
	virtual ~CVipImgBase();
	//@}
	
	/*--------------------------------------------------------------*/
	/**@name			Memory Alloc / Dealloc						*/
	/*--------------------------------------------------------------*/
	//@{
public:
	/** Allocate Memory for Image

		@param		nXSize	X Size (width)
		@param		nYSize	Y Size (height)
		@param		nChannelSize
							Channel Size
		@return		void
		@version	0.1
	*/
	void Allocate(int nXSize, int nYSize, int nChannelSize = 1, bool bFillZero = true, const char *colorModel = NULL, const char *channelSeq = NULL);
	void Allocate(const char *szInput);
	//@}

	/*--------------------------------------------------------------*/
	/**@name			Memory Alloc / Dealloc						*/
	/*--------------------------------------------------------------*/
	//@{
	/// Attach IplImage Pointer
	void		Attach(IplImage *pIplImage);
	/// Detach IplImage Pointer
	IplImage *	Detach();
	/// Attach IplROI Pointer
	void		AttachROI(IplROI *roi);
	/// Detach IplROI Pointer
	IplROI   *	DetachROI();
	//@}

protected:
	void _AllocateBody(IplImage *pIplImage, void **ppVoid);
	void _AllocateBuffer(int XSize, int YSize, int channel, bool bFillZero = true, const char *colorModel = NULL, const char *channelSeq = NULL);
	static void ** _AllocatePointer(IplImage *image);

	void _InitData();
	void _Initialize();
	void _CopyBeforeWrite();
	void _Copy(const CVipImgBase &src);

	void _FreeData(tIplBaseData* pData);
	void _Release();

public:
	/*--------------------------------------------------------------*/
	/**@name			Member Variable Acess Function				*/
	/*--------------------------------------------------------------*/
	//@{

	// Get Dimension Information
	/// Get Channel Size
	int GetCSize      () const { return m_pData->pIplImage->nChannels;    };
	/// Get X Size of Image
	int GetXSize      () const { return m_pData->pIplImage->width;        };
	/// Get Y Size of Image
	int GetYSize      () const { return m_pData->pIplImage->height;       };
	/// Get Bit Depth
	int GetDepth      () const { return m_pData->pIplImage->depth;        };
	/// Get 1D Buffer Size
	int	GetBufferSize () const { return m_pData->pIplImage->imageSize;    };
	/// Get Pixel Size
	int	GetSizeofPixel() const { return (GetDepth() & IPL_DEPTH_MASK)>>3; };

	/// Get ROI Pixel Count
	int	GetPixelCount(bool bUseROI = true) const 
	{ return (bUseROI ? roiWidth() * roiHeight() : GetXSize() * GetYSize() ) * GetCSize() ; };

	// IplImage Data Access
	/// Get 1D Buffer
	      char     *GetBuffer       ()       { _CopyBeforeWrite(); return m_pData->pIplImage->imageData; };
	/// Get 1D Buffer Const
	const char     *GetBufferConst  () const {                    return m_pData->pIplImage->imageData; };

	      char     *GetBuffer       (int x, int y)       { _CopyBeforeWrite(); return m_pData->pIplImage->imageData + m_pData->pIplImage->widthStep * y + x*GetSizeofPixel()*GetCSize(); };
	/// Get 1D Buffer Const
	const char     *GetBufferConst  (int x, int y) const {                    return m_pData->pIplImage->imageData + m_pData->pIplImage->widthStep * y + x*GetSizeofPixel()*GetCSize(); };

	/// Get Width Step of Buffer
	int			    GetWidthStep	() const { return m_pData->pIplImage->widthStep; };

	/// Get IplImage Pointer
	      IplImage *GetIplImage     ()       { _CopyBeforeWrite(); return m_pData->pIplImage;            };
	/// Get IplImage Pointer Const
	const IplImage *GetIplImageConst() const {                    return m_pData->pIplImage;            };

	/// Set Color Model
	void		SetColorModel(const char *colorModel, const char *channelSeq);

	/// Get Start X Offset Position
	int			roiXOffset			() const;
	/// Get Start Y Offset Position
	int			roiYOffset			() const;
	/// Get Width  (If ROI is NULL, return XSize)
	int			roiWidth			() const;
	/// Get Height (If ROI is NULL, return YSize)
	int			roiHeight			() const;
	/// Get Start Position(Pointer) of ROI
	      char* roiBuffer           ();
	const char* roiBufferConst      () const;

	/// Delete ROI
	void		RemoveROI			();
	/// Set ROI
	void		SetROI				(int xPos, int yPos, int width, int height, int channel = 0);
	/// Set ROI
	void		SetROI				(const IplROI &roi);
	/// Set Save ROI
	void		SetSafeROI			(int xPos, int yPos, int width, int height, int channel = 0);
	/// Get ROI
	IplROI		GetROI				() const;
	/// Check ROI is Allocated
	bool        HasROI              () const { return (m_pData->pIplImage->roi) ? true : false; };

	/// Initialize all data with one value
	void		Assign				(int   value);
	/// Initialize all data with one value
	void		Assign				(float value);

	/// Image Deep Copy (Not Reference copy)
	void        AssignCopy          (const CVipImgBase &src);

	/** Setting Border Type for IPL Convolution Process
		@param		mode		mode of dealing border. 
									- IPL_BORDER_CONSTANT
									- IPL_BORDER_REPLICATE
									- IPL_BORDER_REFLECT
									- IPL_BORDER_WRAP
		@param		border		border for use
									- IPL_SIDE_TOP
									- IPL_SIDE_BOTTOM
									- IPL_SIDE_LEFT
									- IPL_SIDE_RIGHT
									- IPL_SIDE_ALL
		@param		constVal	for use of IPL_BORDER_CONSTANT
	*/
	void		SetBorderMode		(int mode, int border = IPL_SIDE_ALL, int constVal = 0);
	///
	bool		IsEmpty				() const { return (GetBufferSize()) ? false : true; };
	///
	bool		IsInRange			(int x, int y, int c) const;
	///
	bool		IsInRange			(int x, int y) const;
	//@}

public:
	/*--------------------------------------------------------------*/
	/**@name			Save & Load									*/
	/*--------------------------------------------------------------*/
	//@{
	/// Save Image
	bool SaveImage(const char *format, ...) const;
	/// Load Image
	bool LoadImage(const char *format, ...);
	/// Save To Raw File
	bool SaveRawFile(const char *format, ...) const;
	/// Load From Raw File
	bool LoadRawFile(int nXSize, int nYSize, int nChannel, const char *format, ...);
	/// Save To Text
	bool SaveToText(const char *format, ...) const;

	/// Load From Binary
	bool LoadBinary(const char *format, ...);
	/// Save To Binary
	bool SaveBinary(const char *format, ...) const;
	//@}

public:
	bool LoadFromText(const char *format, ...);
	/*--------------------------------------------------------------*/
	/**@name			Operator (Copy, Type Conversion,, ETC.... )	*/
	/*--------------------------------------------------------------*/
	//@{
	CVipImgBase& operator=(const CVipImgBase& Src);
	///
	operator       IplImage        *();
	///
	operator const IplImage        *() const;

	///
	operator IplConvKernel   *() const;
	///
	operator IplConvKernelFP *() const;

	//@}
};

VIPLIB_API std::istream & operator >> (std::istream &ios,       CVipImgBase &src);
VIPLIB_API std::ostream & operator << (std::ostream &ios, const CVipImgBase &src);

#endif // !defined(_VIP_IMAGE_BASE_)
