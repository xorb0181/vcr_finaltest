//////////////////////////////////////////////////////////////////////
//
// VipWndTools.h: Include All Header & Library of Vip Library
//
//////////////////////////////////////////////////////////////////////

/** @file VipWndTools.h */

#if !defined(_VipWndTools_Include)
#define _VipWndTools_Include

#include "./VipWndTools/VipWndToolsFunc.h"

#ifndef _WIN32_WCE
	#include "./VipWndTools/ImShowFunc.h"

	#ifdef _DEBUG
		#pragma comment(lib, "VipWndToolsDLLD.lib")
	#else
		#pragma comment(lib, "VipWndToolsDLL.lib")
	#endif
#else

#ifdef _viplib_emb_
	#ifdef _DEBUG
		#pragma comment(lib, "VipWndToolsCED.lib")
	#else
		#pragma comment(lib, "VipWndToolsCE.lib")
	#endif
#else
	#ifdef _DEBUG
		#pragma comment(lib, "VipWndToolsCEDLLD.lib")
	#else
		#pragma comment(lib, "VipWndToolsCEDLL.lib")
	#endif
#endif

#endif

#endif
