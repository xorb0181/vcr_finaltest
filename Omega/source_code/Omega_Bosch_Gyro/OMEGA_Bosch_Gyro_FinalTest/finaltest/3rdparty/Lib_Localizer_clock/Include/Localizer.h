// Localizer.h: interface for the Localizer class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_LOCALIZER_H__3CCF34E3_3AA1_4128_BC58_3F05736D81B0__INCLUDED_)
#define AFX_LOCALIZER_H__3CCF34E3_3AA1_4128_BC58_3F05736D81B0__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "VLocalizerInterface.h"


VLocalizerInterface *GetLocalizer();

#endif // !defined(AFX_LOCALIZER_H__3CCF34E3_3AA1_4128_BC58_3F05736D81B0__INCLUDED_)
