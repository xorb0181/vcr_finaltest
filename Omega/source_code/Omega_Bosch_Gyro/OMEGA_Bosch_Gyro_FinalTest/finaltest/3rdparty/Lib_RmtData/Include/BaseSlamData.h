// BaseSlamData.h: interface for the CBaseSlamData class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_BASESLAMDATA_H__A0A1A606_D198_460F_AB1C_E36EE80D74D3__INCLUDED_)
#define AFX_BASESLAMDATA_H__A0A1A606_D198_460F_AB1C_E36EE80D74D3__INCLUDED_

#include "vipl.h"

#include "Oil_Thread/Sync.h"

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

typedef CVipArrBase <f3DPoint[2]> f6DPointArray;

struct _Base3DSlamView
{
	// Landmark ---------------------------------------------------------------------------
	f3DPointArray             m_arrLMPos    ; // estimated landmark
	FloatMatrixArray          m_arrLMCov    ;
	CVipArrBase <int>         m_arrLMEnable ;
	//-------------------------------------------------------------------------------------
	
	// Particle ---------------------------------------------------------------------------
	CVipArrBase <ushort>      m_arrParticleWeight;
	f6DPointArray             m_arrParticlePose  ;
	f3DPointArray             m_arrParticleVect  ; // velocity of particle
	CVipArrBase <short>       m_arrIdx           ;
	int                       m_nMaxParticlePos  ;
	//-------------------------------------------------------------------------------------
	
	// Trajectory
	CVipArray <f3DPointArray> m_visRobotTrj    ; // trajectory of 1st particle
	f3DPointArray             m_encRobotTrj    ; // trajectory of estimated pose
	//-------------------------------------------------------------------------------------
	
	
	//-------------------------------------------------------------------------------------
	f3DPointArray             m_dbgParticle;
	//-------------------------------------------------------------------------------------
	
	
	// Camera Pos -------------------------------------------------------------------------
	f3DPoint m_visCam6D[2]     ; // estimated camera pose
	f3DPoint m_visCam6DReal[2] ; // real camera pose for simulation
	
	f3DPoint m_visRobot        ;
	f3DPoint m_encRobot        ;
	f3DPoint m_rtlRobot        ;
	FloatMatrix m_visCamCov    ; // for EKF based SLAM
	//-------------------------------------------------------------------------------------
	
	// debug info -------------------------------------------------------------------------
	fPointArray arrBox[2];
	//-------------------------------------------------------------------------------------
	
	fPoint camPos;
	
	f3DPoint degCalcRobotPose(const f3DPoint &camPose)
	{
		f3DPoint robotPose;
		robotPose.z = camPose.z-90;
		float radAngle = float(robotPose.z/180.0f * IPL_PI);
		
		float c = (float)cos(radAngle);
		float s = (float)sin(radAngle);
		robotPose.x = float(camPose.x - (camPos.x*c - camPos.y*s));
		robotPose.y = float(camPose.y - (camPos.x*s + camPos.y*c));
		return robotPose;
	}
	
	CSync m_cs;
};

void ReadStream(_Base3DSlamView *data, std::istream &ios);

#endif // !defined(AFX_BASESLAMDATA_H__A0A1A606_D198_460F_AB1C_E36EE80D74D3__INCLUDED_)

