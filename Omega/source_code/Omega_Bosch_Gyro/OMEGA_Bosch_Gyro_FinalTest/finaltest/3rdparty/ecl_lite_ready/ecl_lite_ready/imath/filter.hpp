#ifndef FILTER_HPP_
#define FILTER_HPP_

#include <vector>
#include <algorithm>
#include <math.h>

template<typename T>
class xyLookup
{
public:
  xyLookup() : idx_last(-1) {}
  void clear()
  {
    table_x.clear();
    table_y.clear();
  }

  //@brief
  // you add the reference point through this function
  // note that this function does not sort the data, please put the data
  // with smallest data first
  void addPoints( const T & xValue, const T & yValue )
  {
    table_x.push_back( xValue );
    table_y.push_back( yValue );
    idx_last = table_x.size()-1;
  }

  void addTable( const T * xValues, const T * yValues, int tableSize, bool backwardPush = false )
  {
    clear();

    for( int i(0); i<tableSize; i++ )
    {
      if( backwardPush )  addPoints( xValues[tableSize-i-1], yValues[tableSize-i-1] );
      else        addPoints( xValues[i], yValues[i] );
    }
  }

  T operator() ( const T & iHave )
  {
    if( table_x.empty() ) return static_cast<T>( 0 );

    if( iHave <= table_x[0] )        return table_y[0];
    else if( iHave >= table_x[idx_last] )  return table_y[idx_last];

    int search_len( static_cast<int>(table_x.size()) );
    for( int i(1); i<search_len; i++ )
    {
      if( table_x[i] > iHave )
      {
        T sx( table_x[i-1] );
        T ex( table_x[i] );
        T sy( table_y[i-1] );
        T ey( table_y[i] );
        T dx = ex - sx;
        T dy = ey - sy;
        return ( sy + dy * ((iHave-sx)/dx) );
      }
    }
    return table_y[0];
  }

  std::vector<T> table_x;
  std::vector<T> table_y;
  int idx_last;
};

template<typename T>
class linearLookup
{
public:
  linearLookup() {}
  
  //@note
  // A. size of x-vector should be same as y-vector
  // B. resolution of xValues should be same
  void genLookup( const T & minX, const T & maxX, std::vector< T > & yValues, int bankSize=10 )
  {
    int length( yValues.size() );
    
    if( length < 2 ) 
    {
//      printf("linearLookup::genLookup; length is smaller than 2 \n" );
      return;
    }
    
    // set the min and max here
    min_x = minX;
    x_resolution = (maxX-minX)/( bankSize*(length-1) );
    
    // set the data in between min and max
    for( int i=0; i<length-1; i++ )
    {
      T sy( yValues[i] );
      T ey( yValues[i+1] );      
      T local_res( ( ey - sy ) / static_cast<T>(bankSize) );
      
      for( int j(0); j<bankSize; j++ )
      {
        T tmp = sy + local_res*static_cast<T>(j);
        table.push_back( tmp );
      }
    }
    
    table.push_back( yValues[length-1] );
  }  
  
  T operator() ( const T & xValue )
  {
    if( table.empty() )
    {
//      printf("linearLookup::(); table is empty \n" );
      return T();
    }
    
    int xidx( static_cast<int>( (xValue - min_x)/x_resolution) );
    if( xidx < 0 ) xidx = 0;
    if( xidx >= table.size() ) xidx = table.size()-1;
    
    return table[xidx];
  }
  
  bool init;
  T min_x;
  T x_resolution;
  std::vector<T> table;
};

template<typename T, int _Size>
class recentMax
{
public:
  recentMax( const T & initialValue=-100 ) : initial_value(initialValue), idx(0), max_value(0)
  {
    clear();
  }

  T update( const T & incomingValue )
  {
    buffer[idx] = incomingValue;
    if( max_value < incomingValue )
    {
      max_value = incomingValue;
      idx_max = idx;
    }

    if( ++idx >= _Size )
    {
      idx = 0;
    }  
    
    if( idx_max == idx )
    {
      max_value = initial_value;
      for( int i(0); i<_Size; i++ ) 
      {
        if( max_value < buffer[i] )
        {
          max_value = buffer[i];
          idx_max = i;
        }
      }
    }

    return max_value;
  }

  void clear()
  {
    for( int i(0); i<_Size; i++ ) buffer[i] = static_cast<T>( initial_value );
    max_value = initial_value;
    idx = 0;
    idx_max = 0;
  }
  
public:
  T initial_value;
  int idx;
  int idx_max;
  T max_value;
  T buffer[_Size];
  
};


template<typename T, int _Size>
class movingAverage
{
public:
  movingAverage() : idx(0), sum(0)
  {
    clear();
  }

  T update( const T & incomingValue )
  {
    sum += incomingValue;    
    buffer[idx] = incomingValue;
    if( ++idx > _Size )
    {
      idx = 0;
      full = true;
    }
    sum -= buffer[idx];

    return sum;
  }

  void clear( const T & targetValue = static_cast<T>( 0 ) )
  {
    for( int i(0); i<_Size+1; i++ )
      buffer[i] = static_cast<T>( targetValue );
    sum = targetValue*_Size;
    idx = 0;
    full = false;
  }

  T getAvg()  { return sum / static_cast<T>(_Size); }
  T getAvgbyidx()
  {
    if ( idx > 0)
      return sum / static_cast<T>(idx);
    else
      return 0;
  }

  T getSum()  { return sum; }

  T getStdDev()
  {
    T sum_diff = 0.0;
    T sd = 0.0;
    T diff;
    T avg( getAvg() );
    int ridx(idx);
    for (int i = 0; i < _Size; i++)
    {
      if( ++ridx > _Size ) ridx = 0;
      diff = buffer[ridx] - avg;
      sum_diff += (diff * diff);      
    }

    sd = sqrt( sum_diff / (0.00000000001f+static_cast<T>(_Size-1)) );
    
    return sd;
  }

  T getItem(int index) { return buffer[index]; }

  int idx;
  T sum;  
  T buffer[_Size+1];
  bool full;
};


template<typename T, int _Size>
class medianFilter
{
public:
  medianFilter() : initialised(false), running_counter(0) {}
  T operator() ( const T & value )
  {
    if( !initialised  )
    {
      initialised = true;
      for( int i(0); i<_Size; i++ ) 
      {
        buffer[i] = value;
        sort_buffer.push_back( value );
      }
      running_counter = 0;
    }
    
    buffer[ running_counter++ ] = value;
    running_counter %= _Size;
    
    for( int i(0); i<_Size; i++ )
    {
      sort_buffer[i] = buffer[i];
    }
    
    std::sort( sort_buffer.begin(), sort_buffer.end() );
    last_value = sort_buffer[_Size/2];
    return last_value;
  }

  T operator() ( const T & value, int noLocalSample )
  {
    if( !initialised  )
    {
      initialised = true;
      for( int i(0); i<_Size; i++ )
      {
        buffer[i] = value;
        sort_buffer.push_back( value );
      }
      running_counter = 0;
    }

    buffer[ running_counter++ ] = value;
    running_counter %= _Size;

    for( int i(0); i<_Size; i++ )
    {
      sort_buffer[i] = buffer[i];
    }

    std::sort( sort_buffer.begin(), sort_buffer.end() );

    last_value = 0;
    for( int k(_Size/2-noLocalSample); k<=(_Size/2+noLocalSample); k++ )
    {
      last_value += sort_buffer[k];
    }
    last_value /= (2*noLocalSample+1);
    return last_value;
  }

  void clear( const T & clearValue = T() )
  {
    for( int i(0); i<_Size; i++ )
    {
      buffer[i] = clearValue;
      sort_buffer[i] = clearValue;
    }
    
    running_counter = 0;
  }

  T getValue() { return last_value; }
  
protected:
  bool initialised;
  T buffer[_Size];  
  std::vector<T> sort_buffer;
  int running_counter;
  T  last_value;
};


template<typename T, int _S>
class gaussianFilter
{
public:
  gaussianFilter( const T & sigma ) : idx(0)
  {
    int size( _S );
    int half_size(size/2);
    sum = 0;
    for( int i(-half_size); i<=(half_size); i++ )
    {
      float x( static_cast<float>(i) );
      sum += kernel[i+half_size] = exp( -(x*x)/(2.0*sigma*sigma) ) / sigma / sqrt(2.0/3.14159265358979324);
//      printf("gaussianFilter kernel[%d] = %f \n", i, kernel[i+half_size] );
      buf[i+half_size] = static_cast<T>( 0 );
    }    
  }

  T getValue() 
  {
    return result;
  }

  T update( const T & value )
  {
    // add data
    buf[idx++] = value;
    if( idx >= _S ) idx = 0;
    
    int i, k(idx);
    result = 0;
    for( i=0; i<_S; i++ )
    {
      result += kernel[i]*buf[k];
      k++;
      if( k >= _S ) k = 0;
    }
    result /= sum;
    return result;
  }
protected:
  T buf[_S];
  T kernel[_S];
  int idx;
  T sum;
public:
  T result;
};

template<int NL=50, int NH=10>
class NNCounter
{
public:
  NNCounter() : issue(false), nh_cnt(0), nl_cnt(0), no_issue(0)
  {
    clear();
  }
  
  int update( const bool issueFlag )
  {
    if( !issue ) issue = issueFlag;
    if( ++nl_cnt >= NL )
    {
      //if ( issue ) printf( " .... %d\n", no_issue );
      nl_cnt = 0;
      nh_buffer[nh_cnt] = issue;      
      no_issue += static_cast<int>( issue );      
      if( ++nh_cnt >= NH ) nh_cnt = 0;
      no_issue -= static_cast<int>( nh_buffer[nh_cnt] );
      issue = false;
    }
    
    return no_issue;
  }
  
  void clear()
  {
    no_issue = 0;
    nl_cnt = 0;
    nh_cnt = 0;
    issue = false;
    for( int i(0); i<NH; i++ )
    {
      nh_buffer[i] = false;
    }
  }
  
  int getNoIssue()
  {
    return no_issue;
  }
  
  bool issue;
  int nh_cnt;
  int nl_cnt;
  int no_issue;
  bool nh_buffer[NH];
};

template<typename T, int _S>
class gaussianDiff
{
public:
  gaussianDiff( const T & sigma ) : idx(0)
  {
    int size( _S );
    int half_size(size/2);
    for( int i(-half_size); i<=(half_size); i++ )
    {
      float x( static_cast<float>(i) );
      kernel[i+half_size] = (-x*exp( - x*x/2/sigma/sigma )) / ( (sigma*sigma*sigma)*sqrt(2.0f*3.14159265358979324));
//      printf("DG kernel[%d] = %f \n", i, kernel[i+half_size] );
      buf[i+half_size] = static_cast<T>( 0 );
    }    
  }
  T update( const T & value )
  {
    // add data
    buf[idx++] = value;
    if( idx >= _S ) idx = 0;

    int i, k(idx);
    diff = 0;
    for( i=0; i<_S; i++ )
    {
      diff += kernel[i]*buf[k];
      k++;
      if( k >= _S ) k = 0;
    }
    return diff;
  }
protected:
  T buf[_S];
  T kernel[_S];
  int idx;
public:
  T diff;
};


template<typename T>
class alphBeta
{
public:
  alphBeta( const T & alphaValue ) : alpha(alphaValue), result( static_cast<T>(0) ) {}

  T update( const T value )
  {
    result = result * alpha + ( static_cast<T>( 1 ) - alpha ) * value;

    return result;
  }
protected:
  T alpha;
  T result;
};


template<typename T, int _Size>
class coefStorage
{
public:
  coefStorage() {}
  coefStorage( const T & samplingFrequency, const T & cutOffFrequency ) : sampling_frequency(samplingFrequency), cut_off_frequency(cutOffFrequency) {}
  
  T sampling_frequency;
  T cut_off_frequency;
  T coe[_Size];
};


template<typename T, int _Size>
class HPFCoeff : public coefStorage<T,_Size>
{
public:
  HPFCoeff( const T & samplingFrequency, const T & cutOffFrequency ) : coefStorage<T,_Size>(samplingFrequency, cutOffFrequency) ,
    verbose(false)
  {
    T fc = ( static_cast<T>(2) * cutOffFrequency / samplingFrequency ) * 3.14159265358979324;
    T d1 = ( static_cast<T>(_Size) - static_cast<T>(1) ) / static_cast<T>(2);
    T d2;
    
    for( int i(0); i<_Size; ++i )
    {
      d2 = static_cast<T>(i) - d1;
      coefStorage<T,_Size>::coe[i] = d2 == 0 ? 1.0 - fc / 3.14159265358979324 : (sin(3.14159265358979324 * d2) - sin(fc * d2)) / (3.14159265358979324 * d2);
      if( verbose )
      {
        #ifndef FW_LEVEL
        printf( "%1.15lf\n", coefStorage<T,_Size>::coe[i] );
        #endif
      }
    }
    
  }
  
protected:
  bool verbose;  
};

template<typename T, int _Size>
class signalProcessor
{
public:
  signalProcessor( coefStorage<T,_Size> & coefficientStorage ) : coef(coefficientStorage), yn( static_cast<T>(0))
  {
    clear();
  }
  
  T operator() ( const T & incoming )
  {
    int i;
    for( i = (_Size-1); i>0; i-- )
    {
      x[i] = x[i-1];
    }
    x[0] = incoming;
    
    yn = 0;
    double yn(0);
    for( i=0; i<_Size; i++ )
    {
      yn += (x[i]*coef.coe[i]);
    }
    
    return yn;
  }
  
protected:
  void clear()
  {
    for( int i(0); i<_Size; i++ ) x[i] = static_cast<T>( 0 );
  }
  coefStorage<T,_Size> coef;
  T yn;
  T x[_Size];  
};


#endif//#define IMATH_HPP_
