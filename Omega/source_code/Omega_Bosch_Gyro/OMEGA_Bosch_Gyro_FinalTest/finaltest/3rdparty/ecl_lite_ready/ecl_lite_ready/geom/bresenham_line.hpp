#ifndef BRESENHAM_LINE_HPP_
#define BRESENHAM_LINE_HPP_

//@interface
// Frequently we use a kind of ray tracing for the robot navigation 
// One of example is that we need to trace the line to find the known obstacle on the grid map
// I would like to introduce the Bresenham's line algorithm here. Please find http://en.wikipedia.org/wiki/Bresenham's_line_algorithm
// Below way is only for the integer line tracing

class bresenhamLine
{
public:
  bresenhamLine() : x0(0), y0(0), x1(0), y1(0) {}
  bresenhamLine ( const int X0, const int Y0, const int X1, const int Y1 ) : x0(X0), y0(Y0), x1(X1), y1(Y1) {}
  void setPoints( const int X0, const int Y0, const int X1, const int Y1 )
  {
    x0 = X0;
    y0 = Y0;
    x1 = X1;
    y1 = Y1;
  }

  void operator() ()
  {
    pre_run();

    int dx( abs(x1-x0) );
    int dy( abs(y1-y0) );

    int sx( x0 < x1 ? 1 : -1 );
    int sy( y0 < y1 ? 1 : -1 );
  
    int err( dx-dy );
    int err2(0);    
    while( iteration < 1000 )
    {
      plot(x0,y0);
      if(x0==x1 && y0==y1) break;
      if( stop ) break;
      err2 = 2*err;
      if( err2 > -dy )
      {
        err -= dy;
        x0 += sx;
      }
      if( err2 < dx )
      {
        err += dx;
        y0 += sy;
      }

      iteration++;
    }

//    if( iteration <= 0 )
//    {
//      printf("ecl_lite_rady::bresenhamLine; iterate 1000 times \n" );
//    }
  }

  //@brief
  // this will be
  virtual void plot( const int x, const int y )
  {
  }
  virtual void pre_run()
  {
    stop = false;
    iteration = 0;
  }

protected:
  int x0, y0;    // this is the start point
  int x1, y1;    // this is the destination point
  bool stop;
  int iteration;
};



#endif//#define BRESENHAM_LINE_HPP_