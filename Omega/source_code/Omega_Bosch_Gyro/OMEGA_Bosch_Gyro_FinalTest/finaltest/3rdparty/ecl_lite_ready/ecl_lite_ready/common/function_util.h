#ifndef FUNCTION_UTIL_HPP_
#define FUNCTION_UTIL_HPP_

class mutex
{
public:
  mutex() : id(-1) {}
  virtual void lock() {id=1;}
  virtual void unlock() {id=0;}
  int id;
};


template<typename Tr>
class FunctionPtr0
{
public:
  typedef Tr (*proc)( void );

  FunctionPtr0() : processor(0)
  {}

  FunctionPtr0( proc thisIsProcessor ) : processor(thisIsProcessor)
  {}

  void connect( proc thisIsProcessor )
  {
    processor = thisIsProcessor;
  }
  
  Tr call()
  {
    if( processor != 0 )
    {
      return (*processor)();
    }
    else
    {
      return Tr();
    }
  }

private:
  proc processor;
};


template<typename Tr, typename Ta>
class FunctionPtr
{
public:
  typedef Tr (*proc)( Ta );

  FunctionPtr() : processor(0)
  {}

  FunctionPtr( proc thisIsProcessor ) : processor(thisIsProcessor)
  {}

  void connect( proc thisIsProcessor )
  {
    processor = thisIsProcessor;
  }
  
  Tr call( Ta arg )
  {
    if( processor != 0 )
    {
      return (*processor)( arg );
    }
    else
    {
      return Tr();
    }
  }

private:
  proc processor;
};

template<typename Tr, typename Ta1, typename Ta2>
class FunctionPtr2
{
public:
  typedef Tr (*proc)( Ta1, Ta2 );

  FunctionPtr2() : processor(0)
  {}

  FunctionPtr2( proc thisIsProcessor ) : processor(thisIsProcessor)
  {}

  void connect( proc thisIsProcessor )
  {
    processor = thisIsProcessor;
  }
  
  Tr call( Ta1 arg1, Ta2 arg2 )
  {
    if( processor != 0 )
    {
      return (*processor)( arg1, arg2 );
    }
    else
    {
      return Tr();
    }
  }

private:
  proc processor;
};


#endif

