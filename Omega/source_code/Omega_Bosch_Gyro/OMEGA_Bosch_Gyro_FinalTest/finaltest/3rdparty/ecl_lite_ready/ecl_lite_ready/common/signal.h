/**
 * @file include/signals/signal.hpp
 *
 * @brief Signals interface & implementation.
 *
 * Signals and slots provide a means for communication of events between
 * classes and methods in your application. This file includes the definition
 * details for the signals. Further details can be found in the @ref sigslotsDocumentation
 * "SigSlots" documentation.
 *
 * @author Daniel J. Stonier
 * @date July 2007
 **/
/*****************************************************************************
** Ifdefs
*****************************************************************************/

#ifndef YCL_SIGNALS_SIGNAL_HPP_
#define YCL_SIGNALS_SIGNAL_HPP_

/*****************************************************************************
** Includes
*****************************************************************************/

#include "slot.h"
#include "void.h"

/*****************************************************************************
** Using
*****************************************************************************/

/*****************************************************************************
** Namespaces
*****************************************************************************/

namespace efl {

/*****************************************************************************
** Using
*****************************************************************************/

/*****************************************************************************
** Signal
*****************************************************************************/
/**
 * @brief Part of a cross platformable solution for event callbacks with functors.
 *
 * Anywhere that triggers an event requiring a callback to be
 * executed can be implemented with a signal. These can be placed anywhere in your
 * code and can be connected to one or many. Further details can be found in the
 * @ref sigslotsDocumentation "SigSlots" documentation.
 *
 * Note:  This class is currently thread-safe and protected by mutexes. There is an
 * alternative way to do this, that is to fix the memory used by the classes when
 * instantiating it (template argument). So, some time later I wish to do a benchmarking
 * test, test the cost for running mutexes when you dont need them and compare
 * the mutex solution vs template solution. Benchmarking : w/ mutexes ~ 3.2-3.5us
 * for an empty signal/slot call. w/o mutexes ~2.9-3.3us for the same. Given that it
 * is a very low cost to maintain I'll leave it like it is for now rather than
 * having separate threaded and non-threaded versions.
 *
 * @sa Slot src/test/core/sigslots.cpp
 **/
template <typename T = Void> 
class Signal
{
    public:
        /*********************
        ** C&D's
        **********************/
        Signal() : slot_low(0), slot_high(0)   {}; /**< Default Constructor **/
        ~Signal() { /* slotsLowPriority.clear(); slotsHighPriority.clear(); */ }; /**< Default Destructor **/

        /*********************
        ** Signal Methods
        **********************/
        void emit(const T data);
        void connect( Slot<T,Low> &slot );
        void connect( Slot<T,High> &slot );
        void disconnect( Slot<T,Low> &slot );
        void disconnect( Slot<T,High> &slot );
        void clear();

    private:
        /*********************
        ** Variables
        **********************/
        long slotIdCounter;
    Slot<T,Low> * slot_low;
    Slot<T,High> * slot_high;
};

/*****************************************************************************
** Signal<Void>
*****************************************************************************/
/**
 * @brief Specialised signal that emits without passing a message.
 *
 * Specialised signal that only emits a signal but passes no data to their
 * respective slots. This is the default state for a signal, so you need only
 * define the signal as shown in this code snippet.
 *
 * @code
 * void cb_Empty()  { cout << "Empty Slot"; }
 *
 * int main() {
 * Slot<> slot;
 * Signal<> signal;
 *
 * slot.load(&cb_Empty);
 * signal.connect(slot);
 *
 * signal.emit();
 * @endcode
 * Further details can be found in the @ref sigslotsDocumentation "SigSlots"
 * documentation.
 **/
template <> class Signal<Void>
{
    /*********************
    ** Friends
    **********************/
    public:
        Signal() : slot_low(0), slot_high(0) {}; /**< Default Constructor **/
        ~Signal(); /**< Default Destructor **/

        void emit();
        void connect( Slot<Void,Low> &slot );
        void connect( Slot<Void,High> &slot );
        void disconnect( Slot<Void,Low> &slot );
        void disconnect( Slot<Void,High> &slot );
        void clear();

    private:
        /*********************
        ** Variables
        **********************/
    Slot<Void,Low> * slot_low;
    Slot<Void,High> * slot_high;
};


/*****************************************************************************
** Signal<T> Implementation
*****************************************************************************/
/**
 * Contact all connected slots and execute their code
 **/
template <typename T> void Signal<T>::emit(const T data)
{
  if( slot_high ) slot_high->processSignal(data);
  if( slot_low )   slot_low->processSignal(data);
}
/**
 * Connect a low priority slot to this signal.
 **/
template <typename T> void Signal<T>::connect( Slot<T,Low> &slot )
{
  slot_low = &slot;
};
/**
 * Connect a high priority slot to this signal.
 **/
template <typename T> void Signal<T>::connect( Slot<T,High> &slot )
{
  slot_high = &slot;
};
/**
 * Disconnect a low priority slot.
 **/
template <typename T> void Signal<T>::disconnect( Slot<T,Low> &slot )
{
  slot_low = 0;    
};
/**
 * Disconnect a high priority slot.
 **/
template <typename T> void Signal<T>::disconnect( Slot<T,High> &slot )
{
  slot_high = 0;
};
/**
 * Clear all connections to this signal.
 **/
template <typename T> void Signal<T>::clear()
{
  slot_high = 0;
  slot_low = 0;
}

/*****************************************************************************
** Signal<Void> Implementation
*****************************************************************************/
/**
 * Default destructor.
 */
inline Signal<Void>::~Signal() {
}
/**
 * Contact all connected slots and execute their code
 **/
inline void Signal<Void>::emit()
{
  if( slot_high )  slot_high->processSignal();
  if( slot_low )  slot_low->processSignal();
  
}
/**
 * Connect a low priority slot to this signal.
 **/
inline void Signal<Void>::connect( Slot<Void,Low> &slot )
{
    slot_low = &slot;
};
/**
 * Connect a high priority slot to this signal.
 **/
inline void Signal<Void>::connect( Slot<Void,High> &slot )
{
  slot_high = &slot;  
};
/**
 * Disconnect a low priority slot.
 **/
inline void Signal<Void>::disconnect( Slot<Void,Low> &slot )
{
    slot_low = 0;
};
/**
 * Disconnect a high priority slot.
 **/
inline void Signal<Void>::disconnect( Slot<Void,High> &slot )
{

    slot_high = 0;
};
/**
 * Clear all connections to this signal.
 **/
inline void Signal<Void>::clear()
{
  slot_low = 0;
  slot_high = 0;
}

}; // Namespace efl

#endif /*YCL_SIGNALS_SIGNAL_HPP_*/
