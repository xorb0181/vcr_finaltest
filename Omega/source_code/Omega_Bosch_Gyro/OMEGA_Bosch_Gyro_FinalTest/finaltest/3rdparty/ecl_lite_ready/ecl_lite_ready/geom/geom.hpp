#ifndef GEOM_HPP_
#define GEOM_HPP_


#include "../imath/imath.hpp"
#include "../imath/const.hpp"

#include <math.h>

template<typename T>
class point2D
{
  public:
    point2D()
      : x(0), y(0)
    {}
    
    point2D( const T xx, const T yy)
      : x(xx), y(yy)
    {}
    
    // @param const int angle : 0.1 degree (ex, 10 is equal to 1 degree)
    int norm_fake()
    {           
      return ( absValue()(x)+absValue()(y) )/2;
    }
    
    point2D<T> operator - ( const point2D<T> & p )
    {
      point2D<T> tmp;
      tmp.x = x - p.x;
      tmp.y = y - p.y;
      return tmp;
    }
    
    void rotate( const int angle )
    {
      int tx(x);
      int ty(y);
      float a( degree2Radian<int,float>()(angle,1800.0f) );
      float c( cosf( a ) );
      float s( sinf( a ) );
      
      x = static_cast<int>(c * tx - s * ty);
      y = static_cast<int>(s * tx + c * ty);
    }
    
  public:
    T x;   // robot coordinate along to x-axis in [mm]
    T y;   // robot coordinate along to y-axis in [mm]
};



template<typename T>
class point3D
{
  public:
    point3D()
      : x(0), y(0), z(0)
    {}
    
    point3D( const T xx, const T yy, const T zz)
      : x(xx), y(yy), z(zz)
    {}
    
  public:
    T x;   // robot coordinate along to x-axis in [mm]
    T y;   // robot coordinate along to y-axis in [mm]
    T z;   // robot coordinate along to z-axis in [mm]
};

template<typename T>
class pose2D
{
  public:
    pose2D()
      : p(), t(0)
    {}
    
  public:
    point2D<T> p;
    T   t;  // robot heading in 0.1 deg
};

typedef point3D <int     > iPoint3D;
typedef point3D <float   > fPoint3D;
typedef point3D <double  > dPoint3D;

typedef point2D <int     > iPoint2D;
typedef point2D <float   > fPoint2D;
typedef point2D <double  > dPoint2D;

#endif//#define IMATH_HPP_
