#ifndef BIT_OPERATIONS_HPP_
#define BIT_OPERATIONS_HPP_
    

#define B15    0x8000
#define B14    0x4000
#define B13    0x2000                
#define B12    0x1000
#define B11    0x0800
#define B10    0x0400
#define B9    0x0200
#define B8    0x0100
#define B7    0x0080
#define B6    0x0040
#define B5    0x0020
#define B4    0x0010
#define B3    0x0008
#define B2    0x0004
#define B1    0x0002
#define B0    0x0001



#define Setbit(x,y) (x |= (1<<y))
#define Clrbit(x,y) (x &= (~(1<<y)))
#define ABS(x)    (((x) >= 0) ? (x):(-(x)))


class bitCheckAt
{
public:
  template<typename T>
  bool operator() ( const T & value, const int bitOrder, bool desiredValue=true )
  {
    return ( (static_cast<bool>(value & (1<<bitOrder))) == desiredValue );
  }
};

#endif

