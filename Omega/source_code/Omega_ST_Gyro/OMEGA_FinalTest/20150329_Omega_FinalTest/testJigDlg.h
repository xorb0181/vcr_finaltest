// testJigDlg.h : header file
//

#if !defined(AFX_TESTJIGDLG_H__FF97657D_C7EA_42D3_8C3D_A2A0B83FC3DC__INCLUDED_)
#define AFX_TESTJIGDLG_H__FF97657D_C7EA_42D3_8C3D_A2A0B83FC3DC__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifdef _MSC_VER
#pragma warning( disable : 4786 )
#endif

#include <MAP>
#include "modules/dd_console.hpp"
#include "PopUp.h"

void NoticeResult(int list, bool bRes);
void NoticeResultEx(int list, int nRes);
void NoticeBoot(int nMain, int nSub, std::string strRes, bool bRes);

/////////////////////////////////////////////////////////////////////////////
// CTestJigDlg dialog

class CTestJigDlg : public CDialog
{
// Construction
public:
	CTestJigDlg(CWnd* pParent = NULL);	// standard constructor
  ~CTestJigDlg()  {}

// Dialog Data
	//{{AFX_DATA(CTestJigDlg)
	enum { IDD = IDD_TESTJIG_DIALOG };
	CStatic	m_static_nozzle;
	CStatic	m_pic_nozzle;
	CStatic	m_static_ui;
	CStatic	m_static_sensor;
	CStatic	m_static_psd_right_top;
	CStatic	m_static_psd_left_top;
	CStatic	m_static_psd_center_top;
	CStatic	m_static_dust_sensor;
	CStatic	m_pic_ui;
	CStatic	m_pic_sensor;
	CEdit	m_edit_ver_sensor;
	CEdit	m_edit_ver_ui;
	CStatic	m_pic_dust_sensor;
	CStatic	m_pic_psd_right_top;
	CStatic	m_pic_psd_left_top;
	CStatic	m_pic_psd_center_top;
	CStatic	m_static_vacuum;
	CStatic	m_static_docking;
	CStatic	m_static_dockingir;
	CEdit	m_edit_elapsed_time;
	CEdit	m_edit_time;
	CEdit	m_edit_ver_pcb;
	CEdit	m_edit_model_main;
	CComboBox	m_cbbox_model;
	CEdit	m_edit_port;
	CEdit	m_edit_serial_number;
	CButton	m_btn_ini;
	CStatic	m_static_dcj_signal;
	CStatic	m_static_gyro;
	CStatic	m_static_rtc;
	CStatic	m_static_rsbr;
	CStatic	m_static_mop;
	CStatic	m_static_hall;
	CStatic	m_static_dust;
	CStatic	m_pic_pver;
	CStatic	m_pic_rtc;
	CStatic	m_pic_dcjack_signal;
	CEdit	m_edit_ver_sw;
	CEdit	m_edit_ver_hw;
	CEdit	m_edit_ver_fw;
	CEdit	m_edit_model_sub;
	CStatic	m_pic_mop;
	CStatic	m_pic_dust;
	CStatic	m_pic_dcjack;
	CStatic	m_pic_docking;
	CStatic	m_pic_charging;
	CStatic	m_pic_hall;
	CEdit	m_edit_getid;
	CButton	m_btn_getid;
	CEdit	m_edit_setid;
	CButton	m_btn_setid;
	CStatic	m_pic_result;
	CStatic	m_pic_gyro;
	CEdit	m_edit_0x;
	CButton	m_btn_f4;
	CButton	m_btn_f3;
	CEdit	m_edit_cmd;
	CButton	m_btn_cmd;
	CButton	m_btn_gtest;
	CButton	m_btn_calib;
	CButton	m_btn_f2;
	CButton	m_btn_f1;
	CButton	m_btn_jpg;
	CButton	m_btn_connect;
	CButton	m_btn_img;
	CEdit	m_edit_camera_scale;
	CEdit	m_edit_camera_r;
	CEdit	m_edit_camera_opty;
	CEdit	m_edit_camera_optx;
	CEdit	m_edit_gytotype;
	CEdit	m_edit_camera_f;
	CEdit	m_edit_camera_camx;
	CEdit	m_edit_camera_camy;
	CEdit	m_edit_applib;
	CEdit	m_edit_appini;
	CEdit	m_edit_app_ver;
	CEdit	m_edit_app_model;
	CEdit	m_edit_camera_ang;
	CStatic	m_pic_psd_left;
	CStatic	m_pic_wheel_right;
	CStatic	m_pic_psd_right;
	CStatic	m_pic_wheel_left;
	CStatic	m_pic_psd_center;
	CStatic	m_pic_sidebrush_left;
	CStatic	m_pic_firmware;
	CStatic	m_pic_vacuum;
	CStatic	m_pic_sidebrush_right;
	CStatic	m_pic_hardware;
	CStatic	m_pic_ir_front;
	CStatic	m_pic_pe_for;
	CStatic	m_pic_ir_docking;
	CStatic	m_pic_brush;
	CStatic	m_pic_pe_back;
	CStatic	m_pic_version;
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CTestJigDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

public:

  CPopUp *pDialog;

	CFont	m_fedit;
	CFont	m_fedit_ver;
  CFont	m_fedit_port;
	CFont	m_ftext;  

	dd_console virtual_console;
	
  std::map<int, bool>      map_editbox;
  std::deque<std::string>  dq_cmd;

	bool update_control;
	bool activate_buttons;
	bool do_gtest;
	bool diag_result;
  bool enable_log_result;

	bool bGetAngX;
	bool bGetAngY;
  bool bGetChar;
  bool bInfiniteRequest;
  bool bElapsedTime;
  bool bEnableModelSet;

  bool isCreatedPopUp;

	int got_battery;
	int	saved_time;
	int delay_time;
  int wait_for_finish;
  int waiting_time_for_gtest_;
  int number_of_retrying_gtest_;

	int	cpuid_count;

  char app_version[10];

  std::string ini_location;
  std::string config_location;

  void initEditBox();
	void setEditBox(int id, bool state);
	bool disable_robot_log;
	void disableLog();
	void setMotion( int motionId, int motionData, int motionSpeed );
	void requestAngle();
	void InitPiCtrl();
	void Initialize();
  void sendCommand(std::string strCmd);
  void showPopUp(char* szMsg);
  void createPopUp();
  void closePopUp(bool);
  void ActivateControlForUser(BOOL);
  void ActivateControlForPop(BOOL);
  void getImage();
  void enableLogging(bool value);

  void setWaitForFinish(int value)          { wait_for_finish = value; }
  void setWaitForGTest(int value)           { waiting_time_for_gtest_ = value; }
  void setNumberOfRetryingGTest(int value)  { number_of_retrying_gtest_ = value; }

  void refreshSerialNumber();
  void setModelVersion(int& model_value);
  void setColor(CDC* pDC, int model_number);

  void addReqCommand(std::string cmd)
  {
#ifndef _TEST_
    dq_cmd.push_back(cmd);
#endif

    if (enable_log_result)
    {
      //std::cout << ">>> command buffer ===========================================================================" << std::endl;
      std::deque<std::string>::iterator it = dq_cmd.begin();
      int count = 0;
      for (; it!=dq_cmd.end(); it++)
      {
        //std::cout << ">>> command buffer[" << count << "] : " << *it << std::endl;
        std::stringstream ss_log;
        ss_log << ">>> command buffer[" << count << "] : " << *it << std::endl;
        virtual_console.slog_console(ss_log.str());
        count++;
      }
    }
  }

// Implementation
protected:
	HICON m_hIcon;
	CStatic m_pic_control;
	UINT m_uTimer;

	// Generated message map functions
	//{{AFX_MSG(CTestJigDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnBtnImg();
	afx_msg void OnBtnTest();
	afx_msg void OnBtnConnect();
	afx_msg void OnBtnJpg();
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnBtnTest2();
	afx_msg void OnButtonGtest();
	afx_msg void OnButtonCalibration();
	afx_msg void OnBtnF3();
	afx_msg void OnBtnF4();
	afx_msg void OnBTN0x();
	afx_msg void OnBtnSetid();
	afx_msg void OnBtnGetid();
	afx_msg void OnBtnInitialize();
	afx_msg void OnBtnSaveLog();
	afx_msg void OnDoubleclickedButtonHidden1();
	afx_msg void OnCommand();
	afx_msg void OnBtnIni();
	afx_msg void OnBtnInitializeSn();
	afx_msg void OnSelchangeComboModel();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TESTJIGDLG_H__FF97657D_C7EA_42D3_8C3D_A2A0B83FC3DC__INCLUDED_)
