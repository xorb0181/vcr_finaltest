#if !defined(AFX_POPUP_H__F17777CD_59D8_48A9_A98C_479BAC4E3FA8__INCLUDED_)
#define AFX_POPUP_H__F17777CD_59D8_48A9_A98C_479BAC4E3FA8__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// PopUp.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CPopUp dialog

class CPopUp : public CDialog
{
// Construction
public:
	CPopUp(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CPopUp)
	enum { IDD = IDD_POPUP };
	CStatic	m_static_msg;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CPopUp)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
//  CFont m_fmsg;

	// Generated message map functions
	//{{AFX_MSG(CPopUp)
		// NOTE: the ClassWizard will add member functions here
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

//  user function
public:
  void setTextMessage(char* szMsg);

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_POPUP_H__F17777CD_59D8_48A9_A98C_479BAC4E3FA8__INCLUDED_)
