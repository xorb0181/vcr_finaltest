// testJigDlg.cpp : implementation file
//

#include "stdafx.h"
#include "testJig.h"
#include "testJigDlg.h"

//#pragma comment(linker, "/SUBSYSTEM:WINDOWS")

//#define _WIN32_WINNT 0x0501  // Windows XP = 0501H
//#include <WINDOWS.H>
//#include <WINCON.H>

#include "modules/dd_console.hpp"
#include "modules/chessboard_detector.hpp"
#include "modules/gyro_tester.hpp"
#include "modules/txt2jpg.hpp"

//#define TEST_MARKER
//#define JFFS2_BOARD

#define WAITING_TIME_FOR_GTEST        20000
#define NUMBER_OF_RETRYING_GET_IMAGE  5

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

CTestJigDlg*		testJigDlg = 0;
CRITICAL_SECTION	crit;
gyroTester			gyro_tester;

//#define JFFS2_BOARD

/////////////////////////////////////////////////////////////////////////////
// CAboutDlg dialog used for App About

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();
	
	// Dialog Data
	//{{AFX_DATA(CAboutDlg)
	enum { IDD = IDD_ABOUTBOX };
	//}}AFX_DATA
	
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAboutDlg)
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL
	
	// Implementation
protected:
	//{{AFX_MSG(CAboutDlg)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
	//{{AFX_DATA_INIT(CAboutDlg)
	//}}AFX_DATA_INIT
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAboutDlg)
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
//{{AFX_MSG_MAP(CAboutDlg)
// No message handlers
//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CTestJigDlg dialog

CTestJigDlg::CTestJigDlg(CWnd* pParent /*=NULL*/)
: CDialog(CTestJigDlg::IDD, pParent),
number_of_retrying_gtest_(0),
wait_for_finish(0),
waiting_time_for_gtest_(0),
cpuid_count(0),
saved_time(0),
delay_time(0),
isCreatedPopUp(false),
bGetAngX(false),
bGetAngY(false),
bGetChar(false),
update_control(false),
do_gtest(false),
diag_result(false),
disable_robot_log(false),
enable_log_result(false),
got_battery(eGetFirstBatteryLevel),
ini_location(".\\diag_standard.ini"),
bInfiniteRequest(false)
, bElapsedTime(false)
, bEnableModelSet(false)
{
	//{{AFX_DATA_INIT(CTestJigDlg)
	//}}AFX_DATA_INIT
	// Note that LoadIcon does not require a subsequent DestroyIcon in Win32
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	testJigDlg = this;
}

void CTestJigDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CTestJigDlg)
	DDX_Control(pDX, IDC_STATIC_NOZZLE, m_static_nozzle);
	DDX_Control(pDX, IDC_PIC_NOZZLE, m_pic_nozzle);
	DDX_Control(pDX, IDC_STATIC_UI, m_static_ui);
	DDX_Control(pDX, IDC_STATIC_SENSOR, m_static_sensor);
	DDX_Control(pDX, IDC_STATIC_TRPSD, m_static_psd_right_top);
	DDX_Control(pDX, IDC_STATIC_TLPSD, m_static_psd_left_top);
	DDX_Control(pDX, IDC_STATIC_TCPSD, m_static_psd_center_top);
	DDX_Control(pDX, IDC_STATIC_DUST_SENSOR, m_static_dust_sensor);
	DDX_Control(pDX, IDC_PIC_UVER, m_pic_ui);
	DDX_Control(pDX, IDC_PIC_SVER, m_pic_sensor);
	DDX_Control(pDX, IDC_EDIT_VER_SENSOR, m_edit_ver_sensor);
	DDX_Control(pDX, IDC_EDIT_VER_UI, m_edit_ver_ui);
	DDX_Control(pDX, IDC_PIC_DUST_SENSOR, m_pic_dust_sensor);
	DDX_Control(pDX, IDC_PIC_PSD_RIGHT_TOP, m_pic_psd_right_top);
	DDX_Control(pDX, IDC_PIC_PSD_LEFT_TOP, m_pic_psd_left_top);
	DDX_Control(pDX, IDC_PIC_PSD_CENTER_TOP, m_pic_psd_center_top);
	DDX_Control(pDX, IDC_STATIC_VACUUM, m_static_vacuum);
	DDX_Control(pDX, IDC_STATIC_DOCKING, m_static_docking);
	DDX_Control(pDX, IDC_STATIC_DOCKINGIR, m_static_dockingir);
	DDX_Control(pDX, IDC_EDIT_ELAPSED_TIME, m_edit_elapsed_time);
	DDX_Control(pDX, IDC_EDIT_VER_PCB, m_edit_ver_pcb);
	DDX_Control(pDX, IDC_EDIT_MODEL_MAIN, m_edit_model_main);
	DDX_Control(pDX, IDC_COMBO_MODEL, m_cbbox_model);
	DDX_Control(pDX, IDC_EDIT_PORT, m_edit_port);
	DDX_Control(pDX, IDC_EDIT_SERIAL_NUMBER, m_edit_serial_number);
	DDX_Control(pDX, IDC_BTN_INI, m_btn_ini);
	DDX_Control(pDX, IDC_STATIC_DCJ_SIGNAL, m_static_dcj_signal);
	DDX_Control(pDX, IDC_STATIC_GYRO, m_static_gyro);
	DDX_Control(pDX, IDC_STATIC_RTC, m_static_rtc);
	DDX_Control(pDX, IDC_STATIC_RSBR, m_static_rsbr);
	DDX_Control(pDX, IDC_STATIC_MOP, m_static_mop);
	DDX_Control(pDX, IDC_STATIC_HALL, m_static_hall);
	DDX_Control(pDX, IDC_STATIC_DUST, m_static_dust);
	DDX_Control(pDX, IDC_PIC_PVER, m_pic_pver);
	DDX_Control(pDX, IDC_PIC_RTC, m_pic_rtc);
	DDX_Control(pDX, IDC_PIC_DCJ_SIGNAL, m_pic_dcjack_signal);
	DDX_Control(pDX, IDC_EDIT_VER_SW, m_edit_ver_sw);
	DDX_Control(pDX, IDC_EDIT_VER_HW, m_edit_ver_hw);
	DDX_Control(pDX, IDC_EDIT_VER_FW, m_edit_ver_fw);
	DDX_Control(pDX, IDC_EDIT_MODEL_SUB, m_edit_model_sub);
	DDX_Control(pDX, IDC_PIC_MOP, m_pic_mop);
	DDX_Control(pDX, IDC_PIC_DUST, m_pic_dust);
	DDX_Control(pDX, IDC_PIC_DCJACK, m_pic_dcjack);
	DDX_Control(pDX, IDC_PIC_DOCK, m_pic_docking);
	DDX_Control(pDX, IDC_PIC_CHAR, m_pic_charging);
	DDX_Control(pDX, IDC_PIC_HALL, m_pic_hall);
	DDX_Control(pDX, IDC_EDIT_GETID, m_edit_getid);
	DDX_Control(pDX, IDC_BTN_GETID, m_btn_getid);
	DDX_Control(pDX, IDC_EDIT_SETID, m_edit_setid);
	DDX_Control(pDX, IDC_BTN_SETID, m_btn_setid);
	DDX_Control(pDX, IDC_PIC_RESULT, m_pic_result);
	DDX_Control(pDX, IDC_PIC_GYRO, m_pic_gyro);
	DDX_Control(pDX, IDC_BTN_F4, m_btn_f4);
	DDX_Control(pDX, IDC_BTN_F3, m_btn_f3);
	DDX_Control(pDX, IDC_EDIT_COMMAND, m_edit_cmd);
	DDX_Control(pDX, IDOK_COMMAND, m_btn_cmd);
	DDX_Control(pDX, IDC_BUTTON_GTEST, m_btn_gtest);
	DDX_Control(pDX, IDC_BUTTON_CALIBRATION, m_btn_calib);
	DDX_Control(pDX, IDC_BTN_TEST2, m_btn_f2);
	DDX_Control(pDX, IDC_BTN_TEST, m_btn_f1);
	DDX_Control(pDX, IDC_BTN_JPG, m_btn_jpg);
	DDX_Control(pDX, IDC_BTN_CONNECT, m_btn_connect);
	DDX_Control(pDX, ID_BTN_IMG, m_btn_img);
	DDX_Control(pDX, IDC_EDIT_SCALE, m_edit_camera_scale);
	DDX_Control(pDX, IDC_EDIT_R, m_edit_camera_r);
	DDX_Control(pDX, IDC_EDIT_OPTY, m_edit_camera_opty);
	DDX_Control(pDX, IDC_EDIT_OPTX, m_edit_camera_optx);
	DDX_Control(pDX, IDC_EDIT_GYROTYPE, m_edit_gytotype);
	DDX_Control(pDX, IDC_EDIT_F, m_edit_camera_f);
	DDX_Control(pDX, IDC_EDIT_CAMX, m_edit_camera_camx);
	DDX_Control(pDX, IDC_EDIT_CAMY, m_edit_camera_camy);
	DDX_Control(pDX, IDC_EDIT_APPLIB_VER, m_edit_applib);
	DDX_Control(pDX, IDC_EDIT_APPINI, m_edit_appini);
	DDX_Control(pDX, IDC_EDIT_APP_VER, m_edit_app_ver);
	DDX_Control(pDX, IDC_EDIT_APP_MODEL, m_edit_app_model);
	DDX_Control(pDX, IDC_EDIT_ANG, m_edit_camera_ang);
	DDX_Control(pDX, IDC_PIC_LPSD, m_pic_psd_left);
	DDX_Control(pDX, IDC_PIC_RWH, m_pic_wheel_right);
	DDX_Control(pDX, IDC_PIC_RPSD, m_pic_psd_right);
	DDX_Control(pDX, IDC_PIC_LWH, m_pic_wheel_left);
	DDX_Control(pDX, IDC_PIC_CPSD, m_pic_psd_center);
	DDX_Control(pDX, IDC_PIC_LSBR, m_pic_sidebrush_left);
	DDX_Control(pDX, IDC_PIC_FVER, m_pic_firmware);
	DDX_Control(pDX, IDC_PIC_VACUUM, m_pic_vacuum);
	DDX_Control(pDX, IDC_PIC_RSBR, m_pic_sidebrush_right);
	DDX_Control(pDX, IDC_PIC_HVER, m_pic_hardware);
	DDX_Control(pDX, IDC_PIC_FRIR, m_pic_ir_front);
	DDX_Control(pDX, IDC_PIC_FPE, m_pic_pe_for);
	DDX_Control(pDX, IDC_PIC_DOIR, m_pic_ir_docking);
	DDX_Control(pDX, IDC_PIC_BRU, m_pic_brush);
	DDX_Control(pDX, IDC_PIC_BPE, m_pic_pe_back);
	DDX_Control(pDX, IDC_PIC_VER, m_pic_version);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CTestJigDlg, CDialog)
//{{AFX_MSG_MAP(CTestJigDlg)
ON_WM_SYSCOMMAND()
ON_WM_PAINT()
ON_WM_QUERYDRAGICON()
ON_BN_CLICKED(ID_BTN_IMG, OnBtnImg)
ON_BN_CLICKED(IDC_BTN_TEST, OnBtnTest)
ON_BN_CLICKED(IDC_BTN_CONNECT, OnBtnConnect)
ON_BN_CLICKED(IDC_BTN_JPG, OnBtnJpg)
ON_WM_CTLCOLOR()
ON_WM_TIMER()
ON_BN_CLICKED(IDC_BTN_TEST2, OnBtnTest2)
ON_BN_CLICKED(IDC_BUTTON_GTEST, OnButtonGtest)
ON_BN_CLICKED(IDC_BUTTON_CALIBRATION, OnButtonCalibration)
ON_BN_CLICKED(IDC_BTN_F3, OnBtnF3)
ON_BN_CLICKED(IDC_BTN_F4, OnBtnF4)
ON_BN_CLICKED(IDC_BTN_SETID, OnBtnSetid)
ON_BN_CLICKED(IDC_BTN_GETID, OnBtnGetid)
ON_BN_CLICKED(IDC_BTN_INITIALIZE, OnBtnInitialize)
ON_BN_CLICKED(IDC_BTN_SAVE_LOG, OnBtnSaveLog)
ON_BN_DOUBLECLICKED(IDC_BUTTON_HIDDEN_1, OnDoubleclickedButtonHidden1)
ON_BN_CLICKED(IDOK_COMMAND, OnCommand)
ON_BN_CLICKED(IDC_BTN_INI, OnBtnIni)
ON_BN_CLICKED(IDC_BTN_INITIALIZE_SN, OnBtnInitializeSn)
ON_CBN_SELCHANGE(IDC_COMBO_MODEL, OnSelchangeComboModel)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CTestJigDlg message handlers

BOOL CTestJigDlg::OnInitDialog()
{
	CDialog::OnInitDialog();
	
	//	HWND hWnd = GetConsoleWindow();
	//	ShowWindow( hWnd, SW_HIDE );
	
	// Add "About..." menu item to system menu.
	
	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);
	
	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}
	
	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon
	
	ini_location = ".\\diag_standard.ini";
	config_location = ".\\diag_config.ini";
	
	//  int model_value = GetPrivateProfileInt( _T("testJig"), "model", 0, ini_location.c_str());
	//  setModelVersion(model_value);
	
	int model_set = GetPrivateProfileInt( _T("system"), "modelset", 0, config_location.c_str());
	if (model_set != 0)
	{
		bEnableModelSet = TRUE;
		m_cbbox_model.EnableWindow(true);
	}
	
	GetPrivateProfileString( "system", "display", "unknown", app_version, 10, config_location.c_str());
	int model_type = GetPrivateProfileInt( _T("system"), "model", 0, config_location.c_str());
	setModelVersion(model_type);
	
	int elapsed_time = GetPrivateProfileInt( _T("system"), "time", 0, config_location.c_str());
	if (elapsed_time != 0)
	{
		bElapsedTime = TRUE;
		m_edit_elapsed_time.EnableWindow(true);
		virtual_console.enable_elapsed_time = true;
	}
	
	enable_log_result = ( GetPrivateProfileInt( _T("system"), "console", 0, config_location.c_str()) == 1 ) ? true : false;
	enableLogging(enable_log_result);
	
	bool whole_logging = ( GetPrivateProfileInt( _T("system"), "logging", 0, config_location.c_str()) == 1 ) ? true : false;
	virtual_console.enableWholeLogging(whole_logging);
	
	bool log_for_debug = ( GetPrivateProfileInt( _T("system"), "log4debug", 0, config_location.c_str()) == 1 ) ? true : false;
	virtual_console.enableLoggingForDebug(log_for_debug);
	
	InitializeCriticalSection(&crit);
	
	virtual_console.set_callback(NoticeResult);
	virtual_console.set_callback_ex(NoticeResultEx);
	virtual_console.set_callback_boot(NoticeBoot);
	
	SetTimer(0, 500, NULL);
	
	int nPort = GetPrivateProfileInt( _T("system"), "port", 0, config_location.c_str());
	if (nPort != 0)
	{
		char szPort[2] = {0};
		itoa(nPort, szPort, 10);
		SetDlgItemText(IDC_EDIT_PORT, szPort);
	}
	
	virtual_console.position_ = GetPrivateProfileInt( _T("system"), "position", 0, config_location.c_str());
	
	int nGTestOnly = GetPrivateProfileInt( _T("testJig"), "gtest", 0, ini_location.c_str());
	if (nGTestOnly)
	{
		virtual_console.gtest_only = true;
	}
	
	//	m_fedit.CreateFont(75, 0, 0, 0, FW_BOLD, FALSE, FALSE, 0, DEFAULT_CHARSET,
	//        OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_SWISS,
	//        "Arial");
	
	m_fedit.CreateFont(28, 0, 0, 0, FW_BOLD, FALSE, FALSE, 0, DEFAULT_CHARSET,
		OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_SWISS,
		"Arial");
	m_edit_model_sub.SetFont(&m_fedit);
	m_edit_model_sub.SetMargins(5,5);
	m_edit_model_main.SetFont(&m_fedit);
	m_edit_model_main.SetMargins(5,5);
	
	m_fedit_ver.CreateFont(28, 0, 0, 0, FW_BOLD, FALSE, FALSE, 0, DEFAULT_CHARSET,
        OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_SWISS,
        "Arial");
	m_edit_ver_sw.SetFont(&m_fedit_ver);
	m_edit_ver_fw.SetFont(&m_fedit_ver);
	m_edit_ver_ui.SetFont(&m_fedit_ver);
	m_edit_ver_sensor.SetFont(&m_fedit_ver);
	m_edit_ver_hw.SetFont(&m_fedit_ver);
	m_edit_ver_pcb.SetFont(&m_fedit_ver);
	
	m_ftext.CreateFont(20, 0, 0, 0, FW_BOLD, FALSE, FALSE, 0, DEFAULT_CHARSET,
        OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_SWISS,
        "Arial");
	
	m_edit_serial_number.SetFont(&m_ftext);
	
	m_fedit_port.CreateFont(24, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, DEFAULT_CHARSET,
		OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_SWISS,
		"Arial");
	
	m_edit_port.SetFont(&m_fedit_port);
	
	createPopUp();
	//Initialize();
	//  wait_for_finish = 0;
	
	//  CComboBox* cbbox = (CComboBox*)GetDlgItem(IDC_COMBO_MODEL);
	//  cbbox->AddString("Arte");
	//  cbbox->AddString("Pop");
	//  cbbox->AddString("Miele");
	
	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CTestJigDlg::ActivateControlForUser(BOOL makeWhat)
{
	m_btn_calib.EnableWindow(makeWhat);
	m_btn_cmd.EnableWindow(makeWhat);
	m_btn_f1.EnableWindow(makeWhat);
	m_btn_f2.EnableWindow(makeWhat);
	m_btn_f3.EnableWindow(makeWhat);
	m_btn_f4.EnableWindow(makeWhat);
	m_btn_gtest.EnableWindow(makeWhat);
	m_btn_img.EnableWindow(makeWhat);
	m_btn_jpg.EnableWindow(makeWhat);
	m_edit_cmd.EnableWindow(makeWhat);
	m_btn_setid.EnableWindow(makeWhat);
	m_edit_setid.EnableWindow(makeWhat);
	m_btn_getid.EnableWindow(makeWhat);
	m_edit_getid.EnableWindow(makeWhat);
	m_btn_ini.EnableWindow(makeWhat);
	
	m_btn_calib.ShowWindow(makeWhat);
	m_btn_cmd.ShowWindow(makeWhat);
	m_btn_f1.ShowWindow(makeWhat);
	m_btn_f2.ShowWindow(makeWhat);
	m_btn_f3.ShowWindow(makeWhat);
	m_btn_f4.ShowWindow(makeWhat);
	m_btn_gtest.ShowWindow(makeWhat);
	m_btn_img.ShowWindow(makeWhat);
	m_btn_jpg.ShowWindow(makeWhat);
	m_edit_cmd.ShowWindow(makeWhat);
	m_btn_setid.ShowWindow(makeWhat);
	m_edit_setid.ShowWindow(makeWhat);
	m_btn_getid.ShowWindow(makeWhat);
	m_edit_getid.ShowWindow(makeWhat);
	m_btn_ini.ShowWindow(makeWhat);
}

void CTestJigDlg::ActivateControlForPop(BOOL makeWhat)
{
	m_edit_app_model.EnableWindow(makeWhat);
	m_edit_app_ver.EnableWindow(makeWhat);
	m_edit_appini.EnableWindow(makeWhat);
	m_edit_applib.EnableWindow(makeWhat);
	m_edit_camera_ang.EnableWindow(makeWhat);
	m_edit_camera_camx.EnableWindow(makeWhat);
	m_edit_camera_camy.EnableWindow(makeWhat);
	m_edit_camera_f.EnableWindow(makeWhat);
	m_edit_camera_optx.EnableWindow(makeWhat);
	m_edit_camera_opty.EnableWindow(makeWhat);
	m_edit_camera_r.EnableWindow(makeWhat);
	m_edit_camera_scale.EnableWindow(makeWhat);
	m_edit_gytotype.EnableWindow(makeWhat);
	m_edit_ver_fw.EnableWindow(makeWhat);
	m_edit_model_sub.EnableWindow(makeWhat);
}

void CTestJigDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CTestJigDlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting
		
		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);
		
		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;
		
		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// The system calls this to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CTestJigDlg::OnQueryDragIcon()
{
	return (HCURSOR) m_hIcon;
}

HBRUSH CTestJigDlg::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor) 
{
	HBRUSH hbr = CDialog::OnCtlColor(pDC, pWnd, nCtlColor);
	
	switch(nCtlColor)
    {
    case CTLCOLOR_EDIT:
        {
			int control_id = pWnd->GetDlgCtrlID();
			switch(control_id)
			{
			case IDC_EDIT_ANG:
			case IDC_EDIT_APP_MODEL:
			case IDC_EDIT_APP_VER:
			case IDC_EDIT_APPINI:
			case IDC_EDIT_APPLIB_VER:
			case IDC_EDIT_CAMX:
			case IDC_EDIT_CAMY:
			case IDC_EDIT_GYROTYPE:
			case IDC_EDIT_F:
			case IDC_EDIT_OPTX:
			case IDC_EDIT_OPTY:
			case IDC_EDIT_R:
			case IDC_EDIT_SCALE:
			case IDC_EDIT_COMMAND:
				{
					if(!map_editbox[control_id])	{	pDC->SetBkColor(RGB(255, 0, 0));	}
				}	break;
				
			case IDC_EDIT_MODEL_MAIN:
				{
					setColor(pDC, virtual_console.getMainModel());
				}
				break;
				
			case IDC_EDIT_MODEL_SUB:
				{
					setColor(pDC, virtual_console.getSubModel());
				}	//	case IDC_EDIT_MODEL_SUB:
				break;
				
			default:	break;
			}	//	switch(control_id)
        }	//	case CTLCOLOR_EDIT:
		break;
    }	//	switch(nCtlColor)
	return hbr;
}

//	===============================================================================================================
//	USER FUNCTION
//	===============================================================================================================

void CTestJigDlg::Initialize()
{
	initEditBox();
	
	int nSN = GetPrivateProfileInt( _T("testJig"), "sn", 0, ini_location.c_str());
	if (nSN != 0)
	{
		char szSN[10] = {0,};
		itoa(nSN, szSN, 10);
		SetDlgItemText(IDC_EDIT_SERIAL_NUMBER, szSN);
		virtual_console.setSerialNumber(nSN);
	}
	else
	{
		AfxMessageBox(_T("The serial number = 0"), MB_OK|MB_ICONSTOP);
		SetDlgItemText(IDC_EDIT_SERIAL_NUMBER, "0");
		nSN = 0;
		virtual_console.setSerialNumber(nSN);
	}
	
	// get configuration
	
	//activate_buttons = ( GetPrivateProfileInt( _T("testJig"), "button", 0, INI_LOCATION) == 1 ) ? true : false;
	//if (!activate_buttons)  ActivateControlForUser(FALSE);
	
	if (virtual_console.getModelVersion() == model_pop || virtual_console.getModelVersion() == model_pop_lowcost )
	{
		ActivateControlForPop(FALSE);
	}
	else
	{
		ActivateControlForPop(TRUE);
	}
	
	( GetPrivateProfileInt( _T("testJig"), "button", 0, ini_location.c_str()) == 1 )
		? ActivateControlForUser(TRUE) : ActivateControlForUser(FALSE);
	
	//  enable_log_result = ( GetPrivateProfileInt( _T("system"), "console", 0, config_location.c_str()) == 1 ) ? true : false;
	//  enableLogging(enable_log_result);
	//  
	//  bool whole_logging = ( GetPrivateProfileInt( _T("system"), "logging", 0, config_location.c_str()) == 1 ) ? true : false;
	//  virtual_console.enableWholeLogging(whole_logging);
	//  
	//  bool log_for_debug = ( GetPrivateProfileInt( _T("system"), "log4debug", 0, config_location.c_str()) == 1 ) ? true : false;
	//  virtual_console.enableLoggingForDebug(log_for_debug);
	
	got_battery                = eGetFirstBatteryLevel;
	gyro_tester.nGyroRetry = 3; 
//	printf( "testJigDig::initialize, got_battery = eGetFirstBatteryLevel\n");
	
	number_of_retrying_gtest_  = NUMBER_OF_RETRYING_GET_IMAGE;
	waiting_time_for_gtest_    = 0;
	wait_for_finish            = 0;
	cpuid_count                = 0;
	saved_time                 = 0;
	delay_time                 = 0;
	
	//isCreatedPopUp = false;
	disable_robot_log  = false;
	update_control     = false;
	diag_result        = false;
	bGetAngX           = false;
	bGetAngY           = false;
	bGetChar           = false;
	do_gtest           = false;
	bInfiniteRequest   = false;
	
	dq_cmd.clear();
	
	InitPiCtrl();
	
	virtual_console.initializeOnce();
	NoticeResult(INFO_VERSION, true);
	NoticeResult(INFO_MODEL_SUB, true);
	
	if (isCreatedPopUp)  { showPopUp(" "); }
}

void CTestJigDlg::setMotion( int motionId, int motionData, int motionSpeed )
{
	char motion_str[256];
	
	sprintf( motion_str, 
#ifdef JFFS2_BOARD
		"./cmd SJ_EXTRA setMotion %d %d %d\n", 
#else
		"cmd SJ_EXTRA setMotion %d %d %d\n", 
#endif
		motionId, 
		motionData, 
		motionSpeed );
	
	virtual_console.write( (char*)motion_str, strlen(motion_str) );
}


void CTestJigDlg::requestAngle() 
{
#ifdef JFFS2_BOARD
	std::string cmd = "./cmd SJ_EXTRA getAngle \n";
#else
	std::string cmd = "cmd SJ_EXTRA getAngle \n";
#endif
	virtual_console.write( (char*)cmd.c_str(), cmd.length());	
}

void CTestJigDlg::OnTimer(UINT nIDEvent) 
{
	if ( nIDEvent == 0 )
	{
		if (bElapsedTime)
		{
			std::stringstream strm_temp;
			strm_temp << virtual_console.getElapsedTime() / 1000;
			SetDlgItemText(IDC_EDIT_ELAPSED_TIME, strm_temp.str().c_str());
		}
		
	#ifndef _TEST_
		//  static unsigned int tmp_spin = 0;
		//  static long tmp_lock = 0;
		//  static long tmp_recs = 0;
		//  
		//  if (tmp_spin != crit.SpinCount || tmp_lock != crit.LockCount || tmp_recs != crit.RecursionCount)
		//  {
		//    tmp_spin = crit.SpinCount;
		//    tmp_lock = crit.LockCount;
		//    tmp_recs = crit.RecursionCount;
		//    
		//    printf(">>> run | Spin(%3d), Lock(%3d), Recs(%3d) \n", tmp_spin, tmp_lock, tmp_recs);
		//  }
		
		//  static unsigned int count_for_infinite = 0;
		//  if ( bInfiniteRequest )
		//  {
		//    count_for_infinite = 0;
		//    printf(">>> infinite request \n");
		//    std::stringstream ss;
		//    ss << "SJ_SetData /dev/mtd2 product model_sub " << 1;
		//    addReqCommand(ss.str());
		//  }
		
		if (!dq_cmd.empty())
		{
			sendCommand( dq_cmd.front() );
			dq_cmd.pop_front();
		}
		
		if (update_control)
		{
			update_control = false;
			Invalidate(TRUE);
		}
/*		
		if (do_gtest)
		{
			if ( gyro_tester.turning_left != 0 && gyro_tester.turning_right != 0 )
			{
				bool res = virtual_console.set_gtest_result(gyro_tester.turning_left, 
					gyro_tester.turning_right, 
					gyro_tester.turning_origin );
				NoticeResult( DIAG_GTEST, res );
				
				testJigDlg->diag_result = res;
				NoticeResult( DIAG_RESULT, testJigDlg->diag_result );
				
				if (enable_log_result)
				{
					printf("++GTEST_ORIGIN:%1.2f, ++GTEST_LEFT:%1.2f, GTEST_RIGHT:%1.2f = %d\n"
						, gyro_tester.turning_origin , gyro_tester.turning_left, gyro_tester.turning_right, res);
				}
				
				gyro_tester.turning_left = gyro_tester.turning_right = 0;
				do_gtest = false;
			}
			
		}
*/
		if (do_gtest)
		{
			if ( gyro_tester.turning_left != 0 && gyro_tester.turning_right != 0 )
			{
				bool res = virtual_console.set_gtest_result(gyro_tester.turning_left, 
					gyro_tester.turning_right, 
					gyro_tester.turning_origin );
				
				if (enable_log_result)
				{
					printf("++GTEST_ORIGIN:%1.2f, ++GTEST_LEFT:%1.2f, GTEST_RIGHT:%1.2f = %d\n"
						, gyro_tester.turning_origin , gyro_tester.turning_left, gyro_tester.turning_right, res);
				}
				
				gyro_tester.turning_left = gyro_tester.turning_right = 0;
				do_gtest = false;

				if ( !res && gyro_tester.nGyroRetry != 0 )
				{
					gyro_tester.nGyroRetry--;
					printf( "\n\nFail to Gyro test. remained : [%d]\n\n", gyro_tester.nGyroRetry );
					Sleep(500);
					gyro_tester.pleaseRun( gyro_tester.nGyroRetry );
					do_gtest = true;
				}

				if ( res || gyro_tester.nGyroRetry <= 0 )
				{
					gyro_tester.nGyroRetry = 3;
					virtual_console.cb_notice_(ACTION_CLEAR_RTC, true);
					virtual_console.cb_notice_(ACTION_GOTO_TESTPOINT, true);

					NoticeResult( DIAG_GTEST, res );
					testJigDlg->diag_result = res;
					NoticeResult( DIAG_RESULT, testJigDlg->diag_result );
				}
			}
			
		}
		
		if ( saved_time != 0 )
		{
			if (saved_time + delay_time < GetTickCount())
			{
				sendCommand("SJ_GetBattery 0");
				saved_time = 0;
			}
			else if (bGetChar)
			{
				if (saved_time + 2000 < GetTickCount())
				{
					bGetChar = false;
					//sendCommand("SJ_GetPk 0");
					sendCommand("SJ_GetPk 3");
				}
			}
		}
		
		if (wait_for_finish != 0)
		{
			if ( !testJigDlg->virtual_console.gotFinishMsg() )
			{
				if ( wait_for_finish + 3000 < GetTickCount() )
				{
					sendCommand("SJ_DIAG StartEx 7");
					setWaitForFinish(GetTickCount());
				}
			}
			else
			{
				setWaitForFinish(0);
			}
		}
		
		gyro_tester.run();
		
		int motion_id;
		int motion_data;
		int motion_speed;
		if( testJigDlg && testJigDlg->virtual_console.JUART_is_connected() ) testJigDlg->virtual_console.EC();
		gyro_tester.getMotion( motion_id, motion_data, motion_speed );
		if( testJigDlg && testJigDlg->virtual_console.JUART_is_connected() ) testJigDlg->virtual_console.LC();
		if( motion_id > 1000 )
		{
			setMotion( motion_id, motion_data, motion_speed );
		}
		
		if( testJigDlg && testJigDlg->virtual_console.JUART_is_connected() ) testJigDlg->virtual_console.EC();
		bool should_i_take_photo = gyro_tester.shouldITakePhoto();
		if( testJigDlg && testJigDlg->virtual_console.JUART_is_connected() ) testJigDlg->virtual_console.LC();
		
		if( should_i_take_photo )
		{
			if (enable_log_result)
			{
				printf(">>> it should take photo! \n");
			}
			
			Sleep(4000);
			requestAngle();
			Sleep(2000);
			//OnBtnJpg();
			getImage();
		}
		
		if (waiting_time_for_gtest_ != 0 && waiting_time_for_gtest_ + WAITING_TIME_FOR_GTEST < GetTickCount())
		{
			if (number_of_retrying_gtest_ > 0)
			{
				if (enable_log_result)
				{
					printf(">>> number_of_retrying_gtest_(%d) (%d + %d, %u) \n"
						, number_of_retrying_gtest_
						, waiting_time_for_gtest_
						, WAITING_TIME_FOR_GTEST
						, GetTickCount());
				}
				
				setNumberOfRetryingGTest(number_of_retrying_gtest_-1);
				OnBtnJpg();
				setWaitForGTest(GetTickCount());
			}
			else if (number_of_retrying_gtest_ <= 0)
			{
				setWaitForGTest(0);
				
				AfxMessageBox(_T("자이로 테스트를 위한 이미지 정보를 가져올 수 없습니다."), MB_OK|MB_ICONWARNING );
				NoticeResult(ACTION_QUIT_DIAG, true);
				NoticeResult(DIAG_GTEST, false);
			}
		}
		//  else if (waiting_time_for_gtest_ != 0)
		//  {
		//    printf(">>> waiting_time_for_gtest_ != 0 (%d)\n"
		//      , waiting_time_for_gtest_);
		//  }
	#endif
	}

	if ( nIDEvent == 1 )
	{
		;
	}
	
	CDialog::OnTimer(nIDEvent);
}

void CTestJigDlg::initEditBox()
{
	map_editbox[IDC_EDIT_ANG]			= true;
	map_editbox[IDC_EDIT_APP_MODEL]		= true;
	map_editbox[IDC_EDIT_APP_VER]		= true;
	map_editbox[IDC_EDIT_APPINI]		= true;
	map_editbox[IDC_EDIT_APPLIB_VER]	= true;
	map_editbox[IDC_EDIT_CAMX]			= true;
	map_editbox[IDC_EDIT_CAMY]			= true;
	map_editbox[IDC_EDIT_GYROTYPE]		= true;
	map_editbox[IDC_EDIT_F]				= true;
	map_editbox[IDC_EDIT_OPTX]			= true;
	map_editbox[IDC_EDIT_OPTY]			= true;
	map_editbox[IDC_EDIT_R]				= true;
	map_editbox[IDC_EDIT_SCALE]			= true;
	map_editbox[IDC_EDIT_COMMAND]		= true;
}

void CTestJigDlg::setEditBox(int id, bool state)
{
	EnterCriticalSection(&crit);
	switch(id)
	{
	case IDC_EDIT_OPTX:
	case IDC_EDIT_OPTY:
		{
			map_editbox[id]	= state;
			update_control = true;
		}	break;
		
	default:
		break;
	}
}

void CTestJigDlg::setColor(CDC* pDC, int model_number)
{
	switch(model_number)
	{
	case 0:	{	pDC->SetTextColor(RGB(0, 0, 0));	}	break;
	case 1:	{	pDC->SetBkColor(RGB(255, 217, 0));	pDC->SetTextColor(RGB(0, 0, 255));	}	break;
	case 2:	{	pDC->SetBkColor(RGB(255, 142, 0));	pDC->SetTextColor(RGB(187, 255, 19));		}	break;
	case 3:	{	pDC->SetBkColor(RGB(128, 0, 0));	pDC->SetTextColor(RGB(255, 255, 0));		}	break;

	case 4:	{	pDC->SetBkColor(RGB(128, 128, 0));	pDC->SetTextColor(RGB(187, 255, 19));		}	break;
	case 6:	{	pDC->SetBkColor(RGB(0, 0, 255));	pDC->SetTextColor(RGB(187, 255, 19));		}	break;
		
	default:{	pDC->SetBkColor(RGB(0, 0, 0));		pDC->SetTextColor(RGB(255, 255, 255));	}	break;	//	default:
	}
}

void CTestJigDlg::disableLog()
{
	disable_robot_log = true;
	
#ifdef JFFS2_BOARD
	std::string cmd("./cmd SJ_EXTRA Log 0\n");
#else
	std::string cmd("cmd SJ_EXTRA Log 0\n");
#endif
	
	virtual_console.write( (char*)cmd.c_str(), cmd.length());
}

void testMarkerDetetor()
{
	chessboardDetector cd(158,122);
	cd.enableLogging(testJigDlg->enable_log_result);
	sv113Rectify sv113_rectifier(158,122); //what the 
	char image_file[1024];
	double angle;
	FILE * pf = fopen("cd_result.txt", "w");
	for( int i(0); i<54; i++ )
	{
		sprintf( image_file, "D:/workspace/iclebo2/svn_new/iclebo2/bin/Record00/Frame%05d.jpg", i );
		//		printf("%s ==> ", image_file );
		IplImage * image = cvLoadImage( image_file );
		sv113_rectifier.rectify( image );			
		cd.run( image, angle );
		//		printf("%f \n", angle );
		fprintf( pf, "%f, %f\n", cd.average_reprojection_error, cd.estiamted_heading_angle );
		cvReleaseImage( &image );
	}
	fclose(pf);
}

void CTestJigDlg::sendCommand(std::string strCmd)
{
	std::stringstream ss;
	ss << "cmd ";
	ss << strCmd;
	ss << "\n";
	if (enable_log_result)
	{
		//printf(">>> sendCommand: %s\n", strCmd.c_str());
		std::stringstream ss_log;
		ss_log << ">>> sendCommand: " << strCmd << "\n";
		virtual_console.slog_console(ss_log.str());
	}
	testJigDlg->virtual_console.write( (char*)ss.str().c_str(), ss.str().length() );
}

void CTestJigDlg::createPopUp()
{
	//  create pop up window
	if (!isCreatedPopUp)
	{
		isCreatedPopUp = true;
		pDialog = NULL;
		pDialog = new CPopUp(this);
		pDialog->Create(IDD_POPUP, NULL);
		pDialog->ShowWindow(SW_SHOW);
	}
	else
	{
		pDialog->ShowWindow(SW_SHOW);
		pDialog->UpdateWindow();
		//    EnableWindow(FALSE);
	}
}

void CTestJigDlg::showPopUp(char* szMsg)
{
	pDialog->setTextMessage(szMsg);
	//::Sleep(2000);
}

void CTestJigDlg::closePopUp(bool completelyClose = false)
{
	if (completelyClose)
	{
		if (pDialog != NULL)
		{
			isCreatedPopUp = false;
			pDialog->DestroyWindow();
			delete pDialog;
			pDialog = NULL;
			//      EnableWindow(TRUE);
		}
	}
	else
	{
		//    pDialog->ShowWindow(HIDE_WINDOW);
		//    pDialog->UpdateWindow();
		//    EnableWindow(TRUE);
	}
}

void CTestJigDlg::getImage()
{
	if (enable_log_result)
	{
		printf(">>> get image! \n");
	}
	
	waiting_time_for_gtest_ = GetTickCount();
	number_of_retrying_gtest_ = NUMBER_OF_RETRYING_GET_IMAGE;
	OnBtnJpg();
}

//	===============================================================================================================
//	BUTTON
//	===============================================================================================================

void CTestJigDlg::OnBtnTest2() 
{
	// TODO: Add your control notification handler code here
	std::string cmd = "./iclebo_s.out &\n";
	virtual_console.write( (char*)cmd.c_str(), cmd.length());	
}

void CTestJigDlg::OnCommand() 
{
	CString strCmd;
	GetDlgItemText(IDC_EDIT_COMMAND, strCmd);
	
#ifdef _TEST_
	
	int fileLength = strCmd.GetLength();
	if (fileLength == 0) { return; }
	
	switch(strCmd.GetAt(0))
	{
	case '@':
		{
			if (fileLength > 1)
			{
				CString fileName = strCmd.Right(fileLength-1);
				std::string strFile(fileName.operator LPCTSTR());
				virtual_console.readFileToBuffer(strFile);
			}
			
		} break;
		
	default:
		{
			if (fileLength > 1)
			{
				strCmd += '\n';
				virtual_console.set_buffer( LPSTR(LPCTSTR(strCmd)) );
			}
			
		} break;
	}
	
#else
	
	strCmd += '\n';
	virtual_console.write( LPSTR(LPCTSTR(strCmd)), strCmd.GetLength());
	
	//testJigDlg->addReqCommand(LPSTR(LPCTSTR(strCmd)),true);
#endif
	
	SetDlgItemText(IDC_EDIT_COMMAND, "");
}

void CTestJigDlg::OnBtnImg() 
{
	//virtual_console.show_jpg(0);
	
	//	#ifdef JFFS2_BOARD
	//		std::string strCmd = "./cmd jpg_show\n";
	//	#else
	//		std::string strCmd = "cmd jpg_show\n";
	//	#endif
	//
	//	testJigDlg->virtual_console.write( (char*)strCmd.c_str(), strCmd.length());
	
#ifdef _TEST_
	std::string str_flashcp = "flashcp ./temp.ini /dev/mtd2\n";
	std::string str_iclebo = "/root/iclebo_s.out\n";
	std::string str_kill = "kill $(pidof 'iclebo_s.out')\n";
	std::string str_temp = "cat > temp.ini\n";
	std::stringstream ss_temp;
	char ch = 0x4;
	ss_temp << "\n\n\n" << ch << "\n";
	
	testJigDlg->virtual_console.write((char*)str_kill.c_str(), str_kill.length());
	Sleep(500);
	
	testJigDlg->virtual_console.write((char*)str_temp.c_str(), str_temp.length());
	Sleep(500);
	
	testJigDlg->virtual_console.write((char*)ss_temp.str().c_str(), ss_temp.str().length());
	Sleep(500);
	
	testJigDlg->virtual_console.write((char*)str_flashcp.c_str(),str_flashcp.length());
	Sleep(500);
#endif
	
	//      testJigDlg->virtual_console.write((char*)str_iclebo.c_str(),str_iclebo.length());
	//      Sleep(500);
	
}

void CTestJigDlg::OnBtnTest() 
{
	// TODO: Add your control notification handler code here
	//	HBITMAP hbit;
	//	hbit = ::LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_BITMAP_FAIL));
	//	hbit = ::LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_BITMAP_SUCCESS));
	//	hbit = ::LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_BITMAP_PENDING));
	//	m_pic_version.SetBitmap(hbit);
	
	//	m_edit_gytotype.SetWindowText("TEST");
	
	//	setEditBox(IDC_EDIT_COMMAND, false);
	std::string strCmd("");
	
	char ascii[2] = {0};
	ascii[0] = 0x3;
	ascii[1] = 0xa;
	strCmd.append(ascii, 2);
	
	virtual_console.write( (char*)strCmd.c_str(), strCmd.length() );
}

void CTestJigDlg::OnBtnConnect() 
{
	// TODO: Add your control notification handler code here
	CString strCmd;
	GetDlgItemText(IDC_EDIT_PORT, strCmd);
	WritePrivateProfileString( _T("system"), _T("port"), LPSTR(LPCTSTR(strCmd)), config_location.c_str());
	int port = atoi(LPSTR(LPCTSTR(strCmd)));
	int ret = virtual_console.pre_run( port, 115200 );
	if (ret == 0)
	{
		AfxMessageBox(_T("Connected"), MB_OK|MB_ICONINFORMATION );
		virtual_console.clear_ib();
		virtual_console.clear_ob();
		
#ifdef _LOG_ALL_
		virtual_console.slog_console("======================================================================================");
		virtual_console.slog_console("		C O N N E C T E D ! ! !");
		virtual_console.slog_console("======================================================================================");
#endif
		
	}
	else
	{
		AfxMessageBox(_T("Failed"), MB_OK|MB_ICONSTOP);
	}
	
	disableLog();
}

void CTestJigDlg::OnBtnJpg() 
{
#ifdef JFFS2_BOARD
	std::string cmd = "./cmd jpg_show\n";
#else
	std::string cmd = "cmd jpg_show\n";
#endif
	
	virtual_console.write( (char*)cmd.c_str(), cmd.length());
	
#ifdef TEST_MARKER
	chessboardDetector cd(158,122);
    cd.enableLogging(enable_log_result);
	double angle;
	
	IplImage * image_0 = cvLoadImage( "image_0.jpg" );
	IplImage * image_1 = cvLoadImage( "image_1.jpg" );
	IplImage * image_2 = cvLoadImage( "image_2.jpg" );
	
	cd.run( image_0, angle );
	cd.run( image_1, angle );
	cd.run( image_2, angle );
	
	cvReleaseImage( &image_0 );
	cvReleaseImage( &image_1 );
	cvReleaseImage( &image_2 );
#endif
}

void CTestJigDlg::OnButtonGtest() 
{
	//	testMarkerDetetor();	return;
	disableLog();
	Sleep(500);
	gyro_tester.nGyroRetry = 3;
	gyro_tester.pleaseRun(); 
	do_gtest = true;
}

void CTestJigDlg::OnButtonCalibration() 
{
	testJigDlg->addReqCommand("cam_calib");
}


void CTestJigDlg::OnBtnF3() 
{
	// TODO: Add your control notification handler code here
	InitPiCtrl();
	
	switch(virtual_console.getModelVersion())
	{
	case model_pop:
	case model_pop_lowcost:
		NoticeResult(ACTION_DIAG,true);
		break;
		
	default:
		CString strCmd;
		GetDlgItemText(IDC_EDIT_COMMAND, strCmd);
		
		std::string cmd("");
		
		if (strCmd.Compare("") != 0)
		{
			cmd = "./cmd SJ_DIAG StartEx ";
			std::string tmp((LPCTSTR)strCmd);
			cmd += tmp;
			//		cmd.append(tmp, tmp.size());
			cmd.append("\n", 1);
		}
		else
		{
#ifdef JFFS2_BOARD
			cmd = "./cmd SJ_DIAG Start\n";
#else
			cmd = "cmd SJ_DIAG Start\n";
#endif
		}
		
		virtual_console.write( (char*)cmd.c_str(), cmd.length());
		break;
	}
}

void CTestJigDlg::OnBtnF4() 
{
	CString strCmd;
	GetDlgItemText(IDC_EDIT_COMMAND, strCmd);
	int fileLength = strCmd.GetLength();
	
	switch(strCmd.GetAt(0))
	{
	case '@':
		{
			if (fileLength > 1)
			{
				CString fileName = strCmd.Right(fileLength-1);
				std::string strFile(fileName.operator LPCTSTR());
				virtual_console.readFileToBuffer(strFile);
			}
		} break;
		
	case '$':
		{
			if (fileLength > 1)
			{
				CString fileName = strCmd.Right(fileLength-1);
				std::string strFile(fileName.operator LPCTSTR());
				txt2jpeg t2j;
				if ( !t2j(strFile.c_str()) )
				{
					printf(">>> t2j is failed.\n");
				}
			}
		} break;
		
	case '&':
		{
			if (fileLength > 1)
			{
				CString fileName = strCmd.Right(fileLength-1);
				std::string strFile(fileName.operator LPCTSTR());
				//virtual_console.readDataToBuffer(strFile);
				virtual_console.readFileToBufferForRun(strFile);
			}
		} break;
		
	case '!':
		{
			if (fileLength > 1)
			{
				CString cmd_name = strCmd.Right(fileLength-1);
				std::string str_cmd(cmd_name.operator LPCTSTR());
				if (str_cmd.find("log 1", 0) != -1)
				{
					printf(">>> log is enabled.\n");
					enableLogging(true);
				}
				else if (str_cmd.find("log 0", 0) != -1)
				{
					printf(">>> log is disabled.\n");
					enableLogging(false);
				}
			}
		} break;
		
	case '>':
		{
			if (fileLength > 1)
			{
				std::string str_msg(strCmd.Right(fileLength-1).operator LPCTSTR());
				if (virtual_console.findString(str_msg, "actctrl 0") != -1)
				{
					ActivateControlForUser(FALSE);
				}
				else if (virtual_console.findString(str_msg, "actctrl 1") != -1)
				{
					ActivateControlForUser(TRUE);
				}
			}
		} break;
		
	case '%':
		{
			SetDlgItemText(IDC_EDIT_COMMAND, "");
			bool res = virtual_console.set_gtest_result(1.0,1.0,1.0);
			testJigDlg->diag_result = res;
			NoticeResult(DIAG_GTEST, res);
			
			bool ret = virtual_console.getResult();
			if (!testJigDlg->diag_result)	ret = false;
			HBITMAP hbit = (ret) ? 
				::LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_BITMAP_RES_SUCCESS))
				: ::LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_BITMAP_RES_FAIL));
			
			testJigDlg->m_pic_result.SetBitmap(hbit);
			
			virtual_console.writeResult();
		}
		
	default:
		{
			virtual_console.set_buffer( LPSTR(LPCTSTR(strCmd)) );
		}
		break;
	}
	SetDlgItemText(IDC_EDIT_COMMAND, "");
	
	//  else if (strCmd.Compare("^C") == 0)
	//  {
	//    SetDlgItemText(IDC_EDIT_COMMAND, "");
	//    char ch = 0x3;
	//    std::stringstream ss;
	//    ss << ch;
	//    virtual_console.write((char*)ss.str().c_str(),ss.str().length());
	//  }
	//  else if (strCmd.Compare("^I") == 0)
	//  {
	//    bInfiniteRequest = true;
	//  }
}

void CTestJigDlg::OnBTN0x() 
{
	// TODO: Add your control notification handler code here
	CString strCmd;
	//	GetDlgItemText(IDC_EDIT_0x, strCmd);
	
	int ascii_dec = 0;
	//	for (; ascii_dec < 128; ascii_dec++)
	//	{
	//		printf("[%3d][%2x][%c]\n", ascii_dec, ascii_dec, ascii_dec);
	//	}
	
	ascii_dec = atoi(LPSTR(LPCTSTR(strCmd)));
	if (enable_log_result)
	{
		printf(">>> [%d][%x][%c]\n", ascii_dec, ascii_dec, ascii_dec);
	}
	//
	//	int ret = virtual_console.pre_run( port, 115200 );	
	char ascii = ascii_dec;
	//	std::string ascii_str;
	//	ascii_str.append(ascii, 1);
	//	virtual_console.write( (char*)ascii_str.c_str(), ascii_str.length() );
	//	
	std::stringstream ss_ascii;
	ss_ascii << ascii << "\n";
	virtual_console.write( (char*)ss_ascii.str().c_str(), ss_ascii.str().length() );
	
	//  for (int n(0); n<128; n++)
	//  {
	//    char tmp_ascii = n;
	//    std::stringstream ss_ascii;
	//    ss_ascii << tmp_ascii << "\n";
	//    virtual_console.write( (char*)ss_ascii.str().c_str(), ss_ascii.str().length() );
	//    ss_ascii.clear();
	//    ss_ascii.str("");
	//  }
}

void CTestJigDlg::OnBtnSetid() 
{
	// TODO: Add your control notification handler code here
	CString strCmd;
	GetDlgItemText(IDC_EDIT_SETID, strCmd);
	std::string strSndCmd("");
	
	if (strCmd != "")
	{
		int cpuid_temp = atoi(LPSTR(LPCTSTR(strCmd)));
		std::stringstream ss;
		ss << "cmd SJ_SETID " << cpuid_temp << "\n";
		strSndCmd = ss.str();
		
		std::stringstream strm_temp;
		strm_temp << ++cpuid_temp;
		SetDlgItemText(IDC_EDIT_SETID, strm_temp.str().c_str());
	}
	else
	{
		strSndCmd = "SJ_SETID 0";
		SetDlgItemText(IDC_EDIT_SETID, "1");
	}
	
	testJigDlg->sendCommand(strSndCmd);
	OnBtnGetid();
}

void CTestJigDlg::OnBtnGetid() 
{
	testJigDlg->addReqCommand("SJ_GETID 0");
}

void CTestJigDlg::OnBtnInitialize() 
{
	// TODO: Add your control notification handler code here
	InitPiCtrl();	
	//Initialize(); //  this function is called in the virtual_console.initialize()
	virtual_console.initialize();
}

void CTestJigDlg::OnBtnIni() 
{
	virtual_console.initializeOnce();
}

void CTestJigDlg::OnBtnSaveLog() 
{
	// TODO: Add your control notification handler code here
	virtual_console.getResult();
	virtual_console.writeResult();
}

int getEditBoxIndex(int nMain, int nSub)
{
	switch(nMain)
	{
	case ENUM_MAIN_INIREADER:
		{
			switch(nSub)
			{
			case ENUM_SUB_CAMERA_OPTX:	return IDC_EDIT_OPTX;
			case ENUM_SUB_CAMERA_OPTY:	return IDC_EDIT_OPTY;
			case ENUM_SUB_CAMERA_F:     return IDC_EDIT_F;
			case ENUM_SUB_CAMERA_CAMX:	return IDC_EDIT_CAMX;
			case ENUM_SUB_CAMERA_CAMY:	return IDC_EDIT_CAMY;
			case ENUM_SUB_CAMERA_ANG:   return IDC_EDIT_ANG;
			case ENUM_SUB_CAMERA_R:     return IDC_EDIT_R;
			case ENUM_SUB_CAMERA_SCALE:	return IDC_EDIT_SCALE;
				
			default:	break;
			}
			
		}	break;
		
	case ENUM_MAIN_INTEGRATOR:	return IDC_EDIT_GYROTYPE;
	case ENUM_MAIN_MPTOOL_APP:
		{
			switch(nSub)
			{
			case ENUM_SUB_MPTOOL_APP_VER:	return IDC_EDIT_APP_VER;
			case ENUM_SUB_MPTOOL_APP_MODEL:	return IDC_EDIT_APP_MODEL;
				
			default:	break;
			}	break;
		}	break;
		
	case ENUM_MAIN_MPTOOL_APPLIB:	return IDC_EDIT_APPLIB_VER;
	case ENUM_MAIN_MPTOOL_APPINI:	return IDC_EDIT_APPINI;
		//	case ENUM_MAIN_CSERIALLINUX:	return BOOT_LOG_CSERIALLINUX;	break;
		
	default:	break;
	}
	
	return -1;
}

void CTestJigDlg::refreshSerialNumber()
{
	std::stringstream ss;
	ss.clear();
	ss.str("");
	
	ss << "SJ_SETID " << virtual_console.serial_number_;
	addReqCommand(ss.str());
	
	ss.clear();
	ss.str("");
	ss << virtual_console.serial_number_;
	
	SetDlgItemText(IDC_EDIT_SERIAL_NUMBER, ss.str().c_str());
	
	WritePrivateProfileString( _T("testJig"), _T("sn")
		, ss.str().c_str(), ini_location.c_str());
}

void CTestJigDlg::InitPiCtrl()
{
	HBITMAP hbit;
	hbit = ::LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_BITMAP_PENDING));
	m_pic_version.SetBitmap(hbit);
	m_pic_firmware.SetBitmap(hbit);
	m_pic_ui.SetBitmap(hbit);
	m_pic_sensor.SetBitmap(hbit);
	m_pic_hardware.SetBitmap(hbit);
	m_pic_docking.SetBitmap(hbit);
	m_pic_brush.SetBitmap(hbit);
	m_pic_sidebrush_left.SetBitmap(hbit);
	m_pic_sidebrush_right.SetBitmap(hbit);
	m_pic_vacuum.SetBitmap(hbit);
	m_pic_psd_right.SetBitmap(hbit);
	m_pic_psd_center.SetBitmap(hbit);
	m_pic_psd_left.SetBitmap(hbit);

	m_pic_psd_right_top.SetBitmap(hbit);
	m_pic_psd_center_top.SetBitmap(hbit);
	m_pic_psd_left_top.SetBitmap(hbit);

	m_pic_wheel_right.SetBitmap(hbit);
	m_pic_wheel_left.SetBitmap(hbit);
	m_pic_pe_for.SetBitmap(hbit);
	m_pic_pe_back.SetBitmap(hbit);
	m_pic_ir_front.SetBitmap(hbit);
	m_pic_ir_docking.SetBitmap(hbit);
	m_pic_gyro.SetBitmap(hbit);
	m_pic_hall.SetBitmap(hbit);
	m_pic_charging.SetBitmap(hbit);
	m_pic_dust_sensor.SetBitmap(hbit);
	m_pic_dust.SetBitmap(hbit);
	m_pic_nozzle.SetBitmap(hbit);
	m_pic_dcjack.SetBitmap(hbit);
	m_pic_mop.SetBitmap(hbit);
	m_pic_dcjack_signal.SetBitmap(hbit);
	
#if ARTE_VERSION > 123
	m_pic_rtc.SetBitmap(hbit);
	m_pic_pver.SetBitmap(hbit);
#endif

	switch(virtual_console.getModelVersion())
	{
	case model_pop:
		{
			m_pic_dust_sensor.ShowWindow(HIDE_WINDOW);
			m_pic_dust.ShowWindow(HIDE_WINDOW);
			m_pic_nozzle.ShowWindow(HIDE_WINDOW);
			m_pic_sidebrush_right.ShowWindow(HIDE_WINDOW);
			m_pic_hall.ShowWindow(HIDE_WINDOW);
			m_pic_gyro.ShowWindow(HIDE_WINDOW);
			m_pic_rtc.ShowWindow(HIDE_WINDOW);
			m_static_dust_sensor.EnableWindow(FALSE);
			m_static_dust.EnableWindow(FALSE);
			m_static_rsbr.EnableWindow(FALSE);
			m_static_hall.EnableWindow(FALSE);
			m_static_rtc.EnableWindow(FALSE);
			m_static_gyro.EnableWindow(FALSE);
			
			if (virtual_console.getHwVersion() < 104)
			{
				m_pic_mop.ShowWindow(HIDE_WINDOW);
				m_static_mop.EnableWindow(FALSE);
			}
			else
			{
				m_pic_mop.ShowWindow(SW_SHOW);
				m_static_mop.EnableWindow(TRUE);
			}
			
		} break;
		
	case model_miele:
		m_pic_dust_sensor.ShowWindow(HIDE_WINDOW);
		m_pic_dust.ShowWindow(SW_SHOW);
		m_pic_nozzle.ShowWindow(HIDE_WINDOW);
		m_pic_mop.ShowWindow(HIDE_WINDOW);
		m_pic_sidebrush_right.ShowWindow(SW_SHOW);
		m_pic_hall.ShowWindow(SW_SHOW);
		m_pic_gyro.ShowWindow(SW_SHOW);
		m_pic_rtc.ShowWindow(SW_SHOW);
		m_static_dust_sensor.EnableWindow(FALSE);
		m_static_dust.EnableWindow(TRUE);
		m_static_mop.EnableWindow(FALSE);
		m_static_rsbr.EnableWindow(TRUE);
		m_static_hall.EnableWindow(TRUE);
		m_static_rtc.EnableWindow(TRUE);
		m_static_gyro.EnableWindow(TRUE);
		break;
		
	case model_pop_lowcost:
		m_pic_dust_sensor.ShowWindow(HIDE_WINDOW);
		m_pic_dust.ShowWindow(HIDE_WINDOW);
		m_pic_nozzle.ShowWindow(HIDE_WINDOW);
		m_pic_sidebrush_right.ShowWindow(HIDE_WINDOW);
		m_pic_hall.ShowWindow(HIDE_WINDOW);
		m_pic_gyro.ShowWindow(HIDE_WINDOW);
		m_pic_rtc.ShowWindow(HIDE_WINDOW);
		m_pic_vacuum.ShowWindow(HIDE_WINDOW);
		m_pic_docking.ShowWindow(HIDE_WINDOW);
		m_pic_ir_docking.ShowWindow(HIDE_WINDOW);
		m_pic_mop.ShowWindow(HIDE_WINDOW);
		
		m_static_dust_sensor.EnableWindow(FALSE);
		m_static_dust.EnableWindow(FALSE);
		m_static_rsbr.EnableWindow(FALSE);
		m_static_hall.EnableWindow(FALSE);
		m_static_rtc.EnableWindow(FALSE);
		m_static_gyro.EnableWindow(FALSE);
		m_static_mop.EnableWindow(FALSE);
		m_static_vacuum.EnableWindow(FALSE);
		m_static_docking.EnableWindow(FALSE);
		m_static_dockingir.EnableWindow(FALSE);
		break;

	case model_arte25:
	case model_arte30:
		m_pic_dust_sensor.ShowWindow(HIDE_WINDOW);
		m_pic_dust.ShowWindow(HIDE_WINDOW);
		m_pic_nozzle.ShowWindow(HIDE_WINDOW);
		m_static_dust_sensor.EnableWindow(FALSE);
		m_static_dust.EnableWindow(FALSE);
	break;
	

	case model_pluto:
		m_pic_psd_left_top.ShowWindow(SW_SHOW); 
		m_pic_psd_center_top.ShowWindow(SW_SHOW); 
		m_pic_psd_right_top.ShowWindow(SW_SHOW); 
		m_pic_dust.ShowWindow(SW_SHOW);
		m_pic_dust_sensor.ShowWindow(SW_SHOW);
		m_pic_nozzle.ShowWindow(SW_SHOW);
		m_pic_mop.ShowWindow(SW_SHOW);
		m_pic_sidebrush_right.ShowWindow(SW_SHOW);
		m_pic_hall.ShowWindow(SW_SHOW);
		m_pic_gyro.ShowWindow(SW_SHOW);
		m_pic_rtc.ShowWindow(SW_SHOW);
		m_static_psd_left_top.EnableWindow(TRUE); 
		m_static_psd_center_top.EnableWindow(TRUE); 
		m_static_psd_right_top.EnableWindow(TRUE); 
		m_static_dust.EnableWindow(TRUE);
		m_static_mop.EnableWindow(TRUE);
		m_static_rsbr.EnableWindow(TRUE);
		m_static_hall.EnableWindow(TRUE);
		m_static_rtc.EnableWindow(TRUE);
		m_static_gyro.EnableWindow(TRUE);
		break;
		
	default:
		m_pic_dust_sensor.ShowWindow(HIDE_WINDOW);
		m_pic_nozzle.ShowWindow(HIDE_WINDOW);
		m_pic_dust.ShowWindow(SW_SHOW);
		m_pic_mop.ShowWindow(SW_SHOW);
		m_pic_sidebrush_right.ShowWindow(SW_SHOW);
		m_pic_hall.ShowWindow(SW_SHOW);
		m_pic_gyro.ShowWindow(SW_SHOW);
		m_pic_rtc.ShowWindow(SW_SHOW);
		m_static_dust_sensor.EnableWindow(FALSE);
		m_static_dust.EnableWindow(TRUE);
		m_static_mop.EnableWindow(TRUE);
		m_static_rsbr.EnableWindow(TRUE);
		m_static_hall.EnableWindow(TRUE);
		m_static_rtc.EnableWindow(TRUE);
		m_static_gyro.EnableWindow(TRUE);
		m_pic_rtc.SetBitmap(hbit);
		break; 
	}

	if ( virtual_console.getModelVersion() != model_pluto )
	{
		m_pic_psd_left_top.ShowWindow(HIDE_WINDOW); 
		m_pic_psd_center_top.ShowWindow(HIDE_WINDOW); 
		m_pic_psd_right_top.ShowWindow(HIDE_WINDOW); 
		m_pic_ui.ShowWindow(HIDE_WINDOW); 
		m_pic_sensor.ShowWindow(HIDE_WINDOW); 
		m_pic_dust_sensor.ShowWindow(HIDE_WINDOW); 
		m_pic_nozzle.ShowWindow(HIDE_WINDOW); 
		m_static_psd_left_top.EnableWindow(FALSE); 
		m_static_psd_center_top.EnableWindow(FALSE); 
		m_static_psd_right_top.EnableWindow(FALSE); 
		m_static_ui.EnableWindow(FALSE); 
		m_static_sensor.EnableWindow(FALSE); 
		m_static_dust_sensor.EnableWindow(FALSE); 
		m_static_nozzle.EnableWindow(FALSE); 
	}
	
	gyro_tester.nGyroRetry = 3;
	hbit = ::LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_BITMAP_RES_PENDING));
	m_pic_result.SetBitmap(hbit);
}

//	===============================================================================================================
//	CALLBACK FUNCTION : NOTIFY RESULT for BOOT INFORMATION
//	===============================================================================================================

void NoticeBoot(int nMain, int nSub, std::string strRes, bool bRes)
{
	printf("++NoticeBoot(): [%d][%d][%s][%d]\n", nMain, nSub, strRes.c_str(), bRes);
	CString res = strRes.c_str();
	switch(nMain)
	{
		//	case ENUM_MAIN_CSV113GRABBER:	testJigDlg->m_edit_appini.SetDlgItemText(IDC_EDIT_, res);	break;
		
	case ENUM_MAIN_INIREADER:
		{
			switch(nSub)
			{
			case ENUM_SUB_CAMERA_OPTX:
				{
					testJigDlg->initEditBox();
					testJigDlg->m_edit_camera_optx.SetWindowText(res);
				}	break;
				
			case ENUM_SUB_CAMERA_OPTY:  testJigDlg->m_edit_camera_opty.SetWindowText(res);	break;
			case ENUM_SUB_CAMERA_F:		  testJigDlg->m_edit_camera_f.SetWindowText(res);		break;
			case ENUM_SUB_CAMERA_CAMX:	testJigDlg->m_edit_camera_camx.SetWindowText(res);	break;
			case ENUM_SUB_CAMERA_CAMY:	testJigDlg->m_edit_camera_camy.SetWindowText(res);	break;
			case ENUM_SUB_CAMERA_ANG:	  testJigDlg->m_edit_camera_ang.SetWindowText(res);	break;
			case ENUM_SUB_CAMERA_R:		  testJigDlg->m_edit_camera_r.SetWindowText(res);		break;
			case ENUM_SUB_CAMERA_SCALE:	testJigDlg->m_edit_camera_scale.SetWindowText(res);	break;
				
			default:	break;
			}
			
		}	break;
		
	case ENUM_MAIN_INTEGRATOR:	testJigDlg->m_edit_gytotype.SetWindowText(res);	break;
		
	case ENUM_MAIN_MPTOOL_APP:
		{
			switch(nSub)
			{
			case ENUM_SUB_MPTOOL_APP_VER:	  testJigDlg->m_edit_app_ver.SetWindowText(res);		break;
			case ENUM_SUB_MPTOOL_APP_MODEL:	testJigDlg->m_edit_app_model.SetWindowText(res);	break;
				
			default:	break;
				
			}	break;
			
		}	break;
		
	case ENUM_MAIN_MPTOOL_APPLIB:	testJigDlg->m_edit_applib.SetWindowText(res);	break;
	case ENUM_MAIN_MPTOOL_APPINI:
		{
			testJigDlg->m_edit_appini.SetWindowText(res);
			testJigDlg->virtual_console.set_boot(true);
			
			//NoticeResult(ACTION_GET_ANGLE_X, true);
			//NoticeResult(ACTION_CAMERA, true);
			
		}	break;
		//	case ENUM_MAIN_CSERIALLINUX:	return BOOT_LOG_CSERIALLINUX;	break;
		
	default:	break;
	}
	
	if (!bRes)	testJigDlg->setEditBox( getEditBoxIndex(nMain,nSub), false );
}

//	===============================================================================================================
//	CALLBACK FUNCTION : NOTIFY RESULT
//	===============================================================================================================

void NoticeResult(int list = 0, bool bRes = false)
{
	//  ::EnterCriticalSection(&crit);
	
	HBITMAP hbit;
	
	if (bRes)
	{
		hbit = ::LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_BITMAP_SUCCESS));
	}
	else
	{
		hbit = ::LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_BITMAP_FAIL));
	}
	
	switch(list)
	{		
	//	-----------------------------------------------------------------------------------------------
	//	DIAGNOSIS
	//	-----------------------------------------------------------------------------------------------	
	case ENUM_DIAG_RTC:               testJigDlg->m_pic_rtc.SetBitmap(hbit);	    		    break;
	case ENUM_DIAG_VERSION:           testJigDlg->m_pic_version.SetBitmap(hbit);			    break;
	case ENUM_DIAG_VERSION_FIRMWARE:  testJigDlg->m_pic_firmware.SetBitmap(hbit);			    break;
	case ENUM_DIAG_VERSION_UI:	      testJigDlg->m_pic_ui.SetBitmap(hbit);			            break;
	case ENUM_DIAG_VERSION_SENSOR:	  testJigDlg->m_pic_sensor.SetBitmap(hbit);			        break;
	case ENUM_DIAG_VERSION_HARDWARE:  testJigDlg->m_pic_hardware.SetBitmap(hbit);			    break;
	case ENUM_DIAG_VERSION_PCB:	      testJigDlg->m_pic_pver.SetBitmap(hbit);			        break;
	case ENUM_DIAG_BRUSH:             testJigDlg->m_pic_brush.SetBitmap(hbit);			        break;
	case ENUM_DIAG_SIDEBRUSH_LEFT:		testJigDlg->m_pic_sidebrush_left.SetBitmap(hbit);	    break;
	case ENUM_DIAG_SIDEBRUSH_RIGHT:		testJigDlg->m_pic_sidebrush_right.SetBitmap(hbit);   	break;
	case ENUM_DIAG_VACUUM:            testJigDlg->m_pic_vacuum.SetBitmap(hbit);			        break;
	case ENUM_DIAG_PSD_RIGHT:         testJigDlg->m_pic_psd_right.SetBitmap(hbit);		        break;
	case ENUM_DIAG_PSD_CENTER:        testJigDlg->m_pic_psd_center.SetBitmap(hbit);		        break;
	case ENUM_DIAG_PSD_LEFT:          testJigDlg->m_pic_psd_left.SetBitmap(hbit);		 	    break;
	case ENUM_DIAG_PSD_LEFT_TOP:	  testJigDlg->m_pic_psd_left_top.SetBitmap(hbit);	 	    break;
	case ENUM_DIAG_PSD_CENTER_TOP:	  testJigDlg->m_pic_psd_center_top.SetBitmap(hbit);		    break;
	case ENUM_DIAG_PSD_RIGHT_TOP:	  testJigDlg->m_pic_psd_right_top.SetBitmap(hbit);		    break;
	case ENUM_DIAG_IR_FRONT_RIGHT:
	case ENUM_DIAG_IR_FRONT_LEFT:     testJigDlg->m_pic_ir_front.SetBitmap(hbit);			    break;
	
	case ENUM_DIAG_IR_DOCKING_CENTER:		
		if ( testJigDlg->virtual_console.getModelVersion() == model_arte30 )
			testJigDlg->m_pic_ir_docking.SetBitmap(hbit);		    
		break;
	case ENUM_DIAG_IR_DOCKING_LEFT:		
		if ( testJigDlg->virtual_console.getModelVersion() != model_arte30 )
			testJigDlg->m_pic_ir_docking.SetBitmap(hbit);		    
		break;

	case ENUM_DIAG_PE_FORWARD:        testJigDlg->m_pic_pe_for.SetBitmap(hbit);			      break;
	case ENUM_DIAG_PE_BACKWARD:       testJigDlg->m_pic_pe_back.SetBitmap(hbit);			    break;
	case ENUM_DIAG_FORWARD_LEFT:      testJigDlg->m_pic_wheel_left.SetBitmap(hbit);		    break;
	case ENUM_DIAG_FORWARD_RIGHT:     testJigDlg->m_pic_wheel_right.SetBitmap(hbit);		  break;
	case ENUM_DIAG_BACKWARD_LEFT:     testJigDlg->m_pic_wheel_left.SetBitmap(hbit);		    break;
	case ENUM_DIAG_BACKWARD_RIGHT:    testJigDlg->m_pic_wheel_right.SetBitmap(hbit);		  break;
	case ENUM_DIAG_CHARGING:          testJigDlg->m_pic_charging.SetBitmap(hbit);			    break;
	case ENUM_DIAG_DCJACK_SIGNAL:     testJigDlg->m_pic_dcjack_signal.SetBitmap(hbit);    break;
	case ENUM_DIAG_CHARGER_ON:        testJigDlg->m_pic_docking.SetBitmap(hbit);			    break;
	case ENUM_DIAG_CHARGER_OFF:
	{
		if (bRes)
			hbit = ::LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_BITMAP_3I_ON));
		
		testJigDlg->m_pic_docking.SetBitmap(hbit);
		
	}	break;
		
	case ENUM_DIAG_HALL:
	{
		testJigDlg->m_pic_hall.SetBitmap(hbit);
		if (!bRes)	AfxMessageBox(_T("Hall Sensor Error!!"), MB_OK|MB_ICONSTOP);
		
	}	break;
		
	case DIAG_GTEST:					testJigDlg->m_pic_gyro.SetBitmap(hbit);				break;
		
	case DIAG_RESULT:
		{
			bool ret = testJigDlg->virtual_console.getResult();
			
			if ( testJigDlg->virtual_console.getModelVersion() != model_pop 
				&& testJigDlg->virtual_console.getModelVersion() != model_pop_lowcost )
			{
				if (!testJigDlg->diag_result)	ret = false;
			}
			
			hbit = (ret) ? 
				::LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_BITMAP_RES_SUCCESS))
				: ::LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_BITMAP_RES_FAIL));
			
			testJigDlg->m_pic_result.SetBitmap(hbit);
			//testJigDlg->virtual_console.writeResult();
			//testJigDlg->virtual_console.finish_test();
			
		}	break;
		
	case INFO_DIAG_START:
		{
			
		}	break;
		
		//	-----------------------------------------------------------------------------------------------
		//	ACTION
		//	-----------------------------------------------------------------------------------------------
		
	case ACTION_INIT_PROCESS:
		{
			testJigDlg->Initialize();
			
		} break;
		
	case ACTION_INIT_PIC:
		{
			testJigDlg->InitPiCtrl();
			testJigDlg->got_battery = eGetFirstBatteryLevel;
			//printf( "==== testJigDlg->got_battery = eGetFirstBatteryLevel\n" );
			
		}	break;
		
		
	case ACTION_CAMERA:
		{
#ifdef JFFS2_BOARD
			std::string strCmd = "./cmd jpg_show\n";
#else
			std::string strCmd = "cmd jpg_show\n";
#endif
			testJigDlg->virtual_console.write( (char*)strCmd.c_str(), strCmd.length());
			
		}	break;
		
	case ACTION_DIAG:
		{
			testJigDlg->virtual_console.set_boot(false);
			
			if (!testJigDlg->virtual_console.gtest_only)
			{
#ifdef JFFS2_BOARD
				std::string strCmd = "./cmd SJ_DIAG Start\n";
				testJigDlg->virtual_console.write( (char*)strCmd.c_str(), strCmd.length());
#else
				//std::string strCmd = "cmd SJ_DIAG Start\n";
				std::string strCmd = "SJ_DIAG Start";
				testJigDlg->addReqCommand(strCmd);
				//testJigDlg->sendCommand(strCmd);
#endif
			}
			
			//testJigDlg->Initialize();
			
		}	break;
		
	case ACTION_IMAGE:
		{
			if (testJigDlg->enable_log_result)
			{
				printf(">>> set the image\n");
			}
			
			gyro_tester.youGetImage( testJigDlg->virtual_console.raw_img, 
				testJigDlg->virtual_console.get_angle(), 
				testJigDlg->virtual_console.optX,
				testJigDlg->virtual_console.optY,
				testJigDlg->virtual_console.focal_len );
			
			testJigDlg->setWaitForGTest(0);
			testJigDlg->setNumberOfRetryingGTest(NUMBER_OF_RETRYING_GET_IMAGE);
			
		}	break;
		
	case ACTION_GTEST:
		{
			testJigDlg->disableLog();
			Sleep(500);
			gyro_tester.pleaseRun();
			testJigDlg->do_gtest = true;
			
		}	break;
		
	case ACTION_GET_BATTERY:
		{
			testJigDlg->addReqCommand("SJ_GetBattery 0");
			
		}	break;
		
	case ACTION_GOTO_TESTPOINT:
		{
			if (!testJigDlg->virtual_console.gtest_only)
			{
				switch( testJigDlg->virtual_console.getModelVersion() )  //taegoo
				{
				case model_pluto: 
					{
						printf("Final Test Program is done!!!!\n"); 
						testJigDlg->addReqCommand("SJ_DIAG StartEx 23");					
					}
					break; 
				default:
					{
						testJigDlg->addReqCommand("SJ_DIAG StartEx 21");
					}
					break; 
				}
			}
			
		}	break;
		
	case ACTION_QUIT_DIAG:
		{
			testJigDlg->addReqCommand("SJ_DIAG StartEx 22");
			
		}	break;
		
	case ACTION_BACKWARD_OUT:
		{
			testJigDlg->addReqCommand("SJ_DIAG StartEx 7");
			
		}	break;
		
	case ACTION_SET_MODEL_MAIN:
		{
			int main_model = testJigDlg->virtual_console.getMainModel();
			
			std::stringstream ss;
			
			if ( testJigDlg->virtual_console.getModelVersion() == model_pop
				|| testJigDlg->virtual_console.getModelVersion() == model_pop_lowcost )
			{
				ss << "SJ_SetINI 20 " << main_model;
			}
			else
			{
				ss << "SJ_SetData /dev/mtd2 product main_model " << main_model;
			}
			
			testJigDlg->addReqCommand(ss.str());
			
		} break;
		
	case ACTION_SET_MODEL_SUB:
		{
			int model_sub = testJigDlg->virtual_console.getSubModel();
			
			std::stringstream ss;
			
			if ( testJigDlg->virtual_console.getModelVersion() == model_pop
				|| testJigDlg->virtual_console.getModelVersion() == model_pop_lowcost )
			{
				ss << "SJ_SetINI 19 " << model_sub;
			}
			else
			{
				ss << "SJ_SetData /dev/mtd2 product sub_model " << model_sub;
			}
			
			testJigDlg->addReqCommand(ss.str());
			
		}	break;
		
	case ACTION_SET_RESERV_DISABLE:
		{
			testJigDlg->addReqCommand("SJ_SetData /dev/mtd3 reservation enable 0");
			//			testJigDlg->addReqCommand("SJ_GetData /dev/mtd3 reservation enable");
			
		}	break;
		
		//	case ACTION_GET_ANGLE_X:
		//		{
		//			AfxMessageBox(_T("로봇 전면을 위로 90도 들어주세요."), MB_OK|MB_ICONINFORMATION);
		//			testJigDlg->bGetAngX	= true;
		//			testJigDlg->saved_time = GetTickCount();
		//
		//		}	break;
		//		
		//	case ACTION_GET_ANGLE_Y:
		//		{
		//			AfxMessageBox(_T("로봇 왼쪽 측면이 위로 향하도록 90도 기울여주세요."), MB_OK|MB_ICONINFORMATION);
		//			testJigDlg->bGetAngY	= true;
		//			testJigDlg->saved_time = GetTickCount();
		//			
		//		}	break;
		
	case ACTION_GET_CHARGER_STATE:
		{
			//testJigDlg->addReqCommand("SJ_GetPk 0");
			
		} break;
		
	case ACTION_CLEAR_RTC:
		{
			testJigDlg->addReqCommand("SJ_ClearRtc 0");
			
		} break;
		
	case ACTION_SET_TIME:
		{
			testJigDlg->addReqCommand("SJ_SetTime 0 0 0");
			
		} break;
		
	case ACTION_GET_TIME:
		{
			testJigDlg->addReqCommand("SJ_GetTime 0");
			
		} break;
		
	case ACTION_WAIT_FOR_FINISH:
		{
			testJigDlg->setWaitForFinish(GetTickCount());
			
		} break;

	case ACTION_VERSION_TEST:
		{
			//testJigDlg->addReqCommand("SJ_DIAG StartEx 2");
			testJigDlg->showPopUp("Charging 검사 완료. 로봇을 위치해 주세요.");
		} break;
		
	case ACTION_ENTER_KEY:
		{
			std::string str_cmd = "\n\n";
			testJigDlg->virtual_console.write( (char*)str_cmd.c_str(), str_cmd.length());
			
		} break;
		
	case ACTION_DISABLE_RETRY:
		{
			testJigDlg->setWaitForFinish(0);
			testJigDlg->setWaitForGTest(0);
			
		} break;
		
	case ACTION_TEST_DIAG:
		{
			bool ret = testJigDlg->virtual_console.getResult();
			
			printf(">>> DIAG_RESULT(%d) \n", ret);
			
			hbit = (ret) ? 
				::LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_BITMAP_RES_SUCCESS))
				: ::LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_BITMAP_RES_FAIL));
			
			testJigDlg->m_pic_result.SetBitmap(hbit);
			
		} break;
		
	case ACTION_GET_PSD:
		{
			testJigDlg->addReqCommand("SJ_GetPk 0");
			
		} break;
		
	case ACTION_SET_SN:
		{
			
		} break;
		
	case ACTION_SET_DIAGTIME:
		{
			std::stringstream ss;
			ss << "SJ_SetData /dev/mtd2 product diag_time ";
			ss << testJigDlg->virtual_console.getLoggedTime();
			testJigDlg->addReqCommand(ss.str());
		}
		
		//	-----------------------------------------------------------------------------------------------
		//	INFORMATION
		//	-----------------------------------------------------------------------------------------------
		
	case INFO_ANGLE:
		{
			//			printf("++callback_angle: %1.2f\n", testJigDlg->virtual_console.get_angle());
		}	break;
		
	case INFO_MOTION:
		{
			//			printf("++callback_motion: %d %d\n"
			//				, testJigDlg->virtual_console.get_motion_id()
			//				, testJigDlg->virtual_console.get_motion_state()
			//				);
			gyro_tester.youGetMotionInfo( testJigDlg->virtual_console.get_motion_id(),
				testJigDlg->virtual_console.get_motion_state() );
			
		}	break;
		
	case INFO_GOT_CPUID:
		{
			std::stringstream strm_temp;
			strm_temp << testJigDlg->virtual_console.get_cpuid();
			testJigDlg->m_edit_getid.SetWindowText(strm_temp.str().c_str());
			
		}	break;
		
	case INFO_GOT_BATTERY:
		{
			if (testJigDlg->enable_log_result)
			{
				//printf(">>> INFO_GOT_BATTERY(%d) \n", testJigDlg->got_battery);
				std::stringstream ss_log;
				ss_log << ">>> INFO_GOT_BATTERY(";
				ss_log << testJigDlg->got_battery << ") \n";
				testJigDlg->virtual_console.slog_console(ss_log.str());
			}
			
			switch(testJigDlg->got_battery)
			{
			case eGetFirstBatteryLevel:
				{
					testJigDlg->got_battery = eGetSecondBatteryLevel;
					printf( "testJigDig::NoticeResult, got_battery = eGetSecondBatteryLevel\n");
					testJigDlg->saved_time  = GetTickCount();
					testJigDlg->delay_time  = 7500;
					testJigDlg->bGetChar    = true;
					
					testJigDlg->virtual_console.set_battery(eGetFirstBatteryLevel);
					
				}	break;
				
			case eGetSecondBatteryLevel:
				{
					//testJigDlg->addReqCommand("SJ_GetPk 0");
					
					testJigDlg->got_battery = eGetFirstBatteryLevel;
					printf( "testJigDig::NoticeResult, got_battery = eGetFirstBatteryLevel\n");
					testJigDlg->virtual_console.set_battery(eGetSecondBatteryLevel);
					
				}	break;
				
				//      case eGetSecondBatteryLevelEx:
				//        {
				//          testJigDlg->got_battery = eGetFirstBatteryLevel;
				//          testJigDlg->virtual_console.set_battery(eGetSecondBatteryLevelEx);
				//          
				//        }	break;        
			}
			
		}	break;
		
	case INFO_MODEL_SUB:
		{
			std::stringstream strm;
			strm << testJigDlg->virtual_console.getSubModel();
			testJigDlg->m_edit_model_sub.SetWindowText(strm.str().c_str());
			strm.clear();
			strm.str("");
			strm << testJigDlg->virtual_console.getMainModel();
			testJigDlg->m_edit_model_main.SetWindowText(strm.str().c_str());
			
		}	break;
		
	case INFO_VERSION:
		{
			std::stringstream strm;
			
			strm << testJigDlg->virtual_console.get_version(ENUM_DIAG_VERSION);
			testJigDlg->m_edit_ver_sw.SetWindowText(strm.str().c_str());
			strm.clear();
			strm.str(std::string());
			
			strm << testJigDlg->virtual_console.get_version(ENUM_DIAG_VERSION_FIRMWARE);
			testJigDlg->m_edit_ver_fw.SetWindowText(strm.str().c_str());
			strm.clear();
			strm.str(std::string());

			strm << testJigDlg->virtual_console.get_version(ENUM_DIAG_VERSION_UI);
			testJigDlg->m_edit_ver_ui.SetWindowText(strm.str().c_str());
			strm.clear();
			strm.str(std::string());

	    	strm << testJigDlg->virtual_console.get_version(ENUM_DIAG_VERSION_SENSOR);
			testJigDlg->m_edit_ver_sensor.SetWindowText(strm.str().c_str());
			strm.clear();
			strm.str(std::string());
			
			strm << testJigDlg->virtual_console.get_version(ENUM_DIAG_VERSION_HARDWARE);
			testJigDlg->m_edit_ver_hw.SetWindowText(strm.str().c_str());
			strm.clear();
			strm.str(std::string());
			
			testJigDlg->virtual_console.getPcbVersion(strm);
			testJigDlg->m_edit_ver_pcb.SetWindowText(strm.str().c_str());
			
		}	break;
		
	case INFO_PSD_CHECK_END:
		//testJigDlg->closePopUp();
		break;
		
	case INFO_PSD_CHECK_NEXT:
		{
			if (!bRes)
			{
				testJigDlg->pDialog->setTextMessage("PSD 검사에 실패하였습니다.");
				testJigDlg->virtual_console.setPsdCheckState(ePsdCheckFail);
				NoticeResult(ACTION_QUIT_DIAG, false);
			}
			else
			{
				if (testJigDlg->enable_log_result)
				{
					//printf(">>> INFO_PSD_CHECK_NEXT:%d\n", testJigDlg->virtual_console.getPsdCheckState());
					std::stringstream ss_log;
					ss_log << ">>> INFO_PSD_CHECK_NEXT:";
					ss_log << testJigDlg->virtual_console.getPsdCheckState() << "\n";
					testJigDlg->virtual_console.slog_console(ss_log.str());
				}
				//  top view
				//  right => center => left
				switch(testJigDlg->virtual_console.getPsdCheckState())
				{
				case ePsdCheckStart:
					testJigDlg->createPopUp();
					testJigDlg->showPopUp("PSD 테스트를 시작합니다.\n오른쪽 PSD를 막아주세요.");
					break;
					
				case ePsdCheckLeftBlock:
					testJigDlg->showPopUp("오른쪽 PSD 막힘 감지 완료.\n막은 것을 떼어주세요.");
					break;
					
				case ePsdCheckLeftClear:
					testJigDlg->showPopUp("오른쪽 PSD 열림 감지 완료.\n가운데 PSD를 막아주세요.");
					break;
					
				case ePsdCheckCenterBlock:
					testJigDlg->showPopUp("가운데 PSD 막힘 감지 완료.\n막은 것을 떼어주세요.");
					break;
					
				case ePsdCheckCenterClear:
					testJigDlg->showPopUp("가운데 PSD 열림 감지 완료.\n왼쪽 PSD를 막아주세요.");
					break;
					
				case ePsdCheckRightBlock:
					testJigDlg->showPopUp("왼쪽 PSD 막힘 감지 완료.\n막은 것을 떼어주세요.");
					break;
					
				case ePsdCheckRightClear:
					testJigDlg->showPopUp("왼쪽 바닥 PSD 열림 감지 완료.\n오른쪽 PSD를 막아주세요.");
					break;
					


				case ePsdCheckTopLeftBlock:
					testJigDlg->showPopUp("오른쪽 PSD 막힘 감지 완료.\n막은 것을 떼어주세요.");
					break;
					
				case ePsdCheckTopLeftClear:
					testJigDlg->showPopUp("오른쪽 PSD 열림 감지 완료.\n가운데 PSD를 막아주세요.");
					break;
					
				case ePsdCheckTopCenterBlock:
					testJigDlg->showPopUp("가운데 PSD 막힘 감지 완료.\n막은 것을 떼어주세요.");
					break;
					
				case ePsdCheckTopCenterClear:
					testJigDlg->showPopUp("가운데 PSD 열림 감지 완료.\n왼쪽 PSD를 막아주세요.");
					break;
					
				case ePsdCheckTopRightBlock:
					testJigDlg->showPopUp("왼쪽 PSD 막힘 감지 완료.\n막은 것을 떼어주세요.");
					break;
					
				case ePsdCheckTopRightClear:
					testJigDlg->showPopUp("왼쪽 PSD 열림 감지 완료.");
					break;



				case ePsdCheckEnd:
					testJigDlg->showPopUp("PSD 검사가 완료되었습니다.\n");
					//          testJigDlg->virtual_console.setPsdCheckState(ePsdCheckNone);
					if ( testJigDlg->virtual_console.getModelVersion() == model_pop_lowcost )					
					{	// start charging test via DC-jack for POP 3.0
						Sleep(1500);
						testJigDlg->showPopUp("Charging 검사를 시작합니다. DC-jack을 연결해 주세요");
						testJigDlg->addReqCommand("SJ_DIAG StartEx 27");
					}
					break;
					
				case ePsdCheckFail:
					testJigDlg->pDialog->setTextMessage("PSD 검사에 실패하였습니다.");
					testJigDlg->virtual_console.setPsdCheckState(ePsdCheckFail);
					NoticeResult(ACTION_QUIT_DIAG, false);
					break;
					
				default:
					break;
				}
			}
			
			//if (testJigDlg->virtual_console.getPsdCheckState() == ePsdCheckEnd){  testJigDlg->closePopUp(); }
			
		} break;
		
	case INFO_GOT_IMAGE:
		{
			//      printf(">>> INFO_GOT_IMAGE \n");
			//      testJigDlg->waiting_time_for_gtest_ = 0;
			//      testJigDlg->number_of_retrying_gtest_ = 3;
			
		} break;
		
	case INFO_SERIAL_NUMBER:
		{
			testJigDlg->refreshSerialNumber();
		} break;
		
		//	-----------------------------------------------------------------------------------------------
		//	ERROR
		//	-----------------------------------------------------------------------------------------------
		
	case ERR_ANGLE:		{	AfxMessageBox(_T("Angle Error!!"), MB_OK|MB_ICONSTOP);	}	break;
	case ERR_BUMPER:	{	AfxMessageBox(_T("Bumper Error!!"), MB_OK|MB_ICONSTOP);	}	break;
	case ERR_PSD:
		{			
			switch( testJigDlg->virtual_console.getModelVersion() ) 
			{
			  case model_pluto: { testJigDlg->addReqCommand("SJ_DIAG StartEx 24"); } break;
			  default:          { testJigDlg->addReqCommand("SJ_DIAG StartEx 22"); } break; 
			}
			AfxMessageBox(_T("PSD Error!!"), MB_OK|MB_ICONSTOP);
			
		}	break;
		
	case ERR_TIME:
		{
			switch(testJigDlg->virtual_console.getTimeOut())
			{
			case 0:	{	AfxMessageBox(_T("Time Over!! : An, boIR"), MB_OK|MB_ICONSTOP);			}	break;
			case 1:	{	AfxMessageBox(_T("Time Over!! : PE"), MB_OK|MB_ICONSTOP);				}	break;
			case 2:	{	AfxMessageBox(_T("Time Over!! : FrIr, boIr, bu"), MB_OK|MB_ICONSTOP);	}	break;
				
			default:
				{
					if (testJigDlg->enable_log_result)
					{
						printf(">>> Time Over!! : Unknown(%d)\n", testJigDlg->virtual_console.getFinishedID() );
					}
					
					AfxMessageBox(_T("Time Over!! : Unknown"), MB_OK|MB_ICONSTOP);
				}
				break;
			}
		}	break;
		
	case ERR_ANGLE_X:
		{
			std::stringstream ss;
			ss << "Angle Error: " << testJigDlg->virtual_console.ang_x << "\n\n다시 측정하시겠습니까?";
			int ret_val = AfxMessageBox(_T(ss.str().c_str()), MB_RETRYCANCEL|MB_ICONSTOP);
			
			switch( ret_val )
			{
			case IDCANCEL:	NoticeResult(ERR_ANGLE);			break;
			case IDRETRY:	NoticeResult(ACTION_GET_ANGLE_X);	break;
			}
			
		}	break;
		
	case ERR_ANGLE_Y:
		{
			std::stringstream ss;
			ss << "Angle Error: " << testJigDlg->virtual_console.ang_y << "\n\n다시 측정하시겠습니까?";
			int ret_val = AfxMessageBox(_T(ss.str().c_str()), MB_RETRYCANCEL|MB_ICONSTOP);
			
			switch( ret_val )
			{
			case IDCANCEL:	NoticeResult(ERR_ANGLE);			break;
			case IDRETRY:	NoticeResult(ACTION_GET_ANGLE_Y);	break;
			}
			
		}	break;
		
    case ERR_CAMERA_INFORMATION:
		{
			int ret_val = AfxMessageBox(_T("카메라 캘리브레이션 정보가 없습니다."), MB_OK|MB_ICONSTOP);
			NoticeResult(ACTION_QUIT_DIAG, false);
			
		} break;
		
    case ERR_CAMERA_CALIBRATION:
		{
			testJigDlg->waiting_time_for_gtest_ = 0;
			int ret_val = AfxMessageBox(_T("카메라 캘리브레이션에 문제가 있습니다."), MB_OK|MB_ICONSTOP);
			NoticeResult(ACTION_QUIT_DIAG, false);
			
		} break;
		
	default:	break;
	}
	
	//  ::LeaveCriticalSection(&crit);
}

//	===============================================================================================================
//	CALLBACK FUNCTION : NOTIFY RESULT for 3 TEST
//	===============================================================================================================

void NoticeResultEx(int list, int nRes)
{
	//	printf("++NoticeResultEx: %d, %d\n", list, nRes);
	HBITMAP hbit;
	
	switch(nRes)
	{
	case -1:	hbit = ::LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_BITMAP_FAIL));		break;
	case 0:		hbit = ::LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_BITMAP_3I_OFF));	break;
	case 1:		hbit = ::LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_BITMAP_3I_ON));		break;
	case 3:		hbit = ::LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_BITMAP_SUCCESS));	break;
		
	default:	break;
	}
	
	switch(list)
	{
	case ENUM_DIAG_DUST_SENSOR: 
		{
			switch(testJigDlg->virtual_console.getModelVersion())
			{
			case model_pluto:
				{
					testJigDlg->m_pic_dust_sensor.SetBitmap(hbit);
					switch ( nRes )
					{
						case -1:	testJigDlg->showPopUp("Dust Sensor 초기화 실패!!. 테스트를 재시작 해주세요");		break; 				
						case 0:		testJigDlg->showPopUp("Dust Sensor 초기화 중...");									break; 				
						case 1:		testJigDlg->showPopUp("Dust Sensor 초기화 성공!! 흡입구에 손가락을 넣어주세요.");	break; 				
						case 3:		testJigDlg->showPopUp("Dust Sensor 감지 완료.     다음 테스트 진행해주세요.");		break; 		
						default: break; 
					}	
				} break;
			}
		} break;

	case ENUM_DIAG_DCJACK:			testJigDlg->m_pic_dcjack.SetBitmap(hbit);	break;
	case ENUM_DIAG_DUST:				testJigDlg->m_pic_dust.SetBitmap(hbit);		break;
	case ENUM_DIAG_NOZZLE:
		{
			switch(testJigDlg->virtual_console.getModelVersion())
			{
			case model_pluto:
				{
					testJigDlg->m_pic_nozzle.SetBitmap(hbit);
				} break;
			}
		} break;
	case ENUM_DIAG_MOP:
		{
			switch(testJigDlg->virtual_console.getModelVersion())
			{
			case model_arte:
			case model_arte25:
			case model_arte30:
			case model_pop:
			case model_pop_lowcost:
			case model_pluto:
				{
					testJigDlg->m_pic_mop.SetBitmap(hbit);
				} break;
			}
		} break;
	}
}

void CTestJigDlg::OnDoubleclickedButtonHidden1() 
{
	// TODO: Add your control notification handler code here
	ActivateControlForUser(TRUE);	
}

void CTestJigDlg::enableLogging(bool value)
{
	//printf(">>> enableLogging(%d) \n", value);
	enable_log_result = value;
	virtual_console.enableLogging(value);
	gyro_tester.enableLogging(value); 
}

void CTestJigDlg::OnBtnInitializeSn() 
{
	int sn = 1;
	virtual_console.setSerialNumber(sn);
	
	std::stringstream ss;
	
	ss.clear();
	ss.str("");
	ss << sn;
	
	SetDlgItemText(IDC_EDIT_SERIAL_NUMBER, ss.str().c_str());
	
	WritePrivateProfileString( _T("testJig"), _T("sn")
		, ss.str().c_str(), ini_location.c_str());
}

void CTestJigDlg::OnSelchangeComboModel() 
{
	//printf(">>> combo box for model : %d \n", m_cbbox_model.GetCurSel());
	int model_value = m_cbbox_model.GetCurSel();
	setModelVersion(model_value);
}

void CTestJigDlg::setModelVersion(int & model_value)
{
	std::stringstream ss;
	std::string str_message;
	
	switch(model_value)
	{
	case model_arte:
		ss << "[ARTE] Final-test v.";
		str_message = "아르떼용 프로그램입니다!!";
		ini_location = ".\\diag_standard_arte.ini";
		break;
		
	case model_miele:
		ss << "[MIELE] Final-test v.";
		str_message = "밀레용 프로그램입니다!!";
		ini_location = ".\\diag_standard_miele.ini";
		break;
		
	case model_pop:
		ss << "[POP] Final-test v.";
		str_message = "팝용 프로그램입니다!!";
		ini_location = ".\\diag_standard_pop.ini";
		break;
	case model_pop_lowcost:
		ss << "[POP-Low cost] Final-test v.";
		str_message = "저가형 팝용 프로그램입니다!!";
		ini_location = ".\\diag_standard_pop_lowcost.ini";
		break;

	case model_arte25:
		ss << "[ARTE 2.5] Final-test v.";
		str_message = "아르떼 2.5용 프로그램입니다!!";
		ini_location = ".\\diag_standard_arte25.ini";
		break;

	case model_arte30:
		ss << "[ARTE 3.0] Final-test v.";
		str_message = "아르떼 3.0용 프로그램입니다!!";
		ini_location = ".\\diag_standard_arte30.ini";
		break;

	case model_pluto:
		ss << "[OMEGA] Final-test v.";
		str_message = "오메가용 프로그램입니다!!";
		ini_location = ".\\diag_standard_omega.ini";
		break;
	
	default:
		ss << "[TEST] Final-test v.";
		str_message = "This is for test!!";
		ini_location = ".\\diag_standard_test.ini";
		break;
		
	}
	
	virtual_console.setModelVersion(model_value);
	virtual_console.setIniLocation(ini_location);
	
	m_cbbox_model.SetCurSel(model_value);
	
	ss << app_version;
	SetWindowText(_T(ss.str().c_str()));
	
	NoticeResult(INFO_VERSION, true);
	
	AfxMessageBox(_T(str_message.c_str()), MB_OK|MB_ICONWARNING);
	
	Initialize();
}

