#ifndef DD_ROBHAZ_HPP_
#define DD_ROBHAZ_HPP_
#include <math.h>
#include <FSTREAM.H>
#include "JUART2.H"

#include "cv.h"
#include "cxcore.h"
#include "highgui.h"

#include "self_diag.hpp"
#include "sv113_rectify.hpp"
#include "image_util.hpp"
#include "common_util.hpp"

#define ARTE_VERSION  140

#define OPTX_MIN    120
#define OPTX_MAX    200
#define OPTY_MIN    80
#define OPTY_MAX    160

#define PLUTO2       1

//namespace ddConsole{

class dd_console_data
{
public:
	//	unsigned char data[4096];
	dd_console_data() {};
	void showme()
	{
	}
};

class dd_console : public JUART2<dd_console_data, CONSOLE_DATA_SIZE>
{
public:
	dd_console() : JUART2<dd_console_data,CONSOLE_DATA_SIZE>()
		, ang_x(0)
		, ang_y(0)
		, raw_img(0)
		, _idx(0)
		, _idx_img(0)
		, _jpg_size(0)
		, _finished_id(0)
		, _angle(0.0f)
		, _motion_id(0)
		, _motion_state(0)
		, _get_value_index(0) 
		, optX(320.0f-155.04f)
		, optY(240.0f-127.76f)
		, focal_len(234)
		, _boot(false)
		, _rcv_diff(false)
		, _start_test(false)
		, print_result_("")
		, _passed_info(false)
		, _check_model_sub(false)
		, gtest_only(false)
		, test_is_finished(false)
		, test_is_initialized(false)
		, got_finish_msg(false)
		, enable_log_result(false)
		, enable_log_console(false)
		, enable_log_debug(false)
		, model_sub(-1)
		, read_file_to_buffer(false)
		, read_file_to_buffer_for_run(false)
		, last_line("")
		, found_string_index_(0)
		, elapsed_time(0)
		, launched_time(0)
		, enable_elapsed_time(false)
	{
		should_i_clear_buffer_idx = false;
		last_data_incoming = 0;
		initializeBuffer();
		
		slog_result.Initialize(".\\log\\result_");
		slog_console.Initialize(".\\log\\console_");
		slog_debug.Initialize(".\\log\\debug_");
		slog_boot.Initialize(".\\log\\boot_");
		
		//    putData();
	};
	
	~dd_console()
	{
		if( raw_img )
		{
			cvReleaseImage( &raw_img );
		}
	}
	
	// ----------------------------------------------------------------------
	//  INITIALIZE
	// ----------------------------------------------------------------------
	
    void initialize(bool clear_buffer = true)
	{
		if (enable_log_console)
		{
			std::stringstream ss_log;
			ss_log << ">>> dd_console | initialize() | processing... \n";
			ss_log << ">>> dd_console | initialize() | inbuf = ";;
			ss_log << inbuf.size() << "\n";
			slog_debug(ss_log.str());
		}
		
#ifndef _TEST_
		if (!read_file_to_buffer)
		{
			//  buffer
			EC();
			if (clear_buffer)
			{
				inbuf.clear();  _idx=0;   should_i_clear_buffer_idx=true;
			}
			LC();
			clear_ib(); clear_ob();
		}
#endif
		
		//cb_notice_(ACTION_INIT_PROCESS, true);
		cb_notice_(ACTION_INIT_PIC, true);
		
		// class
		diag_val.initializeElements();
		//psd_check_state = ePsdCheckNone;
		psd_chk.initialize();
		
		// member variables
		model_sub = -1;
		
		_start_test           = true;
		_check_model_sub      = false;
		test_is_initialized   = false;
		test_is_finished      = false;
		got_finish_msg        = false;
		_passed_info          = false;
		
		is_getting_boot_log_finished = false;
		is_setting_current_time_done = false;
		
		setErrorFlag(0, true);
		
#ifndef _POP_
		set_time_flag = -1;
		set_time_diff = 0;
		
		for (int idx_a(0); idx_a<3; idx_a++)
		{
			for (int idx_b(0); idx_b<3; idx_b++)
			{
				set_time_[idx_a][idx_b] = -1;
			}
		}
		
		error_camera_calibration = false;
#endif
		
#ifndef _TEST_
		if (!read_file_to_buffer)
		{
			// buffer again.
			Sleep(1000);
			clear_ib(); clear_ob(); 
			EC();
			if (clear_buffer && !read_file_to_buffer)
			{
				inbuf.clear();  _idx=0;   should_i_clear_buffer_idx=true;
			}
			LC();
		}
#endif
		
		last_data_incoming = 0;
		
		if (enable_log_console)
		{
			std::stringstream ss_log;
			ss_log << ">>> dd_console | initialize() | done! \n";
			slog_debug(ss_log.str());
			//printf(">>> dd_console | initialize() | done! \n");
		}
		
		state_chk7ir = IR7_standby;
		
		launched_time = GetTickCount();
	}
	
	//#ifndef _POP_
	void initializeForArte()
	{
		for (int n(0); n<3; n++)
		{
			for (int nn(0); nn<3; nn++)
			{
				set_time_[n][nn] = 0;
			}
		}
		
		set_time_diff = 0;
		set_time_flag = -1;
	}
	//#endif
	
	//	===============================================================================================================
	//	RUN
	//	===============================================================================================================
	
	virtual int run()
	{
		//    static unsigned int tmp_spin = 0;
		//    static long tmp_lock = 0;
		//    static long tmp_recs = 0;
		//
		//    if (tmp_spin != cs.SpinCount || tmp_lock != cs.LockCount || tmp_recs != cs.RecursionCount)
		//    {
		//      tmp_spin = cs.SpinCount;
		//      tmp_lock = cs.LockCount;
		//      tmp_recs = cs.RecursionCount;
		//
		//      printf(">>> run | Spin(%3d), Lock(%3d), Recs(%3d) \n", tmp_spin, tmp_lock, tmp_recs);
		//    }
		
		if (read_file_to_buffer_for_run)
		{
			read_file_to_buffer_for_run = false;
		}
		else if( !read_data() )
		{
			return 0;
		}
		
		int index_count = 0;
		
		std::stringstream ss;
		ss.clear();
		ss.str("");
		
		if( last_data_incoming )
		{
			
			//if( GetTickCount() > last_data_incoming + 6000 )
			if( GetTickCount() > last_data_incoming + 12000 )
			{
				printf("last_data_incoming : %d, %d \n", last_data_incoming, GetTickCount() );
				// Initisialisation of buffer
				EC();
				_idx = 0;
				inbuf.clear();
				last_data_incoming = 0;
				LC();
				
				// Initisliation of re-transmission
				EC();
				cb_notice_(ACTION_DISABLE_RETRY, true);
				//initialize(false);
				LC();
			}
		}
		
		EC();
		if( should_i_clear_buffer_idx ) 
		{
			should_i_clear_buffer_idx = false;
			last_data_incoming = 0;
			_idx = 0;
		}
		LC();
		
		//    if (enable_log_debug)
		//    {
		//      ss << "\n>>> ================================= read_data ============================================\n";
		//    }
		
		//		for( std::deque<unsigned char>::iterator it = inbuf.begin();
		//			it!= inbuf.end();
		//			it++ )
		//			
		//    if( inbuf.size() > 100 )
		//    {
		//      printf("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n");
		//    }
		
		while( inbuf.size() )
		{
			unsigned char datum = inbuf[0];
			inbuf.pop_front();
			
			if (enable_log_debug)
			{
				index_count++;
				//printf("%c", *it);
				ss << datum;
				slog_debug(ss.str(), true);
				ss.clear();
				ss.str("");
			}
			
			//      if (datum == '/' && *(it+1) == ' ' && *(it+2) == '#')
			//      {
			//        if (enable_log_debug)
			//        {
			//          printf(">>> deleted '/ #' \n");
			//        }
			//        it += 3;
			//     
			
			switch (datum)
			{
			case 10:  //  line feed '\n'
				{
					_buffer[_idx++] = datum;
					//           if (enable_log_debug)
					//           {
					//             //printf("[ %5d | %5d ]\t", index_count, _idx);
					//             std::stringstream ss_index;
					//             ss_index << "[ " << index_count << " | " << _idx << " ] \t";
					//             slog_debug(ss_index.str(), true);
					//             ss_index.clear();
					//             ss_index.str("");
					//           }
					new_line();
					EC();
					last_data_incoming = 0;
					LC();
				} break;
				
				//       case 13:    //  carriage return '\r'
				//       case 32:    //  space
				//         {
				//           EC();
				//           _buffer[_idx++] = *it;
				//           last_data_incoming = GetTickCount();
				//           LC();
				//         } break;
				
			case 0:   //  NULL
				{
					EC();
					last_data_incoming = 0;
					LC();
				} break;
				
			default:
				{
					if (datum > 0x7F)
					{
						//             if (enable_log_debug)
						//             {
						//               std::stringstream ss_log;
						//               ss_log << ">>> got the BAD ascii code: \n";
						//               ss_log << datum << " <= char.\n";
						//               ss_log << static_cast<int>(datum) << " <= dec.\n";
						//               slog_debug(ss_log.str());
						//               //printf(">>> got the BAD ascii code: \n%c <= char.\n%d <= dec.\n", datum, datum);
						//             }
						
						EC();
						_idx = 0;
						inbuf.clear();
						last_data_incoming = 0;
						LC();
						return 0;
					}
					//           else if (*it < 32)
					//           {
					//             EC();
					//             if (enable_log_debug)
					//             {
					//               printf(":|%d|:", *it);
					//             }
					//             last_data_incoming = 0;
					//             LC();
					//           }
					else if (datum < '0')
					{
						EC();
						_buffer[_idx++] = datum;
						last_data_incoming = 0;
						LC();
					}
					else
					{
						EC();
						_buffer[_idx++] = datum;
						last_data_incoming = GetTickCount();
						LC();
					}
				} break;
			}
		}
		
		inbuf.clear();
		return 0;
	}
	
	//	===============================================================================================================
	//	NEW LINE
	//	===============================================================================================================
	
	void new_line()
	{
#ifndef _POP_
		static unsigned int tmp_time_r = 0;
		unsigned int tmp_time_diff = GetTickCount()-tmp_time_r;
		//    if (_idx>100)
		//    {
		//      printf("%10d >>> RCV %10d \t [%4d] (...big data...)\n", GetTickCount(), tmp_time_diff, _idx);
		//    }
		//    else
		//    {
		//      printf("%10d >>> RCV %10d \t [%4d] %s", GetTickCount(), tmp_time_diff, _idx, _buffer);
		//    }
		tmp_time_r = GetTickCount();
#endif
		
		//		_buffer[_idx+1] = '\n';
		int dummy_string(0);
		int first_bracket(0);
		
		std::stringstream ss;
		ss.clear();
		ss.str("");
		
#ifdef _SELF_DIAGNOSIS_
		
		// find the bracket [
		
		bool found_bracket = false;
		for( int i(0); i<_idx; i++ )
		{
			// for ignoring LF, CR in log file
			if ( _buffer[i] != '\r' || _buffer[i] != '\n' )
			{
				ss << _buffer[i];
			}
			
			if ( _buffer[i] == '[' )
			{
				dummy_string = i;
				if (!found_bracket)	
				{
					found_bracket = true;
					first_bracket = i;
					//printf("++(%02d) ", i);
				}
			}
		}
		//printf("%s\n", _buffer);
		
		if (enable_log_console && _idx<100)
		{
			printf("%s", ss.str().c_str());
		}
		
#ifndef _POP_
		if ( tmp_time_diff == 0 && last_line == ss.str() )
		{
			if (!enable_log_debug)
			{
				//printf(">>> This data seems to be duplicate.\n");
				slog_debug(">>> This data seems to be duplicate.\n");
				return;
			}
		}
		else
		{
			last_line = ss.str();
		}
#endif
		
		if (enable_log_console)
		{
			slog_console(ss.str());
		}
		
		//	------------------------------------------------------------------------------------
		//	test map data
		//	------------------------------------------------------------------------------------
		
		//    int case_number = string_data_.getData(ss.str());
		//
		//    switch(case_number)
		//    {
		//      case 1:
		//      case 2:
		//      case 3:
		//      case 4:
		//      case 5: printf(">>> %s, %d \n", ss.str().c_str(), case_number); break;
		//
		//      default: printf(">>> %s, no match \n", ss.str().c_str()); break;
		//    }
		
		int index_of_string = 0;
		
		//	------------------------------------------------------------------------------------
		//	Initialize
		//	------------------------------------------------------------------------------------
		
		if ( findString(ss.str(), "Linux version") != -1 )
		{
			if (enable_log_debug)
			{
				//printf(">>> init | Linux version\n");
				slog_debug(">>> init | Linux version\n");
			}
			initialize();
			test_is_initialized = true;
		}
		
		else if ( findString(ss.str(), "(gcc version") != -1 )
		{
			if (enable_log_debug)
			{
				//printf(">>> init | gcc version\n");
				slog_debug(">>> init | gcc version\n");
			}
			if (!test_is_initialized)
			{
				initialize();
				test_is_initialized = true;
			}
		}
		
		else if ( findString(ss.str(), "ARM926EJ-S") != -1 )
		{
			//if (enable_log_console)
			if (enable_log_debug)
			{
				//printf(">>> init | ARM926EJ-S\n");
				slog_debug(">>> init | ARM926EJ-S\n");
			}
			if (!test_is_initialized)
			{
				initialize();
				test_is_initialized = true;
			}
		}
		
		else if ( findString(ss.str(), "System initialization...") != -1 )
		{
			//if (enable_log_console)
			if (enable_log_debug)
			{
				//printf(">>> init | System initialization...\n");
				slog_debug(">>> init | System initialization...\n");
			}
			if (!test_is_initialized)
			{
				test_is_initialized = true;
				initialize();
			}
		}
		
		else if ( findString(ss.str(), "[MPTool:OS ] Res:Ok, Sys Init.") != -1 )
		{
			if (!test_is_initialized)
			{
				initialize();
			}
		}
		
		//
		
		else if ( findString(ss.str(), "temperatures_on_calibration = ", index_of_string) )
		{
			//is_getting_boot_log_finished = true;
			getNumberValue(index_of_string,diag_val.gyro_info[0]);
			printf(">>> got the temperature ==> %f\n", diag_val.gyro_info[0]);
		}
		else if ( findString(ss.str(), "estimated average scale factor is = ", index_of_string) )
		{
			//is_getting_boot_log_finished = true;
			getNumberValue(index_of_string,diag_val.gyro_info[1]);
			printf(">>> got the average scale factor ==> %f\n", diag_val.gyro_info[1]);
		}
		else if ( findString(ss.str(), "estimated update rate is          = ", index_of_string) )
		{
			//is_getting_boot_log_finished = true;
			getNumberValue(index_of_string,diag_val.gyro_info[2]);
			printf(">>> got the update rate ==> %f\n", diag_val.gyro_info[2]);
		}
		else if ( findString(ss.str(), "[-", index_of_string) )
		{
			std::stringstream ss_findstring;
			for (int n=10; n>=2; n--)
			{
				ss_findstring.clear();
				ss_findstring.str("");
				ss_findstring << "[-" << n << "0.0] : ";
				
				if ( findString(ss.str(), ss_findstring.str(), index_of_string) )
				{
					int index_of_value = 0;
					getNumberValue(index_of_string, diag_val.gyro_detail[10-n][0], index_of_value);
					index_of_value += 2;
					getNumberValue(index_of_value, diag_val.gyro_detail[10-n][1], index_of_value);
					index_of_value += 3;
					getNumberValue(index_of_value, diag_val.gyro_detail[10-n][2], index_of_value);
					
					//          printf(">>> %s ==> %f, %f, %f\n"
					//            , ss_findstring.str().c_str()
					//            , diag_val.gyro_detail[10-n][0]
					//            , diag_val.gyro_detail[10-n][1]
					//            , diag_val.gyro_detail[10-n][2]);
				}
			}
		}
		else if ( findString(ss.str(), "[+", index_of_string) )
		{
			std::stringstream ss_findstring;
			for (int n=2; n<=10; n++)
			{
				ss_findstring.clear();
				ss_findstring.str("");
				ss_findstring << "[+" << n << "0.0] : ";
				
				if ( findString(ss.str(), ss_findstring.str(), index_of_string) )
				{
					int index_of_value = 0;
					getNumberValue(index_of_string, diag_val.gyro_detail[n+7][0], index_of_value);
					index_of_value += 2;
					getNumberValue(index_of_value, diag_val.gyro_detail[n+7][1], index_of_value);
					index_of_value += 3;
					getNumberValue(index_of_value, diag_val.gyro_detail[n+7][2], index_of_value);
					
					//          printf(">>> %s ==> %f, %f, %f\n"
					//            , ss_findstring.str().c_str()
					//            , diag_val.gyro_detail[n+8][0]
					//            , diag_val.gyro_detail[n+8][1]
					//            , diag_val.gyro_detail[n+8][2]);
				}
			}
		}
		
		//	------------------------------------------------------------------------------------
		//	finish test
		//	------------------------------------------------------------------------------------
		
		else if (compareString(dummy_string, "[Self_Diag] finished."))
		{
			if (!test_is_finished)
			{
#ifndef _POP_
				printf( "Something wrong to robot. test failed. call test result checker\n" );
				cb_notice_( DIAG_RESULT, false );
#endif
				finish_test();
				//initialize();
			}
		}
		
		//	------------------------------------------------------------------------------------
		//	start test
		//	------------------------------------------------------------------------------------
		
		else if (findString(ss.str(), "[iCleboController] OK") != -1)
		{
			//printf( "dd_console, got the iCleboController log. start!\n" );
			//diag_val.initializeElements();
			
			if (!_start_test)	_start_test = true;
			_passed_info = false;
			
#ifdef _POP_
			initialize();
			cb_notice_(ACTION_SET_MODEL_SUB,true);
			cb_notice_(ACTION_DIAG, true);
#else
			initializeForArte();
#endif
		}
#ifndef _POP_
		else if (compareString(dummy_string, HEADER_GET_ANGX))
		{
			int header_size = charp_size(HEADER_GET_ANGX) + 1 + dummy_string;
			bool ret = getNumberValue( header_size, ang_x );
			
			if (diag_val._elemAngle.getMin() < ang_x && ang_x < diag_val._elemAngle.getMax())
			{
				cb_notice_(ACTION_GET_ANGLE_Y, true);
			}
			else
			{
				cb_notice_(ERR_ANGLE_X,true);
			}
		}
		else if (compareString(dummy_string, HEADER_GET_ANGY))
		{
			int header_size = charp_size(HEADER_GET_ANGY) + 1 + dummy_string;
			bool ret = getNumberValue( header_size, ang_y );
			
			if (diag_val._elemAngle.getMin() < ang_y && ang_y < diag_val._elemAngle.getMax())
			{
				cb_notice_(ACTION_DIAG, true);
			}
			else
			{
				cb_notice_(ERR_ANGLE_Y,true);
			}
		}
#endif
		
		//	------------------------------------------------------------------------------------
		//	compare main tag as "[MPTool:AppIni] Res:Ok"
		//	------------------------------------------------------------------------------------
		
		else if ( findString(ss.str(), "[MPTool:AppIni] Res:Ok") != -1 )
		{
			_callback_boot(ENUM_MAIN_MPTOOL_APPINI, 0, "Res:Ok", true);
			
		}
#ifndef _POP_
		else if ( findString(ss.str(), "we got the scale factors") != -1
			&& !is_getting_boot_log_finished )
		{
			//_callback_boot(ENUM_MAIN_MPTOOL_APPINI, 0, "Res:Ok", true);
			
			is_getting_boot_log_finished = true;
			
			cb_notice_(ACTION_ENTER_KEY, true);
			//      cb_notice_(ACTION_ENTER_KEY, true);
			
			//  test code
			//  --->
			//      _check_model_sub = false;
			// <---
			if (error_camera_calibration)
			{
				//error_flag_ |= 0x10;
				setErrorFlag(0x10);
				cb_notice_(ERR_CAMERA_INFORMATION, false);
			}
			else
			{
				if (_check_model_sub)
				{
					//printf(">>> _check_model_sub(true:%d) \n", model_sub);
					//          if (diag_val.modelNumberSub != model_sub)
					{
						cb_notice_(ACTION_SET_MODEL_SUB, true);
					}
					
					cb_notice_(INFO_MODEL_SUB, true);
				}
				else if (!_check_model_sub)
				{
					//printf(">>> _check_model_sub(false) \n");
					cb_notice_(ACTION_SET_MODEL_SUB, true);
					cb_notice_(INFO_MODEL_SUB, true);
				}
				//printf(">>> _check_model_sub(%d) \n", model_sub);
				
				cb_notice_(ACTION_SET_MODEL_MAIN,true);
				
#if ARTE_VERSION > 123
				cb_notice_(ACTION_SET_TIME, true);
#else
				cb_notice_(ACTION_DIAG, true);
#endif
			}
		}
#endif
		
		//	------------------------------------------------------------------------------------
		//	Test rebooting
		//	------------------------------------------------------------------------------------
		
		//    else if ( findString(ss.str(), "[Driver:CAM   ] release") != -1 )
		//    {
		//      std::string str_flashcp = "flashcp ./temp.ini /dev/mtd2\n";
		//      std::string str_iclebo = "./iclebo_s.out &\n";
		//
		//      write((char*)str_flashcp.c_str(),str_flashcp.length());
		//      Sleep(500);
		//
		//      write((char*)str_iclebo.c_str(),str_iclebo.length());
		//      Sleep(500);
		//    }
		//
		//    else if ( findString(ss.str(), "Let's go into self-diagnosis mode") != -1 )
		//    {
		//      std::string str_kill = "kill $(pidof 'iclebo_s.out')\n";
		//      write((char*)str_kill.c_str(), str_kill.length());
		//      Sleep(500);
		//    }
		
		//	------------------------------------------------------------------------------------
		//	Header
		//	------------------------------------------------------------------------------------
		
		else if (compareString(dummy_string,HEADER_MAIN_ANGLE))
		{
			int header_size = charp_size(HEADER_MAIN_ANGLE) + 1 + dummy_string;
			bool ret = getNumberValue(header_size,_angle);
			cb_notice_(INFO_ANGLE,true);
		}
		else if (compareString(dummy_string,HEADER_MAIN_MOTION))
		{
			int header_size = charp_size(HEADER_MAIN_MOTION) + 1 + dummy_string;
			bool ret_id = getNumberValue(header_size,_motion_id);
			bool ret_st = getNumberValue(_get_value_index+1,_motion_state);
			_get_value_index = 0;
			cb_notice_(INFO_MOTION,true);
		}
		
		//	------------------------------------------------------------------------------------
		//	Abort
		//	------------------------------------------------------------------------------------
		
		else if (compareString(0, "[Self_Diag] [Abort] ANGLE_ERROR"))
		{
			setErrorFlag(0x01);
			cb_notice_(ERR_ANGLE,true);
		}
		else if (compareString(0, "[Self_Diag] [Abort] TIME_OVER"))
		{
			setErrorFlag(0x08);
			cb_notice_(ERR_TIME,true);
		}
		else if (compareString(0, "[Self_Diag] [Abort] BUMPER_ERROR"))
		{
			setErrorFlag(0x02);
			cb_notice_(ERR_BUMPER,true);
		}
		//else if (compareString(dummy_string, HEADER_GET_BATTERY))
		else if (findString(ss.str(), HEADER_GET_BATTERY) != -1
			&& !diag_val.isSetElemValue(ENUM_DIAG_CHARGING) )
		{
			//int header_size = charp_size(HEADER_GET_BATTERY) + 1 + dummy_string;
			int header_size
				= findString(ss.str(), HEADER_GET_BATTERY)
				+ charp_size(HEADER_GET_BATTERY) + 1;
			
			getNumberValue( header_size, diag_val.battery[0] );
			cb_notice_(INFO_GOT_BATTERY,true);
		} 

		else if ( compareString(0, HEADER_GET_DCJACK) )
		{
			printf( "==== Get DC-jack connect log from robot\n" );
			printf( "==== Shall we start charging test using DC-jack????\n" );
		}
		
		//	------------------------------------------------------------------------------------
		//	  Serial number
		//	------------------------------------------------------------------------------------
		
		//		else if (compareString(dummy_string, HEADER_GET_ID))
		//		{
		//			int header_size = charp_size(HEADER_GET_ID) + 1 + dummy_string;
		//			getNumberValue( header_size, diag_val.cpu_id );
		//			//printf("++got cpuid: %d", diag_val.cpu_id);
		//			cb_notice_(INFO_GOT_CPUID,true);
		//		}
		
		//	------------------------------------------------------------------------------------
		//	Test mode
		//	------------------------------------------------------------------------------------
		
		else if ( findString(ss.str(), "test mode here (7)") != -1 )
		{
			got_finish_msg = true;
		}
		else if (compareString(0, "We will start the Sungjin's test mode here ("))
		{
			//writeResult();
			if (!_start_test)	_start_test = true;
		}
		else if (compareString(0, "We will start the Sungjin's test mode here"))
		{
			//diag_val.initializeElements();
			_passed_info = false;
		}
		
		//	------------------------------------------------------------------------------------
		//	model_number_sub ; "iCleboParam: [product] [model_sub] %d"
		//	------------------------------------------------------------------------------------
		
		//		else if ( compareString( first_bracket, "[product]" ) )
		//		{
		//			int header_temp = first_bracket + charp_size( "[product]" );
		//
		//			if ( compareString( header_temp + 1, "[model_sub]" ) )
		//			{
		//				header_temp += 2 + charp_size( "[model_sub]" );
		//
		//				model_sub = get_value(header_temp);
		//
		//				_check_model_sub = true;
		//			}
		//		}
		//
		else if ( compareString( first_bracket, CHECK_SUB_MODEL ) )
		{
			int header_temp = first_bracket + charp_size( CHECK_SUB_MODEL );
			getNumberValue(header_temp,model_sub);
			//printf(">>> model_sub(%d) \n", model_sub);
			_check_model_sub = true;
			//			
			//			if (diag_val.modelNumberSub != model_sub)
			//			{
			//				cb_notice_(ACTION_SET_MODEL_SUB, true);
			//			}
		}
		
		//	------------------------------------------------------------------------------------
		//	compare main tag as "[INI Reader   ] /dev/mtd3: enable = "
		//	------------------------------------------------------------------------------------
		
		else if ( compareString( first_bracket, "[INI Reader   ] /dev/mtd3: enable = " ) )
		{
			int header_temp = first_bracket + charp_size( "[INI Reader   ] /dev/mtd3: enable = " );
			int value_temp(0);
			getNumberValue(header_temp, value_temp);
			
			if (value_temp != 0)
			{
				cb_notice_(ACTION_SET_RESERV_DISABLE, true);
			}
		}
		
		//	------------------------------------------------------------------------------------
		//	compare main tag as "[INI Reader   ] /root/mtd2.ini: [psd] right_adjust ="
		//	------------------------------------------------------------------------------------
		
		else if ( findString( ss.str(), CHECK_RIGHT_PSD ) != -1 )
			//      && !diag_val.isSetElemValue(ENUM_DIAG_PSD_RIGHT) )
		{
			int header_temp = findString( ss.str(), CHECK_RIGHT_PSD ) + charp_size( CHECK_RIGHT_PSD );
			int value_temp(999);
			bool res = false;
			getNumberValue(header_temp, value_temp);
			
			int id = ENUM_DIAG_PSD_RIGHT;
			diag_val.setElemValue(id, value_temp);
			if( diag_val.getElemValue(id) > diag_val.getElemMin(id)
				&& diag_val.getElemValue(id) < diag_val.getElemMax(id) )
			{
				res = true;
			} else { res = false; }
			//diag_val._result[id] = res;
			diag_val.setElemResult(id, res);
			cb_notice_(id, res);
		}
		else if ( findString( ss.str(), CHECK_CENTER_PSD ) != -1 )
			//      && !diag_val.isSetElemValue(ENUM_DIAG_PSD_CENTER) )
		{
			int header_temp = findString( ss.str(), CHECK_CENTER_PSD ) + charp_size( CHECK_CENTER_PSD );
			int value_temp(999);
			bool res = false;
			getNumberValue(header_temp, value_temp);
			
			int id = ENUM_DIAG_PSD_CENTER;
			diag_val.setElemValue(id, value_temp);
			if( diag_val.getElemValue(id) > diag_val.getElemMin(id)
				&& diag_val.getElemValue(id) < diag_val.getElemMax(id) )
			{
				res = true;
			} else { res = false; }
			//diag_val._result[id] = res;
			diag_val.setElemResult(id, res);
			cb_notice_(id, res);
		}
		else if ( findString( ss.str(), CHECK_LEFT_PSD ) != -1 )
			//      && !diag_val.isSetElemValue(ENUM_DIAG_PSD_LEFT) )
		{
	    	int header_temp = findString( ss.str(), CHECK_LEFT_PSD ) + charp_size( CHECK_LEFT_PSD );
			int value_temp(999);
			bool res = false;
			getNumberValue(header_temp, value_temp);

			int id = ENUM_DIAG_PSD_LEFT;
			diag_val.setElemValue(id, value_temp);
			if( diag_val.getElemValue(id) > diag_val.getElemMin(id)
				&& diag_val.getElemValue(id) < diag_val.getElemMax(id) )
			{
				res = true;
			} else { res = false; }
		    //diag_val._result[id] = res;
		    diag_val.setElemResult(id, res);
  		    cb_notice_(id, res);
		}
/*
		else if ( findString( ss.str(), CHECK_RIGHT_TOP_PSD ) != -1 )
			//      && !diag_val.isSetElemValue(ENUM_DIAG_PSD_LEFT) )
		{
	    	int header_temp = findString( ss.str(), CHECK_RIGHT_TOP_PSD ) + charp_size( CHECK_RIGHT_TOP_PSD );
			int value_temp(999);
			bool res = false;
			getNumberValue(header_temp, value_temp);

			int id = ENUM_DIAG_PSD_RIGHT_TOP;
			diag_val.setElemValue(id, value_temp);
			if( diag_val.getElemValue(id) > diag_val.getElemMin(id)
				&& diag_val.getElemValue(id) < diag_val.getElemMax(id) )
			{
				res = true;
			} else { res = false; }
		    //diag_val._result[id] = res;
		    diag_val.setElemResult(id, res);
  		    cb_notice_(id, res);
		}
		else if ( findString( ss.str(), CHECK_CENTER_TOP_PSD ) != -1 )
			//      && !diag_val.isSetElemValue(ENUM_DIAG_PSD_LEFT) )
		{
	    	int header_temp = findString( ss.str(), CHECK_CENTER_TOP_PSD ) + charp_size( CHECK_CENTER_TOP_PSD );
			int value_temp(999);
			bool res = false;
			getNumberValue(header_temp, value_temp);

			int id = ENUM_DIAG_PSD_CENTER_TOP;
			diag_val.setElemValue(id, value_temp);
			if( diag_val.getElemValue(id) > diag_val.getElemMin(id)
				&& diag_val.getElemValue(id) < diag_val.getElemMax(id) )
			{
				res = true;
			} else { res = false; }
		    //diag_val._result[id] = res;
		    diag_val.setElemResult(id, res);
  		    cb_notice_(id, res);
		}
		else if ( findString( ss.str(), CHECK_LEFT_TOP_PSD ) != -1 )
			//      && !diag_val.isSetElemValue(ENUM_DIAG_PSD_LEFT) )
		{
	    	int header_temp = findString( ss.str(), CHECK_LEFT_TOP_PSD ) + charp_size( CHECK_LEFT_TOP_PSD );
			int value_temp(999);
			bool res = false;
			getNumberValue(header_temp, value_temp);

			int id = ENUM_DIAG_PSD_LEFT_TOP;
			diag_val.setElemValue(id, value_temp);
			if( diag_val.getElemValue(id) > diag_val.getElemMin(id)
				&& diag_val.getElemValue(id) < diag_val.getElemMax(id) )
			{
				res = true;
			} else { res = false; }
		    //diag_val._result[id] = res;
		    diag_val.setElemResult(id, res);
  		    cb_notice_(id, res);
		}
*/
		
		//	------------------------------------------------------------------------------------
		//	compare main tag as "[getPk3] ?, ?, ....."
		//	------------------------------------------------------------------------------------
		
		else if ( findString( ss.str(), "[getPk3]" ) != -1
			&& !diag_val.isSetElemValue(ENUM_DIAG_CHARGER_ON) )
		{
			int header_temp
				= findString( ss.str(), "[getPk3]" ) + charp_size("[getPk3]") + 1;
			
			int value_temp(0);
			getNumberValue(header_temp, value_temp);
			
			//if (enable_log_console)
			if (enable_log_debug)
			{
				//printf(">>> GOT [getPk3] %d\n", value_temp);
				std::stringstream ss_log;
				ss_log << ">>> GOT [getPk3] ";
				ss_log << value_temp << "\n";
				slog_debug(ss_log.str());
			}
			
			int tmp_charger = diag_val.getElemValue(ENUM_DIAG_CHARGER_ON);
			
			bool set_charger_signal
				= (value_temp && !tmp_charger) ? true : false;
			
			if (!set_charger_signal)
			{
				if (enable_log_console)
				{
					printf( ">>> charger signal is got already. (current:%d) \n"
						, diag_val.getElemValue(ENUM_DIAG_CHARGER_ON) );
				}
			}
			else
			{
				diag_val.setElemValue(ENUM_DIAG_CHARGER_ON, value_temp);
				
				switch(diag_val.getElemValue(ENUM_DIAG_CHARGER_ON))
				{
				case 6:  {
					diag_val.setElemResult(ENUM_DIAG_CHARGER_ON, true);
					diag_val.setElemResult(ENUM_DIAG_DCJACK_SIGNAL, true);
						 } break;
					
				case 22: {
					if ( model_version != model_pop_lowcost )
					{
						diag_val.setElemResult(ENUM_DIAG_CHARGER_ON, true);
						diag_val.setElemResult(ENUM_DIAG_DCJACK_SIGNAL, false);
					}
					else
					{
						//diag_val.setElemResult(ENUM_DIAG_CHARGER_ON, true);
						diag_val.setElemResult(ENUM_DIAG_DCJACK_SIGNAL, true);
					}
						 } break;
					
				default: {
					diag_val.setElemResult(ENUM_DIAG_CHARGER_ON, false);
					diag_val.setElemResult(ENUM_DIAG_DCJACK_SIGNAL, false);
						 } break;
				}
				
				//if (diag_val._result[ENUM_DIAG_CHARGER_OFF])
				if (diag_val.getElemResult(ENUM_DIAG_CHARGER_OFF))
				{
					cb_notice_(
						ENUM_DIAG_CHARGER_ON,
						//diag_val._result[ENUM_DIAG_CHARGER_ON]);
						diag_val.getElemResult(ENUM_DIAG_CHARGER_ON));
				}
				
				cb_notice_(
					ENUM_DIAG_DCJACK_SIGNAL, 
					//diag_val._result[ENUM_DIAG_DCJACK_SIGNAL]);
					diag_val.getElemResult(ENUM_DIAG_DCJACK_SIGNAL));
			}
		}
		
		//	------------------------------------------------------------------------------------
		//	compare main tag as "packet lost recovery is fail"
		//	------------------------------------------------------------------------------------
		
		//    else if ( findString( ss.str(), "packet lost recovery is fail" ) != -1 )
		//    {
		//      cb_notice_(ACTION_GET_IMAGE, true);
		//    }
		
		//	------------------------------------------------------------------------------------
		//	compare main tag as "[Check_Cliff] %d"
		//	------------------------------------------------------------------------------------
		
		else if ( compareString( first_bracket, "[Check_Cliff]" ) )
		{
			int header_temp = first_bracket + charp_size( "[Check_Cliff] " );
			int value_temp(0);
			
			getNumberValue(header_temp,value_temp);
		}
		
		//	------------------------------------------------------------------------------------
		//	compare main tag as "[Check_Psd] %d %d %d"  For Pluto, "[Check_Psd] %d %d %d %d %d %d"
		//	------------------------------------------------------------------------------------
		
		else if ( compareString( first_bracket, "[Check_Psd]" )
			&& psd_chk.getCheckState() != ePsdCheckEnd )
		{
			//printf(">>> got check_psd...\n");
			int header_temp = first_bracket + charp_size( "[Check_Psd] " );
			
			//      int tmp_psd[3] = {0,};
			int psd_l, psd_c, psd_r;
			int psd_l_t, psd_c_t, psd_r_t; 
			psd_l = psd_c = psd_r = 0;
			psd_l_t = psd_c_t = psd_r_t = 0;
			//      tmp_psd[0] = abs( get_value(header_temp) );
			//      tmp_psd[1] = abs( get_value(_get_value_index+1) );
			//      tmp_psd[2] = abs( get_value(_get_value_index+1) );
			
			if ( getModelVersion() == model_pluto )
			{
			  getNumberValue(header_temp,psd_r);
			  getNumberValue(_get_value_index+1,psd_c);
			  getNumberValue(_get_value_index+1,psd_l);
			  getNumberValue(_get_value_index+1,psd_r_t);
			  getNumberValue(_get_value_index+1,psd_c_t); 
			  getNumberValue(_get_value_index+1,psd_l_t);

		      switch ( psd_chk.setPsdValuePluto(psd_r,psd_c,psd_l,psd_l_t, psd_c_t, psd_r_t) )
			  {
			    case 1:   cb_notice_( INFO_PSD_CHECK_NEXT, true ); break;
			    case -1:  cb_notice_( INFO_PSD_CHECK_NEXT, false ); break;
			    default:  break;
			  }
			}
			else
			{
			  getNumberValue(header_temp,psd_r);
			  getNumberValue(_get_value_index+1,psd_c);
			  getNumberValue(_get_value_index+1,psd_l);
			  //      getNumberValue(header_temp,tmp_psd[0]);
			  //      getNumberValue(_get_value_index+1,tmp_psd[1]);
			  //      getNumberValue(_get_value_index+1,tmp_psd[2]);

		      switch ( psd_chk.setPsdValue(psd_r,psd_c,psd_l) )
			  //      switch ( psd_chk.setPsdValue(tmp_psd[0],tmp_psd[1],tmp_psd[2]) )
			  {
			    case 1:   cb_notice_( INFO_PSD_CHECK_NEXT, true ); break;
			    case -1:  cb_notice_( INFO_PSD_CHECK_NEXT, false ); break;
			    default:  break;
			  }
			}
		}
		
#if ARTE_VERSION > 123
		
		//	------------------------------------------------------------------------------------
		//	compare main tag as "[Self_Diag] [VER__PCB] %c%03d\n"
		//	------------------------------------------------------------------------------------
		
		else if ( findString(ss.str(), "[Self_Diag] [VER__PCB] ") != -1 )
		{
			// ...n
			int header_length = found_string_index_ + charp_size( "[Self_Diag] [VER__PCB] " );
			
			if (!diag_val.got_pcb_version.empty())
			{
				//printf(">>> This pcb version is got already.\n");
				diag_val.got_pcb_version = "";
			}
			
			getStringValue(header_length,diag_val.got_pcb_version);
			
			bool res = false;
			if (diag_val.got_pcb_version.compare(diag_val.pcb_version_) == 0)
			{
				//printf(">>> passed \n");
				res = true;
			}
			
			//diag_val._result[ENUM_DIAG_VERSION_PCB] = res;
			diag_val.setElemResult(ENUM_DIAG_VERSION_PCB, res);
			cb_notice_(ENUM_DIAG_VERSION_PCB, res);
			
			//      printf(">>> got a pcb version...header(%d), string(%s), result(%d) \n"
			//        , header_length, diag_val.got_pcb_version.c_str(), res);
		}
#endif
		
#ifndef _POP_
		
#if ARTE_VERSION > 123
		
		//	------------------------------------------------------------------------------------
		//	compare main tag as "[SetTime] %02d:%02d:02%d"
		//	------------------------------------------------------------------------------------
		
		else if ( findString(ss.str(), "[SetTime]") != -1
			&& !is_setting_current_time_done )
		{
			is_setting_current_time_done = true;
			cb_notice_(ACTION_ENTER_KEY, true);
			cb_notice_(ACTION_DIAG, true);
		}
		
		//	------------------------------------------------------------------------------------
		//	compare main tag as "[GetTime] %02d:%02d:02%d"
		//	------------------------------------------------------------------------------------
		
		else if( set_time_flag >= 0 
			&& set_time_flag < 2 
			&& findString(ss.str(), "[GetTime]") != -1 )
		{
			//int header_temp = first_bracket + charp_size( "[GetTime] " );
			int header_temp = findString(ss.str(), "[GetTime]") + charp_size( "[GetTime] " );
			
			std::stringstream ss_time;
			
			for (int n(0); n<3; n++)
			{
				ss_time.clear();  ss_time.str("");
				ss_time << _buffer[header_temp] << _buffer[header_temp+1];
				
				//if (enable_log_console)
				if (enable_log_debug)
				{
					//printf(">>> got_time[%d] => %s \n", n, ss_time.str().c_str());
					std::stringstream ss_log;
					ss_log << ">>> got_time[" << n << "] => ";
					ss_log << ss_time.str() << "\n";
				}
				
				set_time_[set_time_flag][n] = atoi(ss_time.str().c_str());
				
				header_temp += 3;
			}
			
			if (enable_log_console)
			{
				//        printf( "got time => %d %d %d \n"
				//          , set_time_[set_time_flag][0]
				//          , set_time_[set_time_flag][1]
				//          , set_time_[set_time_flag][2] );
				std::stringstream ss_log;
				ss_log << ">>> got time : " << set_time_[set_time_flag][0];
				ss_log << " " << set_time_[set_time_flag][1];
				ss_log << " " << set_time_[set_time_flag][2] << "\n";
				slog_console(ss_log.str());
			}
			
			switch(set_time_flag)
			{
			case 0:
				{
					//set_time_diff = GetTickCount();
					//set_time_flag = 1;
					
				} break;
				
			case 1:
				{
					int time_to_sec[3] = {0,};
					for (int x(0); x<3; x++)
					{
						time_to_sec[x] = set_time_[x][0] * 60;
						time_to_sec[x] = (time_to_sec[x] + set_time_[x][1]) * 60;
						time_to_sec[x] = time_to_sec[x] + set_time_[x][2];
					}
					
					int set_time_second = time_to_sec[2] = time_to_sec[1] - time_to_sec[0];          
					
					set_time_[2][0] = time_to_sec[2] / 3600;  time_to_sec[2] %= 3600;
					set_time_[2][1] = time_to_sec[2] / 60;    time_to_sec[2] %= 60;
					set_time_[2][2] = time_to_sec[2];
					
					unsigned int time_current = GetTickCount();
					int time_diff = time_current - set_time_diff;
					time_diff /= 1000;
					
					int comp_diff = time_diff - set_time_second;
					diag_val.setElemValue(ENUM_DIAG_RTC, comp_diff);
					bool res = false;
					
					int default_value = diag_val.getElemDefault(ENUM_DIAG_RTC);
					
					if ( -default_value < comp_diff && comp_diff < default_value )
					{ 
						res = true;
					}
					
					if (enable_log_debug)
					{
						//            printf( ">>> got_time   : %02d:%02d:%02d - %02d:%02d:%02d = %02d:%02d:%02d \n"
						//              , set_time_[1][0], set_time_[1][1], set_time_[1][2]
						//              , set_time_[0][0], set_time_[0][1], set_time_[0][2]
						//              , set_time_[2][0], set_time_[2][1], set_time_[2][2] );
						
						std::stringstream ss_log;
						ss_log << ">>> got time : " << std::setfill('0') << std::setw(2) << set_time_[1][0];
						ss_log << ":" << std::setfill('0') << std::setw(2) << set_time_[1][1];
						ss_log << ":" << std::setfill('0') << std::setw(2) << set_time_[1][2];
						
						ss_log << " - " << std::setfill('0') << std::setw(2) << set_time_[0][0];
						ss_log << ":" << std::setfill('0') << std::setw(2) << set_time_[0][1];
						ss_log << ":" << std::setfill('0') << std::setw(2) << set_time_[0][2];
						
						ss_log << " = " << std::setfill('0') << std::setw(2) << set_time_[2][0];
						ss_log << ":" << std::setfill('0') << std::setw(2) << set_time_[2][1];
						ss_log << ":" << std::setfill('0') << std::setw(2) << set_time_[2][2];
						
						ss_log << "\n>>> tick_count : " << time_current;
						ss_log << " - " << set_time_diff << " = " << time_diff << "\n";
						
						ss_log << ">>> compare    : (tick)" << time_diff;
						ss_log << " - (got)" << set_time_second << " = ";
						ss_log << comp_diff << " ) => " << res << "\n";
						
						slog_debug(ss_log.str());
						
						//printf( ">>> tick_count : %d - %d = %d \n", time_current, set_time_diff, time_diff );
						//printf( ">>> compare    : (tick)%d - (got)%d = %d ) => %d \n", time_diff, set_time_second, comp_diff, res );
					}
					
					//diag_val._result[ENUM_DIAG_RTC] = res;
					diag_val.setElemResult(ENUM_DIAG_RTC, res);
					cb_notice_(ENUM_DIAG_RTC, res);
					
					set_time_flag = 2;
					set_time_diff = 0;
					
				} break;
			}
			
    }
	
#endif
	
    //	------------------------------------------------------------------------------------
    //	Getting Image Data
    //	------------------------------------------------------------------------------------
    
    else if (compareString(dummy_string,JPEG_DATA))
    {
		decodeImage( charp_size(JPEG_DATA), _buffer, _jpg_size );
		if (_boot)
		{
			if (_rcv_diff)	cb_notice_(ACTION_CAMERA, true);
			//else			cb_notice_(ACTION_DIAG, true);
		}
    }
    else if (compareString(dummy_string,JPEG_SIZE))
    {
		char c_size[6] = {0};
		int index = 0;
		int pre_size = charp_size(JPEG_SIZE)+dummy_string;
		while(_buffer[pre_size + index] != '\n')
		{
			c_size[index] = _buffer[pre_size + index];
			index++;
		}
		_jpg_size = atoi(c_size);
    }
    
    else if ( findString(ss.str(), "[FINISHED]") != -1 )
    {
		diag_val.setElemValue(ENUM_DIAG_GTEST_LEFT, 15);
		diag_val.setElemValue(ENUM_DIAG_GTEST_RIGHT, -10);
		diag_val.setElemValue(ENUM_DIAG_GTEST_ORIGIN, 5);
		
		diag_val.setElemResult(ENUM_DIAG_GTEST_ORIGIN, true);
		diag_val.setElemResult(ENUM_DIAG_GTEST_LEFT, true);
		diag_val.setElemResult(ENUM_DIAG_GTEST_RIGHT, true);
		//      diag_val._result[ENUM_DIAG_GTEST_ORIGIN] = true;
		//      diag_val._result[ENUM_DIAG_GTEST_LEFT] = true;
		//      diag_val._result[ENUM_DIAG_GTEST_RIGHT] = true;
		
		cb_notice_(DIAG_GTEST, true);
		//cb_notice_(DIAG_RESULT, true);
		cb_notice_(ACTION_TEST_DIAG, true);      
    }
	
#endif
	
	//	------------------------------------------------------------------------------------
	//	compare main tag as "[INI Reader   ]", "[Self_Diag]"
	//	------------------------------------------------------------------------------------
	
	else
	//else if ( findString(ss.str(), "Self_Diag") != -1 ) //20160329::taegooKim To avoid from the situation when log message is broken during the test
	{
		bool found = false;
		
#ifndef _POP_
		int end_of_index = (is_getting_boot_log_finished) ? 1 : ENUM_MAIN_TAG_LENGTH;
		
		for ( int idx(0); idx < end_of_index; idx++ )
#else
			int idx = ENUM_MAIN_DIAG;
#endif
		{
			if (!found)
			{
				std::string   strMainTag = diag_val.getMainTag(idx);
				//bool		      resMainTag = compareString( first_bracket, strMainTag );
				bool		      resMainTag = findString( ss.str(), strMainTag ) != -1 ? true : false;
				
				if ( resMainTag )
				{
					int prefix = strMainTag.length() + 1;
					
					switch(idx)
					{
						// -------------------------------------------------------
						//  for DIAGNOSIS : [Self_Diag] [...]
						// -------------------------------------------------------
						
					case ENUM_MAIN_DIAG:
						{
							for(int n = 0; n < ENUM_DIAG_LENGTH; n++)
							{
								if (!found)
								{
									switch(idx)
									{
#if ARTE_VERSION > 123
									case ENUM_DIAG_VERSION_PCB:
										break;
#endif
										
									case -1:
										break;
										
									default:
										//found = compareValue(n, first_bracket);
										found = compareValue(n, found_string_index_);
										//printf("found: %d, n: %d, found_string_index_: %d\n", found, n, found_string_index_); 
										break;
									}
								}
								else
								{
									n = ENUM_DIAG_LENGTH;
								}
							}
						}	break;
						
						// -------------------------------------------------------
						
					case ENUM_MAIN_INIREADER:
						{
							std::string str_2nd_cam(BOOT_LOG_CAMERA);
							std::string str_2nd_opt(BOOT_LOG_CAMERA_MTD2);
							bool res_2nd_cam = compareString( prefix, BOOT_LOG_CAMERA );
							bool res_2nd_opt = compareString( prefix, BOOT_LOG_CAMERA_MTD2 );
							
							if( res_2nd_cam || res_2nd_opt )
							{
								//prefix += str_2nd_cam.length() + 1;
								prefix += res_2nd_opt ? str_2nd_opt.length() + 1 : str_2nd_cam.length() + 1;
								
								for(int n = 1; n < ENUM_SUB_CAMERA_LENGTH; n++)
								{
									if (!diag_val.boot_camera_[n].isSet())
									{
										std::string str_camera = diag_val.getSubTagAboutCamera(n);
										found = compareString(prefix, str_camera);
										if (found)
										{
											//	[INI Reader   ] ./camera.ini: optX = none
											//	|<--     prefix      -->|
											//	prefix + "optX" + " = "
											//	get "none"
											prefix += str_camera.length() + 3;
											float read_value = 0.0f;
											bool ret = getNumberValue(prefix,read_value);
											diag_val.boot_camera_[n].setValue(read_value);
											
											std::stringstream ss_log;
											ss_log << ">>> " << ret << " = getNumberValue(" << read_value << ") " << "\n";
											slog_debug(ss_log.str());
											
											if (!ret)
											{
												_callback_boot(idx, n, "none", ret);
#ifndef _POP_
												switch(n)
												{
												case ENUM_SUB_CAMERA_OPTX:
												case ENUM_SUB_CAMERA_OPTY:
													error_camera_calibration = true;
													break;
												}
#endif
											}
											else
											{
												std::stringstream strm;
												strm << diag_val.boot_camera_[n].getValue();
												_callback_boot(idx, n, strm.str(), ret);
											}
											n = ENUM_SUB_CAMERA_LENGTH;
										}
									}
								}
							}								
						}	break;
						
					case ENUM_MAIN_CSV113GRABBER:
						{
							std::string	str2ndTag("Device open success");
							found = compareString( prefix, str2ndTag );
							
							if( found )
							{
								diag_val.boot_csv113 = true;
								_callback_boot(idx, 0, "Success", true);
							}
							else	_callback_boot(idx, 0, "Fail", false);
							
						}	break;
						
					case ENUM_MAIN_INTEGRATOR:
						{
							std::string	str2ndTag("Gyro Type = ");
							found = compareString( prefix, str2ndTag );
							
							if( found )
							{
								prefix += str2ndTag.length();
								getNumberValue(prefix,diag_val.boot_gyro_type);
								
								std::stringstream strm;
								strm << diag_val.boot_gyro_type;
								_callback_boot(idx, 0, strm.str(), true);
							}
							
						}	break;
						
					case ENUM_MAIN_MPTOOL_APP:
					case ENUM_MAIN_MPTOOL_APPLIB:
					case ENUM_MAIN_MPTOOL_APPINI:
						{
							for(int n = 0; n < ENUM_SUB_MPTOOL_LENGTH; n++)
							{
								std::string	str_mptool = diag_val.getSubTagAboutMptool(n);
								found = compareString( prefix + dummy_string, str_mptool );
								
								if( found )
								{
									prefix += str_mptool.length();
									prefix += dummy_string;
									//diag_val.boot_mptool[n] = getStringValue(prefix);
									getStringValue( prefix, diag_val.boot_mptool[n] );
									_callback_boot(idx, n, diag_val.boot_mptool[n], true);
								}
							}
							
						}	break;
						
					case ENUM_MAIN_CSERIALLINUX:	{}	break;
						
					default:					{}	break;
						}
					}
					
				}	//	if (!found)
				else					idx = ENUM_MAIN_TAG_LENGTH;
			}
		}
		
#else
		
		for( int i(0); i<_idx; i++ )
		{
			if ( _buffer[i] != '\r' || _buffer[i] != '\n' )
				ss << _buffer[i];
			
			if ( _buffer[i] == '[' )
			{
				dummy_string = i;
			}
		}
		
		slog(ss.str());
		//		printf("%s\n", ss.str().c_str());
		
#endif
		
		//memset(_buffer, 0, _idx+1);
		memset(_buffer, 0, BUFFER_SIZE);
		//_buffer[_idx+1] = '\n';
		_idx = 0;   
	}
	
	//	===============================================================================================================
	//	COMPARE
	//	===============================================================================================================
	
	bool compareValue(int diag_id, int start_bracket)
	{
		std::string sub = diag_val.getElemSub(diag_id);
		int index = charp_size(DIAG_TAG) + 1 + start_bracket;
		bool res = false;
		bool display = true;
		
		if (compareString(index, sub))
			//    if (compareString(index, sub) && !diag_val.isSetElemValue(diag_id))
		{
			_finished_id = diag_id;
			
			index += sub.length() + 1;
			int diag_value;
			getNumberValue(index, diag_value);
			if (!diag_val.isSetElemValue(diag_id))
			{
				diag_val.setElemValue(diag_id, diag_value);
			}
			else
			{
				switch(diag_id)
				{
				case ENUM_DIAG_DUST_SENSOR:
				case ENUM_DIAG_DUST:
				case ENUM_DIAG_NOZZLE:
				case ENUM_DIAG_DCJACK:
				case ENUM_DIAG_MOP:
					break;
					
				default: return false;
					break;
				}
			}
			
			//printf(">>> [%d] %d\n", diag_id, diag_value);
			
			//	test code
			//			val = diag_val.getElemValue(diag_id);
			//			int val_min = diag_val.getElemMin(diag_id);
			//			int val_max = diag_val.getElemMax(diag_id);
			//			int val_default = diag_val.getElemDefault(diag_id);
			//			printf("++compare_value: val:%d | min:%d | max:%d | def:%d\n", val, val_min, val_max, val_default);
			
			switch(diag_id)
			{
			case ENUM_DIAG_VERSION:
				
#ifndef _POP_
	#if ARTE_VERSION > 123
				if (set_time_flag == -1 )
				{
					set_time_flag = 0;
					set_time_diff = GetTickCount();
					cb_notice_(ACTION_GET_TIME, true);
					cb_notice_(ACTION_ENTER_KEY, true);
				}
	#endif
#endif
				
				cb_notice_(INFO_PSD_CHECK_END, true);
				
			case ENUM_DIAG_VERSION_FIRMWARE:
			case ENUM_DIAG_VERSION_UI:
			case ENUM_DIAG_VERSION_SENSOR:
			case ENUM_DIAG_VERSION_HARDWARE:
				{
					if( diag_value == diag_val.getElemDefault(diag_id) )
					{
						res = true;
					}
					
				}	break;
				
			case ENUM_DIAG_IR_FRONT_EX0:
			case ENUM_DIAG_IR_FRONT_EX1:
			case ENUM_DIAG_IR_FRONT_EX2:
			case ENUM_DIAG_IR_FRONT_EX3:
			case ENUM_DIAG_IR_FRONT_CENTER:
				{
					if( diag_value == diag_val.getElemDefault(diag_id) )
					{
						switch(model_version)
						{
						case model_miele:
							res = checkSevenIRs(diag_id);
							break;

						case model_pluto:
							res = checkSevenIRs_omega(diag_id);
							break;
							
						default:
							res = true;
							break;
						}
					}
					display = false;
					
				} break;
				
			case ENUM_DIAG_IR_DOCKING_RIGHT:
			case ENUM_DIAG_IR_DOCKING_CENTER:
				{
					if ( model_version == model_arte30 )
					{
						if( diag_value == diag_val.getElemDefault(diag_id) )
						{
							res = true;
						}

						diag_val.setElemResult(diag_id, res);
						
						if ( diag_val.getElemResult(ENUM_DIAG_IR_DOCKING_CENTER) )
						{
							cb_notice_(diag_id, true);
						}
						else
						{
							cb_notice_(diag_id, false);
						}
						
						setTimeOut(1);
						
						return true;
					}
					else
					{
						if( diag_value == diag_val.getElemDefault(diag_id) )
						{
							res = true;
						}
						display = false;
					}
					
				} break;
				
			case ENUM_DIAG_IR_FRONT_RIGHT:
				{
					switch(model_version)
					{
					case model_miele:
						if( diag_value == diag_val.getElemDefault(diag_id) )
						{
							res = checkSevenIRs(diag_id);
							if (res && state_chk7ir == IR7_finish)
							{
								res = true;
								//diag_val._result[diag_id] = true;
								diag_val.setElemResult(diag_id, true);
								
								for (int n=ENUM_DIAG_IR_FRONT_RIGHT;
								n<=ENUM_DIAG_IR_FRONT_EX3; n++)
								{
									//if (!diag_val._result[n])
									if (!diag_val.getElemResult(n))
									{
										res = false;
										std::stringstream ss;
										ss << ">>> [" << n << "] is false.\n";
										slog_debug(ss.str());
									}
								}
							}
							else
							{
								slog_console(">>> The sequence of checking 7-IR is wrong\n");
							}
						}
						
						cb_notice_(diag_id, res);
						return true;
						
						//            {
						//              cb_notice_(diag_id, true);
						//            }
						//            else
						//            {
						//              cb_notice_(diag_id, false);
						//            }            
						break; 

					case model_pluto:
						if( diag_value == diag_val.getElemDefault(diag_id) )
						{
							res = checkSevenIRs_omega(diag_id);
							if (res && state_chk7ir == IR7_finish)
							{
								res = true;
								//diag_val._result[diag_id] = true;
								diag_val.setElemResult(diag_id, true);
								
								for (int n=ENUM_DIAG_IR_FRONT_RIGHT;
								n<=ENUM_DIAG_IR_FRONT_EX3; n++)
								{
									//if (!diag_val._result[n])
									if (!diag_val.getElemResult(n))
									{
										//printf("[debug] Right IR is detected!!! res = false n: %d!!\n", n); 
										res = false;
										std::stringstream ss;
										ss << ">>> [" << n << "] is false.\n";
										slog_debug(ss.str());
									}
								}
							}
							else
							{
								slog_console(">>> The sequence of checking 7-IR is wrong\n");
							}
						}
						
						cb_notice_(diag_id, res);
						return true;         
						break; 

					default:
						if( diag_value == diag_val.getElemDefault(diag_id) )
						{
							res = true;
						}
						display = false;
						break;
					}
				}
				break;
				
			case ENUM_DIAG_IR_FRONT_LEFT:
				{
					switch(model_version)
					{
					case model_miele:
						if( diag_value == diag_val.getElemDefault(diag_id) )
						{
							checkSevenIRs(diag_id);
							res = true;
						}
						display = false;
						break;

					case model_pluto:
						if( diag_value == diag_val.getElemDefault(diag_id) )
						{
							checkSevenIRs_omega(diag_id);
							res = true;
						}
						display = false;
						break; 
						
					default:
						if( diag_value == diag_val.getElemDefault(diag_id) )
						{
							res = true;
						}
						
						//diag_val._result[diag_id] = res;
						diag_val.setElemResult(diag_id, res);
						
						//            if (diag_val._result[ENUM_DIAG_IR_FRONT_RIGHT]
						//              && diag_val._result[ENUM_DIAG_IR_FRONT_CENTER]
						//              && diag_val._result[ENUM_DIAG_IR_FRONT_LEFT] )
						if ( diag_val.getElemResult(ENUM_DIAG_IR_FRONT_RIGHT)
							&& diag_val.getElemResult(ENUM_DIAG_IR_FRONT_CENTER)
							&& diag_val.getElemResult(ENUM_DIAG_IR_FRONT_LEFT))
						{
							cb_notice_(diag_id, true);
						}
						else
						{
							cb_notice_(diag_id, false);
						}
						return true;
					}
				}	break;
				
			case ENUM_DIAG_IR_DOCKING_LEFT:
				{
					if( diag_value == diag_val.getElemDefault(diag_id) )
					{
						res = true;
					}
					
					//diag_val._result[diag_id] = res;
					diag_val.setElemResult(diag_id, res);
					
					//          if (diag_val._result[ENUM_DIAG_IR_DOCKING_RIGHT]
					//            && diag_val._result[ENUM_DIAG_IR_DOCKING_CENTER]
					//            && diag_val._result[ENUM_DIAG_IR_DOCKING_LEFT] )
					if ( model_version != model_arte30 )
					{
						if ( diag_val.getElemResult(ENUM_DIAG_IR_DOCKING_RIGHT)
							&& diag_val.getElemResult(ENUM_DIAG_IR_DOCKING_CENTER)
							&& diag_val.getElemResult(ENUM_DIAG_IR_DOCKING_LEFT))
						{
							cb_notice_(diag_id, true);
						}
						else
						{
							cb_notice_(diag_id, false);
						}
					}
					
					setTimeOut(1);
					
					return true;
					
				}	break;
				
			case ENUM_DIAG_PE_FORWARD:
				{
					if( diag_value < diag_val.getElemDefault(diag_id) )
					{
						res = true;
					}
					
					setTimeOut(3);
					
				}	break;
				
			case ENUM_DIAG_PE_BACKWARD:
				{
					if( diag_value < diag_val.getElemDefault(diag_id) )
					{
						res = true;
					}
					
					setTimeOut(2);
					
				}	break;
				
			case ENUM_DIAG_FORWARD_LEFT:
				{
					if( diag_value > diag_val.getElemMin(diag_id)
						&& diag_value < diag_val.getElemMax(diag_id) )
					{
						res = true;
						
						//if (!diag_val._result[ENUM_DIAG_BACKWARD_LEFT])
						if (!diag_val.getElemResult(ENUM_DIAG_BACKWARD_LEFT))
						{
							display = false;
						}
					}
					
				}	break;
				
			case ENUM_DIAG_FORWARD_RIGHT:
				{
					if( diag_value > diag_val.getElemMin(diag_id)
						&& diag_value < diag_val.getElemMax(diag_id) )
					{
						res = true;
						
						//if (!diag_val._result[ENUM_DIAG_BACKWARD_RIGHT])
						if (!diag_val.getElemResult(ENUM_DIAG_BACKWARD_RIGHT))
						{
							display = false;
						}
					}
					
				}	break;
				
			case ENUM_DIAG_DUST_SENSOR:
				{
					if (!_start_test)
					{
						initialize();
						diag_val.setElemValue(diag_id,diag_value);
					}

					switch(model_version)
					{
					case model_pluto:
						{
							printf("diag_value: %d | min: %d, max: %d\n", diag_value, diag_val.getElemMin(diag_id), diag_val.getElemMax(diag_id) ); 
							switch ( diag_value )
							{
							case -1: 
								diag_value = -1; 
								break; 
							case 0:
								diag_value = 0; // hack
								break; 
							case 1: 
								diag_value = 1; 
								break; 
							default: 
								if( /*diag_value > diag_val.getElemMin(diag_id)
									&&*/ diag_value > diag_val.getElemMax(diag_id) )
								{
									res = true;
									diag_value = 3; 
								}
								break; 
							}
							// Change hbit image color
							cb_triple_check_(diag_id, diag_value);
						} break;
						
					default: return res;
						break;
					}
					//return true; 

				} break; 

			case ENUM_DIAG_NOZZLE:
			case ENUM_DIAG_DUST:
			case ENUM_DIAG_DCJACK:
			case ENUM_DIAG_MOP:
				{
					if (!_start_test && model_version != model_pluto)
					{
						initialize();
						diag_val.setElemValue(diag_id,diag_value);
					}
					
					cb_triple_check_(diag_id, diag_value);
					switch(diag_value)
					{
						//          case -1: diag_val._result[diag_id] = false;  break;
						//          case  3: diag_val._result[diag_id] = true;   break;
					case -1: diag_val.setElemResult(diag_id, false);  break;
					case  3: diag_val.setElemResult(diag_id, true);   break;
						
					case  1: break;
					default: break;
					}
					
					//printf(">>> id(%d), %d\n", diag_id, diag_val.getElemValue(diag_id));
					
					return true;
					
				}	break;
				
			case ENUM_DIAG_CHARGER_ON:
				{
					std::stringstream ss_log;
					ss_log << ">>> ENUM_DIAG_CHARGER_ON (OFF:";
					//ss_log << diag_val._result[ENUM_DIAG_CHARGER_OFF] << ") \n";
					ss_log << diag_val.getElemResult(ENUM_DIAG_CHARGER_OFF) << ") \n";
					slog_debug(ss_log.str());
					//printf(">>> ENUM_DIAG_CHARGER_ON (OFF:%d) \n", diag_val._result[ENUM_DIAG_CHARGER_OFF]);
					//if ( !diag_val._result[ENUM_DIAG_CHARGER_OFF] )
					if ( !diag_val.getElemResult(ENUM_DIAG_CHARGER_OFF) )
					{
						display = false;
					}
				}
			case ENUM_DIAG_CHARGER_OFF:
			case ENUM_DIAG_HALL:
				{
					if( diag_value == diag_val.getElemDefault(diag_id) )
					{
						res = true;
					}
					
				}	break;
				
				//			case ENUM_DIAG_PSD_LEFT:
				//			case ENUM_DIAG_PSD_RIGHT:
				//				{
				//					res = true;
				//
				//				}	break;
				
			case ENUM_DIAG_PSD_LEFT:
			case ENUM_DIAG_PSD_RIGHT:
				{
					switch(model_version)
					{
					case model_pop_lowcost:
					case model_pop:
						{
							if( diag_value > diag_val.getElemMin(diag_id)
								&& diag_value < diag_val.getElemMax(diag_id) )
							{
								res = true;
							}
							else if (diag_val.getElemDefault(ENUM_DIAG_VERSION_HARDWARE) > 103)
							{
								res = true;
							}
							else if (!_passed_info)
							{
								_passed_info = true;
								setErrorFlag(0x04);
								cb_notice_(ERR_PSD, true);
							}
						} break;
						
					default: return res;
						break;
					}
				} break;
				
			case ENUM_DIAG_PSD_CENTER:
				{
					switch(model_version)
					{
					case model_pop_lowcost:
					case model_pop:
						{
							if( diag_value > diag_val.getElemMin(diag_id)
								&& diag_value < diag_val.getElemMax(diag_id) )
							{
								res = true;
							}
							else if (!_passed_info)
							{
								_passed_info = true;
								setErrorFlag(0x04);
								cb_notice_(ERR_PSD, true);
							}
						} break;
						
					default:    return res;
						break;
					}
				}	break;

			case ENUM_DIAG_PSD_LEFT_TOP:
			case ENUM_DIAG_PSD_RIGHT_TOP:
			case ENUM_DIAG_PSD_CENTER_TOP:
				{
					switch(model_version)
					{
						case model_pluto: 
						{
							if( diag_value == diag_val.getElemDefault(diag_id) )
							{
								//printf("[Debug] Side PSD Test [%d] => [%d], Succeded!!!\n", diag_id, diag_value); 
								res = true; 
								diag_val.setElemResult(diag_id, true);
							}
							else
							{
								//printf("[Debug] Side PSD Test [%d] => [%d], Failed!!!\n", diag_id, diag_value); 
								res = false; 
							}
							
							//cb_notice_(diag_id, res);
							//return true;  
						} break; 

						default:    return res; 
							break; 
					}
				} break; 
				
			case ENUM_DIAG_VACUUM:
				{
					switch( model_version )
					{
					case model_pop_lowcost:
						res = true;
						break;

					default:
						if( diag_value > diag_val.getElemMin(diag_id)
							&& diag_value < diag_val.getElemMax(diag_id) )
						{
							res = true;
						}
#ifndef _POP_
	#if ARTE_VERSION > 123
						set_time_flag = 1;
						cb_notice_(ACTION_GET_TIME, true);
	#endif
#endif
						break;
					}
					
				} break;
				
				//    #if ARTE_VERSION > 123
				//      case :
				//        {
				//          if( diag_val.getElemValue(diag_id) == diag_val.getElemDefault(diag_id) )
				//          {
				//            res = true;
				//          }
				//        } break;
				//    #endif
				
			default:
				{
					if( diag_value > diag_val.getElemMin(diag_id)
						&& diag_value < diag_val.getElemMax(diag_id) )
					{
						res = true;
					}
				}	break;
			}
			
			//diag_val._result[diag_id] = res;
			diag_val.setElemResult(diag_id, res);

			if (display)
			{
				cb_notice_(diag_id, res);
			}
			else if (enable_log_debug)
			{
				//printf(">>> The result isn't displayed. (%d) \n", diag_id);
				std::stringstream ss_log;
				ss_log << ">>> The result isn't displayed. (" << diag_id << ")\n";
				slog_debug(ss_log.str(), true);
			}
			
			//printf(">>> id(%d), %d\n", diag_id, res);
			
		}	//	if (compareString(index, print_tag))
		
		return res;
	}
	
	bool compareString(int begin = 0, std::string _str = "", int option = 0)
	{
		int n = begin;
		int end = begin + _str.length();
		
		std::string str("");
		
		if (n >= end )
		{
			return false;
		}
		
		if (option==1)	printf(">>> dd_console : compareString() : ");
		for (; n < end; n++ )
		{
			str.append(1, _buffer[n]);
			if (option==1)	printf("%c", _buffer[n]);
			if (_buffer[n] != _str.at(n - begin))
			{
				if (option==1)	printf("\n");
				return false;
			}
		}
		if (option==1)	printf("\n");
		
		return true;
	}
	
	int findString(std::string& src, std::string tgt)
	{
		found_string_index_ = src.find(tgt, 0);
		return found_string_index_;
		//printf("*** pos = %d \n", pos);
	}
	
	bool findString(std::string& src, std::string tgt, int& index)
	{
		index = src.find(tgt, 0);
		bool ret = (index != -1) ? true : false;
		index += tgt.length();
		return ret;
	}

	bool findMainTag(std::string& src, std::string mainTag, int& index)
	{
		index = src.find(mainTag, 0); 
		bool ret = (index != -1) ? true : false; 
		index += mainTag.length(); 

		return ret; 
	}
	
	//	===============================================================================================================
	//	GET or SET VALUE
	//	===============================================================================================================
	
	void writeResult()
	{
		std::stringstream res;
		
		res.clear();
		res.str("");
		
		for (int index = 0; index < ENUM_DIAG_LENGTH; index++)
		{
			switch(index)
			{
				// item to be ignored
#ifdef _POP_
			case ENUM_DIAG_SIDEBRUSH_RIGHT:
			case ENUM_DIAG_RTC:
			case ENUM_DIAG_DUST:
			case ENUM_DIAG_HALL:
			case ENUM_DIAG_GTEST_LEFT:
			case ENUM_DIAG_GTEST_RIGHT:
			case ENUM_DIAG_GTEST_ORIGIN:  break;
				
			case ENUM_DIAG_MOP:
				{
					if (diag_val.getElemDefault(ENUM_DIAG_VERSION_HARDWARE) > 103 
						&& model_version != model_pop_lowcost )
					{
						res << "\t" << diag_val.getElemValue(index);
					}
				} break;
				
#else	
			case ENUM_DIAG_DUST_SENSOR:
				{
					switch( model_version )
					{
						case model_pluto:
							res << "\t" << diag_val.getElemValue(index);
							break;
						default:
							break;
					}
				}
				break;
			case ENUM_DIAG_DUST:
				{
					switch( model_version )
					{
						case model_arte25:
						case model_arte30:
							break;
						default:
							res << "\t" << diag_val.getElemValue(index);
							break;
					}
				}
				break;
			case ENUM_DIAG_NOZZLE:
				{
					switch( model_version )
					{
						case model_pluto:
							res << "\t" << diag_val.getElemValue(index);
							break;
						default: 
							break; 
					}
				}
				break;
			case ENUM_DIAG_MOP:
				{
					switch(model_version)
					{
					case model_miele:
						break;
					default:
						{
							res << "\t" << diag_val.getElemValue(index);
						}
						break;
					}
				} break;
#endif
			case ENUM_DIAG_SERIAL_NUMBER:
				{
					unsigned int tmp_sn = 0;
					int a = diag_val.getElemValue(ENUM_DIAG_RESULT);
					if ( a )
					//if (diag_val.getElemValue(ENUM_DIAG_RESULT))
					{
						tmp_sn = serial_number_;
						cb_notice_(INFO_SERIAL_NUMBER,true);
						serial_number_++;
					}
					diag_val.setElemValue(ENUM_DIAG_SERIAL_NUMBER,tmp_sn);
					res << "\t" << diag_val.getElemValue(index);
				} 
				break;

#if ARTE_VERSION < 124
			case :
			case ENUM_DIAG_RTC: break;
				break;
#else
			case ENUM_DIAG_VERSION_PCB:
				//res << "\t" << diag_val.pcb_version_;
				res << "\t" << diag_val.got_pcb_version;
				break;
#endif
						
				//      case ENUM_DIAG_RESULT: break;
				
			case ENUM_DIAG_DCJACK_SIGNAL: break;
				
			default:
				{
					res << "\t" << diag_val.getElemValue(index);
				}	
				break;
						
			} //  switch(n)
			
			//std::cout << res.str() << std::endl;
		}
		
		res << "\t" << static_cast<int>(error_flag_) << "\t" << diag_val._timeout;
		if (enable_elapsed_time)
		{
			res << "\t(" << elapsed_time << ")";
		}    
		res << "\n";
		//		res << "\t" << diag_val.gtest_left << "\t" << diag_val.gtest_right;
		if (enable_log_console)
		{
			std::stringstream ss_log;
			ss_log << ">>> The result is saved. (";
			ss_log << res.str().length() << ")\n";
			slog_debug(ss_log.str());
			//printf(">>> The result is saved. (%d)\n", res.str().length());
			
			//      if (res.str().length()!=0)
			//      {
			//        printf(">>> %s \n", res.str().c_str());
			//      }
		}
		
		slog_result(res.str());
		
		switch(model_version)
		{
		case model_pop_lowcost:
		case model_pop: 
			break;
			
		default:
			{
				std::stringstream ss;
				ss << "\t" << optX << "\t" << optY;
				int n = 0;
				for (n=0; n<3; n++)
				{
					ss << "\t" << diag_val.gyro_info[n];
				}
				for (n=0; n<18; n++)
				{
					for (int nn=0; nn<3; nn++)
					{
						ss << "\t" << diag_val.gyro_detail[n][nn];
					}
				}
				ss << std::endl;
				slog_boot(ss.str());
				
			} break;
		}
		
		if (diag_val.getElemValue(ENUM_DIAG_RESULT))
		{
			setDiagTime();
		}
	}
	
	void setDiagTime()
	{
		// saving logged time
		SYSTEMTIME logged_time;
		slog_result.getLoggedTime(logged_time);
		std::stringstream strm;
		strm << std::setfill('0') << std::setw(2) << logged_time.wYear;
		logged_time_ = strm.str();
		strm.clear();
		strm.str("");
		strm << position_ << logged_time_.at(2) << logged_time_.at(3);
		strm << std::setfill('0') << std::setw(2) << logged_time.wMonth;
		strm << std::setfill('0') << std::setw(2) << logged_time.wDay;
		strm << std::setfill('0') << std::setw(2) << logged_time.wHour;
		strm << std::setfill('0') << std::setw(2) << logged_time.wMinute;
		//printf(">>> %s\n", strm.str().c_str());
		logged_time_ = strm.str();
		cb_notice_(ACTION_SET_DIAGTIME, true);
	}
	
	bool getNumberValue(int begin, int& value)
	{
		int n = begin;
		std::string str_val("");
		
		while( checkNull(_buffer[n]) && _buffer[n] != 0x20  )
		{
			str_val.append(1, _buffer[n++]);
		}
		_get_value_index = n;
		
		if (str_val.compare("none") != 0)
		{
			value = atoi(str_val.c_str());
			return true;
		}
		return false;
	}
	
	//  bool getNumberValue(int begin, float& value)
	//  {
	//    int n = begin;
	//    std::string str_val("");
	//    
	//    while( checkNull(_buffer[n]) && _buffer[n] != 0x20  )
	//    {
	//      str_val.append(1, _buffer[n++]);
	//    }
	//    _get_value_index = n;
	//    
	//    if (str_val.compare("none") != 0)
	//    {
	//      value = atof(str_val.c_str());
	//      return true;
	//    }
	//    return false;
	//  }
	
	bool getNumberValue(int begin, float& value)
	{
		int n = begin;
		std::stringstream ss;
		
		while( checkNull(_buffer[n]) && _buffer[n] != 0x20  )
		{
			ss << _buffer[n++];
		}
		_get_value_index = n;
		
		if (ss.str().compare("none") != 0)
		{
			value = atof(ss.str().c_str());
			return true;
		}
		//    else
		//    {
		//      printf(">>> getNumberValue; %s\n", ss.str().c_str());
		//    }
		return false;
	}
	
	bool getNumberValue(int begin, float& value, int& index)
	{
		int n = begin;
		std::stringstream ss;
		
		// 0x20: space
		// 
		//while( checkNull(_buffer[n]) && _buffer[n] != 0x20  )
		while(isItNumber(_buffer[n]))
		{
			ss << _buffer[n++];
		}
		_get_value_index = n;
		index = n;
		
		//    printf(">>> getNumberValue(%d,%f,%d) ==> %s\n"
		//      , begin, value, index, ss.str().c_str());
		
		if (ss.str().compare("none") != 0)
		{
			value = atof(ss.str().c_str());
			return true;
		}
		return false;
	}
	
	void getStringValue(int begin_index, std::string& string_value)
	{
		int	n = begin_index;
		while( checkNull(_buffer[n]) )
		{
			string_value.append(1, _buffer[n++]);
		}
	}
	
	bool checkNull(char& ch)
	{
		switch(ch)
		{
		case 0x0:	//	Ctrl-@ NUL
		case 0xa:	//	Ctrl-J LF
		case 0xd:	//	Ctrl-M CR
		case ' ':
			return false;
			
		default:
			return true;
		}
	}
	
	bool isItNumber(char& ch)
	{
		switch(ch)
		{
		case 0x2b:    // +
		case 0x2d:    // -
		case 0x2e:    // .
		case 0x30:    // 0
		case 0x31:    // 1
		case 0x32:    // 2
		case 0x33:    // 3
		case 0x34:    // 4
		case 0x35:    // 5
		case 0x36:    // 6
		case 0x37:    // 7
		case 0x38:    // 8
		case 0x39:    // 9
			return true;
			
		default:
			return false;
		}
	}
	
	int charp_size(char* _p)
	{
		int _n = 0;
		while(_p[_n++] != NULL)	{}
		return _n - 1;
	}
	
	unsigned char a2h( char code )
	{
		if( code >= 'a' )		return code - 'a' + 10;
		else if( code >= '0')	return code - '0';
		else
		{
			if (enable_log_result)
			{
				std::cout << "a2h; could not convert give code, check your code!" << std::endl;
			}
			return 0;
		}
	}
	
	void show_jpg( IplImage * srcImage )
	{
		IplImage * image;
		if( srcImage == 0 )	image = cvCloneImage( raw_img );
		else				image = cvCloneImage( srcImage );
		
		// Draw the some reference lines
		cvLine(image, cvPoint(0,optY),	cvPoint(320,optY), CV_RGB(255, 0, 0) );
		cvLine(image, cvPoint(optX,0),	cvPoint(optX,240), CV_RGB(255, 0, 0) );
		
		// Draw the some reference lines
		cvLine(srcImage, cvPoint(0,optY),	cvPoint(320,optY), CV_RGB(255, 0, 0) );
		cvLine(srcImage, cvPoint(optX,0),	cvPoint(optX,240), CV_RGB(255, 0, 0) );
		
		cvShowImage( "Image", image );
		
		cvWaitKey(20);
		
		cvReleaseImage( &image );
	}
	
	void decodeImage( int index, char* buffer, int size )
	{
		bool	prefix = false;
		int		rcv_byte = 0;
		
		unsigned char hex_value;
		const int dummy_byte(8);
		int len_of_jpeg_buffer(size+dummy_byte);
		unsigned char * raw_jpg = new unsigned char [len_of_jpeg_buffer];
		memset( raw_jpg, 0, (len_of_jpeg_buffer) );
		
		while ( buffer[index] != '\n' )
		{
			unsigned char nibble = a2h( buffer[index++] );
			
			if(prefix)
			{
				prefix = false;
				hex_value |= nibble;
				if( rcv_byte < len_of_jpeg_buffer )
				{
					raw_jpg[ rcv_byte ] = hex_value;
				}
				rcv_byte++;
			}
			else if(!prefix)
			{
				prefix = true;
				hex_value = nibble << 4;
			}
		}
		
		//		int diff = size - rcv_byte;
		//		diff = abs(diff);
		//		if (diff > 1)	_rcv_diff = true;
		//		else			_rcv_diff = false;
		
		_rcv_diff = ( abs( size - rcv_byte ) > 1 ) ? true : false;
		
		if (enable_log_console)
		{
			std::stringstream ss_log;
			ss_log << ">>> jpg size = " << size;
			ss_log << ", rcv size = " << rcv_byte << "\n";
			slog_debug(ss_log.str());
			//printf(">>> jpg size = %d, rcv size = %d\n", size, rcv_byte);
		}
		
		//@note that
		// if a difference between notified size of jpeg image and received number of bytes is larger than 1, 
		// there might be bad communication
		if( !_rcv_diff )
		{
			Jpeg::Decoder jpeg_decoder( raw_jpg, rcv_byte );
			if( jpeg_decoder.GetResult() != Jpeg::Decoder::OK )
			{
				if (enable_log_console)
				{
					printf(">>> There is jpeg decoding error \n");
				}
			}
			
			
			printf("jpeg_decoder.IsColor() = %d\n"
				, jpeg_decoder.IsColor());
			
			printf("jpeg_decoder.GetImageSize() = %d\n"
				, jpeg_decoder.GetImageSize());
			
			
			unsigned char * decoded_jpeg = jpeg_decoder.GetImage();
			RgbImage rgb_src( raw_img );			
			RgbPixel rgb;
			for( int u(0); u<320; u++ )
			{
				for( int v(0); v<240; v++ )
				{
					if( jpeg_decoder.IsColor() )
					{
						rgb.b = decoded_jpeg[v*320*3+u*3+0];
						rgb.g = decoded_jpeg[v*320*3+u*3+1];
						rgb.r = decoded_jpeg[v*320*3+u*3+2];
					}
					else
					{
						rgb.r = rgb.g = rgb.b = decoded_jpeg[v*320+u];
					}
					
					rgb_src[v][u] = rgb;
				}
			}
			//			cvCircle( raw_img, cvPoint(160,120), 8, cvScalar(0, 0, 255, 0.5), -1, 8, 0);
			//			FILE * f = fopen( (jpeg_decoder.IsColor() ? "jpeg_out.ppm" : "jpeg_out.pgm"), "wb");
			//			fprintf(f, "P%d\n%d %d\n255\n", jpeg_decoder.IsColor() ? 6 : 5, jpeg_decoder.GetWidth(), jpeg_decoder.GetHeight());
			//			fwrite(jpeg_decoder.GetImage(), 1, jpeg_decoder.GetImageSize(), f);
			//			fclose(f);
			//			cvShowImage( "Image", raw_img );	
			//			cvWaitKey(20);
			
			optX = diag_val.boot_camera_[ENUM_SUB_CAMERA_OPTX].getValue();
			optY = diag_val.boot_camera_[ENUM_SUB_CAMERA_OPTY].getValue();
			focal_len = diag_val.boot_camera_[ENUM_SUB_CAMERA_F].getValue();
			
			if( optX < OPTX_MIN || OPTX_MAX < optX || optY < OPTY_MIN || OPTY_MAX < optY )
			{
				cb_notice_(ERR_CAMERA_CALIBRATION, false);
				
				if (enable_log_console)
				{
					printf(">>> optic axis: %f, %f ", optX, optY );
					printf(">>> It may be wrong optic axis \n");			
				}
				optX = 158;
				optY = 122;
				focal_len = 234;
				
				return;
			}
			cvSaveImage( "last_raw.jpg", raw_img );
			sv113Rectify sv113_rectifier(optX,optY);
			sv113_rectifier.rectify( raw_img );			
			show_jpg( raw_img );
			cvSaveImage( "last.jpg", raw_img );
			
			EC();
			cb_notice_(ACTION_IMAGE, true);
			LC();
		}
	}
	
	void read_file(std::string filename, int size)
	{
		ifstream	fin(filename.c_str(), ios::in);
		
		char	buffer[BUFFER_SIZE];
		char	ch = ' ';
		
		int		idx = 0;
		
		memset(buffer, 0, BUFFER_SIZE);
		
		while (!fin.eof())
		{
			fin.get(ch);
			buffer[idx] = ch;
			idx++;
		}
		decodeImage(0, buffer, size);
		
		fin.close();
	}
	
	void readFileToBuffer(std::string& filename)
	{
		read_file_to_buffer = true;
		ifstream	fin(filename.c_str(), ios::in);
		char		ch = ' ';
		
		_idx = 0;
		
		while (!fin.eof())
		{
			fin.get(ch);
			
			_buffer[_idx++] = ch;
			
			//if (ch == '\n')	{	new_line();	::Sleep(25);	}
			if (ch == '\n')
			{
				new_line();
				::Sleep(25);
			}
		}
		
		fin.close();
		read_file_to_buffer = false;
	}
	
	void readFileToBufferForRun(std::string& filename)
	{
		ifstream	fin(filename.c_str(), ios::in);
		char      ch = 0;
		int       cnt = 0;
		
		// input
		while (!fin.eof())
		{
			fin.get(ch);
			inbuf.push_back(ch);
		}
		fin.close();
		
		read_file_to_buffer_for_run = true;
	}
	
	void readDataToBuffer(std::string& filename)
	{
		std::deque<unsigned char> dq_test;
		std::deque<unsigned char>::iterator it;
		
		ifstream	fin(filename.c_str(), ios::in);
		char      ch = 0;
		int       cnt = 0;
		
		// input
		while (!fin.eof())
		{
			fin.get(ch);
			dq_test.push_back(ch);
		}
		fin.close();
		
		// output
		// for ( it = dq_test.begin();
		//          it != dq_test.end();
		//          it++ )
		while( !dq_test.empty() )
		{
			unsigned char datum = dq_test[0];
			dq_test.pop_front();
			if (dq_test.begin() == dq_test.end())
			{
				printf("dq is empty. \n");
			}
			
			
			//      printf("%c", dq_test.front());
			//      dq_test.pop_front();
			printf("%c", datum);
			
			switch(datum)
			{
			case 10:
				printf("[%03d]\t", ++cnt);
				break;
				
			default:
				break;
			}
			
			switch(cnt)
			{
			case 60:
				dq_test.clear();
				break;
				
			default:
				break;
			}
		}
	}
	
	void set_buffer(std::string buff)
	{
		_idx = buff.size();
		memcpy(_buffer, buff.c_str(), _idx);
		new_line();
	}
	
/*
	bool check_gyro_result()
	{
		int l_max = diag_val._elemGyro[0].getMax();
		int r_max = diag_val._elemGyro[1].getMax();
		int o_max = diag_val._elemGyro[2].getMax();
		
		int l_min = diag_val._elemGyro[0].getMin();
		int r_min = diag_val._elemGyro[1].getMin();
		int o_min = diag_val._elemGyro[2].getMin();
		
		int l_res = static_cast<int>(diag_val.gtest_left*10);
		int r_res = static_cast<int>(diag_val.gtest_right*10);
		int o_res = static_cast<int>(diag_val.gtest_origin*10);
		
		if (enable_log_debug)
		{
			//      printf(">>> check_gyro_result:\t%d\t%d\t%d\t%d\t%d\t%d\n",
			//        o_res, o_max, l_res, l_min, r_res, l_max);
			std::stringstream ss_log;
			ss_log << ">>> check_gyro_result:\t";
			ss_log << o_res << "\t" << o_max << "\t" << l_res << "\t";
			ss_log << l_min << "\t" << r_res << "\t" << l_max << "\n";
			slog_debug(ss_log.str());
		}
		
		diag_val.setElemValue(ENUM_DIAG_GTEST_LEFT, l_res);
		diag_val.setElemValue(ENUM_DIAG_GTEST_RIGHT, r_res);
		diag_val.setElemValue(ENUM_DIAG_GTEST_ORIGIN, o_res);
		
		cb_notice_(ACTION_CLEAR_RTC, true);
		//    Sleep(1000);
		//    cb_notice_(ACTION_CLEAR_RTC, true);
		//    Sleep(1000);
		cb_notice_(ACTION_GOTO_TESTPOINT, true);
		
		bool res = true;
		bool ret = true;
		
		if ( o_res < o_min || o_res > o_max )	res = ret = false;
		//diag_val._result[ENUM_DIAG_GTEST_ORIGIN] = res;
		diag_val.setElemResult(ENUM_DIAG_GTEST_ORIGIN, res);
		
		res = true;
		if ( l_res < l_min || l_res > l_max )	res = ret = false;
		//diag_val._result[ENUM_DIAG_GTEST_LEFT] = res;
		diag_val.setElemResult(ENUM_DIAG_GTEST_LEFT, res);
		
		res = true;
		if ( r_res < r_min || r_res > r_max )	res = ret = false;
		//diag_val._result[ENUM_DIAG_GTEST_RIGHT] = res;
		diag_val.setElemResult(ENUM_DIAG_GTEST_RIGHT, res);
		
		return ret;
	}
*/

	bool check_gyro_result()
	{
		int l_max = diag_val._elemGyro[0].getMax();
		int r_max = diag_val._elemGyro[1].getMax();
		int o_max = diag_val._elemGyro[2].getMax();
		
		int l_min = diag_val._elemGyro[0].getMin();
		int r_min = diag_val._elemGyro[1].getMin();
		int o_min = diag_val._elemGyro[2].getMin();
		
		int l_res = static_cast<int>(diag_val.gtest_left*10);
		int r_res = static_cast<int>(diag_val.gtest_right*10);
		int o_res = static_cast<int>(diag_val.gtest_origin*10);
		
		if (enable_log_debug)
		{
			//      printf(">>> check_gyro_result:\t%d\t%d\t%d\t%d\t%d\t%d\n",
			//        o_res, o_max, l_res, l_min, r_res, l_max);
			std::stringstream ss_log;
			ss_log << ">>> check_gyro_result:\t";
			ss_log << o_res << "\t" << o_max << "\t" << l_res << "\t";
			ss_log << l_min << "\t" << r_res << "\t" << l_max << "\n";
			slog_debug(ss_log.str());
		}
		
		diag_val.setElemValue(ENUM_DIAG_GTEST_LEFT, l_res);
		diag_val.setElemValue(ENUM_DIAG_GTEST_RIGHT, r_res);
		diag_val.setElemValue(ENUM_DIAG_GTEST_ORIGIN, o_res);
		
		//cb_notice_(ACTION_CLEAR_RTC, true);
		//    Sleep(1000);
		//    cb_notice_(ACTION_CLEAR_RTC, true);
		//    Sleep(1000);
// 		cb_notice_(ACTION_CLEAR_RTC, true);
// 		cb_notice_(ACTION_GOTO_TESTPOINT, true);
		
		bool res = true;
		bool ret = true;
		
		if ( o_res < o_min || o_res > o_max )	res = ret = false;
		//diag_val._result[ENUM_DIAG_GTEST_ORIGIN] = res;
		diag_val.setElemResult(ENUM_DIAG_GTEST_ORIGIN, res);
		
		res = true;
		if ( l_res < l_min || l_res > l_max )	res = ret = false;
		//diag_val._result[ENUM_DIAG_GTEST_LEFT] = res;
		diag_val.setElemResult(ENUM_DIAG_GTEST_LEFT, res);
		
		res = true;
		if ( r_res < r_min || r_res > r_max )	res = ret = false;
		//diag_val._result[ENUM_DIAG_GTEST_RIGHT] = res;
		diag_val.setElemResult(ENUM_DIAG_GTEST_RIGHT, res);
		
		return ret;
	}
	
	int get_cpuid()
	{
		return diag_val.cpu_id;
	}
	
	void initializeOnce()
	{
		_idx_img = 0;
		memset(_buffer, 0, BUFFER_SIZE);
		
		diag_state = ENUM_DIAG_VERSION;
		
		switch(model_version)
		{
		case model_pop_lowcost:
		case model_pop:
			break;
			
		default:
			raw_img = cvCreateImage( cvSize(320,240),8,3 );
			cvNamedWindow( "Image", CV_WINDOW_AUTOSIZE );
			
			initializeForArte();
			break;
		}
		
		//diag_val.initializeOnce();
		diag_val.initializeElements();
		
		int psd_chk_thr[2] = {0,};
		int psd_chk_woc_thr[2] = {0,};
		int psd_top_chk_thr[2] = {0,};
		int psd_center_top_chk_thr[2] = {0,};

		if ( model_version == model_pluto )
		{
		  // For Bottom PSD sensor threshold with/without Calibration
	  	  diag_val.getPsdCheckThreshold(psd_chk_thr[0],psd_chk_thr[1],psd_chk_woc_thr[0],psd_chk_woc_thr[1]);
		  psd_chk.setThresholdForPSD(psd_chk_thr[0],psd_chk_thr[1],psd_chk_woc_thr[0],psd_chk_woc_thr[1]);

		  // For Top PSD sensor threshold (Right / Left)
		  diag_val.getPsdTopCheckThreshold(psd_top_chk_thr[0],psd_top_chk_thr[1]);
		  psd_chk.setThresholdForPSDTop(psd_top_chk_thr[0],psd_top_chk_thr[1]);

     	  // For Top PSD sensor threshold (Center)
		  diag_val.getPsdCenterTopCheckThreshold(psd_center_top_chk_thr[0],psd_center_top_chk_thr[1]);
		  psd_chk.setThresholdForPSDCenterTop(psd_center_top_chk_thr[0],psd_center_top_chk_thr[1]);
		}
		else 
		{
		  diag_val.getPsdCheckThreshold(psd_chk_thr[0],psd_chk_thr[1]);
		  psd_chk.setThresholdForPSD(psd_chk_thr[0],psd_chk_thr[1]);
		}
		
		psd_chk.setHJIRModel(false);
#ifdef _POP_
		if (diag_val.getElemDefault(ENUM_DIAG_VERSION_HARDWARE) > 103)
		{
			diag_val.getIRCheckThreshold(psd_chk_thr[0],psd_chk_thr[1]);
			psd_chk.setThresholdForIR(psd_chk_thr[0],psd_chk_thr[1]);
			psd_chk.setHJIRModel(true);
		}
#endif
	}
	
	void set_callback(callback_notice pf)	{	cb_notice_	= pf;	}
	void set_callback_ex(callback_triple pf)	{	cb_triple_check_	= pf;	}
	void set_callback_boot(callback_boot pf){	_callback_boot		= pf;	}
	
	bool i2b(int val)	{	return ( val == 1 ? true : false );	}
	
	bool get_boot()			{	return _boot;	}
	void set_boot(bool val)	{	_boot = val;	}
	
	float	get_angle()			{	return _angle;			}
	int		get_motion_id()		{	return _motion_id;		}
	int		get_motion_state()	{	return _motion_state;	}
	
	bool	set_gtest_result(float left, float right, float origin)
	{
		diag_val.gtest_left		= left;
		diag_val.gtest_right	= right;
		diag_val.gtest_origin	= origin;
		
		return check_gyro_result();
	}
	
	void set_battery(int mode)
	{
		switch(mode)
		{
		case eFinished:
			{
#ifndef _POP_
				EC();
				cb_notice_(ACTION_GTEST,true);
				LC();
#else
				// @todo
				// 1. move backward from docking center.
				// 2. goto test point.
				//        printf(">>> Go To Start Point! \n");
				if ( model_version == model_pop_lowcost )
				{
 					//if ( ::MessageBox( NULL, "로봇을 위치한 후 OK 버튼을 눌러주세요", "Notice", MB_OK ) )
					cb_notice_( ACTION_VERSION_TEST, true );
				}
				else
				{
					cb_notice_(ACTION_BACKWARD_OUT, true);
					cb_notice_(ACTION_WAIT_FOR_FINISH, true);
				}
#endif
			}	break;
			
		case eGetFirstBatteryLevel:
			{
				//				if (_start_test)
				//				{
				////					_start_test = false;
				////
				//					diag_val.setElemValue(ENUM_DIAG_CHARGER_ON, 6);
				//					diag_val._result[ENUM_DIAG_CHARGER_ON] = true;
				//
				//					if( diag_val.getElemValue(ENUM_DIAG_CHARGER_OFF) == diag_val.getElemDefault(ENUM_DIAG_CHARGER_OFF) )
				//					{
				//						cb_notice_(ENUM_DIAG_CHARGER_ON, true);
				//					}
				//					else	cb_notice_(ENUM_DIAG_CHARGER_ON, false);
				//				}
				
				for( int n=0; n<3; n++)	{	diag_val.battery[n] = 0;	}
				diag_val.battery[1] = diag_val.battery[0];
				
			}	break;
			
		case eGetSecondBatteryLevel:
			{
				diag_val.battery[2] = diag_val.battery[0];
				diag_val.battery[0] = diag_val.battery[2] - diag_val.battery[1];
				diag_val.setElemValue(ENUM_DIAG_CHARGING, diag_val.battery[0]);
				
				bool res_value = false;
				if( diag_val.battery[0] > diag_val.getElemDefault(ENUM_DIAG_CHARGING) )
				{
					res_value = true;
				}
				diag_val.setElemResult(ENUM_DIAG_CHARGING, res_value);
				cb_notice_(ENUM_DIAG_CHARGING, res_value);
				
				set_battery(eFinished);
				
			}	break;
			
			//		case eGetSecondBatteryLevelEx:
			//			{
			//				diag_val.battery[2] = diag_val.battery[0];
			//				diag_val.battery[0] = diag_val.battery[2] - diag_val.battery[1];
			//				diag_val.setElemValue(ENUM_DIAG_CHARGING, diag_val.battery[0]);
			//				
			//				//printf("++ %d > %d\n", diag_val.battery[0], diag_val.getElemDefault(ENUM_DIAG_CHARGING));
			//				
			//				if( diag_val.battery[0] > diag_val.getElemDefault(ENUM_DIAG_CHARGING) )
			//				{
			//					diag_val._result[ENUM_DIAG_CHARGING] = true;
			//					cb_notice_(ENUM_DIAG_CHARGING, true);
			//					set_battery(0);
			//				}
			//				else
			//				{
			//					if (diag_val.battery[1] >= 160)
			//					{
			//						//	request battery value
			//						cb_notice_(ACTION_GET_BATTERY_AGAIN, true);
			//					}
			//					else
			//					{
			//						diag_val._result[ENUM_DIAG_CHARGING] = false;
			//						cb_notice_(ENUM_DIAG_CHARGING, false);
			//						set_battery(eFinished);
			//					}
			//				}
			//
			//			}	break;
			
		case eGetBatteryLevelTest:
			{
				diag_val.battery[1] = 141;
				diag_val.battery[2] = 150;
				diag_val.battery[0] = diag_val.battery[2] - diag_val.battery[1];
				diag_val.setElemValue(ENUM_DIAG_CHARGING, diag_val.battery[0]);
				
				bool res_value = false;
				if( diag_val.battery[0] > diag_val.getElemDefault(ENUM_DIAG_CHARGING) )
				{
					res_value = true;
					diag_val.setElemResult(ENUM_DIAG_CHARGING, true);
				}
				
				cb_notice_(ENUM_DIAG_CHARGING, res_value);
				
				cb_notice_(ACTION_GTEST,true);
				
			}	break;
			
		default:
			break;
		}
		
		//		int ret = diag_val.battery[mode];
		//printf("++set_battery[%d] = %d\n", mode, ret);
		
		//		return diag_val.battery[mode];
	}
	
	bool getResult()
	{
		//diag_val._result[ENUM_DIAG_CHARGER_ON] = true;
		std::stringstream ss_log;
		
		bool ret = true;
		
		if (enable_log_console)
		{
			ss_log << ">>> get_result: false[";
			//printf(">>> get_result: false[");
		}
		
		for (int n(1); n<ENUM_DIAG_RESULT; n++)
		{
			switch(n)
			{
				// item to be ignored
				
#if ARTE_VERSION < 124
			case ENUM_DIAG_RTC:
			case ENUM_DIAG_VERSION_PCB:
				break;
#else
			case ENUM_DIAG_IR_DOCKING_RIGHT:
			case ENUM_DIAG_IR_DOCKING_LEFT:
				{
					switch( model_version )
					{
					case model_arte30:
						break;
					default:
						{
							//if (!diag_val._result[n])
							if (!diag_val.getElemResult(n))
							{
								if (enable_log_console)
								{
									ss_log << " " << n;
									//printf(" %d", n);
								}
								ret = false;
							}
						}
						break;
					}
				}
				break;

			case ENUM_DIAG_IR_DOCKING_CENTER:
			case ENUM_DIAG_VACUUM:
			case ENUM_DIAG_CHARGER_ON:
			//case ENUM_DIAG_CHARGER_OFF:
				switch ( model_version )
				{
				case model_pop_lowcost:
					break;
					
				default:
					{
						//if (!diag_val._result[n])
						if (!diag_val.getElemResult(n))
						{
							if (enable_log_console)
							{
								ss_log << " " << n;
								//printf(" %d", n);
							}
							ret = false;
						}
					}	break;
				}
				break;
				
			case ENUM_DIAG_IR_FRONT_EX0:
			case ENUM_DIAG_IR_FRONT_EX1:
			case ENUM_DIAG_IR_FRONT_EX2:
			case ENUM_DIAG_IR_FRONT_EX3:
				{
					switch(model_version)
					{
					case model_arte:
					case model_arte25:
					case model_arte30:
					case model_pop:
					case model_pop_lowcost:
						break;
						
					case model_miele:
						{
							//if (!diag_val._result[n])
							if (!diag_val.getElemResult(n))
							{
								if (enable_log_console)
								{
									ss_log << " " << n;
									//printf(" %d", n);
								}
								ret = false;
							}
						}	break;

					case model_pluto:
						{
							//if (!diag_val._result[n])
							if (!diag_val.getElemResult(n))
							{
								if (enable_log_console)
								{
									ss_log << " " << n;
									//printf(" %d", n);
								}
								ret = false;
							}
						}	break;

					}
					
				} break;
				
			case ENUM_DIAG_MOP:
				{
					switch(model_version)
					{
					case model_miele: 
					case model_pop_lowcost:
						break;
						
					case model_pop:
						{
							if (diag_val.getElemDefault(ENUM_DIAG_VERSION_HARDWARE) > 103)
							{
								//if (!diag_val._result[n])
								if (!diag_val.getElemResult(n))
								{
									if (enable_log_console)
									{
										ss_log << " " << n;
									}
									ret = false;
								}
							}
						} break;
						
					default:
						{
							//if (!diag_val._result[n])
							if (!diag_val.getElemResult(n))
							{
								if (enable_log_console)
								{
									ss_log << " " << n;
									//printf(" %d", n);
								}
								ret = false;
							}
						}	break;
					}
					
				} break;

			case ENUM_DIAG_DUST_SENSOR:
				{
					switch( model_version )
					{
						case model_pluto:
							{
								if (!diag_val.getElemResult(n))
								{
									if (enable_log_console)
										ss_log << " " << n;
									ret = false;
								}
							} break;

						default:
							break;
					}
				}
				break;

			case ENUM_DIAG_DUST:
				{
					switch( model_version )
					{
						case model_pop:
						case model_pop_lowcost:
						case model_arte25:
						case model_arte30:
							break;

						default:
							{
								if (!diag_val.getElemResult(n))
								{
									if (enable_log_console)
										ss_log << " " << n;
									ret = false;
								}
						}	break;
					}
				}
				break;

			case ENUM_DIAG_NOZZLE:
				{
					switch( model_version )
					{
						case model_pluto:
						{
							if (!diag_val.getElemResult(n))
							{
								if (enable_log_console)
									ss_log << " " << n;
								ret = false;
							}
						} break; 
						
						default: 
							break; 
					}
				}
				break;
				
			case ENUM_DIAG_RTC:
			case ENUM_DIAG_HALL:
			case ENUM_DIAG_GTEST_LEFT:
			case ENUM_DIAG_GTEST_RIGHT:
			case ENUM_DIAG_GTEST_ORIGIN:
			case ENUM_DIAG_SIDEBRUSH_RIGHT:
				{
					switch(model_version)
					{
					case model_pop_lowcost:
					case model_pop:
						break;
						
					default:
						{
							//if (!diag_val._result[n])
							if (!diag_val.getElemResult(n))
							{
								if (enable_log_console)
								{
									ss_log << " " << n;
									//printf(" %d", n);
								}
								ret = false;
							}
						}	break;
					}
				} break;
#endif
				
				default:
					{
						//if (!diag_val._result[n])
						if (!diag_val.getElemResult(n))
						{
							if (enable_log_console)
							{
								ss_log << " " << n;
								//printf(" %d", n);
							}
							ret = false;
						}
					}	break;
						
			} //  switch(n)
		}   //  for (int n(1); n<ENUM_DIAG_RESULT; n++)
		
		if (enable_log_console)
		{
			ss_log << "] (" << ret << ")\n";
			slog_console(ss_log.str());
			//printf("] (%d)\n", ret);
		}
		
		//diag_val._result[ENUM_DIAG_RESULT] = ret;
		diag_val.setElemResult(ENUM_DIAG_RESULT, ret);
		diag_val.setElemValue(ENUM_DIAG_RESULT, ret);
		
		return ret;
	}
	
	int getSubModel()	  { return diag_val.modelNumberSub;  }
	int getMainModel()  { return diag_val.modelNumberMain; }
	
	int get_version(int tag)
	{
		switch(tag)
		{
		case ENUM_DIAG_VERSION:			   return diag_val.getElemDefault(ENUM_DIAG_VERSION);
		case ENUM_DIAG_VERSION_FIRMWARE:   return diag_val.getElemDefault(ENUM_DIAG_VERSION_FIRMWARE);
		case ENUM_DIAG_VERSION_UI:	       return diag_val.getElemDefault(ENUM_DIAG_VERSION_UI);
		case ENUM_DIAG_VERSION_SENSOR:	   return diag_val.getElemDefault(ENUM_DIAG_VERSION_SENSOR);
		case ENUM_DIAG_VERSION_HARDWARE:   return diag_val.getElemDefault(ENUM_DIAG_VERSION_HARDWARE);
			
		default:	return -1;
		}
	}
	
	void finish_test()
	{
		if (enable_log_console)
		{
			slog_console(">>> TEST IS FINISHED!!\n");
			//printf(">>> TEST IS FINISHED!!\n");
		}
		
		if (_start_test)	_start_test = false;

#ifdef _POP_
		//cb_notice_( DIAG_RESULT, getResult() );
		cb_notice_( DIAG_RESULT, true );
#endif

		writeResult();
		
		test_is_finished = true;
		//    got_finish_msg = true;
	}
	
	//	===============================================================================================================
	//	MEMBER VARIABLE
	//	===============================================================================================================
	
	int		getFinishedID()     {	return _finished_id;		}
	int		getTimeOut()        {	return diag_val._timeout;	}
	void	setTimeOut(int n)	  {	diag_val._timeout = n;		}
	
	int   getPsdCheckState()  { return psd_chk.getCheckState(); }
	void  setPsdCheckState(ePsdCheckState chk_state) { psd_chk.setCheckState(chk_state); }
	
	bool  gotFinishMsg()      { return got_finish_msg;  }
	
	void enableLogging(bool enable) { enable_log_result = enable; }
	void enableWholeLogging(bool value) { enable_log_console = value; }
	void enableLoggingForDebug(bool value) { enable_log_debug = value; }
	
	void setSerialNumber(int& serial_number)
	{
		serial_number_ = serial_number;
	}
	
	int getModelVersion() { return model_version; }
	void setModelVersion(int setting_value)
	{
		model_version = setting_value;
	}
	
	std::string getIniLocation() { return diag_val.ini_location; }
	void setIniLocation(std::string location_value)
	{
		//printf(">>> set the location of ini: %s \n", location_value.c_str());
		if (enable_log_debug)
		{
			//printf(">>> set the location of ini: %s \n", str_value.c_str());
			std::stringstream ss;
			ss << ">>> set the location of ini: " << location_value.c_str() << "\n";
			slog_debug(ss.str(), true);
		}
		//diag_val.ini_location = str_value;
		diag_val.setIniLocation(location_value);
	}
	
	void initializeBuffer()
	{
		memset(_buffer, 0, BUFFER_SIZE);
	}
	
	void getPcbVersion(std::stringstream& ss_pcb)
	{
		ss_pcb << diag_val.pcb_version_;
	}
	
	int getHwVersion()
	{
		int hw_version = diag_val.getElemDefault(ENUM_DIAG_VERSION_HARDWARE);
		//printf(">>> HW version: %d \n", hw_version);
		return hw_version;
	}
	
	std::string& getLoggedTime()
	{
		return logged_time_;
	}
	
	void setErrorFlag(unsigned char error_flag, bool init_flag = false)
	{
		if (init_flag)
		{
			error_flag_ = 0x0;
			//printf(">>> setErrorFlag : %d --> initialized...\n", error_flag_);
		}
		else
		{
			error_flag_ |= error_flag;
			//printf(">>> setErrorFlag : %d --> %d...\n", error_flag, error_flag_);
		}
	}
	
	//  void putData()
	//  {
	//    string_data_.putData("aaa");
	//    string_data_.putData("bbb");
	//    string_data_.putData("ccc");
	//    string_data_.putData("ddd");
	//    string_data_.putData("eee");
	//  }
	
	
	
	// ======================================================================
	//  VARIABLES
	// ======================================================================
	
	// -----------------------------
	//   protected variables
	// -----------------------------
	
protected:
	
	int		_idx;
	int		_idx_img;
	int		_jpg_size;
	int		_motion_id;
	int		_motion_state;
	int		_get_value_index;
	int		_finished_id;
	
	char	_buffer[BUFFER_SIZE];
	
	bool	_boot;
	bool	_rcv_diff;
	bool	_start_test;
	bool	_passed_info;
	bool	_check_model_sub;
	
	// initializing test
	bool  test_is_finished;
	bool  test_is_initialized;
	bool  got_finish_msg;
	bool  enable_log_result;
	bool  enable_log_console;
	bool  enable_log_debug;
	
	bool  read_file_to_buffer;
	bool  read_file_to_buffer_for_run;
	
	bool  is_getting_boot_log_finished;
	bool  is_setting_current_time_done;
	
	// for initializing buffer
	bool  should_i_clear_buffer_idx;
	unsigned int last_data_incoming;
	
	float	_angle;
	
	diagnosis_report	diag_val;
	diagnosis_list		diag_state;
	
	std::string   print_result_;
	std::string   last_line;
	std::string   logged_time_;
	
	// -----------------------------
	//   public variables
	// -----------------------------
	
public:
	
	callback_notice     cb_notice_;
	callback_triple	    cb_triple_check_;
	callback_boot       _callback_boot;
	
	IplImage * raw_img;
	
	float optX;
	float optY;
	float focal_len;
	
	SimpleLogger slog_result;
	SimpleLogger slog_console;
	SimpleLogger slog_debug;
	SimpleLogger slog_boot;
	
	bool	gtest_only;
	
	int		ang_x;
	int		ang_y;
	int   model_sub;
	
	int   model_version;
	
	CPSDCheck       psd_chk;
	
	int   found_string_index_;
	int   position_;
	
	unsigned char   error_flag_;
	
	// -------------------------------------
	// not for POP
	// -------------------------------------
	
	//  #ifndef _POP_
	
    int   set_time_[3][3];
    int   set_time_flag;
	
    unsigned int   set_time_diff;
    unsigned int   serial_number_;
	
    bool  error_camera_calibration;
	
	//  #endif
	
    //StringData string_data_;
	
    // -------------------------------------------------------
    //  2013-09-04, WED
    // -------------------------------------------------------
	
    // for checking seven IRs of the 'Miele' model
	
    int state_chk7ir;
	
#if PLUTO2 == 1

	int getReceivedIR_omega(int received_ir)
    {
		switch(received_ir) //
		{
		case ENUM_DIAG_IR_FRONT_LEFT:     return IR7_left;
		case ENUM_DIAG_IR_FRONT_EX3:      return IR7_left_1st;
		case ENUM_DIAG_IR_FRONT_EX2:      return IR7_left_2nd;
		case ENUM_DIAG_IR_FRONT_CENTER:   return IR7_center;
		case ENUM_DIAG_IR_FRONT_EX1:      return IR7_right_2nd;
		case ENUM_DIAG_IR_FRONT_EX0:      return IR7_right_1st;
		case ENUM_DIAG_IR_FRONT_RIGHT:    return IR7_right;
		}
		
		return 0;
    }

    bool checkSevenIRs_omega(int received_ir)
    {
		int got_ir = getReceivedIR_omega(received_ir);
		//printf("[debug] first checkSevenIRs: received_ir: %d, got_ir: %d, state_chk7ir: %d\n"
		//	, received_ir, got_ir, state_chk7ir); 
		bool ret = false;
		
		switch(state_chk7ir)
		{
		case IR7_standby:
		case IR7_left:
		case IR7_left_1st:
		case IR7_left_2nd:
		case IR7_center:
		case IR7_right_2nd:
		case IR7_right_1st:
			//case IR7_right:
			{
				if ( (got_ir == IR7_left_2nd) || (got_ir == IR7_center) || (got_ir == IR7_right_2nd))
				{
					//printf("Side center IR is set to right 2nd IR => [%d]\n", state_chk7ir);
					got_ir = IR7_right_2nd;  
					state_chk7ir = IR7_center; 
				}

				//printf("[debug] second checkSevenIRs: received_ir: %d, got_ir: %d, state_chk7ir: %d\n"
				//	, received_ir, got_ir, state_chk7ir); 

				if (got_ir == state_chk7ir+1)
				{
					//printf("[debug] checkSevenIRs_omega: res = true\n"); 
					std::stringstream ss;
					ss << ">>> " << got_ir << " ==> OK\n";
					slog_debug(ss.str());
					ret = true;
				}
				else
				{
					std::stringstream ss;
					ss << ">>> " << got_ir << " ==> FAIL!!! ==> (";
					ss << state_chk7ir+1 << ") \n";
					slog_debug(ss.str());
				}
			} break;
			
		case IR7_finish:
			slog_debug(">>> the sequence is already finished...\n");
			return false;
		}
		
		state_chk7ir++;
		
		if (state_chk7ir == IR7_right)
		{
			state_chk7ir = IR7_finish;
			slog_debug(">>> COMPLETE!!! \n");
		}

		return ret;
    }

#endif

    int getReceivedIR(int received_ir)
    {
		switch(received_ir)
		{
		case ENUM_DIAG_IR_FRONT_LEFT:     return IR7_left;
		case ENUM_DIAG_IR_FRONT_EX3:      return IR7_left_1st;
		case ENUM_DIAG_IR_FRONT_EX1:      return IR7_left_2nd;
		case ENUM_DIAG_IR_FRONT_CENTER:   return IR7_center;
		case ENUM_DIAG_IR_FRONT_EX2:      return IR7_right_2nd;
		case ENUM_DIAG_IR_FRONT_EX0:      return IR7_right_1st;
		case ENUM_DIAG_IR_FRONT_RIGHT:    return IR7_right;
		}
		
		return 0;
    }

    bool checkSevenIRs(int received_ir)
    {
		int got_ir = getReceivedIR(received_ir);
		bool ret = false;
		
		switch(state_chk7ir)
		{
		case IR7_standby:
		case IR7_left:
		case IR7_left_1st:
		case IR7_left_2nd:
		case IR7_center:
		case IR7_right_2nd:
		case IR7_right_1st:
			//case IR7_right:
			{
				if (got_ir == state_chk7ir+1)
				{
					std::stringstream ss;
					ss << ">>> " << got_ir << " ==> OK\n";
					slog_debug(ss.str());
					ret = true;
				}
				else
				{
					std::stringstream ss;
					ss << ">>> " << got_ir << " ==> FAIL!!! ==> (";
					ss << state_chk7ir+1 << ") \n";
					slog_debug(ss.str());
				}
			} break;
			
		case IR7_finish:
			slog_debug(">>> the sequence is already finished...\n");
			return false;
		}
		
		state_chk7ir++;
		
		if (state_chk7ir == IR7_right)
		{
			state_chk7ir = IR7_finish;
			slog_debug(">>> COMPLETE!!! \n");
		}
		
		return ret;
    }
	
    // -------------------------------------------------------
    //  2013-09-09, MON
    // -------------------------------------------------------
    
    // elapsed time from started test
    
    bool enable_elapsed_time;
    unsigned int elapsed_time;
    unsigned int launched_time;
    
    unsigned int getElapsedTime()
    {
		elapsed_time = GetTickCount() - launched_time;
		return elapsed_time;
    }
};

//};


#endif // DD_ROBHAZ_HPP_