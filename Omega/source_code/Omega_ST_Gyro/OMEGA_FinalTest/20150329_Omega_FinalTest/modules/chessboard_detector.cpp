//#include "stdafx.h"

#include "image_util.hpp"
#include "util.hpp"
#include "chessboard_detector.hpp"
#include "ls_line_fitting.hpp"




//bool chessboardDetector::run( std::string & fileName, double & estimatedHeadingAngle )
bool chessboardDetector::run( IplImage * srcImage, double & estimatedHeadingAngle )
{
	CvSize board_sz = cvSize( board_w, board_h );

	fit_error = 0;
	average_reprojection_error = 0;

	
	CvPoint2D32f* corners = new CvPoint2D32f[ board_total ];
	LSLineFitting::pointSample * points = new LSLineFitting::pointSample[board_w];
	double * angles = new double [board_h];
	double projected_x(0);
	double projected_y(0);
	CvMat* rot_vects		 = cvCreateMat(1,3,CV_64FC1);
	CvMat* trans_vects		 = cvCreateMat(1,3,CV_64FC1);
	CvMat* image_points      = cvCreateMat(board_total,2,CV_64FC1);
	CvMat* img_points2		 = cvCreateMat(board_total,2,CV_64FC1 );
	CvMat* object_points     = cvCreateMat(board_total,3,CV_64FC1);
	double a[] = {fx, 0, cx, 0, fy, cy, 0, 0, 1};
    CvMat A = cvMat( 3, 3, CV_64F, a );
	
	LSLineFitting fitter;

	// reset the some properties of chessboard detector
	no_found_corners			= 0;
	average_reprojection_error = 100.0;

	if( verbose )
	{
		cvNamedWindow( "Snapshot", CV_WINDOW_AUTOSIZE );
		cvNamedWindow( "Reprojection", CV_WINDOW_AUTOSIZE );
	}
	cvNamedWindow( "Snapshot", CV_WINDOW_AUTOSIZE );

	IplImage *image;
	IplImage *image_reprojection;
	IplImage *gray_image;
	
//	image = cvLoadImage( fileName.c_str() );
	image = cvCloneImage( srcImage );
	image_reprojection = cvCloneImage( srcImage );
	gray_image = cvCreateImage(cvGetSize(image),8,1);    
	cvShowImage( "Snapshot", image );
	
	//Find chessboard corners:
	int found = cvFindChessboardCorners(image, 
										board_sz, corners, &no_found_corners,
										CV_CALIB_CB_ADAPTIVE_THRESH | CV_CALIB_CB_FILTER_QUADS );
  if (verbose)
  {
    std::cout << "we found chess board which has " << no_found_corners  << "corners " << std::endl;
  }

	if (!no_found_corners)	return false;
	
	//Get Sub-pixel accuracy on those corners
	// for the refinement of corner points
	cvCvtColor(image, gray_image, CV_BGR2GRAY);           
//	cvFindCornerSubPix(gray_image, corners, no_found_corners, 
//						cvSize(11,11),
//						cvSize(-1,-1), 
//						cvTermCriteria(CV_TERMCRIT_EPS+CV_TERMCRIT_ITER, 40, 0.09 ) );
	cvFindCornerSubPix(gray_image, corners, no_found_corners, 
		cvSize(7,7),
		cvSize(-1,-1), 
						cvTermCriteria(CV_TERMCRIT_EPS+CV_TERMCRIT_ITER, 40, 0.09 ) );
	
	// Draw points
	cvDrawChessboardCorners(image, board_sz, corners, no_found_corners, found);   
	
	if( verbose )
	{
		cvLine(image, cvPoint(0,cy),	cvPoint(320,cy), CV_RGB(255, 0, 0) );
		cvLine(image, cvPoint(cx,0),	cvPoint(cx,240), CV_RGB(255, 0, 0) );
	}
		
	// If we got a good board, add it to our data	
	if(no_found_corners == board_total) 
	{
		cvShowImage( "Snapshot", image );        
		CvPoint2D32f* corner_ptr = corners;
		int idx(0);
		for( int j(0); j<board_h; j++ )
		{
			for( int i(0); i<board_w; i++ )
			{
//				printf("(%03.2f,%03.2f) ", corner_ptr->x, corner_ptr->y );
				points[i].x = corner_ptr->x;
				points[i].y = corner_ptr->y;
								
				CV_MAT_ELEM(*image_points, double,idx,0) = corner_ptr->x;
				CV_MAT_ELEM(*image_points, double,idx,1) = corner_ptr->y;
				
				CV_MAT_ELEM(*object_points,double,idx,0) = cell_size * (double)(i-board_w/2);
				CV_MAT_ELEM(*object_points,double,idx,1) = cell_size * (double)(j-board_h/2);
				CV_MAT_ELEM(*object_points,double,idx,2) = 0.0f;	

				corner_ptr ++;
				idx++;
			}
			double fitting_error = fitter.fit( points, board_w );
			fit_error += fitting_error;
			if( verbose )
			{
				printf("--> FIT error[%3.3f] angle[%3.6f]\n", fitting_error, 180.0 - fitter.getAngle()*180/3.141592 );
			}
			angles[j] = fitter.getAngle();			
			projected_x += cos( angles[j] );
			projected_y += sin( angles[j] );
			// evaluate the angles here			
		}
	}
	else
	{
		//cvShowImage( "Snapshot", gray_image );          
		cvShowImage( "Snapshot", image );
    if( verbose )
    {
      printf(" invalid corners \n");
    }
	}

	projected_x /= (static_cast<double>(board_h));
	projected_y /= (static_cast<double>(board_h));
//	printf("Average angle of each lines = %3.6f \n", 180- 180/3.141592*atan2( projected_y, projected_x ) );

	// Performs the cvFindExtrinsicCameraParam2()
	cvFindExtrinsicCameraParams2( object_points, image_points, &A, 0, rot_vects, trans_vects );
	if( verbose )
	{
		printf("Tran: %f, %f, %f \n", 
			180/3.141592*CV_MAT_ELEM(*trans_vects, double,0,0), 
			180/3.141592*CV_MAT_ELEM(*trans_vects, double,0,1), 
			180/3.141592*CV_MAT_ELEM(*trans_vects, double,0,2) );
		printf("Rot : %3.6f, %3.6f, %3.6f \n", 
			180/3.141592*CV_MAT_ELEM(*rot_vects,   double,0,0),   
			180/3.141592*CV_MAT_ELEM(*rot_vects, double,0,1), 
			180/3.141592*CV_MAT_ELEM(*rot_vects, double,0,2) );
	}

	estiamted_heading_angle 
		= estimatedHeadingAngle 
		= 180/3.141592*CV_MAT_ELEM(*rot_vects, double,0,2);

  if (verbose)
  {
    printf("Estimated heading angle is %f \n", estimatedHeadingAngle );
  }

	// Re-projection the object points on the image
	double reprojection_err(0);
	cvProjectPoints2( object_points, rot_vects, trans_vects,
					&A, 0, img_points2,
					0, 0, 0, 0, 0 );
	for( int i(0); i<board_total; i++ )
	{
		int u = CV_MAT_ELEM(*img_points2, double,i,0);	
		int v = CV_MAT_ELEM(*img_points2, double,i,1);
		cvCircle( image_reprojection, cvPoint(u,v), 1, cvScalar(0, 0, 255, 0.5), -1, 8, 0);

		u = CV_MAT_ELEM(*image_points, double,i,0);	
		v = CV_MAT_ELEM(*image_points, double,i,1);
		cvCircle( image_reprojection, cvPoint(u,v), 1, cvScalar(0, 255, 0, 0.5), -1, 8, 0);

		double err_x = CV_MAT_ELEM(*img_points2, double,i,0) - CV_MAT_ELEM(*image_points, double,i,0);
		double err_y = CV_MAT_ELEM(*img_points2, double,i,1) - CV_MAT_ELEM(*image_points, double,i,1);
		reprojection_err += sqrt(err_x*err_x+err_y*err_y);
	}

	cvShowImage( "Reprojection", image_reprojection );     
  average_reprojection_error = reprojection_err/static_cast<double>(board_total);

  if (verbose)
  {
    printf("ERROR: Reprojection[%1.4f [pixels]], Fit[%1.4f] \n", average_reprojection_error, fit_error );
  }

	// release
	cvWaitKey(20);
	cvReleaseImage( &image );
	cvReleaseImage( &image_reprojection );
	cvReleaseImage( &gray_image );
	cvReleaseMat(&rot_vects);
	cvReleaseMat(&trans_vects);
	cvReleaseMat(&object_points);
	cvReleaseMat(&image_points);
	cvReleaseMat(&img_points2);

	delete [] corners;
	delete [] points;
	delete [] angles;
	
	return true;
}
