#ifndef GYRO_TESTER_HPP_
#define GYRO_TESTER_HPP_

#include "image_util.hpp"
//#include <ecl_lite_ready/imath/imath.hpp>
#include "../3rdparty/ecl_lite_ready/ecl_lite_ready/imath/imath.hpp"

#define NO_TURN		4
class gyroTester
{
public:
	enum gtStatus
	{
		gt_Standby		= 0,
		gt_StepBack		= 1,
		gt_TakePhoto0,
		gt_TurnLeft,
		gt_TakePhoto1,
		gt_TurnRight,
		gt_TakePhoto2,
		gt_Evaluate,

		gt_Size

	}state;

	gyroTester() : state(gt_Standby), time(GetTickCount()),
    image_0(0), image_1(0), image_2(0), motion_id(-1),
    request_image(false), enable_log_result(false),
		turning_left(0.0), turning_right(0.0), turning_origin(0.0), nGyroRetry(3)
	{
	}

	~gyroTester()
	{
		if( image_0 ) cvReleaseImage( &image_0 );
		if( image_1 ) cvReleaseImage( &image_1 );
		if( image_2 ) cvReleaseImage( &image_2 );
	}

  void enableLogging(bool enable)
  {
    enable_log_result = enable;
  }

 /*
	void pleaseRun()
	{
    if (enable_log_result)
    {
      printf(">>> Start the gyro tester \n");
    }

		state = gt_StepBack;
		time = GetTickCount();
		motion_id = 1003;
		motion_data = 130;
		motion_speed = 160;

		if( image_0 ) cvReleaseImage( &image_0 );
		if( image_1 ) cvReleaseImage( &image_1 );
		if( image_2 ) cvReleaseImage( &image_2 );
	}
*/

  void pleaseRun( BOOL retry = FALSE )
	{
		if (enable_log_result)
		{
		  printf(">>> Start the gyro tester \n");
		}

		if ( !retry )
		{
			state = gt_StepBack;
			time = GetTickCount();
			motion_id = 1003;
			motion_data = 130;
			motion_speed = 160;
		}
		else
		{
			state = gt_TakePhoto0;
			time = GetTickCount(); 
			request_image = true;
		}

		if( image_0 ) cvReleaseImage( &image_0 );
		if( image_1 ) cvReleaseImage( &image_1 );
		if( image_2 ) cvReleaseImage( &image_2 );
	}

	void run()
	{
		unsigned int ctime( GetTickCount() );

		if( time+500 > ctime ) return;
		
		switch( state )
		{
		case gt_Standby:
			break;

		case gt_StepBack:
			break;

		case gt_TakePhoto0:
			break;

		case gt_TurnLeft:
			break;

		case gt_TakePhoto1:
			break;

		case gt_TurnRight:
			break;

		case gt_TakePhoto2:
			break;

		case gt_Evaluate:
			break;
		}
	}

	void youGetImage( IplImage * image, float angle, float optX, float optY, float focalLength )
	{
		time = GetTickCount();

		angle *= 0.01f;

		opt_x = optX;
		opt_y = optY;
		focal_len = focalLength;

    if (enable_log_result)
    {
      printf(">>> We got image: %f, %f, %f \n", opt_x, opt_y, focal_len );
    }

		{
 			double tmp_angle;
			chessboardDetector cd(opt_x,opt_y, focal_len);
			cd.run( image, tmp_angle );

      if (enable_log_result)
      {
        printf(">>> angle is %f \n", tmp_angle );
        printf(">>> state: %d\n", state);
      }
		}

		switch( state )
		{
		case gt_TakePhoto0:
			if( !image_0 ) image_0 = cvCloneImage( image );
			else			cvCopy( image_0, image );
			state = gt_TurnLeft;
			motion_id	= 1001;
			motion_data = 360*NO_TURN-8;
			motion_speed = 145;
			angle_0 = angle;
//			cvNamedWindow( "Image0", CV_WINDOW_AUTOSIZE );
//			cvShowImage( "Image0", image_0 );
			cvSaveImage("image_0.jpg", image_0);
//			cvWaitKey(20);
			break;
			
		case gt_TakePhoto1:
			if( !image_1 ) image_1 = cvCloneImage( image );
			else			cvCopy( image_1, image );
			state = gt_TurnRight;
			motion_id	= 1001;
			motion_data = -(360*NO_TURN-8);
			motion_speed = 145;
			angle_1 = angle;
//			cvNamedWindow( "Image1", CV_WINDOW_AUTOSIZE );
//			cvShowImage( "Image1", image_1 );
			cvSaveImage("image_1.jpg", image_1);
//			cvWaitKey(20);
			break;
			
		case gt_TakePhoto2:
			if( !image_2 ) image_2 = cvCloneImage( image );
			else			cvCopy( image_2, image );
			angle_2 = angle;
//			cvNamedWindow( "Image2", CV_WINDOW_AUTOSIZE );
//			cvShowImage( "Image2", image_2 );
			cvSaveImage("image_2.jpg", image_2);
//			cvWaitKey(20);
			evaluate();
			state = gt_Standby;
			motion_id = -1;
			break;			
		}			
	}

	void youGetMotionInfo( int motionId, int motionState )
	{
		if( state <= gt_Standby ) return;		
		int int_state( state );// 
//		switch( motionId )
//		{
//		case 1001:	// turn
//		case 1003:	// step back
//			
//			int_state ++;
//			state = static_cast<enum gtStatus>( int_state );
//			request_image = true;			
//			break;
//
//		default:
//			break;
//		}

    switch( state )
    {
    case gt_StepBack :
      if( motionId == 1003 ) { state = gt_TakePhoto0; time = GetTickCount(); request_image = true; }
      else                    printf(">>> [%d] unexpected motion id [%d] incoming gt_StepBack \n", GetTickCount(), motionId );	
      break;

    case gt_TurnLeft:
      if( motionId == 1001 ) { state = gt_TakePhoto1; time = GetTickCount(); request_image = true; }
      else                    printf(">>> [%d] unexpected motion id [%d] incoming gt_TurnLeft \n", GetTickCount(), motionId );	
      break;

    case gt_TurnRight:
      if( motionId == 1001 ) { state = gt_TakePhoto2; time = GetTickCount(); request_image = true; }
      else                    printf(">>> [%d] unexpected motion id [%d] incoming gt_TurnRight \n", GetTickCount(), motionId );	
      break;

    default:
      printf(">>> [%d] unexpected motion id [%d] incoming [%d] state \n", GetTickCount(), motionId, state );	
      break;
    }

    if (enable_log_result)
    {
      printf(">>> [%d] state in getMotionInfo is %d \n", GetTickCount(), state );	
    }
	}

	void getMotion( int & motionId, int & motionData, int & motionSpeed )
	{
		motionId	= motion_id;
		motionData	= motion_data;
		motionSpeed = motion_speed;
		motion_id = -1;
	}

	bool shouldITakePhoto()
	{
		bool ret( request_image );
		request_image = false;
		return ret;
	}

	//void evaluate()
//	{
//		dWrapAngle<double,180> angle_wrapper;
//		chessboardDetector cd(opt_x,opt_y, focal_len);
//
//		double r_angle_0;
//		double r_angle_1;
//		double r_angle_2;
//
//		double angle_l( angle_wrapper(angle_1 - angle_0) );
//		double angle_r( angle_wrapper(angle_2 - angle_1) );
//		
//		cd.run( image_0, r_angle_0 );
//		cd.run( image_1, r_angle_1 );
//		cd.run( image_2, r_angle_2 );
//
//		// cd returns opposite angle 
//		r_angle_0 *= -1;
//		r_angle_1 *= -1;
//		r_angle_2 *= -1;
//
//		double r_angle_l( angle_wrapper(r_angle_1 - r_angle_0) );
//		double r_angle_r( angle_wrapper(r_angle_2 - r_angle_1) );
//
//		printf("Angle information \n");
//		printf("Gyro output: %f, %f, %f \n", angle_0, angle_1, angle_2 );
//		printf("Estimated  : %f, %f, %f \n", r_angle_0, r_angle_1, r_angle_2 );
//		printf("Angle Difference\n");
//
//		turning_left	= angle_wrapper(angle_l-r_angle_l);
//		turning_right	= angle_wrapper(angle_r-r_angle_r);
//		printf("TurningLeft : %f = %f - %f \n", turning_left, angle_l, r_angle_l );
//		printf("TurningRight: %f = %f - %f \n", turning_right, angle_r, r_angle_r );
//
//		// Estiamtion of ratio
//		double rotation_angle( NO_TURN * 360.0 );
//		double left_turning_angle( rotation_angle + angle_l );
//		double right_turning_angle( -rotation_angle + angle_r );
//		double r_left_turning_angle( rotation_angle + r_angle_l );
//		double r_right_turning_angle( -rotation_angle + r_angle_r );
//		printf("Error ratio \n");
//		printf("rotaion angles: %f, %f \n", left_turning_angle, right_turning_angle );		
//		printf("rotaion anglesE: %f, %f \n", r_left_turning_angle, r_right_turning_angle );		
//		printf("Left : %lf \n", r_left_turning_angle / left_turning_angle );
//		printf("Right: %lf \n", r_right_turning_angle / right_turning_angle );
//	}



void evaluate()
{
	dWrapAngle<double> angle_wrapper;
	chessboardDetector cd(opt_x,opt_y, focal_len);
	
	double r_angle_0;
	double r_angle_1;
	double r_angle_2;
	
	double angle_l( angle_wrapper(angle_1 - angle_0, 180, 360) );
	double angle_r( angle_wrapper(angle_2 - angle_1, 180, 360) );
	double angle_c( angle_wrapper(angle_2 - angle_0, 180, 360) );
	
	cd.run( image_0, r_angle_0 );
	cd.run( image_1, r_angle_1 );
	cd.run( image_2, r_angle_2 );
	
	// cd returns opposite angle 
	r_angle_0 *= -1;
	r_angle_1 *= -1;
	r_angle_2 *= -1;
	
	double r_angle_l( angle_wrapper(r_angle_1 - r_angle_0, 180, 360) );
	double r_angle_r( angle_wrapper(r_angle_2 - r_angle_1, 180, 360) );
	double r_angle_c( angle_wrapper(r_angle_2 - r_angle_0, 180, 360) );
	
  if (enable_log_result)
  {
    printf(">>> Angle information \n");
    printf(">>> Gyro output: %f, %f, %f \n", angle_0, angle_1, angle_2 );
    printf(">>> Estimated  : %f, %f, %f \n", r_angle_0, r_angle_1, r_angle_2 );
    printf(">>> Angle Difference\n");
  }
	
	turning_left			= angle_wrapper(angle_l-r_angle_l, 180, 360);
	turning_right			= angle_wrapper(angle_r-r_angle_r, 180, 360);
	turning_origin			= angle_wrapper(angle_c-r_angle_c, 180, 360);

  if (enable_log_result)
  {
    printf(">>> TurningLeft		: %f = %f - %f \n", turning_left,	angle_l, r_angle_l );
    printf(">>> TurningRight	: %f = %f - %f \n", turning_right,	angle_r, r_angle_r );
    printf(">>> TurningOrigin	: %f = %f - %f \n", turning_origin, angle_c, r_angle_c );
  }
	
	// Estiamtion of ratio
	double rotation_angle( NO_TURN * 360.0 );
	double left_turning_angle( rotation_angle + angle_l );
	double right_turning_angle( -rotation_angle + angle_r );
	double r_left_turning_angle( rotation_angle + r_angle_l );
	double r_right_turning_angle( -rotation_angle + r_angle_r );

  if (enable_log_result)
  {
    printf(">>> Error ratio \n");
    printf(">>> rotaion angles: %f, %f \n", left_turning_angle, right_turning_angle );		
    printf(">>> rotaion anglesE: %f, %f \n", r_left_turning_angle, r_right_turning_angle );		
    printf(">>> Left : %lf \n", r_left_turning_angle / left_turning_angle );
    printf(">>> Right: %lf \n", r_right_turning_angle / right_turning_angle );
  }
}

public:
	unsigned int time;
	IplImage * image_0;
	IplImage * image_1;
	IplImage * image_2;
	float angle_0, angle_1, angle_2;

	int motion_id;
	int motion_data;
	int motion_speed;
	int nGyroRetry; 
	bool request_image;
	float opt_x, opt_y, focal_len;
	
	double turning_left, turning_right, turning_origin;

    bool enable_log_result;
};



#endif