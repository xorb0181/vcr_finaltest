#if !defined(_DEF_LOCALIZER_INTERFACE_)
#define _DEF_LOCALIZER_INTERFACE_

#include "vipl.h"


enum LocalizerEvent
{
	lc_eventNone      = -1, // 영상 비교를 통한 Slip 감지
	lc_eventSlipVision, // 영상 비교를 통한 Slip 감지
	lc_eventSlipIMU   , // IMU Encoder 비교를 통한 Slip 감지
	lc_eventLockIMU   ,
	lc_eventForcedMove, // 사람 손으로 인한 움직임 감지
	lc_eventRelocStart,
	lc_eventRelocEnd  ,

	lc_err_Grabber ,
	lc_err_CamParam,
	lc_err_ACC     ,
	lc_err_Gyro    ,

	lc_chk_Grabber ,
	lc_chk_CamParam,
	lc_chk_Gyro    ,
	lc_chk_ACC     ,

	lc_eventUser   = 100
};

class VLocalizerInterface
{
public:
	virtual bool Initialize(const char *iniOptCenter, const char *iniFixedParam, bool bUseGyro) = 0;
	virtual void Finalize() = 0;

public:
	virtual f3DPoint Localize(float mmDistance, float degAngle, bool bStoped) = 0;

public:
	virtual void SetPose(const f3DPoint &pose) = 0;
	virtual void Reset(const f3DPoint &pose = f3DPoint(0, 0, 0)) = 0;
	virtual void SystemOn(bool bTrue) = 0;
	virtual void AutoCalibration(bool bTrue) {}

	virtual bool GetEvent(LocalizerEvent &event, int &param1, int &param2) = 0;
	virtual void SetParameter(const char *strName) {}

	virtual void SetMPToolInfo(const char *numBuild, const char *YMD, const char *Model) {}
	virtual void SetOptCenterOffset(const fPoint &offset) {}
	virtual void GetCurLmkInfo(f3DPoint *arrPos, int &count) {};

	virtual int  GetBuildNumber() {};

public: // for debug
	virtual void StreamWrite(std::ostream &ios) {};
	virtual void StreamRead (std::istream &ios) {};
	virtual bool InitLocalizerServer(int portImg = 30001, int portMap = 30002, int portRecord = 30003) { return false; };

public:
	virtual void SetCallBackOnEventIMU(void *pVoid, void (*cbGyroRecv)(void *, void *)) {}
	virtual void SetCallBackOnEventConsole(void *pVoid, void (*cbRecvConsole)(void *, char *[], int )) {}
	virtual void UpdateGyro(float degAngle) {}

	virtual bool SaveMap(const char *filename) { return false; }
	virtual bool LoadMap(const char *filename) { return false; }

public:
	virtual int cmdDebug(int cmd, int param1 = 0, int param2 = 0)  { return 0; };
	virtual void GetImage(BYTE *dst, int step, int width, int height) {}

};

#endif