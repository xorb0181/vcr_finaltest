/**
 * @file include/utilities/function_handler.hpp
 *
 * @brief Utility function loader/executor.
 *
 * Utility service class for loading/storing and executing a function.
 *
 * @author Daniel J. Stonier
 * @date March 2008
 **/
/*****************************************************************************
** Ifdefs
*****************************************************************************/

#ifndef YCL_CORE_FUNCTION_HANDLER_HPP_
#define YCL_CORE_FUNCTION_HANDLER_HPP_

/*****************************************************************************
** Includes
*****************************************************************************/

#include "void.h"

/*****************************************************************************
** Using
*****************************************************************************/


/*****************************************************************************
** Namespaces
*****************************************************************************/

namespace efl {

/*****************************************************************************
** FunctionLoader Interface
*****************************************************************************/
/**
 * @brief Abstract parent template for the function handlers.
 *
 * Virtual base implementation for defining both global and
 * member function handlers. Function handlers are a utility
 * class not intended for direct use, but convenient when used in classes
 * that need to load a worker function. The slot and timer implementations
 * in the YCL utilise function handlers.
 **/
template <typename T>
class FunctionHandler {
    public:
        FunctionHandler() {};/**< Default constructor. **/
        virtual ~FunctionHandler() {}; /**< Default destructor. **/
        /**
         * Execute the loaded function.
         * @param data : data to be passed to the loaded function.
         **/
        virtual void run (T data) = 0;
        /**
         * Clone the object. Virtual function call only.
         * @return FunctionHandler<T>* : pointer to the cloned object.
         */
        virtual FunctionHandler<T>* clone () const = 0;
};

/**
 * @brief Global/static function handler.
 *
 * Function handler for ordinary functions. This applies to both global and
 * static functions.
 **/
template <typename T>
class GlobalFunctionHandler : public FunctionHandler<T>
{
    public:
        /**
         * Constructor that loads a global or static function.
         * @param function : the global/static function to be loaded.
         **/
        GlobalFunctionHandler(void (*function)(T)) : globalFunction(function) {};
        virtual ~GlobalFunctionHandler() {}; /**< Default destructor. **/

        /**
         * Execute the loaded function.
         * @param data : data to be passed to the loaded function.
         **/
        virtual void run( T data ) { (*globalFunction)(data); };
        /**
         * Clone the object. Be sure to clean up the clone elsewhere!
         * @return GlobalFunctionHandler<T>* : pointer to the cloned object.
         */
        virtual GlobalFunctionHandler<T>* clone () const { return new GlobalFunctionHandler<T>(globalFunction); }

    private:
        void (*globalFunction)(T);
};
/**
 * @brief Member function handler.
 *
 * Function handler for class member functions. Note that this needs details of the
 * instance to which the member function belongs, hence the need for these
 * template driven implementation classes.
 **/
template <typename T, typename O>
class MemberFunctionHandler : public FunctionHandler<T>
{
    private:
        O *object;
        void (O::*memberFunction)(T);
    public:
        /**
         * Constructor that loads a class member function.
         * @param obj : the instance to which the member function belongs.
         * @param function : the member function to be loaded.
         **/
        MemberFunctionHandler(O *obj, void (O::*function)(T)) : object(obj),memberFunction(function) {};
        virtual ~MemberFunctionHandler() {}; /**< Default destructor. **/

        /**
         * Execute the loaded function.
         * @param data : data to be passed to the loaded function.
         **/
        virtual void run( T data ) { (*object.*memberFunction)(data); };
        /**
         * Clone the object. Be sure to clean up the clone elsewhere!
         * @return MemberFunctionHandler<T,O>* : pointer to the cloned object.
         */
        virtual MemberFunctionHandler<T,O>* clone () const { return new MemberFunctionHandler<T,O>(object,memberFunction);}
};

/*****************************************************************************
** Specialisation for Void (null argument functions).
*****************************************************************************/
/**
 * @brief Abstract parent template for the void function handlers.
 *
 * Virtual base specialisation for defining both global and
 * member void function handlers.
 **/
template <>
class FunctionHandler<Void> {
    public:
        FunctionHandler() {};/**< Default constructor. **/
        virtual ~FunctionHandler() {} /**< Default destructor. **/
        /**
         * Execute the loaded function.
         **/
        virtual void run () = 0;
        /**
         * Clone the object. Virtual function call only.
         * @return FunctionHandler* : pointer to the cloned object.
         */
        virtual FunctionHandler<Void>* clone () const = 0;
};

/**
 * @brief Global/static function handler.
 *
 * Function handler for ordinary functions. This applies to both global and
 * static functions.
 **/
template <>
class GlobalFunctionHandler<Void> : public FunctionHandler<Void>
{
    public:
        /**
         * Constructor that loads a global or static function.
         * @param function : the global/static function to be loaded.
         **/
        GlobalFunctionHandler(VoidFunction function) : globalFunction(function) {};
        virtual ~GlobalFunctionHandler() {}

        /**
         * Execute the loaded function.
         **/
        virtual void run() { (*globalFunction)(); };
        /**
         * Clone the object. Be sure to clean up the clone elsewhere!
         * @return GlobalFunctionHandler* : pointer to the cloned object.
         */
        virtual GlobalFunctionHandler<Void>* clone () const { return new GlobalFunctionHandler<Void>(globalFunction); }

    private:
        VoidFunction globalFunction;
};

/**
 * @brief Member function handler.
 *
 * Function handler for class member functions. Note that this needs details of the
 * instance to which the member function belongs, hence the need for these
 * template driven implementation classes.
 **/
template <typename O>
class MemberFunctionHandler<Void,O> : public FunctionHandler<Void>
{
    private:
        O *object;
        void (O::*memberFunction)();
    public:
        /**
         * Constructor that loads a class member function.
         * @param obj : the instance to which the member function belongs.
         * @param function : the member function to be loaded.
         **/
        MemberFunctionHandler(O *obj, void (O::*function)() ) : object(obj),memberFunction(function) {};

        /**
         * Execute the loaded function.
         **/
        virtual void run() { (*object.*memberFunction)(); };
        /**
         * Clone the object. Be sure to clean up the clone elsewhere!
         * @return MemberFunctionHandler<Void,O>* : pointer to the cloned object.
         */
        virtual MemberFunctionHandler<Void,O>* clone () const { return new MemberFunctionHandler<Void,O>(object,memberFunction); }
};

}; // namespace efl

#endif /*YCL_CORE_FUNCTION_HANDLER_HPP_*/
