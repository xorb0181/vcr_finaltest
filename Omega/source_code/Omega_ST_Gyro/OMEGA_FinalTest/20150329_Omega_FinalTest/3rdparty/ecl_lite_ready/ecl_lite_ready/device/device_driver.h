#ifndef DEVICE_DRIVER_HPP_
#define DEVICE_DRIVER_HPP_

class DeviceDriver
{
public:
  DeviceDriver( const int deviceId, int workingPeriod) 
  : 
  id(deviceId),
  enabled(false), 
  working_period(workingPeriod), 
  working_cnt(workingPeriod)
  {}

  virtual int run( int arg )=0;

  virtual void enable() { enabled = true; }
  virtual void disable() { enabled = false; }
  virtual bool isRunning() { return enabled; }
  virtual bool isNotRunning() { return !enabled; }

protected:
  int id;
  bool enabled;
  int working_period;
  int working_cnt;

  virtual int shouldIWork( int arg )
  {
    if( isNotRunning() )
    {
      // It would response quickly when you enable again
      working_cnt = working_period < 0 ? 0 : working_period ;
      return -1;
    }

    // 
    if( ++working_cnt < working_period ) return -2;
    working_cnt = 0;
    
    return 0;    
  }
};

#endif

