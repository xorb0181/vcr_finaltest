#ifndef DEVICE_MOTOR_HPP_
#define DEVICE_MOTOR_HPP_

#include "../common/container.h"
#include "../common/util.h"
#include "../common/function_util.h"
#include "device_controller.h"
#include "device_driver.h"

#define MOVING_CURRENT_ESTIMATION
//#define ENABLE_SINGLE_HOLE_SENSOR 	1
class hwMotor
{
public:
  hwMotor()
  {}
  virtual void operator() ( const int motorId, int & encoder, int & current ) {}

  // desiredPower: percentagex1000 of pwm ratio
  virtual void setPower( const int motorId, const int desiredPower ) {}

  virtual void cutPower() {}
  virtual void supplyPower() {}
	virtual bool canIControl() { return false; }
};


class DevMotor : public DeviceDriver
{
public:
  DevMotor( const int motorId, 
        hwMotor & motorConnector, 
        const int maxPower,
        const int reversePower )        
  :
  DeviceDriver(motorId, -1),
  motor_connector( motorConnector ),
  max_power(maxPower),
  reverse_power(reversePower),
  target(0),
  position(0),
  velocity(0),
  encoder_pre(0),
  power(0),
#ifdef MOVING_CURRENT_ESTIMATION      
  current(0),
  current_first(true),
  current_idx(0), 
  current_sum(0)  
#else
  current(0)
#endif
  {
  }  

  void setMotorPower()
  {
    // saturation
    if( power > max_power )     power = max_power;
    else if( power < -max_power )   power = -max_power;

    // est the power
    motor_connector.setPower( id, reverse_power ? -power: power );
  }

  virtual void scan()
  {
    int current_new;

    // get the feedback
    motor_connector( id, encoder_pre, current_new );

    // approach 1; just simple moving average
#ifndef MOVING_CURRENT_ESTIMATION
    current = current * 0.8 + current_new*0.2;
#else
    // approach 2; real moving average
    if( current_first )
    {
      current_first = false;
      for( int i=0; i<8; i++ )
      {
        current_buffer[i] = current_new;
      }
      current = current_new;
      current_sum = current_new * 8;
      current_idx = 0;
    }
    current_sum -= current_buffer[ current_idx ];
    current_sum += current_new;
    current_buffer[ current_idx++ ] = current_new;
    if( current_idx >= 8 ) current_idx = 0;
    current = current_sum / 8;
    
    //for( int i=0; i<8; i++ ) if( current > current_buffer[i] ) current = current_buffer[i];    
#endif
  }

  virtual int run( int arg )
  {
    scan();

    // check the enable/disable state
    if( shouldIWork(0) )
    {
      motor_connector.setPower(id, 0);
      power = 0;
      return -1;
    }

    // run controller
    power = (power+target)/2;

    // check the direction
    setMotorPower();

    return 0;
  }
  virtual void setTarget( int desiredTarget )
  {
    target = desiredTarget;
  }

  int getPosition()   { return position; }
  int getVelocity()  { return velocity; }
  int getPower()     { return power; }

//protected:
  // attributes
  hwMotor & motor_connector;
  int max_power;    // [percentage x 1000]
  bool reverse_power;

  // run-time states
  int target;
  int position;    // [encoder tick] accumulated  
  int velocity;    // [encoder tick / sample time]
  int encoder_pre;  // temporal value

  // other states
  int power;      // supplied power for motor [percentage x 1000]
  int current;    // loaded current on motor  [mA]
#ifdef MOVING_CURRENT_ESTIMATION
  bool current_first;
  int current_buffer[8];
  int current_idx;
  int current_sum;
#endif
};



// When i design this class, first target (application) is just wheel. 
// however it would be used for other motor as well.

class DevMotorControl : public DevMotor
{
public:               
  typedef enum _controlMode
  {
    VelocityControl = 0, 
    PositionControl,
    CustomControl
  }controlMode;

  DevMotorControl( int motorId,
        hwMotor & motorConnector,
        controller & controlModule, 
        int maxPower,                //[percentage x 1000]
        int signValue=14,        
        bool reverseFeedback = false, 
        bool reversePower = false,
        int samplingTime=5 )
  :                               
  DevMotor(motorId, motorConnector, maxPower, reversePower ),
  control_module( controlModule ),  
  reverse_feedback( reverseFeedback ),
  sample_time( samplingTime ),
  sign(1<<signValue),
  sign_1(1<<(signValue-1)),
  zero_velocity_command( 100, true )
  {
    motor_connector( id, encoder_pre, current );
    velocity = 0;
    scan();
  }  
	
	void reset()
	{
		control_module.reset();
	}
  
  virtual void disable()
  { 
    enabled = false; 
    control_module.reset();
    control_module(id, 0);
    target = 0;
  }      

  virtual bool isItOverCurrent()
  {
    return control_module.isItOverCurrent();
  }
  
  virtual bool isItSomethingWrong()
  {
	  return control_module.isItSomethingWrong();
  }
   
  virtual int run( int arg )
  {
    // get the feedback
    scan();

    // check the enable/disable state
    if( shouldIWork(0) )
    {
      control_module(id, 0);
      control_module.reset();
      return -1;
    }

    if( zero_velocity_command( target==0?true:false ) )
    {
      disable();      
      power = 0;
      setMotorPower();
      return 0;
    }

    // run controller
    power = control_module( target, velocity );    
    
    // check the direction
    setMotorPower();

    return 0;
  }

  virtual void scan()
  {
    int encoder(0);
    int current_new;

    // get the feedback
    motor_connector( id, encoder, current_new );
    current = current * 0.8 + current_new*0.2;
    if( reverse_feedback ) encoder = - encoder;

    // first get the velocity from the encoder value
    // this is displacement between some sampling time
    velocity = encoder - encoder_pre;

    // check rolling over
    if( velocity > sign_1 )       velocity = -sign + velocity;
    else if( velocity < -(sign_1+1) )   velocity = +sign + velocity;

//#if ENABLE_SINGLE_HOLE_SENSOR == 1
#if CONFIG_MODEL == CONFIG_ARTE_3_0	  
	if( velocity < 0 ) velocity = -velocity;
	// zero pwm??
	if( power < 0 ) velocity = -velocity;
	//else if( power == 0 ) velocity = 0;
	
	velocity *= 2;
	position += (velocity);
#else
    // integrate position 
    position += velocity;
#endif
    // backup 
    encoder_pre = encoder;

    // current 
    // read_current value

    //@todo
    // later you should put module which ensures safe motor control
    // in order to avoid burning
  }

  virtual void enableHighPower( bool setHighPowerMode )
  {
    control_module.enableHighPower( setHighPowerMode );
  }
  
protected:
  // utility functions to get/set state
  FunctionPtr<int, int>     getFeedback;   // return current robot encoder value  
  controller &         control_module;   // functor to indicate user-specific controller

  // motor properties
  bool reverse_feedback;
  int sample_time;  // [msec]  
  int sign;      // 
  int sign_1;      //
  repeatNTimes<bool> zero_velocity_command;     
};

  

#endif

