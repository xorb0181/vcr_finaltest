#ifndef DEVICE_CONTROLLER_HPP_
#define DEVICE_CONTROLLER_HPP_

#include "../common/container.h"
#include "../common/util.h"


class controller
{
public:
  controller() {}
  // simply i just suppose, it return output 0%x1000 ~ 100%x1000
  virtual int operator() ( const int & target, const int & current ) = 0;
  virtual void reset()=0;
  virtual void enableHighPower( bool setHighPowerMode )=0;
  virtual bool isItOverCurrent()=0;
  virtual bool isItSomethingWrong() = 0;
};



#endif
