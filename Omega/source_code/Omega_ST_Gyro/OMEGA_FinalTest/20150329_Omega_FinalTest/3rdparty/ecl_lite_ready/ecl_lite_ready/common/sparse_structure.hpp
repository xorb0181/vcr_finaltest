#pragma warning(disable: 4786)
#ifndef SPARSE_STRUCTURE_HPP_
#define SPARSE_STRUCTURE_HPP_

//#include <cstdlib>
#include <string>
//#include <iostream>
#include <fstream>
//#include <ostream>
#include <sstream>
#include <map>
#include <vector>
#include <bitset>
template <typename T, typename Index=unsigned int>
class SparseStructure
{
public:
    typedef std::map<Index, std::map<Index , T> > storage_t;
    typedef typename storage_t::iterator row_iter;
    typedef std::map<Index, T> col_t;
    typedef typename col_t::iterator col_iter;
  
  SparseStructure() : num_element(0), verbose(true), mx(INT_MAX), Mx(-INT_MAX), my(INT_MAX), My(-INT_MAX) {}

  void clear()
  {
    storage.clear();
  }
  int size()
  {
    return num_element;
    return storage.size();
  }

  inline T & operator()(Index i, Index j)
    {
       if( i < mx ) mx = i;
    else if( i > Mx ) Mx = i;
       if( j < my ) my = j;
    else if( j > My ) My = j;
    if( !isThere( i, j ) )
    {
      num_element ++;
//       printf("[SS|Add|%d,%d][%d,%d,%d,%d][%d]\n", i, j, mx, Mx, my, My, num_element);
    }
//    if( (num_element%50) == 0 )
//    {
//      static int log_counter(0);
//      std::ostringstream file_name;
//      file_name << "sparse_" << log_counter++ << ".pgm";
//      to_pgm( file_name.str() );
//      std::cout << file_name << " file was generated " << std::endl;
//    }
        return storage[i][j];
    }

  bool isThere( const Index & i, const Index & j )
  {
    typename std::map<Index, std::map<Index , T> >::const_iterator ri = storage.find( i );
    if( ri == storage.end() )
    {
      return false;
    }
    else
    {
      typename std::map<Index, T >::const_iterator ci = ri->second.find( j );
      if( ci == ri->second.end() )
      {
        return false;
      }
      else
      {
        return true;
      }
    }
  }

    inline T operator()(Index i, Index j) const
    {
    T tmp(0);
    get(i,j,tmp);
    return tmp;
    }

  inline bool get( const Index & i, const Index & j, T & value ) const
  {
//    if( verbose ) printf("[SS|Search|%d,%d]\n", i, j);
    // of course, we can use the [] operator to get the data, 
    // however [] operator will insert data at (i,j) point
    // so that i put below search logic
    typename std::map<Index, std::map<Index , T> >::const_iterator ri = storage.find( i );
    if( ri == storage.end() )
    {
      return false;
    }
    else
    {
      typename std::map<Index, T >::const_iterator ci = ri->second.find( j );
      if( ci == ri->second.end() )
      {
        return false;
      }
      else
      {
        value = ci->second;
        return true;
      }
    }    
  }

  void erase( const T & value )
  {
    
    row_iter ii;
        col_iter jj;
    std::vector<row_iter> target_row_iterator;
        for(ii=this->storage.begin(); ii!=this->storage.end(); ii++)
    {
      std::vector<col_iter> target_iterators;
            for( jj=(*ii).second.begin(); jj!=(*ii).second.end(); jj++)
      {
        if( jj->second == value )
          target_iterators.push_back( jj );
            }

      for( int i=0; i<target_iterators.size(); i++ )
        ii->second.erase( target_iterators[i] );

      if( ii->second.size() == 0 ) target_row_iterator.push_back( ii );
        } 

    for( int i(0); i<target_row_iterator.size(); i++ )
      storage.erase( target_row_iterator[i] );    
  }
        
    void printMat()
    {
        row_iter ii;
        col_iter jj;

        for(ii=this->storage.begin(); ii!=this->storage.end(); ii++)
    {
            for( jj=(*ii).second.begin(); jj!=(*ii).second.end(); jj++)
      {
                std::cout << (*ii).first << ' ';
                std::cout << (*jj).first << ' ';
                std::cout << (*jj).second << std::endl;
            }
        } 

    std::cout << std::endl;
    }

  bool to_pgm( std::string & fileName )
  {
    std::ofstream fout( fileName.c_str(), std::ios::out );
    int nx( Mx-mx + 1 );
    int ny( My-my + 1 );
    
    fout << "P2" << " ";
    fout << nx << " ";
    fout << ny << " ";
    fout << 255 << " ";
    fout << "\n";
    
    for( int j(my); j<=My; j++ )
    {
      for( int i(mx); i<=Mx; i++ )
      {
        T value;
        if( get(i,j,value) )  fout << 255 << '\t';
        else          fout << 0  << '\t';
      }
      fout << "\n";
    }
    
    fout.close();
    
    return true;
  }    
protected:
  
private:    
  int num_element;
  bool verbose;
  int mx, Mx, my, My;
  storage_t storage;
};



#ifdef WIN32
// funny magic
// if you remove below class, it cause the compile error on windoz
class ABCd {public:  ABCd() {}};
#endif


template<typename T=unsigned int, int _BSize=5>
class cellBlock
{
public:
  cellBlock()
  {
    clear();
  }
  void clear()
  {
    int num_ele( _BSize * _BSize / sizeof(T) );
    for( int i(0); i<num_ele; i++ )
      block[i] = 0;
  }
  void set( int idx, bool targetValue = true )
  {
    int hidx = idx / sizeof(T);
    int lidx = idx % sizeof(T);
    unsigned int mask = (1 << lidx);
    if( targetValue )  block[ hidx ] |= mask;
    else        block[ hidx ] &= (~mask);
  }

  bool test( int idx )
  {
    int hidx = idx / sizeof(T);
    int lidx = idx % sizeof(T);
    unsigned int mask = (1 << lidx);
    return block[ hidx ] & mask ? true : false;
  }
public:
  T block[ (_BSize * _BSize / sizeof(T)+1) ];
};


template<typename T=unsigned int , int _BSize=5>
class sparseGridMap
{
public:
  typedef std::bitset<_BSize*_BSize> cellValue;

  sparseGridMap() : offset_x(0), offset_y(0), cell_size(20), 
            block_size(_BSize), half_block_size(block_size/2), cell_size_high( cell_size * block_size ), 
            mx(INT_MAX), Mx(-INT_MAX), my(INT_MAX), My(-INT_MAX)
  {}

  inline int getCellSize()  { return cell_size; }
  inline void mm2c( const int mmX, const int mmY, int & cellX, int & cellY )
  {
    cellX = int( ((float)(mmX-offset_x) / (float)(cell_size) + (mmX>0?0.5f:-0.5f)) );
    cellY = int( ((float)(mmY-offset_y) / (float)(cell_size) + (mmY>0?0.5f:-0.5f)) );
  }

  inline void mm2c_high( const int mmX, const int mmY, int & cellX, int & cellY )
  {
    cellX = int( ((float)(mmX-offset_x) / (float)(cell_size_high) + (mmX>0?0.5f:-0.5f)) );
    cellY = int( ((float)(mmY-offset_y) / (float)(cell_size_high) + (mmY>0?0.5f:-0.5f)) );
  }
  
  inline void c2mm( const int cellX, const int cellY, int & mmX, int & mmY )
  {
    mmX = cellX*cell_size+offset_x;
    mmY = cellY*cell_size+offset_y;
  }
  inline void c2mm_high( const int cellX, const int cellY, int & mmX, int & mmY )
  {
    mmX = cellX*cell_size_high+offset_x;
    mmY = cellY*cell_size_high+offset_y;
  }
  
  void setOffset( const int offsetX, const int offsetY )
  {
    offset_x = offsetX;
    offset_y = offsetY;
  }

  void setObstacle( const int mmX, const int mmY, bool obstacleIs=true )
  {
    int cx, cy;
    int cxh, cyh;
    int mmx, mmy;
    mm2c   ( mmX, mmY, cx,  cy );
    mm2c_high( mmX, mmY, cxh, cyh );
    c2mm_high( cxh, cyh, mmx, mmy );
    mm2c   ( mmx, mmy, cxh, cyh );

    cellValue & cell = grid_map( static_cast<signed short int>(cxh), 
                   static_cast<signed short int>(cyh) );

    int dx = cx-cxh+half_block_size;
    int dy = cy-cyh+half_block_size;
    cell.set( dx+dy*block_size, obstacleIs );
    
       if( cx < mx ) mx = cx;
    else if( cx > Mx ) Mx = cx;
       if( cy < my ) my = cy;
    else if( cy > My ) My = cy;

//    {
//      static int add_counter(0);
//      if( ((++add_counter) % 50) == 0 )
//      {
//        static int log_counter(0);
//        std::ostringstream file_name;
//        file_name << "sparse_" << log_counter++ << ".pgm";
//        to_pgm( file_name.str() );
//        std::cout << file_name << " file was generated " << std::endl;
//      }
//    }
  }

  bool getValue( const int cellX, const int cellY )
  {
    int cxh, cyh;
    int mmx, mmy;

    c2mm   ( cellX, cellY, mmx, mmy );
    mm2c_high( mmx, mmy, cxh, cyh );

    c2mm_high( cxh, cyh, mmx, mmy );
    mm2c   ( mmx, mmy, cxh, cyh );
    
    cellValue cell;
    if( !grid_map.get( cxh, cyh, cell ) ) return false;

    int dx = cellX-cxh+half_block_size;
    int dy = cellY-cyh+half_block_size;
    return cell.test( dx+dy*block_size );
  }

  bool to_pgm( std::string & fileName )
  {
    std::ofstream fout( fileName.c_str(), std::ios::out );
    int nx( Mx-mx + 1 );
    int ny( My-my + 1 );
    
    fout << "P2" << " ";
    fout << nx << " ";
    fout << ny << " ";
    fout << 255 << " ";
    fout << "\n";
    
    for( int j(my); j<=My; j++ )
    {
      for( int i(mx); i<=Mx; i++ )
      {
        if( getValue(i,j) )  fout << 255 << '\t';
        else        fout << 0  << '\t';
      }
      fout << "\n";
    }
    
    fout.close();
    
    return true;
  }  

  unsigned char findWall( const int mmX, const int mmY, const int wallLength )
  {
    int cell_x;
    int cell_y;
    
    int wall_length_cell( wallLength / cell_size );
    int half_wall_length_cell( wall_length_cell / 2 );
    
    mm2c( mmX, mmY, cell_x, cell_y );
    
    // search
    int nobs[4] = {0, 0, 0, 0};
    for( int i(-half_wall_length_cell); i<= half_wall_length_cell; i++ )
    {
      nobs[0] += ( getValue(cell_x+half_wall_length_cell,cell_y+i) ? 1 : 0 );
      nobs[1] += ( getValue(cell_x+i,cell_y+half_wall_length_cell) ? 1 : 0 );
      nobs[2] += ( getValue(cell_x-half_wall_length_cell,cell_y+i) ? 1 : 0 );
      nobs[3] += ( getValue(cell_x+i,cell_y-half_wall_length_cell) ? 1 : 0 );
    }
    
    // check wall
    unsigned char wall_mask(0);
    for( int j(0); j<4; j++ )
    {
      //@todo; below thresholding should be parameterised
      if( nobs[j] > 2 ) wall_mask |= (1<<j);      
    }
    
    return wall_mask;
  }
  
public:
  int offset_x;
  int offset_y;
  int cell_size;
  int block_size;
  int half_block_size;
  int cell_size_high;
  int mx, Mx, my, My;
  SparseStructure< cellValue,signed short int> grid_map;
};

class fineGridMap
{
public:
  fineGridMap() : offset_x(0), offset_y(0), cell_size(20) {}

  inline 
  void mm2c( const int mmX, const int mmY, int & cellX, int & cellY )
  {
    cellX = int( ((float)(mmX-offset_x) / (float)(cell_size) + (mmX>0?0.5f:-0.5f)) );
    cellY = int( ((float)(mmY-offset_y) / (float)(cell_size) + (mmY>0?0.5f:-0.5f)) );
  }
  
  inline
  void c2mm( const int cellX, const int cellY, int & mmX, int & mmY )
  {
    mmX = cellX*cell_size+offset_x;
    mmY = cellY*cell_size+offset_y;
  }
  
  void setOffset( const int offsetX, const int offsetY )
  {
    offset_x = offsetX;
    offset_y = offsetY;
  }
  
  void setObstacle( const int mmX, const int mmY, bool obstacleIs=true )
  {
    int cell_x;
    int cell_y;
    mm2c( mmX, mmY, cell_x, cell_y );
    grid_map( static_cast<signed short int>(cell_x), 
          static_cast<signed short int>(cell_y) ) = obstacleIs;  
  }

  // @return :  0; nothing around mmX, mmY
  //        1; east
  //        2; north
  //        4; west
  //        8; south
  unsigned char findWall( const int mmX, const int mmY, const int wallLength )
  {
    int cell_x;
    int cell_y;

    int wall_length_cell( wallLength / cell_size );
    int half_wall_length_cell( wall_length_cell / 2 );
        
    mm2c( mmX, mmY, cell_x, cell_y );

    // search
    int nobs[4] = {0, 0, 0, 0};
    const SparseStructure<bool,signed short int> & gmap_ref = grid_map;
    for( int i(-half_wall_length_cell); i<= half_wall_length_cell; i++ )
    {
      nobs[0] += ( gmap_ref(cell_x+half_wall_length_cell,cell_y+i) ? 1 : 0 );
      nobs[1] += ( gmap_ref(cell_x+i,cell_y+half_wall_length_cell) ? 1 : 0 );
      nobs[2] += ( gmap_ref(cell_x-half_wall_length_cell,cell_y+i) ? 1 : 0 );
      nobs[3] += ( gmap_ref(cell_x+i,cell_y-half_wall_length_cell) ? 1 : 0 );
    }

//    printf("[%d|%d|%d|%d]", nobs[0],nobs[1],nobs[2],nobs[3]);

    // check wall
    unsigned char wall_mask(0);
    for( int j(0); j<4; j++ )
    {
      //@todo; below thresholding should be parameterised
      if( nobs[j] > 3 ) wall_mask |= (1<<j);      
    }

    return wall_mask;
  }

public:
  int offset_x;
  int offset_y;
  int cell_size;
  SparseStructure<bool,signed short int> grid_map;
};

#endif //SPARSE_STRUCTURE_HPP_
