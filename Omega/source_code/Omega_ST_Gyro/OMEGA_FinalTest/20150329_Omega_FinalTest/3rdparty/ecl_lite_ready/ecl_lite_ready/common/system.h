#ifndef SYSTEM_HPP_
#define SYSTEM_HPP_

#include "util.h"



class System
{
public:
  typedef FunctionPtr<int,int>::proc systemFunctionPtr;
  System();
  System( systemFunctionPtr initMPUfptr, systemFunctionPtr setDefaultPowerfptr, systemFunctionPtr disableInterruptfptr ) 
  :fptr_initMPU(initMPUfptr),
  fptr_setDefaultPower(setDefaultPowerfptr),
  fptr_disableInterrupt(disableInterruptfptr)
    
  {
    // sometime sequence of initialisation of system is important    
    // consequently, generalisation of sequence.
    // please put every thing (clock, timer, communications) inside initMPU();
  }
  virtual ~System()
  {
  }

public:
  virtual int runSystem( const int timeStampInMillisecond ) { return 0; }
  
protected:
  FunctionPtr<int,int> fptr_initMPU;
  FunctionPtr<int,int> fptr_setDefaultPower;
  FunctionPtr<int,int> fptr_disableInterrupt;
};

#endif

