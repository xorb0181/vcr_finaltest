#ifndef VIP_MATRIX_H
#define VIP_MATRIX_H

#include "ViplBase.h"

/*------------------------------------------------------------------*/
// Header Include
/*------------------------------------------------------------------*/
#ifndef _WINDOWS
	#include <stdarg.h>
	#include <memory.h>
#endif

#include "VipGlobal.h"
#include "VipCore.h"
#include "VipGlobalFunc.h"

#include "VipDebug.h"
#include "VipArray.h"
#include "NumericalRecipes.h"
#include <math.h>

/*------------------------------------------------------------------*/
/**				Main Data Structure									*/
/*------------------------------------------------------------------*/

struct tMatrixData
{
	char  *	 pMat;
	int      widthStep;

	int		 nCol; // width
	int		 nRow; // height

	void **	ppMat;
	long	 nRef;
};
#define Mat2VipPtr(src) *((Vip2DPtr<T> *)(src))
/*------------------------------------------------------------------*/
// Global Variable
/*------------------------------------------------------------------*/
extern VIPLIB_API tMatrixData MatrixDataStruct;
extern VIPLIB_API tMatrixData * _InitPointerMatrix;

template <class T> class CMatrix;

#ifdef _LINUX
	typedef long long __int64;
#endif

/*------------------------------------------------------------------*/
///@name Type define
/*------------------------------------------------------------------*/
//@{
	///	        Double Matrix
	typedef CMatrix < double>	DoubleMatrix;
	///          Float Matrix
	typedef CMatrix <  float>	 FloatMatrix;
	///        Integer Matrix
	typedef CMatrix <    int>	   IntMatrix;

	///        64bit Integer Matrix
	typedef CMatrix <__int64>	 Int64Matrix;

	///@memo Classes of Matrix Array
	//@{
		/// ByteImage Array
		typedef CVipArray <DoubleMatrix > DoubleMatrixArray;
		/// IntImage Array
		typedef CVipArray <FloatMatrix  >  FloatMatrixArray;
		/// FloatImage Array
		typedef CVipArray <IntMatrix    >    IntMatrixArray;
	//@}
//@}

/*------------------------------------------------------------------*/
// Global Function & Macro
/*------------------------------------------------------------------*/

VIPLIB_API int MatrixLockedIncrement(long *ref);
VIPLIB_API int MatrixLockedDecrement(long *ref);

#define READ_MAT_POINTER(matrix, r, c) (((T **)(matrix.m_pData)->ppMat)[r][c])
#define CHECK_1X1(matrix) ( matrix.GetColSize() == 1 && matrix.GetRowSize() == 1 )

#define _MAT1_    pOp1[i]
#define _MAT2_    pOp2[i]
#define _OUTMAT_  pOut[i]
#define _SCALAR_  Scalar

#define MATRIX_LOOP(MatA, MatB, DST, OPERATION)							\
{																		\
	int size =               ((CMatrix <T> *) MatA)->GetSize  ()    ;	\
	T * pOp1 =          (T *)((CMatrix <T> *) MatA)->GetBuffer()    ;	\
	T * pOp2 = (MatB) ? (T *)((CMatrix <T> *) MatB)->GetBuffer() : 0;	\
	T * pOut = (DST ) ? (T *)((CMatrix <T> *) DST )->GetBuffer() : 0;	\
	for (int i = 0 ; i < size ; i++)									\
	{ OPERATION; }														\
}

#define CHECK_CLONE {if (m_pData->nRef > 1) _CopyBeforeWrite();}

/*------------------------------------------------------------------*/
/// Template Matrix Class
/*------------------------------------------------------------------*/
template <class T> class CMatrix
{
protected:
	///
	tMatrixData  *m_pData;
	/// For Nemerical Recipes Function : MATRIX[Row + 1][Col + 1]
	T           **m_ppMat;
	T			**m_ppNRCPointer;

public:
	/*--------------------------------------------------------------*/
	///@name	Construction/Destruction
	/*--------------------------------------------------------------*/
	//@{
	CMatrix()									{ _Initialize();							}
	///
	CMatrix(const CMatrix <T> & Src)			{ _Initialize(); _Copy(Src);				}
	///
	explicit CMatrix(int nRow, int nCol = 1, bool bFillZero = true)	{ _Initialize(); Allocate(nRow, nCol, bFillZero);	}
	///
	explicit CMatrix(const char *szInput)		{ _Initialize(); Allocate(szInput);		}
    ~CMatrix();
	//@}

public:
	/*--------------------------------------------------------------*/
	///@name	Memory Alloc / Dealloc
	/*--------------------------------------------------------------*/
	//@{
	///
	void Allocate(int nRow, int nCol = 1, bool bFillZero = true)	// Allocate By Size
	{
		if (GetRowSize() != nRow || GetColSize() != nCol)
		{
			_Release();
			_AllocateBuffer(nRow, nCol);
		}
		if (bFillZero) 
			memset(m_pData->pMat, 0, GetBufferSize());
		
		VIP_ASSERT(m_pData->nRef <= 1, "", "");
	}

	void Reallocate(int nRow, int nCol, bool bFillZero = true)
	{
		if ( !nRow || !nCol)
		{
			_Release();
			_AllocateBuffer(nRow, nCol);
			if (bFillZero) Assign(0);
		}
		else
		{
			CMatrix <T> tempMat(nRow, nCol, bFillZero);
			int row = __min(GetRowSize(), nRow);
			int col = __min(GetColSize(), nCol);

			vipCopy(
				        GetBufferConst(),         GetWidthStep(),
				tempMat.GetBuffer     (), tempMat.GetWidthStep(),
				col, row);
			(*this) = tempMat;
		}
	}

	///
	void Allocate(const char * szInput)		// Allocate From String Data
	{
		int strLen = strlen(szInput);
		char *buffer = new char[strLen + 1]; strcpy(buffer, szInput);

		int RowSize, ColSize;
		char ***strMatrix = StringToMatrix(buffer, RowSize, ColSize, ";", " \t");
		if(strMatrix)
		{
			Allocate(RowSize, ColSize, false);

			T **pPixel = (T **)m_ppMat;
			for (int r = 0 ; r < RowSize ; r++)
			{
				for (int c = 0 ; c < ColSize ; c++)
					pPixel[r][c] = atof(strMatrix[r][c]);
			}

			DeleteStringMatrix(strMatrix, RowSize, ColSize);
		}
		else
			VIP_ASSERT(0, "Allocate", "Column Size's are not same each other");

		if(buffer)	delete [] buffer;
	};

	void _CopyBeforeWrite() const;

	bool Attach(T *pInput, int nRowSize, int nColSize)
	{
		_Release();
		tMatrixData* pData = new tMatrixData;
		pData->pMat  = (char*)pInput;
		pData->ppMat = (void **) (m_ppMat = _AllocatePointer(nRowSize, nColSize, (T *)(pData->pMat)));
		pData->nRow  = nRowSize;
		pData->nCol  = nColSize;
		pData->nRef  = 1;
		m_pData = pData;
		return true;
	}

	T * Detach()
	{
		VIP_ASSERT(!IsEmpty(), "Detach", "Matrix Size is zero");
		_CopyBeforeWrite();
		T *ret = (T*)m_pData->pMat;
		delete [] m_ppMat;
		delete m_pData;
		_InitData();
		return ret;
	}

	//@}

protected:
	void _Initialize();
	void _InitData();
	void _Release();
    void _FreeData(tMatrixData *pData);
	void _AllocateBuffer(int row, int col);
	T ** _AllocatePointer(int row, int col, T *pMat);

	void _AllocateNRCPointer();
	void _FreeNRCPointer();

	void _AssignCopy(const CMatrix <T> &src);
	void _Copy(const CMatrix <T> &Src);

public:
	/*--------------------------------------------------------------*/
	///@name	Member Variable Acess Functon
	/*--------------------------------------------------------------*/
	//@{
	///
    int		GetRowSize()	const	{ return m_pData->nRow;      };
	///
    int		GetColSize()	const	{ return m_pData->nCol;      };
	///
	int     GetWidthStep()  const   { return m_pData->widthStep; };
	
	///
	int		GetSize()		const	{ return m_pData->nRow * m_pData->nCol;				};
	///
	int 	GetBufferSize()	const	{ return m_pData->nRow * m_pData->nCol * sizeof(T);	};
	///
	      T   ** Get2DBuffer()      const { _CopyBeforeWrite(); return (      T **)m_ppMat;};
    ///
    const T   ** Get2DBufferConst() const {                    return (const T **)m_ppMat;};
	///
	      T    * GetBuffer     ()         { _CopyBeforeWrite(); return (      T  *)m_pData->pMat; };
	///
	const T    * GetBufferConst()   const {                    return (const T  *)m_pData->pMat; };

	const Vip2DPtr <T> GetVip2DPtrConst() const {                    return Mat2VipPtr(m_pData); }
	      Vip2DPtr <T> GetVip2DPtr     ()       { _CopyBeforeWrite(); return Mat2VipPtr(m_pData); }

	const Vip2DPtr <T> GetVip2DPtrConst(int r, int c) const {                    return vip2DPtr(Mat2VipPtr(m_pData), c, r); }
	      Vip2DPtr <T> GetVip2DPtr     (int r, int c)       { _CopyBeforeWrite(); return vip2DPtr(Mat2VipPtr(m_pData), c, r); }

	const Vip2DInfo <T> GetVip2DInfoConst() const {                    return vip2DInfo(Mat2VipPtr(m_pData), GetColSize(), GetRowSize()); }
	      Vip2DInfo <T> GetVip2DInfo     ()       { _CopyBeforeWrite(); return vip2DInfo(Mat2VipPtr(m_pData), GetColSize(), GetRowSize()); }

	const Vip2DInfo <T> GetVip2DInfoConst(int r, int c, int rowSize, int colSize) const {                    return vip2DInfo(GetVip2DPtrConst(r, c), colSize, rowSize); }
	      Vip2DInfo <T> GetVip2DInfo     (int r, int c, int rowSize, int colSize)       { _CopyBeforeWrite(); return vip2DInfo(GetVip2DPtr     (r, c), colSize, rowSize); }
	///
	      T    * GetBuffer     (int r, int c)         { _CopyBeforeWrite(); return (      T  *) (m_pData->pMat + m_pData->widthStep * r + c*sizeof(T)); };
	///
	const T    * GetBufferConst(int r, int c)   const {                    return (const T  *) (m_pData->pMat + m_pData->widthStep * r + c*sizeof(T)); };

	///		return 2D Pointer which is shifted (-1, -1) : for Numericla Recipes in C
	T   **	GetNRCMatrix()			{ _AllocateNRCPointer(); return m_ppNRCPointer;		};
	///		return 1D Pointer which is shifted (-1)     : for Numericla Recipes in C
	T    *	GetNRCVector()			{ _CopyBeforeWrite();    return ((T*)GetBuffer())-1; };
	//@}

public:
	/*--------------------------------------------------------------*/
	///@name	Matrix Data Access Function
	/*--------------------------------------------------------------*/
	//@{

	///
	CMatrix <T> Crop(int row, int col, int rowSize, int colSize) const
	{
		VIP_ASSERT( (row+rowSize) <= GetRowSize(), "CMatrix::GetMatrix", "Wrong Matrix Dimension");
		VIP_ASSERT( (col+colSize) <= GetColSize(), "CMatrix::GetMatrix", "Wrong Matrix Dimension");
		VIP_ASSERT( 0 <= row && 0 <= col,          "CMatrix::GetMatrix", "Wrong Matrix Dimension");

		CMatrix <T> ret(rowSize, colSize);
		vipCopy(
			    GetBufferConst(row, col),     GetWidthStep(),
			ret.GetBuffer     (          ), ret.GetWidthStep(),
			colSize, rowSize);
		return ret;
	}

	///
	void Paste(const CMatrix <T> &src, int row, int col)
	{
		VIP_ASSERT((row + src.GetRowSize()) <= GetRowSize(), "CMatrix::Paste", "Wrong Size");
		VIP_ASSERT((col + src.GetColSize()) <= GetColSize(), "CMatrix::Paste", "Wrong Size");

		_CopyBeforeWrite();
		vipCopy(
			src.GetBufferConst(          ), src.GetWidthStep(),
			    GetBuffer     (row, col),     GetWidthStep(),
			src.GetColSize(), src.GetRowSize() );
	}

	///
	CMatrix <T> GetMatrix(int row, int rowSize, int col, int colSize) const { return Crop(row, col, rowSize, colSize); }

	///
	CMatrix <T> GetRowVector(int row             ) const { return Crop(row,   0,            1, GetColSize()); };
	///
	CMatrix <T> GetColVector(int col             ) const { return Crop(  0, col, GetRowSize(),            1); };
	///
	CMatrix <T> GetRowMatrix(int row, int rowSize) const { return Crop(row,   0,      rowSize, GetColSize()); };
	///
	CMatrix <T> GetColMatrix(int col, int colSize) const { return Crop(  0, col, GetRowSize(),      colSize); };

	///
	void SetColMatrix(const CMatrix <T> &src, int col) { Paste(src,   0, col); };
	///
	void SetRowMatrix(const CMatrix <T> &src, int row) { Paste(src, row,   0); };

	///
	void SetColVector(const CMatrix <T> &src, int col) { VIP_ASSERT(src.GetColSize() == 1, "", ""); Paste(src,   0, col); };
	///
	void SetRowVector(const CMatrix <T> &src, int row) { VIP_ASSERT(src.GetRowSize() == 1, "", ""); Paste(src, row,   0); };

	/// 
	void Assign(T value)
	{
		_CopyBeforeWrite();
		if (value)
		{
			MATRIX_LOOP(this, 0, 0, _MAT1_ = value);
		}
		else
			memset(m_pData->pMat, 0, GetBufferSize());
	}
	//@}

public:
	/*--------------------------------------------------------------*/
	///@name	Matrix Function
	/*--------------------------------------------------------------*/
	//@{
	///
    CMatrix <T> Transpose() const
	{
		CMatrix <T> out(GetColSize(), GetRowSize());

		if (GetColSize() == 1 && GetRowSize() == 1)
			out(0, 0) = Read(0, 0);
		else
		{
			vipTranspose(
				    GetBufferConst(),     GetWidthStep(), GetColSize(), GetRowSize(),
				out.GetBuffer     (), out.GetWidthStep(), 1 );
		}
		return out;
	};

	///
	void ExchangeCol(int nSrcCol, int nDstCol)
	{
		_CopyBeforeWrite();
		int row = GetRowSize();
		for (int i = 0 ; i < row ; i++)
		{
			T buffer;
			buffer = READ_MAT_POINTER((*this), i, nSrcCol);
			READ_MAT_POINTER((*this), i, nSrcCol) = READ_MAT_POINTER((*this), i, nDstCol);
			READ_MAT_POINTER((*this), i, nDstCol) = buffer;
		}
	}

	///
	void ExchangeRow(int nSrcRow, int nDstRow)
	{
		_CopyBeforeWrite();
		int col = GetColSize();
		for (int i = 0 ; i < col ; i++)
		{
			T buffer;
			buffer = READ_MAT_POINTER((*this), nSrcRow, i);
			READ_MAT_POINTER((*this), nSrcRow, i) = READ_MAT_POINTER((*this), nDstRow, i);
			READ_MAT_POINTER((*this), nDstRow, i) = buffer;
		}
	}
	//@}

public:
	/*--------------------------------------------------------------*/
	///@name	Operator (Copy, Type Conversion,, ETC.... )
	/*--------------------------------------------------------------*/
	//@{
	///
	CMatrix <T> & operator = (const CMatrix <T> &Src) { _Copy(Src); return (*this); }

	inline const T & Read(int nRow = 0, int nCol = 0) const
	{
		CHECK_RANGE_MAT(nRow, nCol);
		return ((T **)m_ppMat)[nRow][nCol];
	}

	///
	inline T  & operator() (int nRow = 0, int nCol = 0)
	{
		CHECK_RANGE_MAT(nRow, nCol);
		CHECK_CLONE;
		return ((T **)m_ppMat)[nRow][nCol];
	};

	///
	inline T    operator() (int nRow = 0, int nCol = 0) const
	{
		CHECK_RANGE_MAT(nRow, nCol);
		return ((T **)m_ppMat)[nRow][nCol];
	};

	///
	inline T* & operator[] (int nRow)
	{
		CHECK_RANGE_MAT(nRow, 0);
		CHECK_CLONE;
		return ((T **)m_ppMat)[nRow];
	};

	operator const T * () const { return GetBufferConst  (); }
	operator       T * ()       { return GetBuffer       (); }

	operator const T **() const { return Get2DBufferConst(); }
	operator       T **()       { return Get2DBuffer     (); }

	///
    CMatrix <T> Inverse() const
	{
		VIP_ASSERT(GetRowSize() == GetColSize(), "CMatrix::Inverse()", "Matrix is not square");
		VIP_ASSERT(GetRowSize() != 0, "Inverse", "Matrix size is zero");

		int row_colSize = GetRowSize();
		CMatrix <T> dst(row_colSize, row_colSize);
		bool ret = vipInverse(
				GetBufferConst(),     GetWidthStep(),
			dst.GetBuffer     (), dst.GetWidthStep(), row_colSize);
		VIP_ASSERT(ret, "CMatrix::Inverse()", "Matrix is Singular");
		return dst;
	};
	//@}

public:
	///
	bool IsEmpty() const { return (m_ppMat) ? false : true; };

	///
	bool LoadFromText(const char *format, ...)
	{
		char *filename = new char[_MAX_PATH];
		va_list vl;
		va_start(vl, format);
		vsprintf(filename, format, vl);
		FILE *fp = fopen(filename, "rb");
		delete [] filename;

		bool ret = true;
		char *buffer = NULL;
		if(fp)
		{
			fseek(fp, 0, SEEK_END);
			unsigned int size = ftell(fp) + 1;
			buffer = new char[size];
			memset(buffer, 0, sizeof(char) * size);
			fseek(fp, 0, SEEK_SET);
			fread(buffer, 1, size, fp);
			fclose(fp);
		}
		else
			return false;

		int RowSize, ColSize;
		char ***strMatrix = StringToMatrix(buffer, RowSize, ColSize, "\f\n\r", " ");
		if(strMatrix)
		{
			Allocate(RowSize, ColSize);

			T **ppBuffer = Get2DBuffer();
			for (int r = 0 ; r < RowSize ; r++)
			{
				T *pBuffer = ppBuffer[r];
				for (int c = 0 ; c < ColSize ; c++)
					pBuffer[c] = (T)atof(strMatrix[r][c]);
			}

			DeleteStringMatrix(strMatrix, RowSize, ColSize);
		}
		else
			VIP_ASSERT(0, "LoadFromText", "Column Size's are not same each other");

		if(buffer)	delete [] buffer;
		return ret;
	}
public:

	///
	void SaveToText(const char *format, ...) const
	{
		char *filename = new char[_MAX_PATH];
		va_list vl;
		va_start(vl, format);
		vsprintf(filename, format, vl);

		FILE  * stream;
		stream = fopen(filename, "w");
		delete [] filename;

		if(stream == NULL)
			return;

		const T **ppBuf = Get2DBufferConst();

		for (int r = 0; r < GetRowSize(); r++)
		{
			const T *pBuf = ppBuf[r];
			for (int c = 0; c < GetColSize(); c++)
				fprintf(stream, "  %5.3e", (float)(pBuf[c]));
			fprintf(stream, "\n");
		}
		fprintf(stream, "\n");
		fclose(stream);
	}
};

/*------------------------------------------------------------------*/
// Operator of Template Matrix Class
/*------------------------------------------------------------------*/

template <class T>
inline CMatrix <T> operator - (const CMatrix <T> & src)
{
	CMatrix <T> out(src.GetRowSize(), src.GetColSize());
	MATRIX_LOOP(&src, 0, &out, _OUTMAT_ = -_MAT1_)
	return out;
};

#define DEFINE_OPERATOR_MAT1_MAT2_OUTPUT(OPERATOR, INI_OP, LOOP_OP)                  \
template <class T>                                                                   \
inline CMatrix <T> operator OPERATOR (const CMatrix <T> &Op1, const CMatrix <T> &Op2)\
{                                                                                    \
	if ( CHECK_1X1(Op1) ) return Op1.Read(0, 0) OPERATOR Op2           ;             \
	if ( CHECK_1X1(Op2) ) return Op1            OPERATOR Op2.Read(0, 0);             \
	CHECK_DIM_MATRIX(Op1, Op2);                                                      \
	INI_OP;                                                                          \
	CMatrix <T> output(Op1.GetRowSize(), Op1.GetColSize());                          \
	MATRIX_LOOP(&Op1, &Op2, &output, LOOP_OP; _OUTMAT_ = _MAT1_   OPERATOR _MAT2_  ) \
	return output;                                                                   \
};

#define DEFINE_OPERATOR_MAT1_SCALAR_OUTPUT(OPERATOR, INI_OP, LOOP_OP)                \
template <class T>                                                                   \
inline CMatrix <T> operator OPERATOR (const CMatrix <T> &Op1, T Scalar)              \
{                                                                                    \
	INI_OP;                                                                          \
	CMatrix <T> output(Op1.GetRowSize(), Op1.GetColSize());                          \
	MATRIX_LOOP(&Op1,    0, &output, LOOP_OP; _OUTMAT_ = _MAT1_   OPERATOR _SCALAR_) \
	return output;                                                                   \
};

#define DEFINE_OPERATOR_SCALAR_MAT1_OUTPUT(OPERATOR, INI_OP, LOOP_OP)                \
template <class T>                                                                   \
inline CMatrix <T> operator OPERATOR (T Scalar, const CMatrix <T> &Op1)              \
{                                                                                    \
	INI_OP;                                                                          \
	CMatrix <T> output(Op1.GetRowSize(), Op1.GetColSize());                          \
	MATRIX_LOOP(&Op1,    0, &output, LOOP_OP; _OUTMAT_ = _SCALAR_ OPERATOR _MAT1_  ) \
	return output;                                                                   \
};

#define DEFINE_OPERATOR_MAT1_SCALAR(OPERATOR, INI_OP, LOOP_OP)                       \
template <class T>                                                                   \
inline CMatrix <T> & operator OPERATOR (CMatrix <T> &Op1, T Scalar)                  \
{                                                                                    \
	INI_OP;                                                                          \
	MATRIX_LOOP(&Op1,    0,       0, LOOP_OP; _MAT1_ OPERATOR _SCALAR_			   ) \
	return Op1;                                                                      \
};

#define DEFINE_OPERATOR_MAT1_MAT2(OPERATOR, INI_OP, LOOP_OP)                         \
template <class T>                                                                   \
inline CMatrix <T> & operator OPERATOR (CMatrix <T> &Op1, const CMatrix <T> &Op2)    \
{                                                                                    \
	INI_OP;                                                                          \
	if ( CHECK_1X1(Op2) ) return Op1 OPERATOR Op2.Read(0, 0);                        \
	CHECK_DIM_MATRIX(Op1, Op2);                                                      \
	MATRIX_LOOP(&Op1, &Op2,       0, LOOP_OP; _MAT1_ OPERATOR _MAT2_               ) \
	return Op1;                                                                      \
};

#define CHK_0(value, OPERATOR) VIP_ASSERT(value != 0  , "operator "#OPERATOR , "Divide by Zero")

DEFINE_OPERATOR_MAT1_MAT2_OUTPUT  (+ ,               void(0),             void(0));
DEFINE_OPERATOR_MAT1_MAT2_OUTPUT  (- ,               void(0),             void(0));
DEFINE_OPERATOR_MAT1_MAT2_OUTPUT  (/ ,               void(0), CHK_0(_MAT2_,  "/"));

DEFINE_OPERATOR_MAT1_SCALAR_OUTPUT(+ ,               void(0),             void(0));
DEFINE_OPERATOR_MAT1_SCALAR_OUTPUT(- ,               void(0),             void(0));
DEFINE_OPERATOR_MAT1_SCALAR_OUTPUT(* ,               void(0),             void(0));
DEFINE_OPERATOR_MAT1_SCALAR_OUTPUT(/ , CHK_0(_SCALAR_,  "/"),             void(0));

DEFINE_OPERATOR_SCALAR_MAT1_OUTPUT(+ ,               void(0),             void(0));
DEFINE_OPERATOR_SCALAR_MAT1_OUTPUT(- ,               void(0),             void(0));
DEFINE_OPERATOR_SCALAR_MAT1_OUTPUT(* ,               void(0),             void(0));
DEFINE_OPERATOR_SCALAR_MAT1_OUTPUT(/ ,               void(0), CHK_0(_MAT1_,  "/"));

DEFINE_OPERATOR_MAT1_MAT2         (+=,               void(0),             void(0));
DEFINE_OPERATOR_MAT1_MAT2         (-=,               void(0),             void(0));
DEFINE_OPERATOR_MAT1_MAT2         (*=,               void(0),             void(0));
DEFINE_OPERATOR_MAT1_MAT2         (/=,               void(0), CHK_0(_MAT2_, "/="));

DEFINE_OPERATOR_MAT1_SCALAR       (+=,               void(0),             void(0));
DEFINE_OPERATOR_MAT1_SCALAR       (-=,               void(0),             void(0));
DEFINE_OPERATOR_MAT1_SCALAR       (*=,               void(0),             void(0));
DEFINE_OPERATOR_MAT1_SCALAR       (/=, CHK_0(_SCALAR_, "/="),             void(0));

#undef CHK_0

///
template <class T>
CMatrix <T> operator * (const CMatrix <T> &srcA, const CMatrix <T> &srcB)
{
	if ( CHECK_1X1(srcA) ) return srcA.Read(0, 0) * srcB           ;
	if ( CHECK_1X1(srcB) ) return srcA            * srcB.Read(0, 0);

	VIP_ASSERT(srcA.GetColSize() == srcB.GetRowSize(), "operator *", "operator * () : Matrix size mismatch");

	CMatrix <T> out(srcA.GetRowSize(), srcB.GetColSize());
	vipMatMul(
		srcA.GetBufferConst(), srcA.GetWidthStep(), srcA.GetColSize(), srcA.GetRowSize(),
		srcB.GetBufferConst(), srcB.GetWidthStep(), srcB.GetColSize(), srcB.GetRowSize(),
		out .GetBuffer     (), out .GetWidthStep() );
	
    return out;
}



/*------------------------------------------------------------------*/
// Protected Member Functions of Template Matrix Class
/*------------------------------------------------------------------*/
template <class T>
void CMatrix <T>::_AllocateBuffer(int row, int col)
{
	VIP_ASSERT(row >= 0 && col >= 0, "_AllocateBuffer", "");

	if (row == 0 && col == 0)
		_InitData();
	else
	{
		tMatrixData* pData = new tMatrixData;

		pData->widthStep = col * sizeof(T);
		pData->pMat      = new char [row * pData->widthStep];
		pData->ppMat     = (void **)(m_ppMat = _AllocatePointer(row, col, (T *)(pData->pMat)));
		pData->nRow      = row;
		pData->nCol      = col;
		pData->nRef      = 1;

		m_pData = pData;
	}
};

template <class T>
void CMatrix <T>::_AllocateNRCPointer()
{
	_CopyBeforeWrite();
	_FreeNRCPointer();
	m_ppNRCPointer = new T *[GetRowSize() + 1];
	for (int r = 0 ; r < GetRowSize() ; r++)
		m_ppNRCPointer[r + 1] = (*this)[r] - 1;
};

template <class T>
void CMatrix <T>::_FreeNRCPointer()
{
	if(m_ppNRCPointer)
	{
		delete [] m_ppNRCPointer;
		m_ppNRCPointer = NULL;
	}
};

template <class T>
T ** CMatrix <T>::_AllocatePointer(int row, int col, T *pMat)
{
	T **ppMat = new T *[row];
	int step = col;//( XSize * (depth/8) * image->nChannels + 3) & -4;
	for (int r = 0 ; r < row ; r++)
		ppMat[r] = pMat + r * step;
	return ppMat;
};

template <class T>
void CMatrix <T>::_AssignCopy(const CMatrix <T> &src)
{
	Allocate(src.GetRowSize(), src.GetColSize(), false);
	memcpy(m_pData->pMat, src.m_pData->pMat, sizeof(T) * GetSize());
};

template <class T>
void CMatrix <T>::_Copy(const CMatrix <T> &Src)
{
	if (m_pData != Src.m_pData)
	{
		if ( (m_pData->nRef < 0 && m_ppMat != NULL) || Src.m_pData->nRef < 0)
			_AssignCopy(Src);
		else
		{
			_Release();
			if (Src.m_ppMat)
			{
				VIP_ASSERT(Src.m_ppMat != NULL, "", "");
	//			ASSERT(Src.m_pData != _InitPointerMatrix);
				m_pData = Src.m_pData;
				m_ppMat = Src.m_ppMat;
				MatrixLockedIncrement(& (m_pData->nRef) );
			}
		}
	}
};

template <class T>
void CMatrix <T>::_CopyBeforeWrite() const
{
	if (m_pData->nRef > 1)
	{
		tMatrixData* pData = m_pData;
		CMatrix <T> *pMatrix = (CMatrix <T> *)this;
		pMatrix->_Release();
		pMatrix->_AllocateBuffer(pData->nRow, pData->nCol);
		memcpy(m_pData->pMat, pData->pMat, sizeof(T) * GetSize());
	}
	VIP_ASSERT(m_pData->nRef <= 1, "", "");
};

template <class T>
void CMatrix <T>::_FreeData(tMatrixData *pData)
{
	delete [] pData->pMat;
	delete [] pData->ppMat;
	delete pData;
};

template <class T>
void CMatrix <T>::_Initialize()
{
	_InitData();
	m_ppNRCPointer		= NULL;
};

template <class T>
void CMatrix <T>::_InitData()
{
	m_pData = _InitPointerMatrix;
	m_ppMat = NULL;
}

template <class T>
CMatrix <T>::~CMatrix()
{
	_Release();
}

template <class T>
void CMatrix <T>::_Release()
{
	_FreeNRCPointer();
	if (m_ppMat != NULL)
	{
		VIP_ASSERT(m_pData->nRef != 0, "", "");
		if ( MatrixLockedDecrement(&(m_pData->nRef)) <= 0)
			_FreeData(m_pData);
		_InitData();
	}
};

#undef CHECK_CLONE
#undef READ_MAT_POINTER
#undef CHECK_1X1

template <class T>
std::ostream & operator << (std::ostream &s, const CMatrix <T> &src)
{
	int r = src.GetRowSize();
	int c = src.GetColSize();
	s.write((const char*)&r, sizeof(r));
	s.write((const char*)&c, sizeof(c));
	s.write((const char*)src.GetBufferConst(), src.GetBufferSize());
	return s;
}

template <class T>
std::istream & operator >> (std::istream &s, CMatrix <T> &src)
{
	int r, c;
	s.read( (char*)&r, sizeof(r));
	s.read( (char*)&c, sizeof(c));
	src.Allocate(r, c, false);
	s.read( (char*)src.GetBuffer(), src.GetBufferSize());
	return s;
}

#endif   //  for VIP_MATRIX_H
