#if !defined(_VIP_IPL_WRAP_)
#define _VIP_IPL_WRAP_

#include "ViplBase.h"

/* Memo //////////////////////////////////////////////////////////////////////////////
a. X86
	1. Win32
		Use VIPL with IPL
	2. Linux
		Use VIPL with IPP2IPL and IPP
		IPP2IPL -> Converting library(IPP -> IPL)
		IPL does not support Linux !
b. XScale
	Do not use overloaded operator like (+, -, *, /),
	because memory fragment may largly affect processing speed.
	Use the base functions which is defined in VipCore.h as possible as you can
	Note that XScale version of IPP does not support all kind of 
	image functions which is in X86 version of IPP

	1. WinCE
		#define _WIN32_WCE and _USE_IPL_WRAP_
	2. Linux
		#define _USE_IPL_WRAP_
*/////////////////////////////////////////////////////////////////////////////////////


#define _USE_IPL_WRAP_INIT_

#define _USE_IPL_WRAP_

#include "./IplWrap/ipl_data_type.h"


#ifdef _USE_IPL_WRAP_INIT_
	VIPLIB_API void vipAllocateImage  (IplImage* image, int doFill,   int fillValue);
	VIPLIB_API void vipAllocateImageFP(IplImage* image, int doFill, float fillValue);
	void vipDeallocate     (IplImage* image, int flag);
	void vipDeleteROI      (IplROI* roi);
	IplROI * vipCreateROI(int coi, int xOffset, int yOffset, int width, int height );
	VIPLIB_API IplImage* vipCreateImageHeader
				   (int   nChannels , int     alphaChannel, int     depth,
					char* colorModel, char*   channelSeq,   int     dataOrder,
					int   origin    , int     align,
					int   width     , int     height,
					IplROI* roi     , IplImage* maskROI,
					void* imageId   , IplTileInfo* tileInfo );

	void vipSetROI(IplROI *roi, int coi, int xOffset, int yOffset, int width, int height);
	void vipSetBorderMode(IplImage *src,int mode,int border,int constVal);
#else
	#define vipAllocateImage     iplAllocateImage  
	#define vipAllocateImageFP   iplAllocateImageFP
	#define vipDeallocate        iplDeallocate
	#define vipDeleteROI         iplDeleteROI
	#define vipCreateROI         iplCreateROI
	#define vipCreateImageHeader iplCreateImageHeader
	#define vipSetROI            iplSetROI
	#define vipSetBorderMode     iplSetBorderMode
#endif

#ifdef _USE_IPL_WRAP_
	void iplComputeHisto(const IplImage* srcImage, IplLUT** lut);
	void iplRemap       (const IplImage* srcImage, const IplImage* xMap, const IplImage* yMap, IplImage* dstImage, int interpolate);

	void iplRealFft2D   (const IplImage* srcImage, IplImage* dstImage, int flags);
	void iplCcsFft2D    (const IplImage* srcImage, IplImage* dstImage, int flags);
	void iplColorToGray (const IplImage* srcImage, IplImage* dstImage);
	void iplGrayToColor (const IplImage* srcImage, IplImage* dstImage, float FractR, float FractG, float FractB);
	void iplConvolve2D  (IplImage* srcImage, IplImage* dstImage, IplConvKernel  ** kernel, int nKernels, int combineMethod);
	void iplConvolve2DFP(IplImage* srcImage, IplImage* dstImage, IplConvKernelFP** kernel, int nKernels, int combineMethod);
	void iplFixedFilter (const IplImage* srcImage, IplImage* dstImage, int filter);
	void iplMedianFilter(const IplImage* srcImage, IplImage* dstImage, int nCols, int nRows, int anchorX, int anchorY);
	void iplErode       (const IplImage* srcImage, IplImage* dstImage, int nIterations);
	void iplDilate      (const IplImage* srcImage, IplImage* dstImage, int nIterations);
	void iplOpen        (const IplImage* srcImage, IplImage* dstImage, int nIterations);
	void iplClose       (const IplImage* srcImage, IplImage* dstImage, int nIterations);
	void iplAddS        (const IplImage* srcImage, IplImage* dstImage, int value);
	void iplAddSFP      (const IplImage* srcImage, IplImage* dstImage, float value);
	void iplSubtractS   (const IplImage* srcImage, IplImage* dstImage, int value, BOOL flip);
	void iplSubtractSFP (const IplImage* srcImage, IplImage* dstImage,float value, BOOL flip);
	void iplMultiplyS   (const IplImage* srcImage, IplImage* dstImage, int value);
	void iplMultiplySFP (const IplImage* srcImage, IplImage* dstImage, float value);
	void iplAbs         (const IplImage* srcImage, IplImage* dstImage);
	void iplSquare      (const IplImage* srcImage, IplImage* dstImage);
	void iplRotate      (const IplImage* srcImage, IplImage* dstImage, double angle, double xShift, double yShift, int interpolate);

	void iplAdd         (const IplImage* srcImageA, const IplImage* srcImageB, IplImage* dstImage);
	void iplSubtract    (const IplImage* srcImageA, const IplImage* srcImageB, IplImage* dstImage);
	void iplMultiply    (const IplImage* srcImageA, const IplImage* srcImageB, IplImage* dstImage);
	double iplNorm      (const IplImage* srcImageA, const IplImage* srcImageB, int normType);
	void iplMpyRCPack2D (const IplImage* srcImageA, const IplImage* srcImageB, IplImage* dstImage);

	void iplMinMaxFP    (const IplImage * srcImage, float * min, float * max);

	void iplGetRotateShift(double xCenter, double yCenter, double angle, double *xShift, double *yShift);
	void iplResize      (const IplImage* srcImage, IplImage* dstImage, int xDst, int xSrc, int yDst, int ySrc, int interpolate);
	void iplSet    (IplImage* image, int   fillValue);
	void iplSetFP  (IplImage* image, float fillValue);
	void iplCopy   (IplImage *src, IplImage *dst);
	void iplConvert(IplImage *src, IplImage *dst);
	void iplSquare(IplImage* srcImage, IplImage* dstImage);
#endif

VIPLIB_API void vipDivide   (const IplImage* srcA, const IplImage* srcB, IplImage* dstC);
VIPLIB_API void vipDivideS  (const IplImage* src , IplImage* dst ,   int value, bool flip);
VIPLIB_API void vipDivideSFP(const IplImage* src , IplImage* dst , float value, bool flip);

VIPLIB_API void vipPow      (const IplImage* srcA, const IplImage* srcB, IplImage* dstC);
VIPLIB_API void vipPowS     (const IplImage* src , IplImage* dst ,   int value, bool flip);
VIPLIB_API void vipPowSFP   (const IplImage* src , IplImage* dst , float value, bool flip);

#endif // !defined(_VIP_IPL_WRAP_)
