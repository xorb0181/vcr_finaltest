// VipArray.h: interface for the CVipArrBase class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_TEMPL_POINT_H__90AF9A28_054D_48B9_AE3D_2109CDEDFDAB__INCLUDED_)
#define AFX_TEMPL_POINT_H__90AF9A28_054D_48B9_AE3D_2109CDEDFDAB__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "VipArray.h"

template <class T> struct Tmpl2DPoint;

typedef Tmpl2DPoint <short > s2DPoint;
typedef Tmpl2DPoint <int   > i2DPoint;
typedef Tmpl2DPoint <float > f2DPoint;
typedef Tmpl2DPoint <double> d2DPoint;

typedef CVipArrBase < s2DPoint > s2DPointArray;
typedef CVipArrBase < i2DPoint > i2DPointArray;
typedef CVipArrBase < f2DPoint > f2DPointArray;
typedef CVipArrBase < d2DPoint > d2DPointArray;

typedef s2DPoint sPoint;
typedef i2DPoint iPoint;
typedef f2DPoint fPoint;
typedef d2DPoint dPoint;

typedef s2DPointArray sPointArray;
typedef i2DPointArray iPointArray;
typedef f2DPointArray fPointArray;
typedef d2DPointArray dPointArray;

class CPoint;

struct CvPoint;
struct CvPoint2D32f;
struct CvPoint2D64d;

template <class T> 
struct Tmpl2DPoint
{
	T x;
	T y;

	Tmpl2DPoint() {};
	Tmpl2DPoint(T xx, T yy) { x = xx; y = yy; };
#ifndef _LINUX
	Tmpl2DPoint(const CPoint &src) { x = (T)src.x; y = (T)src.y; };
	operator CPoint       () const { return CPoint( int(x+0.5), int(y+0.5) ); };
	
	Tmpl2DPoint(const CvPoint       &src) { x = (T)src.x; y = (T)src.y; };
	Tmpl2DPoint(const CvPoint2D32f  &src) { x = (T)src.x; y = (T)src.y; };
	Tmpl2DPoint(const CvPoint2D64d  &src) { x = (T)src.x; y = (T)src.y; };

	operator CvPoint      () const { return cvPointRound(int(x+0.5), int(y+0.5)); };
	operator CvPoint2D32f () const { return cvPoint2D32f(x    , y    ); };
	operator CvPoint2D64d () const { return cvPoint2D64d(x    , y    ); };

private:
	static CvPoint cvPointRound(double x, double y) { return cvPoint(cvRound(x+0.5), cvRound(y+0.5)) };
	static CvPoint cvPointRound(float  x, float  y) { return cvPoint(cvRound(x+0.5), cvRound(y+0.5)) };
	static CvPoint cvPointRound(int    x, int    y) { return cvPoint(x, y); };
#endif
};

#define _TmPt2D      Tmpl2DPoint <T>
#define _TmPt2DArray CVipArrBase < Tmpl2DPoint <T> >

#define _DefTM template <class T>

_DefTM _TmPt2D   operator +  (const _TmPt2D &A, const double    B) { return _TmPt2D(A.x+(T)B, A.y+(T)B); }
_DefTM _TmPt2D   operator -  (const _TmPt2D &A, const double    B) { return _TmPt2D(A.x-(T)B, A.y-(T)B); }
_DefTM _TmPt2D   operator *  (const _TmPt2D &A, const double    B) { return _TmPt2D(A.x*(T)B, A.y*(T)B); }
_DefTM _TmPt2D   operator /  (const _TmPt2D &A, const double    B) { return _TmPt2D(A.x/(T)B, A.y/(T)B); }

_DefTM _TmPt2D & operator += (      _TmPt2D &A, const double    B) { A.x += (T)B; A.y += (T)B; return A;  }
_DefTM _TmPt2D & operator -= (      _TmPt2D &A, const double    B) { A.x -= (T)B; A.y -= (T)B; return A;  }
_DefTM _TmPt2D & operator *= (      _TmPt2D &A, const double    B) { A.x *= (T)B; A.y *= (T)B; return A;  }
_DefTM _TmPt2D & operator /= (      _TmPt2D &A, const double    B) { A.x /= (T)B; A.y /= (T)B; return A;  }

_DefTM _TmPt2D & operator -= (      _TmPt2D &A, const _TmPt2D &B) { A.x -=  B.x; A.y -=  B.y; return A;  }
_DefTM _TmPt2D & operator += (      _TmPt2D &A, const _TmPt2D &B) { A.x +=  B.x; A.y +=  B.y; return A;  }
_DefTM _TmPt2D & operator *= (      _TmPt2D &A, const _TmPt2D &B) { A.x *=  B.x; A.y *=  B.y; return A;  }
_DefTM _TmPt2D & operator /= (      _TmPt2D &A, const _TmPt2D &B) { A.x /=  B.x; A.y /=  B.y; return A;  }

_DefTM _TmPt2D   operator +  (const double    B, const _TmPt2D &A) { return _TmPt2D((T)B + A.x, (T)B + A.y); }
_DefTM _TmPt2D   operator -  (const double    B, const _TmPt2D &A) { return _TmPt2D((T)B - A.x, (T)B - A.y); }
_DefTM _TmPt2D   operator *  (const double    B, const _TmPt2D &A) { return _TmPt2D((T)B * A.x, (T)B * A.y); }
_DefTM _TmPt2D   operator /  (const double    B, const _TmPt2D &A) { return _TmPt2D((T)B / A.x, (T)B / A.y); }

_DefTM _TmPt2D   operator +  (const _TmPt2D &A, const _TmPt2D &B) { return _TmPt2D(A.x  + B.x, A.y  + B.y); }
_DefTM _TmPt2D   operator -  (const _TmPt2D &A, const _TmPt2D &B) { return _TmPt2D(A.x  - B.x, A.y  - B.y); }
_DefTM _TmPt2D   operator *  (const _TmPt2D &A, const _TmPt2D &B) { return _TmPt2D(A.x  * B.x, A.y  * B.y); }
_DefTM _TmPt2D   operator /  (const _TmPt2D &A, const _TmPt2D &B) { return _TmPt2D(A.x  / B.x, A.y  / B.y); }

_DefTM _TmPt2D   operator -  (const _TmPt2D &A                   ) { return _TmPt2D(-A.x, -A.y); }
_DefTM bool      operator == (const _TmPt2D &A, const _TmPt2D &B) { return (A.x == B.x && A.y == B.y) ? true  : false; }
_DefTM bool      operator != (const _TmPt2D &A, const _TmPt2D &B) { return (A.x == B.x && A.y == B.y) ? false : true ; }

#undef _DefTM
#undef _TmPt2D
#undef _TmPt2DArray

//------------------------------------------------------------------------

template <class T> struct Tmpl3DPoint;

typedef Tmpl3DPoint <int     > i3DPoint;
typedef Tmpl3DPoint <float   > f3DPoint;
typedef Tmpl3DPoint <double  > d3DPoint;

typedef CVipArrBase <i3DPoint> i3DPointArray;
typedef CVipArrBase <f3DPoint> f3DPointArray;
typedef CVipArrBase <d3DPoint> d3DPointArray;


template <class T> 
struct Tmpl3DPoint
{
	T x;
	T y;
	T z;

	Tmpl3DPoint() {};
	Tmpl3DPoint(T xx, T yy, T zz) { x = xx; y = yy; z = zz; };
};

#define _TmPt3D      Tmpl3DPoint <T>
#define _TmPt3DArray Tmpl3DPointArray <T>

#define _DefTM template <class T>

_DefTM _TmPt3D   operator +  (const _TmPt3D &A, const double    B) { return _TmPt3D(A.x+(T)B, A.y+(T)B, A.z+(T)B); }
_DefTM _TmPt3D   operator -  (const _TmPt3D &A, const double    B) { return _TmPt3D(A.x-(T)B, A.y-(T)B, A.z-(T)B); }
_DefTM _TmPt3D   operator *  (const _TmPt3D &A, const double    B) { return _TmPt3D(A.x*(T)B, A.y*(T)B, A.z*(T)B); }
_DefTM _TmPt3D   operator /  (const _TmPt3D &A, const double    B) { return _TmPt3D(A.x/(T)B, A.y/(T)B, A.z/(T)B); }

_DefTM _TmPt3D & operator += (      _TmPt3D &A, const double    B) { A.x += (T)B; A.y += (T)B; A.z += (T)B; return A;  }
_DefTM _TmPt3D & operator -= (      _TmPt3D &A, const double    B) { A.x -= (T)B; A.y -= (T)B; A.z -= (T)B; return A;  }
_DefTM _TmPt3D & operator *= (      _TmPt3D &A, const double    B) { A.x *= (T)B; A.y *= (T)B; A.z *= (T)B; return A;  }
_DefTM _TmPt3D & operator /= (      _TmPt3D &A, const double    B) { A.x /= (T)B; A.y /= (T)B; A.z /= (T)B; return A;  }

_DefTM _TmPt3D & operator += (      _TmPt3D &A, const _TmPt3D &B) { A.x +=  B.x; A.y +=  B.y; A.z +=  B.z; return A;  }
_DefTM _TmPt3D & operator -= (      _TmPt3D &A, const _TmPt3D &B) { A.x -=  B.x; A.y -=  B.y; A.z -=  B.z; return A;  }
_DefTM _TmPt3D & operator *= (      _TmPt3D &A, const _TmPt3D &B) { A.x *=  B.x; A.y *=  B.y; A.z *=  B.z; return A;  }
_DefTM _TmPt3D & operator /= (      _TmPt3D &A, const _TmPt3D &B) { A.x /=  B.x; A.y /=  B.y; A.z /=  B.z; return A;  }

_DefTM _TmPt3D   operator +  (const double    B, const _TmPt3D &A) { return _TmPt3D((T)B + A.x, (T)B + A.y, (T)B + A.z); }
_DefTM _TmPt3D   operator -  (const double    B, const _TmPt3D &A) { return _TmPt3D((T)B - A.x, (T)B - A.y, (T)B - A.z); }
_DefTM _TmPt3D   operator *  (const double    B, const _TmPt3D &A) { return _TmPt3D((T)B * A.x, (T)B * A.y, (T)B * A.z); }
_DefTM _TmPt3D   operator /  (const double    B, const _TmPt3D &A) { return _TmPt3D((T)B / A.x, (T)B / A.y, (T)B / A.z); }

_DefTM _TmPt3D   operator +  (const _TmPt3D &A, const _TmPt3D &B) { return _TmPt3D(A.x  + B.x, A.y  + B.y, A.z + B.z); }
_DefTM _TmPt3D   operator -  (const _TmPt3D &A, const _TmPt3D &B) { return _TmPt3D(A.x  - B.x, A.y  - B.y, A.z - B.z); }
_DefTM _TmPt3D   operator *  (const _TmPt3D &A, const _TmPt3D &B) { return _TmPt3D(A.x  * B.x, A.y  * B.y, A.z * B.z); }
_DefTM _TmPt3D   operator /  (const _TmPt3D &A, const _TmPt3D &B) { return _TmPt3D(A.x  / B.x, A.y  / B.y, A.z / B.z); }

_DefTM _TmPt3D   operator -  (const _TmPt3D &A                   ) { return _TmPt3D(-A.x, -A.y, -A.z); }
_DefTM bool      operator == (const _TmPt3D &A, const _TmPt3D &B) { return (A.x == B.x && A.y == B.y && A.z == B.z) ? true  : false; }
_DefTM bool      operator != (const _TmPt3D &A, const _TmPt3D &B) { return (A.x == B.x && A.y == B.y && A.z == B.z) ? false : true ; }

#undef _DefTM
#undef _TmPt3D
#undef _TmPt3DArray

inline i3DPoint operator <<  (const i3DPoint &op, int sh) { return i3DPoint(op.x<<sh, op.y<<sh, op.z<<sh); }
inline i2DPoint operator <<  (const i2DPoint &op, int sh) { return i2DPoint(op.x<<sh, op.y<<sh          ); }

inline i3DPoint operator >>  (const i3DPoint &op, int sh) { return i3DPoint(op.x>>sh, op.y>>sh, op.z>>sh); }
inline i2DPoint operator >>  (const i2DPoint &op, int sh) { return i2DPoint(op.x>>sh, op.y>>sh          ); }

template <class T> std::ostream & operator << (std::ostream &ios, const Tmpl2DPoint <T> &pt) { ios.write((char*)&pt, sizeof(pt)); return ios; }
template <class T> std::istream & operator >> (std::istream &ios,       Tmpl2DPoint <T> &pt) { ios.read ((char*)&pt, sizeof(pt)); return ios; }
template <class T> std::ostream & operator << (std::ostream &ios, const Tmpl3DPoint <T> &pt) { ios.write((char*)&pt, sizeof(pt)); return ios; }
template <class T> std::istream & operator >> (std::istream &ios,       Tmpl3DPoint <T> &pt) { ios.read ((char*)&pt, sizeof(pt)); return ios; }

#endif // !defined(AFX_TEMPL_POINT_H__90AF9A28_054D_48B9_AE3D_2109CDEDFDAB__INCLUDED_)
