// VipArray.h: interface for the CVipArrBase class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_VIPARRAY_H__90AF9A28_054D_48B9_AE3D_2109CDEDFDAB__INCLUDED_)
#define AFX_VIPARRAY_H__90AF9A28_054D_48B9_AE3D_2109CDEDFDAB__INCLUDED_

#include "VipGlobal.h"
#include "VipDebug.h"

#include <iostream>

#include "ViplBase.h"
#define _VIP_ARR_USE_VIRTUAL_ALLOC_

#ifndef _VIP_ARR_USE_VIRTUAL_ALLOC_
	#define VipArrAlloc malloc
	#define VipArrFree  free
#else
	VIPLIB_API void * VipArrAlloc(unsigned int size);
	VIPLIB_API void   VipArrFree (void *pVoid);
#endif

struct tVipArrayData
{
	void *pArray;
	long nRef;
};

VIPLIB_API tVipArrayData * vipGetInitPtrArray();

VIPLIB_API int VipArrayLockedDecrement(long *ref);
VIPLIB_API int VipArrayLockedIncrement(long *ref);

template <class T> class CVipArrBase  
{
protected:
	tVipArrayData *m_pData;
	int  m_nSizeBuf;
	int  m_nSizeArray;
	T   *m_pArray;

protected:
	virtual void _ElementAloc (T * pElements, int nCount) {}
	virtual void _ElementFree (T * pElements, int nCount) {}

public:
	CVipArrBase()                             { _Initialize();            };
	CVipArrBase(const CVipArrBase <T> & Src)  { _Initialize(); _Copy(Src); }
	CVipArrBase(int nInternalBufSize)         { _Initialize(); _Allocate(nInternalBufSize); m_nSizeArray = 0; };
	// Logic 문제로 explicit사용 못함.

	virtual ~CVipArrBase() 
	{
		_Release();
	};

	inline int GetSize() const { return m_nSizeArray; };
	const T * GetDataConst() const {                    return m_pArray; }
	      T * GetData     ()       { _CopyBeforeWrite(); return m_pArray; }

	inline int GetInternalBufferSize() { return m_nSizeBuf; }
	bool IsInternalBufferFull() const { return (m_nSizeArray == m_nSizeBuf); }

	void SetSize(int size, bool bKeepBeforeData = true)
	{
		_CopyBeforeWrite();

		if (bKeepBeforeData)
			_Reallocate(size);
		else
			_Allocate(size);
		m_nSizeArray = size;
	}

	void SetInternalBufferSize(int size)
	{
		_CopyBeforeWrite();
		_Allocate(size);
		m_nSizeArray = 0;
	}

	void Add(const T &element)
	{
		_CopyBeforeWrite();
		_MakeDoubleSize();
		m_pArray[m_nSizeArray++] = element;
	}

	T * AddElementBuf(int nAddSize = 1, bool bCallConstructor = true)
	{
		_CopyBeforeWrite();
		int size = m_nSizeArray;
		_MakeDoubleSize(nAddSize);
		if (bCallConstructor)
		{
			_ElementFree (m_pArray + size, nAddSize);
			_ElementAloc (m_pArray + size, nAddSize);
		}
		m_nSizeArray += nAddSize;
		return m_pArray + size;
	}

	void Add(const CVipArrBase <T> & Src)
	{
		_CopyBeforeWrite();
		int size = GetSize();
		if (size + Src.GetSize() > m_nSizeBuf)
			_Reallocate( (size + Src.GetSize()) * 2);
		SetDataArray(Src.GetDataConst(), size, Src.GetSize());
		m_nSizeArray = size + Src.GetSize();
	}

	void Append(const CVipArrBase <T> & Src) { Add(Src); }

	void RemoveAll()
	{
		_CopyBeforeWrite();
		_Release();
	}

	void RemoveAt(int nIndex, int nCount = 1)
	{
		VIP_ASSERT(nIndex >= 0, "RemoveAt", "Error");
		VIP_ASSERT(nCount >= 0, "RemoveAt", "Error");
		VIP_ASSERT(nIndex + nCount <= GetSize(), "RemoveAt", "Error");
		_CopyBeforeWrite();

		// just remove a range
		int nMoveCount = m_nSizeArray - (nIndex + nCount);

		T *pData  = GetData();
		_ElementFree(pData + nIndex, nCount);
		if (nMoveCount)
			memmove(pData + nIndex, pData + nIndex + nCount, nMoveCount * sizeof(T));
		_ElementAloc (pData + (m_nSizeArray-nCount), nCount);
		m_nSizeArray -= nCount;
	}

	void InsertAt(int nIndex, const T &element)
	{
		VIP_ASSERT(nIndex >= 0, "InsertAt", "Error");
		_CopyBeforeWrite();

		if (nIndex >= m_nSizeArray)
			SetSize(nIndex + 1);
		else
		{
			int nOldSize = m_nSizeArray;
			SetSize(m_nSizeArray + 1);
			_ElementFree (m_pArray + nOldSize, 1);
			memmove(m_pArray + nIndex + 1, m_pArray + nIndex, (nOldSize-nIndex) * sizeof(T));
			_ElementAloc (m_pArray + nIndex  , 1);
		}

		VIP_ASSERT(nIndex + 1 <= m_nSizeArray, "InsertAt", "Error");
		m_pArray[nIndex] = element;
	}

	void InsertAt(int nIndex, const CVipArrBase <T> & Src)
	{
		VIP_ASSERT(nIndex >= 0, "InsertAt", "Error");
		_CopyBeforeWrite();

		int addSize = Src.GetSize();
		if (nIndex >= m_nSizeArray)
			SetSize(nIndex + addSize);
		else
		{
			int nOldSize = m_nSizeArray;
			SetSize(m_nSizeArray + addSize);
			_ElementFree (m_pArray + nOldSize, addSize);
			memmove(m_pArray + nIndex + addSize, m_pArray + nIndex, (nOldSize-nIndex) * sizeof(T));
			_ElementAloc (m_pArray + nIndex  , addSize);
		}

		VIP_ASSERT(nIndex + addSize <= m_nSizeArray, "InsertAt", "Error");
		SetDataArray(Src.GetDataConst(), nIndex, addSize);
	}

	virtual void SetDataArray(const T *pSrcArray, int start, int size)
	{
		VIP_ASSERT( (start + size) <= m_nSizeBuf, "SetDataArray", "Error");
		T *pDst = m_pArray + start;
		memcpy(pDst, pSrcArray, size * sizeof(T));
//		m_nSizeArray = start + size;
	}

	CVipArrBase <T> & operator = (const CVipArrBase <T> &Src) { _Copy(Src); return (*this); }

	inline const T & operator[] (int nIndex) const
	{
		VIP_ASSERT(0 <= nIndex && nIndex < GetSize(), "operator []", "Error");
		return m_pArray[nIndex];
	};

	inline const T & GetAtConst(int nIndex) const
	{
		VIP_ASSERT(0 <= nIndex && nIndex < GetSize(), "GetAtConst", "Error");
		return m_pArray[nIndex];
	}

	inline T & GetAt(int nIndex)
	{
		VIP_ASSERT(0 <= nIndex && nIndex < GetSize(), "GetAt", "Error");
		_CopyBeforeWrite();
		return m_pArray[nIndex];
	}

	inline void SetAt(int nIndex, const T &element)
	{
		VIP_ASSERT(0 <= nIndex && nIndex < GetSize(), "SetAt", "Error");
		_CopyBeforeWrite();
		m_pArray[nIndex] = element;
	}

	void AssignCopy(const CVipArrBase <T> &src);

protected:
	void _InitData();
	void _Initialize();
	void _Release();
	
	void _Copy(const CVipArrBase <T> &Src);

	void _MakeDoubleSize(int sizeAdd = 1);

	void _Allocate  (int size);
	void _AllocateBuffer(int size);
	void _Reallocate(int size);
	void _CopyBeforeWrite();
};

template <class T>
void CVipArrBase <T>::_Initialize()
{
	_InitData();
};

template <class T>
void CVipArrBase <T>::_InitData()
{
	m_pData    = vipGetInitPtrArray();
	m_pArray   = NULL;
	m_nSizeBuf = m_nSizeArray = 0;
}

template <class T>
void CVipArrBase <T>::_Release()
{
	if (m_pArray != NULL)
	{
		VIP_ASSERT(m_pData->nRef != 0, "_Release", "Error");
		if ( VipArrayLockedDecrement(&(m_pData->nRef)) <= 0)
		{
			_ElementFree( (T*)(m_pData->pArray), m_nSizeBuf);
			VipArrFree(m_pData->pArray);
			VipArrFree(m_pData);
		}
		_InitData();
	}
};

template <class T>
void CVipArrBase <T>::_CopyBeforeWrite()
{
	if (m_pData->nRef > 1)
	{
		int nSizeBuf   = m_nSizeBuf;
		int nSizeArray = m_nSizeArray;
		tVipArrayData* pData = m_pData;
		CVipArrBase <T> *pVipArray = (CVipArrBase <T> *)this;
		pVipArray->_Release ();
		pVipArray->_Allocate(nSizeBuf);

		SetDataArray((T *)(pData->pArray), 0, nSizeArray);
		m_nSizeArray = nSizeArray;
	}
	VIP_ASSERT(m_pData->nRef <= 1, "_CopyBeforeWrite", "Error");
};

template <class T>
void CVipArrBase <T>::AssignCopy(const CVipArrBase <T> &src)
{
	_Allocate(src.GetSize());
	SetDataArray(src.m_pArray, 0, src.GetSize());
	m_nSizeArray = src.GetSize();
};

template <class T>
void CVipArrBase <T>::_Copy(const CVipArrBase <T> &Src)
{
	if (m_pData != Src.m_pData)
	{
		if ( (m_pData->nRef < 0 && m_pArray != NULL) || Src.m_pData->nRef < 0)
			AssignCopy(Src);
		else
		{
			_Release();
			if ((Src.m_pArray != NULL))
			{
				m_pData      = Src.m_pData     ;
				m_pArray     = Src.m_pArray    ;
				m_nSizeBuf   = Src.m_nSizeBuf  ;
				m_nSizeArray = Src.m_nSizeArray;
				VipArrayLockedIncrement(& (m_pData->nRef) );
			}
		}
	}
};

template <class T>
void CVipArrBase <T>::_Allocate(int size)
{
	if (size > m_nSizeBuf)
	{
		_Release();
		_AllocateBuffer(size);
	}
}

template <class T>
void CVipArrBase <T>::_AllocateBuffer(int size)
{
	VIP_ASSERT(size >= 0, "_AllocateBuffer", "Error");

	if (size == 0)
		_InitData();
	else
	{
		m_pData         = (tVipArrayData*)VipArrAlloc(sizeof(tVipArrayData));
		m_nSizeBuf      = size;
		m_nSizeArray    = 0;
		m_pData->pArray = m_pArray = (T*)VipArrAlloc(sizeof(T) * size); //new T[size];
		_ElementAloc(m_pArray, size);
		m_pData->nRef   = 1;
	}
}

template <class T>
void CVipArrBase <T>::_Reallocate(int size)
{
	int nArraySize = GetSize();

	if (m_pArray != NULL)
	{
		if (size > m_nSizeBuf)
		{
			T *pTemp = (T *)m_pArray;
			int copySize = m_nSizeBuf;

			m_nSizeBuf = size;

			m_pData->pArray = m_pArray = (T*)VipArrAlloc(sizeof(T) * size); //new T[size];
			memcpy(m_pArray, pTemp, copySize * sizeof(T));
			_ElementAloc(m_pArray+copySize, size-copySize);
			VipArrFree(pTemp);
//			delete [] pTemp;
		}
	}
	else
	{
		_AllocateBuffer(size);
	}
}

template <class T>
void CVipArrBase <T>::_MakeDoubleSize(int sizeAdd)
{
	int size = GetSize() + sizeAdd;
	if (size > m_nSizeBuf)
		_Reallocate( size * 2);
}

#define DEFINE_ARRAY_MONADIC1(OPERATOR, TYPE, ARR_TYPE, OP2_TYPE)		\
ARR_TYPE operator OPERATOR (const ARR_TYPE & OP1, const OP2_TYPE & OP2)	\
{																		\
	int size = OP1.GetSize();											\
	ARR_TYPE ret; ret.SetSize(size);									\
	      TYPE *pRet = ret.GetData     ();								\
	const TYPE *pOP1 = OP1.GetDataConst();								\
	for (int i = 0 ; i < size ; i++)									\
		pRet[i] = pOP1[i] OPERATOR OP2;									\
	return ret;															\
}

#define DEFINE_ARRAY_MONADIC2(OPERATOR, TYPE, ARR_TYPE, OP2_TYPE)		\
ARR_TYPE operator OPERATOR (const OP2_TYPE & OP2, const ARR_TYPE & OP1)	\
{																		\
	int size = OP1.GetSize();											\
	ARR_TYPE ret; ret.SetSize(size);									\
	      TYPE *pRet = ret.GetData     ();								\
	const TYPE *pOP1 = OP1.GetDataConst();								\
	for (int i = 0 ; i < size ; i++)									\
		pRet[i] = OP2 OPERATOR pOP1[i];									\
	return ret;															\
}

template <class T> 
class CVipArray : public CVipArrBase <T>
{
public:
	CVipArray() {}
	CVipArray(const CVipArray <T> & Src) {  _Copy(Src); }
	CVipArray(int nInternalBufSize     ) { CVipArrBase <T>::_Allocate(nInternalBufSize); CVipArrBase <T>m_nSizeArray = 0; };
	// Logic 문제로 explicit사용 못함.

	virtual ~CVipArray() 
	{
		CVipArrBase <T>::_Release();
	};

protected:
	virtual void _ElementAloc (T * pElements, int nCount)
	{
		memset((void*)pElements, 0, nCount * sizeof(T));
		for (; nCount--; pElements++)
 			::new ( (void*)pElements ) T;
	}

	virtual void _ElementFree (T * pElements, int nCount)
	{
		for (; nCount--; pElements++)
			pElements->~T();
	}
	
public:
	virtual void SetDataArray(const T *pSrcArray, int start, int size)
	{
		VIP_ASSERT( (start + size) <= m_nSizeBuf, "SetDataArray", "Error");
		T *pDst = CVipArrBase <T>::m_pArray + start;

		int count = size;
		while(count--)
			*(pDst++) = *(pSrcArray++);
//		CVipArrBase <T>::m_nSizeArray = start + size;
	}
};

template <class T>
std::ostream & operator << (std::ostream &s, const CVipArrBase <T> &src)
{
	int size = src.GetSize();
	s.write((const char*)&size, sizeof(size));
	s.write( (char*)src.GetDataConst(), src.GetSize() * sizeof(T));
	return s;
}

template <class T>
std::istream & operator >> (std::istream &s, CVipArrBase <T> &src)
{
	int size;
	s.read(( char*)&size, sizeof(size));
	src.SetSize(size);
	s.read( (char*)src.GetData(), size * sizeof(T));
	return s;
}

template <class T>
std::ostream & operator << (std::ostream &s, const CVipArray <T> &src)
{
	int size = src.GetSize();
	s.write((const char*)&size, sizeof(size));
	const T *pSrc = src.GetDataConst();
	for (int i = 0 ; i < size ; i++)
		s << pSrc[i];
	return s;
}

template <class T>
std::istream & operator >> (std::istream &s, CVipArray <T> &src)
{
	int size;
	s.read(( char*)&size, sizeof(size));
	src.SetSize(size);
	T *pSrc = src.GetData();

	for (int i = 0 ; i < size ; i++)
		s >> pSrc[i];
	return s;
}

template <class T> DEFINE_ARRAY_MONADIC1(+, T, CVipArrBase <T> , double);
template <class T> DEFINE_ARRAY_MONADIC2(+, T, CVipArrBase <T> , double);
template <class T> DEFINE_ARRAY_MONADIC1(+, T, CVipArrBase <T> , T     );
template <class T> DEFINE_ARRAY_MONADIC2(+, T, CVipArrBase <T> , T     );

template <class T> DEFINE_ARRAY_MONADIC1(-, T, CVipArrBase <T> , double);
template <class T> DEFINE_ARRAY_MONADIC2(-, T, CVipArrBase <T> , double);
template <class T> DEFINE_ARRAY_MONADIC1(-, T, CVipArrBase <T> , T     );
template <class T> DEFINE_ARRAY_MONADIC2(-, T, CVipArrBase <T> , T     );

template <class T> DEFINE_ARRAY_MONADIC1(*, T, CVipArrBase <T> , double);
template <class T> DEFINE_ARRAY_MONADIC2(*, T, CVipArrBase <T> , double);
template <class T> DEFINE_ARRAY_MONADIC1(*, T, CVipArrBase <T> , T     );
template <class T> DEFINE_ARRAY_MONADIC2(*, T, CVipArrBase <T> , T     );

template <class T> DEFINE_ARRAY_MONADIC1(/, T, CVipArrBase <T> , double);
template <class T> DEFINE_ARRAY_MONADIC2(/, T, CVipArrBase <T> , double);
template <class T> DEFINE_ARRAY_MONADIC1(/, T, CVipArrBase <T> , T     );
template <class T> DEFINE_ARRAY_MONADIC2(/, T, CVipArrBase <T> , T     );


#endif // !defined(AFX_VIPARRAY_H__90AF9A28_054D_48B9_AE3D_2109CDEDFDAB__INCLUDED_)
