//////////////////////////////////////////////////////////////////////
//
// VipWndToolsFunc.h: Include All Header & Library of Vip Library
//
//////////////////////////////////////////////////////////////////////

/** @file VipWndToolsFunc.h */

#if !defined(_VipWndToolsFunc_Include)
#define _VipWndToolsFunc_Include


#ifdef _viplib_emb_
	#define VIPWNDTOOLS_API
#else
	#ifdef VIPWNDTOOLS_EXPORTS
		#define VIPWNDTOOLS_API __declspec(dllexport)
	#else
		#define VIPWNDTOOLS_API __declspec(dllimport)
	#endif
#endif

#include "vipl.h"

VIPWNDTOOLS_API ByteImage      LoadImageFromFileDialog();
//ByteImageArray LoadImageArrayFromFileDialog();
VIPWNDTOOLS_API bool OpenImageFile(char *strBuf);

VIPWNDTOOLS_API bool DrawIplImage(HDC hdc, const IplImage* img,
				  int nSrcX, int nSrcY, int nSrcW, int nSrcH,
				  int nDstX, int nDstY, int nDstW, int nDstH);

VIPWNDTOOLS_API bool DrawIplImage(HDC hdc, const ByteImage &img);

// Image and Matrix Debugging Functions
/*
CString GetInfo(const CVipImgBase &image);

void Trace2DBuf(const double *src, int srcStep, int width, int height);
void Trace2DBuf(const  float *src, int srcStep, int width, int height);
void Trace2DBuf(const    int *src, int srcStep, int width, int height);
void Trace2DBuf(const  short *src, int srcStep, int width, int height);
void Trace2DBuf(const USHORT *src, int srcStep, int width, int height);
void Trace2DBuf(const   BYTE *src, int srcStep, int width, int height);
void Trace2DBuf(const   char *src, int srcStep, int width, int height);

template <class T>
void TraceMatrix(const CMatrix <T> &src)
{
	Trace2DBuf((T*)src.GetBufferConst(), src.GetWidthStep(), src.GetColSize(), src.GetRowSize());
}

DefImgTempl
void TraceImage(const TemplImage &src)
{
	Trace2DBuf((T*)src.GetBufferConst(), src.GetWidthStep(), src.GetXSize  (), src.GetYSize  ());
}

template <class T>
CString GetStringData(const CMatrix <T> &matrix, const char * form = NULL)
{
	char defForm[10];

	double mmax = GetMax( matrix );
	double mmin = GetMin( matrix );

	int maxVal = (int)::floor( mmax );
	int minVal = (int)::floor( mmin );
	int max    = (minVal < 0) ? __max(-minVal, maxVal) : maxVal;

	int digit = 12;
	for( ;max != 0 ; digit++)
		max /= 10;

	if(form) sprintf(defForm , " %%%s ", form);
	else     sprintf(defForm , "%%%d.10lf ", digit, form);

	int nRow = matrix.GetRowSize();
	int nCol = matrix.GetColSize();

	CString ret;
	char tmpNum[100];
	for (int r = 0; r < nRow; r++)
	{
		for (int c = 0; c < nCol; c++)
		{
			sprintf(tmpNum, defForm , matrix.Read(r, c));
			ret += tmpNum;
		}

		if (r < nRow-1)
			ret += "\n";
	}

	return ret;
};

#ifdef _DEBUG
	#define TRACE_MAT(mat) { TRACE(_T("matrix name : "#mat"\n")); TraceMatrix(mat); }
	#define TRACE_IMG(img) { TRACE(_T("image  name : "#img"\n")); TraceImage (img); }
#else
	#define TRACE_MAT(mat) ((void)0)
	#define TRACE_IMG(img) ((void)0)
#endif
*/

#endif

//void      Plot  (const DoubleMatrix &y, const char *name = NULL);
//void      Plot  (const DoubleMatrix &y, const DoubleMatrix &x, double *limit = NULL, const char *name = NULL);
//void      Quiver(const DoubleMatrix &x, const DoubleMatrix &y, double dx = 1, double dy = 1, const char *name = NULL);
//void      Quiver(const DoubleMatrix &x, const DoubleMatrix &y, double *delta = NULL, double *limit = NULL, const char *name = NULL);
