#if !defined(_VIP_DEBUG_MACRO)
#define _VIP_DEBUG_MACRO

//////////////////////////////////////////////////////////////////////
// Debug Macro
//////////////////////////////////////////////////////////////////////
#include "ViplBase.h"

#ifndef _WINDOWS
	#include <stdio.h>
	#include <stdlib.h>
	#include <assert.h>
	#include <string.h>

	#ifndef BOOL
		typedef int		BOOL;
	#endif

	#ifndef TRUE
		#define TRUE	1
	#endif

	#ifndef FALSE
		#define FALSE	0
	#endif
#endif

#ifdef _MFC_VER
	#define VipDebugBreak() AfxDebugBreak()
#else
	#ifdef _WINDOWS
		#define VipDebugBreak() _asm { int 3 }
	#else
		#define VipDebugBreak() assert(0)
	#endif
#endif

#ifdef _DEBUG
	VIPLIB_API int VipAssertFailedLine(const char *condition, const char *filename, int linenumber, const char *funcname, const char *format, ...);
	#define VIP_ASSERT(f, funcname, message)										\
		do																			\
		{																			\
			if ( !(f) &&															\
				VipAssertFailedLine(#f, __FILE__, __LINE__, funcname, message))		\
				VipDebugBreak();													\
		} while (0);

	#define CHECK_NULL(input, funcname)												\
		VIP_ASSERT(!input.IsEmpty(), funcname, "Image is Not Allocated");

	#define CHECK_DIM_IMG(input1, input2, funcname)									\
	{																				\
		CHECK_NULL((input1), funcname);												\
		CHECK_NULL((input2), funcname);												\
		char SizeError[] = "Dimension of These Two Image have to be same";			\
		/*char ROIError[]  = "ROI Size of Thease Two Image have to be same";*/		\
		char *pErrorMessage = NULL;													\
		if( (input1).roiWidth()      != (input2).roiWidth()  ||							\
			(input1).roiHeight()     != (input2).roiHeight() ||							\
			(input1).GetCSize()   != (input2).GetCSize())							\
			pErrorMessage = SizeError;												\
																					\
		if(pErrorMessage)															\
		{																			\
			if(VipAssertFailedLine("NULL", __FILE__, __LINE__,						\
				funcname,															\
				"%s\n"				/* error message */								\
				"----------------------------------\n"								\
				"image1 = %s\n"		/* image1 name   */								\
				"----------------------------------\n"								\
				"image2 = %s\n",	/* image2 name   */								\
				pErrorMessage,														\
				#input1,															\
				#input2 															\
				) )													\
				VipDebugBreak();													\
		}																			\
	}

	#define CHECK_RANGE_IMG(xx, yy, cc)												\
		if(	xx < 0 || xx >= GetXSize() ||											\
			yy < 0 || yy >= GetYSize() ||											\
			cc < 0 || cc >= GetCSize() )											\
		{																			\
			if(VipAssertFailedLine(													\
				"NULL",																\
				__FILE__,															\
				__LINE__,															\
				"operator ()",														\
				"Tried to Access Wrong Pixel\n"										\
				"accessed point	: [x=%d, y=%d, channel=%d]\n"						\
				"image range	: [x=%d, y=%d, channel=%d]",						\
				xx,																	\
				yy,																	\
				cc,																	\
				GetXSize(),															\
				GetYSize(),															\
				GetCSize()) )														\
				VipDebugBreak();													\
		}

	#define CHECK_RANGE_MAT(r, c)													\
		if( r < 0 || r >= GetRowSize() ||											\
			c < 0 || c >= GetColSize() )											\
		{																			\
			if(VipAssertFailedLine("NULL", __FILE__, __LINE__,						\
				"operator ()",														\
				"row(%d) col(%d) value Exceed Range(%d, %d)",						\
				r,																	\
				c,																	\
				GetRowSize(),														\
				GetColSize()) )														\
				VipDebugBreak();													\
		}

	#define CHECK_DIM_MATRIX(op1, op2)												\
		{																			\
			VIP_ASSERT(																\
			op1.GetRowSize() == op2.GetRowSize() &&									\
			op1.GetColSize() == op2.GetColSize(),									\
			"*",																	\
			"Matrix Size does not Match");											\
		}
#else
	#define VIP_ASSERT(f, funcname, message)				((void)0)
	#define CHECK_RANGE_IMG(xx, yy, cc)						((void)0)
	#define CHECK_DIM_MATRIX(op1, op2)						((void)0)
	#define CHECK_RANGE_MAT(r, c)							((void)0)
	#define CHECK_NULL(input , funcname)					((void)0)
	#define CHECK_DIM_IMG(input1, input2, funcname)			((void)0)
/*
	#ifndef _WINDOWS
		#define ASSERT(f) ((void)0)
	#endif

	#ifndef ASSERT
		#define ASSERT(f)																\
			if (!(f))																	\
			{																			\
				char cmd;																\
				while(1)																\
				{																		\
					printf("File	: %s\nLine	: %d\nCondition	: %s\n\n",				\
						__FILE__,														\
						__LINE__,														\
						#f);															\
					printf("[c]ancel [r]etry c[o]ntinue\n");							\
					cmd = getchar();													\
					if(cmd == 'c' || cmd == 'r' || cmd == 'o')							\
						break;															\
				}																		\
				switch(cmd)																\
				{																		\
				case 'c':																\
					exit(0);															\
				case 'r':																\
					assert(0);															\
				case 'o':																\
					break;																\
				}																		\
			}
	#endif
*/
#endif

#endif // end _VIP_DEBUG_MACRO
