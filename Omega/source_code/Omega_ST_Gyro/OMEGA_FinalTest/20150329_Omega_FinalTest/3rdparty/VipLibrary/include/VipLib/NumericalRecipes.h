#if !defined(_NRC_HEADER_)
#define _NRC_HEADER_

#include "ViplBase.h"
#include "VipDebug.h"



namespace NumericalRecipes
{
	//////////////////////////////////////////////////////////////////////
	// Nemerical Recipes in C
	//////////////////////////////////////////////////////////////////////

	VIPLIB_API void fourn(double data[], unsigned long nn[], int ndim, int isign);
	VIPLIB_API void fourn(float  data[], unsigned long nn[], int ndim, int isign);

	VIPLIB_API void four1(float  data[], unsigned long nn, int isign);
	VIPLIB_API void four1(double data[], unsigned long nn, int isign);

	VIPLIB_API void tred2(double **a, int n, double d[], double e[]);
	VIPLIB_API void tred2(float  **a, int n, float  d[], float  e[]);

	VIPLIB_API bool tqli(double d[], double e[], int n, double **z);
	VIPLIB_API bool tqli(float  d[], float  e[], int n,  float **z);

	VIPLIB_API void gaussj(double **a, int n, double **b, int m);
	VIPLIB_API void gaussj(float  **a, int n, float  **b, int m);
	
	VIPLIB_API void svdcmp(double **a, int m, int n, double *w, double **v);
	VIPLIB_API void svdcmp(float  **a, int m, int n, float  *w, float  **v);

	VIPLIB_API void choldc(double **a, int n, double *p);
	VIPLIB_API void choldc(float  **a, int n, float  *p);

	VIPLIB_API void svbksb(double **u, double w[], double **v, int m, int n, double b[], double x[]);
	VIPLIB_API void qrdcmp(float **a, int n, float *c, float *d, int *sing);
	VIPLIB_API void qrdcmp(double **a, int n, double *c, double *d, int *sing);
};


#endif