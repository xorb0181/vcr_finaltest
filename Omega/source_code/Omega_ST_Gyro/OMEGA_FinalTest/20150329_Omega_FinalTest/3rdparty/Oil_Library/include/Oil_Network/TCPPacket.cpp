// TCPPacket.cpp: implementation of the TCPPacket class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "TCPPacket.h"
#include "../Oil_Thread/Sync.h"
#include "stdio.h"
#include "assert.h"

//#define _USE_DES_ENCODING_

#ifdef _USE_DES_ENCODING_
	#include "DES.h"
#endif

#ifdef _LINUX
	#include <stdio.h>
	#include <string.h>
	#include <sys/select.h>
	#include <sys/types.h>
	#include <sys/socket.h>
	#include <unistd.h>
	#include <stdarg.h>
	#include <errno.h>
#else
// 	#ifdef _DEBUG
// 	#undef THIS_FILE
// 	static char THIS_FILE[]=__FILE__;
// 	#define new DEBUG_NEW
// 	#endif
#endif

#ifdef _LINUX
	typedef struct timeval TIMEVAL;
#endif

//CSync g_csRecv("g_csRecv");
//CSync g_csSend("g_csSend");

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
#ifdef _LINUX
	int 			WSAGetLastError();
#endif

TCPPacket::TCPPacket(int BodySize)
{
	m_pHeader 		 = NULL;
	m_pData			 = NULL;

	_Allocate(BodySize);
}

TCPPacket::~TCPPacket()
{
	if(m_pData     ) delete [] m_pData;
}

void TCPPacket::_Allocate(int BodySize)
{
	if (m_pData) delete [] m_pData;

	m_nBodySize = BodySize;
	m_pData     = new char[m_nBodySize + sizeof(header_tag)];

	memset(m_pData, 0, m_nBodySize + sizeof(header_tag) );

	m_pHeader = (header_tag *) m_pData;
	m_pBody   = m_pData + sizeof(header_tag);
}

void TCPPacket::_Reallocate(const int BodySize)
{
	if( (m_nBodySize-1) < BodySize )
	{
		int befSize = m_nBodySize + sizeof(header_tag);
		char *pData = new char[befSize];
		memcpy(pData, m_pData, befSize);
		
		_Allocate(BodySize*2);
		memcpy(m_pData, pData, befSize);
		
		delete [] pData;
	}
}

int TCPPacket::SendFormatString(SOCKET hSocket, const char* format, ...)
{
	va_list		args;
	char		temp[2048];

	va_start(args, format);
	vsprintf(temp, format, args);
	
	return SendString(hSocket, temp);
}

int TCPPacket::SendBinary(SOCKET hSocket, const char* input, int bufSize)
{
	return _SendBinBuf(hSocket, input, bufSize, MSG_BINARY);
}

int TCPPacket::SendString(SOCKET hSocket, const char *input)
{
	return _SendBinBuf(hSocket, input, strlen(input) + 1, MSG_STRING);
}


int TCPPacket::_SendBinBuf(SOCKET hSocket, const char* input, int bufSize, eMsgType msgType)
{
	int bodysize = bufSize;
	
	_Reallocate(bodysize);
	
	memset(m_pBody, 0    , m_nBodySize);
	memcpy(m_pBody, input, bodysize   );

	m_pHeader->iden       = MAGIC_NUM ;
	m_pHeader->msgLength  = bodysize  ;
	m_pHeader->msgType    = msgType   ;
	m_pHeader->encodeType = ENCODE_NORMAL;
	m_pHeader->timeStamp  = GetTickCount();
	
	int size = sizeof(header_tag) + bodysize;
	
	int ret = SockSendData(hSocket, m_pData, size);
	if( ret <= 0 ) m_LastError = WSAGetLastError();
	return ret;
}

int  TCPPacket::SendDataInBuffer(SOCKET hSocket)
{
	m_pHeader->iden       = MAGIC_NUM ;
	m_pHeader->encodeType = ENCODE_NORMAL;
	m_pHeader->msgType    = MSG_BINARY;
	m_pHeader->timeStamp  = GetTickCount();
	
	int size = sizeof(header_tag) + m_pHeader->msgLength;
	int ret = SockSendData(hSocket, m_pData, size);
	if( ret <= 0 ) m_LastError = WSAGetLastError();
	return ret;
}

void TCPPacket::ResetBuffer()
{
	m_pHeader->msgLength = 0;
}

void TCPPacket::AddDataToBuffer(const char *pBuf, int size)
{
	if (!size) return;

	_Reallocate(m_pHeader->msgLength + size);
	memcpy(m_pBody + m_pHeader->msgLength, pBuf, size);
	m_pHeader->msgLength += size;
}

void TCPPacket::AddDataToBuffer(int intData)
{
	_Reallocate(m_pHeader->msgLength + sizeof(int));
	memcpy(m_pBody + m_pHeader->msgLength, &intData, sizeof(int));
	m_pHeader->msgLength += sizeof(int);
}

int TCPPacket::_RecvHeader(SOCKET hSocket, int timeout)
{
	int		ret;
	ret = SockRecvData(hSocket, (char *)m_pHeader, sizeof(header_tag), timeout);
	if( ret <= 0 )
	{
		m_LastError = WSAGetLastError();
		return ret;
	}

	if( m_pHeader->iden != MAGIC_NUM )
		return ERR_WRONG_HEADER;

	return ret;
}

int TCPPacket::_RecvBody(SOCKET hSocket, int timeout)
{
	// 메모리가 부족하다면 더 늘린다.
	_Reallocate( m_pHeader->msgLength );
	int ret = SockRecvData(hSocket, m_pBody, m_pHeader->msgLength, timeout);
	if( ret <= 0 )
	{
		m_LastError = WSAGetLastError();
		return ret;
	}
	switch(m_pHeader->encodeType)
	{
	case ENCODE_DES:
#ifdef _USE_DES_ENCODING_
		des_decode_replace(m_pBody, m_desKey, m_pHeader->msgLength);
#else
		assert(0);
#endif
		break;
	}
	return ret;
}

int TCPPacket::Receive(SOCKET hSocket, int timeout)
{
	assert(hSocket);
	int	ret;
	int size = 0;

	ret = _RecvHeader(hSocket, timeout);
	if( ret <= 0 ) return ret;
	size += ret;

	ret = _RecvBody  (hSocket, timeout);
	if( ret <  0 ) return ret;
	size += ret;

	return size;
}

int TCPPacket::SockSendData(SOCKET hSocket, const char *pBuf, int n)
{
	int			nleft;
	int			nwrite;
	char		*ptr;
	int			ret;

	fd_set		writefds;
	
	ptr = (char *)pBuf;
	nleft = n;
	
	while( nleft > 0 )
	{
		if (nleft != n)
			printf("[TCPPacket][SockSendData] Retry send[%d/%d]\n", nleft, n);
		FD_ZERO(&writefds);
		FD_SET(hSocket, &writefds);

		TIMEVAL tv;
		memset(&tv, 0, sizeof(TIMEVAL));
		tv.tv_sec  = 1;//(long)(1 / 1000);
		tv.tv_usec = 0;//(long)(1 % 1000);
		ret = select(hSocket+1, NULL, &writefds, NULL, &tv);

		switch( ret )
		{
		case 0 : continue; // timeout
		case SOCKET_ERROR : return ERR_SOCKET_ERROR;
		}

		if( ret == 0 ) // connection closed successfully
		{
			printf("[TCPPacket][SockSendData] Connection closed\n");
			return 0;
		}

		if( FD_ISSET(hSocket, &writefds) )
		{
//			g_csSend.Lock();
			nwrite = send(hSocket, ptr, nleft, 0 );
//			g_csSend.Unlock();

			if (nwrite == SOCKET_ERROR)
			{
				printf("[TCPPacket][SockSendData] send error [%d]\n", hSocket);
				return ERR_SOCKET_ERROR;
			}

			if( nwrite == 0 )
				break;
		
			nleft -= nwrite;
			ptr   += nwrite;
		}
	}

	return (n-nleft);		
}

int TCPPacket::SockRecvData(SOCKET hSocket, char *pBuf, int n, int msTimeOut)
{
	char *ptr     = (char *)pBuf;
	int   nleft   = n;

	// timeout 시간 지정
	int timeOut = (msTimeOut == -1) ? 1 : msTimeOut;

	while( nleft > 0 )
	{
		fd_set readfds;
		FD_ZERO(&readfds);
		FD_SET(hSocket, &readfds);

		TIMEVAL tv;
		memset(&tv, 0, sizeof(TIMEVAL));
		tv.tv_sec  = 1;
		tv.tv_usec = 0;
		// 주의 : Time out을 무한대로 걸어 놓으면 socket close시 block 됨.
		int ret = select(hSocket+1, &readfds, NULL, NULL, &tv);

		switch( ret )
		{
		case 0 : // timeout
			{
				if (msTimeOut == -1)
					continue;
				else
					return ERR_TIME_OUT;
			}
			break;
		case SOCKET_ERROR : return ERR_SOCKET_ERROR;
		}

		if( FD_ISSET(hSocket, &readfds) )
		{
//			g_csRecv.Lock();
			int nread = recv(hSocket, ptr, nleft, 0);
//			g_csRecv.Unlock();
			switch(nread)
			{
			case            0: return ERR_SOCKET_CLOSED;
			case SOCKET_ERROR: return ERR_SOCKET_ERROR ;
			default:
				{
					nleft -= nread;
					ptr   += nread;
				}
				break;
			}
		}

		Sleep(10);
	}

	return (n-nleft);
}

int TCPPacket::SockRecvWait  (SOCKET hSocket, int msTimeOut)
{
	int timeOut = (msTimeOut == -1) ? 1000 : msTimeOut;

	while (1)
	{
		fd_set readfds;
		FD_ZERO(&readfds);
		FD_SET(hSocket, &readfds);

		TIMEVAL tv;
		memset(&tv, 0, sizeof(TIMEVAL));
		tv.tv_sec  = (long)(timeOut / 1000);
		tv.tv_usec = (long)(timeOut % 1000);
		// 주의 : Time out을 무한대로 걸어 놓으면 socket close시 block 됨.
		int ret = select(hSocket+1, &readfds, NULL, NULL, &tv);

		switch( ret )
		{
		case 0 : // timeout
			{
				if (msTimeOut == -1)
					continue;
				else
					return ERR_TIME_OUT;
			}
			break;
		case SOCKET_ERROR : return ERR_SOCKET_ERROR;
		}
	}

	return 1;
}
