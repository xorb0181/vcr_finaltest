// LogServer.cpp: implementation of the CLogServer class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "LogServer.h"

#include "string.h"
#include "stdarg.h"
#include <stdio.h>

// #ifdef _DEBUG
// #undef THIS_FILE
// static char THIS_FILE[]=__FILE__;
// #define new DEBUG_NEW
// #endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
#include "SingleSock.h"
#include "../Oil_Thread/Sync.h"

CSingleSock gLogSSock(0, "LogServer");
CSync       gSyncLogTCP ("LogServerTCP");

OILLIB_API bool LogServerOpen(int nPort)
{
	return gLogSSock.Listen(nPort);
}

OILLIB_API void LogServerClose()
{
	gLogSSock.Close();
}

OILLIB_API void RemoteLog0(const char* format, ...)
{
	if (!gLogSSock.IsConnected())
		return;

	va_list		args;
	char		temp[512];
	temp[0] = '0';

	va_start(args, format);
	vsprintf(temp+1, format, args);

	gSyncLogTCP.Lock();
	gLogSSock.SendString(temp);
	gSyncLogTCP.Unlock();
}

OILLIB_API void RemoteLog1(const char* format, ...)
{
	if (!gLogSSock.IsConnected())
		return;

	va_list		args;
	char		temp[512];
	temp[0] = '1';

	va_start(args, format);
	vsprintf(temp+1, format, args);
	gSyncLogTCP.Lock();
	gLogSSock.SendString(temp);
	gSyncLogTCP.Unlock();
}

static FILE *fpFile = NULL;

OILLIB_API bool FileLogOpen(const char *filename)
{
	if (fpFile)
		return false;

	fpFile = fopen(filename, "a");
	return fpFile ? true : false;
}

OILLIB_API void FileLogClose()
{
	if (fpFile)
	{
		fclose(fpFile);
		fpFile = NULL;
	}
}

OILLIB_API void FileLog(const char* format, ...)
{
	if (!fpFile)
		return;

	va_list		args;
	char		temp[512];
	temp[0] = '0';
	
	va_start(args, format);
	vsprintf(temp+1, format, args);
// #ifdef _WIN32_WCE
// #ifndef _LINUX
// 	CTime time = CTime::GetCurrentTime();
// 	fprintf(fpFile, "[%02d:%02d:%02d] %s", time.GetHour(),time.GetMinute(),time.GetSecond(), temp);
// #endif
// #else
// #endif
}
