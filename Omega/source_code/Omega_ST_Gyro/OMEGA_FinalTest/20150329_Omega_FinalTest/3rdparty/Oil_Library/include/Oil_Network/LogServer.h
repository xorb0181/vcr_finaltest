// LogServer.h: interface for the CLogServer class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_LOGSERVER_H__00C9E5D4_5555_4470_89E7_F1403271AB60__INCLUDED_)
#define AFX_LOGSERVER_H__00C9E5D4_5555_4470_89E7_F1403271AB60__INCLUDED_

#include "../OilBase.h"

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

OILLIB_API bool LogServerOpen(int nPort);
OILLIB_API void LogServerClose();

OILLIB_API bool FileLogOpen(const char *filename);
OILLIB_API void FileLogClose();

OILLIB_API void RemoteLog0(const char* format, ...);
OILLIB_API void RemoteLog1(const char* format, ...);
OILLIB_API void FileLog   (const char* format, ...);



#endif // !defined(AFX_LOGSERVER_H__00C9E5D4_5555_4470_89E7_F1403271AB60__INCLUDED_)
