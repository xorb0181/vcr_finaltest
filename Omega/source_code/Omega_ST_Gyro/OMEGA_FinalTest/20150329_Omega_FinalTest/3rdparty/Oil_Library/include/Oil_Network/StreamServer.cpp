// GCamServer.cpp: implementation of the CGCamServer class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "StreamServer.h"

// #include "Oil_ETC/VString.h"

#include <strstream>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

using namespace std;

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CStreamServer::CStreamServer() : CSingleSock(0), m_thread("StreamServer")
{
}

CStreamServer::~CStreamServer()
{
	Stop();
}

bool CStreamServer::_cbSendLoop(void *pVoid) { return ((CStreamServer*)pVoid)->_OnSendLoop(); }
bool CStreamServer::_OnSendLoop()
{
	if (!IsConnected())
	{
		Sleep(10); 
		return true; 
	}

	if (m_cbEvent)
	{
		{
			const BYTE *buf = NULL;
			int size = 0;
			m_cbEvent(m_pvEvent, CSingleSock::eLoop   , (int)&buf, (int)&size);
			if (buf)
				SendBinary((const char*)buf, size);
		}

		{
			const BYTE *buf = NULL;
			int size = 0;
			m_cbEvent(m_pvEvent, CSingleSock::eLoopRaw, (int)&buf, (int)&size);
			if (buf)
				SendRawData((const char*)buf, size);
		}
	}

	return true;
}

bool CStreamServer::Start(int portNum)
{
	if (Listen(portNum))
	{
		return m_thread.BeginThread(_cbSendLoop, this, OIL_THREAD_PRIORITY_NORMAL);
	}
	else
	{
		printf("[CStreamServer] Failed to open stream server [%d]\n", portNum);
		Close();
		return false;
	}
}

void CStreamServer::Stop()
{
	Close();
	m_thread.StopThread();
}
