// MMTimer.h:
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MMTIMER_H__123D3AA5_0FC2_4F3B_9740_22F63E54D291__INCLUDED_)
#define AFX_MMTIMER_H__123D3AA5_0FC2_4F3B_9740_22F63E54D291__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CMMTimer
{
private :
    int Initialized;
	__int64 Frequency;
	__int64 BeginTime;
	
public :
    CMMTimer() 
	{
		Initialized = QueryPerformanceFrequency( (LARGE_INTEGER *)&Frequency ); 
	};

    BOOL Begin()
	{
		if( !Initialized ) return 0;
		return QueryPerformanceCounter( (LARGE_INTEGER *)&BeginTime );
	}
    double End()
	{
		if( !Initialized ) return 0.0;
		__int64 endtime;
		QueryPerformanceCounter( (LARGE_INTEGER *)&endtime );
		__int64 elapsed = endtime - BeginTime;
		return (double)elapsed / (double)Frequency;
	}
	
	double GetTime()
	{
		if( !Initialized ) return 0.0; 
		__int64 endtime;
		QueryPerformanceCounter( (LARGE_INTEGER *)&endtime );
		return (double)endtime / (double)Frequency;
	}
	
    BOOL Available()
		{ return Initialized; }
    __int64 GetFreq()
		{ return Frequency; }
};

#endif // !defined(AFX_MMTIMER_H__123D3AA5_0FC2_4F3B_9740_22F63E54D291__INCLUDED_)
