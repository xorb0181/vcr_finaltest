// SockPacket.h: interface for the StringPacket class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_STRINGPACKET_H__25BE090C_6A36_4949_B428_FB5A1F05C13E__INCLUDED_)
#define AFX_STRINGPACKET_H__25BE090C_6A36_4949_B428_FB5A1F05C13E__INCLUDED_

#include "../OilBase.h"

#ifndef _LINUX
	#if _MSC_VER > 1000
	#pragma once
	#endif // _MSC_VER > 1000
#endif

class OILLIB_API TCPPacket  
{
public:
	TCPPacket(int BodySize = 4096);	
	virtual ~TCPPacket();

public:
	enum ePacketErrorType
	{
		ERR_TIME_OUT      = -1,
		ERR_SOCKET_ERROR  = -2,
		ERR_SOCKET_CLOSED = -3,
		ERR_WRONG_HEADER  = -4,
		ERR_USER_CLOSE    = -5
	};

	enum eMsgType
	{
		MSG_STRING       = 0,
		MSG_BINARY       = 1,
		MSG_FILETRANSFER = 2, // not used
		MSG_PING         = 3, // not used
		MSG_PONG         = 4  // not used
	};

public:
	enum eIden
	{
		MAGIC_NUM = 0x0fa4,   // not used
	};
	
	enum eEncodeType
	{
		ENCODE_NORMAL = 0,
		ENCODE_DES    = 1     // not used
	};

public:
	struct header_tag
	{
		int           iden      ; // 0x0fa4		올바른 패킷 인지 확인하기 위한 스크램블
		int           msgLength ; // Body Size
		eMsgType      msgType   ;
		eEncodeType   encodeType;
		unsigned long timeStamp ;
	};

protected:
	char       *m_pData;
    header_tag *m_pHeader;
	char       *m_pBody;
	int	  m_nBodySize;

	char  m_desKey[4];

	unsigned long m_LastError;

public:
	int Receive         (SOCKET hSocket, int timeout = 5000);

	int SendBinary      (SOCKET hSocket, const char* input, int bufSize);
	int SendString      (SOCKET hSocket, const char* input);
	int SendFormatString(SOCKET hSocket, const char* format, ...);

public:
	const char *  GetBody     () { return m_pBody;              }
	int           GetMsgSize  () { return m_pHeader->msgLength; }
	eMsgType      GetMsgType  () { return m_pHeader->msgType  ; }
	unsigned long GetTimeStamp() { return m_pHeader->timeStamp; }

public:
	void ResetBuffer();
	int  SendDataInBuffer(SOCKET hSocket);
	void AddDataToBuffer(const char *pBuf, int size);
	void AddDataToBuffer(int intData);

protected:
	void _Allocate(int BodySize);
	void _Reallocate(const int size);
	
protected:
	int _RecvBody   (SOCKET hSocket, int timeout);
	int _RecvHeader (SOCKET hSocket, int timeout);
	int _SendBinBuf (SOCKET hSocket, const char* input, int bufSize, eMsgType msgType);

public:
	static int SockRecvData  (SOCKET hSocket,       char *pBuf, int n, int msTimeOut);
	static int SockSendData  (SOCKET hSocket, const char *pBuf, int n);
	static int SockRecvWait  (SOCKET hSocket, int msTimeOut);
};

#endif // !defined(AFX_STRINGPACKET_H__25BE090C_6A36_4949_B428_FB5A1F05C13E__INCLUDED_)



