// SingleSock.h: interface for the CSingleAsyncSock class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_SINGLESOCK_H__8F90DF29_5514_463C_A795_8F7A675985A5__INCLUDED_)
#define AFX_SINGLESOCK_H__8F90DF29_5514_463C_A795_8F7A675985A5__INCLUDED_

#include "TCPPacket.h"
#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CSingleCSock : public CAsyncSocket
{
// Attributes
public:

// Operations
public:
	CSingleCSock();
	virtual ~CSingleCSock();

	void  *m_pvReceive;
	void (*m_cbReceive)(void *pVoid, const char *pBuf, int size);

	void  *m_pvOnClose;
	void (*m_cbOnClose)(void *pVoid, int nErrorCode);

	void  *m_pvOnConnect;
	void (*m_cbOnConnect)(void *pVoid, int nErrorCode);

	bool m_bIsConnected;

// Overrides
public:
	TCPPacket m_packet;
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSingleCSock)
	public:
	virtual void OnReceive(int nErrorCode);
	virtual void OnConnect(int nErrorCode);
	virtual void OnClose(int nErrorCode);
	//}}AFX_VIRTUAL

	// Generated message map functions
	//{{AFX_MSG(CSingleCSock)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG

// Implementation
protected:
};

class CSingleSSock : public CAsyncSocket
{
// Attributes
public:

// Operations
public:
	CSingleSSock();
	virtual ~CSingleSSock();

	void (*m_cbOnAccept)(void *pVoid, int nErrorCode);
	void *m_pvOnAccept;

// Overrides
public:
	
	CSingleCSock m_CSock;
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSingleSSock)
	public:
	virtual void OnAccept(int nErrorCode);
	//}}AFX_VIRTUAL

	// Generated message map functions
	//{{AFX_MSG(CSingleSSock)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG

// Implementation
protected:
};

class CSingleAsyncSock  
{
public:
	CSingleAsyncSock(int type, const char *strName = NULL);
	virtual ~CSingleAsyncSock();

public:
	void SetCallBackOnReceive(void *pVoid, void (*cbReceive  )(void *, const char *, int) );
	void SetCallBackOnAccept (void *pVoid, void (*cbOnAccept )(void *, int ) );
	void SetCallBackOnClose  (void *pVoid, void (*cbOnClose  )(void *, int ) );
	void SetCallBackOnConnect(void *pVoid, void (*cbOnConnect)(void *, int ) );

	void Listen(int portNum);
	bool Connect(const char *addr, int port);
	void Close();

	int  SendString(const char *pBuf);
	int  SendBinary(const char *pBuf, int size);
	int  SendFormatString(const char* format, ...);
	int  SendDataInBuffer();

	void ResetBuffer();
	void AddDataToBuffer(const char *pBuf, int size);
	void AddDataToBuffer(int    value);
	void AddDataToBuffer(float  value);
	void AddDataToBuffer(double value);

	bool IsConnected() { return m_SSock.m_CSock.m_bIsConnected; }

protected:
	const int m_type;
	TCPPacket m_sendPacket;

protected:
	CSingleSSock m_SSock;
};

#endif // !defined(AFX_SINGLESOCK_H__8F90DF29_5514_463C_A795_8F7A675985A5__INCLUDED_)
