// Sync.h: interface for the CSync class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_SYNC_H__67F4035F_7C9F_42B5_B5E1_8CD99E9A7DBF__INCLUDED_)
#define AFX_SYNC_H__67F4035F_7C9F_42B5_B5E1_8CD99E9A7DBF__INCLUDED_

#include "../OilBase.h"

#ifdef _LINUX
	#include <pthread.h>
#else
	#if _MSC_VER > 1000
	#pragma once
	#endif // _MSC_VER > 1000

	#include <WINBASE.h>
#endif

//#define DEAD_LOCK_CHECKING

class OILLIB_API CSync
{
public:
	CSync(const char *name = "none");
	virtual ~CSync();

protected:

#ifdef _LINUX
	pthread_mutex_t	  	m_mutex;
#else
	#ifdef DEAD_LOCK_CHECKING
		CMutex m_cs;
	#else
		CRITICAL_SECTION m_cs;
	#endif
#endif

	char *m_strName;

public:
	void Unlock();
	void Lock();
};

#endif // !defined(AFX_SYNC_H__67F4035F_7C9F_42B5_B5E1_8CD99E9A7DBF__INCLUDED_)

