#include "stdafx.h"
#include "ini.h"
#include "string.h"
#include "stdarg.h"
#include <stdio.h>

static void AllTrim(char *str)
{
	int len = strlen(str);
	int i;
	for (i = len-1 ; i >=  0 && str[i] == ' '; i--) {}
	str[ (len=i+1) ] = 0;
	for (i = 0     ; i < len && str[i] == ' '; i++) {}

	if (i)
		memcpy(str, str + i, len - i + 1);
}

int ReadLine(FILE *stream, char *line, int MaxSize)
{
	int count = 0;
	for (int i = 0 ; i < MaxSize ; i++)
	{
		int chBuffer = fgetc(stream);
//		if(chBuffer == '\n' || feof(stream) != 0)
		if(chBuffer == '\n' || chBuffer == EOF)
		{
			line[count] = 0;
			return count;
		}
		
		if((char)chBuffer != '\r')
		{
			line[count] = (char)chBuffer;
			count++;
		}
	}
//	printf("[INI Reader   ] Exceed MaxSize in Reading Line\n");
	return -1;
}

/*
bool ReadINIData(const char *filename, const char *name, char *value, int size)
{
	FILE *stream;
	
	if( (stream  = fopen(filename, "r" )) == NULL )
    {
		printf("Cannot Open %s", filename);
		return false;
    }
	
	while(!feof(stream))
    {
		char strBuffer[256];
		int ret = ReadLine(stream, strBuffer, 255);
		
		char TokenArray[2][256];
		memset(TokenArray, 0, 2 * 256);
		
		char *Token;
		char Del[] = "=";
		
		Token = strtok(strBuffer, Del);
		
		int Count = 0;
		while (Token != NULL)
		{
			if(strlen(Token) > 255)
			{
				printf("Token is over than 255");	//확인
				fclose(stream);
				return false;
			}
			strcpy(TokenArray[Count], Token);	//확인
			AllTrim(TokenArray[Count]);
			
			Token = strtok(NULL, Del);
			Count++;
			if(Count > 2)
				break;
		}
		
		if(Count != 2)
			continue;
		
		if(!strcmp(TokenArray[0], name))
		{
			if(strlen(TokenArray[1]) > (unsigned int)(size - 1))
			{
				printf("Char Size Error in Reading %s (data = %s) (size = %d)\n",
					name,
					TokenArray[1],
					strlen(TokenArray[1])
					);	//확인
				fclose(stream);
				return false;
			}
			strcpy(value, TokenArray[1]);	//확인
			fclose(stream);
			printf("%s: %s = %s\n", filename, name, value);
			fclose(stream);
			return true;
		}
    }
	
	fclose(stream);
	printf("Cannot Find %s\n", name);	//확인
	return false;
}
*/

static bool ReadINIData(const char *filename, const char *section, const char *name, char *value, int size)
{
	FILE *stream;
	
	if( (stream  = fopen(filename, "r" )) == NULL )
    {
		printf("[INI Reader   ] Cannot Open %s\n", filename);
		return false;
    }
	
	char strBuffer[256];
	
	bool bSucess = false;
	
	while(!feof(stream))
    {
		if (ReadLine(stream, strBuffer, 255) == -1)
			goto exit;
		AllTrim(strBuffer);
		if (strBuffer[0] != '[')
			continue;
		if (!strncmp(strBuffer+1, section, strlen(section)))
		{
			bSucess = true;
			break;
		}
	}
	
	if (!bSucess)
		goto exit;
	
	while(!feof(stream))
    {
		ReadLine(stream, strBuffer, 255);
		AllTrim(strBuffer);
		if (strBuffer[0] == '[')
			break;
		
		char TokenArray[2][256];
		memset(TokenArray, 0, 2 * 256);
		
		char *Token;
		char Del[] = "=";
		
		Token = strtok(strBuffer, Del);
		
		int Count = 0;
		while (Token != NULL)
		{
			if(strlen(Token) > 255)
			{
				printf("[INI Reader   ] Token is over than 255\n");	//확인
				goto exit;
			}
			strcpy(TokenArray[Count], Token);	//확인
			AllTrim(TokenArray[Count]);
			
			Token = strtok(NULL, Del);
			Count++;
			if(Count > 2)
				break;
		}
		
		if(Count != 2)
			continue;
		
		if(!strcmp(TokenArray[0], name))
		{
			if(strlen(TokenArray[1]) > (unsigned int)(size - 1))
			{
				printf("[INI Reader   ] Char Size Error in Reading %s (data = %s) (size = %d)\n",
					name,
					TokenArray[1],
					(int)strlen(TokenArray[1])
					);	//확인
				goto exit;
			}
			strcpy(value, TokenArray[1]);	//확인
			fclose(stream);
			printf("[INI Reader   ] %s: %s = %s\n", filename, name, value);
			return true;
		}
    }
	
exit:
	fclose(stream);
	printf("[INI Reader   ] %s: %s = none\n", filename, name);
	return false;
}

/*
static void FindName(const char *buffer, const int bufSize, const char *name, int &posStart, int &posEnd)
{
	posStart = -1;
	posEnd = 0;
	int nameSize = strlen(name);
	
	for (int i = 0 ; i < bufSize ; i++)
	{
		if (posStart == -1)
		{
			if (!strncmp(buffer + i, name, nameSize))
				posStart = i;
		}
		else
		{
			if (buffer[i] == '\n')
			{
				posEnd = i+1;
				break;
			}
		}
	}

	if (posStart == -1)
		posStart = 0;
}

static bool WriteINIData(const char *filename, const char * index, const char *name, char *value)
{
	FILE *fp = fopen(filename, "rb");
	char *buffer = NULL;
	int   bufSize = 0;

	if(fp)
	{
		fseek(fp, 0, SEEK_END);
		bufSize = ftell(fp) + 1;
		buffer = new char[bufSize];
		memset(buffer, 0, sizeof(char) * bufSize);
		fseek(fp, 0, SEEK_SET);
		fread(buffer, 1, bufSize, fp);
		fclose(fp);
	}
	else
		return false;

	int posStart, posEnd; FindName(buffer, bufSize, name, posStart, posEnd);
	
	fp = fopen(filename, "wb");
	
	if (fp)
	{
		if (posStart)
			fwrite(buffer, 1, posStart, fp);
		fprintf(fp, "%s=%s\r\n", name, value);
		fwrite(buffer + posEnd, 1, bufSize-posEnd, fp);
		fclose(fp);
		delete [] buffer;
		return true;
	}
	
	delete [] buffer;
	return false;
}
/*/
static int GetNextLine(const char *buffer, int size)
{
	for (int i = 0 ; i < size ; i++)
	{
		if (buffer[i] == '\n')
		{
			for (int j = i ; j < size ; j++)
			{
				if (buffer[j] != ' ')
					return j+1;
			}
		}
	}
	return size+1;
}

static void FindName(const char *buffer, const int bufSize, const char *name, int &posStart, int &posEnd)
{
	int pos = 0;
	posStart = 0;
	posEnd   = 0;
	int nameSize = strlen(name);

	do 
	{
		bool bEnd = false;
		if (!strncmp(buffer + pos, name, nameSize))
		{
			posStart = pos;
			posEnd   = pos + 1 + GetNextLine(buffer+pos+1, bufSize-pos-1);
			bEnd = true;
		}

		if (bEnd)
			break;

		pos += GetNextLine(buffer+pos, bufSize-pos);
	} while (pos < bufSize);
}

static int FindChar(const char *buffer, int size, char v)
{
	for (int i = 0 ; i < size ; i++)
	{
		if (buffer[i] == v)
			return i;
	}
	return size;
}

static bool FindIndexPos(const char *buffer, int bufSize, const char *index, int &posStart, int &posEnd)
{
	int pos = 0;
	posStart = 0;
	posEnd   = 0;
	int nameSize = strlen(index);

	bool bFoundIndex = false;
	
	do 
	{
		bool bEnd = false;

		if ( *(buffer + pos) == '[')
		{
			if (!strncmp(buffer + pos + 1, index, nameSize))
			{
				bFoundIndex = true;
				bEnd = true;

				pos += GetNextLine(buffer+pos, bufSize-pos);
				posStart = pos;
				posEnd   = pos+1 + FindChar(buffer+pos+1, bufSize-pos-1, '[');
				return true;
			}
		}

		if (bEnd)
			break;
		pos++;
		pos += FindChar(buffer+pos, bufSize-pos, '[');
	} while (pos != -1 && pos < bufSize);

	return false;
}

static bool WriteINIData(const char *filename, const char * index, const char *name, char *value)
{
	FILE *fp = fopen(filename, "rb");
	char *buffer = NULL;
	int   bufSize = 0;

	if(fp)
	{
		fseek(fp, 0, SEEK_END);
		bufSize = ftell(fp) + 1;
		buffer = new char[bufSize];
		memset(buffer, 0, sizeof(char) * bufSize);
		fseek(fp, 0, SEEK_SET);
		fread(buffer, 1, bufSize, fp);
		fclose(fp);
	}
	else
		return false;

	int idxStart, idxEnd;
	bool bIndex = FindIndexPos(buffer, bufSize, index, idxStart, idxEnd);
	
	int posStart, posEnd;
	if (bIndex)
	{
		FindName(buffer+idxStart, idxEnd - idxStart, name, posStart, posEnd);
		posStart += idxStart;
		posEnd   += idxStart;
	}
	else
		FindName(buffer, bufSize, name, posStart, posEnd);
	
	fp = fopen(filename, "wb");
	
	if (fp)
	{
		if (!bIndex)
			fprintf(fp, "[%s]\r\n", index);

		if (posStart)
			fwrite(buffer, 1, posStart, fp);
		fprintf(fp, "%s=%s\r\n", name, value);

		if (!bIndex)
			fprintf(fp, "\r\n");

		fwrite(buffer + posEnd, 1, bufSize-posEnd, fp);
		delete [] buffer;
		fclose(fp);
		return true;
	}
	
	delete [] buffer;
	return false;
}
//*/

OILLIB_API bool ReadINIKey(const char * filename, const char * index, const char * name, char * pBuf, int nBufSize)
{
#if defined(_WIN32_WCE) || (_LINUX)
//	return ReadINIData(filename, name, pBuf, nBufSize);
	return ReadINIData(filename, index, name, pBuf, nBufSize);
#else
	char defBuf[] = "xvzof1234";
	int retSize = GetPrivateProfileString (index, name, defBuf, pBuf, nBufSize, filename);

	if(strncmp(pBuf, defBuf, sizeof(defBuf)) && retSize != nBufSize)
	{
		printf("[INI Reader   ] %s:[%s] %s = %s\n", filename, index, name, pBuf);
		return true;
	}
	else
		return false;
#endif
}

OILLIB_API bool WriteINIKey(const char * filename, const char * index, const char * name, const char *format, ...)
{
	char value[255];
	va_list vl;
	va_start(vl, format);
	vsprintf(value, format, vl);
	
#if defined(_WIN32_WCE) || (_LINUX)
	printf("[INI Reader   ] %s: [%s] %s = %s\n", filename, index, name, value);
	return WriteINIData(filename, index, name, value);
#else
	if(WritePrivateProfileString (index, name, value, filename) > 0)
		return true;
	else
		return false;
#endif
}


