/// @file dksocket.cpp
/// @brief socket wrapping functions
/// @auther hyunmock cho
/// @date 2002

#include "stdafx.h"

#ifdef _LINUX
	#include <netdb.h>
	#include <sys/time.h>
	#include <sys/select.h>
	#include <sys/types.h>
	#include <sys/socket.h>
	#include <unistd.h>
	#include <netinet/in.h>
	#include <arpa/inet.h>
	#include <errno.h>
	#include <string.h>
	#include <ctype.h>
	#include <stdarg.h>
#else
#endif // _LINUX

#include <time.h>
#include "dksocket.h"

#ifndef DEFAULT_PROTOCOL
	#define DEFAULT_PROTOCOL	0
#endif

#ifndef _LINUX
	typedef int	socklen_t;
#endif


#ifdef _LINUX
	int WSAGetLastError()
	{
		return errno;
	}
#endif

////////////////////////////////////////////////////
int sock_init()
{
#ifndef _LINUX
	WSADATA		WsaData;
	WORD		wVersionRequested = 0x0101;
	int			ret;

	wVersionRequested = MAKEWORD( 2, 0 );

	if( (ret = WSAStartup(wVersionRequested, &WsaData) ) != 0 ) 
	{
		printf("[Internal_sock] WSAStartup failed[%d]\n", ret);
		return -1;
	}
#endif

	return 1;
}

void sock_release()
{
#ifndef _LINUX
	WSACleanup();
#endif
}

void sock_close(SOCKET sock_id)
{
#ifdef _LINUX
	int ret = close(sock_id);
	if( ret < 0 )
		printf("[Internal_sock] Close Error[0x%08x]\n", sock_id );
#else
	closesocket(sock_id);
#endif
}


SOCKET sock_create_server(int nPort)
{
	SOCKET serverFd;
	struct sockaddr_in serverINETAddress; 	/* 서버 인터네트 주소 */
	struct sockaddr* serverSockAddrPtr;		/* 서버 주소에 대한 포인터  */	
//	struct sockaddr_in clientINETAddress; 	/* 클라이언트 인터네트 주소 */
//	struct sockaddr* clientSockAddrPtr;		/* 클라이언트 주소에 대한 포인터 */
	int serverLen;
	int rval, on = 1;

	serverFd = socket(AF_INET, SOCK_STREAM, DEFAULT_PROTOCOL);
	if (serverFd == -1)
		return -1;

	serverLen = sizeof(serverINETAddress);

	rval = setsockopt(serverFd, SOL_SOCKET, SO_KEEPALIVE, (char*)&on, sizeof(on));
	rval = setsockopt(serverFd, SOL_SOCKET, SO_REUSEADDR, (char*)&on, sizeof(on));

	memset(&serverINETAddress, 0, serverLen);
	serverINETAddress.sin_family = AF_INET;
	serverINETAddress.sin_addr.s_addr = htonl(INADDR_ANY);
	serverINETAddress.sin_port = htons(nPort); /* 주어진 포트 open */

	serverSockAddrPtr = (struct sockaddr*) &serverINETAddress;

	/* bind */
	if( bind(serverFd, serverSockAddrPtr, serverLen) < 0 )
	{
		sock_close(serverFd);
		return -1;
	}

	/* listen */
	if( listen(serverFd, 5) < 0 ) 
	{
		sock_close(serverFd);
		return -1;
	}
 
//	printf("Server socket create ok[0x%08x]\n", serverFd);
//	fcntl(serverFd, F_SETFL, O_NONBLOCK );

	return serverFd;
}

SOCKET sock_create_client() { return socket(AF_INET, SOCK_STREAM, 0); }
SOCKET sock_create_udp   () { return socket(AF_INET, SOCK_DGRAM, 0 ); }

////////////////////////////////////////////////////
unsigned long nameToAddr(const char* name)
{
	char hostName[100];
	struct hostent* hostStruct;
	struct in_addr* hostNode;

	if( isdigit(name[0])) return (inet_addr(name));
	
	if( strcmp(name, "s") == 0) {
		gethostname(hostName,100);
	} else 
		strcpy(hostName, name);

	hostStruct = gethostbyname(hostName);
	if(hostStruct == NULL) return (0);

	hostNode = (struct in_addr*) hostStruct->h_addr;

	return (hostNode->s_addr);
}

// block mode socket connection fn
int sock_connect(const SOCKET sock_id, const char *hostname, const int port)
{
    int 				serverLen;
    struct sockaddr_in 	serverINETAddress;
    struct sockaddr* 	serverSockAddrPtr;
    unsigned long 		inetAddress;

    serverSockAddrPtr = (struct sockaddr*)&serverINETAddress;
    serverLen = sizeof( serverINETAddress);

    /* get host address */
    inetAddress = nameToAddr(hostname);
    if( inetAddress == 0 ) 
	{
		printf("[Internal_sock] host name not found\n");
		return -1;
    }       

	memset(&serverINETAddress, 0, sizeof(serverINETAddress));
    serverINETAddress.sin_family = AF_INET; /* internet */
    serverINETAddress.sin_addr.s_addr = inetAddress;                
    serverINETAddress.sin_port = htons(port);

    if( connect(sock_id, serverSockAddrPtr, serverLen) == SOCKET_ERROR )
	{
		printf("[Internal_sock] Socket connection error[%d][%s]\n", WSAGetLastError(), get_wsaerrstring( WSAGetLastError() ) );
		return -1;
	}

	return 1;
}

SOCKET sock_accept( SOCKET sock_server, int &TimeOut )
{
	fd_set				readfds, writefds, exceptfds;

	#ifdef _LINUX
	struct timeval		tv;
	#else
	TIMEVAL				tv;
	#endif

	unsigned long		tick_prev, tick_next;

	struct sockaddr_in	clientINETAddress;
	struct sockaddr*	clientSockAddrPtr;

	socklen_t addrlen = sizeof(clientINETAddress);

	clientSockAddrPtr = (struct sockaddr*) &clientINETAddress;

	FD_ZERO(&readfds);
	FD_ZERO(&writefds);
	FD_ZERO(&exceptfds);
	FD_SET(sock_server, &readfds);
	FD_SET(sock_server, &writefds);
	FD_SET(sock_server, &exceptfds);
	
	#ifdef _LINUX
	memset(&tv, 0, sizeof(struct timeval));
	#else
	memset(&tv, 0, sizeof(TIMEVAL));
	#endif

	tv.tv_sec = (long)(TimeOut / 1000 );
	tv.tv_usec = (long)(TimeOut % 1000 );

	tick_prev = GetTickCount();
	int ret = select( sock_server + 1, &readfds, &writefds, &exceptfds, (TimeOut<0)?NULL:&tv );

	switch( ret )
	{
	case 0 :	// timeout
		tick_next = GetTickCount();
		TimeOut -= (tick_next - tick_prev );
		return -2;

	case SOCKET_ERROR :
		tick_next = GetTickCount();
		TimeOut -= (tick_next - tick_prev );
		printf("[Internal_sock] select error\n");
		return -1;
	}

	if (FD_ISSET(sock_server, &readfds))
	{
		FD_CLR(sock_server, &readfds);	

		tick_next = GetTickCount();
		TimeOut -= (tick_next - tick_prev );
		return accept( sock_server, clientSockAddrPtr, &addrlen);
	}

	tick_next = GetTickCount();
	TimeOut -= (tick_next - tick_prev );
	return -1;
}

int sock_send(SOCKET sock_id, const void *vptr, size_t n)
{
	size_t		nleft;
	size_t		nwrite;
	char		*ptr;
	int			ret;

	fd_set		writefds;
	
	ptr = (char *)vptr;
	nleft = n;
	
	while( nleft > 0 )
	{
		FD_ZERO(&writefds);
		FD_SET(sock_id, &writefds);

		while(1)
		{
			ret = select(sock_id + 1, NULL, &writefds, NULL, NULL );
			if( ret == SOCKET_ERROR )
			{
				printf("[Internal_sock] sock select error[%s]\n", get_wsaerrstring( WSAGetLastError() ) );
				return -1;
			}

			break;
		}
	
		if( ret == 0 ) // connection closed successfully
		{
			printf("[Internal_sock] connectin closed\n");
			return 0;
		}

		if( FD_ISSET(sock_id, &writefds) )
		{
			if( (nwrite = send(sock_id, ptr, nleft, 0 )) < 0 )
			{
				printf("[Internal_sock] socker write error[%s]\n", get_wsaerrstring( WSAGetLastError() ) );
				return -1;
			}
			else if( nwrite == 0 )
			{
				break;
			}
		
			nleft -= nwrite;
			ptr += nwrite;
		}
	}

	return (n-nleft); 
}

int sock_recv(SOCKET sock_id, void *vptr, int n)
{
	int			nleft, nread;
	char		*ptr;
	int			ret;

	fd_set		readfds;
	
	ptr = (char *)vptr;
	nleft = n;
	
	while( nleft > 0 )
	{
		FD_ZERO(&readfds);
		FD_SET(sock_id, &readfds);

		while(1)
		{
			ret = select(sock_id + 1, &readfds, NULL, NULL, NULL );
			if( ret == SOCKET_ERROR )
			{
				printf("[Internal_sock] sock select error[%s]\n", get_wsaerrstring(WSAGetLastError()) );
				return -1;
			}

			break;
		}
	
		if( FD_ISSET(sock_id, &readfds) )
		{
			if( (nread = recv(sock_id, ptr, nleft, 0)) == SOCKET_ERROR)
			{
				printf("[Internal_sock] socker read error[%s]\n", get_wsaerrstring(WSAGetLastError()) );
				return -1;
			}
			else if( nread == 0 )
			{
				break;
			}
		
			nleft -= nread;
			ptr += nread;
		}
	}

	return (n-nleft); 
}

char *get_wsaerrstring(const int no)
{
#ifdef _LINUX
	switch(no)
	{
	case EBADF :					return (char*)"EBADF";
	case EFAULT :					return (char*)"EFAULT";
	case ENOTSOCK :					return (char*)"ENOTSOCK";
	case EISCONN :					return (char*)"EISCONN";
	case EINPROGRESS :				return (char*)"EINPROGRESS";	
	}
#else
	switch( no )
	{
	case WSANOTINITIALISED :		return "WSANOTINITIALISED";
	case WSAENETDOWN :				return "WSAENETDOWN";
	case WSAEADDRINUSE :			return "WSAEADDRINUSE";
	case WSAEINTR :					return "WSAEINTR";
	case WSAEINPROGRESS :			return "WSAEINPROGRESS";
	case WSAEALREADY :				return "WSAEALREADY";
	case WSAEADDRNOTAVAIL :			return "WSAEADDRNOTAVAIL";
	case WSAEAFNOSUPPORT :			return "WSAEAFNOSUPPORT";
	case WSAECONNREFUSED :			return "WSAECONNREFUSED";
	case WSAEFAULT :				return "WSAEFAULT";
	case WSAEINVAL :				return "WSAEINVAL";
	case WSAEISCONN :				return "WSAEISCONN";
	case WSAENETUNREACH :			return "WSAENETUNREACH";
	case WSAENOBUFS :				return "WSAENOBUFS";
	case WSAENOTSOCK :				return "WSAENOTSOCK";
	case WSAETIMEDOUT :				return "WSAETIMEDOUT";
	case WSAEWOULDBLOCK :			return "WSAEWOULDBLOCK";
	case WSAEACCES :				return "WSAEACCES";
	}
#endif

	return (char*)"UNKNOWN";
}

