# Microsoft Developer Studio Project File - Name="OilWin32" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Static Library" 0x0104

CFG=OilWin32 - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "OilWin32.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "OilWin32.mak" CFG="OilWin32 - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "OilWin32 - Win32 Release" (based on "Win32 (x86) Static Library")
!MESSAGE "OilWin32 - Win32 Debug" (based on "Win32 (x86) Static Library")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
RSC=rc.exe

!IF  "$(CFG)" == "OilWin32 - Win32 Release"

# PROP BASE Use_MFC 2
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 2
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "../../Temp/Win32Rel"
# PROP Intermediate_Dir "../../Temp/Win32Rel"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_AFXDLL" /Yu"stdafx.h" /FD /c
# ADD CPP /nologo /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /D "_viplib_emb_" /Yu"stdafx.h" /FD /c
# ADD BASE RSC /l 0x412 /d "NDEBUG" /d "_AFXDLL"
# ADD RSC /l 0x412 /d "NDEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LIB32=link.exe -lib
# ADD BASE LIB32 /nologo
# ADD LIB32 /nologo /out:"../../Lib/OilWin32.lib"

!ELSEIF  "$(CFG)" == "OilWin32 - Win32 Debug"

# PROP BASE Use_MFC 2
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 2
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "../../Temp/Win32Dbg"
# PROP Intermediate_Dir "../../Temp/Win32Dbg"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MDd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_AFXDLL" /Yu"stdafx.h" /FD /GZ /c
# ADD CPP /nologo /MDd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /D "_viplib_emb_" /Yu"stdafx.h" /FD /GZ /c
# ADD BASE RSC /l 0x412 /d "_DEBUG" /d "_AFXDLL"
# ADD RSC /l 0x412 /d "_DEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LIB32=link.exe -lib
# ADD BASE LIB32 /nologo
# ADD LIB32 /nologo /out:"../../Lib/OilWin32D.lib"

!ENDIF 

# Begin Target

# Name "OilWin32 - Win32 Release"
# Name "OilWin32 - Win32 Debug"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=.\StdAfx.cpp
# ADD CPP /Yc"stdafx.h"
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl"
# Begin Source File

SOURCE=.\StdAfx.h
# End Source File
# End Group
# Begin Group "ETC"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\..\include\Oil_ETC\DrawingBundle.cpp
# End Source File
# Begin Source File

SOURCE=..\..\include\Oil_ETC\DrawingBundle.h
# End Source File
# Begin Source File

SOURCE=..\..\include\Oil_ETC\ini.cpp
# End Source File
# Begin Source File

SOURCE=..\..\include\Oil_ETC\ini.h
# End Source File
# Begin Source File

SOURCE=..\..\include\Oil_ETC\MMTimer.h
# End Source File
# Begin Source File

SOURCE=..\..\include\Oil_ETC\MsgQueue.cpp
# End Source File
# Begin Source File

SOURCE=..\..\include\Oil_ETC\MsgQueue.h
# End Source File
# Begin Source File

SOURCE=..\..\include\Oil_ETC\VSerialBase.h
# End Source File
# Begin Source File

SOURCE=..\..\include\Oil_ETC\VString.cpp
# End Source File
# Begin Source File

SOURCE=..\..\include\Oil_ETC\VString.h
# End Source File
# End Group
# Begin Group "Network"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\..\include\Oil_Network\ClientSock.cpp
# End Source File
# Begin Source File

SOURCE=..\..\include\Oil_Network\ClientSock.h
# End Source File
# Begin Source File

SOURCE=..\..\include\Oil_Network\DES.cpp
# End Source File
# Begin Source File

SOURCE=..\..\include\Oil_Network\DES.h
# End Source File
# Begin Source File

SOURCE=..\..\include\Oil_Network\dksocket.cpp
# End Source File
# Begin Source File

SOURCE=..\..\include\Oil_Network\dksocket.h
# End Source File
# Begin Source File

SOURCE=..\..\include\Oil_Network\LogServer.cpp
# End Source File
# Begin Source File

SOURCE=..\..\include\Oil_Network\LogServer.h
# End Source File
# Begin Source File

SOURCE=..\..\include\Oil_Network\ServerSock.cpp
# End Source File
# Begin Source File

SOURCE=..\..\include\Oil_Network\ServerSock.h
# End Source File
# Begin Source File

SOURCE=..\..\include\Oil_Network\SingleAsyncSock.cpp
# End Source File
# Begin Source File

SOURCE=..\..\include\Oil_Network\SingleAsyncSock.h
# End Source File
# Begin Source File

SOURCE=..\..\include\Oil_Network\SingleSock.cpp
# End Source File
# Begin Source File

SOURCE=..\..\include\Oil_Network\SingleSock.h
# End Source File
# Begin Source File

SOURCE=..\..\include\Oil_Network\SplitRecv.cpp
# End Source File
# Begin Source File

SOURCE=..\..\include\Oil_Network\SplitRecv.h
# End Source File
# Begin Source File

SOURCE=..\..\include\Oil_Network\TCPPacket.cpp
# End Source File
# Begin Source File

SOURCE=..\..\include\Oil_Network\TCPPacket.h
# End Source File
# End Group
# Begin Group "Thread"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\..\include\Oil_Thread\SingleThread.cpp
# End Source File
# Begin Source File

SOURCE=..\..\include\Oil_Thread\SingleThread.h
# End Source File
# Begin Source File

SOURCE=..\..\include\Oil_Thread\Sync.cpp
# End Source File
# Begin Source File

SOURCE=..\..\include\Oil_Thread\Sync.h
# End Source File
# Begin Source File

SOURCE=..\..\include\Oil_Thread\WaitEvent.cpp
# End Source File
# Begin Source File

SOURCE=..\..\include\Oil_Thread\WaitEvent.h
# End Source File
# End Group
# Begin Group "Vision"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\..\include\Oil_Vision\JpegWrap.cpp
# End Source File
# Begin Source File

SOURCE=..\..\include\Oil_Vision\JpegWrap.h
# End Source File
# End Group
# Begin Source File

SOURCE=.\Readme.txt
# End Source File
# End Target
# End Project
