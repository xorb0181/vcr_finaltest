// stdafx.h : include file for standard system include files,
//  or project specific include files that are used frequently, but
//      are changed infrequently
//

#if !defined(AFX_STDAFX_H__FCF9A58F_2B64_428C_889C_8A3A7E26230E__INCLUDED_)
#define AFX_STDAFX_H__FCF9A58F_2B64_428C_889C_8A3A7E26230E__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifdef _LINUX
//#include <memory.h>
#include <string.h>
#include <ctype.h>
#include <limits.h>
#include <unistd.h>
#include <stdio.h>

#define ASSERT(f)          ((void)0)

#define Sleep(time) usleep(time*1000)

#ifndef __max
	#define __max(a,b)  (((a) > (b)) ? (a) : (b))
#endif
#ifndef __min
	#define __min(a,b)  (((a) < (b)) ? (a) : (b))
#endif

typedef unsigned long DWORD;

#define __cdecl
#define THREAD_PRIORITY_NORMAL 0
#define THREAD_PRIORITY_TIME_CRITICAL 0

#define WAIT_FAILED							0xFFFFFFFF
#define WAIT_OBJECT_0						((STATUS_WAIT_0 ) + 0 )
#define WAIT_ABANDONED						((STATUS_ABANDONED_WAIT_0 ) + 0 )

#define WAIT_TIMEOUT						STATUS_TIMEOUT

typedef int				SOCKET;
#define SOCKET_ERROR						-1


#define STATUS_WAIT_0                  	(0x00000000L)    
#define STATUS_ABANDONED_WAIT_0          (0x00000080L)    
#define STATUS_USER_APC                  (0x000000C0L)    
#define STATUS_TIMEOUT                   (0x00000102L)    
#define STATUS_PENDING                   (0x00000103L)    
#define STATUS_SEGMENT_NOTIFICATION      (0x40000005L)    
#define STATUS_GUARD_PAGE_VIOLATION      (0x80000001L)    
#define STATUS_DATATYPE_MISALIGNMENT     (0x80000002L)    
#define STATUS_BREAKPOINT                (0x80000003L)    
#define STATUS_SINGLE_STEP               (0x80000004L)    
#define STATUS_ACCESS_VIOLATION          (0xC0000005L)    
#define STATUS_IN_PAGE_ERROR             (0xC0000006L)    
#define STATUS_INVALID_HANDLE            (0xC0000008L)    
#define STATUS_NO_MEMORY                 (0xC0000017L)    
#define STATUS_ILLEGAL_INSTRUCTION       (0xC000001DL)    
#define STATUS_NONCONTINUABLE_EXCEPTION  (0xC0000025L)    
#define STATUS_INVALID_DISPOSITION       (0xC0000026L)    
#define STATUS_ARRAY_BOUNDS_EXCEEDED     (0xC000008CL)    
#define STATUS_FLOAT_DENORMAL_OPERAND    (0xC000008DL)    
#define STATUS_FLOAT_DIVIDE_BY_ZERO      (0xC000008EL)    
#define STATUS_FLOAT_INEXACT_RESULT      (0xC000008FL)    
#define STATUS_FLOAT_INVALID_OPERATION   (0xC0000090L)    
#define STATUS_FLOAT_OVERFLOW            (0xC0000091L)    
#define STATUS_FLOAT_STACK_CHECK         (0xC0000092L)    
#define STATUS_FLOAT_UNDERFLOW           (0xC0000093L)    
#define STATUS_INTEGER_DIVIDE_BY_ZERO    (0xC0000094L)    
#define STATUS_INTEGER_OVERFLOW          (0xC0000095L)    
#define STATUS_PRIVILEGED_INSTRUCTION    (0xC0000096L)    
#define STATUS_STACK_OVERFLOW            (0xC00000FDL)    
#define STATUS_CONTROL_C_EXIT            (0xC000013AL)    
#define STATUS_FLOAT_MULTIPLE_FAULTS     (0xC00002B4L)    
#define STATUS_FLOAT_MULTIPLE_TRAPS      (0xC00002B5L)    
#define STATUS_ILLEGAL_VLM_REFERENCE     (0xC00002C0L) 
///////////////////////////////////////////////////////////////////////
// Functions
///////////////////////////////////////////////////////////////////////
unsigned long 	GetTickCount();
double mmGetTime();

typedef unsigned char BYTE;
typedef unsigned int UINT;
#endif

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_STDAFX_H__FCF9A58F_2B64_428C_889C_8A3A7E26230E__INCLUDED_)
