#ifndef SV113_RECTIFY_HPP_
#define SV113_RECTIFY_HPP_

#include <cxcore.h>
#include <cvaux.h>
#include <highgui.h>

class sv113Rectify
{
public:

	IplImage * mapx;
	IplImage * mapy;
	float ox, oy;
	float scale_factor;

	sv113Rectify( const float Ox=160.0f, const float Oy=120.0f, const float scaleFactor=1.0) : mapx(0), mapy(0), ox(Ox), oy(Oy), scale_factor(scaleFactor)
	{
		genLookup();
	}
	~sv113Rectify()
	{
		if( mapx ) cvReleaseImage( &mapx );
		if( mapy ) cvReleaseImage( &mapy );
	}

	void genLookup()
	{

		// if this operation is first, you need to allocate the space for the lookup tables
		if( mapx == 0 )
		{
			mapx = cvCreateImage( cvSize(320,240), IPL_DEPTH_32F, 1 );
			mapy = cvCreateImage( cvSize(320,240), IPL_DEPTH_32F, 1 );
			
			cvSetZero( mapx );
			cvSetZero( mapy );
		}

		// let's go to get the entire map which will be used for the cvRemap
		float ud, vd;
		float * mapx_ptr = (float *)mapx->imageData;
		float * mapy_ptr = (float *)mapy->imageData;
		
		for( int u(0); u<320; u++ )
		{
			for( int v(0); v<240; v++ )
			{
				ud = u - ox;
				vd = v - oy;
				ud *= scale_factor;
				vd *= scale_factor;
				float r( sqrt(ud*ud+vd*vd) * 4 );
				float r_( sv113Fwd(r) );
				float ratio = (r_) / (r);
				ud *= ratio;
				vd *= ratio;
				ud += ox;
				vd += oy;

				// put exception which may cause segment fault
				if( ud < 0 
					|| vd < 0 
					|| ud >= static_cast<float>(320-1) 
					|| vd >= static_cast<float>(240-1) 
					)
				{
					ud = vd = 0.0f;
				}
				
				// set
				mapx_ptr[u+v*320] = ud;
				mapy_ptr[u+v*320] = vd;
			}
		}
	}

	inline void rectify( IplImage * srcImg, IplImage * dstImg)
	{
		// below hack make that unknown region has zero pixel value
		srcImg->imageData[0] = 0;
		
		// remapping by opencv one
		cvRemap( srcImg, dstImg, mapx, mapy );
	}

	inline void rectify( IplImage * & srcImage )
	{
		IplImage * tmp = cvCloneImage( srcImage );
		rectify( tmp, srcImage );
		cvReleaseImage( &tmp );
	}

	float interp(float s0, float s1, float d0, float d1, float val)
	{
		if (s0 == val) return d0;
		if (s1 == val) return d1;
		
		float ratio = (val-s0)/(s1-s0);
		return d0 + (d1-d0)*ratio;
	}
	
	float sv113Inv(float in)
	{
		float _table[][2] =
		{
			{0.0000f/0.00175f, 0.0000f/0.00175f},
			{0.1366f/0.00175f, 0.1369f/0.00175f},
			{0.2732f/0.00175f, 0.2760f/0.00175f},
			{0.4089f/0.00175f, 0.4195f/0.00175f},
			{0.5423f/0.00175f, 0.5699f/0.00175f},
			{0.6720f/0.00175f, 0.7302f/0.00175f},
			{0.7968f/0.00175f, 0.9042f/0.00175f},
			{0.9155f/0.00175f, 1.0968f/0.00175f},
			{1.0266f/0.00175f, 1.3145f/0.00175f},
			{1.1284f/0.00175f, 1.5668f/0.00175f},
			{1.2191f/0.00175f, 1.8678f/0.00175f},
			{1.2983f/0.00175f, 2.2388f/0.00175f},
			{1.3672f/0.00175f, 2.7159f/0.00175f},
			{1.4294f/0.00175f, 3.3633f/0.00175f},
			{10.000f/0.00175f, 23.529f/0.00175f}
		};

		for (int j = 0 ; j < 14 ; j++)
		{
			if (_table[j][0] <= in && in <= _table[j+1][0])
			{
				return interp(_table[j][0], _table[j+1][0], _table[j][1], _table[j+1][1], in);
			}
		}
		
		return 13445;
	}
	
	float sv113Fwd(float in)
	{
		float _table[][2] =
		{
			{0.0000f/0.00175f, 0.0000f/0.00175f},
			{0.1366f/0.00175f, 0.1369f/0.00175f},
			{0.2732f/0.00175f, 0.2760f/0.00175f},
			{0.4089f/0.00175f, 0.4195f/0.00175f},
			{0.5423f/0.00175f, 0.5699f/0.00175f},
			{0.6720f/0.00175f, 0.7302f/0.00175f},
			{0.7968f/0.00175f, 0.9042f/0.00175f},
			{0.9155f/0.00175f, 1.0968f/0.00175f},
			{1.0266f/0.00175f, 1.3145f/0.00175f},
			{1.1284f/0.00175f, 1.5668f/0.00175f},
			{1.2191f/0.00175f, 1.8678f/0.00175f},
			{1.2983f/0.00175f, 2.2388f/0.00175f},
			{1.3672f/0.00175f, 2.7159f/0.00175f},
			{1.4294f/0.00175f, 3.3633f/0.00175f},
			{10.000f/0.00175f, 23.529f/0.00175f}
		};

		for (int j = 0 ; j < 14 ; j++)
		{
			if (_table[j][1] <= in && in <= _table[j+1][1])
			{
				return interp(_table[j][1], _table[j+1][1], _table[j][0], _table[j+1][0], in);
			}
		}		
		return 5714;
	}
};


#endif	//SV113_RECTIFY_HPP_