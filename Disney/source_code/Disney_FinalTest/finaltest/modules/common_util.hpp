#ifndef COMMON_UTIL_HPP_
#define COMMON_UTIL_HPP_

#define PSD_BLOCK   2
#define PSD_CLEAR   7 

enum ePsdCheckState
{
  ePsdCheckNone,
  ePsdCheckStart,
  ePsdCheckLeftBlock,
  ePsdCheckLeftClear,
  ePsdCheckCenterBlock,
  ePsdCheckCenterClear,
  ePsdCheckRightBlock,
  ePsdCheckRightClear,
  ePsdCheckEnd,
  ePsdCheckFail,
};

class CPSDCheck
{
protected:
  int psd_max[3];
  int psd_min[3];
  int psd_current[3];
  int state;
  int last_state;
  int right_process;
  int psd_block_;
  int psd_clear_;
  int ir_block_;
  int ir_clear_;

  bool hjir_model;
//  bool enable_log_result;

  unsigned char current_psd_state_;
  unsigned char last_psd_state_;
  
public:
  CPSDCheck() :
      state(0)
      , last_state(0)
      , right_process(0)
      , psd_block_(0)
      , psd_clear_(0)
      , ir_block_(0)
      , ir_clear_(0)
//      enable_log_result(false),
      , current_psd_state_(ePsdCheckNone)
      , last_psd_state_(ePsdCheckNone)
      , hjir_model(false)
  {
    initialize();
  }
  
  void initialize()
  {
    for (int n(0); n<3; n++)
    {
      psd_min[n] = 0;
      psd_max[n] = 0;
      psd_current[n] = 0;
    }

    state = ePsdCheckNone;
    last_state = -1;

    current_psd_state_ = 0x0;
    last_psd_state_ = 0x0;
  }

//  void enableLogging(bool enable) { enable_log_result = enable; }

  void setThresholdForPSD(int clear, int block)
  {
//    printf(">>> setThreshold | clear(%d), block(%d) \n", clear, block);
    psd_clear_ = clear;
    psd_block_ = block;
  }

  void setThresholdForIR(int clear, int block)
  {
    //    printf(">>> setThreshold | clear(%d), block(%d) \n", clear, block);
    ir_clear_ = clear;
    ir_block_ = block;
  }

  void setHJIRModel(bool model_value)
  {
    hjir_model = model_value;
  }
  
  int& setPsdValue(int& psd_left, int& psd_center, int& psd_right)
  {
    //printf("*** setPsdValue: %d, %d, %d \n", psd_left, psd_center, psd_right);
    psd_current[0] = psd_left;
    psd_current[1] = psd_center;
    psd_current[2] = psd_right;

    for (int n(0); n<3; n++)
    {
      if (psd_current[n] < psd_min[n])
      {
        psd_min[n] = psd_current[n];
      }
      else if (psd_current[n] > psd_max[n])
      {
        psd_max[n] = psd_current[n];
      }
    }

    checkProcess();
    //printf(">>> setPsdValue() => %d\n", right_process);
    return right_process;
  }
  
  void isChangedPsd(unsigned char& psd_flag)
  {
    for (int n(0); n<3; n++)
    {
      if (psd_min[n] <= PSD_BLOCK && PSD_CLEAR <= psd_max[n])
      {
        psd_flag |= (1<<n);
      }
    }
  }

  //void getPsdLevel(unsigned char& psd_level)
  void getPsdLevel()
  {
    last_psd_state_ = current_psd_state_;

    if (hjir_model)
    {
      if (psd_current[0] <= ir_block_) {
        current_psd_state_ |= 0x1;
      }
      else if (ir_clear_ <= psd_current[0]) {
        current_psd_state_ &= ~(0x1);
      }

      if (psd_current[1] <= psd_block_) {
        current_psd_state_ |= 0x2;
      }
      else if (psd_clear_ <= psd_current[1]) {
        current_psd_state_ &= ~(0x2);
      }

      if (psd_current[2] <= ir_block_) {
        current_psd_state_ |= 0x4;
      }
      else if (ir_clear_ <= psd_current[2]) {
        current_psd_state_ &= ~(0x4);
      }
    } // if (hjir_model)
    else
    {
      for (int n(0); n<3; n++)
      {
        if (psd_current[n] <= psd_block_)
        {
          //printf(">>> setPsdValue: %d => block\n", psd_current[n]);
          current_psd_state_ |= (1<<n);
        }
        if (psd_clear_ <= psd_current[n])
        { 
          //printf(">>> setPsdValue: %d => clear\n", psd_current[n]);
          current_psd_state_ &= ~(1<<n);
        }
      } // for (int n(0); n<3; n++)
    }

    if (last_psd_state_ != current_psd_state_)
    {
      printf(">>> current_psd_state_: %d\n"
        , current_psd_state_);
    }
  }

  int getCheckState() { return state; }

  void setCheckState(ePsdCheckState chk_state)  { state = chk_state; }

  void getPsdMin(int& psd_left, int& psd_center, int& psd_right)
  {
    psd_left = psd_min[0];  psd_center = psd_min[1];  psd_right = psd_min[2];
  }

  void getPsdMax(int& psd_left, int& psd_center, int& psd_right)
  {
    psd_left = psd_max[0];  psd_center = psd_max[1];  psd_right = psd_max[2];
  }

  bool checkProcess()
  {
    right_process = 1;

//    unsigned char tmp_state(0);
//    getPsdLevel(tmp_state);
//
//    if(last_state == tmp_state && state != ePsdCheckNone)
//    {
//      right_process = 0;
//      return false;
//    }
//    printf(">>> CHECK_PSD: %d, %d \n", tmp_state, state);
//    last_state = tmp_state;

    getPsdLevel();
    if (last_psd_state_ == current_psd_state_ && state != ePsdCheckNone)
    {
      right_process = 0;
      return false;
    }

//    if (enable_log_result)
//    {
//      printf(">>> CHECK_PSD: current(%d), last(%d), process(%d) \n"
//        , current_psd_state_, last_psd_state_, state);
//    }
    
    switch(current_psd_state_)
    {
    //case 0x6: // 110
    case 0x1:
      {
        switch(state)
        {
        case ePsdCheckStart:
        //case ePsdCheckLeftBlock:
          state = ePsdCheckLeftBlock;
          break;

        default:
          right_process = -1;
          break;
        }
      } break;
      
    //case 0x5: // 101
    case 0x2:
      {
        switch(state)
        {
        case ePsdCheckLeftClear:
        //case ePsdCheckCenterBlock:
          state = ePsdCheckCenterBlock;
          break;
          
        default:
          right_process = -1;
          break;
        }
      } break;
      
    //case 0x3: // 011
    case 0x4:
      {
        switch(state)
        {
        case ePsdCheckCenterClear:
        //case ePsdCheckRightBlock:
          state = ePsdCheckRightBlock;
          break;
          
        default:
          right_process = -1;
          break;
        }
      } break;
      
    case 0x7: // 111
    case 0x0:
      {
        switch(state)
        {
        case ePsdCheckNone:
        //case ePsdCheckStart:
          state = ePsdCheckStart;
          break;
          
        case ePsdCheckLeftBlock:
        //case ePsdCheckLeftClear:
          state = ePsdCheckLeftClear;
          break;
          
        case ePsdCheckCenterBlock:
        //case ePsdCheckCenterClear:
          state = ePsdCheckCenterClear;
          break;
          
        case ePsdCheckRightBlock:
        //case ePsdCheckEnd:
          state = ePsdCheckEnd;
          break;
          
        case ePsdCheckFail:
        default:
          right_process = -1;
          break;
        }
        
      } break;
    } //  switch(tmp_state)

    return true;
  }
};

//	================================================================================================================
//	for LOGGING
//	================================================================================================================

#include <FSTREAM>
#include <CTIME>
#include <IOMANIP>

class SimpleLogger
{
public:
  SimpleLogger() :  m_init(false)
  {
    //TRACE("%d", _sysTime->wMilliseconds);
  }
  
  ~SimpleLogger()
  {
    if (m_init)
    {
      m_fout.close();
    }
  }
  
  void Initialize(std::string prefix = ".\\log\\log_")
  {
    m_prefix = prefix;
    m_init = true;
    GetLocalTime(&m_sysTime);
    m_sysTimeTmp = m_sysTime;
    
    std::stringstream	strm;
    strm << prefix << std::setfill('0') << std::setw(2) << m_sysTime.wMonth << std::setfill('0') << std::setw(2) << m_sysTime.wDay << ".log";
    m_filename = strm.str();
    m_fout.open( m_filename.c_str(), std::fstream::in | std::fstream::out | std::fstream::app);
    //printf(">>> SimpleLogger::Initialize(%s)\n", m_filename.c_str());
  }
  
  void operator() (std::string log, bool only_value = false)
  {
    if(m_init)
    {
      GetLocalTime(&m_sysTime);
      if (m_sysTime.wDay != m_sysTimeTmp.wDay)
      {
        m_fout.close();
        m_sysTimeTmp = m_sysTime;
        
        std::stringstream	ss;
        ss << m_prefix << std::setfill('0') << std::setw(2) << m_sysTime.wMonth << std::setfill('0') << std::setw(2) << m_sysTime.wDay << ".log";
        m_fout.open( ss.str().c_str(), std::fstream::in | std::fstream::out | std::fstream::app);
      }
      
      std::stringstream strm;
      strm << "[" << std::setfill('0') << std::setw(2) << m_sysTime.wHour << std::setfill('0') << std::setw(2) << m_sysTime.wMinute << "|" << std::setfill('0') << std::setw(2) << m_sysTime.wSecond << "][" << std::setfill('0') << std::setw(3) << m_sysTime.wMilliseconds << "] ";
      //m_fout << strm.str() << log << std::endl;
      if (!only_value)
      {
        m_fout << strm.str();
      }
      //printf(">>> slog() | %s | %s \n", m_filename.c_str(), log.c_str());
      m_fout << log;
      m_fout.flush();
    }
  }

  void getLoggedTime(SYSTEMTIME& logged_time)
  {
    logged_time = m_sysTime;
  }
  
  bool isInit()	{	return m_init;	}
  
private:
  std::ofstream	m_fout;
  std::string		m_prefix;
  std::string   m_filename;
  CTime			m_ctime;
  SYSTEMTIME		m_sysTime;
  SYSTEMTIME		m_sysTimeTmp;
  bool			m_init;
};

//	================================================================================================================
//	for string data
//	================================================================================================================

class StringData
{
public:
  std::map<std::string, unsigned int> data_;

  StringData(){}
  ~StringData(){}

  void putData(std::string data_value)
  {
    //data_.insert(data_value, data_.size());
    data_[data_value] = data_.size() + 1;
  }

  unsigned int getData(std::string data_value)
  {
    return data_[data_value];
  }
};

#endif
