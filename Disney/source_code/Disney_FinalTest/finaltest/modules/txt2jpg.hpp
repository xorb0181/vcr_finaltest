#ifndef TXT2JPEG_HPP_
#define TXT2JPEG_HPP_

#include <cxcore.h>
#include <cvaux.h>
#include <highgui.h>
#include <iostream>
#include <fstream>


class txt2jpeg
{
public:
	bool operator() (const char * fileName, IplImage * & image )
	{
		FILE * pf = fopen(fileName, "r");
		char ch;
		bool prefix(false);
		unsigned char hex_value;
		std::ofstream fout_jpg("t2j.jpg", std::ofstream::binary);
		while( fscanf( pf, "%c", &ch ) != EOF )
		{
			unsigned char nibble = a2h( ch );
			
			if(prefix)
			{
				prefix = false;
				hex_value |= nibble;
				fout_jpg << hex_value;
			}
			else if(!prefix)
			{
				prefix = true;
				hex_value = nibble << 4;
			}
		}
		fout_jpg.close();	
		fclose(pf);

		image = cvLoadImage("t2j.jpg");

		return true;
	}

	bool operator() (const char* fileName)
	{
		FILE * pf = fopen(fileName, "r");
    if (!pf)
    {
      printf(">>> The file cannot be opened.\n");
      return false;
    }

		char ch;
		bool prefix(false);
		unsigned char hex_value;
		std::ofstream fout_jpg("t2j.jpg", std::ofstream::binary);
		while( fscanf( pf, "%c", &ch ) != EOF )
		{
			unsigned char nibble = a2h( ch );
			
			if(prefix)
			{
				prefix = false;
				hex_value |= nibble;
				fout_jpg << hex_value;
			}
			else if(!prefix)
			{
				prefix = true;
				hex_value = nibble << 4;
			}
		}
		fout_jpg.close();	
		fclose(pf);
		
		//image = cvLoadImage("t2j.jpg");
		
		return true;
	}

private:
	unsigned char a2h( char code )
	{
		if( code >= 'a' ) return code - 'a' + 10;
		else if( code >= '0') return code - '0';
		else
		{
			std::cout << "a2h; could not convert give code, check your code!" << std::endl;
			return 0;
		}
	}
	
//	void show_jpg(char* _img_file)
//	{
//		char name[8] = {0};
//		sprintf(name, "Img_%02d", _idx_img++);
//		
//		cvNamedWindow( name, CV_WINDOW_AUTOSIZE );
//		IplImage * image = cvLoadImage( _img_file );
//		cvShowImage( name, image );
//	}
	
};


#endif // TXT2JPEG_HPP_