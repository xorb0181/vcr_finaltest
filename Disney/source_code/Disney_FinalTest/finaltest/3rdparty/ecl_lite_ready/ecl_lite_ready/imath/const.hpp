#ifndef CONST_HPP_
#define CONST_HPP_


const int pi_degree     = 1800;
const int two_pi_degree   = 3600;
const int pi_2_degree     = pi_degree/2;
const int pi_4_degree     = pi_degree/4;

const float pi    = 3.14159265358979324f;
const float two_pi  = 2.0f*pi;  /**< @brief Mathematical constant for 2*pi. */
const float pi_2    = pi/2.0f;  /**< @brief Mathematical constant for pi/2. */
const float pi_4    = pi/4.0f;  /**< @brief Mathematical constant for pi/4. */



#endif//#define IMATH_HPP_
