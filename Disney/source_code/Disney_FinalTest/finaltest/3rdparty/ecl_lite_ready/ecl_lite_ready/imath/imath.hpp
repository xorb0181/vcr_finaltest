#ifndef IMATH_HPP_
#define IMATH_HPP_

#include "../common/util.h"

template<int _Pi, int _twoPi=_Pi*2>
class wrapAngle
{
public:
  int operator() (int angle)
  {
    return ((angle) += ((angle) > _Pi) ? -_twoPi : ((angle) < -_Pi) ? _twoPi : 0);    
  }
};

template<typename T>//, int _Pi, int _twoPi=_Pi*2>
class dWrapAngle
{
public:
  inline T operator() ( const T & angle, const T & pi_value, const T & twopi_value )
  {
    //return (angle + ( (angle > pi_value) ? (-twopi_value) : ((angle < -pi_value) ? twopi_value : 0) ) );
       if( angle > +pi_value ) return (angle-twopi_value);
    else if( angle < -pi_value ) return (angle+twopi_value);
    return angle;
  }
};


template<typename Td, typename Tr>
class degree2Radian
{
public:
  Tr operator() ( const Td & degree, const Tr _Pi=static_cast<Tr>(18000) )
  {
    return static_cast<Tr>( degree ) 
        * static_cast<Tr>(3.14159265358979324) 
        / _Pi;
  }
};

template<typename Td, typename Tr>
class Radian2degree
{
public:
  Td operator() ( const Tr & radian, const Tr _Pi=static_cast<Tr>(18000) )
  {
    return static_cast<Td>( radian * _Pi / static_cast<Tr>(3.14159265358979324) );
  }
};


#define TruncAngleRange_10(a) wrapAngle<1800>()(a)


enum { eRight = 0, eTop, eLeft, eBottom };

template< int _halfPi = 90, int _Pi= _halfPi*2, int _twoPi=_Pi*2>
class angle2Code
{
public:
  template< typename T >
  int operator() ( T angle )
  {
    angle += _halfPi/2;
    if (angle <    0)  angle += _twoPi;
    if (angle >= _twoPi) angle -= _twoPi;
    return static_cast<int>(angle / _halfPi);
  }
};

class truncationCode
{
public:
  int operator() ( int directionIs )
  {
    if (directionIs > 3) return directionIs -= 4;
    if (directionIs < 0) return directionIs += 4;
    return directionIs;
  }
};


class diffDirection
{
public:
  int operator() ( int diffTwoAngle )
  {
    if (diffTwoAngle ==  3) return -1;
    if (diffTwoAngle == -3) return  1;
    return diffTwoAngle;
  }
};


class fakeAtan2
{
public:
  int operator() ( const int iY, const int iX )
  {
    float X( static_cast<float>(iX) );
    float Y( static_cast<float>(iY) );

    if( Y == 0 )
    {
      return X>=0 ? 0 : 1800;
    }
    
    if( X == 0 )
    {
      return Y>0 ? +900 : -900;
    }
    
    float aY( absValue()(Y) );
    float aX( absValue()(X) );
    float angle_degree(0);
    
    if( aY <= aX )
    {
      angle_degree = (3667 * aX * aY) / (64 * X * X + 17 * Y * Y);
    }
    else
    {
      angle_degree = 90 - (3667 * aX * aY) / (64 * Y * Y + 17 * X * X);
    }
    
    if( X < 0 )
    {
      angle_degree = (Y<0) ? 180 + angle_degree : 180 - angle_degree;
    }
    else 
    {  
      if( Y < 0 )
        angle_degree = 360 - angle_degree;
    }
    
    if( angle_degree > 180 )    angle_degree = angle_degree - 360;
    else if( angle_degree < -180 )   angle_degree = angle_degree + 360;
    
    return static_cast<int>( angle_degree * 10);
  }
};


class integerSqrt
{
public:
  unsigned int operator () (unsigned int n)
  {
    unsigned int root = 0, t;
    
  #define iter1(N) \
    t = root + (1 << (N)); \
    if (n >= t << (N))   \
    {   n -= t << (N);   \
    root |= 2 << (N); \
    }
    
    iter1 (15);    iter1 (14);    iter1 (13);    iter1 (12);
    iter1 (11);    iter1 (10);    iter1 ( 9);    iter1 ( 8);
    iter1 ( 7);    iter1 ( 6);    iter1 ( 5);    iter1 ( 4);
    iter1 ( 3);    iter1 ( 2);    iter1 ( 1);    iter1 ( 0);
    
  #undef iter1
    return root >> 1;
  }
};


#define     isqrt(a) integerSqrt()(a)
#define Angle2Code(a) angle2Code<90>()( a )
#define   TruncDir(a) truncationCode()( a )
#define    DiffDir(a)  diffDirection()( a )


#endif//#define IMATH_HPP_
