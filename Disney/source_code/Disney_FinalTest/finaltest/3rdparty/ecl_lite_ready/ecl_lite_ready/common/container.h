#ifndef CONTAINER_HPP_
#define CONTAINER_HPP_


// I do not trust by push and pop class
// It shamed me actually
// Here i put bit different implementation
template<typename T, int _Size>
class Ring
{ 
public:
  Ring() : head(0), tail(0)
  {}
  void clear()
  {
    head = 0;
    tail = 0;
  }

  bool isFull()
  {
    return ( (( head + 1 ) % _Size) == tail );
  }

  bool isEmpty()
  {
    return (tail == head);
  }

  bool push( const T & data )
  {
    if( isFull() ) return false;

    buffer[head++] = data;
    if( head >= _Size )  head = 0;      

    return true;
  }

  bool pop( T & data )
  {
    if( isEmpty() ) return false;

    data = buffer[tail++];
    if( tail >= _Size ) tail = 0;
    return true;
  }

  int size()
  {
    return ((tail > head) ? _Size : 0) + head - tail;
  }

  int head;
  int tail;
  T buffer[_Size];
};

//template<typename T, int _Size>
//class Ring
//{ 
//public:
//  Ring() : head(0), tail(0), _size(0)
//  {}
//  void clear()
//  {
//    head = 0;
//    tail = 0;
//    _size = 0;
//  }
//
//  void push( const T & data )
//  {
//    buffer[head++] = data;
//    _size++;
//    
//    if( head >= _Size )
//    {
//      head = 0;
//
//      // todo; should be remove later, quite dum implementation
//      //_size = 0;
//      //tail = 0;
//    }
//  }
//
//  bool pop( T & data )
//  {
//    if( _size <= 0 ) return false;
//    data = buffer[tail++];
//    _size --;
//    if( tail >= _Size ) tail = 0;
//    return true;
//  }
//
//  int size()
//  {
//    return _size;
//  }
//
//  int head;
//  int tail;
//  int _size;
//  T buffer[_Size];
//};


template<typename T, int BufferSize=0>
class Array
{
public:
  Array()
  {}
  typedef T * iterator;

  T & operator[]( int i )
  {
    if( i < BufferSize && i >= 0 ) return array[i];  
    else return array[0];
  }

  const T & operator[]( int i ) const
  {
    if( i < BufferSize && i >= 0 ) return array[i];  
    else return array[0];
  }

  virtual iterator begin() { return array; }
  virtual iterator end() { return (array+BufferSize); }
  virtual int size() { return BufferSize; }
  
protected:
  T array[BufferSize];
};

#ifndef WIN32

template<typename T>
class Array<T,0>
{
public:
  Array( int numberOfData)
  : BufferSize(numberOfData), array( new T [BufferSize] )
  {}
  typedef T * iterator;

  T & operator[]( int i )
  {
    if( i < BufferSize && i >= 0 ) return array[i];  
    else return array[0];
  }

  const T & operator[]( int i ) const
  {
    if( i < BufferSize && i >= 0 ) return array[i];  
    else return array[0];
  }

  virtual iterator begin() { return array; }
  virtual iterator end() { return (array+BufferSize); }
  virtual int size() { return BufferSize; }
  
protected:
  
  int BufferSize;
  T * array;
};

#endif //#ifndef WIN32


// this is not safe for thread or multi-tasking in f/w
// please use the mutex thing for synchronzation when this is medium
// for communication (e.g. ring buffer)
template<typename T, int BufferSize=0>
class PushAndPop
{
public:
  PushAndPop() 
  :
  leader(0),
  follower(0)
  {
  }

  PushAndPop( const T & d )
  :
  leader(0),
  follower(0)
  {
    fill( d );
  }
  virtual ~PushAndPop() {}
  
  typedef T * iterator;
  iterator begin() { return data.begin(); }

  //this ensures that [0] returns always oldest data from the buffer
  T & operator[] (int idx)
  {
    return data[ ((follower+idx)%BufferSize) ];
  }

  const T & operator[] (int idx) const
  {
    return data[ ((follower+idx)%BufferSize) ];
  }

  void push_back( const T & datum )
  {
    data[ leader++ ] = datum;
    //leader %= BufferSize;
    if( leader >= BufferSize ) leader = 0;
  }

  T front()
  {
    return data[follower];
  }
  
  T pop_front()
  {
    T value = data[follower++];
    //follower %= BufferSize;
    if( follower >= BufferSize ) follower = 0;
    return value;
  }

  void fill( const T & d )
  {
    for( unsigned int i=0; i<BufferSize; i++ ) data[i] = d;
  }

  unsigned int asize()
  {
    return BufferSize;
  }

  int size()
  {
    if( leader > follower ) return leader - follower;
    else if( leader < follower ) return BufferSize-follower+leader;
    return 0;
  }

  void clear()
  {
    follower = 0;
    leader = 0;
  }

private:
  Array <T,BufferSize> data;
  int leader;
  int follower;
};


#ifndef WIN32

template<typename T>
class PushAndPop<T,0>
{
public:
  PushAndPop(int numberOfData) 
  :
  BufferSize(numberOfData),
  data(BufferSize),
  leader(0),
  follower(0)
  {
  }

  PushAndPop( const T & d, int numberOfData )
  :
  BufferSize(numberOfData),
  data(BufferSize),
  leader(0),
  follower(0)
  {
    fill( d );
  }
  virtual ~PushAndPop() {}

  //this ensures that [0] returns always oldest data from the buffer
//  T & operator[] (int idx)
//  {
//    return data[ ((follower+idx)%BufferSize) ];
//  }
//
//  const T & operator[] (int idx) const
//  {
//    return data[ ((follower+idx)%BufferSize) ];
//  }
  
  T & at( int idx )
  {
    return data[ ((follower+idx)%BufferSize) ];
  }

  const T & at( int idx ) const
  {
    return data[ ((follower+idx)%BufferSize) ];
  }

  void push_back( const T & datum )
  {
    data[ leader++ ] = datum;
    leader %= BufferSize;
  }

  T pop_front()
  {
    T value = data[follower++];
    follower %= BufferSize;

    return value;
  }

  void fill( const T & d )
  {
    for( unsigned int i=0; i<BufferSize; i++ ) data[i] = d;
  }

  unsigned int asize()
  {
    return BufferSize;
  }

  unsigned int size()
  {
    int cnt = follower-leader;
    return cnt;
  }

  void clear()
  {
    follower = 0;
    leader = 0;
  }

private:
  int BufferSize;
  Array <T>data;
  int leader;
  int follower;
};


#endif  // #ifndef WIN32


#endif

