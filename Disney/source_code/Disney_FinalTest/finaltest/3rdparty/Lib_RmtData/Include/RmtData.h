// RmtData.h: interface for the CRmtData class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_RMTDATA_H__8936090F_EB04_4FF4_BF0A_B2AF1FB3EE9A__INCLUDED_)
#define AFX_RMTDATA_H__8936090F_EB04_4FF4_BF0A_B2AF1FB3EE9A__INCLUDED_

#include "vipl.h"
#include "BaseSlamData.h"
#include "VipOpenCVTools.h"

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


enum rmtEvent
{
	eventImgRecv ,
	eventImgClose,
	eventRbtRecv ,
	eventRbtClose,
	eventMapRecv ,
	eventMapClose
};

class CRmtData  
{
public:
	static bool ConnectImg(const char *addr, int port);
	static bool ConnectRbt(const char *addr, int port);
	static bool ConnectMap(const char *addr, int port);
	static bool ConnectIMU(const char *addr, int port);
	
	static void CloseImg();
	static void CloseRbt();
	static void CloseMap();
	static void CloseIMU();
	
	static void CheckRecord(bool bTrue, const char *strIP);
	
	static void Finalize();
	
	static void SetSlamData(_Base3DSlamView *pData);
	
	static void SetCallbackOnEvent(void *pVoid, void (*cbEvent)(void *, rmtEvent, std::istream *));
	static void SendFormatString(const char* format, ...);
	
	static void ShowVision(bool bTrue);
	static const char *GetSensorInfo();
	static f3DPoint GetRealTimePose();

	static int  cmdDebug(int cmd);


	static void graphUpdate();
	static void graphReset();
	static void graphDrawData(int numImg, int numData, float scale, float range, CvScalar color);

	static const ByteImage & graphGetImg(int idx);
	static const ByteImage & graphGetImgOri(int idx);


};

#endif // !defined(AFX_RMTDATA_H__8936090F_EB04_4FF4_BF0A_B2AF1FB3EE9A__INCLUDED_)
