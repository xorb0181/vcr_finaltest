// SingleThread.h: interface for the CSingleThread class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_SINGLETHREAD_H__94EF2BD9_2D82_4A55_BF17_70BABF027188__INCLUDED_)
#define AFX_SINGLETHREAD_H__94EF2BD9_2D82_4A55_BF17_70BABF027188__INCLUDED_

#include "../OilBase.h"

#include "WaitEvent.h"

#ifdef _LINUX
	#include <pthread.h>
	#include <string>
#else
	#if _MSC_VER > 1000
	#pragma once
	#endif // _MSC_VER > 1000
#endif


#ifndef _WIN32_WCE
	#define OIL_THREAD_BASE_PRIORITY_LOWRT   15  // value that gets a thread to LowRealtime-1
	#define OIL_THREAD_BASE_PRIORITY_MAX     2   // maximum thread base priority boost
	#define OIL_THREAD_BASE_PRIORITY_MIN    -2  // minimum thread base priority boost
	#define OIL_THREAD_BASE_PRIORITY_IDLE   -15 // value that gets a thread to idle

	#define OIL_THREAD_PRIORITY_ERROR_RETURN    (0x7fffffff)

	#define OIL_THREAD_PRIORITY_TIME_CRITICAL   OIL_THREAD_BASE_PRIORITY_LOWRT
	#define OIL_THREAD_PRIORITY_HIGHEST          OIL_THREAD_BASE_PRIORITY_MAX
	#define OIL_THREAD_PRIORITY_ABOVE_NORMAL    (OIL_THREAD_PRIORITY_HIGHEST-1)
	#define OIL_THREAD_PRIORITY_NORMAL           0
	#define OIL_THREAD_PRIORITY_BELOW_NORMAL    (OIL_THREAD_PRIORITY_LOWEST+1)
	#define OIL_THREAD_PRIORITY_LOWEST           OIL_THREAD_BASE_PRIORITY_MIN
	#define OIL_THREAD_PRIORITY_IDLE            OIL_THREAD_BASE_PRIORITY_IDLE

#else
	#define OIL_THREAD_PRIORITY_TIME_CRITICAL   0
	#define OIL_THREAD_PRIORITY_HIGHEST         1
	#define OIL_THREAD_PRIORITY_ABOVE_NORMAL    2
	#define OIL_THREAD_PRIORITY_NORMAL          3
	#define OIL_THREAD_PRIORITY_BELOW_NORMAL    4
	#define OIL_THREAD_PRIORITY_LOWEST          5
	#define OIL_THREAD_PRIORITY_ABOVE_IDLE      6
	#define OIL_THREAD_PRIORITY_IDLE            7
#endif

/*
bool xxxxxxxxxxx::cbLoop(void* pVoid) { return ((xxxxxxxxxxx *)pVoid)->OnLoop(); }
bool xxxxxxxxxxx::OnLoop()
*/

class OILLIB_API CSingleThread  
{
public:
	CSingleThread(const char *strName = NULL);
	virtual ~CSingleThread();

public:
	void SetName(const char *strName);
	bool BeginThread(
		bool( __cdecl *cbLoop)( void * ), 
		void *arglist,
		unsigned stack_size = 0, 
		int priority = OIL_THREAD_PRIORITY_NORMAL);

	void StopThread();
	void PauseThread (unsigned int dwTimeOut = 0xFFFFFFFF);
	void ResumeThread();

	bool IsThreadOn();

protected:
	virtual bool vtlRun() { return false; }

private:

	#ifdef _LINUX
		static void* _cbThreadLoop(void *pParam);
			   void* _OnThreadLoop();
	#else
		static unsigned long __stdcall 	_cbThreadLoop(void *pParam);
			   unsigned long            _OnThreadLoop();
	#endif
	
private:

	#ifdef _LINUX
		pthread_t  m_threadid;
	#else
		HANDLE     m_hHandle;
	#endif
	
	CWaitEvent m_wePause;
	bool       m_bThread;
	bool       m_bPause;
	void      *m_pVoid;
	char      *m_strThreadName;

	bool( __cdecl *m_cbLoop)( void * );
};
#endif // !defined(AFX_SINGLETHREAD_H__94EF2BD9_2D82_4A55_BF17_70BABF027188__INCLUDED_)

