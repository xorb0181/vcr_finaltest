// Sync.cpp: implementation of the CSync class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "Sync.h"


#ifdef _LINUX
	#include <string>
	#include <stdio.h>
#else
// 	#ifdef _DEBUG
// 	#undef THIS_FILE
// 	static char THIS_FILE[]=__FILE__;
// 	#define new DEBUG_NEW
// 	#endif
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CSync::CSync(const char *name)
{
	m_strName = new char[strlen(name)+1];
	strcpy(m_strName, name);

#ifdef _LINUX
	int status 	= pthread_mutex_init(&m_mutex, NULL);
	if( status != 0 ) { }
#else
	#ifndef DEAD_LOCK_CHECKING
		InitializeCriticalSection(&m_cs);
	#endif
#endif

}

CSync::~CSync()
{
	delete [] m_strName;
#ifdef _LINUX
	pthread_mutex_destroy(&m_mutex);
#else
	#ifndef DEAD_LOCK_CHECKING
		DeleteCriticalSection(&m_cs);
	#endif
#endif
}

void CSync::Lock()
{
#ifdef _LINUX
	int status;
	#ifdef DEAD_LOCK_CHECKING
		status = pthread_mutex_lock(&m_mutex);
	#else
		for (int i = 0 ; i < 5000 ; i++)
		{
			status = pthread_mutex_trylock(&m_mutex);
			if (status == 0)
				break;
			usleep(10*1000);
		}
	#endif

	if( status != 0 )
	{
		printf("Error mutex lock error [%s]\n", m_strName );
		ASSERT(1);
	}
#else
	#ifdef DEAD_LOCK_CHECKING
		if (!m_cs.Lock(5000))
		{
			char msg[100]; sprintf(msg, "Critical Error !!!!\nDead Lock State Ocurred in %s", m_strName);
			AfxMessageBox(msg);
			ASSERT(0);
		}
	#else
		EnterCriticalSection(&m_cs);
	#endif
#endif
}

void CSync::Unlock()
{
#ifdef _LINUX
	int status = pthread_mutex_unlock(&m_mutex);
	if( status != 0 )
	{
		printf("Error mutex unlock error [%s]\n", m_strName );
		ASSERT(1);
	}
#else
	#ifdef DEAD_LOCK_CHECKING
		m_cs.Unlock();
	#else
		LeaveCriticalSection(&m_cs);
	#endif
#endif
}
