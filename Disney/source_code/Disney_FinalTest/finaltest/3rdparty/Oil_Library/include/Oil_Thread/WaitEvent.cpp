// WaitEvent.cpp: implementation of the CWaitEvent class.
//
//////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "WaitEvent.h"

#ifdef _LINUX
	#include <pthread.h>
	#include <errno.h>
#else
	#ifdef _DEBUG
	#undef THIS_FILE
	static char THIS_FILE[]=__FILE__;
	#define new DEBUG_NEW
	#endif
#endif

CWaitEvent::CWaitEvent()
{
#ifdef _LINUX
	int status;
	status = pthread_mutex_init(&m_mutex, NULL);
	status = pthread_cond_init(&m_cond, NULL);
	m_flag = 0;
#else
	m_bEndEvent = false;
	m_hEndEvent = CreateEvent(0, false, false, NULL);
#endif
}

CWaitEvent::~CWaitEvent()
{
#ifdef _LINUX
	pthread_cond_destroy(&m_cond);
	pthread_mutex_destroy(&m_mutex);
#else
	CloseHandle(m_hEndEvent);
#endif
}

unsigned int CWaitEvent::WaitEvent(unsigned int dwMilliseconds)
{
#ifdef _LINUX
	int status;

	if( dwMilliseconds == 0xFFFFFFFF )
	{
		pthread_mutex_lock(&m_mutex);
		while( m_flag == 0 )
		{
			status = pthread_cond_wait(&m_cond, &m_mutex);
			if( status != 0 )
				break;
		}
		pthread_mutex_unlock(&m_mutex);

		if( m_flag != 1 )
		{
			m_flag = 0;
			return WAIT_FAILED;
		}

		m_flag = 0;
		return WAIT_OBJECT_0;
	}

	struct timespec timeout;

	timeout.tv_sec 	= time(NULL) + dwMilliseconds / 1000;
	timeout.tv_nsec = (dwMilliseconds % 1000) * 1000;
	
	pthread_mutex_lock(&m_mutex);
	while( m_flag == 0 )
	{
		status = pthread_cond_timedwait(&m_cond, &m_mutex, &timeout);
		if( status == ETIMEDOUT )
		{
			pthread_mutex_unlock(&m_mutex);
			m_flag = 0;
			return WAIT_TIMEOUT;
		}
	}
	pthread_mutex_unlock(&m_mutex);

	m_flag = 0;
	return WAIT_OBJECT_0;
#else
	m_bEndEvent = true;
	unsigned int ret = WaitForSingleObject(m_hEndEvent, dwMilliseconds);
	ResetEvent(m_hEndEvent);
	return ret;
#endif
}

void CWaitEvent::SetEvent ()
{
#ifdef _LINUX
	pthread_mutex_lock(&m_mutex);
	m_flag = 1;
	pthread_cond_signal(&m_cond);
	pthread_mutex_unlock(&m_mutex);
#else
	if (m_bEndEvent)
	{
		m_bEndEvent = false;
		::SetEvent(m_hEndEvent);
	}
#endif
}
