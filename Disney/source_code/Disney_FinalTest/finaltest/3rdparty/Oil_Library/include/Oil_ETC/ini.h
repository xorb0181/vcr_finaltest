#if !defined(_UTILS_H_INCLUDED_)
#define _UTILS_H_INCLUDED_
#include "../OilBase.h"

OILLIB_API bool ReadINIKey (const char * filename, const char * index, const char * name, char * pBuf, int nBufSize);
OILLIB_API bool WriteINIKey(const char * filename, const char * index, const char * name, const char *format, ...);

#endif

