// ClientSock.cpp: implementation of the CClientSock class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "ClientSock.h"
#include "dksocket.h"

#include "assert.h"

// #ifdef _DEBUG
// #undef THIS_FILE
// static char THIS_FILE[]=__FILE__;
// #define new DEBUG_NEW
// #endif

#ifdef _LINUX
	#define _msg printf
#else
	#ifndef _WIN32_WCE
		#define _msg
	#else
		#define _msg printf
	#endif
#endif // _LINUX


static char *strDefault = (char*)"CClientSock";

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CClientSock::CClientSock(const char *strName) : 
m_szSocketName(strDefault),
m_csRecv("CClientSock")
{
	char strTemp[256];
	m_hSocket   = 0;
	m_cbOnRecv  = NULL;
	m_cbOnClose = NULL;
	m_nRecvTimeOut = -1;
	if (strName)
	{
		SetName(strName);
		sprintf(strTemp, "%s:CClientSock", strName);
		m_Thread.SetName(strTemp);
	}
	else
		m_Thread.SetName(strDefault);

	m_Thread.PauseThread();

	m_bInit = false;
}

bool CClientSock::Create()
{
	if (!m_bInit)
	{
		int ret = m_Thread.BeginThread(cbRecvLoop, this, 0);
		m_bInit = true;
		return ret ? true : false;
	}
	return true;
}

void CClientSock::SetName(const char *strName)
{
	if (strName)
	{
		int size = strlen(strName);
		m_szSocketName = new char[size+1];
		strcpy(m_szSocketName, strName);
	}
}

CClientSock::~CClientSock()
{
	_CloseSocket();
	m_Thread.StopThread();

	if (m_szSocketName != strDefault)
		delete [] m_szSocketName;
}

bool CClientSock::Connect(const char *addr, int port)
{
	Create();
	Close();
	SOCKET hSocket = sock_create_client();
	int ret = sock_connect(hSocket, addr, port);

	if (ret == 1)
		return StartRecv(hSocket);
	else
	{
		sock_close(hSocket);
		return false;
	}
}

void CClientSock::_CloseSocket()
{
	m_csRecv.Lock  ();
	if (m_hSocket) 
	{
		_msg("[CClientSock:%s] Socket Close\n", m_szSocketName);
		sock_close(m_hSocket); 
		m_hSocket = 0; 
	}
	m_csRecv.Unlock();
}

void CClientSock::Close()
{
	_CloseSocket();
	m_Thread.PauseThread();
}

void CClientSock::_OnClose()
{
	_CloseSocket();
	if (m_cbOnClose) m_cbOnClose(m_pvOnClose, 0);
}

bool CClientSock::cbRecvLoop(void *pVoid) { return ((CClientSock *)pVoid)->OnRecvLoop(); }
bool CClientSock::OnRecvLoop()
{
	if (!m_hSocket ) { Sleep(1); return true; }

	/*
	int ret = TCPPacket::SockRecvWait(m_hSocket, -1);

	if (ret > 0)
	{
		m_csRecv.Lock  ();
		ret = m_hSocket ? m_pktRecv.Receive(m_hSocket, m_nRecvTimeOut) : TCPPacket::ERR_USER_CLOSE;
		m_csRecv.Unlock();
	}
	*/
	int ret = m_pktRecv.Receive(m_hSocket, m_nRecvTimeOut);

	switch(ret)
	{
	case 0: assert(0);
	case TCPPacket::ERR_TIME_OUT     : // Time out is detected
		{
			_msg("[CClientSock:%s] Recv time out [%d msec]\n", m_nRecvTimeOut);
			_OnClose();
		}
		break;
	case TCPPacket::ERR_SOCKET_ERROR : // Socket Error is detected
		{
			_msg("[CClientSock:%s] Detect socket error, socket will be closed\n", m_szSocketName);
			_OnClose();
		}
		break;
	case TCPPacket::ERR_SOCKET_CLOSED: // Socket is gracefully closed
		{
			_msg("[CClientSock:%s] Detect socket close\n", m_szSocketName);
			_OnClose();
		}
		break;
	case TCPPacket::ERR_USER_CLOSE   : // Socket is gracefully closed by user command
		{
			_msg("[CClientSock:%s] Socket is closed manually\n", m_szSocketName);
		}
		break;
	default:
		{
			const char *buf = m_pktRecv.GetBody();
			const int size  = m_pktRecv.GetMsgSize();
			if (m_cbOnRecv)
				m_cbOnRecv(m_pvOnRecv, buf, size);
			else
				_msg("[CClientSock:%s] Recv Callback is NULL\n", m_szSocketName);
		}
	}
	Sleep(1);
	return true;
}

void CClientSock::SetRecvTimeOut(int time_out)
{
	m_nRecvTimeOut = time_out;
}

void CClientSock::SetCallBackOnReceive(void *pVoid, void (*cbOnRecv)(void *, const char *, int))
{
	m_cbOnRecv = cbOnRecv;
	m_pvOnRecv = pVoid;
}

void CClientSock::SetCallBackOnClose  (void *pVoid, void(*cbOnClose)(void *, int ))
{
	m_cbOnClose = cbOnClose;
	m_pvOnClose = pVoid;
}

bool CClientSock::StartRecv(SOCKET hSock)
{
	Close();
	m_hSocket = hSock;
	_msg("[CClientSock:%s] Start Recv\n", m_szSocketName);
	m_Thread.ResumeThread();
	return true;
}

int CClientSock::SendString(const char *pBuf)
{
	if (!m_hSocket) return false;

	int sendSize = m_pktSend.SendString(m_hSocket, pBuf);
	if (sendSize < 0)
	{
//		_OnClose();
		if (m_cbOnClose) m_cbOnClose(m_pvOnClose, 0);
	}
	return sendSize;
}

int CClientSock::SendBinary(const char *pBuf, int size)
{
	if (!m_hSocket) return false;

	int sendSize = m_pktSend.SendBinary(m_hSocket, pBuf, size);
	if (sendSize < 0)
	{
//		_OnClose();
		if (m_cbOnClose) m_cbOnClose(m_pvOnClose, 0);
	}
	return sendSize;
}

bool CClientSock::IsConnected()
{
	return m_hSocket ? true : false;
}
