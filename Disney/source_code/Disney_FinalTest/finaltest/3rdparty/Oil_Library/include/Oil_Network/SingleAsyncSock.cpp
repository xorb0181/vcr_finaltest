// SingleSock.cpp: implementation of the CSingleAsyncSock class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "SingleAsyncSock.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CSingleAsyncSock::CSingleAsyncSock(int type, const char *strName) : m_type(type)
{

}

CSingleAsyncSock::~CSingleAsyncSock()
{

}

void CSingleAsyncSock::Close()
{
	m_SSock.Close();
	m_SSock.m_CSock.Close();
	m_SSock.m_CSock.m_bIsConnected = false;
}

void CSingleAsyncSock::Listen(int portNum)
{
	ASSERT(m_type == 0);
	m_SSock.Create(portNum);
	m_SSock.Listen();
}

bool CSingleAsyncSock::Connect(const char *addr, int port)
{
	ASSERT(m_type == 1);
	m_SSock.m_CSock.Close();
	m_SSock.m_CSock.Create();
	return m_SSock.m_CSock.Connect(addr, port) ? true : false;
}

void CSingleAsyncSock::SetCallBackOnReceive(void *pVoid, void (*cbReceive )(void *, const char *, int) )
{
	m_SSock.m_CSock.m_cbReceive = cbReceive;
	m_SSock.m_CSock.m_pvReceive = pVoid;
}

void CSingleAsyncSock::SetCallBackOnClose  (void *pVoid, void (*cbOnClose )(void *, int ) )
{
	m_SSock.m_CSock.m_cbOnClose = cbOnClose;
	m_SSock.m_CSock.m_pvOnClose = pVoid;
}

void CSingleAsyncSock::SetCallBackOnAccept(void *pVoid, void (*cbOnAccept)(void *, int ))
{
	ASSERT(m_type == 0);
	m_SSock.m_cbOnAccept = cbOnAccept;
	m_SSock.m_pvOnAccept = pVoid;
}

void CSingleAsyncSock::SetCallBackOnConnect(void *pVoid, void (*cbOnConnect)(void *, int ) )
{
	ASSERT(m_type == 1);
	m_SSock.m_CSock.m_cbOnConnect = cbOnConnect;
	m_SSock.m_CSock.m_pvOnConnect = pVoid;
}

int CSingleAsyncSock::SendString(const char *pBuf)
{
	if (!m_SSock.m_CSock.m_bIsConnected) return -1;

	return m_sendPacket.SendString(m_SSock.m_CSock, pBuf);
}

int CSingleAsyncSock::SendBinary(const char *pBuf, int size)
{
	if (!m_SSock.m_CSock.m_bIsConnected) return - 1;
	return m_sendPacket.SendBinary(m_SSock.m_CSock, pBuf, size);
}

int CSingleAsyncSock::SendFormatString(const char* format, ...)
{
	if (!m_SSock.m_CSock.m_bIsConnected) return -1;

	va_list		args;
	char		temp[3000];

	va_start(args, format);
	vsprintf(temp, format, args);
	
	return SendString(temp);
}


void CSingleAsyncSock::ResetBuffer()
{
	m_sendPacket.ResetBuffer();
}

void CSingleAsyncSock::AddDataToBuffer(const char *pBuf, int size)
{
	m_sendPacket.AddDataToBuffer(pBuf, size);
}

void CSingleAsyncSock::AddDataToBuffer(int    value)
{
	m_sendPacket.AddDataToBuffer( (const char*)(&value), sizeof(int   ) );
}

void CSingleAsyncSock::AddDataToBuffer(float  value)
{
	m_sendPacket.AddDataToBuffer( (const char*)(&value), sizeof(float ) );
}

void CSingleAsyncSock::AddDataToBuffer(double value)
{
	m_sendPacket.AddDataToBuffer( (const char*)(&value), sizeof(double) );
}

int CSingleAsyncSock::SendDataInBuffer()
{
	if (!m_SSock.m_CSock.m_bIsConnected) return - 1;
	return m_sendPacket.SendDataInBuffer(m_SSock.m_CSock);
}


/////////////////////////////////////////////////////////////////////////////
// CSingleCSock

CSingleCSock::CSingleCSock()
{
	m_cbReceive   = NULL;
	m_cbOnClose   = NULL;
	m_cbOnConnect = NULL;
	m_bIsConnected = false;
}

CSingleCSock::~CSingleCSock()
{
}


// Do not edit the following lines, which are needed by ClassWizard.
#if 0
BEGIN_MESSAGE_MAP(CSingleCSock, CAsyncSocket)
	//{{AFX_MSG_MAP(CSingleCSock)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()
#endif	// 0

/////////////////////////////////////////////////////////////////////////////
// CSingleCSock member functions

void CSingleCSock::OnReceive(int nErrorCode) 
{
	int size = m_packet.Receive(*this);

	if (m_cbReceive && size >= 0)
	{
		switch(m_packet.GetMsgType())
		{
		case TCPPacket::MSG_STRING: m_cbReceive(m_pvReceive, m_packet.GetBody  (),                    -1); break;
		case TCPPacket::MSG_BINARY: m_cbReceive(m_pvReceive, m_packet.GetBody  (), m_packet.GetMsgSize()); break;
		}
	}

	CAsyncSocket::OnReceive(nErrorCode);
}

void CSingleCSock::OnConnect(int nErrorCode) 
{
	if (nErrorCode)
		m_bIsConnected = false;
	else
		m_bIsConnected = true;

	if (m_cbOnConnect) m_cbOnConnect(m_pvOnConnect, nErrorCode);
	
	CAsyncSocket::OnConnect(nErrorCode);
}

void CSingleCSock::OnClose(int nErrorCode) 
{
	m_bIsConnected = false;
	Close();
	if (m_cbOnClose)
		m_cbOnClose(m_pvOnClose, nErrorCode);
	
	CAsyncSocket::OnClose(nErrorCode);
}

/////////////////////////////////////////////////////////////////////////////
// CSingleSSock

CSingleSSock::CSingleSSock()
{
	m_cbOnAccept = NULL;
	m_pvOnAccept = NULL;
}

CSingleSSock::~CSingleSSock()
{
}


// Do not edit the following lines, which are needed by ClassWizard.
#if 0
BEGIN_MESSAGE_MAP(CSingleSSock, CAsyncSocket)
	//{{AFX_MSG_MAP(CSingleSSock)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()
#endif	// 0

/////////////////////////////////////////////////////////////////////////////
// CSingleSSock member functions

void CSingleSSock::OnAccept(int nErrorCode) 
{
	m_CSock.m_bIsConnected = false;
	m_CSock.Close();

	Accept(m_CSock);
	m_CSock.m_bIsConnected = true;
	
	CAsyncSocket::OnAccept(nErrorCode);

	if (m_cbOnAccept) m_cbOnAccept(m_pvOnAccept, nErrorCode);
}
