//
// hmcho_socket.h
//
//

#ifndef __HMCHO_2001_02_20_SOCKET_H__
#define __HMCHO_2001_02_20_SOCKET_H__

#include <stdio.h>
#include <stdlib.h>
//#include <fcntl.h>
//#include <sys/types.h>


#define STRING_PACKET_SIZE		2048
#define TIMEOUT					0
#define ERR						-1
#define TERMI_EVENT				-999

unsigned long nameToAddr(const char* name);

int  sock_init();
void sock_release();

void sock_close(SOCKET sock_id);

SOCKET sock_create_udp();
SOCKET sock_create_client();
SOCKET sock_create_server(int m_nPort);

SOCKET sock_accept( SOCKET sock_server, int &nTimeOut );

int sock_connect(const SOCKET sock_id, const char *hostname, const int port);

int sock_send(SOCKET sock_id, const void *vptr, size_t n);
int sock_recv(SOCKET sock_id, void *vptr, int n);

char *get_wsaerrstring(const int no);

#endif

