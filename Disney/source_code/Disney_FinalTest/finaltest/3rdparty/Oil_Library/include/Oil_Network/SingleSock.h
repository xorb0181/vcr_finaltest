// SingleSocket.h: interface for the CSingleSock class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_SINGLESOCKET_H__F65CEE8F_826B_4A4F_8968_45217F96001C__INCLUDED_)
#define AFX_SINGLESOCKET_H__F65CEE8F_826B_4A4F_8968_45217F96001C__INCLUDED_

#include "../OilBase.h"

#include "ServerSock.h"
#include "ClientSock.h"
#include "TCPPacket.h"

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// template
/*
static bool cbSendLoop(void *pVoid);
       bool OnSendLoop();
static void cbRecv(void *pVoid, const char *pBuf, int bufSize) { ((xxxxxx*)pVoid)->OnRecv(pBuf, bufSize); }
       void OnRecv(             const char *pBuf, int bufSize);
static void cbEvent(void *pVoid, int event, int param1, int param2) { ((xxxxxx*)pVoid)->OnEvent(event, param1, param2); }
       void OnEvent(             int event, int param1, int param2);
*/

class OILLIB_API CSingleSock  
{
public:
	CSingleSock(int type, const char *strName = NULL); // type = 0 server, 1 client
	virtual ~CSingleSock();

	enum
	{
		eRecv    = 0,
		eClose   = 1,
		eAccept  = 2,
		eLoop    = 4,
		eLoopRaw = 5
	};

public:
	bool Create();
	void SetCallBackOnEvent  (void *pVoid, void (*cbEvent)(void *, int, int, int) );

public:
	bool Listen(int portNum);
	bool Connect(const char *addr, int port);
	bool IsConnected() { return m_csock.IsConnected(); }

	void Close();
	void CloseClient();

public:
	int  SendString      (const char* pBuf       );
	int  SendFormatString(const char* format, ...);

	int  SendBinary (const char *pBuf, int size);
	int  SendRawData(const char *pBuf, int size);

public:
	static int  GetHeaderSize();
	static void MakeHeader(char *pBuf, int bufSize);

public:
	SOCKET GetSocketHandle() { return m_csock.GetSocketHandle(); }

protected:
	const int m_type;
//	TCPPacket m_sendPacket;

protected:
	static void _cbAccept (void *pVoid, int nErrorCode);
	       void _OnAccept (             int nErrorCode);
	static void _cbRecv (void *pVoid, const char *pBuf, int size);
	       void _OnRecv (             const char *pBuf, int size);
	static void _cbClose(void *pVoid, int nErrorCode);
	       void _OnClose(             int nErrorCode);

protected:
	void (*m_cbEvent)(void *, int, int, int);
	void  *m_pvEvent;

protected:
	CClientSock m_csock;
	CServerSock m_ssock;
};

void FragSend(char *pBuf, const int size, CSingleSock &sock);

#endif // !defined(AFX_SINGLESOCKET_H__F65CEE8F_826B_4A4F_8968_45217F96001C__INCLUDED_)
