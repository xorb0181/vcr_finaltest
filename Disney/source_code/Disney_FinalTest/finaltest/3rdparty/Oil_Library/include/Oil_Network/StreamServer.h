// GCamServer.h: interface for the CGCamServer class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_GCAMSERVER_H__62ADBCF5_A403_47E2_917C_46E3AAF551D6__INCLUDED_)
#define AFX_GCAMSERVER_H__62ADBCF5_A403_47E2_917C_46E3AAF551D6__INCLUDED_


//#include "vipl.h"

#include "SingleSock.h"

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


/* template
	static void cbStream(void *pVoid, const BYTE *&buf, int &size) { ((xxxx*)pVoid)->OnStream(buf, size); }
	       void OnStream(             const BYTE *&buf, int &size);
*/

/*
switch(event)
{
case CStreamServer::eRecv  : // slam parameter ����
	{
	}
	break;
case CStreamServer::eClose :
	{
	}
	break;
case CStreamServer::eAccept:
	{
	}
	break;
case CStreamServer::eLoop  :
	{
		BYTE **ppBuf = (BYTE **)param1;
		int   *pSize = (int  * )param2;
		*ppBuf = (BYTE*)sendBuf;
		*pSize = os.pcount();
	}
	break;
}
*/

class OILLIB_API CStreamServer : public CSingleSock
{
public:
	CStreamServer();
	virtual ~CStreamServer();
	
public:
	bool Start(int portNum);
	void Stop ();

protected:
	static bool _cbSendLoop(void *pVoid);
	       bool _OnSendLoop();
protected:
	CSingleThread m_thread;
};

#endif // !defined(AFX_GCAMSERVER_H__62ADBCF5_A403_47E2_917C_46E3AAF551D6__INCLUDED_)
