// stdafx.h : include file for standard system include files,
//  or project specific include files that are used frequently, but
//      are changed infrequently
//

#if !defined(AFX_STDAFX_H__8F9A32CC_5671_4C1C_96DD_5D9BB56667F7__INCLUDED_)
#define AFX_STDAFX_H__8F9A32CC_5671_4C1C_96DD_5D9BB56667F7__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#define VC_EXTRALEAN		// Exclude rarely-used stuff from Windows headers

#include <afx.h>
#include <afxwin.h>

#include <afxsock.h>
// TODO: reference additional headers your program requires here

//{{AFX_INSERT_LOCATION}}
// Microsoft eMbedded Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_STDAFX_H__8F9A32CC_5671_4C1C_96DD_5D9BB56667F7__INCLUDED_)
