// VGrabber.h: interface for the VGrabber class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_VGRABBER_H__1E45AF99_91E6_41C2_BB59_5AAFF53E1A39__INCLUDED_)
#define AFX_VGRABBER_H__1E45AF99_91E6_41C2_BB59_5AAFF53E1A39__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class VGrabber  
{
public:
	virtual bool Initialize(int width, int height) { return true;}
	virtual void Finalize() {}
	virtual bool Grab(unsigned char *&dst, int &step, int &width, int &height) { Sleep(1); return false; }// = 0;
	virtual bool GetJpeg(unsigned char *buf, int &size) { return false; }
	virtual void ClockEnable(bool bTrue) {}

public:	
	virtual bool  WriteIIC(DWORD code, DWORD data) { return false; }
	virtual DWORD  ReadIIC(DWORD code) { return 0; }

	virtual int UserFunction(int cmd, int param1=0, int param2=0) { return 0; }
};

#endif // !defined(AFX_VGRABBER_H__1E45AF99_91E6_41C2_BB59_5AAFF53E1A39__INCLUDED_)
