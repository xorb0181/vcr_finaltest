// ImShowFunc.h: interface for the CImShowFunc class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_IMSHOWFUNC_H__CE798265_9445_4075_83D4_D3745B39A515__INCLUDED_)
#define AFX_IMSHOWFUNC_H__CE798265_9445_4075_83D4_D3745B39A515__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

void ImShow(const ByteImage &input, const char *name = NULL);
void CloseFigure(const char *name);
bool SetFigureMouseCallBack(const char *name, void (*cbMsgFunc)(void *, UINT, UINT, CPoint), void *pVoid);

void      GetFigureNameList(CStringArray &array);
ByteImage GetImgFromFigureName(const char *name);
void      SetImgFromFigureName(const char *name, const ByteImage &img);
ByteImage SelectImShowImage();

fPointArray GInput(const char *name, int nPosCount = 0);

#endif // !defined(AFX_IMSHOWFUNC_H__CE798265_9445_4075_83D4_D3745B39A515__INCLUDED_)
