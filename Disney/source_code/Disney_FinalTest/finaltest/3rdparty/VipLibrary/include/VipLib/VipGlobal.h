#if !defined(_VIP_GLOBAL_)
#define _VIP_GLOBAL_

#ifdef _LINUX

#ifndef __max
	#define __max(a,b)  (((a) > (b)) ? (a) : (b))
#endif
#ifndef __min
	#define __min(a,b)  (((a) < (b)) ? (a) : (b))
#endif

#ifndef _MAX_PATH
	#define _MAX_PATH   260 // max. length of full pathname
	#define _MAX_DRIVE  3   // max. length of drive component
	#define _MAX_DIR    256 // max. length of path component
	#define _MAX_FNAME  256 // max. length of file name component
	#define _MAX_EXT    256 // max. length of extension component
#endif

#endif

#endif // end _VIP_GLOBAL_
