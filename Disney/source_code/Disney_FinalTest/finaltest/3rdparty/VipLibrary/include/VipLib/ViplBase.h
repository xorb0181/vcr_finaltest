#ifndef _DEF_VIPL_BASE_
#define _DEF_VIPL_BASE_

#ifdef _LINUX
	#define VIPLIB_API
#else
	#ifdef _viplib_emb_
		#define VIPLIB_API
	#else
		#ifdef VIPLIB_EXPORTS
			#define VIPLIB_API __declspec(dllexport)
		#else
			#define VIPLIB_API __declspec(dllimport)
		#endif
	#endif
#endif

#endif   //  for _DEF_VIPL_BASE_
