// VipEmbTools.h: interface for the CVipEmbTools class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_VIPEMB_HEADER_H__58BA123EBD2_8E5D_483F_ACCF_C0CF035DBB23__INCLUDED_)
#define AFX_VIPEMB_HEADER_H__58BA123EBD2_8E5D_483F_ACCF_C0CF035DBB23__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifdef _LINUX
	#ifndef _USE_IPP_XSC_
		#define _USE_IPP_XSC_
	#endif
#endif

#ifndef _WIN32_WCE
	#define _USE_IPP_X86_
#endif

#ifdef _USE_IPP_X86_
	#ifdef _IPP_WRAP_HEADER_DEFINED_
		#error Duplicated define of CPU Type
	#endif
	#define _IPP_WRAP_HEADER_DEFINED_
	#include "ippi.h"
	#include "ippj.h"
	#include "ipps.h"
	#include "ippalign.h"

	#pragma comment(lib, "ipps")
	#pragma comment(lib, "ippi")
	#pragma comment(lib, "ippj")
#endif

#ifdef _USE_IPP_XSC_
	#ifdef _IPP_WRAP_HEADER_DEFINED_
		#error Duplicated define of CPU Type
	#endif
	#define _IPP_WRAP_HEADER_DEFINED_
	#include "ippIP.h"
	#include "ippJP.h"
	#include "ippSP.h"
	#ifdef _DEBUG
		#pragma comment(lib, "ippSP_XSC41PPC_d")
		#pragma comment(lib, "ippIP_XSC41PPC_d")
		#pragma comment(lib, "ippJP_XSC41PPC_d")
	#else
		#pragma comment(lib, "ippSP_XSC41PPC_r")
		#pragma comment(lib, "ippIP_XSC41PPC_r")
		#pragma comment(lib, "ippJP_XSC41PPC_r")
	#endif
#endif

#ifdef _USE_IPP_WMMX_
	#ifdef _IPP_WRAP_HEADER_DEFINED_
		#error Duplicated define of CPU Type
	#endif
	#define _IPP_WRAP_HEADER_DEFINED_
	#include "ippIP.h"
	#include "ippJP.h"
	#include "ippSP.h"
	#ifdef _DEBUG
		#pragma comment(lib, "ippSP_WMMX41PPC_d")
		#pragma comment(lib, "ippIP_WMMX41PPC_d")
		#pragma comment(lib, "ippJP_WMMX41PPC_d")
	#else
		#pragma comment(lib, "ippSP_WMMX41PPC_r")
		#pragma comment(lib, "ippIP_WMMX41PPC_r")
		#pragma comment(lib, "ippJP_WMMX41PPC_r")
	#endif
#endif


#ifdef _USE_IPP_IXP4xx
	#ifdef _IPP_WRAP_HEADER_DEFINED_
		#error Duplicated define of CPU Type
	#endif
	#define _IPP_WRAP_HEADER_DEFINED_
	#include "ippdefs.h"
	#include "ippi.h"
	#include "ippj.h"
	#include "ipps.h"
	#pragma comment(lib, "ippssxl")
	#pragma comment(lib, "ippisxl")
	#pragma comment(lib, "ippjsxl")

	#pragma comment(lib, "ippcorel")
#endif

#endif // !defined(AFX_VIPEMB_HEADER_H__58BA123EBD2_8E5D_483F_ACCF_C0CF035DBB23__INCLUDED_)
