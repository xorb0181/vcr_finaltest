#if !defined(_POINT_FUNCTIONS)
#define _POINT_FUNCTIONS

#include "ViplBase.h"
/** @file PointFunction.h */

#include "TemplPoint.h"

VIPLIB_API f3DPoint cross(const f3DPoint &op1, const f3DPoint &op2);

VIPLIB_API float dot(const f3DPoint &A, const f3DPoint &B);
VIPLIB_API float dot(const f2DPoint &A, const f2DPoint &B);

VIPLIB_API float sq_sum (const f2DPoint &A);
VIPLIB_API float sq_sum (const f3DPoint &A);

VIPLIB_API float norm (const f2DPoint &A);
VIPLIB_API float norm (const f3DPoint &A);

VIPLIB_API float dist(const f2DPoint &A, const f2DPoint &B);
VIPLIB_API float dist(const f3DPoint &A, const f3DPoint &B);

VIPLIB_API f2DPoint abs(const f2DPoint &A);
VIPLIB_API f3DPoint abs(const f3DPoint &A);

VIPLIB_API f2DPoint sqrt(const f2DPoint &A);
VIPLIB_API f3DPoint sqrt(const f3DPoint &A);

VIPLIB_API f2DPoint pow(const f2DPoint &A, float b);
VIPLIB_API f3DPoint pow(const f3DPoint &A, float b);

VIPLIB_API fPoint rotate (const fPoint &A, const float c, const float s);
VIPLIB_API fPoint rotate (const fPoint &A, const float radAngle        );
VIPLIB_API fPoint rotate (const fPoint &A, const fPoint &center, const float radAngle        );
VIPLIB_API fPoint rotate (const fPoint &A, const fPoint &center, const float c, const float s);

template <class T>
Tmpl2DPoint <T> MeanPointArray(const CVipArrBase < Tmpl2DPoint<T> > &arrPos)
{
	fPoint pos(0, 0);
	for (int i = 0 ; i < arrPos.GetSize() ; i++)
		pos += arrPos[i];
	return pos/arrPos.GetSize();
}

template <class T>
Tmpl3DPoint <T> MeanPointArray(const CVipArrBase < Tmpl3DPoint<T> > &arrPos)
{
	f3DPoint pos(0, 0, 0);
	for (int i = 0 ; i < arrPos.GetSize() ; i++)
		pos += arrPos[i];
	return pos/arrPos.GetSize();
}

#endif // _POINT_FUNCTIONS
