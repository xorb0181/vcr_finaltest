//////////////////////////////////////////////////////////////////////
//
// VipLib.h: Include All Header & Library of Vip Library
//
//////////////////////////////////////////////////////////////////////

/** @file VipLib.h */

#if !defined(_VipLib_Include)
#define _VipLib_Include

#include "vipl.h"

#ifndef _NO_IPL_
	#include "opencv.h"
	#include "VipWndTools.h"
	#include "VipOpenCVTools.h"
#endif

#endif
