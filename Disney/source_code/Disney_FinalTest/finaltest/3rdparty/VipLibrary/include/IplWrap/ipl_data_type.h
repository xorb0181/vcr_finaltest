/*
//
//               INTeL CORPORATION PROPRIETARY INFORMATION
//  This software is supplied under the terms of a license agreement or
//  nondisclosure agreement with Intel Corporation and may not be copied
//  or disclosed except in accordance with the terms of that agreement.
//        Copyright (c) 1995 Intel Corporation. All Rights Reserved.
//
//
//  Purpose: IPL Common Header file
//
*/

#if !defined (__IPL_H__) || defined (_OWN_BLDPCS)
#define __IPL_H__

#if defined( _WIN32 )
	#define __STDCALL __stdcall
	#define __CDECL __cdecl
	#define __INT64 __int64
#else
	#define __STDCALL
	#define __CDECL
	#define __INT64 long long
#endif

#if defined( _WIN32 )
	#define __INLINE static __inline
#elif defined( __ICC )
	#define __INLINE static __inline
#elif defined( __GNUC__ )
	#define __INLINE static __inline__
#else
	#define __INLINE static
#endif

#if defined( _VXWORKS )
    #include "types/vxTypesOld.h"
#endif

#if !defined (_INC_WINDOWS) && !defined (_WINDOWS_)
	#if !defined (IPL_WINDOWS) && defined( _WIN32 )
		#include <windows.h> /* include wingdi.h for BITMAPINFOHEADER declaration*/
	#else
		#include "iplwind.h" /* smaller file */
	#endif
#endif


#ifdef __cplusplus
extern "C" {
#endif

/*----------------------  IPLibrary call conversion  ----------------------*/
#if !defined IPLAPI
	#if defined(IPL_W32DLL)
		#ifndef __BORLANDC__
			#define IPLAPI(type,name,arg) extern __declspec(dllimport) type __STDCALL name arg;
		#else
			#define IPLAPI(type,name,arg) extern type _import __STDCALL name arg;
		#endif
	#else
		#define IPLAPI(type,name,arg)            extern type __STDCALL name arg;
	#endif
#endif

/*==========================================================================
       Section: IPLibrary Definitions
 ==========================================================================*/

#define IPL_DEPTH_SIGN 0x80000000               
#define IPL_DEPTH_MASK 0x7FFFFFFF               

#define IPL_DEPTH_1U     1
#define IPL_DEPTH_8U     8
#define IPL_DEPTH_16U   16
#define IPL_DEPTH_32F   32

#define IPL_DEPTH_8S  (IPL_DEPTH_SIGN| 8)
#define IPL_DEPTH_16S (IPL_DEPTH_SIGN|16)
#define IPL_DEPTH_32S (IPL_DEPTH_SIGN|32)

#define IPL_DATA_ORDER_PIXEL  0
#define IPL_DATA_ORDER_PLANE  1

#define IPL_ORIGIN_TL 0
#define IPL_ORIGIN_BL 1

#define IPL_ALIGN_4BYTES   4
#define IPL_ALIGN_8BYTES   8
#define IPL_ALIGN_16BYTES 16
#define IPL_ALIGN_32BYTES 32

#define IPL_ALIGN_DWORD   IPL_ALIGN_4BYTES
#define IPL_ALIGN_QWORD   IPL_ALIGN_8BYTES

#define IPL_GET_TILE_TO_READ   1
#define IPL_GET_TILE_TO_WRITE  2
#define IPL_RELEASE_TILE       4

#define IPL_LUT_LOOKUP 0
#define IPL_LUT_INTER  1


 
/*==========================================================================
     Section: IPLibrary Types
 ==========================================================================*/

#if !defined _OWN_BLDPCS

  typedef int  IPLStatus;
  typedef unsigned char   uchar;

  #if defined( linux )
    #include <sys/types.h>
  #elif defined( _VXWORKS )
    typedef unsigned int    uint; 
  #else
    typedef unsigned short  ushort;
    typedef unsigned int    uint; 
  #endif

typedef struct _IplImage {
    int             nSize;              /* size of iplImage struct         */
    int             ID;                 /* version                         */
    int             nChannels;
    int             alphaChannel;
    int             depth;              /* pixel depth in bits             */
    char            colorModel[4];
    char            channelSeq[4];
    int             dataOrder;
    int             origin;
    int             align;              /* 4 or 8 byte align               */
    int             width;
    int             height;
    struct _IplROI *roi;
    struct _IplImage
                   *maskROI;            /* poiner to maskROI if any        */
    void           *imageId;            /* use of the application          */
    struct
      _IplTileInfo *tileInfo;           /* contains information on tiling  */
    int             imageSize;          /* useful size in bytes            */
    char           *imageData;          /* pointer to aligned image        */
    int             widthStep;          /* size of aligned line in bytes   */
    int             BorderMode[4];      /*                                 */
    int             BorderConst[4];     /*                                 */
    char           *imageDataOrigin;    /* ptr to full, nonaligned image   */
} IplImage;

typedef struct _IplROI {
    int             coi;
    int             xOffset;
    int             yOffset;
    int             width;
    int             height;
} IplROI;



/* /////////////////////////////////////////////////////////////////////////
// type IplCallBack
// Purpose:    Type of functions for access to external manager of tile
// Parameters: 
//   img           - header provided for the parent image
//   xIndex,yIndex - indices of the requested tile. They refer to the tile
//                   number not pixel number, and count from the origin at (0,0)
//   mode          - one of the following:
//    IPL_GET_TILE_TO_READ  - get a tile for reading;
//                            tile data is returned in "img->tileInfo->tileData",
//                            and must not be changed
//    IPL_GET_TILE_TO_WRITE - get a tile for writing;
//                            tile data is returned in "img->tileInfo->tileData"
//                            and may be changed;
//                            changes will be reflected in the image
//    IPL_RELEASE_TILE      - release tile; commit writes
//
// Notes: Memory pointers provided by a get function will not be used after the
//        corresponding release function has been called.
//
*/

typedef void (__STDCALL *IplCallBack)(const IplImage* img, int xIndex,
                                                          int yIndex, int mode);

typedef struct _IplTileInfo {
    IplCallBack     callBack;         /* callback function                  */
    void           *id;               /* additional identification field    */
    char           *tileData;         /* pointer on tile data               */
    int             width;            /* width of tile                      */
    int             height;           /* height of tile                     */
} IplTileInfo;

typedef struct _IplLUT {
    int             num;
    int            *key;
    int            *value;
    int            *factor;
    int             interpolateType;
} IplLUT;

typedef struct _IplColorTwist {
    int           data[16];
    int           scalingValue;
} IplColorTwist;


typedef struct _IplConvKernel {
    int           nCols;
    int           nRows;
    int           anchorX;
    int           anchorY;
    int          *values;
    int           nShiftR;
} IplConvKernel;

typedef struct _IplConvKernelFP {
    int           nCols;
    int           nRows;
    int           anchorX;
    int           anchorY;
    float        *values;
} IplConvKernelFP;

typedef enum {
    IPL_PREWITT_3x3_V=0,
        IPL_PREWITT_3x3_H,
        IPL_SOBEL_3x3_V,   /* vertical   */
        IPL_SOBEL_3x3_H,   /* horizontal */
        IPL_LAPLACIAN_3x3,
        IPL_LAPLACIAN_5x5,
        IPL_GAUSSIAN_3x3,
        IPL_GAUSSIAN_5x5,
        IPL_HIPASS_3x3,
        IPL_HIPASS_5x5,
        IPL_SHARPEN_3x3
} IplFilter;

/*-----------------  IplMomentState Structure Definition  -----------------*/
typedef struct {   
    double scale;  /* value to scale (m,n)th moment */
    double value;  /* spatial (m,n)th moment        */
} ownMoment;

typedef ownMoment IplMomentState[4][4];



/*==========================================================================
      Section: Wavelet transform constants and types.
 ==========================================================================*/
      

/*--------------------  Types of wavelet transforms.  ---------------------*/

typedef enum {
    IPL_WT_HAAR,
    IPL_WT_DAUBLET,
    IPL_WT_SYMMLET,
    IPL_WT_COIFLET,
    IPL_WT_VAIDYANATHAN,
    IPL_WT_BSPLINE,
    IPL_WT_BSPLINEDUAL,
    IPL_WT_LINSPLINE,
    IPL_WT_QUADSPLINE,
    IPL_WT_TYPE_UNKNOWN
} IplWtType;

/*-----------------------  Filters symmetry type.  ------------------------*/
typedef enum {
    IPL_WT_SYMMETRIC,
    IPL_WT_ANTISYMMETRIC,
    IPL_WT_ASYMMETRIC,
    IPL_WT_SYMM_UNKNOWN
} IplWtFiltSymm;


/*---------------------  Filter bank orthogonality.  ----------------------*/
typedef enum {
    IPL_WT_ORTHOGONAL,
    IPL_WT_BIORTHOGONAL,
    IPL_WT_NOORTHOGONAL,
    IPL_WT_ORTH_UNKNOWN
} IplWtOrthType;


/*--------------------------  Filter structure  ---------------------------*/
typedef struct {
    float        *taps;             /* filter taps                         */
    int           length;           /* length of filter                    */
    int           offset;           /* offset of filter                    */
    IplWtFiltSymm symmetry;         /* filter symmetry property            */
} IplWtFilter;


/*---------------  Wavelet functions interchange structure  ---------------*/
typedef struct {
    IplWtType     type;             /* type of wavelet transform           */
    int           par1;             /* first param.  (transform order)     */
    int           par2;             /* second param. (only for biorth. tr.)*/
    IplWtOrthType orth;             /* orthogonality property              */
    IplWtFilter   filtDecLow;       /* low-pass decomposition filter       */
    IplWtFilter   filtDecHigh;      /* high-pass decomposition filter      */
    IplWtFilter   filtRecLow;       /* low-pass reconstruction filter      */
    IplWtFilter   filtRecHigh;      /* high-pass reconstruction filter     */
} IplWtKernel;



/*---------------------  Noise generators structure  ----------------------*/
typedef enum {
    IPL_NOISE_UNIFORM,
    IPL_NOISE_GAUSSIAN
} IplNoise;

typedef struct _IplNoiseParam {
    IplNoise     noise;
    unsigned int seed;
    int          lowInt;
    int          highInt;
    float        lowFlt;
    float        highFlt;
} IplNoiseParam;

#endif

/*==========================================================================
      Section: Image Creation Functions
 ==========================================================================*/

#define IPL_BORDER_CONSTANT   0
#define IPL_BORDER_REPLICATE  1
#define IPL_BORDER_REFLECT    2
#define IPL_BORDER_WRAP       3

/*---  Indexes to access IplImage.BorderMode[],IplImage.BorderConst[]  ----*/
#define IPL_SIDE_TOP_INDEX    0
#define IPL_SIDE_BOTTOM_INDEX 1
#define IPL_SIDE_LEFT_INDEX   2
#define IPL_SIDE_RIGHT_INDEX  3

/*----------  values of argument of iplSetBorderMode(,,border,)  ----------*/
#define IPL_SIDE_TOP        (1<<IPL_SIDE_TOP_INDEX)
#define IPL_SIDE_BOTTOM     (1<<IPL_SIDE_BOTTOM_INDEX)
#define IPL_SIDE_LEFT       (1<<IPL_SIDE_LEFT_INDEX)
#define IPL_SIDE_RIGHT      (1<<IPL_SIDE_RIGHT_INDEX)
#define IPL_SIDE_ALL \
     (IPL_SIDE_RIGHT | IPL_SIDE_TOP | IPL_SIDE_LEFT | IPL_SIDE_BOTTOM)


#define IPL_IMAGE_HEADER 1
#define IPL_IMAGE_DATA   2
#define IPL_IMAGE_ROI    4
#define IPL_IMAGE_TILE   8
#define IPL_IMAGE_MASK  16
#define IPL_IMAGE_ALL (IPL_IMAGE_HEADER|IPL_IMAGE_DATA|\
                       IPL_IMAGE_TILE|IPL_IMAGE_ROI|IPL_IMAGE_MASK)
#define IPL_IMAGE_ALL_WITHOUT_MASK (IPL_IMAGE_HEADER|IPL_IMAGE_DATA|\
                       IPL_IMAGE_TILE|IPL_IMAGE_ROI)

/*----------  Consts of Palette conversion for iplConvertToDIB*  ----------*/
#define IPL_PALCONV_NONE      0
#define IPL_PALCONV_POPULATE  1
#define IPL_PALCONV_MEDCUT    2

/*==========================================================================
      Section: Alpha-blending Functions
 ==========================================================================*/

#define IPL_COMPOSITE_OVER    0
#define IPL_COMPOSITE_IN      1
#define IPL_COMPOSITE_OUT     2
#define IPL_COMPOSITE_ATOP    3
#define IPL_COMPOSITE_XOR     4
#define IPL_COMPOSITE_PLUS    5


#define IPL_SUM               0
#define IPL_SUMSQ             1
#define IPL_SUMSQROOT         2
#define IPL_MAX               3
#define IPL_MIN               4


/*-----------------------  Fast Fourier Transform  ------------------------*/

#define  IPL_FFT_Forw                     1
#define  IPL_FFT_Inv                      2
#define  IPL_FFT_NoScale                  4
#define  IPL_FFT_UseInt                  16
#define  IPL_FFT_UseFloat                32
#define  IPL_FFT_Free                   128


/*----------------------  Discrete Cosine Transform  ----------------------*/

#define  IPL_DCT_Forward      1
#define  IPL_DCT_Inverse      2
#define  IPL_DCT_Free         8
#define  IPL_DCT_UseInpBuf    16



/*==========================================================================
      Section: Color Space Conversion
 ==========================================================================*/

#define IPL_JITTER_NONE       0
#define IPL_DITHER_NONE       1
#define IPL_DITHER_FS         2
#define IPL_DITHER_JJH        4
#define IPL_DITHER_STUCKEY    8
#define IPL_DITHER_BAYER     16


/*==========================================================================
      Section: Geometric Transformation Functions
 ==========================================================================*/

/*------------------------  Kind of Interpolation  ------------------------*/

#define IPL_INTER_NN          0
#define IPL_INTER_LINEAR      1
#define IPL_INTER_CUBIC       2
#define IPL_INTER_SUPER       3
#define IPL_SMOOTH_EDGE      16


#define IPL_WARP_R_TO_Q  0
#define IPL_WARP_Q_TO_R  1

/*==========================================================================
      Section: Image Features functions
 ==========================================================================*/

/*------------------  Definitions of the type of norms  -------------------*/
#define IPL_C   1
#define IPL_L1  2
#define IPL_L2  4
#define IPL_RELATIVE   8
#define IPL_RELATIVEC  (IPL_RELATIVE | IPL_C )
#define IPL_RELATIVEL1 (IPL_RELATIVE | IPL_L1 )
#define IPL_RELATIVEL2 (IPL_RELATIVE | IPL_L2 )

/*==========================================================================
      Section: Misc macros and definitions
 ==========================================================================*/

#define IPL_EPS  (1.0E-12)
#define IPL_PI   (3.14159265358979324)  /* ANSI C does not support M_PI */
#define IPL_2PI  (6.28318530717958648)
#define IPL_PI_2 (1.57079632679489662)
#define IPL_PI_4 (0.785398163397448310)

#define IPL_DegToRad(deg) ((deg)/180.0 * IPL_PI)

#define IPLsDegToRad(deg) ((float) ((deg)/180.0 * IPL_PI))
#define IPLdDegToRad(deg) ((double)((deg)/180.0 * IPL_PI))

#ifndef MAX
# define MAX(a,b) (((a) > (b)) ? (a) : (b))
#endif
#ifndef MIN
# define MIN(a,b) (((a) < (b)) ? (a) : (b))
#endif
    
#ifndef FALSE
# define FALSE 0
# define TRUE  1
#endif

/*----------------------  Code for channel sequence  ----------------------*/
#define IPL_CSEQ_G      0x00000047      /* "G"    */
#define IPL_CSEQ_GRAY   0x59415247      /* "GRAY" */
#define IPL_CSEQ_BGR    0x00524742      /* "BGR"  */
#define IPL_CSEQ_BGRA   0x41524742      /* "BGRA" */
#define IPL_CSEQ_RGB    0x00424752      /* "RGB"  */
#define IPL_CSEQ_RGBA   0x41424752      /* "RGBA" */

#ifdef __cplusplus
}
#endif

#endif /*__IPL_H__*/
