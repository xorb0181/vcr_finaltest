// PopUp.cpp : implementation file
//

#include "stdafx.h"
#include "testJig.h"
#include "PopUp.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CPopUp dialog


CPopUp::CPopUp(CWnd* pParent /*=NULL*/)
	: CDialog(CPopUp::IDD, pParent)
{
	//{{AFX_DATA_INIT(CPopUp)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT

//  m_fmsg.CreateFont(28, 0, 0, 0, FW_BOLD, FALSE, FALSE, 0, DEFAULT_CHARSET,
//    OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_SWISS,
//    "Arial");
//  m_static_msg.SetFont(&m_fmsg);
}


void CPopUp::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CPopUp)
	DDX_Control(pDX, IDC_STATIC_MESSAGE, m_static_msg);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CPopUp, CDialog)
	//{{AFX_MSG_MAP(CPopUp)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CPopUp message handlers

//
//
//
void CPopUp::setTextMessage(char* szMsg)
{
  SetDlgItemText(IDC_STATIC_MESSAGE, szMsg);
}
